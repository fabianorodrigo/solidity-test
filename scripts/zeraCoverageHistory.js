const fs = require('fs');
const path = require('path');
const fsextra = require("fs-extra")

const REPORT_FILE_NAME = 'solidity-test-report.json';
const workdir = path.resolve(__dirname, process.env.WORKDIR || '../workdir');
const backupPath = path.resolve(__dirname, process.env.BACKUPDIR || '../backupResults');

let data = null;
if (fs.existsSync(path.join(workdir, REPORT_FILE_NAME))) {
  data = JSON.parse(fs.readFileSync(path.join(workdir, REPORT_FILE_NAME)).toString());
}

let sucesso = false;
if (process.argv.length < 3) {
  Object.keys(data).forEach((k) => {
    data[k].coverageHistory = [];
    const historyPath = path.resolve(backupPath,k,'history');
    fsextra.emptyDirSync(historyPath);
  });
  sucesso = true;
} else {
  const k = process.argv[2];
  if (Array.isArray(data[k].coverageHistory)) {
    const historyPath = path.resolve(backupPath,k,'history',(data[k].coverageHistory.length-1).toString());
    
    fsextra.emptyDirSync(historyPath);
    fsextra.rmdirSync(historyPath);
    data[k].coverageHistory.pop();
    console.log(`${k}.coverageHistory.length:`,data[k].coverageHistory.length);
    sucesso = true;
  }
}

if (sucesso == true) {
  fs.writeFileSync(path.join(workdir, REPORT_FILE_NAME), JSON.stringify(data));
} else {
  console.log(`Not found: ${process.argv[2]}`);
}
