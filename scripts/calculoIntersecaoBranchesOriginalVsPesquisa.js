const fs = require('fs');
const path = require('path');

const REPORT_FILE_NAME = 'solidity-test-report.json';
const workdir = path.resolve(__dirname, process.env.WORKDIR || '../workdir');
const backupPath = path.resolve(__dirname, process.env.BACKUPDIR || '../backupResults');

let data = null;
if (fs.existsSync(path.join(workdir, REPORT_FILE_NAME))) {
  data = JSON.parse(fs.readFileSync(path.join(workdir, REPORT_FILE_NAME)).toString());
} else {
  throw new Error(`Arquivo não existe`);
}

const resultado = [];

let csv =
  'i;project;positivoOriginalEPesquisa;negativoOriginalEPesquisa;totalIguais;somenteOriginal;somentePesquisa;totalDiferentes\n';

Object.keys(data).forEach((prj) => {
  const covOriginalJSONpath = path.join(data[prj].project, 'coverage.json');
  for (let i = 0; i < 30; i++) {
    const covHistoryPath = path.resolve(backupPath, prj, 'history', i.toString(), 'coverage.json');
    if (!fs.existsSync(covOriginalJSONpath) || !fs.existsSync(covHistoryPath)) return;
    const coverageOriginalJSON = JSON.parse(fs.readFileSync(covOriginalJSONpath).toString());
    const coveragePesquisaJSON = JSON.parse(fs.readFileSync(covHistoryPath).toString());
    Object.keys(coverageOriginalJSON).forEach((arquivo) => {
      Object.keys(coverageOriginalJSON[arquivo].branchMap).forEach((branchId) => {
        const linha = coverageOriginalJSON[arquivo].branchMap[branchId].line;
        if (coveragePesquisaJSON[arquivo] != null && linha != coveragePesquisaJSON[arquivo].branchMap[branchId].line)
          throw new Error(
            `Branch ${branchId} divergente. Original: ${coverageOriginalJSON[arquivo].branchMap[branchId].line}. Pesquisa: ${coveragePesquisaJSON[arquivo].branchMap[branchId].line}`
          );

        resultado.push({
          i: i,
          project: prj,
          id: `${arquivo}.${branchId}.${linha}.true`,
          tipo: 'true',
          line: linha,
          original: coverageOriginalJSON[arquivo].b[branchId][0] > 0,
          pesquisa: coveragePesquisaJSON[arquivo] == null ? 0 : coveragePesquisaJSON[arquivo].b[branchId][0] > 0,
        });
        resultado.push({
          i: i,
          project: prj,
          id: `${arquivo}.${branchId}.${linha}.false`,
          tipo: 'false',
          line: linha,
          original: coverageOriginalJSON[arquivo].b[branchId][1] > 0,
          pesquisa: coveragePesquisaJSON[arquivo] == null ? 0 : coveragePesquisaJSON[arquivo].b[branchId][1] > 0,
        });
      });
    });
  }
});

const keys = Object.keys(data);
keys.sort((a, b) => (a.toLowerCase() < b.toLowerCase() ? -1 : b.toLowerCase() < a.toLowerCase() ? 1 : 0));

keys.forEach((k) => {
  for (let i = 0; i < 30; i++) {
    const totalIguais = resultado.filter((r) => r.project == k && r.i == i && r.original == r.pesquisa).length;
    const totalDiferentes = resultado.filter((r) => r.project == k && r.i == i && r.original != r.pesquisa).length;
    const positivosAmbos = resultado.filter(
      (r) => r.project == k && r.i == i && r.original == true && r.pesquisa == true
    ).length;
    const negativosAmbos = resultado.filter(
      (r) => r.project == k && r.i == i && r.original == false && r.pesquisa == false
    ).length;
    const somenteOriginal = resultado.filter(
      (r) => r.project == k && r.i == i && r.original == true && r.pesquisa == false
    ).length;
    const somentePesquisa = resultado.filter(
      (r) => r.project == k && r.i == i && r.original == false && r.pesquisa == true
    ).length;
    csv += `${i};${k};${positivosAmbos};${negativosAmbos};${totalIguais};${somenteOriginal};${somentePesquisa};${totalDiferentes}\n`;
  }
});

const csvpath = path.join(workdir, 'branchCoverageIntersecao.csv');
fs.writeFileSync(csvpath, csv);
console.log(csvpath);

keys.forEach((prj) => {
  console.log(`=================== ${prj} ===================`);
  console.log(`Iguais:`, resultado.filter((r) => r.project == prj && r.i == 0 && r.original == r.pesquisa).length);
  console.log(
    `ORIGINAL entrou, pesquisa não:`,
    resultado.filter((r) => r.project == prj && r.i == 0 && r.original == true && r.pesquisa == false).length
  );
  console.log(
    `PESQUISA entrou, original não:`,
    resultado.filter((r) => r.project == prj && r.i == 0 && r.original == false && r.pesquisa == true).length
  );
  console.log(`Total:`, resultado.filter((r) => r.project == prj && r.i == 0).length);
});
