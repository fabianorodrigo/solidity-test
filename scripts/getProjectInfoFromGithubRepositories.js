const colors = require('colors');
const fs = require('fs');
const path = require('path');

const REPORT_FILE_NAME = 'solidity-test-report.json';
const dados = JSON.parse(fs.readFileSync(path.join(__dirname, 'gitHubSolidityRepos.json')).toString());
const workdir = path.resolve(__dirname, process.env.WORKDIR || '../workdir');

let data = null;
if (fs.existsSync(path.join(workdir, REPORT_FILE_NAME))) {
  data = JSON.parse(fs.readFileSync(path.join(workdir, REPORT_FILE_NAME)).toString());
} else {
  throw new Error(`Arquivo não existe`);
}

const prjRepos = [
  'Dev43/ethinitium/modules/challenges/token_challenge/06',
  'chaitanyapotti/Action',
  'atpar/actus-solidity',
  'PegaSysEng/aiakos-contracts',
  'abcoathup/button',
  'worldofatlantis/crowdsale',
  'tarrencev/curve-bonded-tokens',
  'kern/debug-solidity',
  'willjgriff/solidity-playground/EIPsERCs/EIP712',
  'gongf05/equilibrium-bonding-curve',
  'willjgriff/solidity-playground/EIPsERCs/ERC165',
  'hav-noms/final-project-hav-noms',
  'golemfactory/golem-contracts',
  'bin-studio/harberger-ads-contracts',
  'abcoathup/king',
  'harmvandendorpel/left-gallery-token',
  'convergentcx/liquid',
  'cds-blog-code-samples/MarketSample',
  'chaitanyapotti/MembershipVerificationToken',
  'willjgriff/solidity-playground/Misc/MetaTransactions',
  'Onther-Tech/minime',
  'Dev43/ethinitium/advanced_modules/plasma/mvp',
  'noia-network/noia-token',
  'abcoathup/ponyBadges',
  'brenj/real-estate-marketplace',
  'nachomazzara/SafeERC20',
  'parity-contracts/secretstore-acl',
  'tokenfoundry/securities',
  'CYBRToken/simple-shared-wallet',
  'jackleslie/SimpleCoin',
  'hamdiallam/Solidity-RLP',
  'dsys/solidity-sigutils',
  'hilmarx/splitter',
  'MyWishPlatform/swaps',
  'MyWishPlatform/swaps2',
  'relevant-community/token',
  'dreamteam-gg/trickle-smart-contracts',
  'nicholashc/MerkleShip/truffle',
  'maxsam4/try-catch-solidity',
  'HAECHI-LABS/vvisp-klaytn-sample',
];

let csv = 'project;dtCriacaoRepo;diasAte20201117;mesesAte20201117;anosAte20201117;stars;watchers;forks;issues;ramos\n';

prjRepos.forEach((prj) => {
  let chave = prj;
  const parts = prj.split('/');
  if (dados[chave] == null) {
    chave = parts[0].concat('/', parts[1]);
  }
  if (dados[chave] == null) {
    console.log(colors.red(chave));
    process.exit();
  }
  const idProjeto = parts[parts.length - 1];
  const projeto = data[idProjeto];
  const repo = dados[chave].repo;
  const creationDate = new Date(repo.created_at);
  const fatorDias = 1000 * 60 * 60 * 24;
  const fatorMes = 1000 * 60 * 60 * 24 * 30;
  const fatorAno = 1000 * 60 * 60 * 24 * 365;
  const idadeDias = Math.floor(Date.now() - creationDate.getTime() / fatorDias);
  const idadeMeses = Math.floor(Date.now() - creationDate.getTime() / fatorMes);
  const idadeAnos = ((Date.now() - creationDate.getTime()) / fatorAno).toFixed(1);
  console.log(chave, idadeDias, idadeMeses, idadeAnos);

  csv += idProjeto.concat(
    ';',
    creationDate.toISOString(),
    ';',
    idadeDias.toString(),
    ';',
    idadeMeses.toString(),
    ';',
    idadeAnos,
    ';',
    repo.stargazers_count,
    ';',
    repo.watchers_count,
    ';',
    repo.forks_count,
    ';',
    repo.open_issues_count,
    ';',
    projeto.benchmarkingCoverage.Branches.total,
    '\n'
  );
});

const csvpath = path.join(workdir, 'projectRepoData.csv');
fs.writeFileSync(csvpath, csv);
console.log(csvpath);

/*Object.values(dados).forEach((prj, i) => {
  const creationDate = new Date(prj.repo.created_at);
  const ano = creationDate.getFullYear();
  console.log(i, prj.repo.name, prj.repo.created_at, ano);
});

let csv = 'i;project;covered;total;percentual;original\n';
Object.keys(data).forEach((k) => {
  data[k].coverageHistory.forEach((cov, i) => {
    csv += `${i};${k};${cov.Branches.covered};${cov.Branches.total};${cov.Branches.percent};${
      data[k].benchmarkingCoverage ? data[k].benchmarkingCoverage.Branches.percent : ''
    }\n`;
  });
});


*/
