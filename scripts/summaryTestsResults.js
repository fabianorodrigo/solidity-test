const fs = require('fs');
const path = require('path');
const colors = require('colors');

//const REPORT_FILE_NAME = 'solidity-test-report.json';
const workdir = path.resolve(__dirname, process.env.WORKDIR || '../workdir');

const backupPath = path.resolve(__dirname, process.env.BACKUPDIR || '../backupResults');

const results = [];

fs.readdirSync(backupPath).forEach((d) => {
  const historyPath = path.resolve(backupPath, d, 'history');
  for (let i = 0; i < 60; i++) {
    const coveragePath = path.resolve(historyPath, i.toString(), 'coverage.out');
    if (fs.existsSync(coveragePath)) {
      const out = fs.readFileSync(coveragePath);
      const shouldExec = getShouldExecute(out);
      const shouldFail = getShouldFail(out);
      const shouldFailSpecifics = getShouldFailSpecifics(out);
      console.log(
        d.magenta,
        ':',
        colors.yellow((shouldExec.passed + shouldExec.fail + shouldFail.passed + shouldFail.fail).toString()),
        `sucesso OK:`,
        shouldExec.passed.toString().green,
        `sucesso FAIL:`,
        shouldExec.fail.toString().red,
        `excecao OK:`,
        shouldFail.passed.toString().green,
        `excecao FAIL:`,
        shouldFail.fail.toString().red,
        `excecao FAIL msg.sender == 0x0`,
        shouldFailSpecifics.msgSenderZero,
        `excecao FAIL - revert not expected (before)`,
        shouldFailSpecifics.revertNotExpected,
        `excecao FAIL - invalid opcode`,
        shouldFailSpecifics.invalidOpcode,
        `excecao FAIL - did not fail`,
        shouldFailSpecifics.didNotFail
      );
      results.push(
        d.concat(
          ';',
          (shouldExec.passed + shouldExec.fail + shouldFail.passed + shouldFail.fail).toString(),
          ';',
          shouldExec.passed.toString(),
          ';',
          shouldExec.fail.toString(),
          ';',
          shouldFail.passed.toString(),
          ';',
          shouldFail.fail.toString(),
          ';',
          shouldFailSpecifics.msgSenderZero.toString(),
          ';',
          shouldFailSpecifics.revertNotExpected.toString(),
          ';',
          shouldFailSpecifics.invalidOpcode.toString(),
          ';',
          shouldFailSpecifics.didNotFail.toString()
        )
      );
    }
  }
});

let csv =
  'project;totalCases;success_passed;success_fail;exception_passed;exception_fail;exception_fail_sender_zero;exception_fail_before;invalid_opcode;didNotFail\n';
results.forEach((line) => {
  csv += line + `\n`;
});

const csvpath = path.join(workdir, 'testCaseResults.csv');
fs.writeFileSync(csvpath, csv);
console.log(csvpath);

function getShouldExecute(textOut) {
  const regexExecutePassed = /(\[90m|✓) Should execute (.)*/gm;
  const regexExecuteFail = /(\[31m  )?([0-9]*)\) Should execute(.)*/gm;
  const result = { passed: 0, fail: 0 };
  let m;

  //Executados com sucesso
  while ((m = regexExecutePassed.exec(textOut)) !== null) {
    // This is necessary to avoid infinite loops with zero-width matches
    if (m.index === regexExecutePassed.lastIndex) {
      regexExecutePassed.lastIndex++;
    }
    result.passed++;
  }

  //falharam na execução
  while ((m = regexExecuteFail.exec(textOut)) !== null) {
    // This is necessary to avoid infinite loops with zero-width matches
    if (m.index === regexExecuteFail.lastIndex) {
      regexExecuteFail.lastIndex++;
    }
    result.fail++;
  }

  return result;
}

function getShouldFail(textOut) {
  const regexExecutePassed = /(\[90m|✓) Should fail (.)*/gm;
  const regexExecuteFail = /(\[31m  )?([0-9]*)\) Should fail(.)*/gm;
  const result = { passed: 0, fail: 0 };
  let m;

  //Executados com sucesso
  while ((m = regexExecutePassed.exec(textOut)) !== null) {
    // This is necessary to avoid infinite loops with zero-width matches
    if (m.index === regexExecutePassed.lastIndex) {
      regexExecutePassed.lastIndex++;
    }
    result.passed++;
  }

  //falharam na execução
  while ((m = regexExecuteFail.exec(textOut)) !== null) {
    // This is necessary to avoid infinite loops with zero-width matches
    if (m.index === regexExecuteFail.lastIndex) {
      regexExecuteFail.lastIndex++;
    }
    result.fail++;
  }

  return result;
}

function getShouldFailSpecifics(textOut) {
  const regexMsgSenderZero = /Should fail (.+)when NOT comply with: msg\.sender != 0(x(0)*)?:\s(.)* AssertionError: Expected to fail with revert, but failed with/gm;
  const regexRevertNotExpected = /Should fail (.)*:\s(.)*Returned error: VM Exception while processing transaction: revert/gm;
  const regexInvalidOpcode = /Should fail (.)*:\s(.)*Returned error: VM Exception while processing transaction: invalid opcode/gm;
  const regexDidNotFail = /\[31m     AssertionError: Did not fail/gm;

  const result = { msgSenderZero: 0, revertNotExpected: 0, invalidOpcode: 0, didNotFail: 0 };
  let m;

  //Executados com sucesso
  while ((m = regexMsgSenderZero.exec(textOut)) !== null) {
    // This is necessary to avoid infinite loops with zero-width matches
    if (m.index === regexMsgSenderZero.lastIndex) {
      regexMsgSenderZero.lastIndex++;
    }
    result.msgSenderZero++;
  }

  //falharam na execução
  while ((m = regexRevertNotExpected.exec(textOut)) !== null) {
    // This is necessary to avoid infinite loops with zero-width matches
    if (m.index === regexRevertNotExpected.lastIndex) {
      regexRevertNotExpected.lastIndex++;
    }
    result.revertNotExpected++;
  }

  //falharam na execução
  while ((m = regexInvalidOpcode.exec(textOut)) !== null) {
    // This is necessary to avoid infinite loops with zero-width matches
    if (m.index === regexInvalidOpcode.lastIndex) {
      regexInvalidOpcode.lastIndex++;
    }
    result.invalidOpcode++;
  }

  //falharam na execução
  while ((m = regexDidNotFail.exec(textOut)) !== null) {
    // This is necessary to avoid infinite loops with zero-width matches
    if (m.index === regexDidNotFail.lastIndex) {
      regexDidNotFail.lastIndex++;
    }
    result.didNotFail++;
  }

  return result;
}

/*
let data = null;
if (fs.existsSync(path.join(workdir, REPORT_FILE_NAME))) {
  data = JSON.parse(fs.readFileSync(path.join(workdir, REPORT_FILE_NAME)).toString());
}

let csv = 'project;covered;total;percentual;original\n';
Object.keys(data).forEach((k) => {
  data[k].coverageHistory.forEach((cov) => {
    csv += `${k};${cov.Branches.covered};${cov.Branches.total};${cov.Branches.percent};${
      data[k].benchmarkingCoverage ? data[k].benchmarkingCoverage.Branches.percent : ''
    }\n`;
  });
});

const csvpath = path.join(workdir, 'branchCoverage.csv');
fs.writeFileSync(csvpath, csv);
console.log(csvpath);
*/
