const colors = require('colors');
const fs = require('fs');
const fsextra = require('fs-extra');
const path = require('path');
const readlineSync = require('readline-sync');

const loaders = require('../loaders');

loaders.init();

(async () => {
  const dados = JSON.parse(fs.readFileSync(path.join(__dirname, 'gitHubSolidityRepos.json')));
  console.log(Object.keys(dados).length);
  const contagemFinal = {};
  let maxCreationDate2019 = null;
  Object.values(dados).forEach(prj => {
    const creationDate = new Date(prj.repo.created_at);
    const ano = creationDate.getFullYear();
    console.log(prj.repo.created_at, ano);
    if (ano == 2019) {
      console.log(colors.red(prj.repo.created_at));
      if (creationDate > maxCreationDate2019) {
        maxCreationDate2019 = creationDate;
      }
    }
    if (!contagemFinal[ano]) {
      contagemFinal[ano] = 0;
    }
    contagemFinal[ano]++;
  });
  console.log(contagemFinal);
  console.log(maxCreationDate2019);
  readlineSync.question('Finished! Press any key to exit');
})();
