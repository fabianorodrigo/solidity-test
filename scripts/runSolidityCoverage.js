const colors = require('colors');
const fs = require('fs');
const fsextra = require('fs-extra');
const path = require('path');
const readlineSync = require('readline-sync');

const shelljs = require('shelljs');
const loaders = require('../loaders');

const jsdom = require('jsdom');
const { JSDOM } = jsdom;

global.generalResults = {};

const REPORT_FILE_NAME = 'solidity-test-report.json';

loaders.init();

(async () => {
  const workdir = path.resolve(__dirname, process.env.WORKDIR || '../workdir');
  if (fs.existsSync(path.join(workdir, REPORT_FILE_NAME))) {
    global.generalResults = JSON.parse(fs.readFileSync(path.join(workdir, REPORT_FILE_NAME)));
  }

  let forceExecution = false;
  if (process.argv.length > 2) {
    if (['-f'].includes(process.argv[2])) {
      forceExecution = true;
    } else {
      throw new Error('Opção desconhecida: '.concat(process.argv[2]));
    }
  }

  fs.readdirSync(workdir).forEach(prjBaseName => {
    const dirPath = path.join(workdir, prjBaseName);
    if (fs.lstatSync(dirPath).isDirectory()) {
      console.log(colors.red(dirPath));
      //Se o arquivo não contémo projeto, cria a estrutura
      if (global.generalResults[prjBaseName] == null) {
        global.generalResults[prjBaseName] = {};
        global.generalResults[prjBaseName].project = dirPath;
      }
      //Se a cobertura já foi executada com sucesso, não executa novamente
      if (!global.generalResults[prjBaseName].coverageSuccessful || forceExecution) {
        shelljs.cd(dirPath);
        const coverageJSON = path.join(dirPath, 'coverage.json');
        const coverageDir = path.join(dirPath, 'coverage');
        fsextra.emptyDirSync(coverageDir);
        fs.rmdir(coverageDir, err => {
          if (err) {
            console.error(colors.red(`Falha ao apagar ${coverageDir}: ${err.message}`));
          }
        });
        if (fs.existsSync(coverageJSON)) {
          fs.unlink(coverageJSON, err => {
            if (err) {
              console.error(colors.red(`Falha ao apagar ${coverageJSON}: ${err.message}`));
            }
          });
        }
        try {
          const filePathPackagejson = path.join(dirPath, 'package.json');
          let packageJSON = null;
          if (fs.existsSync(filePathPackagejson)) {
            packageJSON = JSON.parse(fs.readFileSync(filePathPackagejson));
          }
          if (packageJSON && packageJSON.scripts && packageJSON.scripts.coverage) {
            shelljs.exec('npm run coverage');
          } else if (packageJSON && packageJSON.dependencies && packageJSON.dependencies['solidity-coverage']) {
            shelljs.exec('npx solidity-coverage');
          } else {
            shelljs.exec('solidity-coverage');
          }
        } catch (e) {
          shelljs.touch(path.join(dirPath, 'failSolidityCoverage'));
          global.generalResults[prjBaseName].coverageSuccessful = false;
          fs.writeFileSync(path.join(workdir, REPORT_FILE_NAME), JSON.stringify(global.generalResults));
          return;
        }
        global.generalResults[prjBaseName].coverageSuccessful = true;
        global.generalResults[prjBaseName].dhFinishCoverage = null;
        global.generalResults[prjBaseName].coverage = {};
        const indexCoverageFile = path.join(dirPath, 'coverage', 'index.html');
        if (fs.existsSync(indexCoverageFile)) {
          const data = fs.readFileSync(indexCoverageFile);

          const dom = new JSDOM(data);
          const divSummary = dom.window.document
            .querySelector('div')
            .querySelector('div')
            .querySelector('div');
          divSummary.childNodes.forEach(child => {
            if (child.childNodes.length > 0) {
              global.generalResults[prjBaseName].coverage[child.childNodes[3].textContent] = {
                percent: child.childNodes[1].textContent,
                total: child.childNodes[5].textContent.split('/')[1],
                covered: child.childNodes[5].textContent.split('/')[0],
              };
            }
          });
        } else {
          global.generalResults[prjBaseName].coverageSuccessful = false;
        }
        global.generalResults[prjBaseName].dhFinishCoverage = new Date();
      }
    }
    fs.writeFileSync(path.join(workdir, REPORT_FILE_NAME), JSON.stringify(global.generalResults));
  });
  fs.writeFileSync(path.join(workdir, REPORT_FILE_NAME), JSON.stringify(global.generalResults));
  shelljs.exec('rhythmbox-client --play-uri assets/mozart-symphony40-1.mp3');
  readlineSync.question('Finished! Press any key to exit');
})();
