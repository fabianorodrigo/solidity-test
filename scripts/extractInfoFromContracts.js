const colors = require('colors');
const fs = require('fs');
const fsextra = require('fs-extra');
const path = require('path');
const readlineSync = require('readline-sync');

const shelljs = require('shelljs');
const loaders = require('../dist/loaders');

const { TruffleProjectIceFactory } = require('../dist/services/TruffleFactory');
const SolidityParserIceFactory = require('../dist/services/SolidityParserFactory');
const solidity = require('../dist/services/solidityParser');

const resultados = [];

//Variaveis de instancia
const instanceVariables = {};
//enums
const enums = {};
//variáveis locais por projeto, contrato, funcao
const localVariables = {};
//Parametros de funcoes
const functionParameters = {};

//solução horrivel, mas nao achei outra forma de fazer de forma 'functional'
let currentProject = '';
let currentContract = '';
let currentFunction = '';

loaders.init();

(async () => {
  const workdir = path.resolve(__dirname, '../workdir');

  fs.readdirSync(workdir).forEach(prjBaseName => {
    const truffleProjectHome = path.join(workdir, prjBaseName);
    if (fs.lstatSync(truffleProjectHome).isDirectory()) {
      console.log(colors.red(truffleProjectHome));
      currentProject = path.basename(truffleProjectHome);
      const truffleProject = TruffleProjectIceFactory({ truffleProjectHome });
      truffleProject.getContracts().forEach(handleContract);
    }
    //fs.writeFileSync(path.join(workdir, REPORT_FILE_NAME), JSON.stringify(global.generalResults));
  });
  console.log(resultados);
  fs.writeFileSync(path.join(workdir, 'infoContracts.csv'), resultados.join('\n'));
  //shelljs.exec('rhythmbox-client --play-uri assets/mozart-symphony40-1.mp3');
  readlineSync.question('Finished! Press any key to exit');
})();

function handleContract(contract) {
  console.log(colors.yellow(contract.solFilePath));
  currentContract = contract.contractDefinition.name;
  collectInstanceVariables(contract.contractDefinition);
  collectEnums(contract.contractDefinition);
  const SolidityParser = SolidityParserIceFactory({ solFilePath: contract.solFilePath });
  const functions = solidity.getFunctions(contract.contractDefinition);
  functions.forEach(handleFunction);
}

function collectInstanceVariables(contract) {
  if (!instanceVariables[currentProject]) instanceVariables[currentProject] = {};
  if (!instanceVariables[currentProject][currentContract]) instanceVariables[currentProject][currentContract] = {};

  contract.subNodes.forEach(n => {
    if (solidity.isStateVariableDeclaration(n)) {
      n.variables
        .filter(v => v != null)
        .forEach(v => {
          instanceVariables[currentProject][currentContract][v.name] = v;
        });
    }
  });
}

function collectEnums(contract) {
  if (!enums[currentProject]) enums[currentProject] = {};
  if (!enums[currentProject][currentContract]) enums[currentProject][currentContract] = {};

  contract.subNodes.forEach(n => {
    // Include all functions (except constructor and privates)
    if (n.type === 'EnumDefinition') {
      enums[currentProject][currentContract][n.name] = n;
    }
  });
}

function handleFunction(f) {
  if (f.name && f.name.trim() != '' && f.body) {
    console.log(colors.blue(f.name));
    currentFunction = f.name;
    collectFunctionParameters(f);
    f.body.statements.forEach(handleStatements);
  }
}

function collectFunctionParameters(f) {
  if (!functionParameters[currentProject]) functionParameters[currentProject] = {};
  if (!functionParameters[currentProject][currentContract]) functionParameters[currentProject][currentContract] = {};
  if (!functionParameters[currentProject][currentContract][currentFunction])
    functionParameters[currentProject][currentContract][currentFunction] = {};

  f.parameters.forEach(v => {
    functionParameters[currentProject][currentContract][currentFunction][v.name] = v;
  });
}

function handleStatements(st) {
  if (solidity.isRequireCall(st)) {
    let restrictions;
    if (solidity.isBO(st.expression.arguments[0])) {
      restrictions = getBinaryOperationRestriction(st.expression.arguments[0]);
    } else if (st.expression) {
      restrictions = [getSideInformation(st.expression.arguments[0])];
    }
    console.log(`${currentProject};${currentContract};${currentFunction};REQUIRE;${restrictions.join(';')}`);
    resultados.push(`${currentProject};${currentContract};${currentFunction};REQUIRE;${restrictions.join(';')}`);
  } else if (solidity.isIf(st)) {
    let restrictions;
    if (solidity.isBO(st.condition)) {
      restrictions = getBinaryOperationRestriction(st.condition);
    } else {
      restrictions = [getSideInformation(st.condition)];
    }
    console.log(`${currentProject};${currentContract};${currentFunction};IF;${restrictions.join(';')}`);
    resultados.push(`${currentProject};${currentContract};${currentFunction};IF;${restrictions.join(';')}`);
  } else if (solidity.isVariableDeclaration(st)) {
    if (!localVariables[currentProject]) localVariables[currentProject] = {};
    if (!localVariables[currentProject][currentContract]) localVariables[currentProject][currentContract] = {};
    if (!localVariables[currentProject][currentContract][currentFunction])
      localVariables[currentProject][currentContract][currentFunction] = {};

    st.variables
      .filter(v => v != null)
      .forEach(v => {
        if (v == null) console.log(st);
        localVariables[currentProject][currentContract][currentFunction][v.name] = v;
      });
  }
}

function getBinaryOperationRestriction(bo) {
  let retorno = [];
  //Se operador lógico e for outro BinaryOperation, chama novamente
  if (bo.operator == '&&' || bo.operator == '||') {
    if (bo.left.type == 'BinaryOperation') {
      retorno = retorno.concat(getBinaryOperationRestriction(bo.left));
    } else {
      retorno = retorno.concat([getSideInformation(bo.left)]);
    }
    //Se operador lógico e for outro BinaryOperation, chama novamente
    if (bo.right.type == 'BinaryOperation') {
      retorno = retorno.concat(getBinaryOperationRestriction(bo.right));
    } else {
      retorno = retorno.concat([getSideInformation(bo.right)]);
    }
  } else {
    let r = getSideInformation(bo.left);
    //r += ' '.concat(bo.operator).concat(' ');
    r += '|';
    r += getSideInformation(bo.right);
    retorno.push(r);
  }

  return retorno;
}

function getSideInformation(side) {
  let r = '';
  if (side.name) {
    if (isLocalVariable(side.name)) {
      r += 'variavelLocal';
    } else if (isFunctionParameter(side.name)) {
      r += 'parametroFuncao';
    } else if (isInstanceVariable(side.name)) {
      r += 'variavelEstado';
    } else if (['block', 'msg', 'this'].indexOf(side.name) > -1) {
      r += side.name;
    } else if (isEnum(side.name)) {
      r += 'Enum';
    } else if (side.type && side.name) {
      r += side.name;
    } else {
      r += `????`; // `${side.name}`;
    }
  } else if (side.type.endsWith('Literal')) {
    //r += getRestrictionValue(side);
    r += 'Literal';
  } else if (side.type == 'FunctionCall') {
    r += 'funcao';
    if (side.expression.typeName) {
      //r += side.expression.typeName.name.concat('(');
      r += '(';
    } else {
      //r += getSideInformation(side.expression).concat('(');
      r += '(';
    }
    side.arguments.forEach((a, i) => {
      if (i > 0) r += ',';
      r += getSideInformation(a);
    });
    r += ')';
  } else if (side.type == 'BinaryOperation') {
    r += getSideInformation(side.left);
    r += '|'; //side.operator;
    r += getSideInformation(side.right);
  } else if (side.type == 'UnaryOperation') {
    r += r += getSideInformation(side.subExpression); //side.operator;
  } else if (side.type == 'IndexAccess') {
    r += getSideInformation(side.base);
    r += '[';
    r += getSideInformation(side.index);
    r += ']';
  } else if (side.type == 'MemberAccess') {
    r += getSideInformation(side.expression);
    r += '.memberAccess';
    //r += side.memberName;
  } else if (side.type == 'TupleExpression') {
    side.components.forEach(comp => {
      r += getSideInformation(comp);
    });
  } else {
    solidity.cleanLocData(side.expression);
    r += side.type + '|' + JSON.stringify(side.expression);
  }
  return r;
}

function isInstanceVariable(name) {
  return (
    instanceVariables[currentProject] &&
    instanceVariables[currentProject][currentContract] &&
    instanceVariables[currentProject][currentContract][name]
  );
}

function isEnum(name) {
  return (
    enums[currentProject] && enums[currentProject][currentContract] && enums[currentProject][currentContract][name]
  );
}

function isLocalVariable(name) {
  return (
    localVariables[currentProject] &&
    localVariables[currentProject][currentContract] &&
    localVariables[currentProject][currentContract][currentFunction] &&
    localVariables[currentProject][currentContract][currentFunction][name]
  );
}

function isFunctionParameter(name) {
  return (
    functionParameters[currentProject] &&
    functionParameters[currentProject][currentContract] &&
    functionParameters[currentProject][currentContract][currentFunction] &&
    functionParameters[currentProject][currentContract][currentFunction][name]
  );
}

/**
 * If it has a valid value restriction, return it. If not, returns null
 *
 * @param {object} binaryOperation  A left or right atribute of a BinaryOperation
 */
function getRestrictionValue(binaryOperation) {
  if (binaryOperation.hasOwnProperty('number') || binaryOperation.hasOwnProperty('value')) {
    return binaryOperation.hasOwnProperty('number') ? binaryOperation.number : binaryOperation.value;
  } else if (
    binaryOperation.type == 'FunctionCall' &&
    binaryOperation.expression.type == 'ElementaryTypeNameExpression' &&
    binaryOperation.expression.typeName.type == 'ElementaryTypeName' &&
    binaryOperation.expression.typeName.name == 'address'
  ) {
    if (binaryOperation.arguments[0].type == 'NumberLiteral' && binaryOperation.arguments[0].number == 0) {
      return '"0x0000000000000000000000000000000000000000"'; //typeof(accounts[0]) == 'string'
    } else if (binaryOperation.arguments[0].type == 'Identifier') {
      //TODO: Cenário ainda não tratado quando se passa uma variável para a função nativa 'address'
      return null;
    } else {
      throw new Error(
        `Parâmetro diferente de '0' passado para a função solidity 'address'. type = ${binaryOperation.arguments[0].type}. number = ${binaryOperation.arguments[0].number}`
      );
    }
  }
  return null;
}
