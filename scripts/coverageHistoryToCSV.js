const fs = require('fs');
const path = require('path');

const REPORT_FILE_NAME = 'solidity-test-report.json';
const workdir = path.resolve(__dirname, process.env.WORKDIR || '../workdir');

let data = null;
if (fs.existsSync(path.join(workdir, REPORT_FILE_NAME))) {
  data = JSON.parse(fs.readFileSync(path.join(workdir, REPORT_FILE_NAME)).toString());
} else {
  throw new Error(`Arquivo não existe`);
}

let csv = 'i;project;covered;total;percentual;original\n';
Object.keys(data).forEach((k) => {
  data[k].coverageHistory.forEach((cov, i) => {
    csv += `${i};${k};${cov.Branches.covered};${cov.Branches.total};${cov.Branches.percent};${
      data[k].benchmarkingCoverage ? data[k].benchmarkingCoverage.Branches.percent : ''
    }\n`;
  });
});

const csvpath = path.join(workdir, 'branchCoverage.csv');
fs.writeFileSync(csvpath, csv);
console.log(csvpath);
