export const transactionParameterAttributes = {
  sender: { solidityName: 'sender', jsName: 'from', typeName: { type: 'ElementaryTypeName', name: 'address' } },
  value: { solidityName: 'value', jsName: 'value', typeName: { type: 'ElementaryTypeName', name: 'uint' } },
  gasprice: { solidityName: 'gasprice', jsName: 'gasPrice', typeName: { type: 'ElementaryTypeName', name: 'uint' } },
};
