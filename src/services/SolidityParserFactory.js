const colors = require('colors');
const fs = require('fs');
const path = require('path');
const antlrParser = require('solidity-parser-antlr');
/**
 * Retornará uma instância imutável de um objeto com funcionalidades para
 * leitura de arquivos Solidity
 *
 * @param {string} pathArquivo Path para o arquivo Solidity
 * @returns {object} Objeto com utilidades para leitura de dados do arquivo Solidity
 */
module.exports = function SolidityParserIceFactory({ solFilePath }) {
  const _astCache = {};
  const _contractsCache = {};
  const _importsCache = {};
  const _valuesPool = { structs: {}, contracts: {} };
  const trace = false;

  // Reference for internal use
  const Grammar = {
    NewExpression,
    InlineAssemblyStatement,
    Conditional,
    ContinueStatement,
    EnumDefinition,
    HexLiteral,
    BreakStatement,
    ImportDirective,
    UsingForDeclaration,
    PragmaDirective,
    ContractDefinition,
    InheritanceSpecifier,
    StateVariableDeclaration,
    ModifierDefinition,
    ModifierInvocation,
    FunctionDefinition,
    EventDefinition,
    VariableDeclarationStatement,
    VariableDeclaration,
    Mapping,
    ArrayTypeName,
    TupleExpression,
    UserDefinedTypeName,
    IndexAccess,
    ParameterList,
    Parameter,
    StructDefinition,
    Block,
    IfStatement,
    WhileStatement,
    ForStatement,
    Identifier,
    MemberAccess,
    UnaryOperation,
    BinaryOperation,
    BooleanLiteral,
    NumberLiteral,
    StringLiteral,
    ElementaryTypeName,
    ElementaryTypeNameExpression,
    ExpressionStatement,
    FunctionCall,
    EmitStatement,
    ReturnStatement,
    FunctionTypeName,
    ThrowStatement,
  };

  return Object.freeze({
    getSolidityPath,
    parse,
    getContracts,
    getImports,
    getFunctionFromContractDefinition,
    visit,
    getSolidityVersions,
    getValuesPool,
    appendValuesPool,
    BinaryOperationSource: BinaryOperation,
    FunctionDefinitionSource: FunctionDefinition,
    BlockSource: Block,
  });

  function getSolidityPath() {
    return solFilePath;
  }

  /**
   * Varre o arquivo Solidity e retorna os valores literais contidos no código (string, uint, address, ...)
   */
  function getValuesPool() {
    getContractSourceCode();
    return _valuesPool;
  }

  /**
   * Varre o arquivo Solidity, obtém os valores literais contidos no código (string, uint, address, ...)
   * e insere no objeto {valuesPool} recebido
   *
   * @param {object} valuesPool Objeto onde os valores serão apendados. Este objeto uma propriedade com o nome de
   * data tipo encontrado (NumberLiteral, StringLiteral, ...) e seu valor será um array com os valores
   */
  function appendValuesPool(valuesPool) {
    if (valuesPool == null) {
      valuesPool = {};
    }
    const pool = getValuesPool();
    Object.keys(pool).forEach((tipo) => {
      if (valuesPool[tipo] == null) {
        valuesPool[tipo] = [];
      }
      if (tipo == 'NumberLiteral') {
        const boundaries = [];
        pool[tipo].forEach((n) => {
          boundaries.push((parseInt(n) + 1).toLocaleString('en-US', { minimumFractionDigits: 0, useGrouping: false }));
          boundaries.push((parseInt(n) - 1).toLocaleString('en-US', { minimumFractionDigits: 0, useGrouping: false }));
        });
        pool[tipo] = pool[tipo].concat(boundaries);
      }
      if (tipo == 'structs' || tipo == 'contracts') {
        Object.keys(pool[tipo]).forEach((structORcontractName) => {
          if (valuesPool[tipo][structORcontractName] == null) {
            valuesPool[tipo][structORcontractName] = pool[tipo][structORcontractName];
          }
        });
      } else {
        pool[tipo].forEach((v) => {
          // Acrescenta apenas valores distinct
          if (valuesPool[tipo].findIndex((e) => e === v) == -1) {
            valuesPool[tipo].push(v);
          }
        });
      }
    });
    return valuesPool;
  }

  /**
   * Read the SOLIDITY file and returns it's AST
   *
   * @param {string} solFilePath Path of the solidity file
   * @param {boolean} force If FALSE (default) and exists in the {_astCache} return it, otherwise, read the file, parse it and store in the cache
   */
  function parse({ force = false }) {
    if (force || _astCache[solFilePath] == null) {
      const solContent = fs.readFileSync(solFilePath, 'UTF8');
      try {
        const ast = antlrParser.parse(solContent, { loc: true });
        _astCache[solFilePath] = ast;
      } catch (e) {
        if (e instanceof antlrParser.ParserError) {
          console.error('ParserError', e.errors);
        } else {
          console.error('Error', e.errors);
        }
      }
    } else {
      // console.log(solFilePath, 'AST retrieved from cache')
    }
    return _astCache[solFilePath];
  }

  /**
   * Extract ContractDefinition from the Abstract Syntax Tree retrieved from the solidity file
   *
   * @param {boolean} force If FALSE (default) and exists in the {_contractsCache} return it, otherwise, read the file, parse it and store in the cache
   */
  function getContracts(force = false) {
    const ast = parse(solFilePath);
    if (force || _contractsCache[solFilePath] == null) {
      const contracts = [];
      const pragmaDirectives = {};
      // output the kind of each ContractDefinition found
      antlrParser.visit(ast, {
        PragmaDirective(node) {
          pragmaDirectives[node.name] = node.value;
        },
        ContractDefinition(node) {
          contracts.push({ solFilePath, contractDefinition: node, directives: pragmaDirectives });
        },
      });
      _contractsCache[solFilePath] = contracts;
      return contracts;
    }
    // console.log(solFilePath, 'contracts retrieved from cache')
    return _contractsCache[solFilePath];
  }

  /**
   * Extract ImportDerictive from the Abstract Syntax Tree retrieved from the solidity file
   *
   * @param {boolean} force If FALSE (default) and exists in the {_importsCache} return it, otherwise, read the file, parse it and store in the cache
   */
  function getImports(force = false) {
    const ast = parse(solFilePath);
    if (force || _importsCache[solFilePath] == null) {
      const imports = [];
      // output the kind of each ContractDefinition found
      antlrParser.visit(ast, {
        ImportDirective(node) {
          imports.push({ solFilePath, importDirective: node });
        },
      });
      _importsCache[solFilePath] = imports;
      return imports;
    }
    // console.log(solFilePath, 'contracts retrieved from cache')
    return _importsCache[solFilePath];
  }

  /**
   * Read each node of the Abstract Syntax Tree
   * @param {object} ast Abstract Syntax Tree to be read
   */
  function visit(ast) {
    // output the kind of each ContractDefinition found
    antlrParser.visit(ast, {
      FunctionDefinition(node) {
        console.log(node.visibility, node.name);
        console.log('Parameters:', node.parameters.parameters);
        console.log('Return:', node.returnParameters == null ? 'Void' : node.returnParameters.parameters);
        console.log('Body:', node.body.statements);
        console.log('node', node);
      },
    });
  }

  /** *
   * Searchs for a function with the name {functionName} in the {contractDefinition}
   *
   * @param {contractDefinition} contractDefinition A instance of the contractDefinition in the AST from which the functionsDefinition will be retrieved
   * @param {string} functionName Function's name to be taken
   * @returns {functionDefinition} Instance of FunctionDefinition found or NULL, if not found
   */
  function getFunctionFromContractDefinition(contractDefinition, functionName, parametersCount) {
    for (let i = 0; i < contractDefinition.subNodes.length; i++) {
      const sn = contractDefinition.subNodes[i];
      if (sn.type === 'FunctionDefinition' && sn.name === functionName && sn.parameters.length == parametersCount) {
        return sn;
      }
    }
    return null;
  }

  /** *
   * Receive a contract and return a array with all your functions BUT contructor and private ones
   *
   * @param {contractDefinition} contractDefinition A instance of the contractDefinition in the AST from which the functionsDefinition will be retrieved
   * @returns {Array} Array of FunctionDefinition
   */
  function extractFunctionsFromContractDefinition(contractDefinition) {
    const functionsReturn = [];
    contractDefinition.subNodes.forEach((sn) => {
      // Include all functions (except constructor and privates)
      if (sn.type === 'FunctionDefinition' && !sn.isConstructor && sn.visibility != 'private') {
        functionsReturn.push(sn);
      }
    });
    return functionsReturn;
  }

  /**
   * Extract PragmaDirective with the Solidity version from the Abstract Syntax Tree retrieved from the solidity file
   *
   * @param {string} solFilePath Path of the solidity file
   */
  function getSolidityVersions(solFilePath) {
    try {
      const ast = parse(solFilePath);
      const pragmaDirectives = [];
      // output the kind of each ContractDefinition found
      antlrParser.visit(ast, {
        PragmaDirective(node) {
          pragmaDirectives.push(node);
        },
      });
      return pragmaDirectives;
    } catch (e) {
      console.log(`Fail to parse solidity file ${solFilePath}: ${e.message}`);
      return null;
    }
  }

  /**
   * Lê o AST do contrato e retorna o código fonte
   */
  function getContractSourceCode() {
    const ast = parse({});
    if (ast) {
      return ast.children
        .map((child) => {
          if (Grammar[child.type] == null) {
            const msg = `Formatting function of ${child.type} not found`;
            console.warn(msg, child);
            return msg;
          }
          const nodeFormatted = Grammar[child.type] == null ? child.type : Grammar[child.type](child, ast);
          return `${nodeFormatted}\n${child.body ? Grammar.Block(child.body) : ''}`;
        })
        .join('\n');
    }
  }

  function PragmaDirective(n) {
    return `pragma  ${n.name} ${n.value};\n`;
  }

  /**
   * Contracts are marked as abstract when at least one of their functions lacks an implementation
   * https://solidity.readthedocs.io/en/latest/contracts.html#abstract-contracts
   *
   * @param {contractDefinition} contractDefinition The contract representantion in the AST
   */
  function isAbstractContract(contractDefinition) {
    let retorno = false;
    for (let i = 0; i < contractDefinition.subNodes.length; i++) {
      if (contractDefinition.subNodes[i].type == 'FunctionDefinition' && contractDefinition.subNodes[i].body == null) {
        retorno = true;
        break;
      }
    }
    return retorno;
  }

  function ContractDefinition(contract) {
    if (!_valuesPool.UserDefinedTypeName) {
      _valuesPool.UserDefinedTypeName = [];
    }
    if (!_valuesPool.AbstractUserDefinedTypeName) {
      _valuesPool.AbstractUserDefinedTypeName = [];
    }
    if (
      _valuesPool.UserDefinedTypeName.indexOf(contract.name) === -1 &&
      contract.kind != 'library' &&
      contract.kind != 'interface' &&
      !isAbstractContract(contract)
    ) {
      _valuesPool.UserDefinedTypeName.push(contract.name);
    } else if (contract.kind == 'interface' || isAbstractContract(contract)) {
      _valuesPool.AbstractUserDefinedTypeName.push(contract.name);
    }

    if (!_valuesPool[contract.name]) {
      _valuesPool[contract.name] = [];
    }
    if (_valuesPool[contract.name].indexOf(contract.name) === -1) {
      _valuesPool[contract.name].push(contract.name);
    }
    for (let i = 0; i < contract.baseContracts.length; i++) {
      if (!_valuesPool[contract.baseContracts[i].baseName.namePath]) {
        _valuesPool[contract.baseContracts[i].baseName.namePath] = [];
      }
      if (_valuesPool[contract.baseContracts[i].baseName.namePath].indexOf(contract.name) === -1) {
        _valuesPool[contract.baseContracts[i].baseName.namePath].push(contract.name);
      }
    }

    return `${contract.kind} ${contract.name}{\n${contract.baseContracts
      .map((bc, i) => Grammar[bc.type](bc, i))
      .join(', ')} ${contract.subNodes
      .map((subNode) => {
        if (Grammar[subNode.type] == null) {
          const msg = `Formatting function of ${subNode.type} not found`;
          console.warn(msg, subNode);
          return msg;
        }
        const nodeFormatted = Grammar[subNode.type] == null ? subNode.type : Grammar[subNode.type](subNode, contract);
        return `${nodeFormatted}\n${subNode.body ? `{\n${Grammar.Block(subNode.body, subNode)}\n}` : ''}`;
      })
      .join('\n')}
        }`;
  }

  function InheritanceSpecifier(n, order) {
    let baseNameFormatted = Grammar[n.baseName.type](n.baseName);
    if (n.arguments && n.arguments.length > 0) {
      baseNameFormatted += `(${n.arguments.map((a) => `${Grammar[a.type](a, n)}`)})`;
    }
    return `${order == 0 ? ': ' : ''} ${order != null && order > 0 ? ', ' : ''} ${baseNameFormatted} `;
  }

  function StateVariableDeclaration(n) {
    return `${n.variables.map((v) => Grammar.VariableDeclaration(v))};${trace ? '#StateVariableDeclaration' : ''}`;
  }

  function ModifierDefinition(m, contract) {
    return `modifier ${m.name}${trace ? '#ModifierDefinition' : ''}`;
  }

  function ModifierInvocation(m, parent) {
    return m.name.concat(' ');
  }

  function FunctionDefinition(f) {
    let returnCode = f.isConstructor == true ? 'constructor ' : 'function ';

    returnCode += f.isConstructor == true ? '(' : `${f.name}(`;
    returnCode += Grammar.ParameterList(f.parameters);
    returnCode += ') ';
    returnCode += `${f.visibility} `;
    // Remove the 'pure' stateMutatility because pure functions can't emit events
    // TODO: Deveria remover somente se necessário ao final do processo. Uma opção seria reler os mutantes gerados que possuam pure functions enviando evento e remover somente destas
    returnCode += `${f.stateMutability && f.stateMutability != 'pure' ? f.stateMutability : ''} `;
    if (f.modifiers) {
      f.modifiers.forEach((m) => {
        returnCode += Grammar[m.type](m, f);
      });
    }
    returnCode +=
      f.returnParameters && f.returnParameters.parameters && f.returnParameters.parameters.length > 0
        ? ` returns(${Grammar.ParameterList(f.returnParameters)})`
        : '';
    return returnCode;
  }

  function EventDefinition(e) {
    return `event ${e.name} (${Grammar.ParameterList(e.parameters)});${trace ? '#EventDefinition' : ''}`;
  }

  function VariableDeclarationStatement(n, parent) {
    let retorno = '';
    if (trace) console.log('VariableDeclarationStatement', n);
    retorno += n.variables.map((v, i) => Grammar.VariableDeclaration(v, i, n));
    if (n.initialValue) {
      if (Grammar[n.initialValue.type] == null) {
        console.error(`Grammar.${n.initialValue.type} não existe`);
      }
      retorno += ` = ${Grammar[n.initialValue.type](n.initialValue, n)}`;
    }
    if (parent.type != 'IfStatement' && parent.type != 'BinaryOperation' && parent.type != 'FunctionCall') {
      retorno += ';';
    }
    retorno += `${trace ? '#VariableDeclarationStatement' : ''}`;
    return retorno;
  }

  function VariableDeclaration(v, order, parent) {
    if (v == null) {
      return '';
    }
    //bytes são identificados como NumberLiterals
    if (v.typeName?.name?.startsWith('byte') && v.expression?.number) {
      /*if (!_valuesPool.BytesLiteral) {
        _valuesPool.BytesLiteral = [];
      }
      if (_valuesPool.BytesLiteral.indexOf(v.expression?.number) === -1) {
        _valuesPool.BytesLiteral.push(v.expression?.number);
      }*/
    }
    return `${order != null && order > 0 ? ', ' : ''} ${v.isDeclaredConst ? 'const ' : ''} ${
      v.typeName ? Grammar[v.typeName.type](v.typeName) : ''
    } ${v.visibility && v.visibility != 'default' ? v.visibility : ''} ${v.name ? v.name : ''} ${
      v.expression ? `= ${Grammar[v.expression.type](v.expression, v)}` : ''
    }`;
  }

  function FunctionTypeName(n, parent) {
    return `function(${n.parameterTypes.map((p) => Grammar[p.type](p))}) ${n.visibility} ${n.stateMutability} `;
  }

  function Mapping(mapp) {
    return `mapping(${Grammar[mapp.keyType.type](mapp.keyType)} => ${Grammar[mapp.valueType.type](mapp.valueType)})`;
  }
  function ArrayTypeName(ar) {
    return `[${Grammar[ar.baseTypeName.type](ar.baseTypeName)}]`;
  }

  function TupleExpression(te) {
    return (
      te &&
      te.components &&
      te.components.map((p) => `${p ? Grammar[p.type](p, te) : ''}${trace ? '#TupleExpression' : ''}`)
    );
  }

  function UserDefinedTypeName(usdtn) {
    return `${usdtn.namePath}${trace ? '#UserDefinedTypeName' : ''}`;
  }

  function IndexAccess(ia, parent) {
    return `${Grammar[ia.base.type](ia.base, parent)} [${Grammar[ia.index.type](ia.index, parent)}]`;
  }

  function ParameterList(pList) {
    return pList && pList.parameters && pList.parameters.map((p, i) => Grammar.Parameter(p, i));
  }

  function Parameter(p) {
    return `${p.typeName.name} ${p.name ? p.name : ''} `;
  }

  function StructDefinition(s, parent) {
    if (_valuesPool.structs[s.name] == null) {
      s.parentName = parent.name;
      //_valuesPool.structs[s.name] = s;
      _valuesPool.structs[`${s.parentName}.${s.name}`] = s; //TODO: Fazer uma limpa e deixar somente essa referẽncia
    }
    return `${s.name} (${s.members.map((v, i) => Grammar.VariableDeclaration(v, i))})`;
  }

  function Block(body, parent) {
    if (body.type != 'Block') console.warn(`Type block ${body.type} `, body);
    let bodyText = '';
    for (let i = 0; i < body.statements.length; i++) {
      if (i > 0) {
        bodyText += '\n';
      }
      bodyText += Grammar[body.statements[i].type]
        ? Grammar[body.statements[i].type](body.statements[i], body)
        : `<${body.statements[i].type}>`;
    }
    return bodyText;
  }

  function Conditional(n, parent) {
    if (trace) console.log('Conditional', n);
    // TODO:
    const retorno = JSON.stringify(n);
    return retorno;
  }

  function ContinueStatement(n, parent) {
    if (trace) console.log('ContinueStatement', n);
    // TODO:
    const retorno = JSON.stringify(n);
    return retorno;
  }

  function BreakStatement(n, parent) {
    if (trace) console.log('BreakStatement', n);
    // TODO:
    const retorno = JSON.stringify(n);
    return retorno;
  }

  function EnumDefinition(n, parent) {
    if (trace) console.log('EnumDefinition', n);
    // TODO:
    const retorno = JSON.stringify(n);
    return retorno;
  }

  function HexLiteral(n, parent) {
    if (trace) console.log('HexLiteral', n);
    // TODO:
    const retorno = JSON.stringify(n);
    return retorno;
  }

  function InlineAssemblyStatement(n, parent) {
    if (trace) console.log('InlineAssemblyStatement', n);
    // TODO: n.body.operations ( o tipo do operations é AssemblyAssignment)
    const retorno = JSON.stringify(n);
    return retorno;
  }

  function IfStatement(ifstat, parent) {
    if (trace) console.log('IfStatement', ifstat);
    if (trace && ifstat.condition.left) console.log('IfStatement.left', ifstat.condition.left.type);
    let retorno = ifstat.condition.left
      ? `\nif(${Grammar[ifstat.condition.type](ifstat.condition, ifstat)} ) {
\n`
      : `if (${Grammar[ifstat.condition.type](ifstat.condition, ifstat)}) {
\n`;
    if (Grammar[ifstat.trueBody.type] == null) {
      console.error(`Grammar.${ifstat.trueBody.type} não existe`);
    }
    retorno += Grammar[ifstat.trueBody.type]
      ? Grammar[ifstat.trueBody.type](ifstat.trueBody, ifstat)
      : `<${ifstat.trueBody.type}>`;
    retorno += `\n
} `;
    if (ifstat.falseBody != null) {
      if (trace) console.log('IfStatement.falseBody', ifstat.falseBody);
      retorno += `\nelse {
    \n`;
      if (Grammar[ifstat.falseBody.type] == null) {
        console.error(`Grammar.${ifstat.falseBody.type} não existe`);
      }
      retorno += Grammar[ifstat.falseBody.type](ifstat.falseBody, ifstat);
      retorno += `\n
    } `;
    }

    return retorno;
  }

  function WhileStatement(whstat) {
    if (trace) console.log('WhileStatement', whstat);
    let retorno = whstat.condition.left
      ? `\nwhile(${Grammar[whstat.condition.left.type](whstat.condition.left, whstat)} ${
          whstat.condition.operator
        } ${Grammar[whstat.condition.right.type](whstat.condition.right, whstat)}) {
\n`
      : `while (${whstat.condition.name}) {
\n`;
    retorno += Grammar[whstat.body.type](whstat.body, whstat);
    retorno += `\n
} `;
    return retorno;
  }

  function ForStatement(forStat) {
    if (trace) console.log('ForStatement', forStat);
    let retorno = `\nfor(`;
    if (forStat.initExpression != null) {
      retorno += `${Grammar[forStat.initExpression.type](forStat.initExpression, forStat)}`;
    }
    retorno += `; ${
      forStat.conditionExpression ? Grammar[forStat.conditionExpression.type](forStat.conditionExpression, forStat) : ''
    }; ${Grammar[forStat.loopExpression.type](forStat.loopExpression, forStat)}) {
\n`;
    retorno += Grammar[forStat.body.type](forStat.body, forStat);
    retorno += `\n
} `;
    return retorno;
  }

  function Identifier(id, parent) {
    let returnCode = id.name;
    if (parent && parent.type == 'ExpressionStatement') {
      // /} || parentType == 'ReturnStatement') {
      returnCode += ';';
    }
    return returnCode;
  }

  function MemberAccess(m) {
    return `${Grammar[m.expression.type](m.expression, m)}.${m.memberName}`;
  }

  function UnaryOperation(uo, parent) {
    let returnCode = `${uo.operator}${Grammar[uo.subExpression.type](uo.subExpression, uo)}`;
    if (parent.type != 'IfStatement' && parent.type != 'BinaryOperation') {
      returnCode += ';';
    }
    return returnCode;
  }

  function BooleanLiteral(bl, parent) {
    return bl.value;
  }

  /**
   * Literal addresses are interpreted as NumberLiteral
   * So if it has this carachteriscs, we infer it is a Address
   *
   * @param {string} value The value to evaluate
   */
  function isAddress(value) {
    return value.length == 42 && value.startsWith('0x');
  }

  function NumberLiteral(l, parent) {
    let literalType = l.type;
    // Literal addresses are interpreted as NumberLiteral
    // So if it has this carachteriscs, we infer it is a Address
    /*if (isAddress(l.number)) {
      literalType = 'AddressLiteral';
    }*/
    if (!_valuesPool[literalType]) {
      _valuesPool[literalType] = [];
    }
    const value =
      literalType == 'NumberLiteral' && l.number.indexOf('e') != -1
        ? parseInt(l.number).toLocaleString('en-US', { minimumFractionDigits: 0, useGrouping: false })
        : l.number; //tratando de números expressos com notação científica
    //não guardaremos mais como números os literals que começarem com '0x'
    if (literalType != 'NumberLiteral' || !value.startsWith('0x')) {
      if (_valuesPool[literalType].indexOf(value) === -1) {
        _valuesPool[literalType].push(value);
      }
    }
    return l.number;
  }

  function StringLiteral(l, parent) {
    // Se for uma mensagem de erro, não adiciona
    if (
      parent.type !== 'FunctionCall' ||
      (parent.expression.type !== 'Identifier' &&
        ['require', 'assert', 'revert'].indexOf(parent.expression.name) === -1)
    ) {
      if (!_valuesPool[l.type]) {
        _valuesPool[l.type] = [];
      }
      if (_valuesPool[l.type].indexOf(l.value) === -1) {
        _valuesPool[l.type].push(l.value);
      }
    }
    return `"${l.value}"`;
  }

  function ElementaryTypeName(etne) {
    return etne.name;
  }

  function ElementaryTypeNameExpression(etne) {
    return etne.typeName.name;
  }

  function ExpressionStatement(expressionSt, parent) {
    let returnCode = '';
    if (trace) console.log('ExpressionStatement', expressionSt);
    returnCode += expressionSt.expression
      ? Grammar[expressionSt.expression.type](expressionSt.expression, expressionSt)
      : '';
    return returnCode;
  }

  function FunctionCall(fc, parent) {
    if (trace) console.log('FunctionCall', fc);
    if (Grammar[fc.expression.type] == null || typeof Grammar[fc.expression.type] !== 'function') {
      console.error('Type not supported', fc);
    }

    let returnCode = `${Grammar[fc.expression.type](fc.expression, fc)} (${fc.arguments.map(
      (a) => `${Grammar[a.type](a, fc)}`
    )})`;
    if (
      parent.type != 'IfStatement' &&
      parent.type != 'BinaryOperation' &&
      parent.type != 'UnaryOperation' &&
      parent.type != 'FunctionCall'
    ) {
      returnCode += ';';
    }
    return returnCode;
  }

  function BinaryOperation(bo, parent) {
    if (trace) console.log('BinaryOperation', bo);

    let returnCode = `${Grammar[bo.left.type](bo.left, bo)} ${bo.operator} ${Grammar[bo.right.type](bo.right, bo)}`;
    if (
      parent.type != 'FunctionCall' &&
      parent.type != 'IfStatement' &&
      parent.type != 'VariableDeclarationStatement' &&
      parent.type != 'BinaryOperation'
    ) {
      returnCode += ';';
    }
    return returnCode;
  }

  function EmitStatement(es, parent) {
    if (trace) console.log('EmitStatement', es);
    let returnCode = `emit ${Grammar[es.eventCall.expression.type](
      es.eventCall.expression,
      es
    )} (${es.eventCall.arguments.map((a) => `${Grammar[a.type](a, es)}`)})`;
    if (
      parent.type != 'FunctionCall' &&
      parent.type != 'IfStatement' &&
      parent.type != 'VariableDeclarationStatement'
    ) {
      returnCode += ';';
    }
    return returnCode;
  }

  function ReturnStatement(rs, parent) {
    if (trace) console.log('ReturnStatement', rs);
    let returnCode = `return ${rs.expression ? Grammar[rs.expression.type](rs.expression, rs) : ''}`;
    if (parent.type != 'FunctionCall' && parent.type != 'VariableDeclarationStatement') {
      returnCode += ';';
    }
    return returnCode;
  }

  function ThrowStatement(rs, parent) {
    if (trace) console.log('ThrowStatement', rs);
    return `throw`;
  }

  function ImportDirective(id) {
    if (trace) console.log('ImportDirective', id);
    const returnCode = `import '${id.path}';\n`;

    return returnCode;
  }
  function UsingForDeclaration(ud) {
    if (trace) console.log('using ', ud);
    const returnCode = ud.name
      ? `using ${ud.name} ${ud.typeName ? Grammar[ud.typeName.type](ud.typeName) : ''};\n`
      : '';

    return returnCode;
  }

  function NewExpression(n) {
    if (trace) console.log('new ', n);
    const returnCode = 'new ';

    return returnCode;
  }
};
