export * from './RestrictionsFactory';
export * from './invertRestriction';
export * from './switchRestrictions';
export * from './normalizeRestriction';
export * from './getEnabledElementType';
export * from './extractRealRestrictions';
