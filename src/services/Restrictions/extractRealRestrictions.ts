import { IRestriction } from '../../models/IResctriction';
import { RestrictionElementType } from '../../models/RestrictionElementType';
import { transactionParameterAttributes } from '../transactionParameterAttributers';
import { TypeReferences } from '../../models/scenario';
import { ElementaryTypeName } from 'solidity-parser-antlr';
import { IContractGraph } from '../../models/graph';
import { getReferenceKey } from '../scenarios';
import { mark } from '../Utils';
/**
 * Substitute the right side restrictions from a RestrictionElementType.functionParameter to Literal already set in {references}
 * @param nRestrictions Normalized restrictions
 * @param {object} references Values already calculated for the all parameters of the function being analysed, state variables of contract,
 * transaction parameter
 */
export function extractRealRestrictions(
  contract: IContractGraph,
  nRestrictions: IRestriction[],
  references: TypeReferences
): IRestriction[] {
  const realRestrictions: IRestriction[] = [];
  //Para as restrições que do lado direito fazem referência a um outro parâmetro da função, verifica se esse outro parâmetro
  //já tem um valor setado
  for (let nr of nRestrictions) {
    //Se o lado direito for um parâmetro ou variável de estado
    if (
      [
        RestrictionElementType.functionParameter,
        RestrictionElementType.stateVariable,
        RestrictionElementType.otherContractMember,
      ].includes(nr.right.elementType)
    ) {
      let key = getReferenceKey(contract, nr.right);

      //se o valor já foi setado, inclui literal no array de restrições que será passado para a função {random}
      if (references[key] && references[key].stringfieldValue.js != null) {
        const ref = references[key];
        //se o operador for "<" e o valor do right for ZERO, a restrição só será adicionada
        //caso o tipo do nr.right não seja UINT, do contrário, será impossível atender durante
        //a geração dos valores
        if (
          nr.operator != '<' ||
          ref.stringfieldValue.obj > 0 ||
          nr.left.value.typeName == null ||
          (<ElementaryTypeName>nr.left.value.typeName).name == null ||
          !(<ElementaryTypeName>nr.left.value.typeName).name.startsWith('uint')
        ) {
          realRestrictions.push({
            operator: nr.operator,
            from: nr.from,
            right: {
              value: {
                isCode: ref.stringfieldValue.isCode,
                js: ref.stringfieldValue.js,
                sol: ref.stringfieldValue.sol,
                obj: ref.stringfieldValue.obj,
              },
              elementType: RestrictionElementType.Literal,
            },
            left: nr.left,
          });
        }
      } //se o valor não foi setado: se o seu tipo é uint, ele não pode ser negativo, se for um sinal de maior que ele, tem que colocar zero
      else {
        if (nr.operator == '>' && (<ElementaryTypeName>nr.right.value.typeName).name.startsWith('uint')) {
          realRestrictions.push({
            operator: nr.operator,
            from: nr.from,
            right: {
              value: {
                isCode: false,
                js: '0',
                sol: '0',
                obj: 0,
              },
              elementType: RestrictionElementType.Literal,
            },
            left: nr.left,
          });
        }
      }
    }
    //Se o lado direito for um membro de parâmetro ou de variável de estado
    else if (
      [RestrictionElementType.functionParameterMember, RestrictionElementType.stateVariableMember].includes(
        nr.right.elementType
      )
    ) {
      let key = getReferenceKey(contract, nr.right);
      //se o valor já foi setado, inclui literal no array de restrições que será passado para a função {random}
      if (references[key] && references[key].stringfieldValue.js) {
        //se o operador for "<" e o valor do right for ZERO, a restrição só será adicionada
        //caso o tipo do nr.right não seja UINT, do contrário, será impossível atender durante
        //a geração dos valores
        if (
          nr.operator != '<' ||
          references[key].stringfieldValue.obj[nr.right.value.memberName] > 0 ||
          nr.left.value.typeName == null ||
          (<ElementaryTypeName>nr.left.value.typeName).name == null ||
          !(<ElementaryTypeName>nr.left.value.typeName).name.startsWith('uint')
        ) {
          realRestrictions.push({
            operator: nr.operator,
            from: nr.from,
            right: {
              value: {
                isCode: false,
                js: references[key].stringfieldValue.obj[nr.right.value.memberName].toString(),
                sol: references[key].stringfieldValue.obj[nr.right.value.memberName].toString(),
                obj: references[key].stringfieldValue.obj[nr.right.value.memberName],
              },
              elementType: RestrictionElementType.Literal,
            },
            left: nr.left,
          });
        }
      }
    }
    //Se o lado direito for um transactionParameter
    else if (nr.right.elementType == RestrictionElementType.transactionParameter) {
      //se o valor já foi setado, inclui literal no array de restrições que será passado para a função {random}
      if (references['transactionParameter'] && references['transactionParameter'].stringfieldValue.obj) {
        realRestrictions.push({
          operator: nr.operator,
          from: nr.from,
          right: {
            value: {
              isCode: false,
              js:
                references['transactionParameter'].stringfieldValue.obj[
                  transactionParameterAttributes[nr.right.value.js].jsName
                ],
              sol:
                references['transactionParameter'].stringfieldValue.obj[
                  transactionParameterAttributes[nr.right.value.js].jsName
                ],
              obj:
                references['transactionParameter'].stringfieldValue.obj[
                  transactionParameterAttributes[nr.right.value.js].jsName
                ],
            },
            elementType: RestrictionElementType.Literal,
          },
          left: nr.left,
        });
      }
    }
    //se for qualquer coisa distinta de variável de estado
    //FIXME: condição precisa ser ajustada para nova realidade (os ifs acima já foram, falta também o 'references' vir com variáveis de estado já setadas)
    else if (
      [
        RestrictionElementType.stateVariable,
        RestrictionElementType.stateVariableMember,
        RestrictionElementType.stateVariableIndex,
      ].includes(nr.right.elementType) == false
    ) {
      realRestrictions.push(nr);
    }
    //Se for variável de estado, que ainda não estamos tratando pontualmente, vamos colocar um valor de acordo com o sinal de comparação
    else {
      //FIXME: Isso aqui está assumindo sempre que é inteiro, tem que tratar melhor
      let v = '0';
      //se minha esquerda tem que ser menor que um determinado valor que eu não tenho como inferir, é melhor estipular um valor baixo para que
      //o valor do parâmetro gerado tenha mais possibilidades de, na hora da execução, ser mais baixo que tal variável
      //1. Se o operador é '<', mudo pra um pois colocando como zero não tem como colocar um valor menor
      //2. A mesma coisa se já existir uma restrição dizendo que o mesmo parâmetro não pode ser igual a zero
      if (
        nr.operator == '<' ||
        realRestrictions.find(
          (x) => x.left.value.js == nr.left.value.js && x.operator == '!=' && x.right.value.obj === 0
        )
      ) {
        v = '1';
      }
      //se minha esquerda tem que ser maior que um determinado valor que eu não tenho como inferir, é melhor estipular um valor alto para que
      //o valor do parâmetro gerado tenha mais possibilidades de, na hora da execução, ser mais alto que tal variável
      else if (nr.operator == '>') {
        v = '9999999999999999';
      }
      realRestrictions.push({
        operator: nr.operator,
        from: nr.from,
        right: {
          value: {
            isCode: false,
            js: v,
            sol: v,
            obj: parseInt(v),
          },
          elementType: RestrictionElementType.Literal,
        },
        left: nr.left,
      });
      mark('Tem alguém caindo aqui:', contract.name, nr.left.elementType, nr.right.elementType);
    }
  }
  return realRestrictions;
}
