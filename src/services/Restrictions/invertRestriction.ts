import { IRestriction } from '../../models/IResctriction';

export function invertRestriction(restriction: IRestriction): IRestriction {
  const retorno: IRestriction = JSON.parse(JSON.stringify(restriction));
  delete retorno.operator;
  switch (restriction.operator) {
    case '==': {
      retorno.operator = '!=';
      break;
    }
    case '!=': {
      retorno.operator = '==';
      break;
    }
    case '<=': {
      retorno.operator = '>';
      break;
    }
    case '<': {
      retorno.operator = '>=';
      break;
    }
    case '>=': {
      retorno.operator = '<';
      break;
    }
    case '>': {
      retorno.operator = '<=';
      break;
    }
  }
  return retorno;
}
