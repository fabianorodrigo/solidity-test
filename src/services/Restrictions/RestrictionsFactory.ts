import {
  isBO,
  isRequireCall,
  isAssertCall,
  isFunctionCall,
  getBinaryOperationRestriction,
  isTransactionParameterMemberAccess,
  isParameterMemberAccess,
  isIf,
  isIfWithRevert,
  isFunction,
  getUnaryOperationRestriction,
  getContractConstructor,
  getRestrictionValue,
  getNumberLiteralRestrictionValue,
} from '../solidityParser';
const SolidityParserIceFactory = require('../SolidityParserFactory');
import { invertRestriction } from './invertRestriction';
import { FunctionDefinition, ContractDefinition, IfStatement } from 'solidity-parser-antlr';
import { IBranch } from '../../models/IBranch';
import { IRestriction, IRestrictionValue } from '../../models/IResctriction';
import { BranchType } from '../../models/BranchType';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { RestrictionElementType } from '../../models/RestrictionElementType';
import {
  isUO,
  isBOorUOGetter,
  getBOorUOGetter,
  isSuperFunctionCall,
  isStateVariableReference,
  isIdentifier,
  isLiteral,
  isNumberLiteral,
  isAddressZeroFunctionCall,
  getDivisionDenominator,
} from '../solidityParser/statements';
import { getFunctionParameterEnabledElementTypes, getEnabledElementTypes } from './getEnabledElementType';
import { TypeReferences } from '../../models/scenario';
import { getFunctionGraph } from '../TruffleProjectGraph';

/**
 * Analyse the statements in the function's AST and return the restrictions to the parameter {paramName} in the
 * function {f}.
 *
 * @param {object} truffleProject Instance of Truffle Project under analysis
 * @param {FunctionDefinition} functionStack Function to be analysed
 * @param {Array} inputParameterNames Parameter names that will be considered in the identification of branches of interest
 * If NULL, it will be analysed the 'transaction parameter' (eg. msg.sender, msg.value, etc...)
 * @param {contractDefinition} contractDefinition Contract Definition in the AST that {functionDefinition} belongs to
 * @param {object} branch Branch that gave origin to the scenario being analysed with all it's data including restrictions that have to be considered, besides the restrictions based on requires found, to achieve a desired target
 * @returns {Array} List of restrictions found
 */
export function getFunctionParameterRestriction(
  truffleProject: TruffleProjectIceFactoryFunction,
  functionStack: FunctionDefinition[],
  contractDefinition: ContractDefinition,
  branch: IBranch,
  references: TypeReferences
): IRestriction[] {
  if (!Array.isArray(functionStack)) throw new Error(`functionStack is not an array`);
  const restrictions = getBodyStatementsParameterRestriction(
    truffleProject,
    functionStack[functionStack.length - 1].body,
    contractDefinition,
    functionStack,
    branch,
    references
  );
  if (restrictions.length < 2) return restrictions;
  return removeDuplicateRestrictions(restrictions);
}

/**
 * Removes duplicity of restrictions. Sometimes the same require assertion is done in the
 * target function and inside some function called by it
 *
 * @param restrictions List of restrictions
 */
function removeDuplicateRestrictions(restrictions: IRestriction[]) {
  //tem que converter pra string para que o Set entenda que é o mesmo valor, se for objeto,
  //ele vai considerar a referência e, logo, distintos
  return Array.from(new Set(restrictions.map((r) => JSON.stringify(r)))).map((r) => JSON.parse(r));
}

/**
 * Analyse the statements in {body} and return the restrictions to the parameter {paramName} in the
 * function {f}.
 *
 * @param {object} truffleProject Instance of Truffle Project under analysis
 * @param {object} body Body of statements to be analysed
 * @param {ContractDefinition} contractDefinition Contract being analysed
 * @param {Array} parents Stack of functions or modifiers being called in sequence
 * @param {Array} inputParameterNames Name of the parameter or the member name of Transaction Parameter to be analysed
 * If NULL, it will be analysed the 'transaction parameter' (eg. msg.sender, msg.value, etc...)
 * @param {object} parent The element owner of the body statement
 * @param {object} branch Branch that gave origin to the scenario being analysed with all it's data including restrictions that have to be considered, besides the restrictions based on requires found, to achieve a desired target
 * @returns {Array} List of restrictions found
 */
function getBodyStatementsParameterRestriction(
  truffleProject: TruffleProjectIceFactoryFunction,
  body,
  contractDefinition: ContractDefinition,
  parents: FunctionDefinition[],
  branch: IBranch,
  references: TypeReferences
): IRestriction[] {
  if (!Array.isArray(parents)) throw new Error(`parents is not an array`);
  let retorno: IRestriction[] = [];
  const parent = parents[parents.length - 1];
  //if parent is a function, analyse it's modifiers
  if (isFunction(parent)) {
    parent.modifiers.forEach((modInv) => {
      //Se o uso do modificador recebe parâmetros, só serão válidos os que forem literals ou identifiers
      if (
        modInv.arguments &&
        modInv.arguments.length > 0 &&
        modInv.arguments.some((arg) => !isIdentifier(arg) && !isLiteral(arg))
      )
        return;
      let modifier;
      //A invocação de um modificador (ModifierInvocation) pode ser realizada para um construtor ancestral
      if (truffleProject.getGraph().contracts[contractDefinition.name].inheritsFrom[modInv.name]) {
        const contracts = truffleProject.getContracts();
        modifier = getContractConstructor(
          contracts.find((c) => c.contractDefinition.name == modInv.name).contractDefinition
        );
      } else {
        modifier = truffleProject.getModifier(contractDefinition, modInv.name) as any; //ModifierDefinition não tem body (como assim?!??!)
      }
      if (modifier) {
        const modifierRestrictions = getBodyStatementsParameterRestriction(
          truffleProject,
          modifier.body,
          contractDefinition,
          [modifier],
          branch,
          references
        );
        mapRestrictionsToOriginFunction(modifierRestrictions, modInv, modifier, parent, retorno, references);
      } else {
        console.debug(`Modifier not found: ${modInv.name}`);
      }
    });
  }
  for (let i = 0; i < body.statements.length; i++) {
    let st = body.statements[i];
    //Se houver uma divisão, o denominador precisa ser maior que zero
    const denominador = getDivisionDenominator(st);
    if (denominador != null) {
      const left = getRestrictionValue(
        truffleProject,
        getFunctionGraph(truffleProject.getGraph(), contractDefinition, parent),
        denominador
      );
      if (left != null) {
        retorno.push({
          operator: '>',
          left: left,
          right: getNumberLiteralRestrictionValue(0),
          from: BranchType.notDefined,
        });
      }
    }
    //Se for chamada ao 'require' com uma operação binária (comparação ou combinação de comparações)
    if (
      isRequireCall(st) &&
      (isBO(st.expression.arguments[0]) ||
        isUO(st.expression.arguments[0]) ||
        isStateVariableReference(
          truffleProject.getGraph().contracts[contractDefinition.name],
          st.expression.arguments[0]
        ) ||
        isBOorUOGetter(truffleProject, contractDefinition, st.expression.arguments[0]))
    ) {
      let expression = st.expression.arguments[0];
      //Se não é BinaryOperation nem Unary, parte pra operação mais cara: getBOorUOGetter
      if (
        !isBO(expression) &&
        !isUO(expression) &&
        !isStateVariableReference(
          truffleProject.getGraph().contracts[contractDefinition.name],
          st.expression.arguments[0]
        )
      ) {
        expression = getBOorUOGetter(truffleProject, contractDefinition, expression);
      }
      let restrictions: IRestriction[];
      if (isBO(expression)) {
        restrictions = getBinaryOperationRestriction(truffleProject, contractDefinition, expression, parent);
      } else {
        restrictions = getUnaryOperationRestriction(truffleProject, contractDefinition, expression, parent);
      }
      //stamping that the restriction is from a require
      restrictions.forEach((r) => (r.from = BranchType.require));
      retorno = retorno.concat(restrictions);
    }
    //Se for um IF contendo em seu corpo de funções um 'revert'
    else if (isIfWithRevert(st) && (isBO(st.condition) || isUO(st.condition))) {
      let restrictions: IRestriction[];
      if (isBO(st.condition)) {
        restrictions = getBinaryOperationRestriction(truffleProject, contractDefinition, st.condition, parent);
      } else {
        restrictions = getUnaryOperationRestriction(truffleProject, contractDefinition, st.condition, parent);
      }
      //Se é um with com Revert, inverte-se a restrição pois o que se quer é exatamente não entrar no IF
      restrictions = restrictions.map((r) => invertRestriction(r));
      restrictions.forEach((r) => (r.from = BranchType.ifWithRevert));
      retorno = retorno.concat(restrictions);
    }
    //Se for chamada a função que não seja require
    else if (!isRequireCall(st) && (isFunctionCall(st) || isSuperFunctionCall(st))) {
      const isSuper = isSuperFunctionCall(st);
      const { contract, funcDef: functionCalled } = truffleProject.getFunctionFromContractDefinition(
        contractDefinition,
        isSuper ? st.expression.expression.memberName : st.expression.expression.name,
        st.expression.arguments.length, //TODO: ainda não é suficiente para cobrir overload, apenas alguns casos
        isSuper
      );
      //Se achar esta outra função no mesmo contrato ou pai
      //E desde que não se trate de recursividade
      if (functionCalled != null && !parents.some((f) => Object.is(f, functionCalled))) {
        parents.push(functionCalled);
        const functionCalledRestrictions = getFunctionParameterRestriction(
          truffleProject,
          parents,
          contract,
          branch,
          references
        );
        parents.pop();
        mapRestrictionsToOriginFunction(
          functionCalledRestrictions,
          st.expression,
          functionCalled,
          parent,
          retorno,
          references
        );
      }
    } else if (isIf(st)) {
      let body = getBranchIfBody(branch, st);
      if (body != null && body.statements != null) {
        //se for um IF exatamente do branch que está se analisando, então aprofunda-se nele
        retorno = retorno.concat(
          getBodyStatementsParameterRestriction(
            truffleProject,
            body,
            contractDefinition,
            parents, //st, estava dando erro passando o próprio statement pois pra pegar o valor o binaryOperation.js faz referência a propriedade 'parameters'
            branch,
            references
          )
        );
      }
    } else if (
      st.type != 'VariableDeclarationStatement' &&
      st.type != 'ReturnStatement' &&
      st.expression &&
      st.expression.operator != '='
    ) {
    }
  }
  return retorno;
}

function mapRestrictionsToOriginFunction(
  functionCalledRestrictions: IRestriction[],
  calling: any,
  functionCalled: FunctionDefinition,
  originFunction: FunctionDefinition,
  retorno: IRestriction[],
  references: TypeReferences
) {
  functionCalledRestrictions.forEach((fcr) => {
    const restrictionClone: IRestriction = JSON.parse(JSON.stringify(fcr));
    let mapeado = false;
    if (getEnabledElementTypes().includes(restrictionClone.left.elementType)) {
      if (!getFunctionParameterEnabledElementTypes().includes(restrictionClone.left.elementType)) {
        mapeado = true;
      } else if (
        mapSideRestrictionsToOriginFunction(calling, functionCalled, restrictionClone.left, originFunction, references)
      ) {
        mapeado = true;
      }
    }
    if (mapeado && getEnabledElementTypes().includes(restrictionClone.right.elementType)) {
      if (!getFunctionParameterEnabledElementTypes().includes(restrictionClone.right.elementType)) {
        mapeado = true;
      } else if (
        mapSideRestrictionsToOriginFunction(calling, functionCalled, restrictionClone.right, originFunction, references)
      ) {
        mapeado = true;
      } else {
        mapeado = false;
      }
    }
    if (mapeado) {
      retorno.push(restrictionClone);
    }
  });
}

/**
 * Recebe um restrictionValue (left ou right) e modifica a o value.js e value.sol para o nome
 * do parâmetro da função chamadora e retorna TRUE. Se não foi um parâmetro da função chamadora ou mesmo um
 * parâmetro de transação, não modifica nada e retorna FALSE
 * @param calling
 * @param functionCalled
 * @param side
 * @param originFunction
 */
function mapSideRestrictionsToOriginFunction(
  calling: any,
  functionCalled: FunctionDefinition,
  side: IRestrictionValue,
  originFunction: FunctionDefinition,
  references: TypeReferences
): boolean {
  const param = calling.arguments[functionCalled.parameters.findIndex((p) => p.name == side.value.js)];
  if (isTransactionParameterMemberAccess(param)) {
    side.value.js = side.value.sol = param.memberName;
    side.elementType = RestrictionElementType.transactionParameter;
  } else if (
    isParameterMemberAccess(
      param,
      originFunction.parameters.map((p) => p.name)
    )
  ) {
    //TODO: Tratar casos em que pode na função chamada avaliar o block.number, por exemplo
    side.value.js = side.value.sol = param.expression.name;
    side.value.memberName = param.memberName;
    side.elementType = RestrictionElementType.functionParameterMember;
  } else if (param && param.name && originFunction.parameters.some((p) => p.name == param.name)) {
    side.value.js = side.value.sol = param.name;
  } else if (references[param.name] != null) {
    side.value.js = side.value.sol = param.name; //JSON.parse(JSON.stringify(references[param.name].stringfieldValue));
    side.elementType = RestrictionElementType.stateVariable;
  }
  //setado pelo construtor default (beforeEach)
  /*else if (global['defaultReferences'] && global['defaultReferences'][param.name]) {
    side.value.js = side.value.sol = param.name; //JSON.parse(JSON.stringify(global['defaultReferences'][param.name].stringfieldValue));
    side.elementType = RestrictionElementType.stateVariable;
  }*/
  else if (isLiteral(param)) {
    side.value.js = side.value.sol = `${isNumberLiteral(param) ? param.number : param.value}`;
    side.value.obj = isNumberLiteral(param) ? parseInt(param.number) : param.value;
    side.elementType = RestrictionElementType.Literal;
  } else if (isAddressZeroFunctionCall(param)) {
    //TODO: analisar projeto swaps  em que gera restrição controversa: _quoteAddress == 0x0 && _quoteAdress != 0x0
    side.value.js = side.value.sol = side.value.obj = '0x0000000000000000000000000000000000000000';
    side.elementType = RestrictionElementType.Literal;
    console.debug('oi, aquele caso do swaps');
    //return false;
  } else {
    return false;
  }
  return true;
}

function getBranchIfBody(branch: IBranch, st: IfStatement) {
  if (branch.loc == null) return;
  if (isIt(branch, st)) {
    return st.trueBody;
  } else if (st.falseBody && isIf(st.falseBody)) {
    return getBranchIfBody(branch, <IfStatement>st.falseBody);
  }
}

function isIt(branch: IBranch, st: IfStatement): boolean {
  return (
    branch.loc.start.line == st.loc.start.line &&
    branch.loc.start.column == st.loc.start.column &&
    branch.loc.end.line == st.loc.end.line &&
    branch.loc.end.column == st.loc.end.column
  );
}

/**
 * Analyse if the value is comply with the restrictions.
 *
 * @param {Any} value Value to be analysed
 * @param {object[]} restrictions An array of restrictions {value, operator}
 * @param {boolean} numericalValue Indicate if the parameter {value} is considered a number (only used for operators >,<, >= and <=).
 *  IF true, calls a 'parseInt' in value and restriction[i].value. Default: FALSE
 * @param {boolean} bytes Indicate if the parameter {value} is a representation of array of bytes (necessary because sometimes it comes between brackets others not )
 * @returns {boolean} TRUE if the value is compatible with restrictions, FALSE otherwise
 */
export function fillsRestrictions(value, restrictions: IRestriction[], numericalValue = false, bytes = false): boolean {
  if (!restrictions) return true;
  //TODO RESTRICTIONELEMENTTYPE: Restringir filtro
  const restrictionsFiltered = restrictions.filter(
    (r) =>
      r.right.elementType == RestrictionElementType.Literal || r.right.elementType == RestrictionElementType.EnumMember
  );
  if (restrictionsFiltered.some((r) => r.operator == '==' && r.right.value.obj == value)) return true;
  let fills = restrictionsFiltered.length == 0;
  for (let i = 0; i < restrictionsFiltered.length; i++) {
    if (restrictionsFiltered[i].operator == '==') {
      if (Array.isArray(value)) {
        if (restrictionsFiltered[i].left.value.memberName == 'length') {
          fills = value.length == restrictionsFiltered[i].right.value.obj;
        } else {
          fills =
            value.join(',') ==
            (Array.isArray(restrictionsFiltered[i].right.value.obj)
              ? restrictionsFiltered[i].right.value.obj.join(',')
              : restrictionsFiltered[i].right.value.obj);
        }
      } else {
        fills = value == getValue(restrictionsFiltered[i], numericalValue);
      }
      if (!fills) break;
    } else if (restrictionsFiltered[i].operator == '!=') {
      if (Array.isArray(value)) {
        if (restrictionsFiltered[i].left.value.memberName == 'length') {
          fills = value.length != restrictionsFiltered[i].right.value.obj;
        } else {
          fills =
            value.join(',') !=
            (Array.isArray(restrictionsFiltered[i].right.value.obj)
              ? restrictionsFiltered[i].right.value.obj.join(',')
              : restrictionsFiltered[i].right.value.obj);
        }
      } else {
        fills = value != getValue(restrictionsFiltered[i], numericalValue);
      }
      if (!fills) break;
    } else if (restrictionsFiltered[i].operator == '>') {
      if (Array.isArray(value)) {
        if (restrictionsFiltered[i].left.value.memberName == 'length') {
          fills = value.length > restrictionsFiltered[i].right.value.obj;
        } else {
          fills =
            value.join(',') >
            (Array.isArray(restrictionsFiltered[i].right.value.obj)
              ? restrictionsFiltered[i].right.value.obj.join(',')
              : restrictionsFiltered[i].right.value.obj);
        }
      } else if (!numericalValue) {
        fills = value > getValue(restrictionsFiltered[i], numericalValue);
      } else {
        if (typeof value != 'number' && typeof value != 'bigint' && value.startsWith('0x'))
          throw new Error(`numerical que não é do tipo 'number'; ${value}`);
        const v = typeof value == 'number' || typeof value == 'bigint' ? value : parseInt(value);
        fills = v > getValue(restrictionsFiltered[i], numericalValue);
      }
      if (!fills) break;
    } else if (restrictionsFiltered[i].operator == '<') {
      if (Array.isArray(value)) {
        if (restrictionsFiltered[i].left.value.memberName == 'length') {
          fills = value.length < restrictionsFiltered[i].right.value.obj;
        } else {
          fills =
            value.join(',') <
            (Array.isArray(restrictionsFiltered[i].right.value.obj)
              ? restrictionsFiltered[i].right.value.obj.join(',')
              : restrictionsFiltered[i].right.value.obj);
        }
      } else if (!numericalValue) {
        fills = value < getValue(restrictionsFiltered[i], numericalValue);
      } else {
        if (typeof value != 'number' && typeof value != 'bigint' && value.startsWith('0x'))
          throw new Error(`numerical que não é do tipo 'number'; ${value}`);
        const v = typeof value == 'number' || typeof value == 'bigint' ? value : parseInt(value);
        fills = v < getValue(restrictionsFiltered[i], numericalValue);
      }
      if (!fills) break;
    } else if (restrictionsFiltered[i].operator == '>=') {
      if (Array.isArray(value)) {
        if (restrictionsFiltered[i].left.value.memberName == 'length') {
          fills = value.length >= restrictionsFiltered[i].right.value.obj;
        } else {
          fills =
            value.join(',') >=
            (Array.isArray(restrictionsFiltered[i].right.value.obj)
              ? restrictionsFiltered[i].right.value.obj.join(',')
              : restrictionsFiltered[i].right.value.obj);
        }
      } else if (!numericalValue) {
        fills = value >= getValue(restrictionsFiltered[i], numericalValue);
      } else {
        if (typeof value != 'number' && typeof value != 'bigint' && value.startsWith('0x'))
          throw new Error(`numerical que não é do tipo 'number'; ${value}`);
        const v = typeof value == 'number' || typeof value == 'bigint' ? value : parseInt(value);
        fills = v >= getValue(restrictionsFiltered[i], numericalValue);
      }
      if (!fills) break;
    } else if (restrictionsFiltered[i].operator == '<=') {
      if (Array.isArray(value)) {
        if (restrictionsFiltered[i].left.value.memberName == 'length') {
          fills = value.length <= restrictionsFiltered[i].right.value.obj;
        } else {
          fills =
            value.join(',') <=
            (Array.isArray(restrictionsFiltered[i].right.value.obj)
              ? restrictionsFiltered[i].right.value.obj.join(',')
              : restrictionsFiltered[i].right.value.obj);
        }
      } else if (!numericalValue) {
        fills = value <= getValue(restrictionsFiltered[i], numericalValue);
      } else {
        if (typeof value != 'number' && typeof value != 'bigint' && value.startsWith('0x'))
          throw new Error(`numerical que não é do tipo 'number'; ${value}`);
        const v = typeof value == 'number' || typeof value == 'bigint' ? value : parseInt(value);
        fills = v <= getValue(restrictionsFiltered[i], numericalValue);
      }
      if (!fills) break;
    }
  }
  return fills;
}

function getValue(restriction: IRestriction, numericalValue: boolean): number | string {
  const isTypeOfNumber =
    typeof restriction.right.value.obj == 'number' || typeof restriction.right.value.obj == 'bigint';
  if (!numericalValue) {
    if (isTypeOfNumber) return restriction.right.value.obj;
    return restriction.right.value.js.concat(
      restriction.right.value.memberName ? '.'.concat(restriction.right.value.memberName) : ''
    );
  } else {
    if (!isTypeOfNumber)
      throw new Error(`restrição numerical que não é do tipo 'number'; ${restriction.right.value.obj}`);
    return restriction.right.value.obj;
  }
}

/**
 * Remove brackets ('[' and ']') from string receive
 * @param value value to me cleaned
 */
function remBrackets(value: string): string {
  return value.replace(/\[/g, '').replace(/\]/g, '');
}
