import { IRestriction } from '../../models/IResctriction';

export function switchRestriction(restriction: IRestriction): IRestriction {
  const retorno: IRestriction = JSON.parse(JSON.stringify(restriction));

  if (!['==', '!='].includes(restriction.operator)) {
    delete retorno.operator;
    switch (restriction.operator) {
      case '<=': {
        retorno.operator = '>=';
        break;
      }
      case '<': {
        retorno.operator = '>';
        break;
      }
      case '>=': {
        retorno.operator = '<=';
        break;
      }
      case '>': {
        retorno.operator = '<';
        break;
      }
    }
  }
  return retorno;
}
