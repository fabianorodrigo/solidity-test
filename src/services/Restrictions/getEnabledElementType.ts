import { RestrictionElementType } from '../../models/RestrictionElementType';

/**
 * Retorna um array de RestrictrionElementType que a solução já é capaz de
 * avaliar e/ou gerar valores para satisfazerem as condições encontradas em
 * requires, ifs (binary e unaryOperations).
 * Atualmente consegue identificar mas não consegue gerar/setar valores para
 * variáveis de instância, por exemplo
 */
export function getEnabledElementTypes(): RestrictionElementType[] {
  return [RestrictionElementType.Literal, RestrictionElementType.EnumMember].concat(getNotLiteralEnabledElementTypes());
}

/**
 * Retorna um array de RestrictrionElementType que a solução já é capaz de
 * avaliar e/ou gerar valores para satisfazerem as condições encontradas em
 * requires, ifs (binary e unaryOperations).
 * Com exceção de Literal e EnumMember
 */
export function getNotLiteralEnabledElementTypes(): RestrictionElementType[] {
  return [RestrictionElementType.BlockMember, RestrictionElementType.transactionParameter].concat(
    getFunctionParameterEnabledElementTypes(),
    getStateEnabledElementTypes()
  );
}

export function getStateEnabledElementTypes(): RestrictionElementType[] {
  return [RestrictionElementType.stateVariable];
}

/**
 * Retorna um array de RestrictrionElementType relacionados a parâmetros de entrada
 * como, por exemplo, functionParameter, transactionParameter ou functionParameterMember
 * que a solução já é capaz de avaliar e/ou gerar valores para satisfazerem as condições encontradas em
 * requires, ifs (binary e unaryOperations).
 */
export function getFunctionParameterEnabledElementTypes(): RestrictionElementType[] {
  return [RestrictionElementType.functionParameter, RestrictionElementType.functionParameterMember];
}
