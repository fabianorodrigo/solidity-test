import { IRestriction } from '../../models/IResctriction';
import { invertRestriction } from './invertRestriction';
import { RestrictionElementType } from '../../models/RestrictionElementType';
import { switchRestriction } from './switchRestrictions';

/**
 * Retorna as restrições recebidas de uma forma normalizada em função do parâmetro de referência com
 * o único intuito de facilitar as comparações durante a geração de valores. Na prática, a parâmetro ou
 * variável de instância com o nome {name} ficará do lado esquerdo (RestrictionValue.left)
 */
export function getNormalizedRestrictions(restrictions: IRestriction[], name: string) {
  return restrictions.map((r) => {
    const rClone: IRestriction = JSON.parse(JSON.stringify(r));
    if (r.right.value.js == name) {
      const temp = r.left;
      rClone.left = rClone.right;
      rClone.right = temp;
      rClone.operator = switchRestriction(r).operator;
    }
    return rClone;
  });
}

/**
 * Retorna as restrições recebidas de uma forma normalizada em função do de uso de literais com
 * o único intuito de facilitar as comparações durante a geração de valores. Na prática, tudo
 * que for do tipo RestrictionElementType.Literal fica do lado direito (RestrictionValue.right)
 * se não tiver literal envolvido, o que for funcionParameter fica na esquerda
 */
export function getNormalizedLiteralRestrictions(restrictions: IRestriction[]) {
  return restrictions.map((r) => {
    const rClone: IRestriction = JSON.parse(JSON.stringify(r));
    if (
      r.left.elementType == RestrictionElementType.Literal ||
      ((r.right.elementType == RestrictionElementType.functionParameter ||
        r.right.elementType == RestrictionElementType.functionParameterMember) &&
        r.left.elementType != RestrictionElementType.functionParameter &&
        r.left.elementType != RestrictionElementType.functionParameterMember &&
        r.left.elementType != RestrictionElementType.stateVariable &&
        r.left.elementType != RestrictionElementType.stateVariableMember)
    ) {
      const temp = r.left;
      rClone.left = rClone.right;
      rClone.right = temp;
      rClone.operator = switchRestriction(r).operator;
    }
    return rClone;
  });
}

/**
 * Retorna as restrições recebidas de uma forma normalizada em função do parâmetro de transação com
 * o único intuito de facilitar as comparações durante a geração de valores. Na prática, o transaction
 * parameter ficará do lado esquerdo (RestrictionValue.left)
 */
export function getNormalizedTransactionRestrictions(restrictions: IRestriction[]) {
  return restrictions.map((r) => {
    const rClone: IRestriction = JSON.parse(JSON.stringify(r));
    if (r.right.elementType == RestrictionElementType.transactionParameter) {
      const temp = r.left;
      rClone.left = rClone.right;
      rClone.right = temp;
      rClone.operator = switchRestriction(r).operator;
    }
    return rClone;
  });
}
