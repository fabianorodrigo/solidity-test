import { IContract } from '../models/IContract';

import fs from 'fs';
import path from 'path';
import colors from 'colors';
import shelljs from 'shelljs';
import fsextra from 'fs-extra';
import { isAbstractContract, sortContractsByConstructor, getContractConstructor, getFunctions } from './solidityParser';

import { IFinalBeforeEach } from '../models/IBeforeEach';

import { ITestFile } from '../models/ITestFile';
import {
  appendContract as appendContractGraph,
  appendFunctionGraph,
  extractTruffleProjectGraph,
  getContractPredecessors,
} from './TruffleProjectGraph';
import { TruffleProjectIceFactoryFunction } from './TruffleFactory';
import {
  generateProxyContract,
  getProxyTestFunctionName,
  isExperimentalABIEncoderV2,
  isFunctionProxyable,
} from './testBuilder/solidity';
import { generateJavascriptProxyTestFunctions } from './testBuilder/javascript';
import { getJSTestHeader } from './testBuilder/javascript/getJSTestHeader';
import { generateMigrationBeforeEach } from './testBuilder/javascript/beforeEach';
import { generateJavascriptTestFunctions } from './testBuilder/javascript/testFunctions';

import * as util from 'util'; // has no default export
import { inspect } from 'util'; // or directly
import { IContractGraph } from '../models/graph';
import { debug, mark, info, trace, bigIntSerializer, getParametersValues } from './Utils';
import { getInheritedFunctionsFromAbstract } from './testBuilder/javascript/getInheritedFunctionsFromAbstract';
import {
  getSuccessfulScenarioAndParameterRestrictions,
  initReferencesGlobal,
  updateReferencesFromFunctionCalling,
} from './scenarios';
import { IBranch } from '../models/IBranch';
import { IFunctionCalling } from '../models';
import { FunctionDefinition } from 'solidity-parser-antlr';

const stringToBytes32 = `function stringToBytes32(string memory source) public returns (bytes32 result) {
    bytes memory tempEmptyStringTest = bytes(source);
    if (tempEmptyStringTest.length == 0) {
        return 0x0;
    }

    assembly {
        result := mload(add(source, 32))
    }
}`;

type TestIceFactoryFunction = {
  generateProjectTests: () => Promise<void>;
  generateTestFile: (
    truffleProject: TruffleProjectIceFactoryFunction,
    contract: IContract,
    beforeEachCodeJS: IFinalBeforeEach,
    beforeEachCodeSOL: string,
    experimentalABIEncoderV2: boolean
  ) => Promise<ITestFile[]>;
};
/**
 * Retornará uma instância imutável de um objeto com funcionalidades para geração de testes
 *
 * @param {string} truffleProject Instance of Truffle Project under test
 * @returns {object} Objeto com utilidades para geração de testes
 */
export function TestIceFactory({ truffleProject }): TestIceFactoryFunction {
  const workdir = path.resolve(__dirname, process.env.WORKDIR || '../workdir');
  return Object.freeze({
    generateProjectTests,
    generateTestFile,
  });

  /**
   * Generate all the tests for the truffle project passed in the factory function
   */
  async function generateProjectTests(): Promise<void> {
    const prjBaseName: string = path.basename(truffleProject.truffleProjectHome);
    mark(truffleProject.truffleProjectHome);

    //Se o diretório do projeto existir dentro do workdir
    if (fs.existsSync(path.resolve(workdir, prjBaseName))) {
      fsextra.emptyDirSync(path.resolve(workdir, prjBaseName));
    }
    shelljs.cp('-r', path.join(process.env.DEFAULT_SOLIDITY_DIRECTORY, prjBaseName), workdir);
    //limpando diretório de testes no projeto dentro do workdir (apenas arquivos JS)
    //deleteRecursiveJSFiles(path.join(truffleProject.truffleProjectHome, 'test'));
    fsextra.emptyDirSync(path.join(truffleProject.truffleProjectHome, 'test'));

    // copy truffle project to a temporary directory
    truffleProject.copyTruffleProjectToTemp();
    //clean data result
    global['generalResults'][prjBaseName].dhInitCompile = null;
    global['generalResults'][prjBaseName].dhFinishCompile = null;
    global['generalResults'][prjBaseName].quantityContracts = null;
    global['generalResults'][prjBaseName].quantityLibraries = null;
    global['generalResults'][prjBaseName].dhInitBeforeEachGeneration = null;
    global['generalResults'][prjBaseName].dhFinishBeforeEachGeneration = null;
    global['generalResults'][prjBaseName].dhInitTestFilesGeneration = null;
    global['generalResults'][prjBaseName].dhFinishTestFilesGeneration = null;
    global['generalResults'][prjBaseName].qtyTestFiles = 0;
    global['generalResults'][prjBaseName].qtyTestedFunctions = 0;
    global['generalResults'][prjBaseName].qtyContractTestedFunctions = 0;
    global['generalResults'][prjBaseName].qtyLibraryTestedFunctions = 0;
    global['generalResults'][prjBaseName].qtyTestedScenarios = 0;
    global['generalResults'][prjBaseName].scenariosSummary = {
      contract: { total: 0, contracts: {} },
      library: { total: 0, contracts: {} },
    };
    global['generalResults'][prjBaseName].successfull = null;
    // compile the project
    global['generalResults'][prjBaseName].dhInitCompile = new Date();
    truffleProject.compile();
    global['generalResults'][prjBaseName].dhFinishCompile = new Date();

    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
    //escrever o grafo em arquivo
    fs.writeFileSync(
      path.join(truffleProject.truffleProjectHome, 'graph.json'),
      inspect(truffleProject.getGraph(), { showHidden: false, depth: 5 })
    );
    //Verifica se o projeto possui alguam característica que precisa usar o pragma experimental ABIEncoderV2 nos
    //arquivos '.sol' gerados por nós
    const experimentalABIEncoderV2 = isExperimentalABIEncoderV2(truffleProject);

    // get together all contracts and literals in those contracts
    const allContracts: IContractGraph[] = Object.values(truffleProject.getGraph().contracts);
    // Como não instancia contrato abstrato, focamos nos demais contratos
    const contracts: IContract[] = allContracts.filter(
      (c) => !c.isThirdPartyLib && !c.isOnlyForTest && !isAbstractContract(c.contractDefinition)
    );
    //ordena colocando os que têm menos dependências antes
    contracts.sort(sortContractsByConstructor);

    global['generalResults'][prjBaseName].quantityContracts = contracts.filter(
      (c) => c.contractDefinition.kind == 'contract'
    ).length;
    global['generalResults'][prjBaseName].quantityLibraries = contracts.filter(
      (c) => c.contractDefinition.kind == 'library'
    ).length;

    trace(`Creating beforeEach JS code ...`);
    // gera o código javascript da função 'beforeEach', que é executada antes de cada função de teste,
    global['generalResults'][prjBaseName].dhInitBeforeEachGeneration = new Date();
    let beforeEachCodeJS: IFinalBeforeEach = null;
    global['defaultReferences'] = {};
    if (
      process.env.USE_BEFORE_EACH_CACHE.toUpperCase() == 'S' &&
      fs.existsSync(path.join(truffleProject.truffleProjectHome, 'beforeEach.json'))
    ) {
      beforeEachCodeJS = JSON.parse(
        fs.readFileSync(path.join(truffleProject.truffleProjectHome, 'beforeEach.json')).toString()
      );
      global['defaultReferences'] = JSON.parse(
        fs.readFileSync(path.join(truffleProject.truffleProjectHome, 'defaultReferences.json')).toString()
      );
    } else {
      beforeEachCodeJS = await generateMigrationBeforeEach(
        truffleProject,
        getJSTestHeader,
        allContracts
          .filter((c) => truffleProject.getGraph().contracts[c.contractDefinition.name].instanceNeeded == true)
          .sort(sortContractsByConstructor),
        truffleProject.executeTruffleTest
      );
      fs.writeFileSync(
        path.join(truffleProject.truffleProjectHome, 'beforeEach.json'),
        JSON.stringify(beforeEachCodeJS)
      );
      fs.writeFileSync(
        path.join(truffleProject.truffleProjectHome, 'defaultReferences.json'),
        JSON.stringify(global['defaultReferences'], bigIntSerializer)
      );
    }

    if (
      beforeEachCodeJS == null ||
      (contracts.filter((c) => c.contractDefinition.kind == 'contract').length > 0 &&
        beforeEachCodeJS.contractsDeployed.length == 0)
    ) {
      console.error(colors.bgRed(colors.white(`FALHA AO GERAR BEFORE EACH JS: ${truffleProject.truffleProjectHome}`)));
      //return;
    }
    let beforeEachCodeSOL = `uint nonce = ${Math.ceil(Math.random() * 100)};\n${stringToBytes32}\n`;
    global['generalResults'][prjBaseName].dhFinishBeforeEachGeneration = new Date();

    let qtyTestFiles = 0;
    global['generalResults'][prjBaseName].dhInitTestFilesGeneration = new Date();

    for (let index = 0; index < contracts.length; index++) {
      info(`Creating ${contracts[index].contractDefinition.name} contract tests ...`);
      const testFiles = await generateTestFile(
        truffleProject,
        contracts[index],
        beforeEachCodeJS,
        beforeEachCodeSOL,
        experimentalABIEncoderV2
      );

      if (testFiles != null) {
        testFiles.forEach((tf) => {
          if (tf != null && tf.testFilePath != '' && tf.testFileContent != '') {
            try {
              fs.writeFileSync(tf.testFilePath, tf.testFileContent);
              info(`Arquivo de teste gerado com sucesso ${index + 1}/${contracts.length}`, tf.testFilePath);
              qtyTestFiles++;
            } catch (err) {
              console.log(colors.red('Falha ao escrever arquivo de teste'), tf.testFilePath, err);
            }
          }
        });
      }
    }
    truffleProject.cleanTruffleProjectTemp();
    global['generalResults'][prjBaseName].dhFinishTestFilesGeneration = new Date();
    global['generalResults'][prjBaseName].qtyTestFiles = qtyTestFiles;

    //member types
    let contentMemberTypes = '';
    if (global['memberTypes'] != null) {
      global['memberTypes'].forEach((arrayInterno) => {
        arrayInterno.forEach((e, i) => {
          if (i > 0) contentMemberTypes += ';';
          contentMemberTypes += e;
        });
        contentMemberTypes += '\n';
      });
      fs.writeFileSync(path.join(workdir, 'memberTypes.csv'), contentMemberTypes);
    }
    //quantidade branches
    let contentBranchesByFunction = '';
    if (global['branchesByFunction'] != null) {
      global['branchesByFunction'].forEach((arrayInterno) => {
        arrayInterno.forEach((e, i) => {
          if (i > 0) contentBranchesByFunction += ';';
          contentBranchesByFunction += e;
        });
        contentBranchesByFunction += '\n';
      });
      fs.writeFileSync(path.join(workdir, 'quantityBranches.csv'), contentBranchesByFunction);
    }
  }

  /** *
   * Generate the JS or SOL test file for the contract in the {index} position of {contracts}
   *
   * @param {object} truffleProject The instance of the Truffle Project to generate tests
   * @param {object} contract Contract for which the test will be generated
   * @param {object} beforeEachCodeJS Object with javascript code of function 'beforeEach' (executed before each test function be executed) and the list of contracts deployed in it
   * @param {string} beforeEachCodeSOL The solidity code of function 'beforeEach' (executed before each test function be executed)
   * @param {object} JSTestBuilder Object responsible to return the source code of test functions in Javascript
   * @param {function} generateSolidityTestFunctions Function responsible to return the source code of test functions in Solidity (usually for libraries)
   *
   * @returns {string} The content (javascript source code) of test file
   */
  async function generateTestFile(
    truffleProject: TruffleProjectIceFactoryFunction,
    contract: IContract,
    beforeEachCodeJS: IFinalBeforeEach,
    beforeEachCodeSOL: string,
    experimentalABIEncoderV2: boolean
  ): Promise<ITestFile[]> {
    const retorno: ITestFile[] = [];

    const constructor = getContractConstructor(contract.contractDefinition);
    const contractFunctions = truffleProject.getFunctions(contract);

    //For PUBLIC or EXTERNAL functions of contracts, the tests will be generated directly
    //for libraries, or INTERNAL functions of contracts, the testes will execute over a proxy
    if (
      contract.contractDefinition.kind == 'contract' &&
      !truffleProject.getGraph().contracts[contract.contractDefinition.name].isAbstract &&
      contractFunctions.filter((f) => f.visibility == 'public' || f.visibility == 'external').length > 0 &&
      (constructor == null || ['public', 'default'].includes(constructor.visibility))
    ) {
      const returnFile: ITestFile = { testFilePath: '', testFileContent: '' };
      returnFile.testFilePath = path.join(
        truffleProject.truffleProjectHome,
        'test',
        `${contract.contractDefinition.name}Test.js`
      );

      returnFile.testFileContent += getJSTestHeader(
        Object.values(truffleProject.getGraph().contracts).filter((cg) => cg.instanceNeeded == true),
        contract.contractDefinition.name
      );

      returnFile.testFileContent += beforeEachCodeJS.code;

      returnFile.testFileContent += await generateJavascriptTestFunctions(
        truffleProject,
        contract,
        returnFile.testFileContent // envia o conteúdo parcial do arquivo para que a função que gera os testes possa fazer avaliações antes de dar um resultado final
      );
      returnFile.testFileContent += '});\n';
      retorno.push(returnFile);
    }

    //For PUBLIC or EXTERNAL functions of contracts, the tests will be generated directly
    //for libraries, or INTERNAL functions of contracts, the testes will execute over a proxy
    if (
      contract.contractDefinition.kind == 'library' ||
      (process.env.GENERATE_PROXY_FOR_INTERNAL_FUNCTIONS == 'S' &&
        contractFunctions.filter((f) => f.visibility == 'internal').length >
          0) /*||
      getInheritedFunctionsFromAbstract(truffleProject, contract.contractDefinition).filter(
        (fd) => fd.visibility == 'internal'
      ).length > 0*/
    ) {
      debug('process.env.GENERATE_PROXY_FOR_INTERNAL_FUNCTIONS:', process.env.GENERATE_PROXY_FOR_INTERNAL_FUNCTIONS);
      const proxyContract: IContract = generateProxyContract(truffleProject, contract, experimentalABIEncoderV2);
      //apendando o contrato proxy ao grafo
      const contractProxyGraph = appendContractGraph(truffleProject, truffleProject.getGraph(), proxyContract);
      //copiando os modificadores e stateVariables do original
      contractProxyGraph.modifiers =
        contractProxyGraph.projectGraph.contracts[contract.contractDefinition.name].modifiers;
      contractProxyGraph.stateVariables =
        contractProxyGraph.projectGraph.contracts[contract.contractDefinition.name].stateVariables;

      //adicionando ao truffleProject, que ainda é muito usado
      truffleProject.addContract({
        contractDefinition: contractProxyGraph.contractDefinition,
        solFilePath: contractProxyGraph.solFilePath,
        isOnlyForTest: true,
        isThirdPartyLib: false,
      });
      contractProxyGraph.inheritsFrom = getContractPredecessors(truffleProject, contractProxyGraph);
      //apendando ao grafo as funçṍes proxyadas (os statements da original são mantidos e muda-se o nome)
      truffleProject
        .getFunctions(contract)
        .filter((f) => isFunctionProxyable(contract.contractDefinition, f))
        .forEach((fd) => {
          const proxyedFunction: FunctionDefinition = JSON.parse(JSON.stringify(fd));
          proxyedFunction.name = getProxyTestFunctionName(truffleProject, contract, fd);
          appendFunctionGraph(truffleProject, contractProxyGraph, proxyedFunction);
        });

      //inicializando a global references do contrato
      initReferencesGlobal(contractProxyGraph);

      const returnFileProxyTest: ITestFile = { testFilePath: '', testFileContent: '' };
      returnFileProxyTest.testFilePath = path.join(
        truffleProject.truffleProjectHome,
        'test',
        `${contract.contractDefinition.name}ProxyTest.js`
      );

      returnFileProxyTest.testFileContent += getJSTestHeader(
        Object.values(truffleProject.getGraph().contracts).filter((cg) => cg.instanceNeeded == true),
        `contract${contractProxyGraph.name}`
      );

      let instanceProxyContract = '';
      //Se biblioteca, tem que linkar
      if (contract.contractDefinition.kind == 'library') {
        instanceProxyContract += `    Proxy${contract.contractDefinition.name}.link('${contract.contractDefinition.name}', contract${contract.contractDefinition.name}.address);\n`;
      } else {
        //link com bibliotecas que o proxyado tem dependência (se houver)
        Object.values(truffleProject.getGraph().contracts[contract.contractDefinition.name].dependencies)
          .filter((d) => d.kind == 'library' && d.instanceNeeded == true)
          .forEach((d) => {
            instanceProxyContract += `    Proxy${contract.contractDefinition.name}.link("${d.name}",contract${d.name}.address);\n`;
          });
      }
      instanceProxyContract += `    contract${contractProxyGraph.name} = await Proxy${contract.contractDefinition.name}.new(`;
      // busca os parâmetros gerados no beforeEach do contrato original (se houver)
      const beforeEachEcontract = beforeEachCodeJS.contractsDeployed.find(
        (c) => c.contractName == contract.contractDefinition.name
      );

      const proxyConstructor = getContractConstructor(proxyContract.contractDefinition);
      if (proxyConstructor != null) {
        //apenda construtor no grafo
        appendFunctionGraph(truffleProject, contractProxyGraph, proxyConstructor);
        //esse IF (o else, na verdade) trecho é uma tentativa de puxar o swaps para o patamar anterior
        //talvez a melhor solução seja criar os proxies lá antes de gerar o beforeEach
        //PS: O "menos UM" é porque no beforeEachEcontract.stringfieldParams já vem com o transactionParameter
        if (
          beforeEachEcontract == null ||
          beforeEachEcontract.stringfieldParams.length - 1 != proxyConstructor.parameters.length
        ) {
          const { scenario } = getSuccessfulScenarioAndParameterRestrictions(
            truffleProject,
            proxyConstructor,
            proxyContract.contractDefinition,
            getParametersValues,
            { restrictions: [] } as IBranch, //não vamos tratar de branch restrictions neste momento
            {}
          );
          scenario.parametersValues.forEach((p, i) => {
            if (i > 0) instanceProxyContract += ',';
            instanceProxyContract += p.stringfieldValue.js;
          });
          //atualizando os valores de referência
          const fc: IFunctionCalling = {
            contractInstanceTarget: contractProxyGraph.name,
            functionCalled: contractProxyGraph.functions['constructor'][0],
            parametersValues: scenario.parametersValues,
            stateVariablesAffected: Object.values(
              contractProxyGraph.functions['constructor'][0].stateVariablesWritten
            ).map((sv) => {
              return { name: sv.name };
            }),
          };
          updateReferencesFromFunctionCalling(fc, global['defaultReferences'][contractProxyGraph.name]);
        } else {
          instanceProxyContract += beforeEachEcontract.stringfieldParams;
        }
        //proxyConstructor.parameters.forEach((p) => {});
      } else {
        instanceProxyContract += '{ from: accounts[0] }';
      }

      //FIXME: até aqui
      instanceProxyContract += `);\n`;

      returnFileProxyTest.testFileContent += '    let contract'
        .concat(contractProxyGraph.name, ' = null;\n')
        .concat(
          beforeEachCodeJS.code
            .slice(0, beforeEachCodeJS.code.lastIndexOf('}'))
            .concat(instanceProxyContract, beforeEachCodeJS.code.slice(beforeEachCodeJS.code.lastIndexOf('}')))
        );

      //depois tem que rolar uma refatoração geral pra passar o contractProxyGraph
      //const contractProxy = JSON.parse(JSON.stringify(contract));
      //contractProxy.contractDefinition.name = contractProxy.name;
      returnFileProxyTest.testFileContent += await generateJavascriptProxyTestFunctions(
        truffleProject,
        contractProxyGraph,
        returnFileProxyTest.testFileContent // envia o conteúdo parcial do arquivo para que a função que gera os testes possa fazer avaliações antes de dar um resultado final
      );
      returnFileProxyTest.testFileContent += '});\n';
      retorno.push(returnFileProxyTest);
    }
    //return returnFile;
    return retorno;
  }
}
