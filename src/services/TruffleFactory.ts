import fs from 'fs';
import path from 'path';

import fsextra from 'fs-extra';

import readlineSync from 'readline-sync';
import shelljs from 'shelljs';

const SolidityParserIceFactory = require('./SolidityParserFactory');

import {
  getFunctions as solidityGetFunctions,
  getModifiers as solidityGetModifiers,
  getStructs as solidityGetStructs,
  getEnums as solidityGetEnums,
  getInheritedFunctions,
  getStateVariables as solidityGetStateVariables,
} from './solidityParser';

import { IContract } from '../models/IContract';
import { FunctionDefinition, VariableDeclaration, ModifierDefinition, ContractDefinition } from 'solidity-parser-antlr';
import { IStructsPool } from '../models/IStructsPool';
import { IValuesPool } from '../models/IValuesPool';
import { IEnumPool } from '../models/IEnumPool';
import { ITruffleProjectGraph } from '../models/graph';
import { debug, deleteRecursiveJSFiles, trace } from './Utils';

export type ValidationCodeFunction = (
  testCode: string,
  fileExtension: string
) => { stdout: string; stderr: string; code: number };

export type TruffleProjectIceFactoryFunction = {
  addContract: (contract: IContract) => void;
  getContracts: (force?: boolean) => IContract[];
  getValuesPool: (force?: boolean) => IValuesPool;
  getContractsFiles: () => string[];
  getThirdPartyContractsFiles: () => string[];
  copyTruffleProjectToTemp: () => void;
  compile: () => { stdout: string; stderr: string; code: number };
  getTruffleVersion: () => string;
  truffleDependencySupportsOverload: () => boolean;
  getSolidityCoverageVersion: () => string;
  executeTruffleTest: ValidationCodeFunction;
  cleanTruffleProjectTemp: () => void;
  getFunctionsFromContract: (index: number) => FunctionDefinition[];
  getFunctions: (contract: IContract) => FunctionDefinition[];
  getFunctionFromContractDefinition: (
    contractDefinition: ContractDefinition,
    functionName: string,
    parametersCount: number,
    onlyAncestrals?: boolean
  ) => { contract: ContractDefinition; funcDef: FunctionDefinition };
  getModifiersFromContract: (index: number) => ModifierDefinition[];
  getModifier: (contractDefinition: ContractDefinition, modifierName: string) => ModifierDefinition;
  getModifiers: (contract: IContract) => ModifierDefinition[];
  getStateVariablesFromContract: (index: number) => VariableDeclaration[];
  getStateVariables: (contract: IContract) => VariableDeclaration[];
  getDependenciesFromContract: (index: number) => IContract[];
  getDependencies: (contract: IContract) => IContract[];
  getEnums: () => IEnumPool;
  getEnumsByName: (name: string) => string[];
  getStructs: () => IStructsPool;
  getUndeployableContracts: () => string[];
  registerUndeployableContract: (contractName: string) => void;
  getDeployedJSContracts: () => string[];
  registerDeployedJSContract: (contractName: string) => void;
  truffleProjectHome: string;
  setGraph: (graph: ITruffleProjectGraph) => void;
  getGraph: () => ITruffleProjectGraph;
  isGraphSet: () => boolean;
};

/**
 * Retornará uma instância imutável de um objeto com funcionalidades do framework Truffle
 *
 * @returns {object} Objeto com utilidades para interacao com Truffle
 */
export function TruffleProjectIceFactory({ truffleProjectHome }): TruffleProjectIceFactoryFunction {
  //Store the contracts definitions of the Truffle Project
  let contracts: IContract[] = [];
  //store contracts not instanceables
  let undeployableContracts: string[] = [];
  //store contracts deployed by JS beforeEach function
  let deployedJSContracts: string[] = [];
  //Store the function definitions of each contract of the Truffle Project
  let functions: FunctionDefinition[][] = [];
  //Store the modifiers definitions of each contract of the Truffle Project
  let modifiers = [];
  //Store the state variables of each contract of the Truffle Project
  let stateVariables = [];
  //Store the libraries imported of each contract of the Truffle Project
  let libraries = [];
  // store the structs definitions of all contracts in the Truffle Project
  let structs: IStructsPool = {};
  //store the enums definitions of all contracts in the Truffle Project
  let enums: IEnumPool = {};
  //Store the values pool literals found in the project's contracts
  let valuesPool: IValuesPool = {};
  //Grafo do projeto truffle
  let grapho: ITruffleProjectGraph;

  return Object.freeze({
    addContract,
    getContracts,
    getFunctions,
    getFunctionsFromContract,
    getFunctionFromContractDefinition,
    getModifier,
    getModifiers,
    getModifiersFromContract,
    getStateVariables,
    getStateVariablesFromContract,
    getDependenciesFromContract: getDependenciesFromContract,
    getDependencies: getDependencies,
    getValuesPool,
    getEnums,
    getEnumsByName,
    getStructs,
    truffleProjectHome,
    copyTruffleProjectToTemp,
    compile,
    executeTruffleTest,
    getContractsFiles,
    getThirdPartyContractsFiles,
    getTruffleVersion,
    truffleDependencySupportsOverload,
    getSolidityCoverageVersion,
    cleanTruffleProjectTemp,
    registerUndeployableContract,
    getUndeployableContracts,
    getDeployedJSContracts,
    registerDeployedJSContract,
    isGraphSet,
    getGraph,
    setGraph,
  });

  function addContract(contract: IContract) {
    contracts.push(contract);
    modifiers.push([]);
    functions.push([]);
  }

  /**
   * Get all the contract definition of all contracts in the project
   *
   * @param {boolean} force Se TRUE, mesmo que as variáveis do projeto contracts e valuesPool tenham conteúdo, irá fazer a varredura.
   * Do contrário, retorna os valores armazenados
   *
   */
  function getContracts(force = false): IContract[] {
    getContractsAndLiteralsPool(force);
    return contracts;
  }
  /**
   * Get all the literals, structs and similar data of all contracts in the project
   *
   * @param {boolean} force Se TRUE, mesmo que as variáveis do projeto contracts e valuesPool tenham conteúdo, irá fazer a varredura.
   * Do contrário, retorna os valores armazenados
   *
   */
  function getValuesPool(force = false): IValuesPool {
    getContractsAndLiteralsPool(force);
    return valuesPool;
  }

  /**
   * Varre todos os arquivos Solidity do projeto truffle e retorna um objeto com os contratos (incluindo library)
   * identificados e também um pool de valores literais
   *
   * @param {boolean} force Se TRUE, mesmo que as variáveis do projeto contracts e valuesPool tenham conteúdo, irá fazer a varredura.
   * Do contrário, retorna os valores armazenados
   *
   * @returns {object} Objeto contendo uma propriedade 'contracts' com um Array de objeto {solFilePath, contractDefinition}
   * e uma propriedade {valuesPool} que é um objeto contendo uma propriedade com o nome de cada tipo de literal (eg. NumeberLiteral)
   * e cujos valores são arrays com esses literais encontrados para cada tipo
   */
  function getContractsAndLiteralsPool(
    force = false
  ): {
    contracts: IContract[];
    valuesPool: IValuesPool;
  } {
    if (contracts.length == 0 || valuesPool == null || force) {
      // get Truffle Project's .sol files inside /contracts directory and create SolidityParser for each
      let SolidityParsers = getContractsFiles('contracts').map((solFilePath) => {
        return {
          isThirdPartyLib: false,
          isOnlyForTest: false,
          parser: SolidityParserIceFactory({
            solFilePath,
          }),
        };
      });
      // get Truffle Project's .sol files inside /test directory and create SolidityParser for each
      SolidityParsers = SolidityParsers.concat(
        getContractsFiles('test').map((solFilePath) => {
          return {
            isThirdPartyLib: false,
            isOnlyForTest: true,
            parser: SolidityParserIceFactory({
              solFilePath,
            }),
          };
        })
      );
      // get Truffle Project's .sol files inside /node_modules directory and create SolidityParser for each
      SolidityParsers = SolidityParsers.concat(
        getThirdPartyContractsFiles().map((solFilePath) => {
          return {
            isThirdPartyLib: true,
            isOnlyForTest: false,
            parser: SolidityParserIceFactory({
              solFilePath,
            }),
          };
        })
      );

      // get together all contracts and literals in those contracts
      let contractsLocal: IContract[] = [];
      let valuesPoolLocal = {};
      SolidityParsers.forEach((parserObj) => {
        const parser = parserObj.parser;
        const fileContracts = parser.getContracts().map((icontract) => {
          icontract.isThirdPartyLib = parserObj.isThirdPartyLib;
          icontract.isOnlyForTest = parserObj.isOnlyForTest;
          return icontract;
        });
        fileContracts.forEach((fc) => {
          //Só inclui na lista de contratos se já não existir um de mesmo nome na lista
          const i = contractsLocal.findIndex((cl) => cl.contractDefinition.name == fc.contractDefinition.name);
          if (i == -1) {
            contractsLocal.push(fc);
          }
        });
        valuesPoolLocal = parser.appendValuesPool(valuesPoolLocal);
      });

      //funcoes dos próprios contratos
      contractsLocal.forEach((contract) => {
        const contractFunctions = solidityGetFunctions(contract.contractDefinition, true);
        functions.push(contractFunctions);
        const contractModifiers = solidityGetModifiers(contract.contractDefinition);
        modifiers.push(contractModifiers);
        const contractStateVariablesDeclarations = solidityGetStateVariables(contract.contractDefinition);
        let localVariables: VariableDeclaration[] = [];
        contractStateVariablesDeclarations.forEach((declaration) => {
          localVariables = localVariables.concat(declaration.variables);
        });
        stateVariables.push(localVariables);

        const contractSolidityParser = SolidityParsers.find(
          (sp) => sp.parser.getSolidityPath() == contract.solFilePath
        );

        const contractImportDirectives = contractSolidityParser.parser.getImports();
        let localImports = [];
        contractImportDirectives.forEach((imp) => {
          const impPath = path.resolve(
            imp.importDirective.path.startsWith('.')
              ? path.dirname(imp.solFilePath)
              : truffleProjectHome.concat('/node_modules'),
            imp.importDirective.path
          );
          const libraryParser = SolidityParsers.find((sp) => sp.parser.getSolidityPath() == impPath);
          if (libraryParser != null) {
            localImports = localImports.concat(libraryParser.parser.getContracts());
          }
        });
        libraries.push(localImports);

        //pegando structs
        solidityGetStructs(contract.contractDefinition).forEach((s) => {
          structs[s.name] = {
            parentName: contract.contractDefinition.name,
            contractName: contract.contractDefinition.name,
            fullName: false,
            ...s,
          }; //é necessário manter duplicado para facilitar a localização durante a geração dos testes
          structs[contract.contractDefinition.name.concat('.', s.name)] = {
            parentName: contract.contractDefinition.name,
            contractName: contract.contractDefinition.name,
            fullName: true,
            ...s,
          };
        });
        //Pegando os enums
        solidityGetEnums(contract.contractDefinition).forEach((e) => {
          if (!enums[contract.contractDefinition.name]) enums[contract.contractDefinition.name] = {};
          enums[contract.contractDefinition.name][e.name] = {
            members: [],
          };
          enums[contract.contractDefinition.name][e.name].members = e.members;
        });
      });
      //funcoes herdadas
      /*contractsLocal.forEach((contract, i) => {
        if (contract.contractDefinition.baseContracts.length > 0) {
          functions[i] = functions[i].concat(getInheritedFunctions(contract.contractDefinition, false, contractsLocal));
        }
      });*/

      //apendando as contas default do Truffle ao pool de AddressLiteral
      if (!valuesPool.AddressLiteral) {
        valuesPool.AddressLiteral = [];
      }
      for (let i = 0; i < 10; i++) {
        valuesPool.AddressLiteral.push(`accounts[${i}]`);
      }

      contracts = contractsLocal;
      valuesPool = valuesPoolLocal;
    }

    return {
      contracts,
      valuesPool,
    };
  }
  /**
   * List all files with .sol extension inside the {truffleProjectHome}/contracts
   *
   * @param {string} truffleProjectHome The root directory of the truffle project
   */
  function getContractsFiles(subPath: string = 'contracts'): string[] {
    const contractDir = path.join(truffleProjectHome, subPath);
    if (fs.existsSync(contractDir)) {
      // get .sol files in the 'contracts' directory
      return shelljs
        .find(contractDir)
        .filter((file) => file.toLowerCase().endsWith('.sol') && !file.toLowerCase().endsWith('migrations.sol'));
    } else {
      return [];
    }
  }

  /**
   * List all files with .sol extension inside the {truffleProjectHome}/node_modules
   *
   * @param {string} truffleProjectHome The root directory of the truffle project
   */
  function getThirdPartyContractsFiles(): string[] {
    const node_modulesDir = path.join(truffleProjectHome, 'node_modules');
    if (!fs.existsSync(node_modulesDir)) {
      return [];
    }
    // get .sol files in the 'node_modules' directory since they are under a 'contracts' directory
    return shelljs
      .find(node_modulesDir)
      .filter(
        (file) =>
          file.toLowerCase().indexOf('/contracts/') > -1 &&
          file.toLowerCase().endsWith('.sol') &&
          !file.toLowerCase().endsWith('migrations.sol')
      );
  }

  /**
   * Clean 'temp' directory.
   * Copy the directory {truffleProjectHome} to 'temp' directory
   * in order to be used to run preTests
   * And execute 'npm install truffle-assertions'
   */
  function copyTruffleProjectToTemp(): void {
    // cria diretório temporário para executar o código
    const tempdir = path.resolve(__dirname, '../temp');
    if (!fs.existsSync(tempdir)) {
      fs.mkdirSync(tempdir);
    }
    fsextra.emptyDirSync(tempdir);

    // fazer cópia do projeto pro diretório temporário
    shelljs.cp('-r', truffleProjectHome, tempdir);
    // instala pacote truffle-assertions
    // shelljs.exec('npm install truffle-assertions');
  }

  /**
   * Retorna o caminho do diretório temporário onde são feitas
   * execuções das funções de teste para sua validação
   */
  function getTempDir(): string {
    const tempdir = path.resolve(__dirname, '../temp');
    return path.join(tempdir, path.basename(truffleProjectHome), '/');
  }

  /**
   * Executes 'truffle test' in the 'temp' directory of {truffleProjectHome}
   * @returns Compiling results
   */
  function compile(): {
    stdout: string;
    stderr: string;
    code: number;
  } {
    shelljs.cd(getTempDir());
    const { stdout, stderr, code } = shelljs.exec('truffle compile', {
      silent: true,
    });
    if (stdout.indexOf('Could not connect to your Ethereum client with the following parameters') > -1) {
      throw new Error('STOP! GANACHE IS OUT');
    }
    return {
      stdout,
      stderr,
      code,
    };
  }

  function getTruffleVersion(): string {
    const filePathPackagejson = path.join(truffleProjectHome, 'package.json');
    let packageJSON = null;
    if (fs.existsSync(filePathPackagejson)) {
      packageJSON = JSON.parse(fs.readFileSync(filePathPackagejson).toString());
    }
    if (packageJSON && packageJSON.dependencies && packageJSON.dependencies['truffle']) {
      return packageJSON.dependencies['truffle'];
    } else if (packageJSON && packageJSON.devDependencies && packageJSON.devDependencies['truffle']) {
      return packageJSON.devDependencies['truffle'];
    } else {
      return null;
    }
  }

  function truffleDependencySupportsOverload(): boolean {
    const truffleVersion = getTruffleVersion();
    //Se não tem dependência de truffle no package.json, está usando a global, atual e que suporta overload
    if (!truffleVersion) return true;
    return (
      truffleVersion.startsWith('5.') ||
      truffleVersion.startsWith('^5.') ||
      truffleVersion.startsWith('6.') ||
      truffleVersion.startsWith('^6.') ||
      truffleVersion.startsWith('7.') ||
      truffleVersion.startsWith('^7.')
    );
  }

  function getSolidityCoverageVersion(): string {
    if (global['generalResults'] && global['generalResults'][path.basename(truffleProjectHome)]) {
      return global['generalResults'][path.basename(truffleProjectHome)]['solidity-coverage-version'];
    }
    return null;
  }

  /**
   * Clean 'test' directory in the {truffleProjectHome} that is in the 'temp' directory, persist a test file and executes 'truffle test'
   * @param {string} testCode Javascript or Solidity source code of a test file that will be written in a file and the test executed
   * @returns Test results
   */
  function executeTruffleTest(
    testCode: string,
    fileExtension = 'js'
  ): {
    stdout: string;
    stderr: string;
    code: number;
  } {
    const tempTruffleProjectHome = getTempDir();
    // Limpar diretório de testes
    deleteRecursiveJSFiles(path.join(tempTruffleProjectHome, 'test'));

    fs.writeFileSync(path.join(tempTruffleProjectHome, `test/temp.${fileExtension}`), testCode);
    shelljs.cd(tempTruffleProjectHome);
    const { stdout, stderr, code } = shelljs.exec('truffle test', {
      silent: true,
    });
    if (code > 0) {
      trace('truffle test: código', testCode.replace('\\n', '\n'));
      debug('truffle test: stdout', code, stdout);
    }

    if (stdout.indexOf('Could not connect to your Ethereum client with the following parameters') > -1) {
      readlineSync.question('GANACHE IS OUT. Initialize and press any key');
      return executeTruffleTest(testCode, fileExtension);
    } else if (
      stdout.indexOf('Cannot find module') > -1 ||
      stdout.indexOf('invalid reporter "eth-gas-reporter"') > -1
    ) {
      console.error(stdout);
      process.exit();
    }
    return {
      stdout,
      stderr,
      code,
    };
  }

  /**
   * Remove 'temp' directory.
   */
  function cleanTruffleProjectTemp(): void {
    // cria diretório temporário para executar o código
    const tempdir = path.resolve(__dirname, '../temp');
    if (fs.existsSync(tempdir)) {
      fsextra.emptyDirSync(tempdir);
      fs.rmdirSync(tempdir);
    }
  }

  /**
   * Take all functions of the contract in the position {index}.
   *
   * @param {integer} index Index of contract which functions will be returned
   * @returns {Array} All functionDefinitions of the contract in the position {index}
   */
  function getFunctionsFromContract(index: number): FunctionDefinition[] {
    //If 'contracts' is empty, call the function that populates it
    if (contracts.length == 0) {
      getContractsAndLiteralsPool();
    }
    return functions[index];
  }

  /**
   * Take all functions of the contract passed.
   *
   * @param {obejct} contract contract which functions will be returned
   * @returns {Array} All functionDefinitions of the contract in the position {index}
   */
  function getFunctions(contract: IContract): FunctionDefinition[] {
    //If 'contracts' is empty, call the function that populates it
    if (contracts.length == 0) {
      getContractsAndLiteralsPool();
    }
    const index = contracts.findIndex((c) => c.contractDefinition.name == contract.contractDefinition.name);
    return functions[index];
  }

  /** *
   * Searchs for a function with the name {functionName} in the {contractDefinition} or it's ancestrals
   *
   * @param {contractDefinition} contractDefinition A instance of the contractDefinition in the AST from which the functionsDefinition will be retrieved
   * @param {string} functionName Function's name to be taken
   * @param {number} parametersCount Number of parameters that has the searched function
   * @param {boolean} onlyAncestrals If TRUE, does not search in the own contract, only in the ancestrals
   * @returns {functionDefinition} Instance of FunctionDefinition found or NULL, if not found
   *
   */
  function getFunctionFromContractDefinition(
    contractDefinition: ContractDefinition,
    functionName: string,
    parametersCount: number,
    onlyAncestrals: boolean = false
  ): { contract: ContractDefinition; funcDef: FunctionDefinition } {
    const index = contracts.findIndex(
      (c) =>
        c.contractDefinition.name == contractDefinition.name &&
        c.contractDefinition.baseContracts.length == contractDefinition.baseContracts.length
    );
    if (index > -1) {
      if (onlyAncestrals == false) {
        const functionInContract = functions[index].filter(
          (f) => f.name == functionName && f.parameters.length == parametersCount
        );
        if (functionInContract.length > 0) {
          if (functionInContract.length > 1) {
            debug(
              `${contractDefinition.name} has ${functionInContract.length} functions '${functionName}' with ${parametersCount} input parameters`
            );
          }
          return { contract: contractDefinition, funcDef: functionInContract[0] };
        }
      }
      //if not found in the own contract, search in the ancestrals
      for (let i = 0; i < contractDefinition.baseContracts.length; i++) {
        const parent = contracts.find(
          (c) => c.contractDefinition.name == contracts[index].contractDefinition.baseContracts[i].baseName.namePath
        );
        if (parent) {
          const subResult = getFunctionFromContractDefinition(parent.contractDefinition, functionName, parametersCount);
          if (subResult.contract != null && subResult.funcDef != null) {
            return subResult;
          }
        }
      }
    }
    return { contract: null, funcDef: null };
  }

  /**
   * Take all modifiers defined in the contract in the position {index}.
   *
   * @param {integer} index Index of contract which modifiers will be returned
   * @returns {Array} All functionDefinitions of the contract in the position {index}
   */
  function getModifiersFromContract(index: number): ModifierDefinition[] {
    //If 'contracts' is empty, call the function that populates it
    if (contracts.length == 0) {
      getContractsAndLiteralsPool();
    }
    return modifiers[index];
  }

  /**
   * Take all modifiers of the contract passed.
   *
   * @param {obejct} contract contract which modifiers will be returned
   * @returns {Array} All modifierDefinitions of the contract in the position {index}
   */
  function getModifiers(contract: IContract): ModifierDefinition[] {
    //If 'contracts' is empty, call the function that populates it
    if (contracts.length == 0) {
      getContractsAndLiteralsPool();
    }
    const index = contracts.findIndex((c) => c.contractDefinition.name == contract.contractDefinition.name);
    let result = modifiers[index];
    for (let i = 0; i < contracts[index].contractDefinition.baseContracts.length; i++) {
      const parent = contracts.find(
        (c) => c.contractDefinition.name == contracts[index].contractDefinition.baseContracts[i].baseName.namePath
      );
      if (parent) {
        const subResult = getModifiers(parent);
        if (subResult.length > 0) {
          result = result.concat(subResult);
        }
      }
    }
    return result;
  }

  /**
   * Take the modifier of the contract passed that has the same name as {modifierName}.
   *
   * @param {obejct} contract contract which functions will be returned
   * @param {string} modifierName Name of the chased modifier
   * @returns {ModifierDefinition} ModifierDefinition in the {contract} that has the name {modifierName}
   */
  function getModifier(contractDefinition: ContractDefinition, modifierName: string): ModifierDefinition {
    //If 'contracts' is empty, call the function that populates it
    if (contracts.length == 0) {
      getContractsAndLiteralsPool();
    }
    const index = contracts.findIndex((c) => c.contractDefinition.name == contractDefinition.name);
    let result = modifiers[index].find((m: ModifierDefinition) => m.name == modifierName);
    if (!result) {
      for (let i = 0; i < contracts[index].contractDefinition.baseContracts.length; i++) {
        const parent = contracts.find(
          (c) => c.contractDefinition.name == contracts[index].contractDefinition.baseContracts[i].baseName.namePath
        );
        if (parent) {
          const subResult = getModifier(parent.contractDefinition, modifierName);
          if (subResult) {
            return subResult;
          }
        }
      }
    }
    return result;
  }
  /**
   * Take all state variables of the contract in the position {index}.
   *
   * @param {integer} index Index of contract which state variables will be returned
   * @returns {Array} All state variables of the contract in the position {index}
   */
  function getStateVariablesFromContract(index: number): VariableDeclaration[] {
    //If 'contracts' is empty, call the function that populates it
    if (contracts.length == 0) {
      getContractsAndLiteralsPool();
    }
    return stateVariables[index];
  }

  /**
   * Take all libraries, contracts and interfaces imported in the contract passed.
   *
   * @param {obejct} contract contract which state variables will be returned
   * @returns {Array} All State Variables of the contract in the position {index}
   */
  function getDependencies(contract: IContract): IContract[] {
    //If 'contracts' is empty, call the function that populates it
    if (contracts.length == 0) {
      getContractsAndLiteralsPool();
    }
    const index = contracts.findIndex((c) => c.contractDefinition.name == contract.contractDefinition.name);
    return libraries[index];
  }

  /**
   * Take all  libraries, contracts and interface imported in the contract in the position {index}.
   *
   * @param {integer} index Index of contract which state variables will be returned
   * @returns {Array} All state variables of the contract in the position {index}
   */
  function getDependenciesFromContract(index: number): IContract[] {
    //If 'contracts' is empty, call the function that populates it
    if (contracts.length == 0) {
      getContractsAndLiteralsPool();
    }
    return libraries[index];
  }

  /**
   * Take all state variables of the contract passed.
   *
   * @param {obejct} contract contract which state variables will be returned
   * @returns {Array} All State Variables of the contract in the position {index}
   */
  function getStateVariables(contract: IContract): VariableDeclaration[] {
    //If 'contracts' is empty, call the function that populates it
    if (contracts.length == 0) {
      getContractsAndLiteralsPool();
    }
    const index = contracts.findIndex((c) => c.contractDefinition.name == contract.contractDefinition.name);
    return stateVariables[index];
  }

  function getEnums(): IEnumPool {
    //If 'contracts' is empty, call the function that populates it
    if (contracts.length == 0) {
      getContractsAndLiteralsPool();
    }
    return enums;
  }
  /**
   * Search the enums among all contracts in the project that has the name {name}
   * @param {string} name Name of enum searched in the contracts
   * @returns {Array} List of names of all contratcts + '.' + name of enum
   */
  function getEnumsByName(name: string): string[] {
    const retorno: string[] = [];
    //If 'contracts' is empty, call the function that populates it
    if (contracts.length == 0) {
      getContractsAndLiteralsPool();
    }
    //se busca o enum pelo nome completo (contrato + nomeEnum)
    let contractsLocal = Object.keys(enums);
    let searchFor = name;
    if (name.indexOf('.') != -1) {
      const splitedName = name.split('.');
      contractsLocal = contractsLocal.filter((contractName) => contractName == splitedName[0]);
      searchFor = splitedName[1];
    }
    contractsLocal.forEach((contract) => {
      if (Object.keys(enums[contract]).some((m) => m == searchFor)) {
        retorno.push(`${contract}.${searchFor}`);
      }
    });
    return retorno;
  }

  function getStructs(): IStructsPool {
    //If 'contracts' is empty, call the function that populates it
    if (contracts.length == 0) {
      getContractsAndLiteralsPool();
    }
    return structs;
  }

  function getUndeployableContracts(): string[] {
    return undeployableContracts;
  }
  function registerUndeployableContract(contractName: string): void {
    if (!undeployableContracts.includes(contractName)) {
      undeployableContracts.push(contractName);
    }
  }
  function getDeployedJSContracts(): string[] {
    return deployedJSContracts;
  }
  function registerDeployedJSContract(contractName: string): void {
    if (!deployedJSContracts.includes(contractName)) {
      deployedJSContracts.push(contractName);
    }
  }
  function isGraphSet(): boolean {
    return grapho != null;
  }
  function getGraph(): ITruffleProjectGraph {
    if (grapho == null) throw Error(`Truffle Project Graph not set yet`);
    return grapho;
  }
  function setGraph(graph: ITruffleProjectGraph): void {
    grapho = graph;
  }
}
