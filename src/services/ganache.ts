/**
 * Passando o parâmetro --mnemonic ao instanciar o ganache-cli, ele gera sempre os mesmos endereços de contas
 */
export const ganacheMneumonic = 'gesture rather obey video awake genuine patient base soon parrot upset lounge';

/**
 * Contas geradas automaticamente pelo ganache quando utilizado o mneumonico:
 * 'gesture rather obey video awake genuine patient base soon parrot upset lounge';
 */
export const ganacheAccountsAddresses = {
  '0': '0x2B522cABE9950D1153c26C1b399B293CaA99FcF9',
  '1': '0x3644B986B3F5Ba3cb8D5627A22465942f8E06d09',
  '2': '0x9e8f633D0C46ED7170EF3B30E291c64a91a49C7E',
  '3': '0xd1C3bF2F2fd296249228734299290cf8616c1e7c',
  '4': '0x47a793D7D0AA5727095c3Fe132a6c1A46804c8D2',
  '5': '0x0d95EBB4874f17157e40635C19dBC6E9b0BFdb03',
  '6': '0x5243B5970f327c328B2739dEc88abC46FaE8931A',
  '7': '0xe1a1d3637eE02391ac4035e72456Ca7448c73FD4',
  '8': '0x1cF1919d91cebAb2E56a5c0cC7180bB54eD4f3F6',
  '9': '0x0339f40494265DcDd5AB47A3B55183a28952744b',
};

export const ganhacheAccountsPrivateKeys = {
  '0': '0x1aba488300a9d7297a315d127837be4219107c62c61966ecdf7a75431d75cc61',
  '1': '0x66e56aa2896ef489e42fdf1d8059a1359bd6b6d67c83c69d7dc2ed726778de85',
  '2': '0xa088a755d2f0bbf8fc4e5fac3c3b62904e028db8511e4e5af339af40c4e0e16c',
  '3': '0x5a60bad80cd80b0d5d43cd31637c3bc028ad218db40af53d00b0bd2e6359b83a',
  '4': '0x979d8b20000da5832fc99c547393fdfa5eef980c77bfb1decb17c59738d99471',
  '5': '0x5e9203911a5db108bc1e3e94252a2bd713599a79714d6a13e81b12900cefeed7',
  '6': '0x4de630fc08d8896de2083f1cbd963f69091dc99e712654710249a9a8c0f3584c',
  '7': '0x0da6e9ff9aedb7a6bb7db9959057c4785296333330960a6e3c3c3b5cb21727f8',
  '8': '0x9538b189d0e0d3797867669fde17afff1378f69409747605b4a7248f17106738',
  '9': '0xe298329ec59e70752073c898463d106434004086a4ff3049e939fe1c224a2898',
};
