import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { IBranch } from '../../models/IBranch';
import { IScenario } from '../../models/scenario/IScenario';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { IRestriction } from '../../models/IResctriction';
import { RestrictionElementType } from '../../models/RestrictionElementType';
import { getParametersValues } from '../Utils/UtilsFactory';
import { getFunctionSignature } from '../TruffleProjectGraph';
import { getFunctionParametersValues } from './getFunctionParametersValues';
import { GetFunctionParametersValuesFunction } from './GetFunctionParametersValuesFunction';
import { getFunctionParameterRestriction } from '../Restrictions';
import { getPreFunctionCallings } from './preCallings/getPreFunctionCallings';
import { TypeReferences } from '../../models/scenario';
import { updateReferencesFromFunctionCalling } from './preCallings';

/**
 * Percorre a função em busca de restrições impostas via 'require' e retorna um cenário em que, supostamente, passa com sucesso por todas
 essas restrições. Também retorna as respectivas restrições e cada parâmetro para que possa gerar os cenários de falha posteriormente

 * @param {object} truffleProject Instance of Truffle Project under analysis
 * @param {object} functionDefinition FunctionDefinition extracted from the AST
 * @param {object} contractDefinition Optional contract definition which {f} belongs to
 * @param {function} randomValueFunction The function used to return randomValues. The default is the {UtilsFactory.getRandomValue}
 * @param {object} branch Branch that gave origin to the scenario being analysed with all it's data including restrictions that have to be considered, besides the restrictions based on requires found, to achieve a desired target
 * @returns {object} A object with a property "scenario" with a successful scenario object with it's {scenarioDescription} and a
 * {parametersValues} array with parameters
 * to be passed to the function in javascript source (eg. strings between quotes) and a property "paramsWithRestriction",
 * an object where will be inserted the collection of restrictions based on 'require' found in the statements of {functionDefinition}
 */
export function getSuccessfulScenarioAndParameterRestrictions(
  truffleProject: TruffleProjectIceFactoryFunction,
  functionDefinition: FunctionDefinition,
  contractDefinition: ContractDefinition,
  randomValueFunction: GetFunctionParametersValuesFunction = getParametersValues,
  branch: IBranch,
  references: TypeReferences
): {
  scenario: IScenario;
  restrictions: IRestriction[];
} {
  const scenario: IScenario = {
    success: true,
    transactionParameterRestriction: false,
    scenarioDescription: `Should execute ${getFunctionSignature(
      truffleProject.getGraph(),
      functionDefinition,
      functionDefinition.isConstructor
        ? 'constructor'
        : functionDefinition.stateMutability == 'payable'
        ? 'fallback'
        : '<unamed>'
    )}`,
    parametersValues: [],
    preFunctionCallings: [],
  };

  const returnRestrictions: IRestriction[] = getFunctionParameterRestriction(
    truffleProject,
    [functionDefinition],
    contractDefinition,
    branch,
    references
  );
  const fullRestrictions = branch.restrictions.concat(returnRestrictions);
  //update scenario description
  scenario.scenarioDescription += createScenarioDescription(fullRestrictions);
  //indica se alguma das restrições atendidas pelo cenário envolve transaction parameter
  scenario.transactionParameterRestriction = fullRestrictions.some(
    (r) =>
      r.left.elementType == RestrictionElementType.transactionParameter ||
      r.right.elementType == RestrictionElementType.transactionParameter
  );

  //Identifica a necessidade de execução de funções para preparar o ambiente
  //de forma que atenda às restrições. Por exemplo, invocar o uma função que
  //seta uma variável de estado
  scenario.preFunctionCallings = getPreFunctionCallings(
    truffleProject,
    contractDefinition,
    functionDefinition,
    fullRestrictions,
    references
  );
  scenario.preFunctionCallings.forEach((fc) => {
    updateReferencesFromFunctionCalling(fc, references);
  });

  //Gera os valores de entrada da função baseando nas restrições identificadas
  scenario.parametersValues = getFunctionParametersValues(
    truffleProject,
    contractDefinition,
    functionDefinition,
    fullRestrictions,
    references,
    randomValueFunction
  );
  return { scenario, restrictions: returnRestrictions };
}

function createScenarioDescription(restrictions: IRestriction[]): string {
  return restrictions.length == 0
    ? ''
    : ` WHEN `.concat(
        restrictions
          .map((r) =>
            (r.left.elementType == RestrictionElementType.transactionParameter ? 'msg.' : '').concat(
              r.left.value.js.concat(
                r.left.value.memberName ? `.${r.left.value.memberName}` : '',
                r.operator,
                r.right.elementType == RestrictionElementType.transactionParameter ? 'msg.' : '',
                r.right.value.sol,
                r.right.value.memberName ? `.${r.right.value.memberName}` : ''
              )
            )
          )
          .join(',')
      );
}
