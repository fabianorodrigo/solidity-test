import { IContractGraph } from '../../models/graph';
import { RestrictionElementType } from '../../models/RestrictionElementType';
import { TypeReferences } from '../../models/scenario';
import { getReferenceKey } from './getReferenceKey';

/**
 * Update the object {references} with the initial values of all state variables in {contractGraph}
 *
 * references[state variable name] = state variable initial value
 *
 * @param contractGraph contract graph being analysed
 * @param references object that will be updated with the initial values of state variables
 */
export function initReferences(contractGraph: IContractGraph, references: TypeReferences): void {
  //pega primeiro dos ancestrais
  Object.values(contractGraph.inheritsFrom).forEach((cg) => {
    initReferences(cg.contract, references);
  });

  //depois o do próprio contrato caso venha a sobreescrever alguma
  Object.values(contractGraph.stateVariables).forEach((sv) => {
    if (sv.initialValue != null) {
      references[
        getReferenceKey(contractGraph, {
          elementType: RestrictionElementType.stateVariable,
          value: { isCode: false, js: sv.name, sol: sv.name, obj: '##null##' },
        })
      ] = {
        type: sv.type,
        subtype: null,
        stringfieldValue: {
          isCode: false,
          js: sv.initialValue,
          sol: sv.initialValue,
          obj: sv.initialValue,
        },
      };
    }
  });
}
