import fs from 'fs';
import path from 'path';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { getSuccessfulScenarioAndParameterRestrictions } from './successfulScenarios';
import { getFailedScenariosByRestrictions } from './fail';
import { IScenario } from '../../models/scenario/IScenario';
import { getBranches } from '../solidityParser';
import { BranchType } from '../../models/BranchType';
import { IRestriction } from '../../models/IResctriction';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { getParametersValues } from '../Utils/UtilsFactory';
import { RestrictionElementType } from '../../models/RestrictionElementType';
import { GetFunctionParametersValuesFunction } from './GetFunctionParametersValuesFunction';
import { error, warn, bigIntSerializer } from '../Utils';
import { TypeReferences } from '../../models/scenario';
import { initReferences } from './initReferences';
import { getEnabledElementTypes } from '../Restrictions';

export { getSuccessfulScenarioAndParameterRestrictions, getFailedScenariosByRestrictions };
export * from './preCallings';
export * from './initReferences';
export * from './initReferencesGlobal';
export * from './getReferenceKey';

/** *
 * Generate random input parameters values for the parameters of the FunctionDefinition passed
 * Considering the 'require' inside the function
 *
 * @param {object} truffleProject Instance of truffle project under analysis
 * @param {object} functionDefinition FunctionDefinition extracted from the AST
 * @param {object} contractDefinition Optional contract definition which {f} belongs to
 * @param {function} randomValueFunction The function used to return randomValues. The default is the {getRandomValue}
 * @returns {Array} array of scenario objects indicating {success} or not, a {scenarioDescription} and
 * a {parametersValues} array with parameters to be passed to the function in javascript source (eg. strings between quotes).
 * The first item has the values expected to be sucessful, and the others, fails
 */
export function getFunctionParametersValuesScenarios(
  truffleProject: TruffleProjectIceFactoryFunction,
  functionDefinition: FunctionDefinition,
  contractDefinition: ContractDefinition,
  randomValueFunction: GetFunctionParametersValuesFunction = getParametersValues
): IScenario[] {
  if (functionDefinition == null) throw new Error(`functionDefinition can't be null`);

  let scenarios = [];

  //get all branches of function. Por default, todos os branches são cenários válidos (de sucesso)
  const branches = getBranches(truffleProject, contractDefinition, functionDefinition, functionDefinition);
  //computando branches por funcao
  if (global['branchesByFunction'] == null) {
    global['branchesByFunction'] = [];
  }
  global['branchesByFunction'].push([
    path.basename(truffleProject.truffleProjectHome),
    contractDefinition.name,
    functionDefinition.name,
    branches.length,
  ]);
  //Se não existem branches na função, então cria-se um elemento apenas o loop a seguir rodar
  if (branches.length == 0) {
    const restrictions: IRestriction[] = [];
    branches.push({ loc: null, parent: functionDefinition, type: BranchType.notDefined, restrictions: restrictions });
  }
  const key = new Date().toISOString().concat(contractDefinition.name, '.', functionDefinition.name);
  const objDebug = {};
  objDebug[key] = { branches: {} };
  branches.forEach((branch, i) => {
    try {
      objDebug[key].branches[i] = {};
      objDebug[key].branches[i].branch = {
        loc: `${branch.loc ? branch.loc.start.line : ''}-${branch.loc ? branch.loc.end.line : ''}`,
        restrictions: branch.restrictions.map((r) => getDebugInfoRestrictions(r)),
      };
      const references: TypeReferences = {};
      if (global['defaultReferences'] && global['defaultReferences'][contractDefinition.name] != null) {
        Object.keys(global['defaultReferences'][contractDefinition.name]).forEach((k) => {
          //tem que clonar, se for um objeto como array, fica a referência e daí atualiza a global
          references[k] = JSON.parse(
            JSON.stringify(global['defaultReferences'][contractDefinition.name][k], bigIntSerializer)
          );
          //O parse está retornando o obj com tipo string, isso acaba estourando lá na fillsRestriction
          if (typeof global['defaultReferences'][contractDefinition.name][k].stringfieldValue.obj == 'bigint') {
            references[k].stringfieldValue.obj = BigInt(references[k].stringfieldValue.obj);
          }
        });
      }
      references['transactionParameter'] = null;
      functionDefinition.parameters.forEach((functionParameter) => {
        references[functionParameter.name] = null;
      });

      const { scenario, restrictions } = getSuccessfulScenarioAndParameterRestrictions(
        truffleProject,
        functionDefinition,
        contractDefinition,
        randomValueFunction,
        branch,
        references
      );
      objDebug[key].branches[i].scenarioSuccess = getDegubInfoScenario(scenario);
      scenarios.push(scenario);
      const failScenarios = getFailedScenariosByRestrictions(
        truffleProject,
        contractDefinition,
        functionDefinition,
        scenario,
        restrictions.filter(
          (r) =>
            (getEnabledElementTypes().includes(r.left.elementType) ||
              (r.left.elementType == RestrictionElementType.stateVariableMember &&
                r.left.value.memberName == 'length')) &&
            (getEnabledElementTypes().includes(r.right.elementType) ||
              (r.right.elementType == RestrictionElementType.stateVariableMember &&
                r.right.value.memberName == 'length'))
        ), //TODO: Remover esse "OR" quando no EnableElementTypes incluir o StateVariableMember,
        references
      );
      objDebug[key].branches[i].scenarioFail = failScenarios.map((fs) => getDegubInfoScenario(fs));
      if (failScenarios && failScenarios.length > 0) {
        scenarios = scenarios.concat(failScenarios);
      }
    } catch (e) {
      error(
        `EXCEÇÃO AO GERAR CENÁRIOS:`,
        path.basename(truffleProject.truffleProjectHome),
        contractDefinition.name,
        functionDefinition.name,
        JSON.stringify(branch.restrictions)
      );
      throw e;
    }
  });
  if (truffleProject.truffleProjectHome) {
    fs.appendFileSync(
      path.join(truffleProject.truffleProjectHome, 'branchRestrictions.json'),
      ','.concat(JSON.stringify(objDebug, bigIntSerializer, 2))
    );
  }
  return scenarios;
}

function getDegubInfoScenario(s: IScenario) {
  return {
    success: s.success,
    description: s.scenarioDescription,
    parameters: s.parametersValues.map((p) => {
      return { js: p.stringfieldValue.js, sol: p.stringfieldValue.sol, member: p.stringfieldValue.memberName };
    }),
  };
}

function getDebugInfoRestrictions(r: IRestriction) {
  {
    return (r.left.elementType == RestrictionElementType.transactionParameter ? 'msg.' : '').concat(
      r.left.value.js,
      r.left.value.memberName ? '.'.concat(r.left.value.memberName) : '',
      ' ',
      r.operator,
      ' ',
      r.right.elementType == RestrictionElementType.transactionParameter ? 'msg.' : '',
      r.right.value.js,
      r.right.value.memberName ? '.'.concat(r.right.value.memberName) : ''
    );
  }
}
