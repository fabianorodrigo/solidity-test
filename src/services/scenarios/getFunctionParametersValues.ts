import { getFunctionParameterRestriction } from '../Restrictions';
import { FunctionDefinition, ContractDefinition } from 'solidity-parser-antlr';
import { IBranch } from '../../models/IBranch';
import { IRestriction } from '../../models/IResctriction';
import { ITypeValue } from '../../models/scenario/ITypeValue';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import {
  GetFunctionParametersValuesFunction,
  GetFunctionGraphParametersValuesFunction,
} from './GetFunctionParametersValuesFunction';
import { getParametersValues, getGraphParametersValues } from '../Utils/UtilsFactory';
import { IFunctionGraph } from '../../models/graph';
import { TypeReferences } from '../../models/scenario';
/**
 * Generate random values for all function's parameters and attributes of a transaction parameter (eg. msg.sender, msg.value, etc...)
 *
 * @param {TruffleProjectIceFactoryFunction} truffleProject Intance of Truffle Project under analysis
 * @param {contractDefinition} contractDefinition The contract definition which the {functionDefinition} beleongs to
 * @param {FunctionDefinition} functionDefinition The function definition whose parameters values will be generated
 * @param {IRestriction[]} restrictions Restrictions found in the function and that will be obeyed during values generation
 * @param {function} randomValueFunction Function used to get randomic values
 * @param {object} branch Branch that gave origin to the scenario being analysed with all it's data including restrictions that have to be considered, besides the restrictions based on requires found, to achieve a desired target
 * @returns {object} An array of objects with {type, subtype, stringfieldValue}
 */

export function getFunctionParametersValues(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractDefinition: ContractDefinition,
  functionDefinition: FunctionDefinition,
  restrictions: IRestriction[],
  references: TypeReferences,
  randomValueFunction: GetFunctionParametersValuesFunction = getParametersValues
): ITypeValue[] {
  return randomValueFunction(truffleProject, contractDefinition, functionDefinition, restrictions, references);
}

/**
 * Generate random values for all function's parameters and attributes of a transaction parameter (eg. msg.sender, msg.value, etc...)
 *
 * @param {TruffleProjectIceFactoryFunction} truffleProject Intance of Truffle Project under analysis
 * @param {contractDefinition} contractDefinition The contract definition which the {functionDefinition} beleongs to
 * @param {FunctionDefinition} functionDefinition The function definition whose parameters values will be generated
 * @param {IRestriction[]} restrictions Restrictions found in the function and that will be obeyed during values generation
 * @param {function} randomValueFunction Function used to get randomic values
 * @param {object} branch Branch that gave origin to the scenario being analysed with all it's data including restrictions that have to be considered, besides the restrictions based on requires found, to achieve a desired target
 * @returns {object} An array of objects with {type, subtype, stringfieldValue}
 */

export function getFunctionGraphParametersValues(
  truffleProject: TruffleProjectIceFactoryFunction,
  functionGraph: IFunctionGraph,
  restrictions: IRestriction[],
  randomValueFunction: GetFunctionGraphParametersValuesFunction = getGraphParametersValues,
  branch: IBranch
): ITypeValue[] {
  return randomValueFunction(truffleProject, functionGraph, restrictions.concat(branch.restrictions));
}
