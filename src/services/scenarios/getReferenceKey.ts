import { IContractGraph } from '../../models/graph';
import { IRestrictionValue } from '../../models/IResctriction';
import { RestrictionElementType } from '../../models/RestrictionElementType';

export function getReferenceKey(contract: IContractGraph, restrictionValue: IRestrictionValue) {
  switch (restrictionValue.elementType) {
    case RestrictionElementType.stateVariable: {
      return `${contract.name}.${restrictionValue.value.js}`;
    }
    case RestrictionElementType.stateVariableMember: {
      return `${contract.name}.${restrictionValue.value.js}`;
    }
    case RestrictionElementType.otherContractMember: {
      return `${restrictionValue.value.js}.${restrictionValue.value.memberName}`;
    }
    default: {
      return restrictionValue.value.js;
    }
  }
}
