import { FunctionDefinition, ContractDefinition } from 'solidity-parser-antlr';
import { IRestriction } from '../../models/IResctriction';
import { ITypeValue } from '../../models/scenario/ITypeValue';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { IFunctionGraph } from '../../models/graph';
import { TypeReferences } from '../../models/scenario';
export type GetFunctionParametersValuesFunction = (
  truffleProject: TruffleProjectIceFactoryFunction,
  contractDefinition: ContractDefinition,
  functionDefinition: FunctionDefinition,
  restrictions: IRestriction[],
  references: TypeReferences
) => ITypeValue[];

export type GetFunctionGraphParametersValuesFunction = (
  truffleProject: TruffleProjectIceFactoryFunction,
  functionGraph: IFunctionGraph,
  restrictions: IRestriction[]
) => ITypeValue[];
