import { IRestriction } from '../../../models/IResctriction';
import { IFunctionGraph, IContractGraph } from '../../../models/graph';
import { TStateVariableByFunction } from './TStateVariableByFunction';
import { warn } from '../../Utils';
import { getWrittenStateVariable } from './getWrittenStateVariable';
import { TypeReferences } from '../../../models/scenario';
/**
 * Search in the specific contract {cg} the function that write a state variable with the same name as {stateVariableName}
 * Doesn't look in the ancestrals contracts
 *
 * @param contractGraph Contract Graph being analysed
 * @param stateVariableName Name of the state variable which writer functions are pursuit
 * @param stateByFunctionSignature The scructure that maps the functions and
 * the state variables they write
 * @param parentSideRestriction The restriction that {side} belongs
 * @return TRUE if found some, FALSE if didn't found any function in the {cg}
 */

export function updateStateVariableByContractFunction(
  contractGraph: IContractGraph,
  functionGraph: IFunctionGraph,
  stateVariableName: string,
  stateByFunctionSignature: TStateVariableByFunction,
  parentSideRestriction: IRestriction,
  references: TypeReferences
): boolean {
  let foundSome = false;
  Object.keys(contractGraph.functions).forEach((functionName) => {
    //Não inclui no rol da pesquisa a própria função que tem a restrição
    if (functionGraph && functionGraph.name == functionName) return;

    for (let i = 0; i < contractGraph.functions[functionName].length; i++) {
      const fg = contractGraph.functions[functionName][i];
      //pega as variáveis escritas pela função {fg} cujo nome seja igual a {stateVariableName} AND
      //a fonte não seja um literal ou, se for, que atenda a restrição imposta por {parentSideRestriction}
      const writtenStatesVars = getWrittenStateVariable(fg, stateVariableName, parentSideRestriction, references);
      if (writtenStatesVars.length > 0) {
        if (writtenStatesVars.length > 1) {
          warn('que situação é essa que o writtenStateVars é maior que 1?');
        }
        foundSome = true;
        if (stateByFunctionSignature[fg.signature] == null) {
          stateByFunctionSignature[fg.signature] = {
            contractInstanceTarget: contractGraph.name,
            functionGraph: fg,
            stateVariables: [],
          };
        }
        stateByFunctionSignature[fg.signature].stateVariables = stateByFunctionSignature[
          fg.signature
        ].stateVariables.concat(writtenStatesVars);
      }
    }
  });
  return foundSome;
}
