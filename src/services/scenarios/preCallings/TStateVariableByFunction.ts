import { IContractGraph, IFunctionGraph, IStateVariableSetGraph } from '../../../models/graph';

export type TStateVariableByFunctionItem = {
  contractInstanceTarget: string;
  functionGraph: IFunctionGraph;
  stateVariables: IStateVariableSetGraph[];
};
export type TStateVariableByFunction = {
  [signature: string]: TStateVariableByFunctionItem;
};
