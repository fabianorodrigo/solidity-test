import { IRestriction, IRestrictionValue } from '../../../models/IResctriction';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { TruffleProjectIceFactoryFunction } from '../../TruffleFactory';
import { RestrictionElementType } from '../../../models/RestrictionElementType';
import { TStateVariableByFunction } from './TStateVariableByFunction';
import { updateStateVariableByContractFunction } from './updateStateVariableByContractFunction';
import { TypeReferences } from '../../../models/scenario';
import { IContractGraph } from '../../../models/graph';
/**
 * Com base no grafo do projeto, atualiza o objeto {stateByFunctionSignature},
 * que guarda a relação de variáveis de estado escritas por função.
 * Também atualiza o objeto {uniqueStateVariables}, que guarda a lista 'unique'
 * de variáveis de estado do conjunto de restrições sendo analisado
 *
 * @param truffleProject
 * @param contractDefinition
 * @param functionDefinition
 * @param side A left or right side of {parentSideRescriction} being analysed
 * @param stateByFunctionSignature The scructure that maps the functions and
 * the state variables they write
 * @param uniqueStateVariables The Set where state variable names are stored
 * @param parentSideRestriction The restriction that {side} belongs
 */
export function updateFunctionByStateVar(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractGraph: IContractGraph,
  functionDefinition: FunctionDefinition,
  side: IRestrictionValue,
  stateByFunctionSignature: TStateVariableByFunction,
  uniqueStateVariables: Set<string>,
  parentSideRestriction: IRestriction,
  references: TypeReferences
): boolean {
  let foundSome = false;
  if (
    [
      RestrictionElementType.stateVariable,
      RestrictionElementType.stateVariableMember,
      RestrictionElementType.stateVariableIndex,
    ].includes(side.elementType)
  ) {
    const fg = contractGraph.functions[functionDefinition.name]?.find(
      (f) => f.parameters.length == functionDefinition.parameters.length
    );
    //Percorre as funções do próprio contrato para checar se existe alguma que
    //escreva na state variable cujo nome está armazenado em {side.value.js}
    //se nada for encontrado, começa a olhar nas classes ancestrais
    foundSome = updateStateVariableByContractFunction(
      contractGraph,
      fg,
      side.value.js,
      stateByFunctionSignature,
      parentSideRestriction,
      references
    );
    if (!foundSome) {
      const ancestrals = Object.values(contractGraph.inheritsFrom);
      for (let i = 0; i < ancestrals.length; i++) {
        if (
          updateFunctionByStateVar(
            truffleProject,
            ancestrals[i].contract,
            functionDefinition,
            side,
            stateByFunctionSignature,
            uniqueStateVariables,
            parentSideRestriction,
            references
          )
        ) {
          //se foi encontrada em um ancestral, precisará sobrescrever o nome da instância alvo
          //para que na geração do arquivo de teste, aponte para {estaInstancia.funcao} ao
          //invés de apontar para o {ancestral.funcao}, pois isso acaba mudando estado de uma
          //instância que não é a que se precisa para realizar o teste (ex. isso aconteceu no
          //projeto linha-financiamento-dapp. Ao invés de chamar contractLinhafinancimaento.mudarGerente,
          //estava chamando contractGerenciavel.mudarGerente ...)
          Object.values(stateByFunctionSignature).forEach((sbf) => {
            sbf.contractInstanceTarget = contractGraph.name;
          });
          foundSome = true;
          break;
        }
      }
    } else {
      //TODO: Pensar se vamos precisar guardar membros e indices de variáveis de estado também
      uniqueStateVariables.add(side.value.js);
    }
  }
  return foundSome;
}
