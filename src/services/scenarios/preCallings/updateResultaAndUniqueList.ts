import { TruffleProjectIceFactoryFunction } from '../../TruffleFactory';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { IFunctionCalling } from '../../../models';
import { getFunctionParameterRestriction } from '../../Restrictions';
import { getPreFunctionCallings } from './getPreFunctionCallings';
import { TStateVariableByFunctionItem } from './TStateVariableByFunction';
import { EMPTY_BRANCH } from '../../../models/IBranch';
import { mapStateRestrictionsToFunctionParameter } from '../mapStateRestrictionsToFunctionParameters';
import { getFunctionParametersValues } from '../getFunctionParametersValues';
import { TypeReferences } from '../../../models/scenario';
import { GetFunctionParametersValuesFunction } from '../GetFunctionParametersValuesFunction';
import { getParametersValues } from '../../Utils/UtilsFactory';
import { IRestriction } from '../../../models/IResctriction';
import { RestrictionElementType } from '../../../models/RestrictionElementType';
import { updateReferencesFromFunctionCalling } from './updateReferencesFromFunctionCalling';

export function updateResultAndUniqueList(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractDefinition: ContractDefinition,
  stateRestrictions: IRestriction[],
  references: TypeReferences,
  randomValueFunction: GetFunctionParametersValuesFunction = getParametersValues,
  mappedFunction: TStateVariableByFunctionItem,
  result: IFunctionCalling[],
  uniqueStateVariables: Set<string>
) {
  //A função a ser chamada pode ter restrições, por exemplo, modifiers
  //e devem ser avaliadas para verificar se existe necessidade de chamar
  //outras funções antes. Contudo, são descartadas restrições que façam
  //referência às mesmas state variables presentes em {stateRestrictions}
  const functionCalledRestrictions = getFunctionParameterRestriction(
    truffleProject,
    [mappedFunction.functionGraph.functionDefinition],
    mappedFunction.functionGraph.contract.contractDefinition,
    EMPTY_BRANCH,
    references
  );
  const functionCalledStateRestrictions = functionCalledRestrictions.filter(
    (fcr) =>
      ([RestrictionElementType.stateVariable, RestrictionElementType.otherContractMember].includes(
        fcr.left.elementType
      ) &&
        !stateRestrictions.some(
          (sr) => sr.left.value.js == fcr.left.value.js || sr.left.value.js == fcr.right.value.js
        )) ||
      ([RestrictionElementType.stateVariable, RestrictionElementType.otherContractMember].includes(
        fcr.right.elementType
      ) &&
        !stateRestrictions.some(
          (sr) => sr.right.value.js == fcr.left.value.js || sr.right.value.js == fcr.right.value.js
        ))
  );

  //Se a pré função chamada tiver também pré requisitos (precisa invocar outra função antes dela)
  if (functionCalledStateRestrictions.length > 0) {
    const preFunctionCalling = getPreFunctionCallings(
      truffleProject,
      mappedFunction.functionGraph.contract.contractDefinition,
      mappedFunction.functionGraph.functionDefinition,
      functionCalledStateRestrictions, //.concat(branch.restrictions)
      references
    );
    if (preFunctionCalling.length > 0) {
      preFunctionCalling.forEach((pfc) => {
        result.push(pfc);
      });
      // Atualiza a propriedade do {references} que tem o mesmo nome da StateVariable
      // para um valor Literal que já fora atribuído aos parâmetros das funções
      // armazenadas no {functionCallings}. ex: references['owner'] = 'accounts[0]'
      preFunctionCalling.forEach((fc) => {
        updateReferencesFromFunctionCalling(fc, references);
      });
    }
  }

  result.push({
    contractInstanceTarget: mappedFunction.contractInstanceTarget,
    functionCalled: mappedFunction.functionGraph,
    parametersValues: getFunctionParametersValues(
      truffleProject,
      truffleProject.getGraph().contracts[mappedFunction.contractInstanceTarget].contractDefinition,
      mappedFunction.functionGraph.functionDefinition,
      functionCalledRestrictions.concat(
        mapStateRestrictionsToFunctionParameter(
          stateRestrictions,
          mappedFunction.functionGraph,
          functionCalledRestrictions
        ).filter((r) => !hasContradiction(r, functionCalledRestrictions))
      ), //TODO: pegar as restriçõees para setar a própria variável de estado?
      references,
      randomValueFunction
    ),
    stateVariablesAffected: mappedFunction.stateVariables.map((sv) => {
      return { name: sv.name };
    }),
  });
  mappedFunction.stateVariables.forEach((sv) => {
    uniqueStateVariables.delete(sv.name);
  });
}

/**
 * Check in the {restrictions} array if there is some restriction that is the
 * opposite of {restricton}
 *
 * @param restriction
 * @param filteredRestrictions
 */
function hasContradiction(restriction: IRestriction, restrictions: IRestriction[]) {
  const filteredRestrictions = restrictions.filter(
    (r) =>
      (r.left.value.js == restriction.left.value.js && r.right.value.js == r.right.value.js) ||
      (r.left.value.js == restriction.right.value.js && r.right.value.js == restriction.left.value.js)
  );
  for (let i = 0; i < filteredRestrictions.length; i++) {
    //restrição diz que tem que ser igual X, mas na coleção tem restrições dizendo que tem que ser diferente de X
    if (
      restriction.operator == '==' &&
      ((restriction.left.value.js == filteredRestrictions[i].left.value.js &&
        restriction.right.value.js == filteredRestrictions[i].right.value.js) ||
        (restriction.left.value.js == filteredRestrictions[i].right.value.js &&
          restriction.right.value.js == filteredRestrictions[i].left.value.js)) &&
      ['>', '<', '!='].includes(filteredRestrictions[i].operator)
    ) {
      return true;
    }
    //restrição diz que tem que ser igual X, mas na coleção tem restrições dizendo que tem que ser igual a Y
    if (
      restriction.operator == '==' &&
      ((restriction.left.value.js == filteredRestrictions[i].left.value.js &&
        restriction.right.value.js != filteredRestrictions[i].right.value.js) ||
        (restriction.left.value.js == filteredRestrictions[i].right.value.js &&
          restriction.right.value.js != filteredRestrictions[i].left.value.js)) &&
      ['=='].includes(filteredRestrictions[i].operator)
    ) {
      return true;
    }
    //restrição diz que tem que ser maior, menor, difente de X, mas na coleção tem restrições dizendo que tem que ser igual a X
    if (
      ['>', '<', '!='].includes(restriction.operator) &&
      ((restriction.left.value.js == filteredRestrictions[i].left.value.js &&
        restriction.right.value.js == filteredRestrictions[i].right.value.js) ||
        (restriction.left.value.js == filteredRestrictions[i].right.value.js &&
          restriction.right.value.js == filteredRestrictions[i].left.value.js)) &&
      ['=='].includes(filteredRestrictions[i].operator)
    ) {
      return true;
    }
  }
  /*if (['==', '<=', '<'].includes(restriction.operator) && filtered.filter((r) => r.operator == '>')) return true;
  if (['==', '>=', '>'].includes(restriction.operator) && filtered.filter((r) => r.operator == '<')) return true;
  if (['=='].includes(restriction.operator) && filtered.filter((r) => r.operator == '!=')) return true;
  if (['!='].includes(restriction.operator) && filtered.filter((r) => r.operator == '==')) return true;*/
  return false;
}
