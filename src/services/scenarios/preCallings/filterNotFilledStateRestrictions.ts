import { IContractGraph } from '../../../models/graph';
import { IRestriction, IRestrictionValue } from '../../../models/IResctriction';
import { RestrictionElementType } from '../../../models/RestrictionElementType';
import { TypeReferences } from '../../../models/scenario';
import { fillsRestrictions, getNormalizedLiteralRestrictions, extractRealRestrictions } from '../../Restrictions';
import { getReferenceKey } from '../getReferenceKey';

/**
 * Return TRUE if:
 * 1. the left or right side of {restriction} is a RestrictionElementType.stateVariable
 * 2. the refered state variable is not set in {references} or, if set, it's value not attends the restriction
 * @param restriction Restriction being analysed
 */
export function filterNotFilledStateRestrictions(
  contract: IContractGraph,
  restriction: IRestriction,
  references: TypeReferences
): boolean {
  return (
    (isElementTypeCovered(restriction.left) &&
      (references[getReferenceKey(contract, restriction.left)] == null ||
        !fillsRestrictions(
          references[getReferenceKey(contract, restriction.left)]?.stringfieldValue.obj,
          extractRealRestrictions(contract, [restriction], references)
        ))) ||
    (isElementTypeCovered(restriction.right) &&
      (references[getReferenceKey(contract, restriction.right)] == null ||
        !fillsRestrictions(
          references[getReferenceKey(contract, restriction.right)]?.stringfieldValue.obj,
          extractRealRestrictions(contract, getNormalizedLiteralRestrictions([restriction]), references)
        )))
  );
}

//TODO: depois de testar pode mudar para REstrictions.getStateEnabledElementTypes
export function isElementTypeCovered(rv: IRestrictionValue) {
  return (
    rv.elementType == RestrictionElementType.stateVariable ||
    rv.elementType == RestrictionElementType.stateVariableMember
  );
}
