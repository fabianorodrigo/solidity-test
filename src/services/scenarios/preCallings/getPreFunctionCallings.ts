import { IFunctionCalling } from '../../../models';
import { IRestriction, IRestrictionValue } from '../../../models/IResctriction';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { TruffleProjectIceFactoryFunction } from '../../TruffleFactory';
import { GetFunctionParametersValuesFunction } from '../GetFunctionParametersValuesFunction';
import { getParametersValues } from '../../Utils/UtilsFactory';
import { TypeReferences } from '../../../models/scenario';
import { trace, warn } from '../../Utils';
import { updateFunctionByStateVar } from './updateFunctionByStateVar';
import { TStateVariableByFunction } from './TStateVariableByFunction';
import { updateResultAndUniqueList } from './updateResultaAndUniqueList';
import { filterNotFilledStateRestrictions } from './filterNotFilledStateRestrictions';
import { getContractConstructor } from '../../solidityParser';
import { filterNotFilledOtherContractStateRestrictions } from './filterNotFilledOtherContractStateRestrictions';
import { RestrictionElementType } from '../../../models/RestrictionElementType';
import { IContractGraph, ITruffleProjectGraph } from '../../../models/graph';

/**
 * Identifica a necessidade de execução de funções para preparar o ambiente de forma que
 * atenda às restrições passadas. Por exemplo, se entre as restrições {restrictions} existir
 * uma restrição associada a uma variável de estado, procurará uma função que possa invocar
 * e setar a respectiva variável
 *
 * @param truffleProject Instance of Truffle Project under analysis
 * @param contractDefinition Optional contract definition which {f} belongs to
 * @param functionDefinition FunctionDefinition extracted from the AST
 * @param restrictions restrições da função para a qual está sendo gerado o scenario
 */
export function getPreFunctionCallings(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractDefinition: ContractDefinition,
  functionDefinition: FunctionDefinition,
  restrictions: IRestriction[],
  references: TypeReferences,
  randomValueFunction: GetFunctionParametersValuesFunction = getParametersValues
): IFunctionCalling[] {
  const result: IFunctionCalling[] = [];
  //filtra restrições que tenham variável de estado envolvidas e que
  //a referida state já tem chamadas anteriores que a setam (está em references)
  //TODO: ampliar o filtro para stateVariableMember e stateVariableIndex ...
  const stateRestrictions = restrictions
    .filter((r) =>
      filterNotFilledStateRestrictions(truffleProject.getGraph().contracts[contractDefinition.name], r, references)
    )
    .concat(
      restrictions.filter((r) =>
        filterNotFilledOtherContractStateRestrictions(
          truffleProject.getGraph().contracts[contractDefinition.name],
          r,
          references
        )
      )
    );
  trace('getPreFunctionCallings', functionDefinition.name, stateRestrictions.length);
  if (stateRestrictions.length == 0) return result;

  const stateByFunction: TStateVariableByFunction = {};
  const uniqueStateVariables: Set<string> = new Set<string>();
  //1. identificar as funções que alteram a variável (variáveis)
  stateRestrictions.forEach((sr) => {
    const { contractTargetLeft, restrictionValueLeft, contractTargetRight, restrictionValueRight } = getTarget(
      truffleProject.getGraph().contracts[contractDefinition.name],
      sr
    );
    updateFunctionByStateVar(
      truffleProject,
      contractTargetLeft,
      functionDefinition,
      restrictionValueLeft,
      stateByFunction,
      uniqueStateVariables,
      sr,
      references
    );
    updateFunctionByStateVar(
      truffleProject,
      contractTargetRight,
      functionDefinition,
      restrictionValueRight,
      stateByFunction,
      uniqueStateVariables,
      sr,
      references
    );
  });
  //com base na lista de state variables encontrada, seta o o references, se já não ocorreu isso antes
  uniqueStateVariables.forEach((stateVarName) => {
    if (references[stateVarName] == null) {
      references[stateVarName] = null;
    } else {
      warn(
        `${contractDefinition.name}.${functionDefinition.name}: precalling já está com '${stateVarName}' setada: ${
          typeof references[stateVarName] == 'object'
            ? JSON.stringify(references[stateVarName])
            : references[stateVarName]
        }`
      );
    }
  });
  //2. identificar se essa alteração tem restrições a serem atentidas
  //3. identificar se essas restrições são conflitantes com as restrições em stateREstrictions
  //4. priorizar o construtor, gerar os valores dos parâmetros, fazer um push em 'result'
  //e limpar da lista unique de variáveis de estado aquelas que foram satisfeitas
  Object.values(stateByFunction)
    //só considera construtor se for do próprio contrato ou de um ancestral caso o contrato não tenha um construtor
    .filter(
      (sbf) =>
        sbf.functionGraph.isConstructor &&
        (sbf.functionGraph.contract.name == contractDefinition.name ||
          getContractConstructor(contractDefinition, true) == null)
    )
    .forEach((sbf) => {
      updateResultAndUniqueList(
        truffleProject,
        contractDefinition,
        stateRestrictions,
        references,
        randomValueFunction,
        sbf,
        result,
        uniqueStateVariables
      );
    });
  //5. Se restou alguma variável de estado não atendida pelo construtor, procura pelas funções que a
  //atualiza para gerar chamadas a estas, prioriza funções que alteram mais variáveis de estado sem restrições
  while (uniqueStateVariables.size > 0) {
    //Filtra somente as funções que tenham referência para alguma das variáveis restantes
    //na lista unique de variáveis de estado obtida das restrições e tira o construtor também
    //e ordena colocando as funções com maior número de variáveis de estado alterada primeiro
    const filteredStateByFunction = Object.values(stateByFunction)
      .filter(
        (sbf) => !sbf.functionGraph.isConstructor && sbf.stateVariables.some((sv) => uniqueStateVariables.has(sv.name))
      )
      .sort((a, b) => a.stateVariables.length - b.stateVariables.length); //TODO: Verificar se esta ordenação está correta
    //FIXME: por enquanto, vamos deixar uma implementação simples, depois tentamos otimizar fazendo o mínimo de chamadas possível (se uma função atualizar duas variáveis de estado, dá preferência a esta ao invés de chamar funções mais vezes)

    uniqueStateVariables.forEach((svName) => {
      const writerFunctionsOfSvName = filteredStateByFunction.filter((sbf) =>
        sbf.stateVariables.some((sv) => sv.name == svName)
      );
      if (writerFunctionsOfSvName.length > 0) {
        updateResultAndUniqueList(
          truffleProject,
          contractDefinition,
          stateRestrictions,
          references,
          randomValueFunction,
          writerFunctionsOfSvName[0],
          result,
          uniqueStateVariables
        );
      } else {
        uniqueStateVariables.delete(svName);
      }
    });
  }
  return result;
}

function getTarget(contractGraph: IContractGraph, sr: IRestriction) {
  let contractTargetLeft: IContractGraph = contractGraph;
  let contractTargetRight: IContractGraph = contractGraph;
  let restrictionValueLeft: IRestrictionValue = sr.left;
  let restrictionValueRight: IRestrictionValue = sr.right;
  //se for membro de outro contrato, tem que passar o contractDefinition deste contrato;
  if (sr.left.elementType == RestrictionElementType.otherContractMember) {
    contractTargetLeft = contractGraph.projectGraph.contracts[sr.left.value.js];
    restrictionValueLeft = {
      elementType: RestrictionElementType.stateVariable,
      value: {
        isCode: false,
        js: sr.left.value.memberName,
        sol: sr.left.value.memberName,
        obj: sr.left.value.obj,
      },
    };
  }
  if (sr.right.elementType == RestrictionElementType.otherContractMember) {
    contractTargetRight = contractGraph.projectGraph.contracts[sr.right.value.js];
    restrictionValueRight = {
      elementType: RestrictionElementType.stateVariable,
      value: {
        isCode: false,
        js: sr.right.value.memberName,
        sol: sr.right.value.memberName,
        obj: sr.right.value.obj,
      },
    };
  }
  return { contractTargetLeft, restrictionValueLeft, contractTargetRight, restrictionValueRight };
}
