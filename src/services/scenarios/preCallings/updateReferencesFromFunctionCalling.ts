import { IFunctionCalling } from '../../../models';
import { TypeReferences } from '../../../models/scenario';
import {
  IStateVariableSetGraph,
  SourceVariableSetType,
  IStateVariableGraph,
  StateVariableSetOperator,
} from '../../../models/graph';
import { getWrittenStateVariables } from './getWrittenStateVariables';
import { getIValueFromStateVariableSetGraph, getIValueFromValue } from '../../types';
import { TransactionParameterType } from '../../Utils';
import { transactionParameterAttributes } from '../../transactionParameterAttributers';
import { getStateVariableReference } from '../../TruffleProjectGraph';
import { ArrayTypeNameFactory } from '../../types/ArrayTypeName';

/**
 * Com base na chamada de função {functionCalling}, o código desta função e os parâmetros de entrada
 * determina os valores das variáveis de estado após sua chamada
 *
 * Se o último parâmetro for do tipo TransactionParameter, também atualiza a references['transactionParameter']
 *
 * @param functionCalling Chamada a função com os valores dos parâmetros a serem utilizados
 * @param references Instância que mapeia as variáveis de estado com seus respectivos valores
 * @returns TRUE se algo foi atualiado em {references}
 */
export function updateReferencesFromFunctionCalling(
  functionCalling: IFunctionCalling,
  references: TypeReferences
): boolean {
  let result = false;
  //Só atualiza a referência de transaction parameter se o último parâmetro for do tipo transaction parameter
  if (functionCalling.parametersValues.length > 0) {
    const lastParameter = functionCalling.parametersValues[functionCalling.parametersValues.length - 1];
    if (lastParameter.type == TransactionParameterType) {
      references['transactionParameter'] = lastParameter;
      result = true;
    }
  }
  //TODO: essa  função abaixo deveria buscar as states modificadas pelas funções chamadas também
  const stateVarsByFunction: { [functionSignature: string]: IStateVariableSetGraph[] } = getWrittenStateVariables(
    functionCalling.functionCalled
  );
  Object.keys(stateVarsByFunction).forEach((functionSignature) => {
    stateVarsByFunction[functionSignature]?.forEach((sv) => {
      const state: IStateVariableGraph = getStateVariableReference(functionCalling.functionCalled.contract, sv.name);
      if (!state) return;
      const svKey = `${functionCalling.contractInstanceTarget}.${sv.name}`;
      //TODO: e se state.isArray == true?
      if (sv.source.type == SourceVariableSetType.transactionParameter) {
        const i = functionCalling.parametersValues.length > 0 ? functionCalling.parametersValues.length - 1 : -1;
        if (i >= 0) {
          if (
            functionCalling.parametersValues[i].type == TransactionParameterType &&
            transactionParameterAttributes.hasOwnProperty(sv.source.aditionalInfo)
          ) {
            let v =
              functionCalling.parametersValues[i].stringfieldValue.obj[
                transactionParameterAttributes[sv.source.aditionalInfo].jsName
              ];
            references[svKey] = getIValueFromValue(state.type, v);
            result = true;
          }
        }
      }
      //TODO: e se state.isArray == true?
      else if (sv.source.type == SourceVariableSetType.parameterMemberAccess) {
        const i = functionCalling.functionCalled.parameters.findIndex((p) => p.name == sv.source.name);
        let v = null;
        if (functionCalling.functionCalled.parameters[i].isEnum) {
          v = functionCalling.functionCalled.contract.enums[sv.source.name].members.indexOf(sv.source.aditionalInfo);
        } else {
          v = functionCalling.parametersValues[i].stringfieldValue.obj[sv.source.aditionalInfo];
        }
        references[svKey] = getIValueFromValue(state.type, v);
        result = true;
      }
      //TODO: e se state.isArray == true?
      else if (sv.source.type == SourceVariableSetType.parameter) {
        const i = functionCalling.functionCalled.parameters.findIndex((p) => p.name == sv.source.name);
        if (i >= 0) {
          const v = functionCalling.parametersValues[i].stringfieldValue;
          if (sv.operator == StateVariableSetOperator.simpleSet) {
            references[svKey] = {
              type: functionCalling.functionCalled.parameters[i].type,
              subtype: null,
              stringfieldValue: v,
            };
          } else if (sv.operator == StateVariableSetOperator.arrayPush) {
            if (references[svKey] == null) {
              references[svKey] = {
                type: 'ArrayTypeName',
                subtype: functionCalling.functionCalled.parameters[i].type,
                stringfieldValue: { isCode: false, js: `[]`, obj: [], sol: null },
              };
            }
            const objV = references[svKey].stringfieldValue.obj;
            objV.push(v.obj);
            references[svKey].stringfieldValue.js = `[${objV.toString()}]`; //TODO: Isso precisa ser melhor testado
            references[svKey].stringfieldValue.obj = objV;
          }
          result = true;
        }
      }
      //TODO: e se state.isArray == true?
      else if (sv.source.type == SourceVariableSetType.literal) {
        references[svKey] = getIValueFromStateVariableSetGraph(functionCalling.functionCalled, sv);
        result = true;
      }
      //Se o tipo da fonte é "Others", não é possível determinar este valor, então insere null apenas pra incrementar seu length
      else if (
        sv.source.type == SourceVariableSetType.others &&
        state.isArray &&
        sv.operator == StateVariableSetOperator.arrayPush
      ) {
        references[svKey].stringfieldValue.obj.push(null);
      }
    });
  });
  return result;
  //TODO: em algum momento, considerar as restrictions para que a variável consiga ser escrita
}
