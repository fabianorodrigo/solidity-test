import { IRestriction } from '../../../models/IResctriction';
import { IFunctionGraph, SourceVariableSetType, IStateVariableSetGraph } from '../../../models/graph';
import { fillsRestrictions, extractRealRestrictions } from '../../Restrictions';
import { TypeReferences } from '../../../models/scenario';
import { RestrictionElementType } from '../../../models/RestrictionElementType';
/**
 * Retorna todas as referências a escrita da variável de estado com o nome {stateVariableName}
 * pela função {fg} diretamente em seu corpo ou indiretamente por chamada a outras funções do
 * próprio do contrato ou de mesmo de outros contratos.
 *
 * Contudo, essa escrita só é retornada se não for um valor literal (ex. atribuiu 10 e pronto)
 * Ou então, se for literal, esse literal tem que atender ao requisito da restrição {restriction}
 *
 * @param fg Function Graph sendo analisada
 * @param stateVariableName Nome da variável de estado procurada
 */
export function getWrittenStateVariable(
  fg: IFunctionGraph,
  stateVariableName: string,
  restriction: IRestriction,
  references: TypeReferences
): IStateVariableSetGraph[] {
  const result = Object.values(fg.stateVariablesWritten).filter(
    (sv) =>
      sv.name == stateVariableName &&
      (sv.source.type != SourceVariableSetType.literal ||
        (extractRealRestrictions(fg.contract, [restriction], references).filter(
          (r) =>
            r.right.elementType == RestrictionElementType.Literal ||
            r.right.elementType == RestrictionElementType.EnumMember
        ).length > 0 &&
          fillsRestrictions(
            typeof sv.source.aditionalInfo == 'string'
              ? sv.source.aditionalInfo
              : (sv.source.aditionalInfo as any).toString(),
            extractRealRestrictions(fg.contract, [restriction], references)
          )))
  );
  //Apenas se não encontrar na própria função, busca nas funções as quais ela chama
  if (result.length == 0) {
    const calledArray = Object.values(fg.calledFunctions);
    forOut: for (let i = 0; i < calledArray.length; i++) {
      for (let j = 0; j < calledArray[i].length; j++) {
        //se for uma chamada recursiva, ignora
        if (Object.is(fg, calledArray[i][j])) continue;
        const r = getWrittenStateVariable(calledArray[i][j], stateVariableName, restriction, references);
        if (r.length > 0) {
          return r;
          break forOut;
        }
      }
    }
  }
  return result;
}
