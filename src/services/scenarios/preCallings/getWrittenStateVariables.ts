import { IFunctionGraph, SourceVariableSetType, IStateVariableSetGraph } from '../../../models/graph';
import { SvgIcon } from '@material-ui/core';
import { isIdentifier, isTransactionParameterMemberAccess, isMemberAccess } from '../../solidityParser';
import { isLiteral } from '../../solidityParser/statements';
import { NumberLiteral } from 'solidity-parser-antlr';

/**
 * Retorna todas as referências a escrita de variáveis de estado pela função {fg}
 * diretamente em seu corpo ou indiretamente por chamada a outras funções do
 * próprio do contrato ou de mesmo de outros contratos.
 *
 * @param fg Function Graph sendo analisada
 */
export function getWrittenStateVariables(
  fg: IFunctionGraph
): { [functionSignature: string]: IStateVariableSetGraph[] } {
  let result = {};
  //pega as variáveis de estado setadas na função
  const stateVars = Object.values(fg.stateVariablesWritten);
  if (stateVars.length > 0) {
    result = { [fg.signature]: stateVars };
  }
  //se a função se tratar de um construtor, pega todos os construtores das funções que herda
  //É esquisito, mas é isso que está acontecendo, por exemplo, com o contrato Splitter do projeto splitter
  //o ONWER é setado pelo Ownable
  if (fg.isConstructor) {
    Object.values(fg.contract.inheritsFrom).forEach((cg) => {
      if (Array.isArray(cg.contract.functions['constructor']) && cg.contract.functions['constructor'].length > 0) {
        const parentConstructor = cg.contract.functions['constructor'][0];
        const wsvPc = getWrittenStateVariables(parentConstructor);
        ///
        Object.keys(wsvPc).forEach((constructorSignature) => {
          if (result[fg.signature] == null) {
            result[fg.signature] = [];
          }
          wsvPc[constructorSignature].forEach((svsg) => {
            const svsgClone = JSON.parse(JSON.stringify(svsg));
            result[fg.signature].push(svsgClone);
          });
        });
        ///
      }
    });
  }
  const calledArray = Object.values(fg.functionCallings);
  for (let i = 0; i < calledArray.length; i++) {
    for (let j = 0; j < calledArray[i].length; j++) {
      const resultCallings = getWrittenStateVariables(calledArray[i][j].functionCalled);
      if (Object.keys(resultCallings).length > 0) {
        //Pra cada assinatura de função encontrada, percorre as invocações encontradas
        //e adiciona ao resultado final
        Object.keys(resultCallings).forEach((functionSignature) => {
          if (result[functionSignature] == null) {
            result[functionSignature] = [];
          }
          resultCallings[functionSignature].forEach((svsg) => {
            const svsgClone = JSON.parse(JSON.stringify(svsg));
            if (svsgClone.source.type == SourceVariableSetType.parameter) {
              const iParam = calledArray[i][j].functionCalled.parameters.findIndex(
                (p) => p.name == svsgClone.source.name
              );
              //encontra a posição do parâmetro na função chamada
              //com isso, pode-se pegar o identificador no escopo da chamadora
              if (iParam > -1) {
                //se a função chamadora está passando um transaction parameter
                //modifica o source para adequar
                if (isTransactionParameterMemberAccess(calledArray[i][j].parameters[iParam])) {
                  svsgClone.source.aditionalInfo = 'msg';
                  svsgClone.source.aditionalInfo = calledArray[i][j].parameters[iParam].memberName;
                  svsgClone.source.type = SourceVariableSetType.transactionParameter;
                }
                //se a chamadora está repassando uma propriedade de um parâmetro de entrada,
                //então muda o parameter type
                else if (
                  isMemberAccess(calledArray[i][j].parameters[iParam]) &&
                  fg.parameters.find((p) => p.name == calledArray[i][j].parameters[iParam].name)
                ) {
                  svsgClone.source.name = calledArray[i][j].parameters[iParam].name;
                  svsgClone.source.type = SourceVariableSetType.parameterMemberAccess;
                  svsgClone.source.aditionalInfo = calledArray[i][j].parameters[iParam].memberName;
                }
                //se a chamadora tem um parâmetro com o mesmo nome passado para o argument,
                //então só está repassando o parâmetro e o source.type continua sendo parameter
                else if (
                  isIdentifier(calledArray[i][j].parameters[iParam]) &&
                  fg.parameters.find((p) => p.name == calledArray[i][j].parameters[iParam].name)
                ) {
                  svsgClone.source.name = calledArray[i][j].parameters[iParam].name;
                } //se a chamadora passa um literal
                else if (isLiteral(calledArray[i][j].parameters[iParam])) {
                  svsgClone.source.type = SourceVariableSetType.literal;
                  svsgClone.source.aditionalInfo = calledArray[i][j].parameters[iParam].hasOwnProperty('number')
                    ? (calledArray[i][j].parameters[iParam] as NumberLiteral).number
                    : (calledArray[i][j].parameters[iParam] as any).value;
                }
              }
            }
            result[functionSignature].push(svsgClone);
          });
        });
      }
    }
  }
  return result;
}
