import { IRestriction, IRestrictionValue } from '../../models/IResctriction';
import { FunctionDefinition } from 'solidity-parser-antlr';
import { RestrictionElementType } from '../../models/RestrictionElementType';
import { IFunctionGraph, SourceVariableSetType } from '../../models/graph';

/**
 * Converte as restrições do tipo StateVariable para functionParameter
 * quando ocorrer atribuição dentro de {functionCalled}
 *
 * @param stateRestrictions
 * @param functionCalled
 * @param preCallingRestrictions Restrictions already present in the previous callings
 */
export function mapStateRestrictionsToFunctionParameter(
  stateRestrictions: IRestriction[],
  functionCalled: IFunctionGraph,
  preCallingRestrictions: IRestriction[]
): IRestriction[] {
  const result: IRestriction[] = [];
  const cloneRestrictions = JSON.parse(JSON.stringify(stateRestrictions)) as IRestriction[];
  cloneRestrictions.forEach((sr) => {
    //treatTransitiveRestriction(sr, preCallingRestrictions);
    switchSideStateRestrictionValueToParameter(functionCalled, sr.left);
    switchSideStateRestrictionValueToParameter(functionCalled, sr.right);
  });
  return cloneRestrictions;
}

function switchSideStateRestrictionValueToParameter(functionCalled: IFunctionGraph, side: IRestrictionValue): void {
  if (
    side.elementType == RestrictionElementType.stateVariable &&
    functionCalled.stateVariablesWritten[side.value.js]?.source.type == SourceVariableSetType.parameter
  ) {
    // como já há um clone das restrições na função mapStateRestrictionsToFunctionParameter, vamos alterar direto
    side.value.sol = functionCalled.stateVariablesWritten[side.value.js].source.name;
    side.value.js = functionCalled.stateVariablesWritten[side.value.js].source.name;
    side.elementType = RestrictionElementType.functionParameter;
  }
}
/**
 * Identifica se a {stateRestriction} possui transitividade com algum dos elementos de {restrictionTarget}.
 * Por exemplo,se a {stateRestriction} for "msg.sender == a" e existir uma "msg.sender == b" no array,
 * pode-se afirmar que "a == b".
 * Neste caso, altera a {stateRestriction} substituindo/simplificando a transitividade
 * Isto é, no exemplo acima, modificaria para msg.sender == b e retornaria TRUE
 * @param stateRestriction
 * @param restrictionTarget
 */
function treatTransitiveRestriction(stateRestriction: IRestriction, restrictionsTarget: IRestriction[]): boolean {
  if (stateRestriction.right.elementType == RestrictionElementType.stateVariable && stateRestriction.operator != '!=') {
    const transitiveRestriction = restrictionsTarget.find(
      (rt) =>
        rt.right.elementType == stateRestriction.right.elementType &&
        rt.operator == stateRestriction.operator &&
        rt.left.value.js == stateRestriction.left.value.js
    );
    if (transitiveRestriction) {
      stateRestriction.right = JSON.parse(JSON.stringify(transitiveRestriction.right));
      return true;
    }
  } else if (
    stateRestriction.left.elementType == RestrictionElementType.stateVariable &&
    stateRestriction.operator != '!='
  ) {
    const transitiveRestriction = restrictionsTarget.find(
      (rt) =>
        rt.left.elementType == stateRestriction.left.elementType &&
        rt.operator == stateRestriction.operator &&
        rt.right.value.js == stateRestriction.right.value.js
    );
    if (transitiveRestriction) {
      stateRestriction.left = JSON.parse(JSON.stringify(transitiveRestriction.left));
      return true;
    }
  }
  return false;
}
