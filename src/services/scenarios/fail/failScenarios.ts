import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { IScenario, IScenarioFail } from '../../../models/scenario/IScenario';
import { ITypeValue } from '../../../models/scenario/ITypeValue';
import { TruffleProjectIceFactoryFunction } from '../../TruffleFactory';
import { RestrictionElementType } from '../../../models/RestrictionElementType';
import { IRestriction } from '../../../models/IResctriction';
import { getFunctionSignature } from '../../TruffleProjectGraph';
import { getFailedScenarioToMatchRestrictions } from './getFailedScenarioToMatchRestrictions';
import { IFunctionCalling } from '../../../models';
import { TypeReferences } from '../../../models/scenario';
import { getFailedScenarioDescription } from './getFailedScenarioDescription';

/**
 * Percorre a função em busca de restrições impostas via 'require' e retorna um cenário em que, supostamente, passa com sucesso por todas
 essas restrições. Também retorna as respectivas restrições e cada parâmetro para que possa gerar os cenários de falha posteriormente

 * @param {TruffleProjectIceFactoryFunction} truffleProject Truffle Project analysed
 * @param {ContractDefinition} contractDefinition Optional contract definition which {functionDefinition} belongs to
 * @param {FunctionDefinition} functionDefinition FunctionDefinition extracted from the AST
 * @param {IScenario} successfulScenario Object that represents a successful scenario with it's arguments values. It will be used to have
 * @param {IRestriction[]} restrictions REstrictions found in the branch being analysed and it will be generated one faild scenario for each
 * @returns {IScenarioFail[]} A failed scenario por each Restriction
 * an
 */
export function getFailedScenariosByRestrictions(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractDefinition: ContractDefinition,
  functionDefinition: FunctionDefinition,
  successfulScenario: IScenario,
  restrictions: IRestriction[],
  references: TypeReferences
): IScenarioFail[] {
  const failScenarios: IScenarioFail[] = [];
  restrictions.map((r) => {
    const cloneSuccessParameterValues: ITypeValue[] = JSON.parse(JSON.stringify(successfulScenario.parametersValues));
    const cloneSuccessPreFunctionCallings: IFunctionCalling[] = JSON.parse(
      JSON.stringify(successfulScenario.preFunctionCallings, stringifyReplacer)
    );
    //o stringfyReplace remove o contrato para conseguir fazer a serialização
    //aqui, reatribuímos a propriedade à funcionCalled
    for (let i = 0; i < successfulScenario.preFunctionCallings.length; i++) {
      cloneSuccessPreFunctionCallings[i].functionCalled.contract =
        successfulScenario.preFunctionCallings[i].functionCalled.contract;
    }
    const cloneRestriction: IRestriction = JSON.parse(JSON.stringify(r));

    const failScenario: IScenarioFail = getFailedScenarioToMatchRestrictions(
      truffleProject,
      contractDefinition,
      functionDefinition,
      cloneSuccessPreFunctionCallings,
      cloneSuccessParameterValues,
      restrictions,
      cloneRestriction,
      references
    );
    if (failScenario != null) {
      //Quando a restrição envolve state variables, por exemplo, a descrição vem com o valor literal para o qual
      //ela foi convertida, passo necessário para geração do valor, aqui redefinimos a descrição do cenário para
      //sua abstração original: o nome da variável
      failScenario.scenarioDescription = getFailedScenarioDescription(truffleProject, functionDefinition, r);
      //Se o cenário de sucesso precisa atender a alguma restrição de transaction parameter,
      //os seus cenários de falha também deverão ser marcados assim
      if (successfulScenario.transactionParameterRestriction) failScenario.transactionParameterRestriction = true;
      failScenarios.push(failScenario);
    }
  });
  //O ganache está caindo quando o "from" era passado como "0x0000000000000000000000000000000000000000"
  //mensagem: nonce generation function failed or private key is invalid
  /*if (
          transactionAttribute != 'from' ||
          valueSuccess[transactionAttribute].js != '"0x0000000000000000000000000000000000000000"' //typeof(accounts[0]) == 'string'
        ) {*/
  //TODO: Abordar outros atributos de transação (gasPrice, por exemplo)

  return failScenarios;
}

function stringifyReplacer(key, value) {
  if (typeof value === 'object' && value !== null) {
    // não serializa o contrato
    if (key == 'contract') return;
  }
  return value;
}
