import { FunctionDefinition } from 'solidity-parser-antlr';
import { IRestriction } from '../../../models/IResctriction';
import { RestrictionElementType } from '../../../models/RestrictionElementType';
import { TruffleProjectIceFactoryFunction } from '../../TruffleFactory';
import { getFunctionSignature } from '../../TruffleProjectGraph';

export function getFailedScenarioDescription(
  truffleProject: TruffleProjectIceFactoryFunction,
  functionDefinition: FunctionDefinition,
  nRestriction: IRestriction
) {
  return `Should fail ${getFunctionSignature(
    truffleProject.getGraph(),
    functionDefinition,
    functionDefinition.isConstructor
      ? 'constructor'
      : functionDefinition.stateMutability == 'payable'
      ? 'fallback'
      : '<unamed>'
  )} when NOT comply with: ${
    nRestriction.left.elementType == RestrictionElementType.transactionParameter ? 'msg.' : ''
  }${nRestriction.left.value.js}${nRestriction.left.value.memberName ? `.${nRestriction.left.value.memberName}` : ''} ${
    nRestriction.operator
  } ${nRestriction.right.elementType == RestrictionElementType.transactionParameter ? 'msg.' : ''}${
    nRestriction.right.value.isCode ? nRestriction.right.value.js : nRestriction.right.value.sol
  }${nRestriction.right.value.memberName ? `.${nRestriction.right.value.memberName}` : ''}`;
}
