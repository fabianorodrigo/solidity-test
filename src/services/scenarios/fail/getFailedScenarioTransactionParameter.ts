import { Types, TypeValueAddress, TypeValueUint } from '../../types';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { IRestriction } from '../../../models/IResctriction';
import { ITypeValue } from '../../../models/scenario/ITypeValue';
import { TruffleProjectIceFactoryFunction } from '../../TruffleFactory';
import { RestrictionElementType } from '../../../models/RestrictionElementType';
import { ITypeValueGenerator } from '../../../models/scenario/ITypeValueGenerator';
import { getFailedScenarioDescription } from './getFailedScenarioDescription';
import { IFunctionCalling } from '../../../models';
import { IScenarioFail } from '../../../models/scenario';

export function getFailedScenarioTransactionParameter(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractDefinition: ContractDefinition,
  functionDefinition: FunctionDefinition,
  successScenarioPreFunctionCallings: IFunctionCalling[],
  successScenarioParameterValues: ITypeValue[],
  nRestriction: IRestriction
): IScenarioFail {
  const parameterOrder = successScenarioParameterValues.length - 1;
  let parameterTypeValue: ITypeValue = successScenarioParameterValues[parameterOrder];
  if (parameterTypeValue.type == null) {
    throw new Error(`parameterTypeValue.type  is null: ${JSON.stringify(parameterTypeValue)}`);
  } else if (parameterTypeValue.type == 'TransactionParameter') {
    let tp = JSON.parse(JSON.stringify(parameterTypeValue.stringfieldValue.obj));
    if (nRestriction.left.value.js == 'sender') {
      const v = (<ITypeValueGenerator>Types.address).getExtraBoundary(
        truffleProject,
        contractDefinition,
        functionDefinition,
        nRestriction,
        TypeValueAddress
      );
      tp.from = v.js;
    } else {
      const v = (<ITypeValueGenerator>Types.uint).getExtraBoundary(
        truffleProject,
        contractDefinition,
        functionDefinition,
        nRestriction,
        TypeValueUint
      );
      tp[nRestriction.left.value.js] = v.js;
    }
    parameterTypeValue.stringfieldValue = {
      isCode: false,
      js: `{${Object.keys(tp).map((k) => `${k}:${tp[k]}`)}}`,
      sol: JSON.stringify(tp),
      obj: tp,
    };
  } else {
    throw new Error(
      `Unexpected parameter type: ${parameterTypeValue.type} ${JSON.stringify(parameterTypeValue.type)}` //stringfieldValue.typeName.name
    );
  }
  successScenarioParameterValues[parameterOrder] = parameterTypeValue;
  return {
    success: false,
    transactionParameterRestriction:
      nRestriction.left.elementType == RestrictionElementType.transactionParameter ||
      nRestriction.right.elementType == RestrictionElementType.transactionParameter,
    paramNameRestriction: nRestriction.left.value.js,
    //Se for código, a restrição vai ter os atributos 'js'  e 'sol', caso contrário, será o valor direto
    scenarioDescription: getFailedScenarioDescription(truffleProject, functionDefinition, nRestriction),
    preFunctionCallings: successScenarioPreFunctionCallings,
    parametersValues: successScenarioParameterValues,
  };
}
