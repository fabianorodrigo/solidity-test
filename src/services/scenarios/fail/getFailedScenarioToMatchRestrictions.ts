import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { IRestriction } from '../../../models/IResctriction';
import { ITypeValue } from '../../../models/scenario/ITypeValue';
import { TruffleProjectIceFactoryFunction } from '../../TruffleFactory';
import { getNormalizedLiteralRestrictions, extractRealRestrictions, getEnabledElementTypes } from '../../Restrictions';
import { RestrictionElementType } from '../../../models/RestrictionElementType';
import { IScenarioFail } from '../../../models/scenario/IScenario';
import { warn } from '../../Utils/log';
import { getFailedScenarioFunctionParameter } from './getFailedScenarioFunctionParameter';
import { getFailedScenarioTransactionParameter } from './getFailedScenarioTransactionParameter';
import { getFailedScenarioStateVariable } from './getFailedScenarioStateVariable';
import { IFunctionCalling } from '../../../models';
import { TypeReferences } from '../../../models/scenario';
import { getReferenceKey } from '../getReferenceKey';
import { IContractGraph } from '../../../models/graph';
import { getFailedScenarioDescription } from './getFailedScenarioDescription';
import { getFunctionParametersValues } from '../getFunctionParametersValues';
import { getParametersValues, getRandomValue } from '../../Utils';

export function getFailedScenarioToMatchRestrictions(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractDefinition: ContractDefinition,
  functionDefinition: FunctionDefinition,
  successScenarioPreFunctionCallings: IFunctionCalling[],
  successScenarioParameterValues: ITypeValue[],
  restrictions: IRestriction[],
  restriction: IRestriction,
  references: TypeReferences
): IScenarioFail {
  //atualizando references para os parâmetros que ainda não forem setados (estão null)
  updateReferenceWithSucessScenarioLiteral(
    restriction,
    truffleProject.getGraph().contracts[contractDefinition.name],
    functionDefinition,
    successScenarioParameterValues,
    references
  );
  //Normalizando a restrição e atribuindo à restrição os valores literais presentes no references
  const nRestrictions = getNormalizedLiteralRestrictions([JSON.parse(JSON.stringify(restriction))]);
  const realRestrictions = extractRealRestrictions(
    truffleProject.getGraph().contracts[contractDefinition.name],
    nRestrictions,
    references
  );
  if (realRestrictions.length == 0) return null;

  const realRestriction = realRestrictions[0];
  //Ajustando a restrição substituindo valor de parâmetro pelo valor que lhe foi dado
  if (
    [RestrictionElementType.stateVariable, RestrictionElementType.stateVariableMember].includes(
      realRestriction.left.elementType
    ) ||
    [RestrictionElementType.stateVariable, RestrictionElementType.stateVariableMember].includes(
      realRestriction.right.elementType
    )
  ) {
    return getFailedScenarioStateVariable(
      truffleProject,
      contractDefinition,
      functionDefinition,
      successScenarioPreFunctionCallings,
      successScenarioParameterValues,
      realRestriction,
      references
    );
  } else if (
    realRestriction.left.elementType == RestrictionElementType.functionParameter ||
    realRestriction.left.elementType == RestrictionElementType.functionParameterMember
  ) {
    return getFailedScenarioFunctionParameter(
      truffleProject,
      contractDefinition,
      functionDefinition,
      successScenarioPreFunctionCallings,
      successScenarioParameterValues,
      realRestriction
    );
  } else if (
    realRestriction.left.elementType == RestrictionElementType.transactionParameter ||
    realRestriction.right.elementType == RestrictionElementType.transactionParameter
  ) {
    return getFailedScenarioTransactionParameter(
      truffleProject,
      contractDefinition,
      functionDefinition,
      successScenarioPreFunctionCallings,
      successScenarioParameterValues,
      realRestriction
    );
  } else {
    warn(
      `${contractDefinition.name}.${functionDefinition.name}: nRestriction.left.elementType does not refer to stateVariable, functionParameter or transactionParameter:`,
      realRestriction.left.elementType
    );
    return null;
  }
}

function _deprecated_getUnhandledRestrictionFailedScenario(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractDefinition: ContractDefinition,
  functionDefinition: FunctionDefinition,
  restrictions: IRestriction[],
  targetNormalizedRestriction: IRestriction,
  successScenarioPreFunctionCallings: IFunctionCalling[],
  references: TypeReferences
) {
  return {
    success: false,
    transactionParameterRestriction:
      targetNormalizedRestriction.left.elementType == RestrictionElementType.transactionParameter ||
      targetNormalizedRestriction.right.elementType == RestrictionElementType.transactionParameter,
    paramNameRestriction: targetNormalizedRestriction.left.value.js,
    //Se for código, a restrição vai ter os atributos 'js'  e 'sol', caso contrário, será o valor direto
    scenarioDescription: getFailedScenarioDescription(truffleProject, functionDefinition, targetNormalizedRestriction),
    preFunctionCallings: successScenarioPreFunctionCallings,
    parametersValues: getFunctionParametersValues(
      truffleProject,
      contractDefinition,
      functionDefinition,
      restrictions,
      references,
      getParametersValues
    ),
  };
}

function updateReferenceWithSucessScenarioLiteral(
  nr: IRestriction,
  contract: IContractGraph,
  functionDefinition: FunctionDefinition,
  successScenarioParameterValues: ITypeValue[],
  references: TypeReferences
) {
  if (
    [RestrictionElementType.functionParameter, RestrictionElementType.functionParameterMember].includes(
      nr.right.elementType
    ) &&
    references[getReferenceKey(contract, nr.right)] == null
  ) {
    const parameterRightSideRestrictionOrder = functionDefinition.parameters.findIndex(
      (p) => p.name == nr.right.value.js
    );
    references[getReferenceKey(contract, nr.right)] =
      successScenarioParameterValues[parameterRightSideRestrictionOrder];
  } else if (
    nr.right.elementType == RestrictionElementType.transactionParameter &&
    references['transactionParameter'] == null
  ) {
    const i = successScenarioParameterValues.length - 1;
    const prop = nr.right.value.js == 'sender' ? 'from' : nr.right.value.js;
    references['transactionParameter'] = successScenarioParameterValues[i];
  }
}
