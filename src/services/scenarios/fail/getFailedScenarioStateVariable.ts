import { Types } from '../../types';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { IRestriction } from '../../../models/IResctriction';
import { ITypeValue } from '../../../models/scenario/ITypeValue';
import { TruffleProjectIceFactoryFunction } from '../../TruffleFactory';
import { RestrictionElementType } from '../../../models/RestrictionElementType';
import { ITypeValueGenerator } from '../../../models/scenario/ITypeValueGenerator';
import { getFailedScenarioDescription } from './getFailedScenarioDescription';
import { IFunctionCalling } from '../../../models';
import { getPreFunctionCallings } from '../preCallings/getPreFunctionCallings';
import { invertRestriction } from '../../Restrictions';
import { warn, bigIntSerializer } from '../../Utils';
import { IScenarioFail, TypeReferences } from '../../../models/scenario';
import { isElementTypeCovered } from '../preCallings/filterNotFilledStateRestrictions';
import { getReferenceKey } from '../getReferenceKey';

export function getFailedScenarioStateVariable(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractDefinition: ContractDefinition,
  functionDefinition: FunctionDefinition,
  successScenarioPreFunctionCallings: IFunctionCalling[],
  successScenarioParameterValues: ITypeValue[],
  nRestriction: IRestriction,
  references: TypeReferences
): IScenarioFail {
  const cloneReferences = JSON.parse(JSON.stringify(references, bigIntSerializer));
  //se chegou aqui, ou left ou right da nRestriction é uma State Variable
  if (nRestriction.left.elementType == RestrictionElementType.stateVariable) {
    delete cloneReferences[
      getReferenceKey(truffleProject.getGraph().contracts[contractDefinition.name], nRestriction.left)
    ];
  } else if (nRestriction.right.elementType == RestrictionElementType.stateVariable) {
    delete cloneReferences[
      getReferenceKey(truffleProject.getGraph().contracts[contractDefinition.name], nRestriction.right)
    ];
  } else if (nRestriction.left.elementType == RestrictionElementType.stateVariableMember) {
    //se for um array, não dá pra deletar sua propriedade length, então,
    //pega o valor inicial do contrato do objeto global SE EXISTIR
    if (
      nRestriction.left.value.memberName == 'length' &&
      cloneReferences[
        getReferenceKey(truffleProject.getGraph().contracts[contractDefinition.name], nRestriction.left)
      ] != null &&
      Array.isArray(
        cloneReferences[
          getReferenceKey(truffleProject.getGraph().contracts[contractDefinition.name], nRestriction.left)
        ].stringfieldValue.obj
      )
    ) {
      if (
        global['defaultReferences'][contractDefinition.name][
          getReferenceKey(truffleProject.getGraph().contracts[contractDefinition.name], nRestriction.left)
        ] != null
      ) {
        cloneReferences[
          getReferenceKey(truffleProject.getGraph().contracts[contractDefinition.name], nRestriction.left)
        ].stringfieldValue = JSON.parse(
          JSON.stringify(
            global['defaultReferences'][contractDefinition.name][
              getReferenceKey(truffleProject.getGraph().contracts[contractDefinition.name], nRestriction.left)
            ].stringfieldValue
          )
        );
      } else {
        delete cloneReferences[
          getReferenceKey(truffleProject.getGraph().contracts[contractDefinition.name], nRestriction.left)
        ];
      }
    } else if (
      cloneReferences[
        getReferenceKey(truffleProject.getGraph().contracts[contractDefinition.name], nRestriction.left)
      ] != null
    ) {
      delete cloneReferences[
        getReferenceKey(truffleProject.getGraph().contracts[contractDefinition.name], nRestriction.left)
      ].stringfieldValue.obj[nRestriction.left.value.memberName];
    }
  } else if (nRestriction.right.elementType == RestrictionElementType.stateVariableMember) {
    if (
      nRestriction.right.value.memberName == 'length' &&
      cloneReferences[
        getReferenceKey(truffleProject.getGraph().contracts[contractDefinition.name], nRestriction.right)
      ] != null &&
      Array.isArray(
        cloneReferences[
          getReferenceKey(truffleProject.getGraph().contracts[contractDefinition.name], nRestriction.right)
        ].stringfieldValue.obj
      )
    ) {
      if (
        global['defaultReferences'][contractDefinition.name][
          getReferenceKey(truffleProject.getGraph().contracts[contractDefinition.name], nRestriction.right)
        ] != null
      ) {
        cloneReferences[
          getReferenceKey(truffleProject.getGraph().contracts[contractDefinition.name], nRestriction.right)
        ].stringfieldValue = JSON.parse(
          JSON.stringify(
            global['defaultReferences'][contractDefinition.name][
              getReferenceKey(truffleProject.getGraph().contracts[contractDefinition.name], nRestriction.right)
            ].stringfieldValue
          )
        );
      } else {
        delete cloneReferences[
          getReferenceKey(truffleProject.getGraph().contracts[contractDefinition.name], nRestriction.right)
        ];
      }
    } else if (
      cloneReferences[
        getReferenceKey(truffleProject.getGraph().contracts[contractDefinition.name], nRestriction.right)
      ] != null
    ) {
      delete cloneReferences[
        getReferenceKey(truffleProject.getGraph().contracts[contractDefinition.name], nRestriction.right)
      ].stringfieldValue.obj[nRestriction.right.value.memberName];
    }
  }

  //Chama a getPreFunctionCalling passando a restrição analisada invertida
  const preFunctionCallings = getPreFunctionCallings(
    truffleProject,
    contractDefinition,
    functionDefinition,
    [invertRestriction(nRestriction)],
    cloneReferences
  );
  //Apenas se encontrar função que atualiza o state citado na nRestriction, senão, não tem jeito
  if (preFunctionCallings.length > 0) {
    if (preFunctionCallings.length > 1) {
      warn(`getFailedScenarioStateVariable.getPreFunctionCallings retornou ${preFunctionCallings.length} chamadas`);
    }
    const index = successScenarioPreFunctionCallings.findIndex(
      (spfc) => spfc.functionCalled.signature == preFunctionCallings[0].functionCalled.signature
    );
    //Dado que os state variables têm valores iniciais setados.
    //determinada condição pode fazer que o cenário de sucesso
    //não precise chamar uma função pra setar o estado. Neste caso,
    //faz-se o push ao invés de atribuição
    if (index > -1) {
      successScenarioPreFunctionCallings[index] = preFunctionCallings[0];
    } else {
      preFunctionCallings.forEach((pfc) => {
        successScenarioPreFunctionCallings.push(pfc);
      });
    }
  } else {
    warn(
      `${contractDefinition.name}.${
        functionDefinition.name
      }: getFailedScenarioStateVariable.getPreFunctionCallings não retornou chamadas. Não encontrada função que altere a state: ${JSON.stringify(
        nRestriction
      )}`
    );
    const filteredPreCallings = successScenarioPreFunctionCallings.filter(
      (spfc) =>
        spfc.stateVariablesAffected.length > 1 ||
        (!spfc.stateVariablesAffected.find(
          (sv) => sv.name == nRestriction.left.value.js && isElementTypeCovered(nRestriction.left)
        ) &&
          !spfc.stateVariablesAffected.find(
            (sv) => sv.name == nRestriction.right.value.js && isElementTypeCovered(nRestriction.right)
          ))
    );
    if (filteredPreCallings.length == successScenarioPreFunctionCallings.length) {
      return null;
    } else {
      successScenarioPreFunctionCallings = filteredPreCallings;
    }
  }
  return {
    success: false,
    transactionParameterRestriction:
      nRestriction.left.elementType == RestrictionElementType.transactionParameter ||
      nRestriction.right.elementType == RestrictionElementType.transactionParameter,
    paramNameRestriction: null,
    //Se for código, a restrição vai ter os atributos 'js'  e 'sol', caso contrário, será o valor direto
    scenarioDescription: getFailedScenarioDescription(truffleProject, functionDefinition, nRestriction),
    preFunctionCallings: successScenarioPreFunctionCallings,
    parametersValues: successScenarioParameterValues,
  };
}
