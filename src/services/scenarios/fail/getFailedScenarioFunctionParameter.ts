import { Types } from '../../types';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { IRestriction } from '../../../models/IResctriction';
import { ITypeValue } from '../../../models/scenario/ITypeValue';
import { TruffleProjectIceFactoryFunction } from '../../TruffleFactory';
import { RestrictionElementType } from '../../../models/RestrictionElementType';
import { ITypeValueGenerator } from '../../../models/scenario/ITypeValueGenerator';
import { getFailedScenarioDescription } from './getFailedScenarioDescription';
import { IFunctionCalling } from '../../../models';
export function getFailedScenarioFunctionParameter(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractDefinition: ContractDefinition,
  functionDefinition: FunctionDefinition,
  successScenarioPreFunctionCallings: IFunctionCalling[],
  successScenarioParameterValues: ITypeValue[],
  nRestriction: IRestriction
) {
  const parameterOrder = functionDefinition.parameters.findIndex((p) => p.name == nRestriction.left.value.js);
  if (parameterOrder == -1) {
    throw new Error(
      `nRestriction.left.value.js não encontrado entre o parâmetros da função '${functionDefinition.name}': ${nRestriction.left.value.js}`
    );
  }
  let parameterTypeValue: ITypeValue = successScenarioParameterValues[parameterOrder];
  if (Types[parameterTypeValue.type]) {
    parameterTypeValue.stringfieldValue = (<ITypeValueGenerator>Types[parameterTypeValue.type]).getExtraBoundary(
      truffleProject,
      contractDefinition,
      functionDefinition,
      nRestriction,
      parameterTypeValue
    );
  } else {
    throw new Error(
      `Unexpected parameter type: ${parameterTypeValue.type} ${JSON.stringify(parameterTypeValue.type)}` //stringfieldValue.typeName.name
    );
  }
  successScenarioParameterValues[parameterOrder] = parameterTypeValue;
  return {
    success: false,
    preFunctionCallings: successScenarioPreFunctionCallings,
    transactionParameterRestriction:
      nRestriction.left.elementType == RestrictionElementType.transactionParameter ||
      nRestriction.right.elementType == RestrictionElementType.transactionParameter,
    paramNameRestriction: nRestriction.left.value.js,
    //Se for código, a restrição vai ter os atributos 'js'  e 'sol', caso contrário, será o valor direto
    scenarioDescription: getFailedScenarioDescription(truffleProject, functionDefinition, nRestriction),
    parametersValues: successScenarioParameterValues,
  };
}
