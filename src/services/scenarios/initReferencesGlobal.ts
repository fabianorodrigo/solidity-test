import { IContractGraph } from '../../models/graph';
import { TransactionParameterType } from '../Utils';
import { initReferences } from './initReferences';

/**
 * Inicializa os valores de referência globais para o contrato passado
 *
 * @param contractGraph contrato que terá seus valores globais inicializados
 */
export function initReferencesGlobal(contractGraph: IContractGraph): void {
  if (global['defaultReferences'] == null) global['defaultReferences'] = {};
  global['defaultReferences'][contractGraph.name] = {};
  global['defaultReferences'][contractGraph.name]['transactionParameter'] = {
    type: TransactionParameterType,
    stringfieldValue: {},
  };
  initReferences(contractGraph, global['defaultReferences'][contractGraph.name]);
  global['defaultReferences'][contractGraph.name]['transactionParameter'].stringfieldValue.js = `{from: accounts[0]}`;
  global['defaultReferences'][contractGraph.name]['transactionParameter'].stringfieldValue.obj = {
    from: 'accounts[0]',
  };
}
