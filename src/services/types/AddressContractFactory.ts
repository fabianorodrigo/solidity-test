import { IValue } from '../../models/IValue';
import { IRestriction } from '../../models/IResctriction';
import { ContractDefinition, FunctionDefinition, TypeName } from 'solidity-parser-antlr';

import { ITypeValueGenerator } from '../../models/scenario/ITypeValueGenerator';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { ITypeValue } from '../../models/scenario';
import { getFunctionParametersValuesScenarios } from '../scenarios';

/** *
 * Address auxiliar functionalities
 * */

/** *
 * It's going to create a instance of an immutable object with functions
 * to create or select random contract's address values
 *
 * @returns {object} Object with Address functionalities
 * */
export function AddressContractFactory(): ITypeValueGenerator {
  return Object.freeze({
    random,
    getExtraBoundary,
  });

  /**
   * Return a value in the pool in a random position or, return random position in the accounts of ganache.
   * In both cases, must obbey the restrictions
   *
   * @param {object} truffleProject Instance of Truffle Project under analysis
   * @param {*} restrictions Restrictions that need to be respected in order to select a value
   */
  function random(truffleProject, restrictions: IRestriction[] = []): IValue {
    let idx = 0;
    if (restrictions.length > 1) {
      idx = Math.floor(Math.random() * restrictions.length);
    }

    return {
      isCode: false,
      js: `contract${restrictions[idx].right.value.obj}.address`,
      sol: `contract${restrictions[idx].right.value.obj}.address`,
      obj: `"${restrictions[idx].right.value.obj}"`,
    };
  }

  /**
   * Generate a value that is not in compliance with the {restriction}
   *
   * @param {object} truffleProject Instance of Truffle Project under analysis
   * @param {contractDefinition} contractDefinition
   * @param {IRestriction} restriction Restriction that will be transgressed
   */
  function getExtraBoundary(
    truffleProject: TruffleProjectIceFactoryFunction,
    contractDefinition: ContractDefinition,
    functionDefinition: FunctionDefinition,
    restriction: IRestriction,
    typeValue: ITypeValue
  ): IValue {
    //TODO: Pensar em como colocar valores válidos. Ao invés de retornar uma string inválida com essas concatenações
    let value = null;
    if (restriction.operator == '==' || restriction.operator == '<=') {
      const v = restriction.right.value.js;
      value = v.startsWith('accounts[9') ? 'accounts[8]' : 'accounts[9]';
    } else if (restriction.operator == '!=' || restriction.operator == '>' || restriction.operator == '<') {
      value = restriction.right.value.obj;
    } else if (restriction.operator == '>=') {
      value = restriction.right.value.js.substring(0, restriction.right.value.js.length - 2);
    }
    return {
      isCode: false,
      //o valor 0x0 no teste javascript tem que ser passado como "0x0000000000000000000000000000000000000000". Se passar "0x0", vai dizer que não é tipo address
      js:
        value == '0x0'
          ? `"0x0000000000000000000000000000000000000000"`
          : value.startsWith('"') || value.startsWith('accounts[')
          ? value
          : `"${value}"`,
      sol: value,
      obj: value,
      typeName: typeValue?.stringfieldValue?.typeName,
    };
  }
}

function isAddressZero(value: string) {
  return [
    '0x0',
    '"0x0"',
    '"0x0000000000000000000000000000000000000000"',
    '0x0000000000000000000000000000000000000000',
  ].includes(value);
}
