import { IRestriction } from '../../models/IResctriction';
import colors from 'colors';
import { IValue } from '../../models/IValue';
import { ContractDefinition, FunctionDefinition, TypeName } from 'solidity-parser-antlr';
import fromExponential from 'from-exponential';

import { fillsRestrictions, getNormalizedRestrictions } from '../Restrictions';
import { ITypeValueGenerator } from '../../models/scenario/ITypeValueGenerator';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { RestrictionElementType } from '../../models/RestrictionElementType';
import { warn } from '../Utils';
import { IContractGraph } from '../../models/graph';
import { ITypeValue } from '../../models/scenario';

/** *
 * It's going to create a instance of an immutable object with functions
 * to create or select random integer values
 *
 * @param {boolean} unsigned Indicates if is a unsigned integer. If true, negative values are not allowed
 * @param {number} size The maximum number of bytes allowed
 * @returns {object} Object with Integer functionalities
 * */
export function IntegerFactory(unsigned: boolean, size: number): ITypeValueGenerator {
  return Object.freeze({
    random,
    getNewInteger,
    getExtraBoundary,
  });

  /**
   * Return a value in the pool in a random position or, exceptionally return another random value.
   * In both cases, must obbey the restrictions
   *
   * @param {object} truffleProject Instance of Truffle Project under analysis
   * @param {IRestriction[]} restrictions Restrictions that need to be respected in order to select a value
   */
  function random(
    truffleProject: TruffleProjectIceFactoryFunction,
    restrictions: IRestriction[] = [],
    typeName: TypeName,
    contract: ContractDefinition | IContractGraph
  ): IValue {
    if (
      truffleProject.getValuesPool().NumberLiteral &&
      truffleProject.getValuesPool().NumberLiteral.indexOf('NaN') > -1
    ) {
      console.warn('NaN encontrado no pool', truffleProject.getValuesPool().NumberLiteral);
    }
    let value = null;
    let contaTentativas = 0;
    let sequentialIndex = 0;
    let arrayCompatibleSize =
      truffleProject.getValuesPool().NumberLiteral && truffleProject.getValuesPool().NumberLiteral.length > 0
        ? truffleProject
            .getValuesPool()
            .NumberLiteral.map((v) => parseInt(v))
            .filter((v) => v < 2 ** size && v < Number.MAX_SAFE_INTEGER)
        : []; //Javascript suporta até 2^53 -1.
    //Filtrando restrições que demandam igualdade com valores literais
    const restEqualities = restrictions.filter(
      (r) =>
        r.operator == '==' &&
        (r.right.elementType == RestrictionElementType.Literal ||
          r.right.elementType == RestrictionElementType.EnumMember)
    );
    const restCode = restrictions.filter((r) => r.right.value.isCode);

    //Se alguma restrição tem sinal de maior / maior ou igual, filtra só com valores maiores igual ao solicitado
    //MAS ISTO É RESTRITO A RESTRIÇÕES EM QUE O LADO DIREITO É DO TIPO LITERAL
    const restrictionsGreater = restrictions.filter(
      (r) =>
        ['>', '>='].includes(r.operator) &&
        (r.right.elementType == RestrictionElementType.Literal ||
          r.right.elementType == RestrictionElementType.EnumMember)
    );
    if (restrictionsGreater.length > 0) {
      arrayCompatibleSize = arrayCompatibleSize.filter((v) => restrictionsGreater.some((r) => r.right.value.obj <= v));
      //Se tinha sinal de maior ou maior ou igual restou um array de tamanho zero, adiciona o maior número + 1
      if (arrayCompatibleSize.length == 0) {
        arrayCompatibleSize.push(
          restrictionsGreater
            .map((r) => {
              return r.right.value.obj;
            })
            .reduce(function (a, b) {
              return Math.max(a, b);
            }) + 2 //tinha colocado "+1" mas não voltava inalterado e com o "+2" funcionou ... não sei explicar
        );
      }
    }
    //Se alguma restrição tem sinal de menor / menor ou igual, filtra só com valores menores igual ao solicitado
    //MAS ISTO É RESTRITO A RESTRIÇÕES EM QUE O LADO DIREITO É DO TIPO LITERAL
    const restrictionsLesser = restrictions.filter(
      (r) =>
        ['<', '<='].includes(r.operator) &&
        (r.right.elementType == RestrictionElementType.Literal ||
          r.right.elementType == RestrictionElementType.EnumMember)
    );
    if (restrictionsLesser.length > 0) {
      //TODO: se tiver sinal de maior e menor aqui não vai sobrepor o que foi atribuído dentro do IF restrictionGreater.length > 0 ???
      arrayCompatibleSize = arrayCompatibleSize.filter((v) => restrictionsLesser.some((r) => r.right.value.obj >= v));
      //Se tinha sinal de menor ou menor ou igual restou um array de tamanho zero, adiciona o maior número -1
      if (arrayCompatibleSize.length == 0) {
        arrayCompatibleSize.push(
          restrictionsLesser
            .map((r) => {
              return r.right.value.obj;
            })
            .reduce(function (a, b) {
              return Math.min(a, b);
            }) - 1
        );
      }
    }
    if (arrayCompatibleSize.includes(NaN)) {
      throw new Error(`NaN encontrado no array de valores compatíveis: ${contract.name}`);
    }
    do {
      value = null;
      //if there is a restriction with equality, them returns this value OR it will be hard to fillRestrictions randomically
      if (restEqualities.length > 0) {
        if (restEqualities.length == 1) {
          value = restEqualities[0].right.value.js;
        } else {
          console.warn(
            colors
              .red(truffleProject.truffleProjectHome)
              .concat(` has more than one equality restriction: `, JSON.stringify(restEqualities))
          );
          value = restEqualities[Math.floor(Math.random() * (restEqualities.length + 0.49))].right.value.js;
        }
      }
      //if there is a restriction that is code, then returns the code directly
      //without even call 'fillRestrictions' function
      else if (restCode.length > 0) {
        return getCodeValue(truffleProject, restCode[0]);
      }
      // If there is number literals in the pool, return one of them
      else if (arrayCompatibleSize.length > 0) {
        let index = null;
        contaTentativas++;
        //Se o número de tentativas superou o tamanho do pool, começa a percorrê-lo sequencialmente
        if (contaTentativas > arrayCompatibleSize.length) {
          index = sequentialIndex;
          sequentialIndex++;
          warn(
            'Tentativa de geração de valor randômico superou o tamanho do arrayCompatibleSize',
            contaTentativas.toString()
          );
        } else {
          index = Math.floor(
            Math.random() * (arrayCompatibleSize.length + 1) // +1 to have a chance to get a value out of bounds
          );
        }
        if (index < arrayCompatibleSize.length) {
          value = arrayCompatibleSize[index];
        }
      }
      if (truffleProject.getValuesPool().NumberLiteral == null) {
        truffleProject.getValuesPool().NumberLiteral = [];
      }
      //If not found in the pool
      if (value == null) {
        value = getNewInteger(arrayCompatibleSize, restrictions);
        if (
          truffleProject.getValuesPool().NumberLiteral.indexOf(value) == -1 &&
          (typeof value == 'bigint' || !isNaN(value))
        ) {
          truffleProject.getValuesPool().NumberLiteral.push(value);
          arrayCompatibleSize.push(value);
        }
      }
    } while (!fillsRestrictions(value, restrictions, true) || (value < 0 && unsigned)); //se unsigned integer, não pode ter negativo
    if (value == null) {
      console.error('truffleProject.valuesPool', JSON.stringify(truffleProject.getValuesPool()));
      console.error('restrictions', JSON.stringify(restrictions));
      console.error('getNewInteger', getNewInteger(truffleProject.getValuesPool().NumberLiteral, restrictions));
      throw new Error('Valor null');
    }

    if (typeof value != 'bigint' && isNaN(value)) {
      throw new Error(
        `Valor NaN: ${truffleProject.truffleProjectHome} ${contract.name} - ${JSON.stringify(restrictions)}`
      );
    }
    //Números grandes estão gerando a exceção provavelmente por esperar BigNumber:
    //Error: invalid number value (arg="initialExchangeDate", coderType="uint256", value=1000000000000000000)
    //Para esses casos vamos passar sua representação hexadecimal com aspas
    /* if (
      (typeof value == 'number' || typeof value == 'bigint') &&
      (value > Number.MAX_SAFE_INTEGER || value < Number.MIN_SAFE_INTEGER)
    ) {
      //value = `"${parseInt(value).toString(16)}"`;
      value = `0x${value.toString(16)}`;
    }*/
    return {
      isCode: false,
      js:
        value > Number.MAX_SAFE_INTEGER || value < Number.MIN_SAFE_INTEGER
          ? `"0x${value.toString(16)}"`
          : value.toString() /*(value.length >= 8 && !value.startsWith('"')) || value.toString().startsWith('0x')
          ? `"${value}"`
          : value.toString()*/,
      sol:
        value > Number.MAX_SAFE_INTEGER || value < Number.MIN_SAFE_INTEGER
          ? `0x${value.toString(16)}`
          : value.toString(),
      obj: typeof value == 'string' ? parseInt(value) : value,
    };
  }

  function getNewBigInt(arrayCompatibleSize: bigint[], restrictions: IRestriction[]): bigint {
    for (let i = 0; i < restrictions.length; i++) {
      let intValueRestriction = BigInt(restrictions[i].right.value.obj);
      if (arrayCompatibleSize.indexOf(intValueRestriction) == -1) {
        return intValueRestriction;
      } else if (arrayCompatibleSize.indexOf(intValueRestriction + BigInt(1)) == -1) {
        return intValueRestriction + BigInt(1);
      } else if (arrayCompatibleSize.indexOf(intValueRestriction - BigInt(1)) == -1) {
        return intValueRestriction - BigInt(1);
      }
    }
  }

  function getNewInteger(arrayCompatibleSize: number[], restrictions: IRestriction[]): number | bigint {
    //If has restrictions, use it to generate values
    if (restrictions.length > 0) {
      for (let i = 0; i < restrictions.length; i++) {
        let intValueRestriction = restrictions[i].right.value.obj;
        if (arrayCompatibleSize.indexOf(restrictions[i].right.value.obj) == -1 && intValueRestriction < 2 ** size) {
          return restrictions[i].right.value.obj;
        } else if (arrayCompatibleSize.indexOf(intValueRestriction + 1) == -1 && intValueRestriction < 2 ** size) {
          return intValueRestriction + 1;
        } else if (arrayCompatibleSize.indexOf(intValueRestriction - 1) == -1 && intValueRestriction < 2 ** size) {
          return intValueRestriction - 1;
        }
        if (intValueRestriction > Number.MAX_SAFE_INTEGER) {
          let v = getNewBigInt(
            arrayCompatibleSize.map((v) => BigInt(v)),
            restrictions
          );
          if (v) return v;
        }
      }
    }
    if (arrayCompatibleSize.length > 0) {
      const randomico = Math.random();
      let position = Math.floor(randomico * arrayCompatibleSize.length);
      //existe a possibilidade de Math.random retornar "1", neste caso,
      //a posição do array ficaria com sendo exatamente seu tamanho, o que gera um "out of bounds"
      if (position >= arrayCompatibleSize.length) {
        position = arrayCompatibleSize.length - 1;
      }
      const retorno = arrayCompatibleSize[position];
      if (retorno == null) {
        console.log('randomico', randomico);
        console.log('length', arrayCompatibleSize.length);
        console.log('position', position);
        console.log('retorno', retorno);
      }
      return retorno;
    }
    //Caso hajam literais coletadas no código,
    // vamos avaliar como fica se colocarmos como limite máximo o dobro do maior número do pool
    let max =
      arrayCompatibleSize.length > 0
        ? Math.max.apply(
            null,
            arrayCompatibleSize.map((v) => {
              return v;
            })
          )
        : 2 ** size;
    const retorno = fromExponential(Math.random() * max).split('.')[0];
    return retorno;
  }

  /**
   * Generate a value that is not in compliance with the {restriction}
   *
   * @param {object} truffleProject Instance of Truffle Project under analysis
   * @param {*} restriction Restriction that will be transgressed
   */
  function getExtraBoundary(
    truffleProject: TruffleProjectIceFactoryFunction,
    contractDefinition: ContractDefinition,
    functionDefinition: FunctionDefinition,
    restriction: IRestriction,
    typeValue: ITypeValue
  ): IValue {
    let value = null;
    if (restriction.operator == '==' || restriction.operator == '<=') {
      if (restriction.right.value.isCode) {
        return {
          isCode: true,
          js: restriction.right.value.js.concat('+1'),
          sol: restriction.right.value.js.concat('+1'),
          obj: parseInt(restriction.right.value.js) + 1,
          typeName: typeValue?.stringfieldValue?.typeName,
        };
      }
      if (typeof restriction.right.value.obj == 'bigint') {
        value = restriction.right.value.obj + BigInt(1);
      } else {
        value = restriction.right.value.obj + 1;
      }
    } else if (restriction.operator == '!=' || restriction.operator == '>' || restriction.operator == '<') {
      if (restriction.right.value.isCode) return restriction.right.value;
      value = restriction.right.value.obj;
    } else if (restriction.operator == '>=') {
      if (restriction.right.value.isCode) {
        return {
          isCode: true,
          js: restriction.right.value.js.concat('-1'),
          sol: restriction.right.value.js.concat('-1'),
          obj: restriction.right.value.obj - 1,
          typeName: typeValue?.stringfieldValue?.typeName,
        };
      }
      if (typeof restriction.right.value.obj == 'bigint') {
        value = restriction.right.value.obj - BigInt(1);
      } else {
        value = restriction.right.value.obj - 1;
      }
    }
    return {
      isCode: false,
      js: value,
      sol: value,
      obj: parseInt(value),
      typeName: typeValue?.stringfieldValue?.typeName,
    };
  }

  /**
   * Generate a value that has a code (js and solidity) based on restriction received
   *
   * @param {object} truffleProject Instance of Truffle Project under analysis
   * @param {*} restriction Restriction under analysis
   * @param {object} stringfieldValue The original supposed to be a valid value (para o tipo inteiro não faz diferença)
   * @returns {object} A object with attributes 'js' and 'sol' that attends the restriction
   */
  function getCodeValue(truffleProject, restriction: IRestriction): IValue {
    let value = JSON.parse(JSON.stringify(restriction.right.value)); //clone
    if (restriction.operator == '==') {
      return value;
    } else if (restriction.operator == '!=' || restriction.operator == '>' || restriction.operator == '>=') {
      const delta = Math.floor(Math.random() * 1000);
      value.js += `+${delta}`;
      value.sol += `+${delta}`;
    } else if (restriction.operator == '<=' || restriction.operator == '<') {
      const delta = Math.floor(Math.random() * 1000);
      value.js += `-${delta}`;
      value.sol += `-${delta}`;
    }
    return value;
  }
}
