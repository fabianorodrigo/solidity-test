import { ITypeValue } from '../../models/scenario';

export const TypeValueUint: ITypeValue = {
  type: 'uint',
  subtype: null,
  stringfieldValue: {
    isCode: false,
    js: null,
    sol: null,
    obj: null,
    typeName: {
      type: 'ElementaryTypeName',
      name: 'uint',
    },
  },
};
