export const ElementTypeUINT: { type: 'ElementaryTypeName'; name: string } = {
  type: 'ElementaryTypeName',
  name: 'uint',
};
