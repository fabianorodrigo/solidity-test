import { ITypeValue } from '../../models/scenario';

export const TypeValueAddress: ITypeValue = {
  type: 'address',
  subtype: null,
  stringfieldValue: {
    isCode: false,
    js: null,
    sol: null,
    obj: null,
    typeName: {
      type: 'ElementaryTypeName',
      name: 'address',
    },
  },
};
