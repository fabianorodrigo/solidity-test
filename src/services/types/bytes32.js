const web3 = require('web3');

/** *
 * Address auxiliar functionalities
 * */
module.exports = Object.freeze({
  /**
   * Return a value in the pool in a random position or, return random value
   * In both cases, must obbey the restrictions
   *
   * @param {object} truffleProject Instance of Truffle Project under analysis
   * @param {*} restrictions Restrictions that need to be respected in order to select a value
   */
  random: function randomBytes(truffleProject, restrictions = []) {
    //if there is a restriction with equality, them returns this value OR it will be hard to fillRestrictions randomically
    const restEqualities = restrictions.filter(r => r.operator == '==');
    if (restEqualities.length > 0) {
      value =
        restEqualities.length == 1
          ? restEqualities[0].value
          : restEqualities[Math.floor(Math.random() * (restEqualities.length + 1))].right.value.js;
    }
    // If there is bytes literals in the pool, return one of them
    else if (truffleProject.getValuesPool().BytesLiteral && truffleProject.getValuesPool().BytesLiteral.length > 0) {
      const index = Math.floor(Math.random() * truffleProject.getValuesPool().BytesLiteral.length + 1); // +1 to have a chance to get a value out of bounds
      if (index < truffleProject.getValuesPool().BytesLiteral.length) {
        return {
          js: `[${truffleProject.getValuesPool().BytesLiteral[index]}]`,
          sol: `bytes32("${truffleProject.getValuesPool().BytesLiteral[index]}")`,
        };
      }
    } else if (truffleProject.getValuesPool().BytesLiteral == null) {
      truffleProject.getValuesPool().BytesLiteral = [];
    }
    // You can pass bytes parameters in Remix or browser-solidity as the array of single bytes, for example ["0x00","0xaa", "0xff"] is equivalent to "0x00aaff" https://ethereum.stackexchange.com/questions/13483/how-to-pass-arbitrary-bytes-to-a-function-in-remix
    const stringHex = randHex(Math.ceil(Math.random() * 16));
    truffleProject.getValuesPool().BytesLiteral.push(`${web3.utils.hexToBytes(`0x${stringHex}`)}`);
    return {
      js: `[${truffleProject.getValuesPool().BytesLiteral[truffleProject.getValuesPool().BytesLiteral.length - 1]}]`,
      sol: `bytes32("${
        truffleProject.getValuesPool().BytesLiteral[truffleProject.getValuesPool().BytesLiteral.length - 1]
      }")`,
    };
  },
});

//  random hex string generator
function randHex(len) {
  var maxlen = 4,
    min = Math.pow(16, Math.min(len, maxlen) - 1);
  (max = Math.pow(16, Math.min(len, maxlen)) - 1),
    (n = Math.floor(Math.random() * (max - min + 1)) + min),
    (r = n.toString(16));
  while (r.length < len) {
    r = r + randHex(len - maxlen);
  }
  return r;
}
