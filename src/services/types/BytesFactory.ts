import web3 from 'web3';
import { IRestriction } from '../../models/IResctriction';
import { IValue } from '../../models/IValue';
import { ContractDefinition, FunctionDefinition, TypeName } from 'solidity-parser-antlr';
import { ITypeValueGenerator } from '../../models/scenario/ITypeValueGenerator';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { IntegerFactory } from './IntegerFactory';
import { RestrictionElementType } from '../../models/RestrictionElementType';
import { getRandomValue, warn } from '../Utils';
import { invertRestriction, fillsRestrictions } from '../Restrictions';
import { IContractGraph } from '../../models/graph';
import { ITypeValue } from '../../models/scenario';
import { StringFactory } from './StringFactory';

const BYTES_TYPENAME: { type: 'ElementaryTypeName'; name: string } = { type: 'ElementaryTypeName', name: 'bytes' };

/** *
 * It's going to create a instance of an immutable object with functions
 * to create or select random bytes values
 *
 * @param {boolean} unsigned Indicates if is a unsigned integer. If true, negative values are not allowed
 * @param {number} size The maximum number of bytes allowed
 * @returns {object} Object with Integer functionalities
 * */
export function BytesFactory(size: number): ITypeValueGenerator {
  let sizeReal = size ? size : 32;
  return Object.freeze({
    random,
    getExtraBoundary,
  });

  /**
   * Return a value in the pool in a random position or, return random value
   * In both cases, must obbey the restrictions
   *
   * @param {object} truffleProject Instance of Truffle Project under analysis
   * @param {*} restrictions Restrictions that need to be respected in order to select a value
   */
  function random(
    truffleProject: TruffleProjectIceFactoryFunction,
    restrictions: IRestriction[] = [],
    typeName: TypeName,
    contract: ContractDefinition | IContractGraph
  ): IValue {
    let value = null;
    do {
      let arrayCompatibleSize =
        truffleProject.getValuesPool().BytesLiteral && truffleProject.getValuesPool().BytesLiteral.length > 0
          ? truffleProject.getValuesPool().BytesLiteral.filter((bv) => bv.length <= size) //pega somente bytes com tamanho igual ou menor ao 'size' passado
          : [];

      const lengthRestrictions = restrictions.filter((r) => r.left.value.memberName == 'length');
      value = null;
      //if there is a restriction with equality, them returns this value OR it will be hard to fillRestrictions randomically
      const restEqualities = restrictions.filter(
        (r) =>
          r.operator == '==' &&
          r.left.value.memberName != 'length' &&
          (r.right.elementType == RestrictionElementType.Literal ||
            r.right.elementType == RestrictionElementType.EnumMember)
      );
      if (restEqualities.length > 0) {
        value =
          restEqualities.length == 1
            ? restEqualities[0].right.value.obj
            : restEqualities[Math.floor(Math.random() * (restEqualities.length + 1))].right.value.obj;
      }
      // If there is no restriction on length of bytes AND
      // If there is bytes literals in the pool, return one of them
      else if (lengthRestrictions.length == 0 && arrayCompatibleSize.length > 0) {
        const index = Math.floor(Math.random() * arrayCompatibleSize.length + 1); // +1 to have a chance to get a value out of bounds
        if (index < arrayCompatibleSize.length) {
          value = arrayCompatibleSize[index];
        }
      } else if (truffleProject.getValuesPool().BytesLiteral == null) {
        truffleProject.getValuesPool().BytesLiteral = [];
      }
      if (value == null) {
        // You can pass bytes parameters in Remix or browser-solidity as the array of single bytes, for example ["0x00","0xaa", "0xff"] is equivalent to "0x00aaff" https://ethereum.stackexchange.com/questions/13483/how-to-pass-arbitrary-bytes-to-a-function-in-remix
        let bytesSize = sizeReal;
        let stringHex = '';
        if (lengthRestrictions.length > 0) {
          bytesSize = IntegerFactory(true, 8).random(truffleProject, lengthRestrictions).obj;
        }
        //bytes32 vamos gerar uma string randômica e convertê-la pra bytes com essa 'web3.utils.fromAscii'
        //tentamos passar o resultado de web3.utils.randomHex, mas não funcionou
        // if (bytesSize == 32) {
        //   //padEnd(66,'0') veio daqui: github.com/ethereum/web3.js/issues/2256
        //   value = web3.utils.fromAscii(StringFactory().random(truffleProject, []).obj).padEnd(66, '0');
        // } else {
        stringHex = web3.utils.randomHex(bytesSize);
        value = web3.utils.hexToBytes(stringHex);
        // }
      }
    } while (!fillsRestrictions(value, restrictions, false, true));
    return {
      isCode: false,
      js: Array.isArray(value) ? `[${value}]` : `"${value}"`,
      sol:
        size == 32
          ? `stringToBytes32("${value}")`
          : size == null
          ? `abi.encode("${value}")`
          : `bytes${size ? size : ''}("${value}")`,
      obj: value,
    };
  }

  /**
   * Generate a value that is not in compliance with the {restriction}
   *
   * @param {object} truffleProject Instance of Truffle Project under analysis
   * @param {*} restriction Restriction that will be transgressed
   * @param {object} stringfieldValue The original supposed to be a valid value
   */
  function getExtraBoundary(
    truffleProject: TruffleProjectIceFactoryFunction,
    contractDefinition: ContractDefinition,
    functionDefinition: FunctionDefinition,
    restriction: IRestriction,
    typeValue: ITypeValue
  ): IValue {
    let value = JSON.parse(JSON.stringify(typeValue.stringfieldValue)); //clone the object received
    //se a restrição do array for em relação ao tamanho
    if (restriction.left.value.memberName == 'length') {
      let size = 0; //setando tamanho do array de bytes
      if (restriction.operator == '==' || restriction.operator == '<=') {
        size = parseInt(restriction.right.value.js) + 1;
      } else if (restriction.operator == '!=' || restriction.operator == '>' || restriction.operator == '<') {
        size = parseInt(restriction.right.value.js);
      } else if (restriction.operator == '>=') {
        size = parseInt(restriction.right.value.js) - 1;
      }
      // You can pass bytes parameters in Remix or browser-solidity as the array of single bytes, for example ["0x00","0xaa", "0xff"] is equivalent to "0x00aaff" https://ethereum.stackexchange.com/questions/13483/how-to-pass-arbitrary-bytes-to-a-function-in-remix
      const stringHex = web3.utils.randomHex(size);

      const bytesValue = web3.utils.hexToBytes(stringHex);
      value = {
        js: Array.isArray(bytesValue) ? `[${bytesValue}]` : `"${bytesValue}"`,
        sol:
          size == 32
            ? `stringToBytes32("${bytesValue}")`
            : size == null
            ? `abi.encode("${bytesValue}")`
            : `bytes${size ? size : ''}("${bytesValue}")`,
        obj: bytesValue,
      };
    } else if (['==', '!='].includes(restriction.operator)) {
      if (restriction.operator == '!=') {
        value = {
          isCode: false,
          js: Array.isArray(restriction.right.value.obj)
            ? `[${restriction.right.value.obj}]`
            : `"${restriction.right.value.js}"`,
          sol: restriction.right.value.sol,
          obj: restriction.right.value.obj,
        };
      } else {
        value = random(truffleProject, [invertRestriction(restriction)], BYTES_TYPENAME, contractDefinition); //se o restriction define que tem que ser igual a um valor, gera-se então um novo valor com a restrição que seja diferente dele
      }
    } else {
      throw new Error(
        `${contractDefinition.name}.${functionDefinition.name}: Tem um match restriction de bytes cujo operador não é '==' nem '!=' e que o memberName não é o length ` +
          JSON.stringify(typeValue.stringfieldValue.typeName)
      );
    }
    return value;
  }
}
