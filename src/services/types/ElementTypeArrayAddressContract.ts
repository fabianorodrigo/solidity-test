export const ElementTypeArrayAddressContract: {
  type: 'ArrayTypeName';
  baseTypeName: { type: 'ElementaryTypeName'; name: string };
} = {
  type: 'ArrayTypeName',
  baseTypeName: {
    type: 'ElementaryTypeName',
    name: 'addressContract',
  },
};
