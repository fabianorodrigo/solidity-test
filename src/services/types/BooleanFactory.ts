import { IValue } from '../../models/IValue';
import { ITypeValueGenerator } from '../../models/scenario/ITypeValueGenerator';
import { ContractDefinition, FunctionDefinition, TypeName } from 'solidity-parser-antlr';
import { IRestriction } from '../../models/IResctriction';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { RestrictionElementType } from '../../models/RestrictionElementType';
import { ITypeValue } from '../../models/scenario';

/** *
 * Boolean auxiliar functionalities
 * */
export function BooleanFactory(): ITypeValueGenerator {
  return Object.freeze({
    random,
    getExtraBoundary,
  });

  /**
   * Return, randomically, true or false
   */
  function random(truffleProject, restrictions: IRestriction[] = []): IValue {
    let value = null;
    if (restrictions.length == 0) {
      value = Math.random() >= 0.5;
    } else if (restrictions[0].operator == '==') {
      value = restrictions[0].right.value.obj;
    } else if (restrictions[0].operator == '!=') {
      value = restrictions[0].right.value.obj == 'true' || restrictions[0].right.value.obj == true ? false : true;
    } else {
      value = Math.random() >= 0.5;
    }
    return { isCode: false, js: value.toString(), sol: value.toString(), obj: value };
  }

  /**
   * Generate a value that is not in compliance with the {restriction}
   *
   * @param {object} truffleProject Instance of Truffle Project under analysis
   * @param {contractDefinition} contractDefinition
   * @param {IRestriction} restriction Restriction that will be transgressed
   */
  function getExtraBoundary(
    truffleProject: TruffleProjectIceFactoryFunction,
    contractDefinition: ContractDefinition,
    functionDefinition: FunctionDefinition,
    restriction: IRestriction,
    typeValue: ITypeValue
  ): IValue {
    let value = null;
    if (restriction.operator == '!=') {
      value = restriction.right.value.js;
    } else {
      value = restriction.right.value.js == 'true' ? 'false' : 'true';
    }
    return {
      isCode: false,
      js: value.startsWith('"') ? value : `"${value}"`,
      sol: value,
      obj: value == 'true',
      typeName: typeValue?.stringfieldValue?.typeName,
    };
  }
}
