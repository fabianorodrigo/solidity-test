import { Mapping, ContractDefinition, FunctionDefinition, TypeName } from 'solidity-parser-antlr';
import { IRestriction } from '../../models/IResctriction';
import { ITypeValueGenerator } from '../../models/scenario/ITypeValueGenerator';
import { IValue } from '../../models/IValue';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { RestrictionElementType } from '../../models/RestrictionElementType';
import { IContractGraph } from '../../models/graph';
import { ITypeValue } from '../../models/scenario';

/** *
 * Mapping auxiliar functionalities
 * */
export function MappingFactory(): ITypeValueGenerator {
  return Object.freeze({
    random,
    getExtraBoundary,
  });

  /**
   * Return, randomically Mapping
   */
  function random(
    truffleProject,
    restrictions: IRestriction[],
    typeName: Mapping,
    contract: ContractDefinition | IContractGraph,
    getRandomValue: Function
  ): IValue {
    const key = getRandomValue(typeName.keyType, truffleProject.getValuesPool(), contract, restrictions);
    const value = getRandomValue(typeName.valueType, truffleProject.getValuesPool(), contract, restrictions);
    return {
      isCode: false,
      js: `[${key.js},${value.js}]`,
      sol: `[${key.sol},${value.sol}]`,
      obj: '###null###',
    };
  }

  /**
   * Generate a value that is not in compliance with the {restriction}
   *
   * @param {object} truffleProject Instance of Truffle Project under analysis
   * @param {contractDefinition} contractDefinition
   * @param {IRestriction} restriction Restriction that will be transgressed
   */
  function getExtraBoundary(
    truffleProject: TruffleProjectIceFactoryFunction,
    contractDefinition: ContractDefinition,
    functionDefinition: FunctionDefinition,
    restriction: IRestriction,
    typeValue: ITypeValue
  ): IValue {
    let value = null;
    return {
      isCode: false,
      js: value.startsWith('"') ? value : `"${value}"`,
      sol: value,
      obj: '###null###',
      typeName: typeValue?.stringfieldValue?.typeName,
    };
  }
}
