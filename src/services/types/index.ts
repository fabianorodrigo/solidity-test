import { AddressFactory } from './AddressFactory';
import { StringFactory } from './StringFactory';
import { BytesFactory } from './BytesFactory';
import { BooleanFactory } from './BooleanFactory';

import { ArrayTypeNameFactory } from './ArrayTypeName';
import { MappingFactory } from './Mapping';
import { UserDefinedTypeNameFactory } from './UserDefinedTypeName';
import { IntegerFactory } from './IntegerFactory';
import { IStateVariableSetGraph, SourceVariableSetType, IFunctionGraph } from '../../models/graph';
import { ITypeValue } from '../../models/scenario';
import { AddressContractFactory } from './AddressContractFactory';

export * from './ElementTypeAddress';
export * from './ElementTypeAddressContract';
export * from './ElementTypeArrayAddressContract';
export * from './ElementTypeUint';
export * from './TypeValueAddress';
export * from './TypeValueUint';

/*import bytes from './bytes');
import bytes4 from './bytes4');
import bytes32 from './bytes32');*/

export const Types = {
  int: IntegerFactory(false, 32),
  int256: IntegerFactory(false, 32),
  uint: IntegerFactory(true, 32),
  uint256: IntegerFactory(true, 32),
  uint8: IntegerFactory(true, 8),
  uint32: IntegerFactory(true, 32),
  uint48: IntegerFactory(true, 48),
  uint64: IntegerFactory(true, 64),
  uint96: IntegerFactory(true, 96),
  uint128: IntegerFactory(true, 128),
  address: AddressFactory(),
  addressContract: AddressContractFactory(),
  string: StringFactory(),
  bool: BooleanFactory(),
  //The value types bytes1, bytes2, bytes3, …, bytes32 hold a sequence of bytes from one to up to 32. byte is an alias for bytes1.
  byte: BytesFactory(1),
  bytes1: BytesFactory(1),
  bytes: BytesFactory(null),
  bytes4: BytesFactory(4),
  bytes32: BytesFactory(32),
  ArrayTypeName: ArrayTypeNameFactory(),
  Mapping: MappingFactory(),
  UserDefinedTypeName: UserDefinedTypeNameFactory(),
};

export function getIValueFromStateVariableSetGraph(
  functionGraph: IFunctionGraph,
  sv: IStateVariableSetGraph
): ITypeValue {
  const stateType = functionGraph.contract.projectGraph.contracts[sv.contractName].stateVariables[sv.name].type;
  const result: ITypeValue = {
    type: stateType,
    subtype: null,
    stringfieldValue: { isCode: false, js: '', sol: '', obj: null },
  };
  if (sv.source.type == SourceVariableSetType.literal) {
    result.stringfieldValue.js = sv.source.aditionalInfo;
    result.stringfieldValue.sol = sv.source.aditionalInfo;
    result.stringfieldValue.obj = sv.source.aditionalInfo;
    if (['int', 'int256', 'uint', 'uint8', 'uint32', 'uint48', 'uint64', 'uint96', 'uint128'].includes(stateType)) {
      result.stringfieldValue.obj = parseInt(sv.source.aditionalInfo);
    } else if (stateType == 'string') {
      result.stringfieldValue.js = `"${sv.source.aditionalInfo}"`;
      result.stringfieldValue.sol = `"${sv.source.aditionalInfo}"`;
    }
    return result;
  }
}

export function getIValueFromValue(type: string, value: any): ITypeValue {
  const result: ITypeValue = {
    type: type,
    subtype: null,
    stringfieldValue: { isCode: false, js: '', sol: '', obj: null },
  };

  result.stringfieldValue.js = value;
  result.stringfieldValue.sol = value;
  result.stringfieldValue.obj = value;
  if (['int', 'int256', 'uint', 'uint8', 'uint32', 'uint48', 'uint64', 'uint96', 'uint128'].includes(type)) {
    result.stringfieldValue.obj = parseInt(value);
  } else if (type == 'string') {
    result.stringfieldValue.js = `"${value}"`;
    result.stringfieldValue.sol = `"${value}"`;
  }
  return result;
}
