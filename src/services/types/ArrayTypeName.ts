import { ContractDefinition, FunctionDefinition, ArrayTypeName, Identifier } from 'solidity-parser-antlr';
import { IRestriction } from '../../models/IResctriction';
import { IValue } from '../../models/IValue';
import { ITypeValueGenerator } from '../../models/scenario/ITypeValueGenerator';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { getRandomValue, trace } from '../Utils';
import { ITypeValue } from '../../models/scenario/ITypeValue';
import { Types } from '../types';
import { isIdentifier, isNumberLiteral } from '../solidityParser';
import { IContractGraph, IFunctionGraph } from '../../models/graph';

/** *
 * Array auxiliar functionalities
 * */
export function ArrayTypeNameFactory(): ITypeValueGenerator {
  return Object.freeze({
    random,
    getExtraBoundary,
  });

  /**
   * Return, randomically Array
   */
  function random(
    truffleProject: TruffleProjectIceFactoryFunction,
    restrictions: IRestriction[],
    typeName: any,
    contract: ContractDefinition | IContractGraph,
    getRandomValue: Function
  ): IValue {
    const uint = Types.uint8;
    let literalArrayJS = `[`;
    let literalArraySOL = `[`;
    let literalArrayOBJ = `[`;
    let size = Math.random() * 10; //if not specified, size until 10
    if (typeName.length && typeName.length.number) {
      size = parseInt(typeName.length.number);
    } else if (isIdentifier(typeName.length)) {
      //Se existe no contrato
      const contratGraph = truffleProject.getGraph().contracts[contract.name];
      let value = getStateVarFromContractGraph(contratGraph, (<Identifier>typeName.length).name);
      if (value) {
        size = value;
      } //se não existir, vamos procurar dentro dos arquivos que o contrato importa
      else {
        const libNames = Object.keys(contratGraph.dependencies);
        for (let z = 0; z < libNames.length; z++) {
          value = getStateVarFromContractGraph(
            truffleProject.getGraph().contracts[libNames[z]],
            (<Identifier>typeName.length).name
          );
          if (value) {
            size = value;
            break;
          }
        }
      }
    }
    let i = 0;

    if (
      restrictions &&
      restrictions.length > 0 &&
      restrictions.filter((r) => r.left.value.memberName == 'length').length > 0
    ) {
      size = parseInt(
        uint.random(
          truffleProject,
          restrictions.filter((r) => r.left.value.memberName == 'length'),
          null,
          null
        ).js
      );
    }
    trace('ArrayTypeName: size', size.toString(), JSON.stringify(restrictions));
    while (size > 0) {
      if (i > 0) {
        literalArrayJS += ',';
        literalArraySOL += ',';
        literalArrayOBJ += ',';
      }
      let v = getRandomValue(
        truffleProject,
        typeName.baseTypeName,
        contract,
        restrictions.filter((r) => r.left.value.memberName != 'length')
      );
      literalArrayJS += v.js;
      literalArraySOL += `${typeName.baseTypeName.name ? typeName.baseTypeName.name.concat('(') : ''}${v.sol}${
        typeName.baseTypeName.name ? ')' : ''
      }`;
      literalArrayOBJ += ['string', 'address'].includes(typeName.baseTypeName?.name) ? '"' : '';
      literalArrayOBJ += typeof v.obj == 'object' ? JSON.stringify(v.obj) : v.obj;
      literalArrayOBJ += ['string', 'address'].includes(typeName.baseTypeName?.name) ? '"' : '';
      size--;
      i++;
    }
    literalArrayJS += `]`;
    literalArraySOL += `]`;
    literalArrayOBJ += `]`;
    return { isCode: false, length: i, js: literalArrayJS, sol: literalArraySOL, obj: eval(literalArrayOBJ) };
  }

  /**
   * Generate a value that is not in compliance with the {restriction}
   *
   * @param {object} truffleProject Instance of Truffle Project under analysis
   * @param {*} restriction Restriction that will be transgressed
   * @param {object} stringfieldValue The original supposed to be a valid value
   */
  function getExtraBoundary(
    truffleProject: TruffleProjectIceFactoryFunction,
    contract: ContractDefinition | IContractGraph,
    func: FunctionDefinition | IFunctionGraph,
    restriction: IRestriction,
    typeValue: ITypeValue
  ): IValue {
    let value = JSON.parse(JSON.stringify(typeValue.stringfieldValue)); //clone the object received
    //se a restrição do array for em relação ao tamanho
    if (restriction.left.value.memberName == 'length') {
      let literalArrayJS = `[`;
      let literalArraySOL = `[`;
      let arraySize = 0; //setando tamanho do array
      if (restriction.operator == '==' || restriction.operator == '<=') {
        arraySize = parseInt(restriction.right.value.js) + 1;
      } else if (restriction.operator == '!=' || restriction.operator == '>' || restriction.operator == '<') {
        arraySize = parseInt(restriction.right.value.js);
      } else if (restriction.operator == '>=') {
        arraySize = parseInt(restriction.right.value.js) - 1;
      }

      let i = 0;
      while (arraySize > 0) {
        if (i > 0) {
          literalArrayJS += ',';
          literalArraySOL += ',';
        }
        literalArrayJS += getRandomValue(truffleProject, (<ArrayTypeName>value.typeName).baseTypeName, contract).js;
        literalArraySOL += getRandomValue(truffleProject, (<ArrayTypeName>value.typeName).baseTypeName, contract).sol;
        arraySize--;
        i++;
      }
      literalArrayJS += `]`;
      literalArraySOL += `]`;
      value = { js: literalArrayJS, sol: literalArraySOL };
    } else {
      throw new Error('Tem um match restriction de array que não é o length' + JSON.stringify(value.typeName));
    }
    return value;
  }
}

function getStateVarFromContractGraph(contractGraph: IContractGraph, varName: string) {
  const stateVar = contractGraph.stateVariables[varName];
  if (stateVar && stateVar.initialValue != null) {
    return stateVar.initialValue;
  }
  return null;
}
