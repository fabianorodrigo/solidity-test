import { IValue } from '../../models/IValue';
import { IRestriction } from '../../models/IResctriction';
import { ContractDefinition, FunctionDefinition, TypeName } from 'solidity-parser-antlr';

import { fillsRestrictions } from '../Restrictions';
import { ITypeValueGenerator } from '../../models/scenario/ITypeValueGenerator';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { RestrictionElementType } from '../../models/RestrictionElementType';
import { ganacheAccountsAddresses } from '../ganache';
import { ITypeValue } from '../../models/scenario';

/** *
 * Address auxiliar functionalities
 * */

/** *
 * It's going to create a instance of an immutable object with functions
 * to create or select random address values
 *
 * @returns {object} Object with Address functionalities
 * */
export function AddressFactory(): ITypeValueGenerator {
  return Object.freeze({
    random,
    getExtraBoundary,
  });

  /**
   * Return a value in the pool in a random position or, return random position in the accounts of ganache.
   * In both cases, must obbey the restrictions
   *
   * @param {object} truffleProject Instance of Truffle Project under analysis
   * @param {*} restrictions Restrictions that need to be respected in order to select a value
   */
  function random(truffleProject, restrictions: IRestriction[] = []): IValue {
    let value = null;
    let valueObj = null;
    let contaTentativas = 0;
    let sequentialIndex = 0;
    do {
      value = null;
      //if there is a restriction with equality, them returns this value OR it will be hard to fillRestrictions randomically
      const restEqualities = restrictions.filter(
        (r) =>
          r.operator == '==' &&
          (r.right.elementType == RestrictionElementType.Literal ||
            r.right.elementType == RestrictionElementType.EnumMember)
      );
      if (restEqualities.length > 0) {
        let idx = Math.floor(Math.random() * (restEqualities.length + 1));
        if (idx >= restEqualities.length) idx = restEqualities.length - 1;
        value = restEqualities[idx].right.value.js;
        valueObj = value;
      }
      // If there is number literals in the pool, return one of them
      else if (
        truffleProject.getValuesPool().AddressLiteral &&
        truffleProject.getValuesPool().AddressLiteral.length > 0
      ) {
        let index = null;
        contaTentativas++;
        //Se o número de tentativas superou o tamanho do pool, começa a percorrê-lo sequencialmente
        if (contaTentativas > truffleProject.getValuesPool().AddressLiteral.length) {
          index = sequentialIndex;
          sequentialIndex++;
        } else {
          index = Math.floor(
            Math.random() * (truffleProject.getValuesPool().AddressLiteral.length + 1) // +1 to have a chance to get a value out of bounds
          );
        }
        if (index < truffleProject.getValuesPool().AddressLiteral.length) {
          value = truffleProject.getValuesPool().AddressLiteral[index];
          valueObj = value;
        }
      }
      //If not found in the pool
      if (value == null) {
        const idx = Math.floor(Math.random() * (10 - 0) + 0);
        value = `accounts[${idx}]`;
        valueObj = value; //TODO: conferir depois se deu certo tirar essas contas fixadas ganacheAccountsAddresses[idx];
      }
    } while (!fillsRestrictions(value, restrictions));
    return {
      isCode: false,
      js: value.startsWith('0x') ? `"${value}"` : value,
      sol: value.startsWith('accounts[')
        ? `address(uint160(uint(keccak256(abi.encodePacked(nonce, blockhash(block.number))))))` //FIXME: Solidity é isso mesmo?
        : value.startsWith('"')
        ? value.substring(1, value.length - 1)
        : value, //está meio inconsistente essa joça. Tem hora que vem com aspas, tem hora que sem
      obj: valueObj,
    };
  }

  /**
   * Generate a value that is not in compliance with the {restriction}
   *
   * @param {object} truffleProject Instance of Truffle Project under analysis
   * @param {contractDefinition} contractDefinition
   * @param {IRestriction} restriction Restriction that will be transgressed
   */
  function getExtraBoundary(
    truffleProject: TruffleProjectIceFactoryFunction,
    contractDefinition: ContractDefinition,
    functionDefinition: FunctionDefinition,
    restriction: IRestriction,
    typeValue: ITypeValue
  ): IValue {
    //TODO: Pensar em como colocar valores válidos. Ao invés de retornar uma string inválida com essas concatenações
    let value = null;
    if (restriction.operator == '==' || restriction.operator == '<=') {
      const v = restriction.right.value.js;
      value = v.startsWith('accounts[9') ? 'accounts[8]' : 'accounts[9]';
    } else if (restriction.operator == '!=' || restriction.operator == '>' || restriction.operator == '<') {
      value = restriction.right.value.obj;
    } else if (restriction.operator == '>=') {
      value = restriction.right.value.js.substring(0, restriction.right.value.js.length - 2);
    }
    return {
      isCode: false,
      //o valor 0x0 no teste javascript tem que ser passado como "0x0000000000000000000000000000000000000000". Se passar "0x0", vai dizer que não é tipo address
      js:
        value == '0x0'
          ? `"0x0000000000000000000000000000000000000000"`
          : value.startsWith('"') || value.startsWith('accounts[')
          ? value
          : `"${value}"`,
      sol: value,
      obj: value,
      typeName: typeValue?.stringfieldValue?.typeName,
    };
  }
}

function isAddressZero(value: string) {
  return [
    '0x0',
    '"0x0"',
    '"0x0000000000000000000000000000000000000000"',
    '0x0000000000000000000000000000000000000000',
  ].includes(value);
}
