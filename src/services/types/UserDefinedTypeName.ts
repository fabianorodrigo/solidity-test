import { Types } from '../types';
import {
  ContractDefinition,
  TypeName,
  UserDefinedTypeName,
  FunctionDefinition,
  ElementaryTypeName,
} from 'solidity-parser-antlr';
import { IRestriction } from '../../models/IResctriction';
import { IValue } from '../../models/IValue';

import { fillsRestrictions } from '../Restrictions';
import { ITypeValueGenerator } from '../../models/scenario/ITypeValueGenerator';
import { ITypeValue } from '../../models/scenario/ITypeValue';
import { GetRandomValueFunction } from '../Utils/GetRandomValueFunction';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { RestrictionElementType } from '../../models/RestrictionElementType';
import { isString, isBytes } from '../solidityParser';
import { IContractGraph } from '../../models/graph';

/** *
 * UserDefinedType auxiliar functionalities
 * */
export function UserDefinedTypeNameFactory(): ITypeValueGenerator {
  return Object.freeze({
    random,
    getExtraBoundary,
  });

  /**
   * Return Object with randomic values
   */
  function random(
    truffleProject: TruffleProjectIceFactoryFunction,
    restrictions: IRestriction[] = [], //Não pude colocar o any por que não consegui fazer o Type Guard nas interfaces herdadas
    typeName: UserDefinedTypeName,
    contract: ContractDefinition | IContractGraph,
    getRandomValue: GetRandomValueFunction
  ): IValue {
    //Se for um struct
    if (
      truffleProject.getStructs()[contract.name.concat('.', typeName.namePath)] ||
      truffleProject.getStructs()[typeName.namePath]
    ) {
      const structFullName = truffleProject.getStructs()[typeName.namePath]
        ? `${truffleProject.getStructs()[typeName.namePath].parentName}.${
            truffleProject.getStructs()[typeName.namePath].name
          }`
        : contract.name.concat('.', typeName.namePath);
      //No Javascript pode-se passar um JSON do objeto equivalente ao struct
      let literalStructJS = `{`;
      //NO Solidity instancia-se chamando contrato.struct(...parameteros)
      let literalStructSOL = `${structFullName}(`;
      let obj = {};
      for (let ps = 0; ps < truffleProject.getStructs()[structFullName].members.length; ps++) {
        //Se for Mapping, não passa nenhum valor para o struct
        //The mapping is also ignored in assignments of structs or arrays containing a mapping.
        //https://solidity.readthedocs.io/en/develop/security-considerations.html#clearing-mappings
        if (truffleProject.getStructs()[structFullName].members[ps].typeName.type == 'Mapping') {
          continue;
        }
        if (ps > 0) {
          literalStructJS += ',';
          if (literalStructSOL != null) literalStructSOL += ',';
        }
        literalStructJS += `"${truffleProject.getStructs()[structFullName].members[ps].name}": `;
        const valueMember = getRandomValue(
          truffleProject,
          truffleProject.getStructs()[structFullName].members[ps].typeName,
          contract,
          restrictions.filter(
            (r) => r.left.value.memberName == truffleProject.getStructs()[structFullName].members[ps].name
          )
        );
        //if string, replaces special caracters
        if ((<ElementaryTypeName>truffleProject.getStructs()[structFullName].members[ps].typeName).name == 'string') {
          valueMember.js = valueMember.js.replace(/\\/, '\\');
        }
        obj[truffleProject.getStructs()[structFullName].members[ps].name] = valueMember.obj;
        literalStructJS += valueMember.js;
        //Se um dos parâmetros do Struct retornar null, todo ele será null e não será possível gerar
        //a função de teste pois Solidity não suporta 'null'
        if (valueMember.sol == null || literalStructSOL == null) {
          literalStructSOL = null;
        } else {
          literalStructSOL += valueMember.sol;
        }
      }
      literalStructJS += `}`;
      if (literalStructSOL != null) {
        literalStructSOL += `)`;
      }
      return { isCode: false, js: literalStructJS, sol: literalStructSOL, obj: obj };
    } //Se for um enum
    else if (truffleProject.getEnumsByName(typeName.namePath).length > 0) {
      let value = null;
      do {
        //if there is a restriction with equality, them returns this value OR it will be hard to fillRestrictions randomically
        const restEqualities = restrictions.filter(
          (r) =>
            r.operator == '==' &&
            (r.right.elementType == RestrictionElementType.Literal ||
              r.right.elementType == RestrictionElementType.EnumMember)
        );
        if (restEqualities.length > 0) {
          value =
            restEqualities.length == 1
              ? restEqualities[0].right.value.js.concat('.', restEqualities[0].right.value.memberName)
              : restEqualities[Math.floor(Math.random() * (restEqualities.length + 1))].right.value.js.concat(
                  '.',
                  restEqualities[Math.floor(Math.random() * (restEqualities.length + 1))].right.value.memberName
                );
        } else {
          const enumSameName = truffleProject.getEnumsByName(typeName.namePath);
          let contractKey = enumSameName[0].split('.')[0];
          //Caso haja mais de um enum que contenha o mesmo nome, dará preferência pelo
          //enum do contrato em análise
          if (enumSameName.length > 1) {
            const startsWith = enumSameName.find((e) => e.startsWith(`${contract.name}.`));
            if (startsWith) {
              contractKey = startsWith.split('.')[0];
            }
          }

          //se eu tenho exatamente o mesmo número de restrições igual ao tamanho das opçoes do Enum
          //e todas as restrições são com operador '!=', vai ficar em loop infinito. Solução: NULL
          if (
            restrictions.length == truffleProject.getEnums()[contractKey][typeName.namePath].members.length &&
            restrictions.filter((r) => r.operator != '!=').length == 0
          ) {
            value = null;
          } else {
            value = `${contractKey}.${typeName.namePath}.${
              truffleProject.getEnums()[contractKey][typeName.namePath].members[
                Math.floor(Math.random() * truffleProject.getEnums()[contractKey][typeName.namePath].members.length)
              ].name
            }`;
          }
        }
      } while (!fillsRestrictions(value, restrictions));
      return getValueObject(truffleProject, value);
    } else if (truffleProject.getValuesPool(false)[typeName.namePath]) {
      //Não pode ser do mesmo tipo sob analise, não pode ser abstrato AND
      //(o tipo do parâmetro tem ser de um outro contrato do projeto ou de algum que herde de tal tipo)
      //Inicialmente, prioriza os contratos do projeto do contrato (bibliotecas de terceiros ficam de fora)
      let candidatesContracts = Object.values(truffleProject.getGraph().contracts).filter(
        (c) =>
          c.name != contract.name &&
          c.isThirdPartyLib == false &&
          c.isAbstract == false &&
          (c.name == typeName.namePath || Object.keys(c.inheritsFrom).includes(typeName.namePath))
      );
      if (candidatesContracts.length == 0) {
        candidatesContracts = Object.values(truffleProject.getGraph().contracts).filter(
          (c) =>
            c.isThirdPartyLib == true &&
            c.isAbstract == false &&
            (c.name == typeName.namePath || Object.keys(c.inheritsFrom).includes(typeName.namePath))
        );
      }

      let indice = Math.floor(Math.random() * candidatesContracts.length);
      // while (
      //   truffleProject.getValuesPool(false)[typeName.namePath][indice] == contractDefinition.name &&
      //   truffleProject.getValuesPool(false)[typeName.namePath].length > 1
      // ) {
      //   indice = Math.floor(Math.random() * truffleProject.getValuesPool(false)[typeName.namePath].length);
      // }
      if (!candidatesContracts[indice])
        console.log('====================> retornou nullo ao buscar valor para tipo:', typeName.namePath);
      return {
        isCode: false,
        js: candidatesContracts[indice]
          ? 'contract'.concat(candidatesContracts[indice].name).concat('.address')
          : 'null',
        sol: candidatesContracts[indice] ? 'contract'.concat(candidatesContracts[indice].name) : 'null',
        obj: truffleProject.getValuesPool(false)[typeName.namePath],
      };
    } else {
      const deployedContracts = truffleProject.getDeployedJSContracts();

      if (deployedContracts.length == 0) throw new Error(`Deployed contracts not found`);
      //Não pode ser do mesmo tipo
      let indice = Math.floor(Math.random() * deployedContracts.length);
      while (deployedContracts[indice] == contract.name && deployedContracts.length > 1) {
        indice = Math.floor(Math.random() * deployedContracts.length);
      }

      if (deployedContracts[indice]) {
        return {
          isCode: false,
          js: 'contract'.concat(deployedContracts[indice]).concat('.address'),
          sol: 'contract'.concat(deployedContracts[indice]),
          obj: deployedContracts[indice],
        };
      } else {
        return {
          isCode: false,
          js: 'null',
          sol: 'address(0)',
          obj: null,
        };
      }
    }
  }

  /**
   * Generate a value that is not in compliance with the {restriction}
   *
   * @param {object} truffleProject Instance of Truffle Project under analysis
   * @param {contractDefinition} contractDefinition Contract Definition in the AST that {functionDefinition} belongs to
   * @param {*} restriction Restriction that will be transgressed
   * @param {object} stringfieldValue The original supposed to be a valid value
   */
  function getExtraBoundary(
    truffleProject: TruffleProjectIceFactoryFunction,
    contractDefinition: ContractDefinition,
    functionDefinition: FunctionDefinition,
    restriction: IRestriction,
    parameterTypeValue: ITypeValue
  ): IValue {
    let paramTypeValue: IValue = JSON.parse(JSON.stringify(parameterTypeValue.stringfieldValue)); //clone the object received
    if (
      truffleProject.getStructs()[(<UserDefinedTypeName>parameterTypeValue.stringfieldValue.typeName).namePath] ||
      truffleProject.getStructs()[
        contractDefinition.name.concat(
          '.',
          (<UserDefinedTypeName>parameterTypeValue.stringfieldValue.typeName).namePath
        )
      ]
    ) {
      const structFullName = truffleProject.getStructs()[
        (<UserDefinedTypeName>parameterTypeValue.stringfieldValue.typeName).namePath
      ]
        ? (<UserDefinedTypeName>parameterTypeValue.stringfieldValue.typeName).namePath
        : contractDefinition.name.concat(
            '.',
            (<UserDefinedTypeName>parameterTypeValue.stringfieldValue.typeName).namePath
          );
      //se for um struct
      const struct = truffleProject.getStructs()[structFullName];
      let jsValueObj = JSON.parse(paramTypeValue.js.replace('\\', '\\\\'));
      //No Javascript pode-se passar um JSON do objeto equivalente ao struct
      let literalStructJS = `{`;
      //NO Solidity instancia-se chamando contrato.struct(...parameteros)
      let literalStructSOL = `${struct.parentName}.${
        (<UserDefinedTypeName>parameterTypeValue.stringfieldValue.typeName).namePath
      }(`;
      for (let ps = 0; ps < struct.members.length; ps++) {
        //Se for Mapping, não passa nenhum valor para o struct (deu erro quando tentamos)
        if (struct.members[ps].typeName.type == 'Mapping') {
          continue;
        }
        if (ps > 0) {
          literalStructJS += ',';
          literalStructSOL += ',';
        }
        literalStructJS += `${struct.members[ps].name} : `;
        //se a restrição for naquele parâmetro/atributo do struct, busca o valor
        //senão, repete o que recebeu
        if (restriction.left.value.memberName && restriction.left.value.memberName == struct.members[ps].name) {
          const valueToMatch = (<ITypeValueGenerator>(
            Types[(<ElementaryTypeName>struct.members[ps].typeName).name]
          )).getExtraBoundary(truffleProject, contractDefinition, functionDefinition, restriction, {
            type: (<ElementaryTypeName>struct.members[ps].typeName).name,
            subtype: null,
            stringfieldValue: { isCode: false, js: null, sol: null, obj: null },
          });
          paramTypeValue.obj[struct.members[ps].name] = valueToMatch.obj;
          literalStructJS += valueToMatch.js;
          literalStructSOL += valueToMatch.sol;
        } else {
          let lJS: string = '',
            rJS: string = '';
          let lSOL: string = '',
            rSOL: string = '';
          if (isString(truffleProject.getGraph(), struct.members[ps])) {
            lJS = rJS = lSOL = rSOL = '"';
          } else if (isBytes(truffleProject.getGraph(), struct.members[ps])) {
            lJS = '[';
            rJS = ']';
            lSOL = 'stringToBytes32("';
            rSOL = '")';
          }
          literalStructJS += `${lJS}${jsValueObj[struct.members[ps].name]}${rJS}`;
          literalStructSOL += `${lSOL}${jsValueObj[struct.members[ps].name]}${rSOL}`;
        }
      }
      literalStructJS += `}`;
      literalStructSOL += `)`;
      paramTypeValue.js = literalStructJS;
      paramTypeValue.sol = literalStructSOL;
    } else {
      throw new Error(
        'Tem um match restriction que não é struct' + JSON.stringify(parameterTypeValue.stringfieldValue.typeName)
      );
    }
    return paramTypeValue;
  }

  function getValueObject(truffleProject, value): IValue {
    //Se for enum, no teste em Javascript, o valor a ser passado será inteiro, então, fazemos a conversão
    if (
      truffleProject.getEnums &&
      value &&
      truffleProject.getEnums()[value.substring(0, value.indexOf('.'))] &&
      truffleProject.getEnums()[value.substring(0, value.indexOf('.'))][
        value.substring(value.indexOf('.') + 1, value.lastIndexOf('.'))
      ]
    ) {
      const vJS = truffleProject
        .getEnums()
        [value.substring(0, value.indexOf('.'))][
          value.substring(value.indexOf('.') + 1, value.lastIndexOf('.'))
        ].members.findIndex((m) => m.name == value.substring(value.lastIndexOf('.') + 1));
      return { isCode: false, js: vJS, sol: value, obj: vJS };
    }
    //O valor retorna null quando existe, por exemplo, uma sequencia de IF/ELSEIF's
    //tratando cada elemento do Enum e tem um ELSE no caso de vir algo inesperado
    //Para o Javascript, podemos incluir um valor numérico que extrapola, Solidity não rola
    else if (value == null) {
      return { isCode: false, js: '99999', sol: null, obj: '99999' };
    } else {
      return { isCode: false, js: value, sol: value, obj: value };
    }
  }
}
