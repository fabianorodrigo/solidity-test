import { IRestriction } from '../../models/IResctriction';
import { IValue } from '../../models/IValue';
import { ContractDefinition, FunctionDefinition, TypeName } from 'solidity-parser-antlr';

import { fillsRestrictions } from '../Restrictions';
import { ITypeValueGenerator } from '../../models/scenario/ITypeValueGenerator';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { RestrictionElementType } from '../../models/RestrictionElementType';
import { ITypeValue } from '../../models/scenario';

/** *
 * String auxiliar functionalities
 * */

/** *
 * It's going to create a instance of an immutable object with functions
 * to create or select random string values
 *
 * @returns {object} Object with Integer functionalities
 * */
export function StringFactory(): ITypeValueGenerator {
  return Object.freeze({
    random,
    getExtraBoundary,
  });

  /**
   * Return a value in the pool in a random position or exceptionally return another random value.
   * In both cases, must obbey the restrictions
   *
   * @param {object} truffleProject Instance of Truffle Project under analysis
   * @param {*} restrictions Restrictions that need to be respected in order to select a value
   */
  function random(truffleProject, restrictions: IRestriction[] = []): IValue {
    let value = null;
    let contaTentativas = 0;
    let sequentialIndex = 0;
    do {
      value = null;
      //if there is a restriction with equality, them returns this value OR it will be hard to fillRestrictions randomically
      const restEqualities = restrictions.filter(
        (r) =>
          r.operator == '==' &&
          (r.right.elementType == RestrictionElementType.Literal ||
            r.right.elementType == RestrictionElementType.EnumMember)
      );
      if (restEqualities.length > 0) {
        value =
          restEqualities.length == 1
            ? restEqualities[0].right.value.js
            : restEqualities[Math.floor(Math.random() * (restEqualities.length + 1))].right.value.js;
      }
      // If there is literals in the pool, return one of them
      else if (
        truffleProject.getValuesPool().StringLiteral &&
        truffleProject.getValuesPool().StringLiteral.length > 0
      ) {
        let index = null;
        contaTentativas++;
        //Se o número de tentativas superou o tamanho do pool, começa a percorrê-lo sequencialmente
        if (contaTentativas > truffleProject.getValuesPool().StringLiteral.length) {
          index = sequentialIndex;
          sequentialIndex++;
        } else {
          index = Math.floor(
            Math.random() * (truffleProject.getValuesPool().StringLiteral.length + 1) // +1 to have a chance to get a value out of bounds
          );
        }
        if (index < truffleProject.getValuesPool().StringLiteral.length) {
          value = `"${truffleProject.getValuesPool().StringLiteral[index]}"`;
        }
      } else if (truffleProject.getValuesPool().StringLiteral == null) {
        truffleProject.getValuesPool().StringLiteral = [];
      }
      //If not found in the pool
      if (value == null) {
        truffleProject.getValuesPool().StringLiteral.push(`${Math.random().toString(36).substring(7).toString()}`);
        value = `"${
          truffleProject.getValuesPool().StringLiteral[truffleProject.getValuesPool().StringLiteral.length - 1]
        }"`;
      }
    } while (!fillsRestrictions(value, restrictions));
    return { isCode: false, js: value, sol: value, obj: value.substr(1, value.length - 2) };
  }

  /**
   * Generate a value that is not in compliance with the {restriction}
   *
   * @param {object} truffleProject Instance of Truffle Project under analysis
   * @param {*} restriction Restriction that will be transgressed
   * @param {object} stringfieldValue The original supposed to be a valid value (para o tipo inteiro não faz diferença)
   */
  function getExtraBoundary(
    truffleProject: TruffleProjectIceFactoryFunction,
    contractDefinition: ContractDefinition,
    functionDefinition: FunctionDefinition,
    restriction: IRestriction,
    typeValue: ITypeValue
  ): IValue {
    let value = null;
    if (restriction.operator == '==' || restriction.operator == '<=') {
      value = 'Z'.concat(restriction.right.value.js);
    } else if (restriction.operator == '!=' || restriction.operator == '>' || restriction.operator == '<') {
      value = restriction.right.value.js;
    } else if (restriction.operator == '>=') {
      value = 'a'.concat(restriction.right.value.js);
    }
    return { isCode: false, js: value, sol: value, obj: value, typeName: typeValue?.stringfieldValue?.typeName };
  }
}
