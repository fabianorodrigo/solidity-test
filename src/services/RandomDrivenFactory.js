const fs = require('fs');
const path = require('path');
const fsextra = require('fs-extra');
const colors = require('colors');
const shelljs = require('shelljs');

const spawnSync = require('child_process').spawnSync;

const SolidityParserIceFactory = require('./SolidityParserFactory');
const MutationIceFactory = require('./MutationFactory');
const TruffleUtilsIceFactory = require('./TruffleFactory');

const TruffleUtils = TruffleUtilsIceFactory();

/** *
 * Create a RandomDriven Strategy
 *
 * @param {string} truffleProjectHome Path of the home of the truffle project
 * @param {Date} timeLimit Time limit to process to find the solution
 * */
module.exports = function RandomDrivenStrategyFactory({
  truffleProjectHome,
  timeLimit = new Date(new Date().getTime() + 10 * 60000),
}) {
  let valuesPool = {};

  return Object.freeze({
    createMutants,
    generateSequences,
    seqExists,
  });

  /** *
   * Generate a sequence of contract function calls
   *
   * @param {string} solidityPath Path of the solidity file
   * @param {date} timeLimit  The maximum date/time until the generateSequences effort can last (default: 10 minutes ahead)
   */
  async function generateSequences() {
    // generate mutations, record mutant files and return info about the mutation process
    //FIXME: const mutationProcess = await createMutants();
    // Clone the literal values read during mutation process to the {valuesPool}
    /*FIXME valuesPool = JSON.parse(
      JSON.stringify(mutationProcess._parameterValuesPool),
    );*/

    //get .sol files in the 'contracts' directory
    const solidityPaths = shelljs
      .find(path.join(truffleProjectHome, 'contracts'))
      .filter(file => {
        return (
          file.endsWith('.sol') &&
          !file.toLowerCase().endsWith('migrations.sol')
        );
      });
    const SolidityParsers = solidityPaths.map(solFilePath => {
      return SolidityParserIceFactory({
        solFilePath,
      });
    });

    let functions = [];
    // Extract list of functions from the solidity file
    SolidityParsers.forEach(SolidityParser => {
      functions = functions.concat(
        SolidityParser.extractFunctionsFromContractDefinition(),
      );
    });

    const errorSeqs = {}; // Their execution violates a contract
    const nonErrorSeqs = {}; // Their execution violates no contract

    while (new Date() < timeLimit) {
      let sequenceIndex = -1;
      const indiceRandomico = Math.floor(Math.random() * functions.length);
      const f = functions[indiceRandomico]; // random function
      //if (f == null) {
      console.log(indiceRandomico, functions.length);
      //}
      if (!nonErrorSeqs[f.contractName]) {
        nonErrorSeqs[f.contractName] = {};
        nonErrorSeqs[f.contractName].name = f.contractName;
        nonErrorSeqs[f.contractName].sequences = [];
        nonErrorSeqs[
          f.contractName
        ].beforeEachMigrationCode = await TruffleUtils.generateMigrationBeforeEach(
          f.contractPath,
          f.contractName,
          getFunctionParametersValues,
        );
      }
      if (!errorSeqs[f.contractName]) {
        errorSeqs[f.contractName] = {};
        nonErrorSeqs[f.contractName].name = f.contractName;
        errorSeqs[f.contractName].sequences = [];
      }

      let seq = {
        functions: [],
        params: [],
      };
      // if successful seq exists already for the contract of function selected, select one to extend
      if (
        nonErrorSeqs[f.contractName] &&
        nonErrorSeqs[f.contractName].sequences.length > 0
      ) {
        // take a existing sequence randomicaly
        sequenceIndex = Math.floor(
          Math.random() * (nonErrorSeqs[f.contractName].sequences.length + 1),
        ); // +1 to have a chance to get a value out of bounds
        // if the random index is in the range of nonError Sequence array, clone the one. Otherwise, is a new sequence
        if (sequenceIndex < nonErrorSeqs[f.contractName].sequences.length) {
          seq = Utils.clone(
            nonErrorSeqs[f.contractName].sequences[sequenceIndex],
          );
        }
      }
      // append the random function selected above to the sequence
      seq.functions.push(f);
      seq.params.push(getFunctionParametersValues(f.functionDefinition));

      // It continues only if the sequence wasn't created before
      if (
        !seqExists(nonErrorSeqs[f.contractName].sequences, seq) &&
        !seqExists(errorSeqs[f.contractName].sequences, seq)
      ) {
        // const newSeq = RandomStrategy.extend(f)
        const validSequence = await executeSeq(
          solidityPath,
          seq,
          nonErrorSeqs[f.contractName].beforeEachMigrationCode,
        );
        if (validSequence) {
          console.log('validSequence');
          // If out of the bounds, is a new sequence. Otherwise, an update
          if (
            sequenceIndex == -1 ||
            sequenceIndex >= nonErrorSeqs[f.contractName].sequences.length
          ) {
            nonErrorSeqs[f.contractName].sequences.push(seq);
          } else {
            nonErrorSeqs[f.contractName].sequences[sequenceIndex] = seq;
          }
        } else {
          errorSeqs[f.contractName].sequences.push(seq);
        }
      }
      printSequences(nonErrorSeqs);
      const timeDiff = Math.abs(timeLimit.getTime() - new Date().getTime());
      console.log(
        'TEMPO RESTANTE: ',
        colors.cyan(Math.ceil(timeDiff / 1000)),
        'segundos',
      );
    }
    const testContracts = [];
    return testContracts;
  }

  /** *
   * Serializes a seq in a test file, execute and return if it violates the contract (throws exception or not)
   *
   * @param {string} contractPath Path of the solidity file
   * @param {object} seq Sequence to be executed {functions: Array, params: Array}
   * @param {string} beforeEachMigrationCode Code of the method beforeEach that makes the migration of the contract
   * @returns {boolean} TRUE if the sequence is successfully executed (no errors on 'truffle test'), or FALSE otherwise
   */
  async function executeSeq(contractPath, seq, beforeEachMigrationCode) {
    // init a temp truffle project
    const outputDir = path.join(
      './temp',
      path.basename(contractPath).replace('.', ''),
    );
    TruffleUtils.initProject([{ path: contractPath }], './temp');
    // Generate test file
    let testContract = `const ${
      seq.functions[0].contractName
    } = artifacts.require("./${path.basename(contractPath)}");`;
    const innerTest = getMethodTestCodeFromSeq(seq);
    testContract += `\ncontract('${seq.functions[0].contractName}', function (accounts) {\n

        ${beforeEachMigrationCode}

        it("Test whether Sequence Execution is violated or not", async () => {
                    ${innerTest}
        });\n});`;
    fs.writeFileSync(
      path.join(outputDir, 'test', `${seq.functions[0].contractName}.js`),
      testContract,
    );
    const truffleTest = spawnSync('truffle', ['test'], { cwd: outputDir });
    // LOG RESULT
    console.log(innerTest, colors.blue(truffleTest.status));
    if (truffleTest.status != 0) {
      const regExp = /Error: Returned error:(.+?)at Object\.ErrorResponse/gms;
      const msg = regExp.exec(truffleTest.stdout.toString().trim());
      if (msg != null) {
        console.log(msg[1]);
      }
    }
    /* if (truffleTest.status === 0) {
            console.log(colors.red(testContract));
        } */
    return truffleTest.status === 0; // zero means no errors during test execution
  }
  function printSequences(sequenceCollection) {
    console.log('sequenceCollection'.red, sequenceCollection);
    let result =
      '##################################################################\n';
    Object.keys(sequenceCollection).forEach(contractName => {
      sequenceCollection[contractName].sequences.forEach((s, seqPos) => {
        s.functions.forEach((f, i) => {
          result += `${contractName}: ${seqPos} - ${f.functionDefinition.name}(`;
          s.params[i].forEach((p, i) => {
            if (i > 0) result += `, `;
            result += `${p}`;
          });
          result += `)\n`;
        });
      });
    });
    console.log(colors.yellow(result));
  }

  /** *
   * Check if already exists {seq} in the {seqCollection}
   */
  function seqExists(seqCollection, seq) {
    const seqJSON = JSON.stringify(seq);
    return (
      seqCollection.findIndex(s => {
        return JSON.stringify(s) === seqJSON;
      }) > -1
    );
  }

  /** *
   * Generate the code inside a method of test based on the sequence of method calls received
   *
   * @param {Array} seq Sequence of functions to call and it's respective parameters
   * @returns code javascript to insert into a method of test
   */
  function getMethodTestCodeFromSeq(seq) {
    let innerTest = `//const owner = ${GanacheAccounts[0]};\n`;
    seq.functions.forEach((f, j) => {
      let paramsString = '';
      seq.params[j].forEach((p, i) => {
        if (i > 0) {
          paramsString += ', ';
        }
        paramsString += `${p}`;
      });
      innerTest += `const r${j} = await contract.${f.functionDefinition.name}(${paramsString});\n`;
      if (f.functionDefinition.stateMutability == 'payable') {
        innerTest += `assert.notEqual(r${j},null);\n`;
        innerTest += `assert.isAtLeast(r${j}.logs.length,1);\n`;
      }
    });
    return innerTest;
  }

  /** *
   * Create multiples mutant contracts each one with a fault/mutation
   *
   * @returns {object} Object with literal values found in the code during the creating of mutants
   */
  async function createMutants() {
    // init a temp truffle project
    const outputDir = path.join(
      './temp',
      path.basename(solidityPath).replace('.', ''),
    );
    await TruffleUtils.initProject([{ path: solidityPath }], './temp');

    // Read contracts from solidity file and generate and return the source code of mutants
    const MutationUtils = MutationIceFactory({ solidityPath });
    const mutationProcess = MutationUtils.generateMutants();

    mutationProcess._contractSources.forEach(m => {
      fs.writeFileSync(
        path.join(outputDir, 'contracts', `${m.name}.sol`),
        m.content,
      );
    });

    return mutationProcess;
  }
};
