import { FunctionDefinition } from 'solidity-parser-antlr';

export function newConstructor(): FunctionDefinition {
  return {
    type: 'FunctionDefinition',
    name: '',
    isConstructor: true,
    parameters: [],
    modifiers: [],
    visibility: 'public',
    body: { type: 'Block', statements: [] },
  };
}
