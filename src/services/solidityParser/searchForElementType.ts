import {
  BaseASTNode,
  BinaryOperation,
  ForStatement,
  FunctionCall,
  IfStatement,
  IndexAccess,
  MemberAccess,
  TupleExpression,
  VariableDeclarationStatement,
} from 'solidity-parser-antlr';
import {
  isBO,
  isExpressionStatement,
  isFor,
  isFunctionCall,
  isIf,
  isIndexAccess,
  isMemberAccess,
  isTupleExpression,
  isUO,
  isVariableDeclaration,
} from './statements';

/**
 * Procura na estrutura aninhada da Expression {st} pelos elementos com type == 'type'
 *
 * @param st
 */
export function searchForElementType(st, types: string[]): any[] {
  let result = [];
  if (st == null) return result;
  if (types.includes(st?.type)) result.push(st);
  if (st.type == 'FunctionCall') {
    const subResultExpression = searchForElementType(st.expression, types);
    if (subResultExpression.length > 0) result = result.concat(subResultExpression);
    for (let i = 0; i < st.arguments.length; i++) {
      const subResultParam = searchForElementType(st.arguments[i], types);
      if (subResultParam.length > 0) result = result.concat(subResultParam);
    }
  } else if (isIndexAccess(st)) {
    const subResultBase = searchForElementType((<IndexAccess>st).base, types);
    if (subResultBase.length > 0) result = result.concat(subResultBase);
    const subResultIndex = searchForElementType((<IndexAccess>st).index, types);
    if (subResultIndex.length > 0) result = result.concat(subResultIndex);
  } else if (isMemberAccess(st)) {
    const subResult = searchForElementType((<MemberAccess>st).expression, types);
    if (subResult.length > 0) result = result.concat(subResult);
  } else if (isTupleExpression(st)) {
    for (let i = 0; i < (<TupleExpression>st).components.length; i++) {
      const subResult = searchForElementType((<TupleExpression>st).components[i], types);
      if (subResult.length > 0) result = result.concat(subResult);
    }
  } else if (isBO(st)) {
    const subResultLeft = searchForElementType((<BinaryOperation>st).left, types);
    if (subResultLeft.length > 0) result = result.concat(subResultLeft);
    const subResultRight = searchForElementType((<BinaryOperation>st).right, types);
    if (subResultRight.length > 0) result = result.concat(subResultRight);
  } else if (isUO(st)) {
    const subResult = searchForElementType((<any>st).subExpression, types);
    if (subResult.length > 0) result = result.concat(subResult);
  } else if (isExpressionStatement(st)) {
    const subResult = searchForElementType((<any>st).expression, types);
    if (subResult.length > 0) result = result.concat(subResult);
  } else if (isIf(st)) {
    const subResult = searchForElementType((<IfStatement>st).condition, types);
    if (subResult.length > 0) result = result.concat(subResult);
    if (st.falseBody != null) {
      const subResultFalseBody = searchForElementType((<IfStatement>st).falseBody, types);
      if (subResultFalseBody.length > 0) result = result.concat(subResultFalseBody);
    }
  } else if (isFor(st)) {
    const subResultInit = searchForElementType((<ForStatement>st).initExpression, types);
    if (subResultInit.length > 0) result = result.concat(subResultInit);
    const subResultCondition = searchForElementType((<ForStatement>st).conditionExpression, types);
    if (subResultCondition.length > 0) result = result.concat(subResultCondition);
    const subResultLoop = searchForElementType((<ForStatement>st).loopExpression, types);
    if (subResultLoop.length > 0) result = result.concat(subResultLoop);
  } else if (isVariableDeclaration(st)) {
    const subResult = searchForElementType((<VariableDeclarationStatement>st).initialValue, types);
    if (subResult.length > 0) result = result.concat(subResult);
  }
  return result;
}
