import { ContractDefinition, FunctionDefinition, StateVariableDeclaration } from 'solidity-parser-antlr';
import { isStateVariableDeclaration } from './statements';

/** *
 * Receive a contractDefinition and return a array with all it's state variable
 *
 * @param {contractDefinition} contractDefinition A instance of the contractDefinition in the AST from which the functionsDefinition will be retrieved
 * @returns {Array} Array of FunctionDefinition
 */
export function getStateVariables(contractDefinition: ContractDefinition): StateVariableDeclaration[] {
  const stateVariablesReturn: StateVariableDeclaration[] = [];
  contractDefinition.subNodes.forEach(sn => {
    if (isStateVariableDeclaration(sn)) {
      stateVariablesReturn.push(<StateVariableDeclaration>sn);
    }
  });
  return stateVariablesReturn;
}
