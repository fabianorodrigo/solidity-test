import { BinaryOperation, FunctionDefinition, ContractDefinition } from 'solidity-parser-antlr';
import { IRestriction } from '../../models/IResctriction';

import { isBO, isUO } from './statements';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { getRestrictionValue } from './restrictionValue';
import { BranchType } from '../../models/BranchType';
import { getUnaryOperationRestriction } from './unaryOperation';
import { getFunctionGraph } from '../TruffleProjectGraph';
import { RestrictionElementType } from '../../models/RestrictionElementType';

/**
 * Analyse the BinaryOperation {bo} and return the restrictions to the parameter {paramName} in it
 *
 * Consider only 'require' by now
 *
 * @param {object} truffleProject Instance of Truffle Project under analysis
 * @param {BinaryOperation} bo Binary Operation to be analysed
 * @param {Array} inputParameterNames Parameter names that will be considered in the identification of branches of interest
 * @param {FunctionDefinition} functionDetinition Reference to the function where the {bo} belongs
 * @returns {Array} List of restrictions found
 */
export function getBinaryOperationRestriction(
  truffleProject: TruffleProjectIceFactoryFunction,
  contract: ContractDefinition,
  bo: BinaryOperation,
  functionDefinition: FunctionDefinition
): IRestriction[] {
  let retorno = [];
  //Se operador lógico e for outro BinaryOperation, chama novamente
  if (bo.operator == '&&' || bo.operator == '||') {
    if (isBO(bo.left)) {
      retorno = retorno.concat(
        getBinaryOperationRestriction(truffleProject, contract, <BinaryOperation>bo.left, functionDefinition)
      );
    } else if (isUO(bo.left)) {
      retorno = retorno.concat(getUnaryOperationRestriction(truffleProject, contract, bo.left, functionDefinition));
    }
    //Se operador lógico e for outro BinaryOperation, chama novamente
    if (isBO(bo.right)) {
      retorno = retorno.concat(
        getBinaryOperationRestriction(truffleProject, contract, <BinaryOperation>bo.right, functionDefinition)
      );
    } else if (isUO(bo.right)) {
      retorno = retorno.concat(getUnaryOperationRestriction(truffleProject, contract, bo.right, functionDefinition));
    }
    //se for o operador || (OR), apaga randomicamente um dos dois inseridos. Sem isso, pode entrar em contradição (aconteceu no swaps)
    if (bo.operator == '||') {
      let deleteIndex = Math.random() >= 0.5 ? retorno.length - 1 : retorno.length - 2;
      retorno.splice(deleteIndex, 1);
    }
  } else {
    const functionGraph = getFunctionGraph(truffleProject.getGraph(), contract, functionDefinition);
    const left = getRestrictionValue(truffleProject, functionGraph, bo.left);
    if (left != null && left.value != null && left.elementType != RestrictionElementType.unhandled) {
      const right = getRestrictionValue(truffleProject, functionGraph, bo.right);
      if (right != null && right.value != null && right.elementType != RestrictionElementType.unhandled) {
        retorno.push({
          operator: bo.operator,
          left: left,
          right: right,
          from: BranchType.notDefined,
        });
      }
    }
  }
  return retorno;
}
