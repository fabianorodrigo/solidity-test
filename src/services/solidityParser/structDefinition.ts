import { ContractDefinition, StructDefinition } from 'solidity-parser-antlr';

/** *
 * Receive a contractDefinition and return a array with all Structs
 *
 * @param {contractDefinition} contractDefinition A instance of the contractDefinition in the AST from which the structDefinition will be retrieved
 * @returns {Array} Array of StructDefinition
 */
export function getStructs(contractDefinition: ContractDefinition): StructDefinition[] {
  const structsReturn: StructDefinition[] = [];
  contractDefinition.subNodes.forEach(sn => {
    // Include all functions (except constructor and privates)
    if (sn.type === 'StructDefinition') {
      structsReturn.push(sn);
    }
  });
  return structsReturn;
}
