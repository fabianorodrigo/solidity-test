import { FunctionDefinition, ContractDefinition } from 'solidity-parser-antlr';
import { IRestriction, IRestrictionValue } from '../../models/IResctriction';

import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { getRestrictionValue } from './restrictionValue';
import { BranchType } from '../../models/BranchType';
import { RestrictionElementType } from '../../models/RestrictionElementType';
import { getFunctionGraph } from '../TruffleProjectGraph';

const rvTrue: IRestrictionValue = {
  value: {
    isCode: false,
    js: 'true',
    sol: 'true',
    obj: true,
  },
  elementType: RestrictionElementType.Literal,
};

/**
 * Analyse the BinaryOperation {bo} and return the restrictions to the parameter {paramName} in it
 *
 * Consider only 'require' by now
 *
 * @param {object} truffleProject Instance of Truffle Project under analysis
 * @param {BinaryOperation} uo Binary Operation to be analysed
 * @param {Array} inputParameterNames Parameter names that will be considered in the identification of branches of interest
 * @param {FunctionDefinition} functionDetinition Reference to the function where the {bo} belongs
 * @returns {Array} List of restrictions found
 */
export function getUnaryOperationRestriction(
  truffleProject: TruffleProjectIceFactoryFunction,
  contract: ContractDefinition,
  uo: any,
  functionDefinition: FunctionDefinition
): IRestriction[] {
  let retorno = [];

  const left = getRestrictionValue(
    truffleProject,
    getFunctionGraph(truffleProject.getGraph(), contract, functionDefinition),
    uo.subExpression ? uo.subExpression : uo
  );
  if (left != null && left.value != null) {
    retorno.push({
      operator: uo.operator == '!' ? '!=' : '==',
      left: left,
      right: rvTrue,
      from: BranchType.notDefined,
    });
  }

  return retorno;
}
