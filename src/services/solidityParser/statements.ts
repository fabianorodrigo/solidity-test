import {
  Statement,
  Expression,
  FunctionDefinition,
  Identifier,
  ContractDefinition,
  BinaryOperation,
} from 'solidity-parser-antlr';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { ITruffleProjectGraph, IContractGraph, IFunctionGraph } from '../../models/graph';
import { getFunctionSignature } from '../TruffleProjectGraph';

const transactionParameterAttributes = ['sender', 'value'];
const txParameterAttributes = ['origin', 'gasprice'];

/**
 * Returns true if the statement {st} is a IfStatement
 *
 * @param {ExpressionStatement} st The statement in the AST under analysis
 */
export function isIdentifier(st: Expression) {
  return st && st.type == 'Identifier';
}

export function isNumberLiteral(st: Expression) {
  return st.type == 'NumberLiteral';
}

/**
 * Returns true if the statement {st} is a IfStatement
 *
 * @param {ExpressionStatement} st The statement in the AST under analysis
 */
export function isIf(st: Statement) {
  return st.type == 'IfStatement';
}

/**
 * Returns true if the statement {st} is a ForStatement
 *
 * @param {ExpressionStatement} st The statement in the AST under analysis
 */
export function isFor(st: Statement) {
  return st.type == 'ForStatement';
}

/**
 * Returns true if the statement {st} is a WhileStatement
 *
 * @param {ExpressionStatement} st The statement in the AST under analysis
 */
export function isWhile(st: Statement) {
  return st.type == 'WhileStatement';
}

/**
 * Returns true if the statement {st}} is a call to the 'require' function and it's first
 * argument is a BinaryOperation
 *
 * @param {ExpressionStatement} st The statement in the AST under analysis
 */
export function isRequireCall(st) {
  return isFunctionCall(st) && st.expression.expression.name == 'require';
}

/**
 * Returns true if the statement {st}} is a call to the 'require' function and it's first
 * argument is a BinaryOperation
 *
 * @param {ExpressionStatement} st The statement in the AST under analysis
 */
export function isAssertCall(st) {
  return isFunctionCall(st) && st.expression.expression.name == 'assert';
}

/**
 * Returns true if the statment {st} is a IF and it has a call to revert() as body statement
 * What is, technically, the same as a 'require'
 * @param {ExpressionStatement} st The statement in the AST under analysis
 */
export function isIfWithRevert(st) {
  return (
    isIf(st) &&
    ((Array.isArray(st.trueBody.statements) &&
      st.trueBody.statements.length > 0 &&
      isFunctionCall(st.trueBody.statements[0]) &&
      st.trueBody.statements[0].expression.expression.name == 'revert') ||
      (isFunctionCall(st.trueBody) && st.trueBody.expression.expression.name == 'revert'))
  );
}

/**
 * Returns true if the statement {st} has type as 'FunctionDefinition'
 *
 * @param {ExpressionStatement} st The statement in the AST under analysis
 */
export function isFunction(st) {
  return st.type == 'FunctionDefinition';
}

/**
 * Returns true if the statement {st} is a FunctionCall
 *
 * @param {ExpressionStatement} st The statement in the AST under analysis
 */
export function isFunctionCall(st) {
  return (
    (st.type == 'ExpressionStatement' || st.type == 'ReturnStatement') &&
    st.expression &&
    st.expression.type == 'FunctionCall' &&
    st.expression.expression.type == 'Identifier'
  );
}

/**
 * Returns true if the statement {st} is a call  of a function in another contract
 *
 * @param {ITruffleProjectGraph} graph of the project being analysed
 * @param {ExpressionStatement} st The statement in the AST under analysis
 */
export function isAnotherContractFunctionCall(graph: ITruffleProjectGraph, st): boolean {
  return (
    st.type == 'ExpressionStatement' &&
    st.expression.type == 'FunctionCall' &&
    st.expression.expression.type == 'MemberAccess' &&
    st.expression.expression.expression.type == 'Identifier' &&
    graph.contracts[st.expression.expression.expression.name] != null
  );
}

/**
 * Returns true if the statement {st} is a calling  a member (getter) in another contract
 * something like: GasPriceOracle(addressOfGasPrice).maxGa()
 *
 * @param {IFunctionGraph} function graph being analysed
 * @param {ExpressionStatement} st The statement in the AST under analysis
 */
export function isAnotherContractMemberAccess(functionGraph: IFunctionGraph, st): boolean {
  return (
    st.type == 'FunctionCall' &&
    st.arguments.length == 0 &&
    st.expression.type == 'MemberAccess' &&
    isIdentifier(st.expression.expression.expression) &&
    functionGraph.contract.projectGraph.contracts[st.expression.expression.expression.name] != null &&
    functionGraph.contract.projectGraph.contracts[st.expression.expression.expression.name].stateVariables[
      st.expression.memberName
    ] != null &&
    st.expression.expression.type == 'FunctionCall' &&
    st.expression.expression.arguments?.length == 1 &&
    isIdentifier(st.expression.expression.arguments[0]) &&
    (functionGraph.parameters.find(
      (pg) => pg.isAddressContract && pg.name == st.expression.expression.arguments[0].name
    ) != null ||
      Object.values(functionGraph.contract.stateVariables).find(
        (sv) => sv.isAddressContract && sv.name == st.expression.expression.arguments[0].name
      ) != null)
  );
}

/**
 * Returns true if the statement {st} is a FunctionCall using keywork 'super'
 *
 * @param {ExpressionStatement} st The statement in the AST under analysis
 */
export function isSuperFunctionCall(st) {
  return (
    st.type == 'ExpressionStatement' &&
    st.expression.type == 'FunctionCall' &&
    st.expression.expression.type == 'MemberAccess' &&
    st.expression.expression.expression.type == 'Identifier' &&
    st.expression.expression.expression.name == 'super'
  );
}

/**
 * Returns true if the statement {st} is a FunctionCall that only returns a literal or identifier
 *
 * @param {ExpressionStatement} st The statement in the AST under analysis
 */
export function isGetterFunctionCall(truffleProject: TruffleProjectIceFactoryFunction, contract: IContractGraph, st) {
  const expression = getGetterReturn(truffleProject, contract, st);
  return expression != null;
}

/**
 * Returns the returned expression if the statment {st} is a function call
 * thas just return a Identifier or a Literal as body statement
 * @param {ExpressionStatement} st The statement in the AST under analysis
 */
export function getGetterReturn(truffleProject: TruffleProjectIceFactoryFunction, contract: IContractGraph, st) {
  if (st.type == 'FunctionCall' && isIdentifier(st.expression)) {
    const functionCalled = getFunctionCalled(truffleProject, contract.contractDefinition, st);
    if (
      functionCalled &&
      functionCalled.body.statements.length == 1 &&
      functionCalled.body.statements[0].type == 'ReturnStatement' &&
      (isIdentifier(functionCalled.body.statements[0].expression) ||
        isLiteral(functionCalled.body.statements[0].expression))
    ) {
      return functionCalled.body.statements[0].expression;
    }
  }
  return null;
}

export function isExpressionStatement(st) {
  return st.type == 'ExpressionStatement';
}

/**
 * Returns true if the element {e} has a property 'type' with value 'BinaryOperation'
 *
 * @param {object} e The element in the AST under analysis
 */
export function isBOExpressionStatement(e) {
  return e.type == 'ExpressionStatement' && e.expression.type == 'BinaryOperation';
}

/**
 * Returns true if the element {e} has a property 'type' with value 'BinaryOperation'
 *
 * @param {object} e The element in the AST under analysis
 */
export function isBO(e) {
  return e.type == 'BinaryOperation';
}

/**
 * Returns true if the element {e} has a property 'type' with value 'UnaryOperation'
 *
 * @param {object} e The element in the AST under analysis
 */
export function isUO(e) {
  return e.type == 'UnaryOperation';
}

/**
 * Returns true if the statment {st} is a function call thas just return a Identifier as body statement
 * @param {ExpressionStatement} st The statement in the AST under analysis
 */
export function isBOorUOGetter(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractDefinition: ContractDefinition,
  st
): boolean {
  const expression = getBOorUOGetter(truffleProject, contractDefinition, st);
  return expression != null;
}

/**
 * Returns BinaryOrUOOperation if the statment {st} is a function call
 * thas just return a BinaryOperation or UnaryOperation as body statement
 * @param {ExpressionStatement} st The statement in the AST under analysis
 */
export function getBOorUOGetter(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractDefinition: ContractDefinition,
  st
) {
  if (st.type == 'FunctionCall' && isIdentifier(st.expression)) {
    const functionCalled = getFunctionCalled(truffleProject, contractDefinition, st);
    if (
      functionCalled &&
      functionCalled.body.statements.length == 1 &&
      functionCalled.body.statements[0].type == 'ReturnStatement' &&
      (isBO(functionCalled.body.statements[0].expression) || isUO(functionCalled.body.statements[0].expression))
    ) {
      return functionCalled.body.statements[0].expression;
    }
  }
  return null;
}

function getFunctionCalled(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractDefinition: ContractDefinition,
  st
) {
  const { contract, funcDef } = truffleProject.getFunctionFromContractDefinition(
    contractDefinition,
    st.expression.name,
    st.arguments.length //TODO: ainda não é suficiente para cobrir overload, apenas alguns casos
  );
  return funcDef;
}
/**
 * Returns true if the statement {st} has a property 'type' with value 'StateVariableDeclaration'
 *
 * @param {object} st The element in the AST under analysis
 */
export function isStateVariableDeclaration(st) {
  return st.type == 'StateVariableDeclaration';
}

/**
 * Returns true if the statement {st} has a property 'type' with value 'VariableDeclarationStatement'
 *
 * @param {object} st The element in the AST under analysis
 */
export function isVariableDeclaration(st) {
  return st.type == 'VariableDeclarationStatement';
}
/**
 * Return true if the statement {st} has a property 'type' with value 'MemberAccess' and property 'expression.type'
 * with a value 'Identifier' and property 'expression.name' with value 'msg' or 'tx', and a memberName valid for those
 * @param {object} st The element in the AST under analysis
 */
export function isTransactionParameterMemberAccess(st) {
  return (
    isMemberAccess(st) &&
    st.expression.type == 'Identifier' &&
    st.memberName &&
    ((st.expression.name == 'msg' && transactionParameterAttributes.includes(st.memberName)) ||
      (st.expression.name == 'tx' && txParameterAttributes.includes(st.memberName)))
  );
}
/**
 * Return true if the statement {st} has a property 'type' with value 'MemberAccess' and property 'expression.type'
 * with a value 'Identifier' and property 'expression.name' with value that is {paramName}
 * @param {object} st The element in the AST under analysis
 * @param {string|array} paramName Name or array of names of parameter that is being veried if it's member is being accessed
 */
export function isParameterMemberAccess(st, paramName) {
  return (
    isMemberAccess(st) &&
    st.expression.type == 'Identifier' &&
    ((Array.isArray(paramName) && paramName.includes(st.expression.name)) ||
      (!Array.isArray(paramName) && st.expression.name == paramName))
  );
}

/**
 * Return true if the statement {st} has a property 'type' with value 'MemberAccess' and property 'expression.type'
 * with a value 'Identifier' and property 'expression.name' with value that is {paramName}
 * @param {object} st The element in the AST under analysis
 * @param {string|array} paramName Name or array of names of parameter that is being veried if it's member is being accessed
 */
export function isParameterIndexAccess(st, paramName) {
  return (
    isIndexAccess(st) &&
    st.base.type == 'Identifier' &&
    ((Array.isArray(paramName) && paramName.includes(st.base.name)) ||
      (!Array.isArray(paramName) && st.base.name == paramName))
  );
}

/**
 * Return true if the statement {st} has a property 'type' with value 'MemberAccess'
 * @param {object} st The element in the AST under analysis
 */
export function isMemberAccess(st) {
  return st && st.type == 'MemberAccess';
}

/**
 * Return true if the statement {st} has a property 'type' with value 'TupleExpression'
 * @param {object} st The element in the AST under analysis
 */
export function isTupleExpression(st) {
  return st && st.type == 'TupleExpression';
}

/**
 * Return true if the statement {st} has a property 'type' with value 'IndexAccess'
 * @param {object} st The element in the AST under analysis
 */
export function isIndexAccess(st) {
  return st.type == 'IndexAccess';
}

/**
 * Return true if the statement {st} has a property 'type' with value 'MemberAccess', a property 'expression.type' with value 'identifier'
 * and a property 'expression.name' and 'memberName'
 * @param {object} truffleProject Instance of Truffle Project under analysis
 * @param {object} st The element in the AST under analysis
 */
export function isEnumMemberAccess(graph: ITruffleProjectGraph, st) {
  return (
    isMemberAccess(st) &&
    st.expression.type == 'Identifier' &&
    st.expression.name != null &&
    st.memberName != null &&
    graph.enums[st.expression.name] != null
  );
}

/**
 * Return TRUE is it has property 'number' or 'value'
 * @param st
 */
export function isLiteral(st) {
  return st.hasOwnProperty('number') || st.hasOwnProperty('value');
}

/**
 * Return TRUE is it is a functioncall 'address(param)'
 * @param st
 */
export function isAddressFunctionCall(st) {
  return (
    st.type == 'FunctionCall' &&
    st.expression.type == 'ElementaryTypeNameExpression' &&
    st.expression.typeName.type == 'ElementaryTypeName' &&
    st.expression.typeName.name == 'address'
  );
}

/**
 * Return TRUE is it is a functioncall 'address(0)'
 * @param st
 */
export function isAddressZeroFunctionCall(st) {
  return (
    isAddressFunctionCall(st) &&
    st.arguments[0].type == 'NumberLiteral' &&
    ['0', '0x0', '0x0000000000000000000000000000000000000000'].includes(st.arguments[0].number)
  );
}

/**
 * Return true if the statement is a Identifier and has the same name as one of the {functionDefinition} parameters
 * @param st
 * @param functionDefinition
 */
export function isFunctionParameterReference(st, functionDefinition: FunctionDefinition) {
  return (
    isIdentifier(st) &&
    functionDefinition.parameters &&
    functionDefinition.parameters.findIndex((p) => p.name == (<Identifier>st).name) > -1
  );
}

/**
 * Return true if the statement is a MemberAccess and has the same name as one of the {functionDefinition} parameters
 * @param st
 * @param functionDefinition
 */
export function isFunctionParameterMemberReference(st, functionDefinition: FunctionDefinition) {
  return (
    functionDefinition.parameters &&
    isParameterMemberAccess(
      st,
      functionDefinition.parameters.map((p) => p.name)
    )
  );
}

/**
 * Return true if the statement is a MemberAccess and has the same name as one of the {functionDefinition} parameters
 * @param st
 * @param functionDefinition
 */
export function isFunctionParameterIndexReference(st, functionDefinition: FunctionDefinition) {
  return (
    functionDefinition.parameters &&
    isParameterIndexAccess(
      st,
      functionDefinition.parameters.map((p) => p.name)
    )
  );
}

/**
 * Return true if the statement is  a state variable of {contract} or it inherits it
 * @param st
 * @param functionDefinition
 */
export function isStateVariableReference(contract: IContractGraph, st) {
  return (
    isIdentifier(st) &&
    contract.projectGraph.contracts[contract.name] &&
    (contract.projectGraph.contracts[contract.name].stateVariables[(<Identifier>st).name] != null ||
      Object.values(contract.projectGraph.contracts[contract.name].inheritsFrom).some(
        (parent) => parent.contract.stateVariables[(<Identifier>st).name] != null
      ))
  );
}

/**
 * Return true if the statement is  a local variable of {function}
 * @param st
 * @param functionDefinition
 */
export function isLocalVariableReference(functionGraph: IFunctionGraph, st) {
  if (
    isIdentifier(st) &&
    functionGraph.contract.projectGraph.contracts[functionGraph.contract.name] &&
    functionGraph.contract.projectGraph.contracts[functionGraph.contract.name].functions[functionGraph.name]
  ) {
    const funcGraph = functionGraph.contract.projectGraph.contracts[functionGraph.contract.name].functions[
      functionGraph.name
    ].find(
      (f) =>
        f.signature ==
        getFunctionSignature(functionGraph.contract.projectGraph, functionGraph.functionDefinition, functionGraph.name)
    );
    return funcGraph != null && funcGraph.localVariables.find((lv) => lv.name == (<Identifier>st).name) != null;
  }
  return false;
}

/**
 * Return true if the statement is  a state variable of {contract} or it inherits it
 * @param st
 * @param functionDefinition
 */
export function getContractStateVariableReferenceGraph(
  truffleProject: TruffleProjectIceFactoryFunction,
  contract: IContractGraph,
  name: string
): IContractGraph {
  if (contract.stateVariables[name] != null) return contract;

  const ancestrais = Object.values(contract.inheritsFrom);

  for (let i = 0; i < ancestrais.length; i++) {
    const subresult = getContractStateVariableReferenceGraph(truffleProject, ancestrais[i].contract, name);
    if (subresult) return subresult;
  }

  return null;
}

/**
 * Return true if the statement is  a member of state variable of {contract}
 * @param st
 * @param functionDefinition
 */
export function isStateVariableMemberReference(contract: IContractGraph, st) {
  return (
    isMemberAccess(st) && contract.projectGraph.contracts[contract.name].stateVariables[st.expression.name] != null
  );
}

/**
 * Return true if the statement is  a access of indexed state variable of {contract}
 * @param st
 * @param functionDefinition
 */
export function isStateVariableIndexReference(contract: IContractGraph, st) {
  return isIndexAccess(st) && contract.projectGraph.contracts[contract.name].stateVariables[st.base.name] != null;
}

/**
 * Returns the statement own {st} one it's nested statements that represents
 * a division denominator. It's identified by 'div' MemberAccess FunctionCall
 * or by Math operatot '/'
 *
 * @param {ExpressionStatement} st The statement in the AST under analysis
 */
export function getDivisionDenominator(st) {
  if (isBO(st) && st.operator == '/') {
    return (<BinaryOperation>st).right;
  } else if (st.type == 'FunctionCall' && isMemberAccess(st.expression) && st.expression.memberName == 'div') {
    return st.arguments[0];
  } else if (st.expression != null) {
    const sub = getDivisionDenominator(st.expression);
    if (sub != null) {
      return sub;
    }
  }
  return null;
}

/**
 * Returns true if the element {st} is a call for the 'push' of an array
 *
 * @param {object} st The element in the AST under analysis
 */
export function isPushArray(st) {
  return (
    (st.type == 'ExpressionStatement' || st.type == 'ReturnStatement') &&
    st.expression &&
    st.expression.type == 'FunctionCall' &&
    isIdentifier(st.expression.expression.expression) &&
    isMemberAccess(st.expression.expression) &&
    st.expression.expression.memberName == 'push' &&
    st.expression.arguments.length == 1
  );
}

/**
 * Remove ao properties with name 'loc'
 */
export function cleanLocData(obj) {
  if (obj == null) return;
  Object.keys(obj).forEach((k) => {
    if (k == 'loc') {
      delete obj.loc;
    } else if (obj[k]) {
      if (typeof obj[k] == 'object') {
        cleanLocData(obj[k]);
      }
    }
  });
}
