import { ContractDefinition, EnumDefinition } from 'solidity-parser-antlr';

/** *
 * Receive a contractDefinition and return a array with all Enums
 *
 * @param {contractDefinition} contractDefinition A instance of the contractDefinition in the AST from which the structDefinition will be retrieved
 * @returns {Array} Array of StructDefinition
 */
export const getEnums = function getEnums(contractDefinition: ContractDefinition): EnumDefinition[] {
  const enumsReturn: EnumDefinition[] = [];
  contractDefinition.subNodes.forEach(sn => {
    // Include all functions (except constructor and privates)
    if (sn.type === 'EnumDefinition') {
      enumsReturn.push(sn);
    }
  });
  return enumsReturn;
};
