import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { IContract } from '../../models/IContract';

/** *
 * Receive a contractDefinition and return a array with all it's functions BUT contructor
 *
 * @param {contractDefinition} contractDefinition A instance of the contractDefinition in the AST from which the functionsDefinition will be retrieved
 * @param {boolean} includePrivate If TRUE, return also the private functions. Default is FALSE
 * @returns {Array} Array of FunctionDefinition
 */
export function getFunctions(contractDefinition: ContractDefinition, includePrivate = false): FunctionDefinition[] {
  const functionsReturn: FunctionDefinition[] = [];
  contractDefinition.subNodes.forEach((sn) => {
    // Include all functions (except constructor, privates and with no body)
    if (
      sn.type === 'FunctionDefinition' &&
      !sn.isConstructor &&
      sn.body != null &&
      (includePrivate || sn.visibility != 'private')
    ) {
      functionsReturn.push(sn);
    }
  });
  return functionsReturn;
}

/** *
 * Receive a contractDefinition and return a array with all the functions it inherits
 *
 * @param {contractDefinition} contractDefinition A instance of the contractDefinition in the AST
 * @param {boolean} includePrivate If TRUE, return also the private functions. Default is FALSE
 * @param {array} contracts List of all contracts where it will be searched by definitions of baseContracts (contracts it inhrerits from)
 * @returns {Array} Array of FunctionDefinition
 */
export function getInheritedFunctions(
  contractDefinition: ContractDefinition,
  includePrivate = false,
  contracts: IContract[]
): FunctionDefinition[] {
  let functionsReturn = [];
  contractDefinition.baseContracts.forEach((parent) => {
    const foundParentContract = contracts.find((c) => c.contractDefinition.name == parent.baseName.namePath);
    if (foundParentContract) {
      if (foundParentContract.contractDefinition.baseContracts.length > 0) {
        functionsReturn = functionsReturn.concat(
          getInheritedFunctions(foundParentContract.contractDefinition, includePrivate, contracts)
        );
      }
      functionsReturn = functionsReturn.concat(getFunctions(foundParentContract.contractDefinition, includePrivate));
    }
  });
  return functionsReturn;
}
