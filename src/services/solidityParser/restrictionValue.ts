import {
  FunctionDefinition,
  Identifier,
  BooleanLiteral,
  HexLiteral,
  StringLiteral,
  NumberLiteral,
  MemberAccess,
  IndexAccess,
  TypeName,
  FunctionCall,
} from 'solidity-parser-antlr';
import path from 'path';
import { IRestrictionValue } from '../../models/IResctriction';
import {
  isTransactionParameterMemberAccess,
  isEnumMemberAccess,
  isMemberAccess,
  isLiteral,
  isFunctionParameterReference,
  isFunctionParameterMemberReference,
  isStateVariableReference,
  isStateVariableMemberReference,
  isStateVariableIndexReference,
  isAddressFunctionCall,
  isAddressZeroFunctionCall,
  isGetterFunctionCall,
  getGetterReturn,
  isFunctionParameterIndexReference,
  isBO,
  isLocalVariableReference,
  isAnotherContractMemberAccess,
} from './statements';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { RestrictionElementType } from '../../models/RestrictionElementType';
import { ElementTypeUINT } from '../types';
import { IContractGraph, IFunctionGraph } from '../../models/graph';

/**
 * If it has a valid value restriction, return it. If not, returns null
 *
 * @param {object} truffleProject Instance of Truffle Project under analysis
 * @param {ContractDefinition} contract ContractDefinition being analysed
 * @param {FunctionDefinition} functionDefinition FunctionDefinition in the AST being analysed
 * @param {object} sideOperation  A left or right atribute of a BinaryOperation
 */
export function getRestrictionValue(
  truffleProject: TruffleProjectIceFactoryFunction,
  functionGraph: IFunctionGraph,
  sideOperation
): IRestrictionValue {
  function push(info) {
    global['memberTypes'].push([
      path.basename(functionGraph.contract.projectGraph.truffleProjectHome),
      functionGraph.contract.name,
      functionGraph.name,
      info,
    ]);
  }
  if (global['memberTypes'] == null) {
    global['memberTypes'] = [];
  }
  if (isLiteral(sideOperation)) {
    push(RestrictionElementType.Literal);
    return getLiteralRestrictionValue(sideOperation);
  } else if (isEnumMemberAccess(functionGraph.contract.projectGraph, sideOperation)) {
    push(RestrictionElementType.EnumMember);
    return getEnumMemberRestrictionValue(functionGraph.contract, sideOperation);
  } else if (
    isMemberAccess(sideOperation) &&
    sideOperation.memberName == 'number' &&
    sideOperation.expression.name == 'block'
  ) {
    push(RestrictionElementType.BlockMember);
    return {
      value: { isCode: true, js: '(await web3.eth.getBlockNumber())', sol: 'block.number', obj: '###null###' },
      elementType: RestrictionElementType.BlockMember,
    };
  } else if (
    isMemberAccess(sideOperation) &&
    sideOperation.memberName == 'timestamp' &&
    sideOperation.expression.name == 'block'
  ) {
    push(RestrictionElementType.BlockMember);
    return {
      value: {
        isCode: true,
        js: '(await web3.eth.getBlock(await web3.eth.getBlockNumber())).timestamp',
        sol: 'block.timestamp',
        obj: '###null###',
      },
      elementType: RestrictionElementType.BlockMember,
    };
  } else if (sideOperation.name == 'now' && sideOperation.type == 'Identifier') {
    push(RestrictionElementType.BlockMember);
    return {
      value: {
        isCode: true,
        js: '(await web3.eth.getBlock(await web3.eth.getBlockNumber())).timestamp',
        sol: 'block.timestamp',
        obj: '###null###',
      },
      elementType: RestrictionElementType.BlockMember,
    };
  } else if (isAddressFunctionCall(sideOperation)) {
    if (isAddressZeroFunctionCall(sideOperation)) {
      push('address(0)');
      return {
        value: {
          isCode: false,
          js: '0x0000000000000000000000000000000000000000',
          sol: '0x0000000000000000000000000000000000000000',
          obj: '0x0000000000000000000000000000000000000000',
        },
        elementType: RestrictionElementType.Literal,
      }; //typeof(accounts[0]) == 'string'
    } else if (sideOperation.arguments[0].type == 'Identifier') {
      push('address(Identifier)');
      //TODO: Cenário ainda não tratado quando se passa uma variável para a função nativa 'address'
      return {
        value: null,
        elementType: null,
      };
    } else {
      push('address(?)');
      //TODO: Cenário ainda não tratado. Existe caso no Oraclize em que o argumento é o resultado
      //de uma chamada a função keccak256
      return {
        value: null,
        elementType: null,
      };
    }
  } else if (isFunctionParameterReference(sideOperation, functionGraph.functionDefinition)) {
    push(RestrictionElementType.functionParameter);
    return getFunctionParameterRestrictionValue(sideOperation, functionGraph.functionDefinition);
  } else if (isFunctionParameterMemberReference(sideOperation, functionGraph.functionDefinition)) {
    push(RestrictionElementType.functionParameterMember);
    return getFunctionParameterMemberRestrictionValue(sideOperation, functionGraph.functionDefinition);
  } else if (isFunctionParameterIndexReference(sideOperation, functionGraph.functionDefinition)) {
    push(RestrictionElementType.functionParameterIndex);
    return getFunctionParameterIndexRestrictionValue(sideOperation);
  } else if (isTransactionParameterMemberAccess(sideOperation)) {
    push(RestrictionElementType.transactionParameter);
    return getTransactionParameterMemberRestrictionValue(sideOperation);
  } else if (isStateVariableReference(functionGraph.contract, sideOperation)) {
    push(RestrictionElementType.stateVariable);
    return getStateVariableRestrictionValue(sideOperation);
  } else if (isStateVariableMemberReference(functionGraph.contract, sideOperation)) {
    push(RestrictionElementType.stateVariableMember);
    return getStateVariableMemberRestrictionValue(sideOperation);
  } else if (isStateVariableIndexReference(functionGraph.contract, sideOperation)) {
    push(RestrictionElementType.stateVariableIndex);
    return getStateVariableIndexRestrictionValue(sideOperation);
  } else if (isLocalVariableReference(functionGraph, sideOperation)) {
    push(RestrictionElementType.localVariable);
    return getLocalVariableRestrictionValue(sideOperation);
  } else if (isGetterFunctionCall(truffleProject, functionGraph.contract, sideOperation)) {
    const x = getRestrictionValue(
      truffleProject,
      functionGraph,
      getGetterReturn(truffleProject, functionGraph.contract, sideOperation)
    );
    if (x != null && x.elementType != RestrictionElementType.unhandled) {
      push('functionGetter('.concat(x.elementType, ')'));
      return x;
    } else {
      push(getTipoNaoIdentificado(sideOperation, truffleProject, functionGraph));
    }
  } else if (isBO(sideOperation) && ['+', '-', '*', '/', '%'].includes(sideOperation.operator)) {
    const left = getRestrictionValue(truffleProject, functionGraph, sideOperation.left);
    const right = getRestrictionValue(truffleProject, functionGraph, sideOperation.right);
    return {
      value: {
        isCode: false,
        js: `${left.value.js} ${sideOperation.operator} ${right.value.js}`,
        sol: `${left.value.sol} ${sideOperation.operator} ${right.value.sol}`,
        obj: '###null###',
      },
      elementType: RestrictionElementType.unhandled,
    };
  } else if (isAnotherContractMemberAccess(functionGraph, sideOperation)) {
    return getAnotherContractMemberReference(sideOperation.expression);
  } else {
    const unhandledValue = getTipoNaoIdentificado(sideOperation, truffleProject, functionGraph);
    push(unhandledValue);
    return {
      value: {
        isCode: false,
        js: unhandledValue,
        sol: unhandledValue,
        obj: unhandledValue,
      },
      elementType: RestrictionElementType.unhandled,
    };
  }
  return null;
}

function getTipoNaoIdentificado(
  st,
  truffleProject: TruffleProjectIceFactoryFunction,
  functionGraph: IFunctionGraph
): string {
  let tipo = '';
  if (st != null && st.type) {
    if (isBO(st)) {
      tipo += getTipoNaoIdentificado(st.left, truffleProject, functionGraph).concat(
        ' ',
        st.operator,
        ' ',
        getTipoNaoIdentificado(st.right, truffleProject, functionGraph)
      );
    } else {
      if (isFunctionParameterReference(st, functionGraph.functionDefinition)) {
        tipo += RestrictionElementType.functionParameter;
      } else if (isFunctionParameterMemberReference(st, functionGraph.functionDefinition)) {
        tipo += RestrictionElementType.functionParameterMember;
      } else if (isFunctionParameterIndexReference(st, functionGraph.functionDefinition)) {
        tipo += RestrictionElementType.functionParameterMember;
      } else if (isTransactionParameterMemberAccess(st)) {
        tipo += RestrictionElementType.transactionParameter;
      } else if (isStateVariableReference(functionGraph.contract, st)) {
        tipo += RestrictionElementType.stateVariable;
      } else if (isStateVariableMemberReference(functionGraph.contract, st)) {
        tipo += RestrictionElementType.stateVariableMember;
      } else if (isStateVariableIndexReference(functionGraph.contract, st)) {
        tipo += RestrictionElementType.stateVariableIndex;
      } else if (st.type == 'TupleExpression') {
        st.components.forEach((c, i) => {
          tipo += getTipoNaoIdentificado(c, truffleProject, functionGraph);
          if (i > 0) tipo += ' # ';
        });
      } else if (st.type == 'FunctionCall') {
        tipo += st.type;
      } else {
        tipo += st.type;
        const subtipo = getTipoNaoIdentificado(st.expression || st.base, truffleProject, functionGraph);
        if (subtipo != null) {
          tipo += '.' + subtipo;
        }
      }
    }
  } else {
    return null;
  }
  return tipo;
}

function getLiteralRestrictionValue(
  sideOperation: BooleanLiteral | HexLiteral | StringLiteral | NumberLiteral
): IRestrictionValue {
  const v = sideOperation.hasOwnProperty('number')
    ? (<NumberLiteral>sideOperation).number
    : (<BooleanLiteral | HexLiteral | StringLiteral>sideOperation).value.toString();
  return {
    value: { isCode: false, js: v, sol: v, obj: sideOperation.hasOwnProperty('number') ? parseInt(v) : v },
    elementType: RestrictionElementType.Literal,
  };
}

export function getNumberLiteralRestrictionValue(value: number): IRestrictionValue {
  return {
    value: { isCode: false, js: value.toString(), sol: value.toString(), obj: value },
    elementType: RestrictionElementType.Literal,
  };
}

function getEnumMemberRestrictionValue(contract: IContractGraph, sideOperation): IRestrictionValue {
  let v = null;
  let enumerador = contract.enums[sideOperation.expression.name];
  if (enumerador != null) {
    v = `${enumerador.contract.name}.${enumerador.name}`;
  } else {
    const ancestrais = Object.values(contract.inheritsFrom);
    for (let i = 0; i < ancestrais.length; i++) {
      const enumParent = getEnumMemberRestrictionValue(ancestrais[i].contract, sideOperation);
      if (enumParent != null) {
        return enumParent;
      }
    }
  }

  if (v == null) return null;

  return {
    value: {
      isCode: false,
      js: v,
      sol: v,
      memberName: sideOperation.memberName,
      obj: v,
    },
    elementType: RestrictionElementType.EnumMember,
  };
}

function getFunctionParameterRestrictionValue(sideOperation: Identifier, functionDefinition: FunctionDefinition) {
  return {
    elementType: RestrictionElementType.functionParameter,
    ...getIdentifierReference(
      sideOperation,
      functionDefinition.parameters.find((p) => p.name == sideOperation.name).typeName
    ),
  };
}

function getFunctionParameterMemberRestrictionValue(
  sideOperation: MemberAccess,
  functionDefinition: FunctionDefinition
) {
  return {
    elementType: RestrictionElementType.functionParameterMember,
    //FIXME: complementar com os os tipos corretos
    ...getMemberReference(sideOperation, getFunctionParameterMemberType(sideOperation, functionDefinition)),
  };
}

function getFunctionParameterMemberType(sideOperation: MemberAccess, functionDefinition: FunctionDefinition): TypeName {
  const param = functionDefinition.parameters.find((p) => p.name == (<Identifier>sideOperation.expression).name);
  if (param != null) {
    //FIXME: contemplar outros tipos (structs, por exemplo)
    return param.typeName.type == 'ArrayTypeName' && sideOperation.memberName == 'length' ? ElementTypeUINT : null;
  }
  return null;
}

function getFunctionParameterIndexRestrictionValue(sideOperation: IndexAccess) {
  return {
    elementType: RestrictionElementType.functionParameterIndex,
    ...getIndexReference(sideOperation),
  };
}

function getTransactionParameterMemberRestrictionValue(sideOperation: MemberAccess) {
  return {
    value: {
      isCode: false,
      js: sideOperation.memberName,
      sol: sideOperation.memberName,
      obj: '###null###',
    },
    elementType: RestrictionElementType.transactionParameter,
  };
}

function getLocalVariableRestrictionValue(sideOperation: Identifier) {
  return {
    elementType: RestrictionElementType.localVariable,
    ...getIdentifierReference(sideOperation),
  };
}

function getStateVariableRestrictionValue(sideOperation: Identifier) {
  return {
    elementType: RestrictionElementType.stateVariable,
    ...getIdentifierReference(sideOperation),
  };
}

function getStateVariableMemberRestrictionValue(sideOperation: MemberAccess) {
  return {
    elementType: RestrictionElementType.stateVariableMember,
    ...getMemberReference(sideOperation),
  };
}

function getStateVariableIndexRestrictionValue(sideOperation: IndexAccess) {
  return {
    elementType: RestrictionElementType.stateVariableIndex,
    ...getIndexReference(sideOperation),
  };
}

function getIdentifierReference(sideOperation: Identifier, typeName?: TypeName) {
  return {
    value: {
      isCode: false,
      js: sideOperation.name,
      sol: sideOperation.name,
      obj: '###null###',
      typeName: typeName,
    },
  };
}

function getMemberReference(sideOperation: MemberAccess, typeName?: TypeName) {
  return {
    value: {
      isCode: false,
      js: (<Identifier>sideOperation.expression).name,
      sol: (<Identifier>sideOperation.expression).name,
      memberName: sideOperation.memberName,
      obj: '###null###',
      typeName: typeName,
    },
  };
}

function getAnotherContractMemberReference(sideOperation: MemberAccess, typeName?: TypeName) {
  return {
    value: {
      isCode: false,
      js: ((<FunctionCall>sideOperation.expression).expression as Identifier).name,
      sol: ((<FunctionCall>sideOperation.expression).expression as Identifier).name,
      memberName: sideOperation.memberName,
      obj: '###null###',
      typeName,
    },
    elementType: RestrictionElementType.otherContractMember,
  };
}

function getIndexReference(sideOperation: IndexAccess) {
  return {
    value: {
      isCode: false,
      js: (<Identifier>sideOperation.base).name,
      sol: (<Identifier>sideOperation.base).name,
      index: sideOperation.index,
      obj: '###null###',
    },
  };
}
