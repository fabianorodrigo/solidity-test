import { DirectionsSubway } from '@material-ui/icons';
import {
  Block,
  ContractDefinition,
  ForStatement,
  FunctionCall,
  FunctionDefinition,
  IfStatement,
  ModifierDefinition,
  VariableDeclaration,
  WhileStatement,
} from 'solidity-parser-antlr';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { searchForElementType } from './searchForElementType';
import { isFor, isFunctionCall, isIf, isRequireCall, isSuperFunctionCall, isWhile } from './statements';

/**
 * Retorna os nomes das funções para as quais foi passado {parameter} direta ou indiretamente
 *
 * @param parent element that has statements to be analysed (function.body, if.body, etc)
 * @param name Parameter name or state variable name that has to be passed to the functionCall in order
 * the function name is returned
 * @param parameterCount If set, only functions with this size of input parameters will be returned
 * @returns function names called passing the name direct on indirectaly
 */
export function getFunctionCallNameReference(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractDefinition: ContractDefinition,
  parent: FunctionDefinition | Block | ModifierDefinition,
  name: string,
  parameterCount?: number
): string[] {
  let result: string[] = [];
  const body: Block = ['FunctionDefinition', 'ModifierDefinition'].includes(parent.type)
    ? (parent as any).body
    : parent;
  let statements = body.statements != null ? body.statements : [body];
  for (let i = 0; i < statements.length; i++) {
    const st = statements[i];
    if (isIf(st)) {
      const subResult = getFunctionCallNameReference(
        truffleProject,
        contractDefinition,
        (<IfStatement>st).trueBody as Block,
        name,
        parameterCount
      );
      if (subResult.length > 0) result = result.concat(subResult);
      if ((<IfStatement>st).falseBody != null) {
        let falseBody = (<IfStatement>st).falseBody;
        if (isIf(falseBody)) falseBody = (falseBody as IfStatement).trueBody;
        const subResult = getFunctionCallNameReference(
          truffleProject,
          contractDefinition,
          falseBody as Block,
          name,
          parameterCount
        );
        if (subResult.length > 0) result = result.concat(subResult);
      }
    } else if (isFor(st)) {
      const subResult = getFunctionCallNameReference(
        truffleProject,
        contractDefinition,
        (<ForStatement>st).body as Block,
        name,
        parameterCount
      );
      if (subResult.length > 0) result = result.concat(subResult);
    }
    //TODO: por que while não tem body?
    /*else if (isWhile(st)){
      const subResult = getFunctionCallNameReference((<WhileStatement>st). as Block, parameter);
      if (subResult.length > 0) result = result.concat(subResult);
    }*/

    //FIXME: Testar
    const functionCalls = searchForElementType(st, ['FunctionCall']);
    functionCalls
      .filter(
        (fc) =>
          (parameterCount == null || (<FunctionCall>fc).arguments.length == parameterCount) &&
          fc.arguments.find((arg) => arg?.name == name) != null
      )
      .forEach((fc) => {
        result.push(fc.expression.name);
      });

    if (!isRequireCall(st) && (isFunctionCall(st) || isSuperFunctionCall(st))) {
      const isSuper = isSuperFunctionCall(st);
      const { contract: contractCalled, funcDef: functionCalled } = truffleProject.getFunctionFromContractDefinition(
        contractDefinition,
        isSuper ? (st as any).expression.expression.memberName : (st as any).expression.expression.name,
        (st as any).expression.arguments.length, //TODO: ainda não é suficiente para cobrir overload, apenas alguns casos
        isSuper
      );
      //parâmetro analisado é repassado na chamada
      const passedOnParamIndex = (<any>st).expression.arguments.findIndex((arg) => arg?.name == name);
      //Se achar esta outra função no mesmo contrato
      if (functionCalled != null && passedOnParamIndex > -1) {
        const subResult = getFunctionCallNameReference(
          truffleProject,
          contractCalled,
          functionCalled,
          functionCalled.parameters[passedOnParamIndex].name,
          parameterCount
        );
        if (subResult.length > 0) result = result.concat(subResult);
      }
    }
  }
  return result;
}
