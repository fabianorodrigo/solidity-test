import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { IContractGraph } from '../../models/graph';
import { IContract } from '../../models/IContract';

/** *
 * Receive a contract and return its constructor function
 *
 * @param {object} contractDefinition Contract extracted from a AST
 * @param {boolean} onlyPublicORdefault If TRUE, returns only constructor with visibility 'public' or 'default'
 * @returns {FunctionDefinition} Constructor of contract
 */
export function getContractConstructor(
  contractDefinition: ContractDefinition,
  onlyPublicORdefault = false
): FunctionDefinition {
  const constructors: FunctionDefinition[] = <FunctionDefinition[]>(
    contractDefinition.subNodes.filter((n) => n.type == 'FunctionDefinition' && n.isConstructor)
  );
  if (constructors.length == 0) return null;
  if (!onlyPublicORdefault) return constructors[0];

  const accessibleConstructors = constructors.filter(
    (constructor) => constructor.visibility == 'public' || constructor.visibility == 'default'
  );
  if (accessibleConstructors.length == 0) return null;
  return accessibleConstructors[0];
}

/* Contracts are marked as abstract when at least one of their functions lacks an implementation
 * https://solidity.readthedocs.io/en/latest/contracts.html#abstract-contracts
 *
 * @param {contractDefinition} contractDefinition The contract representantion in the AST
 */

export function isAbstractContract(contractDefinition: ContractDefinition): boolean {
  const constructor = getContractConstructor(contractDefinition, false);
  //A constructor set as internal causes the contract to be marked as abstract.
  //https://solidity.readthedocs.io/en/v0.5.3/contracts.html
  return (
    (constructor != null && constructor.visibility == 'internal') ||
    contractDefinition.subNodes.filter((n) => n.type == 'FunctionDefinition').length == 0 ||
    contractDefinition.subNodes.filter((n) => n.type == 'FunctionDefinition' && n.body == null).length > 0
  );
}

/**
 * Função de ordenação dos contratos de acordo com as dependências de seus contrutores
 */
export function sortContractsByConstructor(a: IContractGraph, b: IContractGraph): number {
  const constructorA = getContractConstructor(a.contractDefinition);
  const constructorB = getContractConstructor(b.contractDefinition);
  //os contratos de terceiros (node_modules) são instanciados primeiro quando for o caso
  if (a.isThirdPartyLib > b.isThirdPartyLib) {
    return -1;
  } else if (a.isThirdPartyLib < b.isThirdPartyLib) {
    return 1;
  } else {
    if (a.contractDefinition.kind > b.contractDefinition.kind) {
      return -1;
    } else if (a.contractDefinition.kind < b.contractDefinition.kind) {
      return 1;
    } else {
      if (constructorA != null && constructorB == null) {
        return 1;
      } else if (constructorA == null && constructorB != null) {
        return -1;
      } else if (constructorA == null && constructorB == null) {
        return 0;
      } else {
        if (
          constructorA.parameters.some((p) => p.typeName.type == 'UserDefinedTypeName') ||
          (a.functions['constructor'].some((fg) => fg.parameters.some((p) => p.isAddressContract)) &&
            !constructorB.parameters.some((p) => p.typeName.type == 'UserDefinedTypeName') &&
            !b.functions['constructor'].some((fg) => fg.parameters.some((p) => p.isAddressContract)))
        ) {
          return 1;
        }
        if (
          !constructorA.parameters.some((p) => p.typeName.type == 'UserDefinedTypeName') &&
          !a.functions['constructor'].some((fg) => fg.parameters.some((p) => p.isAddressContract)) &&
          (constructorB.parameters.some((p) => p.typeName.type == 'UserDefinedTypeName') ||
            b.functions['constructor'].some((fg) => fg.parameters.some((p) => p.isAddressContract)))
        ) {
          return -1;
        } else {
          //se algum dos parâmetros do A é endereço de contrato do B ou de algum ancestral de B
          if (
            a.functions['constructor'].some((fg) =>
              fg.parameters.some(
                (pg) =>
                  pg.isAddressContract &&
                  pg.contractAddressOf.some(
                    (cao) => cao == b.name || Object.values(b.inheritsFrom).some((x) => x.contract.name == cao)
                  )
              )
            )
          ) {
            return 1;
          } else if (
            b.functions['constructor'].some((fg) =>
              fg.parameters.some(
                (pg) =>
                  pg.isAddressContract &&
                  pg.contractAddressOf.some(
                    (cao) => cao == a.name || Object.values(a.inheritsFrom).some((x) => x.contract.name == cao)
                  )
              )
            )
          ) {
            return -1;
          } else {
            if (
              !constructorA.parameters.some((p) => p.typeName.type == 'UserDefinedTypeName') &&
              !constructorB.parameters.some((p) => p.typeName.type == 'UserDefinedTypeName')
            ) {
              return 0;
            }
            if (
              constructorA.parameters.some((p) => p.typeName.type == 'UserDefinedTypeName') &&
              constructorB.parameters.some((p) => p.typeName.type == 'UserDefinedTypeName')
            ) {
              return 0; //TODO: Se os dois tem UserDefinedTypeName, então precisa implementar pra checar se um referencia o outro ou algum ancestral
            }
          }
        }
      }
    }
  }
}
