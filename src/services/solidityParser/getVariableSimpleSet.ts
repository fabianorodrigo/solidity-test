import { DirectionsSubway } from '@material-ui/icons';
import {
  Block,
  ContractDefinition,
  ForStatement,
  FunctionCall,
  FunctionDefinition,
  IfStatement,
  ModifierDefinition,
  VariableDeclaration,
  WhileStatement,
} from 'solidity-parser-antlr';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { searchForElementType } from './searchForElementType';
import { isFor, isFunctionCall, isIdentifier, isIf, isRequireCall, isSuperFunctionCall, isWhile } from './statements';

/**
 * Retorna os nomes de variáveis em {variableNames} que são setadas com o valor do parâmetro passado {parameter}
 * direta ou indiretamente
 *
 * @param parent element that has statements to be analysed (function.body, if.body, etc)
 * @param state
 * @param name Parameter name or state variable name that has to be passed to the functionCall in order
 * the function name is returned
 */
export function getVariableSimpleSet(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractDefinition: ContractDefinition,
  variableNames: string[],
  parent: FunctionDefinition | Block | ModifierDefinition,
  name: string
): string[] {
  let result: string[] = [];
  const body: Block = ['FunctionDefinition', 'ModifierDefinition'].includes(parent.type)
    ? (parent as any).body
    : parent;
  let statements = body.statements != null ? body.statements : [body];
  for (let i = 0; i < statements.length; i++) {
    const st = statements[i];
    if (isIf(st)) {
      const subResult = getVariableSimpleSet(
        truffleProject,
        contractDefinition,
        variableNames,
        (<IfStatement>st).trueBody as Block,
        name
      );
      if (subResult.length > 0) result = result.concat(subResult);
      if ((<IfStatement>st).falseBody != null) {
        let falseBody = (<IfStatement>st).falseBody;
        if (isIf(falseBody)) falseBody = (falseBody as IfStatement).trueBody;
        const subResult = getVariableSimpleSet(
          truffleProject,
          contractDefinition,
          variableNames,
          falseBody as Block,
          name
        );
        if (subResult.length > 0) result = result.concat(subResult);
      }
    } else if (isFor(st)) {
      const subResult = getVariableSimpleSet(
        truffleProject,
        contractDefinition,
        variableNames,
        (<ForStatement>st).body as Block,
        name
      );
      if (subResult.length > 0) result = result.concat(subResult);
    }
    //TODO: por que while não tem body?
    /*else if (isWhile(st)){
      const subResult = getFunctionCallNameReference((<WhileStatement>st). as Block, parameter);
      if (subResult.length > 0) result = result.concat(subResult);
    }*/

    //FIXME: Testar
    const binaryOperations = searchForElementType(st, ['BinaryOperation']);
    binaryOperations
      .filter(
        (bo) =>
          bo.operator == '=' &&
          isIdentifier(bo.left) &&
          variableNames.includes(bo.left.name) &&
          isIdentifier(bo.right) &&
          bo.right.name == name
      )
      .forEach((bo) => {
        result.push(bo.left.name);
      });

    if (!isRequireCall(st) && (isFunctionCall(st) || isSuperFunctionCall(st))) {
      const isSuper = isSuperFunctionCall(st);
      const { contract, funcDef: functionCalled } = truffleProject.getFunctionFromContractDefinition(
        contractDefinition,
        isSuper ? (st as any).expression.expression.memberName : (st as any).expression.expression.name,
        (st as any).expression.arguments.length, //TODO: ainda não é suficiente para cobrir overload, apenas alguns casos
        isSuper
      );
      //parâmetro analisado é repassado na chamada
      const passedOnParamIndex = (<any>st).expression.arguments.findIndex((arg) => arg?.name == name);
      //Se achar esta outra função no mesmo contrato
      if (functionCalled != null && passedOnParamIndex > -1) {
        const subResult = getVariableSimpleSet(
          truffleProject,
          contractDefinition,
          variableNames,
          functionCalled,
          functionCalled.parameters[passedOnParamIndex].name
        );
        if (subResult.length > 0) result = result.concat(subResult);
      }
    }
  }
  return result;
}
