import { ContractDefinition, ModifierDefinition } from 'solidity-parser-antlr';

/** *
 * Receive a contractDefinition and return a array with all it's modifiers defined
 *
 * @param {contractDefinition} contractDefinition A instance of the contractDefinition in the AST from which the modifierDefinition will be retrieved
 * @returns {Array} Array of ModifierDefinition
 */
export function getModifiers(contractDefinition: ContractDefinition, includePrivate = false): ModifierDefinition[] {
  const modifiersReturn: ModifierDefinition[] = [];
  contractDefinition.subNodes.forEach((sn) => {
    // Include all functions (except constructor, privates and with no body)
    if (sn.type === 'ModifierDefinition') {
      modifiersReturn.push(sn);
    }
  });
  return modifiersReturn;
}
