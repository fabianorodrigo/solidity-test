import { ContractDefinition, FunctionDefinition, ImportDirective } from 'solidity-parser-antlr';
import { IContract } from '../../models/IContract';

/** *
 * Search for libraries that need to be linked in the contract during migration process
 *
 * @param {contractDefinition} contractDefinition A instance of the contractDefinition in the AST from where the imports will be searched
 * @returns {Array} Array of Import Directives
 */
export function getLinkedLibraries(contractDefinition: ContractDefinition): ImportDirective[] {
  const importsReturn: ImportDirective[] = [];
  contractDefinition.subNodes.forEach(sn => {
    // Include all functions (except constructor, privates and with no body)
    if (sn.type === 'ImportDirective') {
      importsReturn.push(sn);
    }
  });
  return importsReturn;
}
