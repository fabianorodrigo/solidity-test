import {
  IfStatement,
  Location,
  ContractDefinition,
  FunctionDefinition,
  BinaryOperation,
  Block,
} from 'solidity-parser-antlr';
import { IBranch } from '../../models/IBranch';
import { BranchType } from '../../models/BranchType';
import { IRestriction } from '../../models/IResctriction';
import { invertRestriction } from '../Restrictions/invertRestriction';
import { getBinaryOperationRestriction } from './binaryOperation';
import { isIf, isIfWithRevert, isBO, isUO } from './statements';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { bgYellow, red } from 'colors';
import { getUnaryOperationRestriction } from './unaryOperation';
import { getNormalizedLiteralRestrictions } from '../Restrictions';
import { RestrictionElementType } from '../../models/RestrictionElementType';

/**
 * Takes branches that make reference to one input parameter and has condition of type BinaryOperation
 *
 * @param {object} truffleProject Instance of Truffle Project under analysis
 * @param {ContractDefinition} ContractDefinition in AST under analysis
 * @param {FunctionDefinition|Block} parent A body of statements o FUnction or other elements (like IF) in the AST
 * @param {number} level Level of the search for branches/restrictions. The body of a function is level 0, the body of a IF
 * in the body of a function is level 1. This will be important when getting ELSE restrictions when we nested IFs
 * @returns {array} A list with all branches identified in the statements received and
 * the respectives conditions to match them
 */
export function getBranches(
  truffleProject: TruffleProjectIceFactoryFunction,
  contract: ContractDefinition,
  functionDefinition: FunctionDefinition,
  parent: FunctionDefinition | Block,
  level: number = 0
): IBranch[] {
  let branches: IBranch[] = [];
  const body: Block = parent.type == 'FunctionDefinition' ? (parent as FunctionDefinition).body : parent;
  body.statements.forEach((st) => {
    //ifs que consistem de revert não serão considerados pois tem a mesma função de um 'require' e, portanto,
    //são tratados nas restrições e não como branches
    if (isIf(st) && !isIfWithRevert(st)) {
      const branchesFromIf = getBranchFromIfStatement(
        truffleProject,
        contract,
        functionDefinition,
        parent,
        <IfStatement>st,
        branches.filter((b) => b.type != 'else').filter((b) => b.restrictions.some((r) => r.operator != '!=')),
        level
      );
      if (branchesFromIf != null && branchesFromIf.length > 0) {
        branches = branches.concat(branchesFromIf);
      }
    }
  });
  return branches;
}

function getBranchFromIfStatement(
  truffleProject: TruffleProjectIceFactoryFunction,
  contract: ContractDefinition,
  functionDefinition: FunctionDefinition,
  parent,
  st: IfStatement,
  branchesPrevious: IBranch[] = [],
  level: number
) {
  let branches: IBranch[] = [];

  let restrictions: IRestriction[] = [];
  if (isBO(st.condition)) {
    restrictions = getBinaryOperationRestriction(
      truffleProject,
      contract,
      <BinaryOperation>st.condition,
      functionDefinition
    );
  } else {
    restrictions = getUnaryOperationRestriction(truffleProject, contract, st.condition, functionDefinition);
  }
  if (restrictions.length > 0) {
    //se voltou mais de 1, é por que não era uma comparação simples, tinha algum nível de AND/OR
    //Se for AND, é um branch só, se for OR, são dois branches cada um com sua restrição
    if (restrictions.length == 1 || (<BinaryOperation>st.condition).operator == '&&') {
      //Apenda as restrições na coleção {branches} e, se houve, as branches internas
      appendBranches(
        truffleProject,
        contract,
        functionDefinition,
        st.loc,
        st.trueBody as Block,
        level,
        branches,
        restrictions
      );
    } else if ((<BinaryOperation>st.condition).operator == '||') {
      restrictions.forEach((restriction) => {
        //Apenda as restrições na coleção {branches} e, se houve, as branches internas
        appendBranches(truffleProject, contract, functionDefinition, st.loc, st.trueBody as Block, level, branches, [
          restriction,
        ]);
      });
    } else {
      throw new Error(`Unexpected operator: ${(<BinaryOperation>st.condition).operator}`);
    }
    //Os else if's são representados com um IfStatement no 'falseBody'
    if (st.falseBody != null && isIf(st.falseBody)) {
      branches = branches.concat(
        getBranchFromIfStatement(
          truffleProject,
          contract,
          functionDefinition,
          parent,
          <IfStatement>st.falseBody,
          branches.concat(branchesPrevious),
          level
        )
      );
    }
    //Se não for um elseif, cria-se um novo branch cujas condições sejam exatamente aquelas que não façam
    //interseção com os if/elseif's acima
    else {
      const branchesSoFar: IBranch[] = branches.concat(branchesPrevious);
      const branchesDoElse: IBranch[] = [];
      //Cria um branch para o ELSE na coleção {branchesDoElse} sem restrições e, se houver branches internos,
      //cria os branches internos com suas respectivas restrições
      appendBranches(
        truffleProject,
        contract,
        functionDefinition,
        st?.falseBody?.loc,
        st.falseBody as Block,
        level,
        branchesDoElse,
        []
      );
      branchesDoElse.forEach((bde) => {
        //o appendBranches acima retornou as restrições como origem notDefined, vamos setar para o else
        bde.type = BranchType.else;

        //filtra os branches que tenham alguma restriction do nível atual ou superior
        branchesSoFar
          .filter((b) => b.restrictions.some((r) => r.level <= level))
          .forEach((branch) => {
            //pega somente as restrições do nível atual ou superior
            const restrictionsSameLevel = branch.restrictions.filter((r) => r.level <= level);
            //Se restar só uma será a negação desta , senão, sorteia pois
            //as restrições dentro do branch são acumuláveis (AND). Então basta um dos itens ser invertido para o branch
            //não ser executado. Estávamos invertendo todas as restrições e isso levou a situações de deadlock. exemplo: param < 0 && param > 12
            //Depois de identificar isso, passou-se a fazer desta forma: negar apenas uma das restrições de cada branch
            const restrictionToPush =
              restrictionsSameLevel.length == 1
                ? invertRestriction(restrictionsSameLevel[0])
                : invertRestriction(restrictionsSameLevel[Math.floor(Math.random() * restrictionsSameLevel.length)]);
            //Inclui a restrição no branche se não existir, atualiza a lista de restrições do branch se necessário, ou
            //se já existir uma idêntica, não faz nada
            updateRestrictionList(bde.restrictions, restrictionToPush);
          });
      });
      if (functionDefinition.name == 'isValidDate') {
        console.log(bgYellow(red(JSON.stringify(branchesDoElse))));
      }
      branches = branches.concat(branchesDoElse);
    }

    return branches;
  }
}

/**
 * Analisa a restrição {restriction} e o conteúdo do array {restrictions} e avalia
 * a necessicdade de incluí-la na lista, atualizar algum elemento já presente na
 * lista ou não tomar ação alguma
 *
 * @param restrictions Lista de restrições a ser atualizada
 * @param restriction restrição a se avaliar a inclusão ou atualização na lista
 */
function updateRestrictionList(restrictions: IRestriction[], restriction: IRestriction) {
  //Se não estiver uma idêntica, faz as análises
  //Além disso, análises mais produndas só fazem sentido para inequações maior/menor
  //e se algum dos lados for um valor literal
  if (
    !restrictions.find((r) => JSON.stringify(r) == JSON.stringify(restriction)) &&
    ['>', '<', '>=', '<='].includes(restriction.operator) &&
    (restriction.left.elementType == RestrictionElementType.Literal ||
      restriction.right.elementType == RestrictionElementType.Literal)
  ) {
    const nr: IRestriction = getNormalizedLiteralRestrictions([restriction])[0];
    const i = restrictions.findIndex(
      (r) => r.left.value.js == nr.left.value.js || r.right.value.js == nr.left.value.js
    );
    //não há restrição que se refira ao parâmetro
    if (i < 0) {
      restrictions.push(restriction);
    }
    //Há restrições com operador > ou >= que estão comparando com literal menor
    else if (
      ['>', '>='].includes(nr.operator) &&
      getNormalizedLiteralRestrictions([restrictions[i]])[0].right.value.obj < nr.right.value.obj
    ) {
      restrictions[i] = restriction;
    } //Há restrições com operador < ou <= que estão comparando com literal maior
    else if (
      ['<', '<='].includes(nr.operator) &&
      getNormalizedLiteralRestrictions([restrictions[i]])[0].right.value.obj > nr.right.value.obj
    ) {
      restrictions[i] = restriction;
    }
  } else if (!restrictions.find((r) => JSON.stringify(r) == JSON.stringify(restriction))) {
    restrictions.push(restriction);
  }
}

/**
 * Append a new branch with the {restrictions} informed.
 * Additionaly, search for internal branches in the {body} IF found, in spite of
 * appending a single branch with {restrictions}, it will appended as many
 * branches as found wich one with theri own restrictions plus {restrictions}
 *
 * @param truffleProject  Truffle Project under analysis
 * @param contract Contract Definition in AST under analysis
 * @param functionDefinition  Function Definition in AST under analysis
 * @param ifLocation The location of the IF/ELSEIF/ELSE statement which {restrictions} were extracted from it's condition
 * @param body the body of the IF/ELSEIF/ELSE statement which {restrictions} were extracted from it's condition
 * @param currentLevel Level inside the function. If the IF is in the body of {FunctionDefinition} is level 0,
 *  if the IF is inside another IF, is level 1, and ..
 * @param branchesToUpdate The array where the new branch(es) will be appended
 */
function appendBranches(
  truffleProject: TruffleProjectIceFactoryFunction,
  contract: ContractDefinition,
  functionDefinition: FunctionDefinition,
  ifLocation: Location,
  body: Block,
  currentLevel: number,
  branchesToUpdate: IBranch[],
  restrictions: IRestriction[]
): void {
  //atribui o level às restrições para posterior utilização
  restrictions.forEach((r) => (r.level = currentLevel));
  //Se o body for BLOCK, procura por os branches (IFs/ELSEIFs/ELSEs) dentro do IF
  const branchesInternos: IBranch[] =
    body?.type == 'Block' ? getBranches(truffleProject, contract, functionDefinition, body, currentLevel + 1) : [];
  //Se tem branch interno, cada um deles será retornado com a restrição adicional do IF corrente
  if (branchesInternos.length > 0) {
    branchesInternos.forEach((bi) => {
      bi.restrictions = bi.restrictions.concat(restrictions);
      branchesToUpdate.push(bi);
    });
  } //senão, vai retornar só a branch do IF corrente com sua respectiva restrição
  else {
    branchesToUpdate.push({
      loc: ifLocation,
      parent: body,
      restrictions: restrictions,
      type: BranchType.notDefined,
    });
  }
}
