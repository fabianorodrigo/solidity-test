import { isAbstractContract, getContractConstructor, sortContractsByConstructor } from './contractDefinition';
import {
  isFunction,
  isFunctionCall,
  isSuperFunctionCall,
  isRequireCall,
  isAssertCall,
  isIfWithRevert,
  isIdentifier,
  isIf,
  isFor,
  isBO,
  isUO,
  isAddressFunctionCall,
  isAddressZeroFunctionCall,
  isBOExpressionStatement,
  isStateVariableDeclaration,
  isVariableDeclaration,
  isParameterMemberAccess,
  isMemberAccess,
  isNumberLiteral,
  isIndexAccess,
  cleanLocData,
  isTransactionParameterMemberAccess,
  isExpressionStatement,
} from './statements';
import { getBranches } from './branches';
import { getFunctions, getInheritedFunctions } from './functionDefinition';
import { getStateVariables } from './stateVariable';
import { getEnums } from './enumDefinition';
import { getStructs } from './structDefinition';
import { getBinaryOperationRestriction } from './binaryOperation';
import { getUnaryOperationRestriction } from './unaryOperation';
import { getLinkedLibraries } from './libraries';
import {
  ArrayTypeName,
  NumberLiteral,
  ElementaryTypeName,
  VariableDeclaration,
  Identifier,
  TypeName,
} from 'solidity-parser-antlr';
import { getModifiers } from './modifierDefinition';
import { ITruffleProjectGraph } from '../../models/graph';
export * from './restrictionValue';
export * from './newConstructor';

export {
  isAbstractContract,
  getContractConstructor,
  isFunction,
  isFunctionCall,
  isSuperFunctionCall,
  isRequireCall,
  isAssertCall,
  isIfWithRevert,
  isIdentifier,
  isIf,
  isFor,
  isBO,
  isBOExpressionStatement,
  isStateVariableDeclaration,
  isVariableDeclaration,
  isTransactionParameterMemberAccess,
  isParameterMemberAccess,
  isMemberAccess,
  isNumberLiteral,
  isIndexAccess,
  cleanLocData,
  getBranches,
  getFunctions,
  getModifiers,
  getInheritedFunctions,
  getStateVariables,
  getStructs,
  getEnums,
  getBinaryOperationRestriction,
  getUnaryOperationRestriction,
  sortContractsByConstructor,
  getType,
  getLinkedLibraries,
  isString,
  isBytes,
  isArray,
  isAddressFunctionCall,
  isAddressZeroFunctionCall,
  isExpressionStatement,
};

export * from './getFunctionCallNameReference';
export * from './searchForElementType';

function isString(graph: ITruffleProjectGraph, variable: VariableDeclaration): boolean {
  return getType(graph, variable.typeName) == 'string';
}

function isBytes(graph: ITruffleProjectGraph, variable: VariableDeclaration): boolean {
  const type = getType(graph, variable.typeName);
  return type.startsWith('bytes');
}

function isArray(typeName: TypeName) {
  return typeName.type == 'ArrayTypeName';
}

function getType(graph: ITruffleProjectGraph, typeName: TypeName): string {
  if (
    typeName.type != 'UserDefinedTypeName' &&
    !isArray(typeName) &&
    typeName.type != 'Mapping' &&
    typeName.hasOwnProperty('name')
  ) {
    return (<ElementaryTypeName>typeName).name;
  } else if (typeName.type == 'Mapping') {
    return `mapping(${getType(graph, typeName.keyType)}=>${getType(graph, typeName.valueType)})})`;
  } else if (typeName.type == 'ArrayTypeName') {
    return getType(graph, typeName.baseTypeName).concat(
      `[${
        (<NumberLiteral>typeName?.length)?.number
          ? (<NumberLiteral>typeName.length).number
          : isIdentifier(typeName?.length)
          ? (<Identifier>typeName.length).name
          : ''
      }]`
    ); // getArrayTypeName(typeName);
  } else if (typeName.type == 'UserDefinedTypeName') {
    //structs
    const struct = graph.structs[typeName.namePath];
    if (struct) {
      return struct.contract.name.concat('.', struct.name);
    }
    //outros tipos
    return typeName.namePath;
  } else {
    return typeName.type;
  }
}

function getArrayTypeName(typeName: ArrayTypeName): string {
  if (typeName.baseTypeName.type == 'UserDefinedTypeName') {
    let tipo = typeName.baseTypeName.namePath;
    return tipo.concat(
      `[${
        (<NumberLiteral>typeName?.length)?.number
          ? (<NumberLiteral>typeName.length).number
          : isIdentifier(typeName?.length)
          ? (<Identifier>typeName.length).name
          : ''
      }]`
    );
  } else if (typeName.baseTypeName.type == 'ArrayTypeName') {
    return getArrayTypeName(typeName.baseTypeName).concat(
      `[${typeName.length ? (<NumberLiteral>typeName.length).number : ''}]`
    );
  } else {
    let tipo = (<ElementaryTypeName>typeName.baseTypeName).name;
    return tipo.concat(
      `[${
        (<NumberLiteral>typeName?.length)?.number
          ? (<NumberLiteral>typeName.length).number
          : isIdentifier(typeName.length)
          ? (<Identifier>typeName.length).name
          : ''
      }]`
    );
  }
}
