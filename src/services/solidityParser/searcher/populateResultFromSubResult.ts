import { TypeSearchResultList } from '../../../models';

export function populateResultFromSubResult(result: TypeSearchResultList, subResult: TypeSearchResultList) {
  //avalia cada uma das buscas solicitadas
  Object.keys(subResult).forEach((searchName) => {
    //se para a busca {searchName} retornou algo, avaliará cada um dos elementos retornados
    if (subResult[searchName].length > 0) {
      subResult[searchName].forEach((rSub) => {
        //Se o elemento retornado na subbusca já não existe no resultado final {result}, inclui
        if (result[searchName].find((r) => Object.is(r, rSub)) == null) {
          result[searchName].push(rSub);
        }
      });
    }
  });
}
