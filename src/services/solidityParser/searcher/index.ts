import {
  Block,
  ContractDefinition,
  ForStatement,
  FunctionDefinition,
  IfStatement,
  ModifierDefinition,
} from 'solidity-parser-antlr';
import { TruffleProjectIceFactoryFunction } from '../../TruffleFactory';
import { searchForElementType } from './../searchForElementType';
import {
  isFor,
  isFunction,
  isFunctionCall,
  isIdentifier,
  isIf,
  isLiteral,
  isRequireCall,
  isSuperFunctionCall,
  isWhile,
} from '../statements';
import { ISearch, TypeSearchResultList } from '../../../models';
import { populateResultFromSubResult } from './populateResultFromSubResult';
import { mark } from '../../Utils';
import { getContractConstructor } from '../contractDefinition';
import { IFunctionGraph, IModifierGraph } from '../../../models/graph';
import { getConstructorGraph, getFunctionGraph, getModifierGraph } from '../../TruffleProjectGraph';
import { contractEmpty } from '../../../test/utils';

export * from './searchFunctions';

/**
 * Percorre as linhas de código do {parent} em busca dos elementos solicitados nos parâmetros de entrada
 *
 * @param parent element that has statements to be analysed (function.body, if.body, etc)
 * @param name Parameter name or state variable name that has to be passed to the functionCall in order
 * the function name is returned
 * @param parameterCount If set, only functions with this size of input parameters will be returned
 * @returns function names called passing the name direct on indirectaly
 */
export function searchThroughoutStatements(
  truffleProject: TruffleProjectIceFactoryFunction,
  functionGraph: IFunctionGraph | IModifierGraph,
  parent: FunctionDefinition | Block | ModifierDefinition,
  searchs: ISearch[]
): TypeSearchResultList {
  let result: TypeSearchResultList = {};
  searchs.forEach((s) => {
    result[s.name] = [];
  });
  //if parent is a function, analyse it's modifiers
  if (isFunction(parent)) {
    const graph = functionGraph.contract.projectGraph;
    const modifiers = (parent as FunctionDefinition).modifiers;
    for (let i = 0; i < modifiers.length; i++) {
      const modInv = modifiers[i];
      if (modInv.arguments && modInv.arguments.length > 0 && modInv.arguments.some((arg) => isIdentifier(arg))) {
        let modifierGraph: IFunctionGraph | IModifierGraph;

        if (graph.contracts[functionGraph.contract.name].inheritsFrom[modInv.name]) {
          //const contracts = truffleProject.getContracts();
          modifierGraph = getConstructorGraph(
            graph.contracts[functionGraph.contract.name].inheritsFrom[modInv.name].contract
          ).constructor;
          /*modifier = getContractConstructor(
            contracts.find((c) => c.contractDefinition.name == modInv.name).contractDefinition
          );*/
        } else {
          modifierGraph = getModifierGraph(functionGraph.contract, modInv.name);
        }
        if (modifierGraph != null) {
          const subResult = searchThroughoutStatements(
            truffleProject,
            modifierGraph,
            modifierGraph.functionDefinition,
            searchs
          );
          populateResultFromSubResult(result, subResult);
        } else {
          console.debug(`Modifier not found: ${modInv.name}`);
        }
      }
    }
  }
  const body: Block = ['FunctionDefinition', 'ModifierDefinition'].includes(parent.type)
    ? (parent as any).body
    : parent;
  let statements = body.statements != null ? body.statements : [body];
  for (let i = 0; i < statements.length; i++) {
    const st = statements[i];
    if (isIf(st)) {
      const subResult = searchThroughoutStatements(
        truffleProject,
        functionGraph,
        (<IfStatement>st).trueBody as Block,
        searchs
      );
      populateResultFromSubResult(result, subResult);
      if ((<IfStatement>st).falseBody != null) {
        let falseBody = (<IfStatement>st).falseBody;
        if (isIf(falseBody)) falseBody = (falseBody as IfStatement).trueBody;
        const subResult = searchThroughoutStatements(truffleProject, functionGraph, falseBody as Block, searchs);
        populateResultFromSubResult(result, subResult);
      }
    } else if (isFor(st)) {
      const subResult = searchThroughoutStatements(
        truffleProject,
        functionGraph,
        (<ForStatement>st).body as Block,
        searchs
      );
      populateResultFromSubResult(result, subResult);
    }
    //TODO: por que while não tem body?
    /*else if (isWhile(st)){
      const subResult = getFunctionCallNameReference((<WhileStatement>st). as Block, parameter);
      if (subResult.length > 0) result = result.concat(subResult);
    }*/
    else if (!isRequireCall(st) && (isFunctionCall(st) || isSuperFunctionCall(st))) {
      const isSuper = isSuperFunctionCall(st);
      const { contract: contractCalled, funcDef: functionCalled } = truffleProject.getFunctionFromContractDefinition(
        functionGraph.contract.contractDefinition,
        isSuper ? (st as any).expression.expression.memberName : (st as any).expression.expression.name,
        (st as any).expression.arguments.length, //TODO: ainda não é suficiente para cobrir overload, apenas alguns casos
        isSuper
      );
      //Se achar esta outra função no mesmo contrato e não for ela mesma
      if (functionCalled != null && !Object.is(functionCalled, functionGraph.functionDefinition)) {
        //Remapeia o nom dos parâmetros/variáveis de estado originais para os parâmetros de entrada da função chamada (se repassados)
        const subResult = searchThroughoutStatements(
          truffleProject,
          getFunctionGraph(functionGraph.contract.projectGraph, contractCalled, functionCalled),
          functionCalled,
          searchs.map((search) => {
            const transformation = transformFunctionParametersNames(
              search.functionParametersNames,
              (<any>st).expression.arguments,
              functionCalled,
              search.originalFunctionParametersNames == null
                ? search.functionParametersNames
                : search.originalFunctionParametersNames
            );
            return {
              ...search,
              functionParametersNames: transformation.functionParametersNames,
              originalFunctionParametersNames: transformation.originalFunctionParametersNames,
            };
          })
        );
        populateResultFromSubResult(result, subResult);
      }
    }
    searchs.forEach((searchParams) => {
      searchParams.searchFunction(
        truffleProject,
        functionGraph.contract.contractDefinition,
        st,
        result[searchParams.name],
        searchParams.functionParametersNames,
        searchParams.originalFunctionParametersNames,
        searchParams.args
      );
    });
  }
  return result;
}

/**
 * Baseado no array {current}, nos parâmetros da função chamada, e nos valores repassados na FunctionCall,
 * recria a propriedade  {functionParametersNames} para manter a mesma intenção da busca original
 *
 * @param current Nomes dos parâmetros da função cujos statements estão sendo analisados
 * @param functionCallArguments Argumetnos passados a uma chamada a outra função
 * @param functionCalled Função que foi chamada
 * @param original Nomes dos parâmetros da busca original (consierando-se que com funções chamando outras e repassando
 * os parâmetros, esses nomes vão mudando mas a ideia é manter o rastreamento para que o resultado seja inserido com
 * o mesmo nome da busca original)
 */
function transformFunctionParametersNames(
  current: string[],
  functionCallArguments: any[],
  functionCalled: FunctionDefinition,
  original: string[]
): { functionParametersNames: string[]; originalFunctionParametersNames: string[] } {
  const result = { functionParametersNames: [], originalFunctionParametersNames: [] };
  for (let i = 0; i < current.length; i++) {
    const passedOnParamIndex = functionCallArguments.findIndex((arg) => arg?.name == current[i]);
    if (passedOnParamIndex > -1) {
      result.functionParametersNames.push(functionCalled.parameters[passedOnParamIndex].name);
      result.originalFunctionParametersNames.push(original[i]);
    }
  }
  return result;
}
