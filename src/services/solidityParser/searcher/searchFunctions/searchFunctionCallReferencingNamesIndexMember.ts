import { ContractDefinition, FunctionCall, Statement } from 'solidity-parser-antlr';
import { TruffleProjectIceFactoryFunction } from '../../../TruffleFactory';
import { searchForElementType } from '../../searchForElementType';
import { isIndexAccess } from '../../statements';

/**
 *
 * @param truffleProject  Projeto Truffle analisado
 * @param contract Contrato analisado
 * @param statement Statement em análise
 * @param functionParametersNames Nomes de parâmetros ou de variáveis de estado que são entrada
 * de alguma chamada de função e, assim, precisarão ser traduzidos para se manter o link com
 * a busca inicial (ex. se o statement analisado é uma chamada de função que está repassando um parâmetro de entrada
 * X como parâmetro para esta outra função em uma posição cujo parâmetro se chama Y, deve-se fazer a tradução para
 * que na lógica da busca ele passe a procurar por Y )
 * @param result array a ser populado com resultados (quando e se encontrado)
 */
export function searchFunctionCallReferencingNamesIndexMember(
  truffleProject: TruffleProjectIceFactoryFunction,
  contract: ContractDefinition,
  statement: Statement,
  result: any[],
  functionParametersNames: string[],
  //Nomes de variáveis de estado ou parâmetros da origem da busca (lembrando que caso ocorrada chamdas de outras funções com repassagem
  //das variáveis de estado ou dos parâmetros de entrada, a {functionParameterNames} terá os nomes dos parâmetros da função chamada)
  originalParametersNames: string[],
  //filtro opcional da quantidade de parâmetros de entrada que deve ter a função e o nome dos parâmetros ou state variables que são passados como entrada em chamadas de função
  args: { parameterCount?: number }
): boolean {
  let affected = false;
  const nodesSearchScope = searchForElementType(statement, ['FunctionCall']);
  const nodesResult = nodesSearchScope.filter(
    (fc) =>
      (args?.parameterCount == null || (<FunctionCall>fc).arguments.length == args?.parameterCount) &&
      indiceDosNomesPassadosNaFunctionCall(fc, functionParametersNames)
  );
  for (let i = 0; i < nodesResult.length; i++) {
    const fc = nodesResult[i];
    for (let j = 0; j < functionParametersNames.length; j++) {
      const name = functionParametersNames[j];
      if (fc.arguments.find((funcArg) => isIndexAccess(funcArg) && name == (<any>funcArg)?.base?.name) != null) {
        if (result.find((r) => r.name == name && r.functionName == (<any>fc.expression).name) == null) {
          affected = true;
          //Se existir valor na originalParamtersNames, é porque houve chamada de função aninhada e o  resultado deve
          //refletir o nome original
          result.push({
            name: originalParametersNames?.length > j ? originalParametersNames[j] : name,
            functionName: (<any>fc.expression).name,
          });
        }
      }
    }
  }
  return affected;
}

/**
 * Retorna TRUE se algum dos nomes {nomes} está sendo passado na chamada {fc}
 *
 * @param fc FunctionCall analisada
 * @param nomes nomes de variáveis ou parâmetros analisados
 */
function indiceDosNomesPassadosNaFunctionCall(fc: FunctionCall, nomes: string[]) {
  return fc.arguments.find((funcArg) => isIndexAccess(funcArg) && nomes.includes((<any>funcArg)?.base?.name)) != null;
}
