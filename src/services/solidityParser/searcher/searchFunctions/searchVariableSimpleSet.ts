import { ContractDefinition, Identifier, Statement } from 'solidity-parser-antlr';
import { TruffleProjectIceFactoryFunction } from '../../../TruffleFactory';
import { searchForElementType } from '../../searchForElementType';
import { isIdentifier } from '../../statements';

/**
 * Retorna os nomes de variáveis em {variableNames} que são setadas com o valor do parâmetro passado {parameter}
 * direta ou indiretamente
 *
 * @param truffleProject  Projeto Truffle analisado
 * @param contract Contrato analisado
 * @param statement Statement em análise
 * @param functionParametersNames Nomes de parâmetros ou de variáveis de estado que são entrada
 * de alguma chamada de função e, assim, precisarão ser traduzidos para se manter o link com
 * a busca inicial (ex. se o statement analisado é uma chamada de função que está repassando um parâmetro de entrada
 * X como parâmetro para esta outra função em uma posição cujo parâmetro se chama Y, deve-se fazer a tradução para
 * que na lógica da busca ele passe a procurar por Y )
 * @param result array a ser populado com resultados (quando e se encontrado)
 */
export function searchVariableSimpleSet(
  truffleProject: TruffleProjectIceFactoryFunction,
  contract: ContractDefinition,
  statement: Statement,
  result: any[],
  functionParametersNames: string[],
  //Nomes de variáveis de estado ou parâmetros da origem da busca (lembrando que caso ocorrada chamdas de outras funções com repassagem
  //das variáveis de estado ou dos parâmetros de entrada, a {functionParameterNames} terá os nomes dos parâmetros da função chamada)
  originalParametersNames: string[],
  //filtro opcional da quantidade de parâmetros de entrada que deve ter a função e o nome dos parâmetros ou state variables que são passados como entrada em chamadas de função
  args: { variableNames: string[] }
): boolean {
  let affected = false;
  const nodesSearchScope = searchForElementType(statement, ['BinaryOperation']);
  const nodesResult = nodesSearchScope.filter(
    (bo) =>
      bo.operator == '=' &&
      isIdentifier(bo.left) &&
      args.variableNames.includes((<Identifier>bo.left).name) &&
      isIdentifier(bo.right) &&
      functionParametersNames.includes((<Identifier>bo.right).name)
  );

  for (let i = 0; i < nodesResult.length; i++) {
    const bo = nodesResult[i];
    for (let j = 0; j < functionParametersNames.length; j++) {
      const name = functionParametersNames[j];
      if ((<Identifier>bo.right).name == name) {
        if (result.find((r) => r.paramName == name && r.stateVariableName == (<Identifier>bo.left).name) == null) {
          affected = true;
          //Se existir valor na originalParamtersNames, é porque houve chamada de função aninhada e o  resultado deve
          //refletir o nome original
          result.push({
            paramName: originalParametersNames?.length > j ? originalParametersNames[j] : name,
            stateVariableName: (<Identifier>bo.left).name,
          });
        }
      }
    }
  }
  return affected;
}
