import fs from 'fs';
import path from 'path';

export function deleteRecursiveJSFiles(caminho: string): void {
  if (fs.statSync(caminho).isFile()) {
    if (path.extname(caminho).toLowerCase() == '.js') fs.unlinkSync(caminho);
  } else {
    fs.readdirSync(caminho).forEach((d) => {
      deleteRecursiveJSFiles(path.join(caminho, d));
    });
  }
}
