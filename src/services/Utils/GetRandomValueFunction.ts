import { ContractDefinition, TypeName } from 'solidity-parser-antlr';
import { IRestriction } from '../../models/IResctriction';
import { IValue } from '../../models/IValue';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { IContractGraph } from '../../models/graph';
export type GetRandomValueFunction = (
  truffleProject: TruffleProjectIceFactoryFunction,
  typeName: TypeName,
  contract: ContractDefinition | IContractGraph,
  restrictions: IRestriction[]
) => IValue;
