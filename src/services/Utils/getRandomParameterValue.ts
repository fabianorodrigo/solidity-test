import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { IRestriction } from '../../models/IResctriction';
import { RestrictionElementType } from '../../models/RestrictionElementType';
import { IValue } from '../../models/IValue';
import { extractRealRestrictions, getNormalizedRestrictions, getEnabledElementTypes } from '../Restrictions';
import {
  VariableDeclaration,
  ContractDefinition,
  FunctionDefinition,
  ElementaryTypeName,
  ArrayTypeName,
} from 'solidity-parser-antlr';
import { getRandomValue, getRandomValueGraph } from './getRandomValue';
import { ITypeValue } from '../../models/scenario/ITypeValue';
import { getSubType, getSubTypeGraph } from './getSubType';
import { getNormalizedTransactionRestrictions } from '../Restrictions/normalizeRestriction';
import { IFunctionGraph, IParameterGraph, IContractGraph } from '../../models/graph';
import { TypeReferences } from '../../models/scenario';
import { transactionParameterAttributes } from '../transactionParameterAttributers';
import { getType } from '../solidityParser';
import { createIValue } from './createIValue';
import { BranchType } from '../../models/BranchType';
import { ElementTypeAddressContract, ElementTypeArrayAddressContract } from '../types';
import { getFunctionGraph, getFunctionName } from '../TruffleProjectGraph';

/**
 * Return a value in the pool in a random position or, exceptionally return another random value.
 * In both cases, must obbey the restrictions
 *
 * @param {TruffleProjectIceFactoryFunction} truffleProject Instance of Truffle Project under analysis
 * @param {ContractDefinition} contractDefinition Contract in the AST being analysed
 * @param {FunctionDefinition} functionDefinition Function in the AST being analysed
 * @param {VariableDeclaration} parameter Parameter that will have the value generated
 * @param {IRestriction[]} restrictions Restrictions that need to be respected in order to select a value
 * @param {object} references Values already calculated for the all parameters of the function being analysed, state variables of contract,
 * transaction parameter
 */
export function getRandomParameterTypeValue(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractDefinition: ContractDefinition,
  functionDefinition: FunctionDefinition,
  parameter: VariableDeclaration,
  restrictions: IRestriction[] = [],
  references: TypeReferences
): ITypeValue {
  if (parameter == null) {
    throw new Error(`A parameter is required`);
  }
  //filtra as restrições que se referem apenas aos parâmetro desejado e as normaliza (a referência ao parâmetro ficará sempre à esquerda)
  //Na sequência, filtra por aquelas cujos ElementTypes aos quais são comparados sejam tratados pela solução
  const nRestrictions = getNormalizedRestrictions(
    restrictions.filter((r) => r.left.value.js == parameter.name || r.right.value.js == parameter.name),
    parameter.name
  ).filter(
    (r) =>
      getEnabledElementTypes().includes(r.right.elementType) ||
      (r.right.elementType == RestrictionElementType.stateVariableMember && r.right.value.memberName == 'length')
  ); //TODO: Remover esse "OR" quando no EnableElementTypes incluir o StateVariableMember
  const realRestrictions: IRestriction[] = extractRealRestrictions(
    truffleProject.getGraph().contracts[contractDefinition.name],
    nRestrictions,
    references
  );

  //Se já não existir uma restrição  que se aplica ao parâmetro AND
  //Se o parâmetro for do tipo address, checa se é foi identificado ser um address de contrato
  let parameterAddressContract = null;
  if (
    realRestrictions.length == 0 &&
    ((<ElementaryTypeName>parameter.typeName).name == 'address' ||
      (<ElementaryTypeName>(<ArrayTypeName>parameter.typeName)?.baseTypeName)?.name == 'address')
  ) {
    let func = getFunctionGraph(truffleProject.getGraph(), contractDefinition, functionDefinition);
    parameterAddressContract = func.parameters.find((p) => p.name == parameter.name && p.isAddressContract);
    if (parameterAddressContract != null) {
      parameterAddressContract.contractAddressOf.forEach((contractName) => {
        //busca o contrato entre os contratos do projeto que não sejam abstratos
        let contract = Object.values(truffleProject.getGraph().contracts).find(
          (c) => c.name == contractName && !c.isAbstract && !c.isThirdPartyLib
        );
        //se não existir, busca por contratos que implementem contrato deste nome
        if (contract == null || !truffleProject.getDeployedJSContracts().includes(contract.name)) {
          const compatibleContracts = Object.values(truffleProject.getGraph().contracts).filter(
            (c) => !c.isAbstract && !c.isThirdPartyLib && Object.keys(c.inheritsFrom).includes(contractName)
          );
          if (compatibleContracts.length == 1) contract = compatibleContracts[0];
          else if (compatibleContracts.length > 1)
            contract = compatibleContracts[Math.floor(Math.random() * compatibleContracts.length)];
        }
        //se não existir, busca por contratos de terceiros que foram instanciados
        if (contract == null || !truffleProject.getDeployedJSContracts().includes(contract.name)) {
          const compatibleContracts = Object.values(truffleProject.getGraph().contracts).filter(
            (c) =>
              c.instanceNeeded == true &&
              c.isThirdPartyLib &&
              (c.name == contractName ||
                Object.values(c.inheritsFrom).find((ih) => ih.contract.name == contractName) != null)
          );
          if (compatibleContracts.length == 1) contract = compatibleContracts[0];
          else if (compatibleContracts.length > 1)
            contract = compatibleContracts[Math.floor(Math.random() * compatibleContracts.length)];
        }
        //se ainda continuar null, POR ENQUANTO, desiste e passa address comum
        if (contract != null && truffleProject.getDeployedJSContracts().includes(contract.name)) {
          realRestrictions.push({
            operator: 'typeof',
            from: BranchType.notDefined,
            left: { elementType: RestrictionElementType.functionParameter, value: createIValue(parameter.name) },
            right: { elementType: RestrictionElementType.contractType, value: createIValue(contract.name) },
          });
        }
      });
    }
  }
  const value = getRandomValue(
    truffleProject,
    realRestrictions.filter((r) => r.operator == 'typeof').length == 0
      ? parameter.typeName
      : parameterAddressContract.isArray
      ? ElementTypeArrayAddressContract
      : ElementTypeAddressContract,
    contractDefinition,
    realRestrictions
  );
  return {
    type:
      parameter.typeName.type == 'UserDefinedTypeName' || parameter.typeName.type == 'ArrayTypeName'
        ? parameter.typeName.type
        : (<ElementaryTypeName>parameter.typeName).name,
    subtype: getSubType(truffleProject, functionDefinition, parameter, value),
    stringfieldValue: value,
  };
}

/**
 * Return a value in the pool in a random position or, exceptionally return another random value.
 * In both cases, must obbey the restrictions
 *
 * @param {TruffleProjectIceFactoryFunction} truffleProject Instance of Truffle Project under analysis
 * @param {ContractDefinition} contractDefinition Contract in the AST being analysed
 * @param {FunctionDefinition} functionGraph Function in the AST being analysed
 * @param {VariableDeclaration} parameter Parameter that will have the value generated
 * @param {IRestriction[]} restrictions Restrictions that need to be respected in order to select a value
 * @param {object} references Values already calculated for the all parameters of the function being analysed, state variables of contract,
 * transaction parameter
 */
export function getRandomGraphParameterTypeValue(
  truffleProject: TruffleProjectIceFactoryFunction,
  functionGraph: IFunctionGraph,
  parameter: IParameterGraph,
  restrictions: IRestriction[] = [],
  references: { [name: string]: ITypeValue }
): ITypeValue {
  if (parameter == null) {
    throw new Error(`A parameter is required`);
  }
  //filtra as restrições que se referem apenas aos parâmetro desejado e as normaliza (a referência ao parâmetro ficará sempre à esquerda)
  //Na sequência, filtra por aquelas cujos ElementTypes aos quais são comparados sejam tratados pela solução
  const nRestrictions = getNormalizedRestrictions(
    restrictions.filter((r) => r.left.value.js == parameter.name || r.right.value.js == parameter.name),
    parameter.name
  ).filter((r) => getEnabledElementTypes().includes(r.right.elementType));
  const realRestrictions: IRestriction[] = extractRealRestrictions(functionGraph.contract, nRestrictions, references);

  const value = getRandomValueGraph(truffleProject, parameter.type, functionGraph.contract, realRestrictions);
  return {
    type: parameter.type,
    subtype: getSubTypeGraph(truffleProject, functionGraph, parameter, value),
    stringfieldValue: value,
  };
}

export const TransactionParameterType = 'TransactionParameter';

/**
 * Return a value in the pool in a random position or, exceptionally return another random value.
 * In both cases, must obbey the restrictions
 *
 * @param {TruffleProjectIceFactoryFunction} truffleProject Instance of Truffle Project under analysis
 * @param {ContractDefinition} contract Contract in the AST or in the Project Graph being analysed
 * @param {FunctionDefinition} functionDefinition Function in the AST being analysed
 * @param {VariableDeclaration} parameter Parameter that will have the value generated
 * @param {IRestriction[]} restrictions Restrictions that need to be respected in order to select a value
 * @param {object} references Values already calculated for the all parameters of the function being analysed, state variables of contract,
 * transaction parameter
 */
export function getRandomTransactionParameterTypeValue(
  truffleProject: TruffleProjectIceFactoryFunction,
  contract: ContractDefinition | IContractGraph,
  restrictions: IRestriction[] = [],
  references: { [name: string]: ITypeValue }
): ITypeValue {
  let transactionParameter: ITypeValue = {
    type: TransactionParameterType,
    subtype: null,
    stringfieldValue: { isCode: false, js: null, sol: null, obj: '###null###' },
  };
  let stringfieldValue = {
    js: { from: 'accounts[0]' },
    sol: { from: '0x0' },
    obj: { from: 'accounts[0]' },
  };

  //filtra as restrições que se referem apenas aos parâmetro desejado e as normaliza (a referência ao parâmetro ficará sempre à esquerda)
  const nRestrictions = getNormalizedTransactionRestrictions(
    restrictions.filter(
      (r) =>
        r.left.elementType == RestrictionElementType.transactionParameter ||
        r.right.elementType == RestrictionElementType.transactionParameter
    )
  );
  const realRestrictions: IRestriction[] = extractRealRestrictions(
    truffleProject.getGraph().contracts[contract.name],
    nRestrictions,
    references
  );
  //se não houver restrições, vai retornar o valor default: accounts[0]
  if (realRestrictions.length > 0) {
    //Object.values(transactionParameterAttributes)
    //FIXME: incluir o 'value' só quando existir referência a ele. Tirei o 'value' porque estava dando exceção no CappedTransfer.setCap do simple-shared-wallet, por exemplo
    const transactionAttributes: {
      solidityName: string;
      jsName: string;
      typeName: { type: 'ElementaryTypeName'; name: string };
    }[] = [];
    realRestrictions
      .filter((r) => r.left.elementType == RestrictionElementType.transactionParameter)
      .forEach((rr) => {
        if (transactionAttributes.find((tpa) => tpa.solidityName == rr.left.value.js) == null) {
          transactionAttributes.push(transactionParameterAttributes[rr.left.value.js]);
        }
      });

    transactionAttributes.forEach((tpa) => {
      const paramObj: IValue = getRandomValue(
        truffleProject,
        <ElementaryTypeName>tpa.typeName,
        contract,
        realRestrictions.filter((r) => r.left.value.js == tpa.solidityName)
      );
      stringfieldValue.js[tpa.jsName] = paramObj.js;
      stringfieldValue.sol[tpa.jsName] = paramObj.sol;
      stringfieldValue.obj[tpa.jsName] = paramObj.obj;
    });
  }
  transactionParameter.stringfieldValue.js = `{${Object.keys(stringfieldValue.js).map(
    (k) => `${k}:${stringfieldValue.js[k]}`
  )}}`;
  transactionParameter.stringfieldValue.sol = JSON.stringify(stringfieldValue.sol);
  transactionParameter.stringfieldValue.obj = stringfieldValue.obj;

  return transactionParameter;
}

function initTransactionParamParamsWithRestriction(transactionParamsWithRestriction, paramName) {
  if (!transactionParamsWithRestriction.transaction) {
    transactionParamsWithRestriction.transaction = {};
  }
  if (!transactionParamsWithRestriction.transaction[paramName]) {
    transactionParamsWithRestriction.transaction[paramName] = [];
  }
}
