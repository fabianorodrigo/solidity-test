import { FunctionDefinition, ContractDefinition } from 'solidity-parser-antlr';
import { IRestriction } from '../../models/IResctriction';
import { ITypeValue } from '../../models/scenario/ITypeValue';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import {
  getRandomParameterTypeValue,
  getRandomTransactionParameterTypeValue,
  getRandomGraphParameterTypeValue,
} from './getRandomParameterValue';
import { IFunctionGraph } from '../../models/graph';
import { TypeReferences } from '../../models/scenario';
import { RestrictionElementType } from '../../models/RestrictionElementType';

/**
 * Returns a random value of the type infomed by {typeName}
 *
 * @param {TruffleProjectIceFactoryFunction} truffleProject Instance of Truffle Project under analysis
 * @param {ContractDefinition} contractDefinition contract definition which {functionDefinition} belongs to
 * @param {FunctionDefinition} functionDefinition Function Definition for which parameters will be generated values
 * @param {Array} restrictions Restrictions that generated values must be complyed with
 * @returns Values complyed with the restrictions
 */
export function getParametersValues(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractDefinition: ContractDefinition,
  functionDefinition: FunctionDefinition,
  restrictions: IRestriction[] = [],
  references: TypeReferences
): ITypeValue[] {
  const result = functionDefinition.parameters
    .map((functionParameter) => {
      references[functionParameter.name] = getRandomParameterTypeValue(
        truffleProject,
        contractDefinition,
        functionDefinition,
        functionParameter,
        restrictions,
        references
      );
      return references[functionParameter.name];
    })
    .concat(
      references['transactionParameter'] &&
        restrictions.filter(
          (r) =>
            r.left.elementType == RestrictionElementType.transactionParameter ||
            r.right.elementType == RestrictionElementType.transactionParameter
        ).length == 0
        ? references['transactionParameter']
        : getRandomTransactionParameterTypeValue(truffleProject, contractDefinition, restrictions, references)
    );
  references['transactionParameter'] = result[result.length - 1];
  return result;
}

/**
 * Returns a random value of the type infomed by {typeName}
 *
 * @param {TruffleProjectIceFactoryFunction} truffleProject Instance of Truffle Project under analysis
 * @param {ContractDefinition} contractDefinition contract definition which {functionDefinition} belongs to
 * @param {FunctionDefinition} functionGraph Function Definition for which parameters will be generated values
 * @param {Array} restrictions Restrictions that generated values must be complyed with
 * @returns Values complyed with the restrictions
 */
export function getGraphParametersValues(
  truffleProject: TruffleProjectIceFactoryFunction,
  functionGraph: IFunctionGraph,
  restrictions: IRestriction[] = []
): ITypeValue[] {
  let parameters: { [name: string]: ITypeValue } = {};
  functionGraph.parameters.forEach((functionParameter) => {
    parameters[functionParameter.name] = null;
  });
  return functionGraph.parameters
    .map((functionParameter) => {
      parameters[functionParameter.name] = getRandomGraphParameterTypeValue(
        truffleProject,
        functionGraph,
        functionParameter,
        restrictions,
        parameters
      );
      return parameters[functionParameter.name];
    })
    .concat(getRandomTransactionParameterTypeValue(truffleProject, functionGraph.contract, restrictions, parameters));
}

/**
 * Generate a random value for an attribute of a transaction parameter (eg. msg.sender, msg.value, etc...)
 *
 * @param {object} truffleProject Instance of Truffle Project under analysis
 * @param {FunctionDefinition} functionDefinition The function definition that will be analysed to check if there are
 *  restrictions to the values of the transaction attribute
 * @param {object} transactionParameterAttribute Attribute's name of transaction parameter which restrictions will be analysed
 *  and a value comply with them will be returned: {name, typeName{type}}
 * @param {*} contractDefinition The contract definition which the {functionDefinition} beleongs to
 * @param {function} onRestrictionFound Function called if some restriction for the {functionParameter} is found in the statements of {functionDefinition}
 * @param {object} branch Branch that gave origin to the scenario being analysed with all it's data including restrictions that have to be considered, besides the restrictions based on requires found, to achieve a desired target
 * @returns {object} An object with {type, subtype, stringfieldValue}
 */
/*export function getTransactionParameterAttributeRandomValue(
  truffleProject: TruffleProjectIceFactoryFunction,
  functionDefinition: FunctionDefinition,
  contractDefinition: ContractDefinition,
  onRestrictionFound: onRestrictionFoundFunction,
  randomValueFunction: GetRandomValueFunction = getRandomValue,
  branch: IBranch
): IValue {
  const restrictions: IRestriction[] = getFunctionParameterRestriction(
    truffleProject,
    functionDefinition,
    [],
    contractDefinition,
    branch
  );
  if (
    onRestrictionFound &&
    restrictions.filter(r => r.restrictionType == RestrictionValueType.transactionParameter).length > 0
  ) {
    onRestrictionFound(restrictions.filter(r => r.restrictionType == RestrictionValueType.transactionParameter));
  }
  const value: IValue = randomValueFunction(
    truffleProject,
    transactionParameterAttribute.typeName,
    contractDefinition,
    restrictions
      .filter(r => r.name == transactionParameterAttribute.solidityName)
      .concat(branch.restrictions.filter(r => r.name == transactionParameterAttribute.solidityName)) //pass only the restrictions of current attribute of transaction parameter)
  );
  return value;
}*/

/** *
 * Generate random input parameters values for the parameters of the FunctionDefinition passed
 *
 * @param {object} f FunctionDefinition extracted from the AST
 * @param {object} valuesPool A object with pre-selected values to be used
 * @param {object} contractDefinition Optional contract definition which {f} belongs to
 * @returns {Array} array with paramters values in javascript source (eg. strings between quotes)
 */
/*function getFunctionParametersValues(f: FunctionDefinition, valuesPool, contractDefinition: ContractDefinition) {
  let params = [];
  if (f != null) {
    if (f.parameters && f.parameters.length > 0) {
      params = f.parameters.map(p => {
        const value = getRandomValue(p.typeName, valuesPool, contractDefinition);
        return {
          type:
            p.typeName.type == 'UserDefinedTypeName' || p.typeName.type == 'ArrayTypeName'
              ? p.typeName.type
              : (<ElementaryTypeName>p.typeName).name,
          subtype:
            p.typeName.type == 'UserDefinedTypeName'
              ? value.js.substring(8).split('.')[0] //Se for UserDefinedTypeName, retorna: 'contract' + NomeTipo + '.address'. Por isso o substr e o split
              : null,
          stringfieldValue: value,
        };
      });
    }
    return params;
  }
}
*/
