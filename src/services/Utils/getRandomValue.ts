import { Types } from '../types';
import { ContractDefinition, ElementaryTypeName, TypeName } from 'solidity-parser-antlr';
import { IRestriction } from '../../models/IResctriction';
import { IValue } from '../../models/IValue';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { IContractGraph } from '../../models/graph';
/**
 * Returns a random value of the type infomed by {typeName}
 *
 * @param {TruffleProjectIceFactoryFunction} truffleProject Instance of Truffle Project under analysis
 * @param {TypeName} typeName Type name of the random value desired. If NULL, it's the transaction parameter (eg. msg.sender, msg.value, etc)
 * @param {ContractDefinition} contract Optional contract definition which {f} belongs to
 * @param {IRestriction[]} restrictions Restrictions for the generated value
 * @returns Value complyed with the restrictions
 */
export function getRandomValue(
  truffleProject: TruffleProjectIceFactoryFunction,
  typeName: TypeName,
  contract: ContractDefinition | IContractGraph,
  restrictions: IRestriction[] = []
): IValue {
  let valueReturn = null;
  if (typeName == null) {
    throw new Error(`typeName is null`);
  } else if (Types[(<ElementaryTypeName>typeName).name]) {
    valueReturn = Types[(<ElementaryTypeName>typeName).name].random(truffleProject, restrictions, typeName, contract);
  } else if (Types[typeName.type]) {
    //Array, Mapping, UserDefined
    valueReturn = Types[typeName.type].random(truffleProject, restrictions, typeName, contract, getRandomValue);
  } else {
    throw new Error(`Unexpected parameter type: ${(<ElementaryTypeName>typeName).name} ${JSON.stringify(typeName)}`);
  }
  valueReturn.typeName = typeName;
  return valueReturn;
}

/**
 * Returns a random value of the type infomed by {typeName}
 *
 * @param {TruffleProjectIceFactoryFunction} truffleProject Instance of Truffle Project under analysis
 * @param {string} type Type name of the random value desired. If NULL, it's the transaction parameter (eg. msg.sender, msg.value, etc)
 * @param {ContractDefinition} contract Optional contract definition which {f} belongs to
 * @param {IRestriction[]} restrictions Restrictions for the generated value
 * @returns Value complyed with the restrictions
 */
export function getRandomValueGraph(
  truffleProject: TruffleProjectIceFactoryFunction,
  type: string,
  contract: ContractDefinition | IContractGraph,
  restrictions: IRestriction[] = []
): IValue {
  let valueReturn = null;
  if (type == null) {
    throw new Error(`typeName is null`);
  } else if (Types[type]) {
    valueReturn = Types[type].random(truffleProject, restrictions, type, contract, getRandomValueGraph);
  } else {
    throw new Error(`Unexpected parameter type: ${type}`);
  }
  valueReturn.typeName = type;
  return valueReturn;
}
