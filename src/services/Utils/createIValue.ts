import { IValue } from '../../models/IValue';

export function createIValue(value: any): IValue {
  return { isCode: false, js: value, sol: value, obj: value };
}
