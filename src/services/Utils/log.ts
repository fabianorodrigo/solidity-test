import { DebugLevel } from '../../models/debugLevel';
import { bgYellow, black, yellow, green, red, bgWhite, bgRed, white } from 'colors';

export function error(msg: string, ...msg2: string[]) {
  console.log(new Date().toLocaleTimeString(), bgRed(white(msg)), msg2);
}

export function warn(msg: string, ...msg2: any[]) {
  if (!process.env.DEBUG_LEVEL || parseInt(process.env.DEBUG_LEVEL) >= DebugLevel.WARNING) {
    console.log(new Date().toLocaleTimeString(), bgYellow(black(msg)), msg2);
  }
}

export function info(msg: string, ...msg2: any[]) {
  if (!process.env.DEBUG_LEVEL || parseInt(process.env.DEBUG_LEVEL) >= DebugLevel.INFO) {
    console.log(new Date().toLocaleTimeString(), green(msg), msg2 ? msg2.toString() : '');
  }
}

export function mark(msg: string, ...msg2: any[]) {
  if (!process.env.DEBUG_LEVEL || parseInt(process.env.DEBUG_LEVEL) >= DebugLevel.INFO) {
    console.log(new Date().toLocaleTimeString(), red(msg), msg2 ? msg2 : '');
  }
}

export function debug(msg: string, ...msg2: any[]) {
  if (!process.env.DEBUG_LEVEL || parseInt(process.env.DEBUG_LEVEL) >= DebugLevel.DEBUG) {
    console.log(new Date().toLocaleTimeString(), yellow(msg), msg2);
  }
}

export function trace(msg: string, ...msg2: any[]) {
  if (!process.env.DEBUG_LEVEL || parseInt(process.env.DEBUG_LEVEL) >= DebugLevel.TRACE) {
    console.log(new Date().toLocaleTimeString(), bgWhite(black(msg)), msg2);
  }
}
