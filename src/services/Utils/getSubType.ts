import { FunctionDefinition, UserDefinedTypeName, VariableDeclaration } from 'solidity-parser-antlr';
import { IValue } from '../../models/IValue';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { IParameterGraph, IFunctionGraph } from '../../models/graph';
export function getSubType(
  truffleProject: TruffleProjectIceFactoryFunction,
  functionDefinition: FunctionDefinition,
  functionParameter: VariableDeclaration,
  value: IValue
): string {
  function getArrayTypeName(typeName) {
    if (typeName.baseTypeName.type == 'UserDefinedTypeName') {
      return typeName.baseTypeName.namePath;
    } else if (typeName.baseTypeName.type == 'ArrayTypeName') {
      return getArrayTypeName(typeName.baseTypeName).concat(`[${typeName.length ? typeName.length.number : ''}]`);
    } else {
      let tipo = typeName.baseTypeName.name;
      if (typeName.baseTypeName.name == 'uint') {
        tipo = 'uint256';
      } else if (typeName.baseTypeName.name == 'int') {
        tipo = 'int256';
      } else if (truffleProject.getEnumsByName(typeName.baseTypeName.name).length > 0) {
        tipo = 'uint8';
      }
      return tipo.concat(`[${typeName.length ? typeName.length.number : ''}]`);
    }
  }
  if (functionParameter.typeName.type != 'UserDefinedTypeName' && functionParameter.typeName.type != 'ArrayTypeName') {
    return null;
  } else if (functionParameter.typeName.type == 'ArrayTypeName') {
    return getArrayTypeName(functionParameter.typeName);
  } else if (
    value.sol != null &&
    truffleProject.getEnums()[value.sol.substring(0, value.sol.indexOf('.'))] &&
    truffleProject.getEnums()[value.sol.substring(0, value.sol.indexOf('.'))][
      value.sol.substring(value.sol.indexOf('.') + 1, value.sol.lastIndexOf('.'))
    ]
  ) {
    return value.sol.substring(0, value.sol.lastIndexOf('.'));
  } else if (value.sol != null && truffleProject.getStructs()[value.sol.split('(')[0]]) {
    return value.sol.split('(')[0];
  } else if (value.js.startsWith('contract')) {
    return value.js.substring(8).split('.')[0];
  } else if (value.js == 'null') {
    const paramInFunction = functionDefinition.parameters.find((p) => p.name == functionParameter.name);
    if (paramInFunction.typeName && (<UserDefinedTypeName>paramInFunction.typeName).namePath) {
      const contractsWithEnums = Object.keys(truffleProject.getEnums());
      for (let contractName of contractsWithEnums) {
        if (
          Object.keys(truffleProject.getEnums()[contractName]).some(
            (enumName) => enumName == (<UserDefinedTypeName>paramInFunction.typeName).namePath
          )
        ) {
          return contractName.concat('.', (<UserDefinedTypeName>paramInFunction.typeName).namePath);
        } else {
          throw new Error('Subtype not identified: '.concat(value.sol));
        }
      }
    }
  } else if (value.sol == null) {
    return null;
  } else {
    throw new Error('Subtype not identified: '.concat(value.sol));
  }
}

export function getSubTypeGraph(
  truffleProject: TruffleProjectIceFactoryFunction,
  functionGraph: IFunctionGraph,
  functionParameter: IParameterGraph,
  value: IValue
): string {
  //FIXME: Vai dar m*, vai precisar ser reescrita, repensada, eventualmente, mexer no grafo
  if (
    !functionParameter.isArray &&
    !functionParameter.isStruct &&
    !Object.keys(functionGraph.contract.projectGraph.contracts).includes(functionParameter.type)
  ) {
    return null;
  } else if (functionParameter.isArray) {
    return functionParameter.type.replace(/\[/, '').replace(/\]/, '');
  } else if (
    value.sol != null &&
    truffleProject.getEnums()[value.sol.substring(0, value.sol.indexOf('.'))] &&
    truffleProject.getEnums()[value.sol.substring(0, value.sol.indexOf('.'))][
      value.sol.substring(value.sol.indexOf('.') + 1, value.sol.lastIndexOf('.'))
    ]
  ) {
    return value.sol.substring(0, value.sol.lastIndexOf('.'));
  } else if (value.sol != null && truffleProject.getStructs()[value.sol.split('(')[0]]) {
    return value.sol.split('(')[0];
  } else if (value.js.startsWith('contract')) {
    return value.js.substring(8).split('.')[0];
  } else if (value.js == 'null') {
    const paramInFunction = functionGraph.parameters.find((p) => p.name == functionParameter.name);
    if (paramInFunction.type) {
      const contractsWithEnums = Object.keys(truffleProject.getEnums());
      for (let contractName of contractsWithEnums) {
        if (Object.keys(truffleProject.getEnums()[contractName]).some((enumName) => enumName == paramInFunction.type)) {
          return contractName.concat('.', paramInFunction.type);
        } else {
          throw new Error('Subtype not identified: '.concat(value.sol));
        }
      }
    }
  } else if (value.sol == null) {
    return null;
  } else {
    throw new Error('Subtype not identified: '.concat(value.sol));
  }
}
