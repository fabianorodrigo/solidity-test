import { FunctionDefinition } from 'solidity-parser-antlr';
/**
 * Evaluate if the function is payable
 *
 * @param {functionDefinition} functionDefinition The function definition in the AST
 */

export default function isPayable(functionDefinition: FunctionDefinition): boolean {
  return functionDefinition.stateMutability == 'payable';
}
