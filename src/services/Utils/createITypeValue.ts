import { createIValue } from './createIValue';
import { TypeName, ElementaryTypeName, FunctionDefinition } from 'solidity-parser-antlr';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { getSubType } from './getSubType';
import { ITypeValue } from '../../models/scenario';

export function createITypeValue(
  truffleProject: TruffleProjectIceFactoryFunction,
  functionDefinition: FunctionDefinition,
  typeName: TypeName,
  value: any
): ITypeValue {
  return {
    type:
      typeName.type == 'UserDefinedTypeName' || typeName.type == 'ArrayTypeName'
        ? typeName.type
        : (<ElementaryTypeName>typeName).name,
    subtype: getSubType(
      truffleProject,
      functionDefinition,
      { type: 'VariableDeclaration', typeName: typeName, isIndexed: false, isStateVar: false, name: '' },
      value
    ),
    stringfieldValue: createIValue(value),
  };
}
