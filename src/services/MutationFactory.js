'use strict';
const path = require('path');
const SolidityParserFactory = require('./SolidityParserFactory');
const UtilsFactory = require('./Utils/UtilsFactory');
const trace = false;
const nativeFunctions = ['revert', 'assert', 'require'];

const utils = UtilsFactory();

/**
 * It's going to create a instance of an immutable object with functions to create
 * mutants Solidity contracts
 *
 * @param {string} solidityPath Path of the origal solidity file
 * @returns {object} Object with mutants functionalities
 */

module.exports = function MutationIceFactory({ solidityPath }) {
  /**
   * Store all the data about the mutation process
   */
  let mutationsProcess = {
    // Store the number of the current mutant (current iteration)
    _currentMutantNumber: 0,
    // Store the number of the last interation that ocurred a mutation (if equals _currentMutantNumber, the current iterations has already been mutated)
    _lastMutated: -1,
    //Store the name of the current Contract being analysed during current iteration
    _currentContract: null,
    //Store the name of the current FunctionBody being analysed during current iteration
    _currentFunctionBody: null,
    //Store the name of the current ModifierBody being analysed during current iteration
    _currentModifierBody: null,
    //Store the last mutation identifier while there is no 'emit event' about it in the mutated solidity code
    _mutationToRegisterEmit: null,
    // Store the mutations already identified
    _mutations: {},
    // Store for each function the other functions are called by it
    _functionFunctionCallsMapping: {},
    // Store for each modifier the functions are called by it
    _modifierFunctionCallsMapping: {},
  };

  const MUTATION_TYPES = {
    //Check if the {mutationsProcess._currentMutantNumber} is equal {mutationsProcess._lastMutated}
    //IF it is EQUAL, the contract version of the current interation was already mutated and will keep just that mutation and will return FALSE
    //IF NOT, it will be checked if the {MutationUtils._mutations} has already contain a mutation for the node by it's localization
    //If NOT: Update {_mutationsProcess._lastMutated} to {_mutationsProcess._currentMutantNumber} and return TRUE in order to the
    //specific function {execute} proceed with the mutation
    _proceedMutation: node => {
      //The current interation was not mutated yet
      if (mutationsProcess._currentMutantNumber > mutationsProcess._lastMutated) {
        //The point of the code analysed was not mutated yet
        if (!mutationsProcess._mutations[JSON.stringify(node.loc)]) {
          mutationsProcess._lastMutated = mutationsProcess._currentMutantNumber;
          return true;
        }
      }
      return false;
    },
    BinaryOperation: {
      id: `BINARYOPERATION_CHANGE_LOGICAL_OPERATORS`,
      description: `Change logical operators`,
      //Execute the {_proceedMutation} function. If it returns TRUE, the mutation is executed
      //and the {_mutationsProcess._mutations} is updated with the data about the mutation
      execute: (binaryOperationNode, parent) => {
        const logicalOperators = ['!=', '==', '>', '<', '>=', '<='];
        // Apply just for operators in the list and when The current interation was not mutated yet
        if (
          logicalOperators.indexOf(binaryOperationNode.operator) != -1 &&
          MUTATION_TYPES._proceedMutation(binaryOperationNode)
        ) {
          const before = binaryOperationNode.operator;
          //Change the operator
          switch (before) {
            case '!=': {
              binaryOperationNode.operator = '==';
              break;
            }
            case '==': {
              binaryOperationNode.operator = '!=';
              break;
            }
            case '>': {
              binaryOperationNode.operator = '<=';
              break;
            }
            case '<': {
              binaryOperationNode.operator = '>=';
              break;
            }
            case '>=': {
              binaryOperationNode.operator = '<';
              break;
            }
            case '<=': {
              binaryOperationNode.operator = '>';
              break;
            }
          }

          const idMutation = JSON.stringify(binaryOperationNode.loc);
          //Store the id mutation in order to include the 'emit MutationExecutedEvent' in the source of mutant contract
          mutationsProcess._mutationToRegisterEmit = idMutation;
          //logicalOperators[Math.floor(Math.random() * logicalOperators.length)];
          mutationsProcess._mutations[idMutation] = {
            typeId: `BINARYOPERATION_CHANGE_LOGICAL_OPERATORS`,
            before: before,
            change: binaryOperationNode.operator,
            contractName: mutationsProcess._currentContract,
            functionName: mutationsProcess._currentFunctionBody,
            modifierName: mutationsProcess._currentModifierBody,
            loc: binaryOperationNode.loc,
            mutantNumber: mutationsProcess._currentMutantNumber,
          };
        }
      },
    },
  };

  //Reference for internal use
  const Grammar = {
    PragmaDirective,
    ContractDefinition,
    InheritanceSpecifier,
    StateVariableDeclaration,
    ModifierDefinition,
    ModifierInvocation,
    FunctionDefinition,
    EventDefinition,
    VariableDeclarationStatement,
    VariableDeclaration,
    Mapping,
    ArrayTypeName,
    TupleExpression,
    UserDefinedTypeName,
    IndexAccess,
    ParameterList,
    Parameter,
    StructDefinition,
    Block,
    IfStatement,
    WhileStatement,
    ForStatement,
    Identifier,
    MemberAccess,
    UnaryOperation,
    BinaryOperation,
    BooleanLiteral,
    NumberLiteral,
    StringLiteral,
    ElementaryTypeName,
    ElementaryTypeNameExpression,
    ExpressionStatement,
    FunctionCall,
    EmitStatement,
    ReturnStatement,
  };

  return Object.freeze({
    generateMutants,
  });

  /**
   * Create multiples mutant contracts each one with a fault/mutation
   *
   * @param {string} solidityPath Path of the origal solidity file
   * @returns {object} Object with data about the generation such as the collection of source code mutated,
   *  the map of mudations and  literal values found in the code during the creating of mutants
   */
  function generateMutants() {
    // Erases the mutation process data
    mutationsProcess = {
      _currentMutantNumber: 0,
      _lastMutated: -1,
      _mutations: {},
      _parameterValuesPool: {},
      _contractSources: [],
      _currentContract: null,
      _currentFunctionBody: null,
      _currentModifierBody: null,
      _functionFunctionCallsMapping: {},
      _modifierFunctionCallsMapping: {},
    };

    const SolidityParser = SolidityParserFactory({ solFilePath: solidityPath });
    const ast = SolidityParser.parse({});

    // Do until the interation doesn't generate a mutant
    do {
      // clone the original source unit
      const astClone = JSON.parse(JSON.stringify(ast));

      mutationsProcess._contractSources.push({
        name: `${path.parse(solidityPath).name}${mutationsProcess._currentMutantNumber}`,
        content: astClone.children
          .map(child => {
            if (Grammar[child.type] == null) {
              const msg = `Formatting function of ${child.type} not found`;
              console.warn(msg, child);
              return msg;
            }
            const nodeFormatted = Grammar[child.type] == null ? child.type : Grammar[child.type](child, astClone);
            return `${nodeFormatted}\n${child.body ? Grammar.Block(child.body) : ''}`;
          })
          .join('\n'),
      });
      mutationsProcess._currentMutantNumber++;
    } while (mutationsProcess._lastMutated == mutationsProcess._currentMutantNumber - 1);
    //O último não é mutante, portanto, é removido
    mutationsProcess._contractSources.pop();
    if (trace) console.log(mutationsProcess._mutations);
    if (trace) console.log(mutationsProcess._functionFunctionCallsMapping);
    if (trace) console.log(mutationsProcess._modifierFunctionCallsMapping);

    return mutationsProcess;
  }

  function PragmaDirective(n) {
    return `pragma  ${n.name} ${n.value};\n`;
  }

  function ContractDefinition(contract) {
    mutationsProcess._currentContract = contract.name;
    return `${contract.kind} ${contract.name}{\n${contract.baseContracts
      .map((bc, i) => {
        return Grammar[bc.type](bc, i);
      })
      .join(', ')} ${contract.subNodes
      .map(subNode => {
        if (Grammar[subNode.type] == null) {
          const msg = `Formatting function of ${subNode.type} not found`;
          console.warn(msg, subNode);
          return msg;
        }
        const nodeFormatted = Grammar[subNode.type] == null ? subNode.type : Grammar[subNode.type](subNode, contract);
        return `${nodeFormatted}\n${subNode.body ? `{\n` + Grammar.Block(subNode.body, subNode) + `\n}` : ''}`;
      })
      .join(`\n`)}
            event MutationExecutedEvent (string mutationId);
        }`;
  }

  function InheritanceSpecifier(n, order) {
    let baseNameFormatted = Grammar[n.baseName.type](n.baseName);
    if (n.arguments && n.arguments.length > 0) {
      baseNameFormatted += `(${n.arguments.map(a => {
        return `${Grammar[a.type](a)}`;
      })})`;
    }
    return `${order == 0 ? ': ' : ''} ${order != null && order > 0 ? ', ' : ''} ${baseNameFormatted} `;
  }

  function StateVariableDeclaration(n) {
    return `${n.variables.map(v => {
      return Grammar.VariableDeclaration(v);
    })};${trace ? '#StateVariableDeclaration' : ''}`;
  }

  function ModifierDefinition(m, contract) {
    return `modifier ${m.name}${trace ? '#ModifierDefinition' : ''}`;
  }

  function ModifierInvocation(m, parent) {
    return m.name.concat(' ');
  }

  function FunctionDefinition(f) {
    let returnCode = f.isConstructor == true ? `constructor ` : `function `;

    returnCode += f.isConstructor == true ? `(` : `${f.name}(`;
    returnCode += Grammar.ParameterList(f.parameters);
    returnCode += `) `;
    returnCode += `${f.visibility} `;
    //Remove the 'pure' stateMutatility because pure functions can't emit events
    //TODO: Deveria remover somente se necessário ao final do processo. Uma opção seria reler os mutantes gerados que possuam pure functions enviando evento e remover somente destas
    returnCode += `${f.stateMutability && f.stateMutability != 'pure' ? f.stateMutability : ''} `;
    if (f.modifiers) {
      f.modifiers.forEach(m => {
        returnCode += Grammar[m.type](m, f);
      });
    }
    returnCode +=
      f.returnParameters && f.returnParameters.parameters && f.returnParameters.parameters.length > 0
        ? ` returns(${Grammar.ParameterList(f.returnParameters)})`
        : '';
    return returnCode;
  }

  function EventDefinition(e) {
    return `event ${e.name} (${Grammar.ParameterList(e.parameters)});${trace ? '#EventDefinition' : ''}`;
  }

  function VariableDeclarationStatement(n, parent) {
    let retorno = '';
    if (trace) console.log('VariableDeclarationStatement', n);
    retorno += n.variables.map((v, i) => {
      return Grammar.VariableDeclaration(v, i, n);
    });
    if (n.initialValue) {
      retorno += ' = ' + Grammar[n.initialValue.type](n.initialValue, n);
    }
    if (parent.type != 'IfStatement' && parent.type != 'BinaryOperation' && parent.type != 'FunctionCall') {
      retorno += ';';
    }
    retorno += `${trace ? '#VariableDeclarationStatement' : ''}`;
    return retorno;
  }

  function VariableDeclaration(v, order) {
    return `${order != null && order > 0 ? ', ' : ''} ${v.isDeclaredConst ? 'const ' : ''} ${Grammar[v.typeName.type](
      v.typeName
    )} ${v.visibility && v.visibility != 'default' ? v.visibility : ''} ${v.name} ${
      v.expression ? '= ' + Grammar[v.expression.type](v.expression) : ''
    }`;
  }

  function Mapping(mapp) {
    return `mapping(${Grammar[mapp.keyType.type](mapp.keyType)} => ${Grammar[mapp.valueType.type](mapp.valueType)})`;
  }
  function ArrayTypeName(ar) {
    return `[${Grammar[ar.baseTypeName.type](ar.baseTypeName)}]`;
  }

  function TupleExpression(te) {
    return (
      te &&
      te.components &&
      te.components.map(p => {
        return `${Grammar[p.type](p, te)}${trace ? '#TupleExpression' : ''}`;
      })
    );
  }

  function UserDefinedTypeName(usdtn) {
    return `${usdtn.namePath}${trace ? '#UserDefinedTypeName' : ''}`;
  }

  function IndexAccess(ia) {
    return `${Grammar[ia.base.type](ia.base)} [${Grammar[ia.index.type](ia.index)}]`;
  }

  function ParameterList(pList) {
    return (
      pList &&
      pList.parameters &&
      pList.parameters.map((p, i) => {
        return Grammar.Parameter(p, i);
      })
    );
  }

  function Parameter(p) {
    return `${p.typeName.name} ${p.name ? p.name : ''} `;
  }

  function StructDefinition(s) {
    return `${s.name} (${s.members.map((v, i) => {
      return Grammar.VariableDeclaration(v, i);
    })})`;
  }

  function Block(body, parent) {
    //Store the current function or modifier definition which the block is in, in order to map where the next mutation is gonna be
    if (parent.type == 'FunctionDefinition') {
      mutationsProcess._currentFunctionBody = parent.isConstructor ? '#constructor#' : parent.name;
    } else if (parent.type == 'ModifierDefinition') {
      mutationsProcess._currentModifierBody = parent.name;
    }

    if (body.type != 'Block') console.warn(`Type block ${body.type} `, body);
    let bodyText = '';
    for (let i = 0; i < body.statements.length; i++) {
      if (i == 0) {
        if (mutationsProcess._mutationToRegisterEmit != null) {
          bodyText += `emit MutationExecutedEvent('${mutationsProcess._mutationToRegisterEmit}');\n`;
          mutationsProcess._mutationToRegisterEmit = null;
        }
      } else if (i > 0) {
        bodyText += '\n';
      }
      //bodyText += MutationUtils.getBody('Block', body.statements[i]);
      bodyText += Grammar[body.statements[i].type](body.statements[i], body);
    }

    if (parent.type == 'FunctionDefinition') {
      mutationsProcess._currentFunctionBody = null;
    } else if (parent.type == 'ModifierDefinition') {
      mutationsProcess._currentModifierBody = null;
    }

    return bodyText;
  }

  function IfStatement(ifstat, parent) {
    if (trace) console.log('IfStatement', ifstat);
    if (trace && ifstat.condition.left) console.log('IfStatement.left', ifstat.condition.left.type);
    let retorno = ifstat.condition.left
      ? `\nif(${Grammar[ifstat.condition.type](ifstat.condition, ifstat)} ) {
\n`
      : `if (${Grammar[ifstat.condition.type](ifstat.condition, ifstat)}) {
\n`;
    //retorno += MutationUtils.getBody('IfStatement.trueBody', ifstat.trueBody);
    retorno += Grammar[ifstat.trueBody.type](ifstat.trueBody, ifstat);
    retorno += `\n
} `;
    if (ifstat.falseBody != null) {
      if (trace) console.log('IfStatement.falseBody', ifstat.falseBody);
      retorno += `\nelse {
    \n`;
      //retorno += MutationUtils.getBody('IfStatement.falseBody', ifstat.falseBody);
      retorno += Grammar[ifstat.falseBody.type](ifstat.falseBody, ifstat);
      retorno += `\n
    } `;
    }

    return retorno;
  }

  function WhileStatement(whstat) {
    if (trace) console.log('WhileStatement', whstat);
    let retorno = whstat.condition.left
      ? `\nwhile(${Grammar[whstat.condition.left.type](whstat.condition.left, whstat)} ${
          whstat.condition.operator
        } ${Grammar[whstat.condition.right.type](whstat.condition.right, whstat)}) {
\n`
      : `while (${whstat.condition.name}) {
\n`;
    //retorno += MutationUtils.getBody('WhileStatement.body', whstat.body);
    retorno += Grammar[whstat.body.type](whstat.body, whstat);
    retorno += `\n
} `;
    return retorno;
  }

  function ForStatement(forStat) {
    if (trace) console.log('ForStatement', forStat);
    let retorno = `\nfor(${Grammar[forStat.initExpression.type](forStat.initExpression, forStat)}; ${Grammar[
      forStat.conditionExpression.type
    ](forStat.conditionExpression, forStat)}; ${Grammar[forStat.loopExpression.type](
      forStat.loopExpression,
      forStat
    )}) {
\n`;
    //retorno += MutationUtils.getBody('ForStatement.body', forStat.body);
    retorno += Grammar[forStat.body.type](forStat.body, forStat);
    retorno += `\n
} `;
    return retorno;
  }

  function Identifier(id, parent) {
    let returnCode = id.name;
    if (parent && parent.type == 'ExpressionStatement') {
      ///} || parentType == 'ReturnStatement') {
      returnCode += ';';
    }
    return returnCode;
  }

  function MemberAccess(m) {
    return `${Grammar[m.expression.type](m.expression, m)}.${m.memberName}`;
  }

  function UnaryOperation(uo, parent) {
    let returnCode = `${uo.operator}${Grammar[uo.subExpression.type](uo.subExpression, uo)}`;
    if (parent.type != 'IfStatement' && parent.type != 'BinaryOperation') {
      returnCode += ';';
    }
    return returnCode;
  }

  function BooleanLiteral(bl) {
    return bl.value;
  }

  function NumberLiteral(l) {
    let literalType = l.type;
    //Literal addresses are interpreted as NumberLiteral
    //So if it has this carachteriscs, we infer it is a Address
    if (utils.isAddress(l.number)) {
      literalType = 'AddressLiteral';
    }
    if (!mutationsProcess._parameterValuesPool[literalType]) {
      mutationsProcess._parameterValuesPool[literalType] = [];
    }
    if (mutationsProcess._parameterValuesPool[literalType].indexOf(l.number) === -1) {
      mutationsProcess._parameterValuesPool[literalType].push(l.number);
    }
    return l.number;
  }

  function StringLiteral(l) {
    if (!mutationsProcess._parameterValuesPool[l.type]) {
      mutationsProcess._parameterValuesPool[l.type] = [];
    }
    if (mutationsProcess._parameterValuesPool[l.type].indexOf(l.value) === -1) {
      mutationsProcess._parameterValuesPool[l.type].push(l.value);
    }
    return `"${l.value}"`;
  }

  function ElementaryTypeName(etne) {
    return etne.name;
  }

  function ElementaryTypeNameExpression(etne) {
    return etne.typeName.name;
  }

  function ExpressionStatement(expressionSt, parent) {
    let returnCode = '';
    if (mutationsProcess._mutationToRegisterEmit != null) {
      returnCode += `emit MutationExecutedEvent('${mutationsProcess._mutationToRegisterEmit}');\n`;
      mutationsProcess._mutationToRegisterEmit = null;
    }
    if (trace) console.log('ExpressionStatement', expressionSt);
    returnCode += Grammar[expressionSt.expression.type](expressionSt.expression, expressionSt);
    return returnCode;
  }

  function FunctionCall(fc, parent) {
    if (trace) console.log('FunctionCall', fc);
    // update the function calls mapping if it's inside a function body and is a call to a function that is not
    // a native function
    if (
      mutationsProcess._currentFunctionBody &&
      fc.expression &&
      fc.expression.type == 'Identifier' &&
      !nativeFunctions.find(n => fc.expression.name == n)
    ) {
      if (!mutationsProcess._functionFunctionCallsMapping[mutationsProcess._currentFunctionBody]) {
        mutationsProcess._functionFunctionCallsMapping[mutationsProcess._currentFunctionBody] = [];
      }
      if (
        mutationsProcess._functionFunctionCallsMapping[mutationsProcess._currentFunctionBody].indexOf(
          fc.expression.name
        ) === -1
      ) {
        mutationsProcess._functionFunctionCallsMapping[mutationsProcess._currentFunctionBody].push(fc.expression.name);
      }
    }
    // update the function calls mapping if it's inside a modifier body and is a call to a function that is not
    // a native function
    if (
      mutationsProcess._currentModifierBody &&
      fc.expression &&
      fc.expression.type == 'Identifier' &&
      !nativeFunctions.find(n => fc.expression.name == n)
    ) {
      if (!mutationsProcess._modifierFunctionCallsMapping[mutationsProcess._currentModifierBody]) {
        mutationsProcess._modifierFunctionCallsMapping[mutationsProcess._currentModifierBody] = [];
      }
      if (
        mutationsProcess._modifierFunctionCallsMapping[mutationsProcess._currentModifierBody].indexOf(
          fc.expression.name
        ) === -1
      ) {
        mutationsProcess._modifierFunctionCallsMapping[mutationsProcess._currentModifierBody].push(fc.expression.name);
      }
    }
    if (Grammar[fc.expression.type] == null) console.error('Type not supported', fc);
    let returnCode = `${Grammar[fc.expression.type](fc.expression, fc)} (${fc.arguments.map(a => {
      return `${Grammar[a.type](a, fc)}`;
    })})`;
    if (
      parent.type != 'IfStatement' &&
      parent.type != 'BinaryOperation' &&
      parent.type != 'UnaryOperation' &&
      parent.type != 'FunctionCall'
    ) {
      returnCode += ';';
    }
    return returnCode;
  }

  function BinaryOperation(bo, parent) {
    if (trace) console.log('BinaryOperation', bo);
    ///MUTATION PROCESS///
    MUTATION_TYPES[bo.type].execute(bo, parent);
    //TODO:
    /*
        confirmar se esses casos se são tratados aqui:
          if (cancelado) {
              ...
          }
        */

    let returnCode = `${Grammar[bo.left.type](bo.left, bo)} ${bo.operator} ${Grammar[bo.right.type](bo.right, bo)}`;
    if (
      parent.type != 'FunctionCall' &&
      parent.type != 'IfStatement' &&
      parent.type != 'VariableDeclarationStatement' &&
      parent.type != 'BinaryOperation'
    ) {
      returnCode += ';';
    }
    return returnCode;
  }

  function EmitStatement(es, parent) {
    if (trace) console.log('EmitStatement', es);
    let returnCode = `emit ${Grammar[es.eventCall.expression.type](
      es.eventCall.expression,
      es
    )} (${es.eventCall.arguments.map(a => {
      return `${Grammar[a.type](a, es)}`;
    })})`;
    if (
      parent.type != 'FunctionCall' &&
      parent.type != 'IfStatement' &&
      parent.type != 'VariableDeclarationStatement'
    ) {
      returnCode += ';';
    }
    return returnCode;
  }

  function ReturnStatement(rs, parent) {
    if (trace) console.log('ReturnStatement', rs);
    let returnCode = `return ${rs.expression ? Grammar[rs.expression.type](rs.expression, rs) : ''}`;
    if (parent.type != 'FunctionCall' && parent.type != 'VariableDeclarationStatement') {
      returnCode += ';';
    }
    return returnCode;
  }
};
