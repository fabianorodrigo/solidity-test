import {
  isAbstractContract,
  getContractConstructor,
  getLinkedLibraries,
  isAssertCall,
  isNumberLiteral,
} from '../../solidityParser';
import {
  getFunctionParametersValuesScenarios,
  updateReferencesFromFunctionCalling,
  initReferencesGlobal,
} from '../../scenarios';
import { testBeforeEach } from './testBeforeEach';
import { ContractDefinition } from 'solidity-parser-antlr';
import { IBeforeEach } from '../../../models/IBeforeEach';
import { ValidationCodeFunction, TruffleProjectIceFactoryFunction } from '../../TruffleFactory';
import { getParametersValues } from '../../Utils/UtilsFactory';
import { IFunctionCalling } from '../../../models';
import { getConstructorGraph } from '../../TruffleProjectGraph';
import { IFunctionGraph } from '../../../models/graph';
import { debug, mark, trace, warn } from '../../Utils';
import { createITypeValue } from '../../Utils/createITypeValue';

const MAX_ATTEMPTS = 6;

/**
 * Generate the code that instance the {contract} and the dependencies of it's constructor (parameter with type of other contracts)

 * @param {contractDefinition} contractDefinition The contractDefinition in the AST
 * @param {object} previousResult Object with the information accumulated about previous calling, information about instance
 * of other contracts in the same project: contractsDeployed, variableDeclare, result, etc
 * @param {string} jsTestHeader JS code in the header of javascript test file
 * @param {function} validationFunction Function to validate if the code generated is valid.
 * Poderia ter chamado simplesmente truffleProject.executeTruffleTest, mas por questões de testabilidade,
 * da própria ferramenta solidity-test foi feita essa injeção de dependência
 *
 * @returns {string} Returns the javascript code that intances the contract in the chain and, eventually, it's dependencies
 */

export async function generateCodeContractInstance(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractDefinition: ContractDefinition,
  previousResult: IBeforeEach,
  jsTestHeader: string,
  validationFunction: ValidationCodeFunction
): Promise<IBeforeEach> {
  let tentativas = 0;
  const currentResult: IBeforeEach = JSON.parse(JSON.stringify(previousResult)); //clone to not mutate
  // Não instancia biblioteca, nem interface, nem contrato abstrato,
  //nem contrato que não tenha construtor com visibilidade 'public' ou 'default'
  if (contractDefinition.kind != 'interface' && !isAbstractContract(contractDefinition)) {
    currentResult.variableDeclare += `  let contract${contractDefinition.name} = null;\n`;

    const constructor = getContractConstructor(contractDefinition);
    // Se existe construtor mas ele não tem visibilidade pública, não constará no beforeEach
    if (constructor != null && constructor.visibility != 'public' && constructor.visibility != 'default') {
      mark(`${contractDefinition.name}.constructor.visibility: ${constructor.visibility}`);
      //TODO:SE O CONSTRUTOR FOR NAO VISIVEL, TEM QUE INSTNACIAR UM FILHO
      return currentResult;
    }
    let partialBeforeEachExecutionSucessfull = false;
    // Não passará para a próxima interação do contrato enquanto não appendar no
    // contractsDeployed uma chamada válida
    whilePrincipal: while (!partialBeforeEachExecutionSucessfull && tentativas <= MAX_ATTEMPTS) {
      if (truffleProject.getUndeployableContracts().includes(contractDefinition.name)) {
        warn(`${contractDefinition.name} na lista de undeployables`, truffleProject.getUndeployableContracts());
        return previousResult;
      }
      tentativas++;
      //inicializando a global references do contrato
      initReferencesGlobal(truffleProject.getGraph().contracts[contractDefinition.name]);
      //Se o construtor não existe
      if (constructor == null) {
        currentResult.contractsDeployed.push({
          contractName: contractDefinition.name,
          stringfieldParams: [
            global['defaultReferences'][contractDefinition.name]['transactionParameter'].stringfieldValue.js,
          ],
        });

        const construtorHerdado = getConstructorGraph(truffleProject.getGraph().contracts[contractDefinition.name]);
        if (construtorHerdado && construtorHerdado.constructor != null) {
          let params = [];
          if (Array.isArray(construtorHerdado.parameters)) {
            construtorHerdado.parameters.forEach((p) => {
              const pvalue = isNumberLiteral(p) ? parseInt(p.number) : p.value;
              params.push(
                createITypeValue(truffleProject, construtorHerdado.constructor.functionDefinition, p, pvalue)
              );
            });
          }
          params.push(global['defaultReferences'][contractDefinition.name]['transactionParameter']);
          const fc: IFunctionCalling = {
            contractInstanceTarget: contractDefinition.name,
            functionCalled: construtorHerdado.constructor,
            parametersValues: params,
            stateVariablesAffected: Object.values(
              (construtorHerdado.constructor as IFunctionGraph).stateVariablesWritten
            ).map((sv) => {
              return { name: sv.name };
            }),
          };
          updateReferencesFromFunctionCalling(fc, global['defaultReferences'][contractDefinition.name]);
        }
      } else {
        const scenario = getFunctionParametersValuesScenarios(
          truffleProject,
          constructor,
          contractDefinition,
          getParametersValues
        ).filter((s) => s.success == true)[0];
        const fc: IFunctionCalling = {
          contractInstanceTarget: contractDefinition.name,
          functionCalled: truffleProject.getGraph().contracts[contractDefinition.name].functions['constructor'][0],
          parametersValues: scenario.parametersValues,
          stateVariablesAffected: Object.values(
            truffleProject.getGraph().contracts[contractDefinition.name].functions['constructor'][0]
              .stateVariablesWritten
          ).map((sv) => {
            return { name: sv.name };
          }),
        };
        updateReferencesFromFunctionCalling(fc, global['defaultReferences'][contractDefinition.name]);
        // FIXME: APenas enquanto não terminamos a mudança da obtenção do TransactionParameter, vamos remover o último parâmetro
        scenario.parametersValues.pop();
        //Se já existem contratos deployed, as tentativas já chegaram a metade e existem parâmetros do tipo address,
        //substitui o valor dos parâmetros address por endereços de contratos
        if (
          currentResult.contractsDeployed.length > 0 &&
          tentativas > MAX_ATTEMPTS / 2 &&
          scenario.parametersValues.some((pv) => pv.type == 'address')
        ) {
          for (let ipv = 0; ipv < scenario.parametersValues.length; ipv++) {
            if (scenario.parametersValues[ipv].type == 'address') {
              let iContratoDeployed =
                currentResult.contractsDeployed.length == 1
                  ? 0
                  : (tentativas - MAX_ATTEMPTS / 2) % currentResult.contractsDeployed.length; // tentativas - MAX_ATTEMPTS / 2;
              /*if (currentResult.contractsDeployed.length <= iContratoDeployed)
                iContratoDeployed % currentResult.contractsDeployed.length;*/
              scenario.parametersValues[
                ipv
              ].stringfieldValue.js = `contract${currentResult.contractsDeployed[iContratoDeployed].contractName}.address`;
            }
          }
        }

        const constructorParams = scenario.parametersValues;

        let stringfieldParams: string[] = [];
        for (let iParam = 0; iParam < constructorParams.length; iParam++) {
          const p = constructorParams[iParam];
          if (p.type == 'UserDefinedTypeName') {
            // Se o contrato do tipo do parãmetro não está na lista que já teve o  código de instanciação gerado,
            // busca esse código para inserir antes
            if (!currentResult.contractsDeployed.find((c) => c.contractName == p.subtype)) {
              let contractDefinitionDependency = truffleProject
                .getContracts()
                .find((c) => c.contractDefinition.name == p.subtype).contractDefinition;
              //avalia se é uma interface ou contrato abstrato. Se for o caso, precisa encontrar uma implementação sua
              if (
                contractDefinitionDependency.kind == 'interface' ||
                isAbstractContract(contractDefinitionDependency)
              ) {
                const sameType = truffleProject.getValuesPool()[contractDefinitionDependency.name];
                for (let ist = 0; ist < sameType.length; ist++) {
                  contractDefinitionDependency = truffleProject
                    .getContracts()
                    .find((c) => c.contractDefinition.name == sameType[ist]).contractDefinition;
                  if (
                    contractDefinitionDependency.kind != 'interface' &&
                    !isAbstractContract(contractDefinitionDependency)
                  ) {
                    break;
                  }
                }
              }
              if (!currentResult.contractsDeployed.find((c) => c.contractName == contractDefinitionDependency.name)) {
                const dependencyResult = await generateCodeContractInstance(
                  truffleProject,
                  contractDefinitionDependency,
                  previousResult,
                  jsTestHeader,
                  validationFunction
                );
                //Se a instanciação da dependência retornar exatamente o mesmo resultado 'previousResult', algo deu errado
                //começamos a próxima interação do while para tentar gerar novos valores
                //de dependencia nao passar novamente por scenarios.getSuccessfulScenarioAndParameterRestrictions
                if (JSON.stringify(dependencyResult) == JSON.stringify(previousResult)) continue whilePrincipal;

                currentResult.variableDeclare = dependencyResult.variableDeclare;
                //como o atributo variableDeclare é sobrescrito, repetimos o comando de concatenação com o contrato corrente
                currentResult.variableDeclare += `  let contract${contractDefinition.name} = null;\n`;

                currentResult.result = dependencyResult.result;
                currentResult.contractsDeployed = dependencyResult.contractsDeployed;
              }
              p.stringfieldValue.js = `contract${contractDefinitionDependency.name}.address`;
            }
          }
          stringfieldParams.push(`${p.stringfieldValue.js}`);
        }

        stringfieldParams.push(
          global['defaultReferences'][contractDefinition.name]['transactionParameter'].stringfieldValue.js
        );
        currentResult.contractsDeployed.push({
          contractName: contractDefinition.name,
          stringfieldParams,
        });
      }
      const testResult = await testBeforeEach(
        truffleProject,
        jsTestHeader,
        currentResult.contractsDeployed,
        currentResult.variableDeclare,
        validationFunction
      );
      // If not passed, the last element in contractsDeployed is removed
      if (testResult == null) {
        debug(
          `${contractDefinition.name} - testBeforeEach FAIL`,
          `tentativas: ${tentativas}`,
          currentResult.contractsDeployed
        );
        currentResult.contractsDeployed.pop();
      } else {
        truffleProject.registerDeployedJSContract(contractDefinition.name);
        currentResult.result = testResult;
        partialBeforeEachExecutionSucessfull = true;
      }
    }
  }
  return currentResult;
}
