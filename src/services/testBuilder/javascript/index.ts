import path from 'path';

import { getFunctions } from '../../solidityParser';
import { IContract } from '../../../models/IContract';
import { ElementaryTypeName, ContractDefinition } from 'solidity-parser-antlr';
import { IStructsPool } from '../../../models/IStructsPool';
import { generateJavascriptProxyTestFunctions } from './testProxyFunctions';
import { TruffleProjectIceFactoryFunction } from '../../TruffleFactory';

export { generateJavascriptProxyTestFunctions };

export * from './getJSTestHeader';
export * from './getInheritedFunctionsFromAbstract';
/**
 * Produces the import statements of distinct solidity files in the {contracts} passed
 *
 * @param {array} contracts Array of contracts of the project
 */
export function getImportStatements(truffleProject: TruffleProjectIceFactoryFunction, contracts: IContract[]): string {
  return contracts
    .map((c) => {
      return `import "${path.relative(path.join(truffleProject.truffleProjectHome, 'test'), c.solFilePath)}";\n`;
    })
    .reduce((acumulador, importStatement, i) => {
      return i == 0
        ? importStatement
        : acumulador.indexOf(importStatement) == -1
        ? acumulador + importStatement
        : acumulador;
    }, '');
}

/**
 * Para as funções com visibilidade 'internal' e com algum parâmetro incluímos a cláusula 'using for' para o tipo do primeiro parâmetro
 * De acordo com esse link: https://itnext.io/libraries-and-using-for-in-solidity-5c954da04128,
 * mesmo se nao for do mesmo tipo, vai atachar a função ao tipo
 *
 * @param {contractDefinition} library A contract of kind 'library' which functions will be analysed
 * @param {object} structsPool A object with the Solidity structs identified in the project
 * @returns {string} Statement 'using Library for Type' in the Solidity syntax
 */
export function getUsingForStatements(library: ContractDefinition, structsPool): string {
  if (library.kind != 'library') {
    throw new Error('getUsingForStatement has to receive a contractDefinition of kind "library"');
  }

  const functions = getFunctions(library);
  const controleUsingFor = {};
  return functions
    .map((fd) => {
      if (
        fd.visibility == 'internal' &&
        fd.parameters.length > 0 &&
        !controleUsingFor[(<ElementaryTypeName>fd.parameters[0].typeName).name]
      ) {
        controleUsingFor[(<ElementaryTypeName>fd.parameters[0].typeName).name] = true;
        // using x for y
        return `  using ${library.name} for ${getNameTypeName(fd.parameters[0].typeName, structsPool)};\n`;
      }
    })
    .filter((result) => result != null)
    .join('')
    .concat('\n');
}

/**
 * Get the name of type depending oh it type
 *
 * @param {object} typeName AST Type Name
 * @param {object} structsPool A object with list of structs indentified
 */ function getNameTypeName(typeName, structsPool: IStructsPool) {
  if (typeName.type == 'ArrayTypeName') {
    return `${getNameTypeName(typeName.baseTypeName, structsPool)}[]`;
  } else if (typeName.type == 'UserDefinedTypeName') {
    if (structsPool[typeName.namePath]) {
      return typeName.namePath;
    } else {
      const struct = Object.values(structsPool).find((s) => s.name == typeName.namePath);
      if (struct) {
        return `${struct.parentName}.${struct.name}`;
      } else {
        return typeName.namePath;
      }
    }
  } else {
    return typeName.name;
  }
}
