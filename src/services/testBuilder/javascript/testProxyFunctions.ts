import colors from 'colors';
import path from 'path';
import { isPayable, debug, getRandomValue } from '../../Utils';
import { ElementTypeUINT, Types } from '../../types';
const uint = Types.uint;
import { getFunctionParametersValuesScenarios } from '../../scenarios';
import { IContract } from '../../../models/IContract';
import { FunctionDefinition, ElementaryTypeName } from 'solidity-parser-antlr';
import { IScenario, IScenarioFail } from '../../../models/scenario/IScenario';
import { TruffleProjectIceFactoryFunction } from '../../TruffleFactory';
import { getType, getContractConstructor } from '../../solidityParser';
import { updateTestedScenariosSummary } from '../updateTestedScenariosSummary';
import { getProxyTestFunctionName, isFunctionProxyable } from '../solidity';
import { DebugLevel } from '../../../models/debugLevel';
import { serializeJSIValue } from './serializeJSValue';
import { getFunctionNameCalling } from './getFunctionNameCalling';
import { getInheritedFunctionsFromAbstract } from './getInheritedFunctionsFromAbstract';
import { IContractGraph } from '../../../models/graph';

/** *
 * Generate the source code of test functions for the contract in the {index} position of {contracts}
 * DO NOT generate test for functions with visibility INTERNAL or PRIVATE
 *
 * @param {object} truffleProject The instance of the Truffle Project to generate tests
 * @param {object} contract Contract in the {contracts} for which the test will be generated
 * @param {string} partialJSContract Partial content of the JS test file being generated. Has just declarations and beforeEach function and misses the enclosing of most external element
 *
 * @returns {string} The content (javascript source code) of test functions to be inserted in a JS test file
 */
export async function generateJavascriptProxyTestFunctions(
  truffleProject: TruffleProjectIceFactoryFunction,
  contract: IContractGraph,
  partialJSContract: string
): Promise<string> {
  const prjBaseName = path.basename(truffleProject.truffleProjectHome);
  // Pega o stdout após a compilação para checar diferença entre as diferentes execuções
  const RegexpTestResults = new RegExp('Compiled successfully using(.*)Contract: (.*)Error:(.*)', 'gs');
  let result = '  \n';
  const functionsDefinition: FunctionDefinition[] = [];
  Object.values(contract.functions).forEach((array) => {
    array
      .filter((fg) => fg.isConstructor == false)
      .forEach((fg) => {
        functionsDefinition.push(fg.functionDefinition);
      });
  });

  // forEach não respeita o await
  for (let f = 0; f < functionsDefinition.length; f++) {
    debug('generateJavascriptProxyTestFunctions', functionsDefinition[f].name);
    let countTests = 0;
    const fd = functionsDefinition[f];

    //count the number of tested functions
    global['generalResults'][prjBaseName].qtyTestedFunctions++;
    contract.contractDefinition.kind == 'contract'
      ? global['generalResults'][prjBaseName].qtyContractTestedFunctions++
      : global['generalResults'][prjBaseName].qtyLibraryTestedFunctions++;

    const ini = new Date();
    const scenarios: IScenario[] = getFunctionParametersValuesScenarios(
      truffleProject,
      fd,
      contract.contractDefinition
    );
    //summaryze the number of tested scenarios
    updateTestedScenariosSummary(prjBaseName, contract, scenarios, fd);

    for (let isce = 0; isce < scenarios.length; isce++) {
      const functionResults = [];
      const scenarioId = `${fd.name} - ${(<IScenarioFail>scenarios[isce]).paramNameRestriction || ''}: ${
        scenarios[isce].success ? '' : 'FALHA'
      }`;
      console.time(scenarioId);
      const params = scenarios[isce].parametersValues;

      //* *********####################################################################******** */
      // Se o scenário[i] for uma mera derivação criada por conta de require em cima de atributos
      // do parâmetro de transação (msg.sender, msg.value),executa novamente com esta derivação
      // para avaliar se gera um erro diferente
      if ((<IScenarioFail>scenarios[isce]).transactionParameterRestriction) {
        const stringfieldTransactionParameterObj =
          scenarios[isce].parametersValues[scenarios[isce].parametersValues.length - 1].stringfieldValue.obj;
        const { fCode, testResult } = await generateFunctionTestCodeJavascript(
          scenarios[isce],
          contract,
          fd,
          scenarios[isce].parametersValues,
          { from: stringfieldTransactionParameterObj.from, value: stringfieldTransactionParameterObj.value },
          truffleProject,
          partialJSContract
        );
        functionResults.push({
          accountIndex: scenarios[isce].parametersValues[scenarios[isce].parametersValues.length - 1].stringfieldValue,
          parameters: params,
          functionSource: fCode,
          ...testResult,
        });
      }
      // senão, executará primeiro com a account[0], usada sempre no construtor, e uma outra conta aleatória
      // guardará as duas saídas e depois as comparará. Se forem iguais, desctarta uma delas. Do contrário, mantém ambas
      else {
        // TODO: Se o contrato tem número de conta literal, precisaremos criar no beforeEach e também considerá-la aqui
        let indiceContaRandomica = 0;
        while (indiceContaRandomica == 0) {
          indiceContaRandomica = Math.ceil(Math.random() * 9); // a segunda conta é selecionada de forma randômica
        }
        const accountsIndex = process.env.TEST_AFTER_EACH_FUNCTION === 'S' ? [0, indiceContaRandomica] : [0];
        for (let i = 0; i < accountsIndex.length; i++) {
          debug(scenarios[isce].scenarioDescription, accountsIndex.toString());
          const { fCode, testResult } = await generateFunctionTestCodeJavascript(
            scenarios[isce],
            contract,
            fd,
            scenarios[isce].parametersValues,
            {
              from: `accounts[${accountsIndex[i]}]`,
              value: isPayable(fd) ? uint.random(truffleProject, [], null, null).obj : undefined,
            },
            truffleProject,
            partialJSContract
          );
          functionResults.push({
            accountIndex: accountsIndex[i],
            parameters: params,
            functionSource: fCode,
            ...testResult,
          });
        }
      }

      // selecionando resultados distintos
      const selectedFunctionResults = [];
      for (let fri = 0; fri < functionResults.length; fri++) {
        const fr = functionResults[fri];
        // Como o RegExp está sendo reutilizado. Precisamos zerar o lastIndex para começar da posição zero
        RegexpTestResults.lastIndex = 0;
        const frTestResults = RegexpTestResults.exec(fr.stdout);
        // Se no array dos selecionados não existe já um resultado igual, adiciona
        const selecionadoJa = selectedFunctionResults.find((sfr) => {
          let retorno = sfr.code === fr.code && sfr.stderr === fr.stderr;
          if (sfr.code !== 0 && retorno) {
            // Como o RegExp está sendo reutilizado. Precisamos zerar o lastIndex para começar da posição zero
            RegexpTestResults.lastIndex = 0;
            const sfrTestResults = RegexpTestResults.exec(sfr.stdout);
            retorno = (sfrTestResults == null && frTestResults == null) || sfrTestResults[3] === frTestResults[3];
          }
          return retorno;
        });
        if (selecionadoJa == null) {
          selectedFunctionResults.push(fr);
        }
      }
      countTests += selectedFunctionResults.length;

      // montando o código final resultante
      for (let sfri = 0; sfri < selectedFunctionResults.length; sfri++) {
        const sfr = selectedFunctionResults[sfri];
        // Quando há mais de um resultado diferente, vamos referenciar o nome da conta utilizada no from
        // Isso não é feito antes porque senão todo stdout daria resultado diferente pois ele contém o nome do teste
        if (selectedFunctionResults.length > 1) {
          // sfr.functionSource = `/*\n${sfr.stdout.match(RegexpTestResults)}\n*/\n${sfr.functionSource}`;
          sfr.functionSource = sfr.functionSource.replace(' - accounts[$xxx$]', ` - accounts[${sfr.accountIndex}]`);
        } else {
          sfr.functionSource = sfr.functionSource.replace(' - accounts[$xxx$]', '');
        }
        //
        result += sfr.functionSource;
      }
      if (!process.env.DEBUG_LEVEL || parseInt(process.env.DEBUG_LEVEL) >= DebugLevel.TRACE) {
        console.timeEnd(scenarioId);
      }
    }
    debug(
      `Proxy4Test${contract.contractDefinition.name}.${fd.name} resultou ${countTests} testes em ${Math.round(
        (new Date().getTime() - ini.getTime()) / 1000
      )} segundos`
    );
  }
  return result;
}

/**
 * Generates a function test code in Javascript for a specific test scenario,
 * executes it using 'truffle test' and returns the code and the result
 *
 * @param {object} scenario Object representing the test scenario
 * @param {FunctionDefinition} fd Function Definition under test
 * @param {object[]} params array with parameters values generated
 * @param {string} contractName Contract's name which the {fd} belongs to
 * @param {string} transactionParameterValue The transaction parameter value with it's
 *  members in a string representantion
 * @param {TruffleProject} truffleProject The Truffle Project under test
 * @param {string} partialJSContract Partial content of the JS test file being generated.
 *  Has just declarations and beforeEach function and misses the enclosing of most external element
 * @returns {object} Object with the function test code in Javascript and it's results execution
 */
async function generateFunctionTestCodeJavascript(
  scenario,
  contract: IContract,
  fd: FunctionDefinition,
  params,
  transactionParameterValue: { from: string; value: number },
  truffleProject: TruffleProjectIceFactoryFunction,
  partialJSContract
) {
  let fCode = `  it('${scenario.scenarioDescription} - accounts[$xxx$]', async () => {\n`;
  let contractVarName = `contract${contract.contractDefinition.name}`;
  //Se existir funções a serem chamadas para preparar o ambiente a fim de se exercitar
  //o cenário pretendido, inclui tais chamadas (eg. setar variáveis de estado)
  scenario.preFunctionCallings?.forEach((pfc) => {
    //Se for o construtor, cria uma nova instância
    if (pfc.functionCalled.isConstructor) {
      contractVarName = `local${contractVarName}`;
      fCode += `    let ${contractVarName} = await ${
        contract.contractDefinition.name
      }.new(${pfc.parametersValues.map((pv) => serializeJSIValue(pv.stringfieldValue)).join(',')});\n`;
    } else {
      const functionName = getFunctionNameCalling(
        truffleProject,
        pfc.functionCalled.contract.contractDefinition,
        pfc.functionCalled.functionDefinition,
        pfc.parametersValues
      );
      fCode += `    await ${contractVarName}.${functionName}(${pfc.parametersValues
        .map((pv) => serializeJSIValue(pv.stringfieldValue))
        .join(',')});\n`;
    }
    //fCode += `    await ${contractVarName}.${functionName}(`;
  });

  // Se a função não tem nome, trata-se de uma fallback (envio de Ether pro contrato) e
  // é testada de chamando o 'sendTransaction'
  const functionName = fd.name === '' ? 'sendTransaction' : fd.name;

  if (scenario.success) {
    fCode += `    let result = await ${contractVarName}.${functionName}(`;
  } else {
    fCode += `    let result = await truffleAssert.fails(${contractVarName}.${functionName}(`;
  }

  // Os parâmetros na UtilsFactory vêm inclusive com o parâmetro de transação
  // Portanto, vamos ignorá-lo, já que aquina TestFactory implementamos uma
  // lõgica de randomização deste parâmetro que inclui a chamada com a conta
  // que chamou o construtor do contrato (suposto owner), uma outra conta aleatória
  // e ainda chamamos usando o parâmetro vindo da UtilsFactory quando se trata
  // de um cenário derivado de require em cima de membros do parâmetro de transação
  // Por isso só apendamos os parâmetros se existir mais de um e, se for o caso,
  // transcrevemos apenas até o params.length -1
  if (params.length > 1) {
    for (let i = 0; i < params.length - 1; i++) {
      if (i > 0) fCode += ', ';
      const struct = truffleProject.getGraph().structs[getType(truffleProject.getGraph(), fd.parameters[i].typeName)];
      //Se struct e data location STORAGE, os parâmetros são desmembrados na classeProxy
      /*console.log(i, 'struct', getType(truffleProject.getGraph(), fd.parameters[i].typeName), struct);
      console.log('parametro', fd.parameters[i].name, `${params[i].stringfieldValue.js}`);
      console.log('vamos lá', params[i].stringfieldValue.obj);*/
      if (fd.parameters[i].storageLocation == 'storage' && struct) {
        //let v = params[i].stringfieldValue.js.replace('{', '').replace('}', '').replace(/ /g, '');
        const structParametersSemMapping = struct.parameters.filter((m) => !m.type.startsWith('mapping('));
        //TODO: colocar no IValue uma propriedade que contenha a representação em forma de objeto pra evitar essa gambiarra toda
        structParametersSemMapping.forEach((m, mi) => {
          if (mi > 0) fCode += ',';
          //fCode += m.type.startsWith('bytes') ? '[' : m.type == 'string' ? '"' : '';
          fCode += JSON.stringify(params[i].stringfieldValue.obj[m.name]);
          //m.type == 'string'
          //? params[i].stringfieldValue.obj[m.name]
          //: JSON.stringify(params[i].stringfieldValue.obj[m.name]);
          //fCode += m.type.startsWith('bytes') ? ']' : m.type == 'string' ? '"' : '';
        });
        /*let structsPart = params[i].stringfieldValue.js.replace('{', '').replace('}', '').split(','); //tentei JSON.parse, mas quando parâmetro tinha letra no nome dava erro
        for (let ip = 0; ip < structsPart.length; ip++) {
          if (ip > 1) fCode += ',';
          fCode += structsPart[ip].split(':')[1];
        }*/
      } else {
        fCode += `${serializeJSIValue(params[i].stringfieldValue)}`;
        //fCode += `${params[i].stringfieldValue.js}`;
      }
    }
  }
  // Transaction parameters. TODO: Pensar sobre os outros atributos do parâmetro da transação (to, gasprice, etc)
  fCode += params.length > 1 ? ',' : '';
  //mesmo que o transactionParameter chegue sem valor, se a função é a sendTransaction, busc aum valor randômico
  fCode += `{from: ${
    transactionParameterValue.from == '"0x0000000000000000000000000000000000000000"'
      ? '"0x0"'
      : transactionParameterValue.from
  }${
    transactionParameterValue.value
      ? `,value:${transactionParameterValue.value}`
      : functionName == 'sendTransaction'
      ? `,value:${serializeJSIValue(getRandomValue(truffleProject, ElementTypeUINT, contract.contractDefinition, []))}`
      : ''
  }}`;
  // fecha chamada
  if (scenario.success) {
    fCode += ');\n';
  } else {
    fCode += "),'revert');\n";
  }
  // fecha função de teste
  fCode += '  });\n';
  // executa o teste somente com esta chamada e guarda o resultado
  const testResult =
    process.env.TEST_AFTER_EACH_FUNCTION === 'S'
      ? await truffleProject.executeTruffleTest(`${partialJSContract}${fCode}});\n`, 'js')
      : {
          code: 0,
        };
  return { fCode, testResult };
}
