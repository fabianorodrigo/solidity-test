import { IContract } from '../../../models/IContract';
import { TruffleProjectIceFactoryFunction } from '../../TruffleFactory';
import { IDeployedContract } from '../../../models/IBeforeEach';

/**
 *
 * @param {string} jsTestHeader The header of javascript test file
 * @param {*} contractsDeployed Array of contracts construction that already passed in the test
 * @param {*} paramsUserDefinitionTypeNameSortReviewd  Parameters of UserDefinitiontypeName that will need to be realocated
 * @param {*} variableDeclare Variable declarations of contracts
 * @param {*} validationFunction Function that will validate if the beforeEach code so far is valid
 * @returns The javascript code if was executed properly, NULL if the execution FAILED, FALSE for especific fails
 * when is detected, based on the message error, that is impossible to run that code
 *
 * @returns {string} Returns the beforeEach code if valid, or null if it's not
 */

export async function testBeforeEach(
  truffleProject: TruffleProjectIceFactoryFunction,
  jsTestHeader: string,
  contractsDeployed: IDeployedContract[],
  variableDeclare: string,
  validationFunction: Function
): Promise<string> {
  let result = '  beforeEach(async () => {\n';

  contractsDeployed.forEach((cd) => (result += generateInstanceCode(truffleProject, cd)));

  // Executa o código parcial contendo apenas o beforeEach e uma função de testes dummy apenas
  // para garantir que o beforeEach será executado
  let beforeEachResult = await validateBeforeEach(jsTestHeader, variableDeclare, result, validationFunction);
  if (beforeEachResult.stderr && beforeEachResult.stderr.indexOf('truffle: not found') > -1) {
    throw new Error(beforeEachResult.stderr);
  }
  if (beforeEachResult.stdout && beforeEachResult.stdout.indexOf("sender doesn't have enough funds") > -1) {
    throw new Error(beforeEachResult.stdout);
  }
  if (
    beforeEachResult.stdout.indexOf(`contract binary not set. Can't deploy new instance`) > -1 ||
    beforeEachResult.stdout.indexOf(
      'contains unresolved libraries. You must deploy and link the following libraries before you can deploy a new version of'
    ) > -1
  ) {
    truffleProject.registerUndeployableContract(contractsDeployed[contractsDeployed.length - 1].contractName);
    if (contractsDeployed.length == 1) return null;
    contractsDeployed.pop();
    result = '  beforeEach(async () => {\n';
    for (let i = 0; i < contractsDeployed.length; i++) {
      result += generateInstanceCode(truffleProject, contractsDeployed[i]);
    }
    beforeEachResult = await validateBeforeEach(jsTestHeader, variableDeclare, result, validationFunction);
  }
  return beforeEachResult.code == 0 ? result : null;
}

async function validateBeforeEach(jsTestHeader, variableDeclare, result, validationFunction) {
  return await validationFunction(
    `${jsTestHeader}${`${variableDeclare}\n${result}  });\n`}
      it('Should confirm beforeEach was executed', async () => { });
      });`
  );
}

/**
 * Deveria gerar algo assim:
 *
    contractSafeMath = await SafeMath.new({from: accounts[0]});
    SimpleToken.link("SafeMath",contractSafeMath.address);
    contractSimpleToken = await SimpleToken.new({from: accounts[0]});
 *
 * @param truffleProject
 * @param cd
 */
function generateInstanceCode(truffleProject: TruffleProjectIceFactoryFunction, cd: IDeployedContract): string {
  return (
    (truffleProject.getGraph().contracts[cd.contractName].kind != 'contract' ||
    Object.values(truffleProject.getGraph().contracts[cd.contractName].dependencies).filter(
      (d) => d.kind == 'library' && d.instanceNeeded == true
    ).length == 0
      ? ''
      : Object.values(truffleProject.getGraph().contracts[cd.contractName].dependencies)
          .filter((d) => d.kind == 'library' && d.instanceNeeded == true)
          .map((lb) => `    ${cd.contractName}.link("${lb.name}",contract${lb.name}.address);\n`)
          .join(' ')) +
    `    contract${cd.contractName} = ` +
    `await ${cd.contractName}.new(${cd.stringfieldParams});\n` +
    `    if(trace) console.log('SUCESSO: ${cd.contractName}.new(${cd.stringfieldParams}');\n`
  );
}
