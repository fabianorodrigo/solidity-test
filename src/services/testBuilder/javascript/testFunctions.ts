import colors from 'colors';
import path from 'path';
import { isPayable, debug, getRandomValue } from '../../Utils';
import { ElementTypeUINT, Types } from '../../types';
const uint = Types.uint;
import { getFunctionParametersValuesScenarios } from '../../scenarios';
import { IContract } from '../../../models/IContract';
import { FunctionDefinition, ContractDefinition } from 'solidity-parser-antlr';
import { IScenario, IScenarioFail } from '../../../models/scenario/IScenario';
import { TruffleProjectIceFactoryFunction } from '../../TruffleFactory';
import { updateTestedScenariosSummary } from '../updateTestedScenariosSummary';
import { DebugLevel } from '../../../models/debugLevel';
import { serializeJSIValue } from './serializeJSValue';
import { ITypeValue } from '../../../models/scenario';
import { getFunctionNameCalling } from './getFunctionNameCalling';
import { getInheritedFunctionsFromAbstract } from './getInheritedFunctionsFromAbstract';

/** *
 * Generate the source code of test functions for the contract in the {index} position of {contracts}
 * DO NOT generate test for functions with visibility INTERNAL or PRIVATE
 *
 * @param {object} truffleProject The instance of the Truffle Project to generate tests
 * @param {object} contract Contract in the {contracts} for which the test will be generated
 * @param {string} partialJSContract Partial content of the JS test file being generated. Has just declarations and beforeEach function and misses the enclosing of most external element
 *
 * @returns {string} The content (javascript source code) of test functions to be inserted in a JS test file
 */
export async function generateJavascriptTestFunctions(
  truffleProject: TruffleProjectIceFactoryFunction,
  contract: IContract,
  partialJSContract: string
): Promise<string> {
  const prjBaseName = path.basename(truffleProject.truffleProjectHome);
  // Pega o stdout após a compilação para checar diferença entre as diferentes execuções
  const RegexpTestResults = new RegExp('Compiled successfully using(.*)Contract: (.*)Error:(.*)', 'gs');
  let result = '  \n';
  const functionsDefinition: FunctionDefinition[] = truffleProject.getFunctions(contract);

  //Se não existir nenhuma função fallBack, inclui uma artificialmente para testar o envio de Ether pro contrato
  //if (!functionsDefinition.some((f) => isPayable(f))) functionsDefinition.push(artificialFallBackFunction);
  //Se o contrato herda de um outro contrato do projeto que é abstrato, inclui as funções pública ou external do pai do escopo
  const funcoesHerdadas = getInheritedFunctionsFromAbstract(truffleProject, contract.contractDefinition).filter((fd) =>
    ['external', 'public'].includes(fd.visibility)
  );
  funcoesHerdadas.forEach((fd) => {
    functionsDefinition.push(fd);
  });

  for (let f = 0; f < functionsDefinition.length; f++) {
    debug(colors.yellow(functionsDefinition[f].name));
    let countTests = 0;
    const fd = functionsDefinition[f];
    // Se a função não for pública ou se ela for abstrata (body == null), passa pra próxima
    if (fd.visibility == 'internal' || fd.visibility == 'private' || fd.body == null) {
      continue;
    }
    //count the number of tested functions
    global['generalResults'][prjBaseName].qtyTestedFunctions++;
    contract.contractDefinition.kind == 'contract'
      ? global['generalResults'][prjBaseName].qtyContractTestedFunctions++
      : global['generalResults'][prjBaseName].qtyLibraryTestedFunctions++;

    const ini = new Date();
    const scenarios: IScenario[] = getFunctionParametersValuesScenarios(
      truffleProject,
      fd,
      contract.contractDefinition
    );
    //summaryze the number of tested scenarios
    updateTestedScenariosSummary(prjBaseName, contract, scenarios, fd);

    for (let isce = 0; isce < scenarios.length; isce++) {
      const functionResults = [];
      const scenarioId = `${fd.name} - ${(<IScenarioFail>scenarios[isce]).paramNameRestriction || ''}: ${
        scenarios[isce].success ? '' : 'FALHA'
      }`;
      console.time(scenarioId);
      const params = scenarios[isce].parametersValues;

      //* *********####################################################################******** */
      // Se o scenário[i] envolver restrições de parâmetro de transação (msg.sender, msg.value),
      // invocará as funções passando os valores gerados
      //if ((<IScenarioFail>scenarios[isce]).transactionParameterRestriction) {
      const stringfieldTransactionParameterObj =
        scenarios[isce].parametersValues[scenarios[isce].parametersValues.length - 1].stringfieldValue.obj;
      const { fCode, testResult } = await generateFunctionTestCodeJavascript(
        scenarios[isce],
        fd,
        scenarios[isce].parametersValues,
        contract.contractDefinition,
        { from: stringfieldTransactionParameterObj.from, value: stringfieldTransactionParameterObj.value },
        truffleProject,
        partialJSContract
      );
      functionResults.push({
        accountIndex: scenarios[isce].parametersValues[scenarios[isce].parametersValues.length - 1].stringfieldValue,
        parameters: params,
        functionSource: fCode,
        ...testResult,
      });

      // selecionando resultados distintos
      const selectedFunctionResults = [];
      for (let fri = 0; fri < functionResults.length; fri++) {
        const fr = functionResults[fri];
        // Como o RegExp está sendo reutilizado. Precisamos zerar o lastIndex para começar da posição zero
        RegexpTestResults.lastIndex = 0;
        const frTestResults = RegexpTestResults.exec(fr.stdout);
        // Se no array dos selecionados não existe já um resultado igual, adiciona
        const selecionadoJa = selectedFunctionResults.find((sfr) => {
          let retorno = sfr.code === fr.code && sfr.stderr === fr.stderr;
          if (sfr.code !== 0 && retorno) {
            // Como o RegExp está sendo reutilizado. Precisamos zerar o lastIndex para começar da posição zero
            RegexpTestResults.lastIndex = 0;
            const sfrTestResults = RegexpTestResults.exec(sfr.stdout);
            retorno = (sfrTestResults == null && frTestResults == null) || sfrTestResults[3] === frTestResults[3];
          }
          return retorno;
        });
        if (selecionadoJa == null) {
          selectedFunctionResults.push(fr);
        }
      }
      countTests += selectedFunctionResults.length;

      // montando o código final resultante
      for (let sfri = 0; sfri < selectedFunctionResults.length; sfri++) {
        const sfr = selectedFunctionResults[sfri];
        // Quando há mais de um resultado diferente, vamos referenciar o nome da conta utilizada no from
        // Isso não é feito antes porque senão todo stdout daria resultado diferente pois ele contém o nome do teste
        if (selectedFunctionResults.length > 1) {
          // sfr.functionSource = `/*\n${sfr.stdout.match(RegexpTestResults)}\n*/\n${sfr.functionSource}`;
          sfr.functionSource = sfr.functionSource.replace(' - accounts[$xxx$]', ` - accounts[${sfr.accountIndex}]`);
        } else {
          sfr.functionSource = sfr.functionSource.replace(' - accounts[$xxx$]', '');
        }
        //
        result += sfr.functionSource;
      }
      if (!process.env.DEBUG_LEVEL || parseInt(process.env.DEBUG_LEVEL) >= DebugLevel.TRACE) {
        console.timeEnd(scenarioId);
      }
    }
    debug(
      `${contract.contractDefinition.name}.${fd.name} resultou ${countTests} testes em ${Math.round(
        (new Date().getTime() - ini.getTime()) / 1000
      )} segundos`
    );
  }
  return result;
}

/**
 * Generates a function test code in Javascript for a specific test scenario,
 * executes it using 'truffle test' and returns the code and the result
 *
 * @param {object} scenario Object representing the test scenario
 * @param {FunctionDefinition} fd Function Definition under test
 * @param {object[]} params array with parameters values generated
 * @param {string} contractName Contract's name which the {fd} belongs to
 * @param {string} transactionParameterValue The transaction parameter value with it's
 *  members in a string representantion
 * @param {TruffleProject} truffleProject The Truffle Project under test
 * @param {string} partialJSContract Partial content of the JS test file being generated.
 *  Has just declarations and beforeEach function and misses the enclosing of most external element
 * @returns {object} Object with the function test code in Javascript and it's results execution
 */
async function generateFunctionTestCodeJavascript(
  scenario: IScenario,
  fd: FunctionDefinition,
  params: ITypeValue[],
  contract: ContractDefinition,
  transactionParameterValue: { from: string; value: number },
  truffleProject,
  partialJSContract: string
) {
  let contractVarName = `contract${contract.name}`;
  let fCode = `  it('${scenario.scenarioDescription} - accounts[$xxx$]', async () => {\n`;
  //global references em forma de comentário
  //Object.keys(global['defaultReferences'][contract.name]).forEach((k) => {
  //  fCode += `    //${k}: ${global['defaultReferences'][contract.name][k].stringfieldValue.obj}\n`;
  //});
  //Se existir funções a serem chamadas para preparar o ambiente a fim de se exercitar
  //o cenário pretendido, inclui tais chamadas (eg. setar variáveis de estado)
  scenario.preFunctionCallings?.forEach((pfc) => {
    //Se for o construtor, cria uma nova instância
    if (pfc.functionCalled.isConstructor) {
      contractVarName = `local${contract.name}`;
      fCode += `    let ${contractVarName} = await ${contract.name}.new(${pfc.parametersValues
        .map((pv) => serializeJSIValue(pv.stringfieldValue))
        .join(',')});\n`;
    } /*TODO: else if(pfc.functionCalled.isPayable){
if (isPayable(functionDefinition) && !result[result.length - 1].stringfieldValue.obj.hasOwnProperty('value')) {
    const v = getRandomValue(truffleProject, ElementTypeUINT, contractDefinition, []);
    result[result.length - 1].stringfieldValue.obj.value = v.obj;
    result[result.length - 1].stringfieldValue.js = result[result.length - 1].stringfieldValue.js
      .substr(0, result[result.length - 1].stringfieldValue.js.length - 1)
      .concat(', value:', serializeJSIValue(v), '}');
  }
    } */ else {
      const functionName = getFunctionNameCalling(
        truffleProject,
        pfc.functionCalled.contract.contractDefinition,
        pfc.functionCalled.functionDefinition,
        pfc.parametersValues
      );
      const pfcContractVarName =
        pfc.contractInstanceTarget != contract.name ? `contract${pfc.contractInstanceTarget}` : contractVarName;
      fCode += `    await ${pfcContractVarName}.${functionName}(${pfc.parametersValues
        .map((pv) => serializeJSIValue(pv.stringfieldValue))
        .join(',')});\n`;
    }
    //fCode += `    await ${contractVarName}.${functionName}(`;
  });
  // Se a função não tem nome, trata-se de uma fallback (envio de Ether pro contrato) e
  // é testada de chamando o 'sendTransaction'
  const functionName = getFunctionNameCalling(truffleProject, contract, fd, scenario.parametersValues);
  if (scenario.success) {
    //se for o construtor ou a versão do truffle for anterior à 5, não suportará overload
    fCode += `    let result = await ${contractVarName}.${functionName}(`;
  } else {
    fCode += `    let result = await truffleAssert.fails(${contractVarName}.${functionName}(`;
  }

  // Os parâmetros na UtilsFactory vêm inclusive com o parâmetro de transação
  // Portanto, vamos ignorá-lo, já que aquina TestFactory implementamos uma
  // lõgica de randomização deste parâmetro que inclui a chamada com a conta
  // que chamou o construtor do contrato (suposto owner), uma outra conta aleatória
  // e ainda chamamos usando o parâmetro vindo da UtilsFactory quando se trata
  // de um cenário derivado de require em cima de membros do parâmetro de transação
  // Por isso só apendamos os parâmetros se existir mais de um e, se for o caso,
  // transcrevemos apenas até o params.length -1
  if (params.length > 1) {
    for (let i = 0; i < params.length - 1; i++) {
      if (i > 0) fCode += ', ';
      fCode += `${serializeJSIValue(params[i].stringfieldValue)}`;
      //fCode += `${params[i].stringfieldValue.js}`;
    }
  }
  // Transaction parameters. TODO: Pensar sobre os outros atributos do parâmetro da transação (to, gasprice, etc)
  fCode += params.length > 1 ? ',' : '';
  //mesmo que o transactionParameter chegue sem valor, se a função é a sendTransaction, busc aum valor randômico
  fCode += `{from: ${
    transactionParameterValue.from == '"0x0000000000000000000000000000000000000000"'
      ? '"0x0"'
      : transactionParameterValue.from
  }${
    transactionParameterValue.value
      ? `,value:${transactionParameterValue.value}`
      : functionName == 'sendTransaction'
      ? `,value:${serializeJSIValue(getRandomValue(truffleProject, ElementTypeUINT, contract, []))}`
      : ''
  }}`;
  // fecha chamada
  if (scenario.success) {
    fCode += ');\n';
  } else {
    fCode += "),'revert');\n";
  }
  // fecha função de teste
  fCode += '  });\n';
  // executa o teste somente com esta chamada e guarda o resultado
  const testResult =
    process.env.TEST_AFTER_EACH_FUNCTION === 'S'
      ? await truffleProject.executeTruffleTest(`${partialJSContract}${fCode}});\n`)
      : 0;
  return { fCode, testResult };
}
