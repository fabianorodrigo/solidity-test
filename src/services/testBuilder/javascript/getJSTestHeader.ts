import { IContractGraph } from '../../../models/graph';

/**
 * Generate the header of the test file. What is the artifacts.require("contractName") for each contract
 * and the declaration of test function 'contract("any string", accounts=>{\n'
 *
 * @param {object[]} contracts Array of Truffle Project's contracts
 * @param {string} testName Name to identify the test that will be created. It will be inserted as parameter to the 'contract'/'describe' function
 *
 * @returns {string} Javascript code
 */

export function getJSTestHeader(contracts: IContractGraph[], testName: string): string {
  let result = "const truffleAssert = require('truffle-assertions');\n";
  for (let c = 0; c < contracts.length; c++) {
    if (!contracts[c].isThirdPartyLib) {
      result += `const ${contracts[c].name} = artifacts.require("${contracts[c].name}");\n`;
    } else {
      const init = contracts[c].solFilePath.indexOf('/node_modules/') + '/node_modules/'.length;
      const relativePathFromNodeModules = contracts[c].solFilePath.substr(init);
      result += `const ${contracts[c].name} = artifacts.require("${relativePathFromNodeModules}");\n`;
    }
  }
  result += '\n';
  result += `contract("${testName}",(accounts)=>{\n`;
  return result;
}
