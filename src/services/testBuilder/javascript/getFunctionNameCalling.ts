import { TruffleProjectIceFactoryFunction } from '../../TruffleFactory';
import { FunctionDefinition, ContractDefinition } from 'solidity-parser-antlr';
import { isFunctionOverloaded } from '../../TruffleProjectGraph';
import { IScenario, ITypeValue } from '../../../models/scenario';

export function getFunctionNameCalling(
  truffleProject: TruffleProjectIceFactoryFunction,
  contract: ContractDefinition,
  fd: FunctionDefinition,
  parametersValues: ITypeValue[]
): string {
  const functionName = fd.name === '' ? 'sendTransaction' : fd.name;
  //Se NÃO existe sobreposição nem mesmo via herança
  //OU a versão do truffle não é a 5, chama a funçaõ direto, do contrário, acessa a função via propriedade 'methods'
  //ATENÇÃO: A partir da versão 5 do Truffle, suporta sobreposição, mas não se herdada (constatado isso no projeto curve-bonded-tokens)
  //A versão 4.* a partir de algum minor version tem esse recurso de chamar contrato.methods['assinatura'],
  //mas não são todas, a versão 4.1.5 não possui a propriedade 'methods' (constatado no projeto secretstore-acl).
  if (
    fd.name === '' ||
    !isFunctionOverloaded(truffleProject.getGraph().contracts[contract.name], fd.name) ||
    !truffleProject.truffleDependencySupportsOverload()
  ) {
    return functionName;
  } else {
    //Para suportar contratos que fazem overload de método, foi necessário mudar a forma de invocar o método (problema no Truffle)
    //O TRUFFLE (OU WEB3) CONTINUA COM PROBLEMAS NA SOBREPOSIÇÃO DE FUNÇÕES. VAMOS MANTER ASSIM DE FORMA FIXA
    return `methods["${functionName}(${parametersValues
      .filter((p) => p.type != 'TransactionParameter')
      .map(getParamType)
      .join(',')})"]`;
  }

  function getParamType(p) {
    if (p.type == 'uint') {
      return 'uint256';
    } else if (p.type == 'int') {
      return 'int256';
    } else if (p.subtype && truffleProject.getEnumsByName(p.subtype).length > 0) {
      return 'uint8';
    } else if (p.subtype && truffleProject.getStructs()[p.subtype]) {
      return `(${truffleProject
        .getStructs()
        [p.subtype].members.map((m) =>
          getParamType(
            m.typeName.type == 'UserDefinedTypeName'
              ? { subtype: m.typeName.namePath }
              : { type: (m.typeName as any).name }
          )
        )
        .join(',')})`;
    } else if (
      p.type == 'UserDefinedTypeName' &&
      p.subtype &&
      truffleProject.getContracts().find((c) => c.contractDefinition.name == p.subtype)
    ) {
      return 'address';
    } else if (p.type == 'ArrayTypeName') {
      return p.subtype;
    } else {
      return p.type;
    }
  }
}
