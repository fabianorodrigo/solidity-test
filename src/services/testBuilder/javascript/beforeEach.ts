import { isAbstractContract } from '../../solidityParser';
import { generateCodeContractInstance } from './contractInstance';
import { IContract } from '../../../models/IContract';
import { IBeforeEach, IFinalBeforeEach } from '../../../models/IBeforeEach';
import { ValidationCodeFunction, TruffleProjectIceFactoryFunction } from '../../TruffleFactory';
import { IContractGraph } from '../../../models/graph';
import { mark } from '../../Utils';

/** *
 * Generate the migration in format of the method beforeEach
 *
 * A transação de criação do contrato está sendo realizada sempre como a accounts[0]
 *
 * @param {object} truffleProject Instance of Truffle Project under analysis
 * @param {function} jsTestHeaderFunction A function that receives a array of contracts and a string with test name and
 * generates the header of javascript test file
 * @param {object[]} contracts Array of Project's Contracts
 * @param {function} validationFunction Function to validate if the code generated is valid.
 * Poderia ter chamado simplesmente truffleProject.executeTruffleTest, mas por questões de testabilidade,
 * da própria ferramenta solidity-test foi feita essa injeção de dependência
 * @returns {object} Object with Javascript code and list of contracts deployed
 */
export async function generateMigrationBeforeEach(
  truffleProject: TruffleProjectIceFactoryFunction,
  jsTestHeaderFunction: Function,
  contracts: IContractGraph[],
  validationFunction: ValidationCodeFunction
): Promise<IFinalBeforeEach> {
  // Guarda os contratos na ordem em que tiveram seus construtores chamados no beforeEach
  // Isso é necessário pois caso o construtor de algum dos contratos tenha com parâmetro
  // de entrada um UserDefinitionTypeName (contrato, interface, ...), um contrato deste
  // tipo já tem que ter sido deployado antes
  let result = { variableDeclare: '  let trace = false;\n', contractsDeployed: [], result: '' };

  const jsTestHeader = jsTestHeaderFunction(contracts, 'validation beforeEach');
  for (let i = 0; i < contracts.length; i++) {
    if (result.contractsDeployed.find((c) => c.contractName == contracts[i].contractDefinition.name)) {
      continue;
    }
    result = await generateCodeContractInstance(
      truffleProject,
      contracts[i].contractDefinition,
      result,
      jsTestHeader,
      validationFunction
    );
  }
  // Se tiver tentando mais de 20x sem sucesso, retorna null
  if (result == null) {
    mark('generateMigrationBeforeEach resultou em null');
    /*const befEachDeployed = await generateBeforeEachDeployed(
      jsTestHeaderFunction,
      contracts,
      result.variableDeclare,
      validationFunction
    );
    if (befEachDeployed == null) {
      return null;
    } else {
      result = befEachDeployed;
    }*/
    //FIXME
  }
  //return variableDeclare.concat(result != '' ? result.concat('  });\n') : '');
  //return result.variableDeclare.concat(result.result != '' ? result.result.concat('  });\n') : '');
  return {
    code: result.variableDeclare.concat(result.result != '' ? result.result.concat('  });\n') : ''),
    contractsDeployed: result.contractsDeployed,
  };
}

/**
 * Gera um código beforeEach que não cria novas instâncias de contratos mas pega as instâncias
 * deployadas pelo migration do projeto
 *
 * @param {function} jsTestHeaderFunction A function that receives a array of contracts and a string with test name and
 * generates the header of javascript test file
 * @param {object[]} contracts Array of Project's Contracts
 * @param {*} variableDeclare Variable declarations of contracts
 * @param {*} validationFunction Function that will validate if the beforeEach code so far is valid
 *
 * @returns {string} Returns the beforeEach code if valid, or null if it's not
 */
async function generateBeforeEachDeployed(jsTestHeaderFunction, contracts, variableDeclare, validationFunction) {
  let result = '  beforeEach(async () => {\n';

  contracts.forEach((cd) => {
    // Não instancia biblioteca, nem interface, nem contrato abstrato
    if (
      cd.contractDefinition.kind != 'library' &&
      cd.contractDefinition.kind != 'interface' &&
      !isAbstractContract(cd.contractDefinition)
    ) {
      result += `    try{\n`;
      result += `      contract${cd.contractDefinition.name} = `;
      result += `await ${cd.contractDefinition.name}.deployed();\n`;
      result += `    } catch(e){\n`;
      result += `      console.error(e);\n`;
      result += `    }\n`;
    }
  });

  // Executa o código parcial contendo apenas o beforeEach e uma função de testes dummy apenas
  // para garantir que o beforeEach será executado
  const beforeEachResult = await validationFunction(
    `${jsTestHeaderFunction(contracts, 'validation beforeEach')}${`${variableDeclare}\n${result}  });\n`}
      it('Should confirm beforeEach was executed', async () => { });
      });`
  );
  if (beforeEachResult.stderr && beforeEachResult.stderr.indexOf('truffle: not found') > -1) {
    throw new Error(beforeEachResult.stderr);
  }
  return beforeEachResult.code == 0 ? result : null;
}
