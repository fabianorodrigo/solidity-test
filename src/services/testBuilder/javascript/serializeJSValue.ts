import { IValue } from '../../../models/IValue';
import { ElementaryTypeName, TypeName } from 'solidity-parser-antlr';

export function serializeJSIValue(value: IValue): string {
  if ((<ElementaryTypeName>value?.typeName)?.name?.startsWith('bytes')) {
    if (Array.isArray(value.obj)) {
      return `[${value.obj.toString()}]`;
    } else {
      return `"${value.obj}"`;
    }
  } else if (
    (<ElementaryTypeName>value?.typeName)?.name?.startsWith('uint') ||
    (<ElementaryTypeName>value?.typeName)?.name?.startsWith('int')
  ) {
    if (value.obj > Number.MAX_SAFE_INTEGER) {
      return `new web3.utils.BN('${value.obj}')`;
    } else {
      return value.js;
    }
  }
  return value.js;
}

export function serializeJSValue(value: any, typeName: TypeName): string {
  if ((<ElementaryTypeName>typeName)?.name?.startsWith('bytes')) {
    if (Array.isArray(value)) {
      return value.toString();
    } else {
      return `"${value}"`;
    }
  }
  return value;
}
