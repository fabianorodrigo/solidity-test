import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { TruffleProjectIceFactoryFunction } from '../../TruffleFactory';

/**
 * Caso o contrato {contractDefinition} tenha herança direta (primeiro nível) de contratos
 * ABSTRATOS que pertençam ao projeto (not isThirdPartyLib), retorna as funções desses
 * contratos abstratos que ainda não foram implementadas (excluindo construtor e fallback)
 *
 * @param truffleProject Projeto Truffle analisado
 * @param contractDefinition contrato analisado
 */
export function getInheritedFunctionsFromAbstract(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractDefinition: ContractDefinition
): FunctionDefinition[] {
  const result: FunctionDefinition[] = [];
  const targetContract = truffleProject.getGraph().contracts[contractDefinition.name];
  const ancestrais = Object.values(targetContract.inheritsFrom).filter(
    (a) => a.contract.isAbstract && !a.contract.isThirdPartyLib && a.level == 0
  );
  ancestrais.forEach((a) => {
    Object.values(a.contract.functions).forEach((array) => {
      array
        .filter((fg) => fg.isConstructor == false && fg.isPayable == false)
        .forEach((fg) => {
          //Só adiciona se o contrato já não implementa a dita cuja
          if (
            !Object.values(targetContract.functions).some((array) => array.some((f) => f.signature == fg.signature))
          ) {
            result.push(fg.functionDefinition);
          }
        });
    });
  });
  return result;
}
