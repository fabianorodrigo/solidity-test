import { FunctionDefinition } from 'solidity-parser-antlr';
import { IFunctionGraph } from '../../../../models/graph';
import { getType } from '../../../solidityParser';
import { TruffleProjectIceFactoryFunction } from '../../../TruffleFactory';

export function getFunctionParametersDeclarationText(
  truffleProject: TruffleProjectIceFactoryFunction,
  funcDef: FunctionDefinition,
  qtPreviousParams = 0
): string {
  let resultingCode = '';
  funcDef.parameters.forEach((p, i) => {
    if (qtPreviousParams > 0 || i > 0) resultingCode += ',';
    resultingCode += `${getType(truffleProject.getGraph(), p.typeName)} ${
      p.typeName.hasOwnProperty('stateMutability') ? (p.typeName as any).stateMutability : ''
    } ${p.storageLocation ? p.storageLocation : ''} ${p.name}`;
  });
  return resultingCode;
}
