import { ContractDefinition } from 'solidity-parser-antlr';
import { IContractGraph } from '../../../../models/graph';
import { TruffleProjectIceFactoryFunction } from '../../../TruffleFactory';
import { sortAncestrais } from '../../../TruffleProjectGraph';

/**
 * Retorna a cadeia de herança de um contrato (incluindo o próprio) respeitando a regra “C3 Linearization”
 * https://solidity.readthedocs.io/en/v0.4.24/contracts.html#multiple-inheritance-and-linearization
 *
 * @param truffleProject
 * @param contract
 */
export function getContractChainInheritance(
  truffleProject: TruffleProjectIceFactoryFunction,
  contract: ContractDefinition
): IContractGraph[] {
  const result: IContractGraph[] = [];
  let cadeiaHeranca: IContractGraph[] = [];

  //se o contrato analisado for abstrato, inclui ele na cadeia
  const contractGraph = truffleProject.getGraph().contracts[contract.name];
  //if (contractGraph.isAbstract) {
  cadeiaHeranca.push(contractGraph);
  //}
  //e em seguida, seus ancestrais
  cadeiaHeranca = cadeiaHeranca.concat(
    Object.values(truffleProject.getGraph().contracts[contract.name].inheritsFrom)
      .sort(sortAncestrais)
      .map((ih) => ih.contract)
  );

  cadeiaHeranca.forEach((cg) => {
    //SE existir algum contrato no resultado que já herde de {cg},
    //não precisa incluir {cg} no resultado, assim, respeitamos a C3 Linearization
    //AH! Mas se o contrato que estiver em resultado for ABSTRACT,
    //incluiremos assim mesmo. Pois ele pode ser aquele abstrato
    //devido ao fato de não passar para o seu pai todos os parâmetros
    //como acontece no projeto crowdwale onde o MinLimitCrowdsale não
    //repassa os parâmetros para o Crowdsale
    if (result.find((c) => c.inheritsFrom[cg.name] != null && c.isAbstract == false) == null) {
      result.push(cg);
    }
  });
  result.reverse();
  return result;
}
