import { getFunctions } from '../../solidityParser';
export * from './beforeEach';
export * from './migration';
import { ContractDefinition } from 'solidity-parser-antlr';
import { getNameTypeName } from './getNameTypeName';
export * from './getSolidityHeader';
export * from './testFunctions';
import { generateProxyContract } from './generateProxyContract';
import { TruffleProjectIceFactoryFunction } from '../../TruffleFactory';
export * from './isExperimentalABIEncoderV2';
export * from './getDataLocationTypeName';
export * from './generateSolidityTests';
export * from './getProxyTestFunctionName';
export * from './isFunctionProxyable';

export { generateProxyContract };
