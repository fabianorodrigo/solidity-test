/**
 * Get the name of type depending oh it type
 *
 * @param {object} typeName AST Type Name
 * @param {object} structsPool A object with list of structs indentified
 */

import { IStructsPool } from '../../../models/IStructsPool';
import { isIdentifier } from '../../solidityParser';
import { TruffleProjectIceFactoryFunction } from '../../TruffleFactory';

export function getNameTypeName(truffleProject: TruffleProjectIceFactoryFunction, typeName, length?: number) {
  if (typeName.type == 'ArrayTypeName') {
    return `${getNameTypeName(truffleProject, typeName.baseTypeName)}[${
      typeName?.length?.number ? typeName.length.number : length ? length : '' //FIXME: Dá pra deixar só o atributo de IValue?
    }]`;
  } else if (typeName.type == 'UserDefinedTypeName') {
    if (truffleProject.getGraph().structs[typeName.namePath]) {
      return `${truffleProject.getGraph().structs[typeName.namePath].contract.name}.${
        truffleProject.getGraph().structs[typeName.namePath].name
      }`;
    } else if (truffleProject.getGraph().contracts[typeName.namePath]) {
      return typeName.namePath;
    } else {
      throw new Error(`Que cenárioe é esse? Debuga aqui`);
      /*const struct = Object.values(structsPool).find((s) => s.name == typeName.namePath);
      if (struct) {
        return `${struct.parentName}.${struct.name}`;
      } else {
        return typeName.namePath;
      }*/
    }
  } else {
    return typeName.name;
  }
}
