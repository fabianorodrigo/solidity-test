import {
  ContractDefinition,
  FunctionDefinition,
  ArrayTypeName,
  UserDefinedTypeName,
  TypeName,
} from 'solidity-parser-antlr';
import { ITypeValue } from '../../../models/scenario/ITypeValue';
import { IScenarioFail } from '../../../models/scenario/IScenario';
import { IContract } from '../../../models/IContract';
import { getFunctions } from '../../solidityParser';
import path from 'path';
import { getFunctionParametersValuesScenarios } from '../../scenarios';
import { getNameTypeName } from './getNameTypeName';
import { getDataLocationTypeName } from './getDataLocationTypeName';
import { updateTestedScenariosSummary } from '../updateTestedScenariosSummary';
import { info } from '../../Utils';
/** *
 * Generate the source code of test functions for the contract in the {index} position of {contracts}
 *
 * @param {object} truffleProject Instance of Truffle Project under analysis
 * @param {contract} Contract in the {contracts} for which the function tests will be generated
 * @param {object[]} contracts Array of Contracts in the scope
 * @param {object} valuesPool A object with pre-selected values to be used as parameters
 * @param {string} partialSOLContract Partial content of the SOL test file being generated. Has just declarations and misses the enclosing of most external element
 *
 * @returns {string} The content (solidity source code) of test functions to be inserted in a SOL test file
 */
export async function generateSolidityTestFunctions(truffleProject, contract: IContract): Promise<string> {
  const prjBaseName = path.basename(truffleProject.truffleProjectHome);
  /*
    if a library contains only internal functions and/or structs/enums, then the library doesn’t need to be deployed,
    as everything that’s there in the library is copied to the contract that uses it.
    */
  const iniContrato = new Date();
  let result = '  \n';

  const functionsDefinition: FunctionDefinition[] = getFunctions(contract.contractDefinition);
  const testFunctionNames = {};
  // forEach não respeita o await
  for (let f = 0; f < functionsDefinition.length; f++) {
    const fd = functionsDefinition[f];
    // Se a função for privada, for construtor ou for internal de um contrato, passa pra próxima
    if (fd.visibility == 'private' || fd.name == '') {
      continue;
    }

    //count the number of tested functions
    global['generalResults'][prjBaseName].qtyTestedFunctions++;
    contract.contractDefinition.kind == 'contract'
      ? global['generalResults'][prjBaseName].qtyContractTestedFunctions++
      : global['generalResults'][prjBaseName].qtyLibraryTestedFunctions++;

    //Se o cenário tiver algum valor de parâmetro em Solitiy igual a null, não é possível gerar
    //funçaõ para ele pois a linguagem não suporta o valor 'null'
    const scenarios = getFunctionParametersValuesScenarios(
      truffleProject,
      fd,
      contract.contractDefinition
    ).filter((s) => s.parametersValues.every((pv) => pv.stringfieldValue.sol != null));
    //summaryze the number of tested scenarios
    updateTestedScenariosSummary(prjBaseName, contract, scenarios, fd);

    for (let isce = 0; isce < scenarios.length; isce++) {
      const scenarioId = `${fd.name} - ${(<IScenarioFail>scenarios[isce]).paramNameRestriction || ''}: ${
        scenarios[isce].success ? '' : 'FALHA'
      }`;
      console.time(scenarioId);
      const params = scenarios[isce].parametersValues;
      // Os parâmetros na UtilsFactory vêm inclusive com o parâmetro de transação
      // Portanto, vamos ignorá-lo, pois, para uma chamada dentro de um contrato Solidity,
      // isso não faz sentido
      params.pop();
      let testFunctionName = `test_${fd.name}${scenarios.length > 1 ? isce : ''}`;
      if (!testFunctionNames[testFunctionName]) {
        testFunctionNames[testFunctionName] = [testFunctionName];
      } else {
        testFunctionNames[testFunctionName].push(testFunctionName.concat(testFunctionNames[testFunctionName].length));
        testFunctionName = testFunctionNames[testFunctionName][testFunctionNames[testFunctionName].length - 1];
      }
      let fCode = `  //${scenarios[isce].scenarioDescription}\n`;
      fCode += `  function ${testFunctionName}() public {\n`;

      // Se a funçaõ tiver visibilidade 'internal' e tem parâmetros
      // o tipo do primeiro parâmetro foi atachado a biblioteca
      // através da clásula 'using {biblioteca} for {tipo}'
      if (contract.contractDefinition.kind == 'library' && fd.visibility == 'internal' && fd.parameters.length > 0) {
        //Se for struct, usa a variável da intãncia já criada
        let varName = 'varLib';
        const struct = truffleProject.getStructs()[(<UserDefinedTypeName>fd.parameters[0].typeName).namePath];
        if (struct) {
          varName = `var${struct.name}`;
          fCode += `    ${varName} = ${params[0].stringfieldValue.sol};\n`;
        } else if (
          truffleProject.getStructs()[
            contract.contractDefinition.name.concat('.', (<UserDefinedTypeName>fd.parameters[0].typeName).namePath)
          ]
        ) {
          varName = `var${(<UserDefinedTypeName>fd.parameters[0].typeName).namePath}`;
          fCode += `    ${varName} = ${params[0].stringfieldValue.sol};\n`;
        } else {
          fCode += `    ${getNameTypeName(
            truffleProject,
            fd.parameters[0].typeName,
            params[0].stringfieldValue.length
          )} ${getDataLocationTypeName(truffleProject, fd.parameters[0])} varLib = ${getValueInstanceSolidityVariable(
            truffleProject,
            contract.contractDefinition,
            fd.parameters[0].typeName,
            params[0].stringfieldValue.sol,
            'varLib'
          )}`;
        }
        fCode += getFunctionCall(truffleProject, contract.contractDefinition, fd, params, varName);
      } else if (contract.contractDefinition.kind == 'contract') {
        //TODO: Avaliar se precisa mesmo desse elseif ou se poderia mesclar com o else. A dúvida está naquele if sobre o contract.kind
        fCode += generateParamsVariables(fd, params);
        fCode += generateReturnVariables(fd);

        fCode += `${fd.name}(`;

        params.forEach((p, i) => {
          if (i > 0) fCode += ', ';
          fCode += `param${i}`;
        });
      } else {
        //fCode += `    ${contracts[index].contractDefinition.name} lib = ${contracts[index].contractDefinition.name}(DeployedAddresses.${contracts[index].contractDefinition.name}());\n`;
        fCode += generateParamsVariables(fd, params);
        fCode += generateReturnVariables(fd);
        if (contract.contractDefinition.kind !== 'library') {
          fCode += `contract`;
        }
        fCode += `${contract.contractDefinition.name}.${fd.name}(`;

        params.forEach((p, i) => {
          if (i > 0) fCode += ', ';
          fCode += `param${i}`;
        });

        // https://ethereum.stackexchange.com/questions/48547/truffle-test-library-typeerror-member-not-found-or-not-visible-after-argument
        // TODO: Pode ser o fato de ser 'internal' TypeError: Member "sub" not found or not visible after argument-dependent lookup in library SafeMath
        // ou;
        // https://ethereum.stackexchange.com/questions/49342/truffle-test-how-to-test-safemath-library-for-overflow
        // TODO: bool passed = address(instance).call(bytes4(keccak256("add(uint256,uint256)")), 10, max);
      }

      // fecha chamada
      fCode += ');\n';
      // fecha função de teste
      fCode += '  }\n';
      result += fCode;
    }
  }
  info(
    `${contract.contractDefinition.name}: concluído em ${Math.round(
      (new Date().getTime() - iniContrato.getTime()) / 1000
    )} segundos`
  );
  return result;

  function generateParamsVariables(fd: FunctionDefinition, params: ITypeValue[]): string {
    let fCode = '';
    if (fd.parameters) {
      fd.parameters.forEach((parameter, i) => {
        fCode += `    ${getNameTypeName(
          truffleProject,
          parameter.typeName,
          params[i].stringfieldValue.length
        )} ${getDataLocationTypeName(truffleProject, parameter)} param${i} = ${params[i].stringfieldValue.sol};\n`;
      });
    }
    return fCode;
  }

  function generateReturnVariables(fd: FunctionDefinition): string {
    let fCode = '';
    let varDeclare = '';
    if (fd.returnParameters) {
      fd.returnParameters.forEach((parameter, i) => {
        varDeclare += `    ${getNameTypeName(truffleProject, parameter.typeName)} ${getDataLocationTypeName(
          truffleProject,
          parameter
        )} result${i};\n`;
        if (i > 0) {
          fCode += ', ';
        }
        fCode += `result${i}`;
      });
      if (fd.returnParameters.length > 1) {
        fCode = `    (${fCode})`;
      } else {
        fCode = '    ' + fCode;
      }
      fCode = varDeclare + fCode;
      if (fd.returnParameters) fCode += ` = `;
    } else {
      fCode += `    `;
    }
    return fCode;
  }
}

/**
 * Get the value instance based on type
 *
 * @param {object} typeName AST Type Name
 */
function getValueInstanceSolidityVariable(
  truffleProject,
  contractDefinition: ContractDefinition,
  typeName: TypeName,
  stringValueSolidity: string,
  varName: string
): string {
  //se array de struct
  if (
    typeName.type == 'ArrayTypeName' &&
    typeName.baseTypeName.type == 'UserDefinedTypeName' &&
    (truffleProject.getStructs()[contractDefinition.name.concat('.', typeName.baseTypeName.namePath)] ||
      truffleProject.getStructs()[typeName.baseTypeName.namePath]) &&
    stringValueSolidity.startsWith('[') &&
    stringValueSolidity.endsWith(']')
  ) {
    const structFullName = truffleProject.getStructs()[typeName.baseTypeName.namePath]
      ? `${truffleProject.getStructs()[typeName.baseTypeName.namePath].parentName}.${
          truffleProject.getStructs()[typeName.baseTypeName.namePath].name
        }`
      : contractDefinition.name.concat('.', typeName.baseTypeName.namePath);
    const splitedElements = stringValueSolidity
      .substr(1, stringValueSolidity.length - 2)
      .split(`${structFullName}(`)
      .filter((e) => e.trim() != '');
    const countElements = splitedElements.length;
    let retorno = `new ${structFullName}[](${countElements});\n`;
    for (let i = 0; i < countElements; i++) {
      retorno += `  ${varName}[${i}] = ${structFullName}(${splitedElements[i]}`;
      if (retorno.endsWith(',')) retorno = retorno.substr(0, retorno.length - 1);
      retorno += ';\n';
    }
    return retorno;
  } else {
    return stringValueSolidity.toString().concat(';\n');
  }
}

function getFunctionCall(
  truffleProject,
  contractDefinition: ContractDefinition,
  fd: FunctionDefinition,
  params: ITypeValue[],
  varName: string
): string {
  let fCode = '  ';

  //declara uma variável para cada parâmetro de entrada e instancia
  params.forEach((p, i) => {
    if (
      i > 0 && //se array de struct
      fd.parameters[i].typeName.type == 'ArrayTypeName' &&
      (<ArrayTypeName>fd.parameters[i].typeName).baseTypeName.type == 'UserDefinedTypeName' &&
      (truffleProject.getStructs()[
        contractDefinition.name.concat(
          '.',
          (<UserDefinedTypeName>(<ArrayTypeName>fd.parameters[i].typeName).baseTypeName).namePath
        )
      ] ||
        truffleProject.getStructs()[
          (<UserDefinedTypeName>(<ArrayTypeName>fd.parameters[i].typeName).baseTypeName).namePath
        ]) &&
      p.stringfieldValue.sol.startsWith('[') &&
      p.stringfieldValue.sol.endsWith(']')
    ) {
      fCode += `    ${getNameTypeName(
        truffleProject,
        fd.parameters[i].typeName,
        p.stringfieldValue.length
      )} ${getDataLocationTypeName(truffleProject, fd.parameters[i])} varParam${i} = ${getValueInstanceSolidityVariable(
        truffleProject,
        contractDefinition,
        fd.parameters[i].typeName,
        p.stringfieldValue.sol,
        `varParam${i}`
      )}\n`;
    }
  });
  //fCode += generateReturnVariables(fd);
  fCode += `${varName}.${fd.name}(`;
  //faz apenas referência às variáveis já declaradas e instanciadas
  params.forEach((p, i) => {
    if (i > 1) fCode += ', ';
    if (i > 0) {
      if (
        fd.parameters[i].typeName.type == 'ArrayTypeName' &&
        (<ArrayTypeName>fd.parameters[i].typeName).baseTypeName.type == 'UserDefinedTypeName' &&
        (truffleProject.getStructs()[
          contractDefinition.name.concat(
            '.',
            (<UserDefinedTypeName>(<ArrayTypeName>fd.parameters[i].typeName).baseTypeName).namePath
          )
        ] ||
          truffleProject.getStructs()[
            (<UserDefinedTypeName>(<ArrayTypeName>fd.parameters[i].typeName).baseTypeName).namePath
          ]) &&
        p.stringfieldValue.sol.startsWith('[') &&
        p.stringfieldValue.sol.endsWith(']')
      ) {
        fCode += `varParam${i}`;
      } else {
        fCode += `${p.stringfieldValue.sol}`;
      }
    }
  });

  return fCode;
}
