import { VariableDeclaration, ElementaryTypeName } from 'solidity-parser-antlr';
import { getType } from '../../solidityParser';
/**
 * Get the data location of type
 * Explicit data location for all variables of struct, array or mapping types is now mandatory.
 * @param  {object} truffleProject The Truffle Project being analysed
 * @param {VariableDeclaration} parameter Function parameter in the AST
 */
export function getDataLocationTypeName(truffleProject, parameter: VariableDeclaration): string {
  //se o parâmetro já tiver um storageLocation, retorna este. Senão, retorna de acordo com o tipo
  return parameter.storageLocation
    ? parameter.storageLocation
    : parameter.typeName.type == 'ArrayTypeName' ||
      (<ElementaryTypeName>parameter.typeName).name == 'bytes' ||
      (<ElementaryTypeName>parameter.typeName).name == 'string' ||
      (parameter.typeName.type == 'UserDefinedTypeName' &&
        truffleProject.getGraph().structs[getType(truffleProject.getGraph(), parameter.typeName)])
    ? ' memory '
    : ' ';
}
