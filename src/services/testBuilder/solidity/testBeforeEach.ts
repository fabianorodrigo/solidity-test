import { IDeployedContract } from '../../../models/IBeforeEach';
import { ITestResult } from '../../../models/ITestResult';

/**
 *
 * @param {object} truffleProject Instance of Truffle Project under analysis
 * @param {string} solTestHeader The header of solidity test file
 * @param {*} contractsDeployed Array of contracts construction that already passed in the test
 * @param {*} variableDeclare Variable declarations of contracts
 * @param {*} validationFunction Function that will validate if the beforeEach code so far is valid
 * @returns The Solidity code if was executed properly, NULL if the execution FAILED, FALSE for especific fails
 * when is detected, based on the message error, that is impossible to run that code
 *
 * @returns {string} Returns the beforeEach code if valid, or null if it's not
 */
export async function testBeforeEach(
  truffleProject,
  solTestHeader: string,
  contractsDeployed: IDeployedContract[],
  variableDeclare: string,
  validationFunction: Function
): Promise<string> {
  let result = '  function beforeEach() public{\n';

  contractsDeployed.forEach(cd => (result += generateInstanceCode(cd)));

  // Executa o código parcial contendo apenas o beforeEach e uma função de testes dummy apenas
  // para garantir que o beforeEach será executado
  let beforeEachResult: ITestResult = await validateBeforeEach(
    solTestHeader,
    variableDeclare,
    result,
    validationFunction
  );
  if (beforeEachResult.stderr && beforeEachResult.stderr.indexOf('truffle: not found') > -1) {
    throw new Error(beforeEachResult.stderr);
  }
  if (beforeEachResult.stdout && beforeEachResult.stdout.indexOf("sender doesn't have enough funds") > -1) {
    throw new Error(beforeEachResult.stderr);
  }
  if (beforeEachResult.stdout.indexOf(`contract binary not set. Can't deploy new instance`) > -1) {
    truffleProject.registerUndeployableContract(contractsDeployed[contractsDeployed.length - 1].contractName);
    if (contractsDeployed.length == 1) return null;
    result = '  function beforeEach() public{\n';
    for (let i = 0; i < contractsDeployed.length - 1; i++) {
      result += generateInstanceCode(contractsDeployed[i]);
    }
    beforeEachResult = await validateBeforeEach(solTestHeader, variableDeclare, result, validationFunction);
  }
  return beforeEachResult.code == 0 ? result : null;
}

async function validateBeforeEach(
  solTestHeader: string,
  variableDeclare: string,
  result: string,
  validationFunction: Function
): Promise<ITestResult> {
  return await validationFunction(
    `${solTestHeader}\ncontract TestBeforeEach{\n${`${variableDeclare}\n${result}}\n`}
      function ShouldConfirmBeforeEachExecutes() public{}\n}`,
    'sol'
  );
}

function generateInstanceCode(cd): string {
  return `    contract${cd.contractName} = ` + `new ${cd.contractName}(${cd.stringfieldParams});\n`;
}
