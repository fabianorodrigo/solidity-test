/**
 *
 * @param testContractName The name of contract that tests the library under testesj
 * @param libraryName Name of the library under tests
 * @returns The content of a Truffle migration file that publish the library under test, link with the contract that tests it
 * and publish the contract test
 */
export function getMigrationContentFile(testContractName: string, libraryName: string) {
  return `const TestContract = artifacts.require("${testContractName}");
  const LibraryUnderTest = artifacts.require("${libraryName}");

   async function doDeploy(deployer, network) {
       await deployer.deploy(LibraryUnderTest);
       await deployer.link(LibraryUnderTest, TestContract);
       await deployer.deploy(TestContract);
   }

 module.exports = (deployer, network) => {
      deployer.then(async () => {
              await doDeploy(deployer, network);
      });
  };`;
}
