import { TruffleProjectIceFactoryFunction } from '../../TruffleFactory';
import { IContract } from '../../../models/IContract';
import { FunctionDefinition } from 'solidity-parser-antlr';
import { getFunctionSignature } from '../../TruffleProjectGraph';
import { DebugLevel } from '../../../models/debugLevel';
import { debug } from '../../Utils';

/**
 * O web3 que o Truffle 4 utiliza possui uma limitação de não conseguir tratar sobreposição de funções do contrato
 * Caso o projeto tenha dependência local de uma versão anterior e a função tenha sobreposição, será
 * necessário colocar um nome diferente na função do contrato proxy para contornar essa situação eliminando
 * a existência de sobreposição
 * @param truffleProject Projeto Truffle em análise
 *
 * @param contract  Contrato original para o qual está sendo gerado proxy
 * @param functionDefinition  Função original para a qual está sendoo gerado proxy
 */
export function getProxyTestFunctionName(
  truffleProject: TruffleProjectIceFactoryFunction,
  contract: IContract,
  functionDefinition: FunctionDefinition
) {
  let proxyFunctionName = `test${functionDefinition.name}`;
  if (
    //truffleProject.truffleDependencySupportsOverload() &&
    truffleProject.getGraph().contracts[contract.contractDefinition.name].functions[functionDefinition.name]?.length >
      1 &&
    functionDefinition.name !== ''
  ) {
    proxyFunctionName += truffleProject
      .getGraph()
      .contracts[contract.contractDefinition.name].functions[functionDefinition.name].findIndex(
        (s) => s.signature == getFunctionSignature(truffleProject.getGraph(), functionDefinition, '')
      );
    debug('proxyFunctionName', proxyFunctionName);
  }
  return proxyFunctionName;
}
