import { getSolidityHeader, generateSolidityTestFunctions } from '.';
import { IContract } from '../../../models/IContract';
import { TruffleProjectIceFactoryFunction } from '../../TruffleFactory';
import { IStruct } from '../../../models/IStructsPool';
import { getUsingForStatementQuestionavel } from '..';

export async function generateSolidityTests(
  truffleProject: TruffleProjectIceFactoryFunction,
  contract: IContract,
  beforeEachCodeSOL: string
) {
  //resulting code
  let resultingCode = '';
  resultingCode += getSolidityHeader(truffleProject, truffleProject.getContracts());
  resultingCode += `contract Test${contract.contractDefinition.name} {\n`;
  //Criação de uma variável nonce que pode ser eventualmente usada caso tenha como parâmetro um endereço
  //*** LINHA ABAIXO COMENTADA POIS ESTAVA ENTRANDO EM DUPLICIDADE COM A DECLAREÇÃO GERADA EM generateMigrationBeforeEach */
  //resultingCode += `\n  uint nonce = ${Math.ceil(Math.random() * 100)};\n`;
  resultingCode += beforeEachCodeSOL;
  // Se a função tiver visibilidade 'internal' e tiver parâmetros
  // incluímos a cláusula 'using for' para o tipo do primeiro parâmetro
  if (contract.contractDefinition.kind == 'library') {
    resultingCode += getUsingForStatementQuestionavel(truffleProject, contract.contractDefinition);
  }
  //Criando uma variável local para cada struct encontrada
  //https://ethereum.stackexchange.com/questions/4467/initialising-structs-to-storage-variables
  Object.values(truffleProject.getStructs())
    .filter((s) => s.fullName === true)
    .forEach((s) => {
      resultingCode += `  ${s.parentName}.${s.name} var${s.name};\n`;
    });

  resultingCode += await generateSolidityTestFunctions(truffleProject, contract);
  resultingCode += '}\n';
  return resultingCode;
}
