import { isAbstractContract, getContractConstructor } from '../../solidityParser';
import { getSuccessfulScenarioAndParameterRestrictions } from '../../scenarios';
import { getRandomValue } from '../../../services/Utils';
import { testBeforeEach } from './testBeforeEach';
import { ContractDefinition } from 'solidity-parser-antlr';
import { IBeforeEach } from '../../../models/IBeforeEach';
import { ValidationCodeFunction, TruffleProjectIceFactoryFunction } from '../../TruffleFactory';
import { IBranch } from '../../../models/IBranch';
import { getParametersValues } from '../../Utils/UtilsFactory';

const MAX_ATTEMPTS = 2;

/**
 * Generate the code that instance the {contract} and the dependencies of it's constructor (parameter with type of other contracts)

 * @param {contractDefinition} contractDefinition The contractDefinition in the AST
 * @param {object} previousResult Object with the information accumulated about previous calling, information about instance
 * of other contracts in the same project: contractsDeployed, variableDeclare, result, etc
 * @param {string} solTestHeader Solidity code in the header of solidity test file
 * @param {function} validationFunction Function to validate if the code generated is valid.
 * Poderia ter chamado simplesmente truffleProject.executeTruffleTest, mas por questões de testabilidade,
 * da própria ferramenta solidity-test foi feita essa injeção de dependência
 *
 * @returns {string} Returns the javascript code that intances the contract in the chain and, eventually, it's dependencies
 */

export async function generateCodeContractInstance(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractDefinition: ContractDefinition,
  previousResult: IBeforeEach,
  solTestHeader: string,
  validationFunction: ValidationCodeFunction
): Promise<IBeforeEach> {
  let tentativas = 0;
  const currentResult = JSON.parse(JSON.stringify(previousResult)); //clone to not mutate
  // Não instancia biblioteca, nem interface, nem contrato abstrato,
  //nem contrato que não tenha construtor com visibilidade 'public' ou 'default'
  if (
    contractDefinition.kind != 'library' &&
    contractDefinition.kind != 'interface' &&
    !isAbstractContract(contractDefinition)
  ) {
    currentResult.variableDeclare += `${contractDefinition.name} contract${contractDefinition.name};\n`;

    const constructor = getContractConstructor(contractDefinition);
    // Se existe construtor mas ele não tem visibilidade pública, não constará no beforeEach
    if (constructor != null && constructor.visibility != 'public' && constructor.visibility != 'default') {
      //TODO:SE O CONSTRUTOR FOR NAO VISIVEL, TEM QUE INSTNACIAR UM FILHO
      return currentResult;
    }
    let partialBeforeEachExecutionSucessfull = false;
    // Não passará para a próxima interação do contrato enquanto não appendar no
    // contractsDeployed uma chamada válida
    whilePrincipal: while (!partialBeforeEachExecutionSucessfull && tentativas <= MAX_ATTEMPTS) {
      if (truffleProject.getUndeployableContracts().includes(contractDefinition.name)) {
        return previousResult;
      }
      tentativas++;
      //Se o construtor não existe
      if (constructor == null) {
        currentResult.contractsDeployed.push({
          contractName: contractDefinition.name,
          stringfieldParams: '',
          sort: -1000,
        });
      } else {
        const { scenario } = getSuccessfulScenarioAndParameterRestrictions(
          truffleProject,
          constructor,
          contractDefinition,
          getParametersValues,
          { restrictions: [] } as IBranch, //não vamos tratar de branch restrictions neste momento
          {}
        );
        // FIXME: APenas enquanto não terminamos a mudança da obtenção do TransactionParameter, vamos remover o último parâmetro
        scenario.parametersValues.pop();
        const constructorParams = scenario.parametersValues;

        let stringfieldParams = '';
        for (let iParam = 0; iParam < constructorParams.length; iParam++) {
          const p = constructorParams[iParam];
          if (iParam > 0) stringfieldParams += ', ';
          if (p.type == 'UserDefinedTypeName') {
            // Se o contrato do tipo do parãmetro não está na lista que já teve o  código de instanciação gerado,
            // busca esse código para inserir antes
            if (!currentResult.contractsDeployed.find((c) => c.contractName == p.subtype)) {
              let contractDefinitionDependency = truffleProject
                .getContracts()
                .find((c) => c.contractDefinition.name == p.subtype).contractDefinition;
              //avalia se é uma interface ou contrato abstrato. Se for o caso, precisa encontrar uma implementação sua
              if (
                contractDefinitionDependency.kind == 'interface' ||
                isAbstractContract(contractDefinitionDependency)
              ) {
                const sameType = truffleProject.getValuesPool()[contractDefinitionDependency.name];
                for (let ist = 0; ist < sameType.length; ist++) {
                  contractDefinitionDependency = truffleProject
                    .getContracts()
                    .find((c) => c.contractDefinition.name == sameType[ist]).contractDefinition;
                  if (
                    contractDefinitionDependency.kind != 'interface' &&
                    !isAbstractContract(contractDefinitionDependency)
                  ) {
                    break;
                  }
                }
              }
              if (!currentResult.contractsDeployed.find((c) => c.contractName == contractDefinitionDependency.name)) {
                const dependencyResult = await generateCodeContractInstance(
                  truffleProject,
                  contractDefinitionDependency,
                  previousResult,
                  solTestHeader,
                  validationFunction
                );
                //Se a instanciação da dependência retornar exatamente o mesmo resultado 'previousResult', algo deu errado
                //começamos a próxima interação do while para tentar gerar novos valores
                //de dependencia nao passar novamente por getSuccessfulScenarioAndParameterRestrictions
                if (JSON.stringify(dependencyResult) == JSON.stringify(previousResult)) continue whilePrincipal;

                currentResult.variableDeclare = dependencyResult.variableDeclare;
                //como o atributo variableDeclare é sobrescrito, repetimos o comando de concatenação com o contrato corrente
                currentResult.variableDeclare += `${contractDefinition.name} contract${contractDefinition.name};\n`;

                currentResult.result = dependencyResult.result;
                currentResult.contractsDeployed = dependencyResult.contractsDeployed;
              }
              p.stringfieldValue.sol = `contract${contractDefinitionDependency.name}`;
            }
          }
          stringfieldParams += `${p.stringfieldValue.sol}`;
        }

        currentResult.contractsDeployed.push({
          contractName: contractDefinition.name,
          stringfieldParams,
        });
      }
      const testResult = await testBeforeEach(
        truffleProject,
        solTestHeader,
        currentResult.contractsDeployed,
        currentResult.variableDeclare,
        validationFunction
      );
      // If not passed, the last element in contractsDeployed is removed
      if (testResult == null) {
        currentResult.contractsDeployed.pop();
      } else {
        currentResult.result = testResult;
        partialBeforeEachExecutionSucessfull = true;
      }
    }
  }
  return currentResult;
}
