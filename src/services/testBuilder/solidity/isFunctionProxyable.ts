import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { getContractConstructor } from '../../solidityParser';

/**
 * Determinadas condições  impossibilitam a geração do teste para uma função de um contrato de forma direta.
 * Por exemplo, funções em um contrato do tipo 'library' ou funções que tenham visibilidade 'internal'.
 * Para isso, a estratégia é criar um contrato proxy que conterá funções que simplesmente repassarão os parâmetros.
 * Isso é feito somente para funções que não possam ser testadas diretamente.
 * @returns TRUE se as condições da função {functionDefinition} levam a necessidade de gerar proxy e se também é possível
 */
export function isFunctionProxyable(
  contractDefinition: ContractDefinition,
  functionDefinition: FunctionDefinition
): boolean {
  const constructor = getContractConstructor(contractDefinition);
  //funções privadas não são proxiáveis pois o proxy é criado baseado na herança, e private
  //não estará visível
  // Se o contrato tem construtor definido mas não tem é visível OU é uma biblioteca
  // ENTÃO: gera proxy de todas as funções, SENÃO: somente as com visibilidade 'internal'
  let retorno =
    functionDefinition.visibility != 'private' &&
    (functionDefinition.body == null ||
      (constructor && !['public', 'default'].includes(constructor.visibility)) ||
      contractDefinition.kind == 'library' ||
      functionDefinition.visibility == 'internal');
  if (!retorno) return retorno;
  //Recebemos uma exceção "Stack Too Deep" no projeto real-estate-marketplace, library BN256G2, função  _ECTwistAddJacobian
  //essa função tem 12 parâmetros de entrada do tipo uint256, mais de saída do tipo uint256[6]
  //No mesmo projeto, na library Pairing, função pairingProd4, também recebemos. Essa função recebe
  // 8 parãmetros de entrada (struct com data location MEMORY (???)) e um booleano de saída.
  //https://blog.aventus.io/stack-too-deep-error-in-solidity-5b8861891bae
  //O que tiver mais de 8 parâmetro somados, vamos limar
  return (
    process.env.ALLOW_UNLIMITED_PARAMS_IN_PROXY_CONTRACTS == 'S' ||
    functionDefinition.parameters.length +
      (functionDefinition.returnParameters ? functionDefinition.returnParameters.length : 0) <=
      8
  );
}
