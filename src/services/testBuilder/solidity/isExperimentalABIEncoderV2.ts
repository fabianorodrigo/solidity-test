import { TruffleProjectIceFactoryFunction } from '../../TruffleFactory';
import { IContractGraph } from '../../../models/graph';

/**
 * Identifica no grafo que há alguma característica no projeto que force a necessidade de se incluir
 * a diretiva "pragma experimental ABIEncoderV2" os arquivos .sol gerados por nós
 *
 * @param truffleProject Truffle Project under analysis
 */
export function isExperimentalABIEncoderV2(truffleProject: TruffleProjectIceFactoryFunction) {
  const graph = (truffleProject as TruffleProjectIceFactoryFunction).getGraph();
  //Se alguma função tiver parâmetro do tipo struct ou array de struct, precisa usar o pragma experimental ABIEncoderV2;
  //Senão analisados somente contratos do projeto (exclui os de bibliotecas de terceiros)
  const contractsGraph: IContractGraph[] = Object.values(graph.contracts).filter((cg) => cg.isThirdPartyLib == false);
  for (let i = 0; i < contractsGraph.length; i++) {
    const functionsGraph = Object.values(contractsGraph[i].functions);
    for (let j = 0; j < functionsGraph.length; j++) {
      //functionsGraph[j] é um array com um item para cada assinatura (overload)
      for (let z = 0; z < functionsGraph[j].length; z++) {
        //input parameters
        for (let p = 0; p < functionsGraph[j][z].parameters.length; p++) {
          //Se algum parâmetro de entrada ou de retorno de função for STRUCT, precisará utilizar o pragma experimental ABIEncodeV2
          if (functionsGraph[j][z].parameters[p].isStruct) {
            return true;
          }
          //Solidity não suporta dois níveis e arrays dinâmicos por default, e como string é uma array dinâmico por si só,
          //Se existir um parãmetro array de string, precisará incluir a diretiva
          //Exemplo onde isso ocorre: Projeto truffle, contract Merkleship, function hashEach
          if (
            functionsGraph[j][z].parameters[p].type.startsWith('string[') &&
            functionsGraph[j][z].parameters[p].isArray
          ) {
            return true;
          }
        }
        //return parameters
        for (let p = 0; p < functionsGraph[j][z].return.length; p++) {
          //Se algum parâmetro de entrada ou de retorno de função for STRUCT, precisará utilizar o pragma experimental ABIEncodeV2
          if (functionsGraph[j][z].return[p].isStruct) {
            return true;
          }
          //Solidity não suporta dois níveis e arrays dinâmicos por default, e como string é uma array dinâmico por si só,
          //Se existir um parãmetro array de string, precisará incluir a diretiva
          //Exemplo onde isso ocorre: Projeto truffle, contract Merkleship, function hashEach
          if (functionsGraph[j][z].return[p].type.startsWith('string[') && functionsGraph[j][z].return[p].isArray) {
            return true;
          }
        }
      }
    }
  }
  return false;
}
