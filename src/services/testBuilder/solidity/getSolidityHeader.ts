import { TruffleProjectIceFactoryFunction } from '../../TruffleFactory';
import { IContract } from '../../../models/IContract';
import path from 'path';

/**
 * Produces the header of a Solidity test file. Since 'pragma solidity {solc version}' and imports based on the
 * {contracts} passed
 *
 * @param {array} contracts Array of contracts of the project
 */
export function getSolidityHeader(
  truffleProject: TruffleProjectIceFactoryFunction,
  contracts: IContract[],
  importTruffleLibs: boolean = true,
  relativePath: string = path.join(truffleProject.truffleProjectHome, 'test')
): string {
  const solcVersion = null; //truffleProject.getCompilerVersion();
  const solidityCoverageVersion = truffleProject.getSolidityCoverageVersion();
  return (solcVersion ? `pragma solidity ${solcVersion};\n` : '')
    .concat(importTruffleLibs ? 'import "truffle/Assert.sol";\n' : '')
    .concat(
      solidityCoverageVersion == '0.6.7' ? '' : importTruffleLibs ? 'import "truffle/DeployedAddresses.sol";\n' : ''
    ) //https://github.com/sc-forks/solidity-coverage/issues/496#issuecomment-612101099
    .concat(getImportStatements(truffleProject, contracts, relativePath))
    .concat(`\n`);
}

/**
 * Produces the import statements of distinct solidity files in the {contracts} passed
 *
 * @param {array} contracts Array of contracts of the project
 */
function getImportStatements(
  truffleProject: TruffleProjectIceFactoryFunction,
  contracts: IContract[],
  relativePath: string
): string {
  return contracts
    .map((c) => {
      return `import "${path.relative(relativePath, c.solFilePath)}";\n`;
    })
    .reduce((acumulador, importStatement, i) => {
      return i == 0
        ? importStatement
        : acumulador.indexOf(importStatement) == -1
        ? acumulador + importStatement
        : acumulador;
    }, '');
}
