import { IContract } from '../../../models/IContract';
import { TruffleProjectIceFactoryFunction } from '../../TruffleFactory';
import path from 'path';
import fs from 'fs';
import {
  getFunctions,
  getType,
  isArray,
  getContractConstructor,
  isAbstractContract,
  newConstructor,
} from '../../solidityParser';
import { getSolidityHeader } from './getSolidityHeader';
import {
  FunctionDefinition,
  ContractDefinition,
  VariableDeclaration,
  ArrayTypeName,
  ElementaryTypeName,
} from 'solidity-parser-antlr';
import { fstat } from 'fs';
import { IContractGraph, ITruffleProjectGraph } from '../../../models/graph';
import { getInheritedFunctionsFromAbstract, getUsingForStatementQuestionavel } from '../index';
import { getProxyTestFunctionName } from './getProxyTestFunctionName';
import { isFunctionProxyable } from './isFunctionProxyable';
import { sortAncestrais } from '../../TruffleProjectGraph';
import { getContractChainInheritance, getFunctionParametersDeclarationText } from './proxy';
import result from '../../../webserver/routes/result';

/**
 * Create a solidity file with a contract that just passes
 *
 * @param contract Target contract for which the generates contract
 */
export function generateProxyContract(
  truffleProject: TruffleProjectIceFactoryFunction,
  contract: IContract,
  experimentalABIEncoderV2: boolean
): IContract {
  let retorno: IContract = {} as IContract;
  const proxyContractName = `Proxy${contract.contractDefinition.name}`;
  const proxyPath = path.join(truffleProject.truffleProjectHome, 'contracts/proxy4tests/');
  retorno.contractDefinition = {
    name: proxyContractName,
    kind: contract.contractDefinition.kind,
    baseContracts: [{ type: 'InheritanceSpecifier', baseName: { namePath: contract.contractDefinition.name } }],
    subNodes: [],
  } as ContractDefinition;
  retorno.solFilePath = path.join(proxyPath, contract.contractDefinition.name.concat('.sol'));

  const functions = getFunctions(contract.contractDefinition);
  const proxyFunctions = functions.filter((f) => isFunctionProxyable(contract.contractDefinition, f));

  let resultingCode = experimentalABIEncoderV2 ? 'pragma experimental ABIEncoderV2;\n' : '';

  resultingCode += getSolidityHeader(truffleProject, [contract], false, proxyPath); //import somente no proxyado

  /**
   * Pega a cadeia de herança mas somente para contratos que possuam algum construtor com parâmetros
   */
  const cadeiaHeranca = getContractChainInheritance(truffleProject, contract.contractDefinition);
  const cadeiaHerancaComConstructor = cadeiaHeranca.filter(
    (c) =>
      c.functions['constructor'] != null &&
      c.functions['constructor'][0] != null &&
      c.functions['constructor'][0].parameters.length > 0
  );

  resultingCode += `contract ${proxyContractName} ${
    contract.contractDefinition.kind == 'contract' ? ` is ${cadeiaHeranca.map((c) => c.name)} ` : ''
  } {\n\n    `;

  //se existir construtor, temos que replicá-lo e repassar os parâmetros
  //Contudo, esta solução implicou na não compilação do projeto minime:
  //UnimplementedFeatureError: Copying of type struct MiniMeToken.Checkpoint memory[] memory to storage not yet supported
  //Embora o construtor não tenha nenhuma referênia a este struct ...
  //Vamos aplicar o critério de que se experimentalABIEncoderV2, não replica o construtor
  if (!experimentalABIEncoderV2) {
    let contaParametros = 0;
    const constructor = getContractConstructor(contract.contractDefinition, true);
    if (constructor) {
      resultingCode += `  constructor(`;
      //se o contrato proxyado é abstrato e herda de algum outro, vai precisar incluir na lista de parâmetros os parâmetros
      //deste pai do proxyado
      if (cadeiaHerancaComConstructor.length > 0) {
        retorno.contractDefinition.subNodes.push(newConstructor());
        cadeiaHerancaComConstructor.forEach((ancestral) => {
          const constructorAncestral = getContractConstructor(ancestral.contractDefinition, true);
          if (constructorAncestral != null) {
            resultingCode += getFunctionParametersDeclarationText(
              truffleProject,
              constructorAncestral,
              contaParametros
            );
            (retorno.contractDefinition.subNodes[0] as FunctionDefinition).parameters = (retorno.contractDefinition
              .subNodes[0] as FunctionDefinition).parameters.concat(constructorAncestral.parameters);
            contaParametros += constructorAncestral.parameters.length;
          }
        });
      }
      resultingCode += `) public ${cadeiaHerancaComConstructor
        .map((c) => c.name.concat('(', c.functions['constructor'][0].parameters.map((p) => p.name).toString(), ')'))
        .join(' ')} {}\n`;
    }
  }

  //Se for biblioteca já declaramos o 'using for'
  if (contract.contractDefinition.kind == 'library') {
    resultingCode += getUsingForStatementQuestionavel(truffleProject, contract.contractDefinition);
  }

  //Se tiver structs no contrato, criamos uma variável de instância pois structs precisam ser storage location
  resultingCode += getStorageInstanceVariableStatement(truffleProject, contract, proxyFunctions);

  //Se o contrato herda de um outro contrato do projeto que é abstrato, inclui as funções INTERNAL do pai do escopo
  /*const funcoesHerdadas = getInheritedFunctionsFromAbstract(truffleProject, contract.contractDefinition).filter(
    (fd) => fd.visibility == 'internal'
  );
  funcoesHerdadas.forEach((fd) => {
    proxyFunctions.push(fd);
  });*/

  proxyFunctions.forEach((functionToBeProxyed) => {
    //Se a função tem algum parâmetro que tem data location STORAGE e, logo, vai utilizar a variável
    //de instância respectiva, deixamos sem mutabilidade (pure, view, etc).
    //Do contrário, usa a mesma da função que está sendo proxyada
    const stateMutability =
      functionToBeProxyed.parameters.some((p) => {
        return p.storageLocation == 'storage';
      }) || functionToBeProxyed.stateMutability == null
        ? ''
        : functionToBeProxyed.stateMutability;

    let proxyFunctionName = getProxyTestFunctionName(truffleProject, contract, functionToBeProxyed);
    retorno.contractDefinition.subNodes.push({ ...functionToBeProxyed, name: proxyFunctionName });

    resultingCode += `   function ${proxyFunctionName}(${functionToBeProxyed.parameters
      .map((p) => getProxyParameters(truffleProject, p, 'declare'))
      .join(', ')}) public ${stateMutability} ${getReturnDeclaration(
      truffleProject.getGraph(),
      functionToBeProxyed
    )}{\n`;
    //Se a função recebe algum parãmetro do tipo storage, precisa criar uma variável local STORAGE e
    //reatribuir os valores do parâmetro de entrada da função, que são MEMORY, antes de repassar à função original
    resultingCode += convertParametersToDataLocationStorage(truffleProject, functionToBeProxyed);
    //se a função original tiver retorno, repassamos o retorno
    resultingCode += `    ${
      functionToBeProxyed.returnParameters && functionToBeProxyed.returnParameters.length > 0 ? 'return ' : ''
    }`;
    //Se biblioteca e a função recebe parâmetros, a função é chamada a partir do primeiro parâmetro e
    // não diretamente (lembra do using ... for ...?)
    if (contract.contractDefinition.kind == 'library' && functionToBeProxyed.parameters.length > 0) {
      //se o primeiro parâmetro tem data location STORAGE, precisa usar a variável de instância
      //Do contrário, usa o primeiro parâmetro mesmo
      if (functionToBeProxyed.parameters[0].storageLocation == 'storage') {
        //se for estruct, vai pegar o nome simples dela
        const typeNameParam = getType(truffleProject.getGraph(), functionToBeProxyed.parameters[0].typeName);
        const struct = truffleProject.getGraph().structs[typeNameParam];
        resultingCode += (struct ? `var${struct.name}` : functionToBeProxyed.parameters[0].name).concat('.');
      } else {
        resultingCode += functionToBeProxyed.parameters[0].name.concat('.');
      }
    } else if (contract.contractDefinition.kind == 'library' && functionToBeProxyed.parameters.length == 0) {
      resultingCode += `${contract.contractDefinition.name}.`;
    }
    //An external function f cannot be called internally (i.e. f() does not work, but this.f() works).
    if (contract.contractDefinition.kind == 'contract' && functionToBeProxyed.visibility == 'external') {
      resultingCode += 'this.';
    }
    resultingCode += getProxyFunctionCall(truffleProject, contract, functionToBeProxyed);

    resultingCode += `   }\n\n`;
  });

  resultingCode += '\n}';
  if (!fs.existsSync(proxyPath)) {
    fs.mkdirSync(proxyPath);
  }

  fs.writeFileSync(retorno.solFilePath, resultingCode);
  return retorno;
}

/**
 * Para cada parâmetro das funções que tenham data location STORAGE, criará uma variável de instância
 *
 * @param truffleProject
 * @param contract
 * @param functions
 * @deprecated
 */
function getStorageInstanceVariableStatement(
  truffleProject: TruffleProjectIceFactoryFunction,
  contract: IContract,
  functions: FunctionDefinition[]
) {
  type varDeclare = { type: string; name: string };
  let retorno = '';
  const instanceVariables = {};
  //Buscamos por parâmetros de função nas funções proxiadas que são declaradas com data location STORAGE
  functions.forEach((pf) => {
    const paramsDataLocationStorage = getParametersStorage(truffleProject, pf);
    paramsDataLocationStorage.forEach((pos) => {
      let posType = getType(truffleProject.getGraph(), pos.typeName);
      if (isArray(pos.typeName)) {
        const posBaseType = getType(truffleProject.getGraph(), (<ArrayTypeName>pos.typeName).baseTypeName);
        let name = posBaseType;
        const struct = truffleProject.getGraph().structs[posBaseType];
        if (struct) {
          name = struct.name;
        }
        instanceVariables[posType] = {
          type: posType,
          name: `varArray${name}`,
        };
      } else {
        let name = posType;
        const struct = truffleProject.getGraph().structs[posType];
        if (struct) {
          name = struct.name;
        }
        instanceVariables[posType] = { type: posType, name: `var${name}` };
      }
    });
  });
  //declarando variáveis dos tipos referenciados como storage
  Object.values(instanceVariables).forEach((s: varDeclare) => {
    retorno += `  ${s.type} ${s.name};\n`;
  });

  return retorno;
}

/**
 * Retorna todos os parâmetros da função que tenham data location em STORAGE
 *
 * @param functionDefinition Função analisada
 */
function getParametersStorage(
  truffleProject: TruffleProjectIceFactoryFunction,
  functionDefinition: FunctionDefinition
): VariableDeclaration[] {
  return functionDefinition.parameters.filter((p) => p.storageLocation == 'storage');
}

/**
 * Gera o código que invoca a função original de dentro da função proxy: funcaOrginal(param1, param2, param3);
 * @param truffleProject
 * @param contract
 * @param functionDefinition
 */
function getProxyFunctionCall(
  truffleProject: TruffleProjectIceFactoryFunction,
  contract: IContract,
  functionDefinition: FunctionDefinition
) {
  let retorno = `${functionDefinition.name}(`;
  //Se for library, começa a partir do segundo parâmetro pois o primeiro, graças ao using for, fica sendo <primeiroParametro>.funcaoBiblioteca(p2,p3...)
  let paramInicial = contract.contractDefinition.kind == 'library' && functionDefinition.parameters.length > 0 ? 1 : 0;
  functionDefinition.parameters.forEach((p, i) => {
    let name = p.name;
    if (i >= paramInicial) {
      if (i > paramInicial) {
        retorno += ',';
      }
      //Se o parâmetro da função original tiver data location MEMORY, apenas repassa o parâmetro recebido.
      //Se for storage, um tratamento precisará ser feito
      if (p.storageLocation == 'storage') {
        //Se o parâmetro for um struct no contrato originanl, seus membros foram transformados em vários parâmetros na função proxy
        //que foram passados para o construtor para atribuir à variável de instãncia logo na primeira instrução da função proxy
        //Neste caso, o que será passado para a função original deve ser a variável de instância
        //O mesmo acontece para os casos de array de struct, ou seja, chama a variável de instância
        const struct = truffleProject.getGraph().structs[getType(truffleProject.getGraph(), p.typeName)];
        if (struct) {
          //TODO: Vai dar problema quando existir mais de um parâmetro do mesmo tipo struct
          name = `var${struct.name}`;
        }
        let arrayStruct = null;
        if (isArray(p.typeName)) {
          //TODO: Vai dar problema quando existir mais de um parâmetro do mesmo tipo array
          name = `varArray${(<ElementaryTypeName>(<ArrayTypeName>p.typeName).baseTypeName).name}`;
          arrayStruct = truffleProject.getGraph().structs[
            getType(truffleProject.getGraph(), (<ArrayTypeName>p.typeName).baseTypeName)
          ];
          if (arrayStruct) {
            //TODO: Vai dar problema quando existir mais de um parâmetro do mesmo tipo array de struct
            name = `varArray${arrayStruct.name}`;
          }
        }
      }
      retorno += name;
    }
  });
  retorno += ');\n';
  return retorno;
}

/**
 * Se a função original receber algum struct com data location STORAGE, precisa atribuir valores recebidos
 * dos parâmetros de entrada , que são location MEMORY, a uma variável  do tipo STORAGE para poder repassá-los
 * Essa função retorna o código que cria uma variável local STORAGE usando os parâmetros recebidos da função
 * proxy (ex: Struct storage varStructInstancia = Struct(a,b,c))
 * Se a função não possuir nenhum parâmetro com data location STORAGE, retorna string vazia
 * @param truffleProject
 * @param functionDefinition Função sendo proxiada e que será verificada a existência de parâmetros do tipo struct
 */
function convertParametersToDataLocationStorage(
  truffleProject: TruffleProjectIceFactoryFunction,
  functionDefinition: FunctionDefinition
) {
  let retorno = '';
  functionDefinition.parameters
    .filter((p) => p.storageLocation == 'storage')
    .forEach((p) => {
      let posType = getType(truffleProject.getGraph(), p.typeName);
      const struct = truffleProject.getGraph().structs[posType];
      let arrayStruct = null;
      if (isArray(p.typeName)) {
        arrayStruct = truffleProject.getGraph().structs[
          getType(truffleProject.getGraph(), (<ArrayTypeName>p.typeName).baseTypeName)
        ];
      }

      if (struct) {
        //TODO: Vai dar problema quando existir mais de um parâmetro do mesmo tipo struct
        retorno += `    var${struct.name} = ${struct.contract.name}.${struct.name}(${getProxyParameters(
          truffleProject,
          p,
          'consume'
        )});\n`;
      } else if (arrayStruct) {
        retorno += `    //Since we receive a parameter with storage location 'memory', we have to pass the values to the instance variable in 'storage'\n`;
        retorno += `    varArray${arrayStruct.name} = new ${arrayStruct.contract.name}.${arrayStruct.name}[](${p.name}.length);\n`;
        retorno += `    for(uint it__ = 0; it__ < ${p.name}.length; it__++){\n`;
        retorno += `        varArray${arrayStruct.name}.push(${p.name}[it__]);\n`;
        retorno += `    }\n`;
      }
    });
  return retorno;
}

/**
 * Rebece o parâmetro da função sendo proxiada e de acordo com o {use}:
 * declare: gera string com código de declaração dos parãmetros da função proxy (eg. function xpto(<isso aqui>)).
 *          Sendo struct com data location STORAGE, declara um parâmetro para cada membro do struct
 * consume: Se struct, apena replica o nome dos seus membros separados por vírgula
 * Um parâmetro do tipo struct, por exemplo, será quebrado em vários parâmetros
 * @param parameter Parameter of original function being proxied
 */
function getProxyParameters(
  truffleProject: TruffleProjectIceFactoryFunction,
  parameter: VariableDeclaration,
  use: 'declare' | 'consume' = 'declare'
) {
  const paramType = getType(truffleProject.getGraph(), parameter.typeName);
  const struct = truffleProject.getGraph().structs[paramType];
  if (parameter.storageLocation == 'storage' && struct) {
    //Se for Mapping, não inclui no desmembramento
    //The mapping is also ignored in assignments of structs or arrays containing a mapping.
    //https://solidity.readthedocs.io/en/develop/security-considerations.html#clearing-mappings
    return struct.parameters
      .filter((m) => !m.type.startsWith('mapping('))
      .map((m) => {
        if (use == 'declare') {
          return `${m.type} ${['string', 'bytes'].includes(m.type) ? 'memory' : ''} ${parameter.name}${m.name}`;
        }
        return `${parameter.name}${m.name}`;
      });
  } else if (use == 'declare') {
    let dataLocation = parameter.storageLocation == null || parameter.storageLocation.trim() == '' ? '' : 'memory';
    //Para as funções 'internal', um parâmetro de entrada pode ser 'storage'
    //mas aqui, como ela está sendo declarada como pública e, assim, pode vir de fora da chain
    //os parâmetros não podem ser 'storage'
    //Lembrando que: Data location can only be specified for array, struct or mapping types,
    return `${paramType} ${dataLocation} ${parameter.name}`;
  }
}

function getReturnDeclaration(graph: ITruffleProjectGraph, functionDefinition: FunctionDefinition) {
  if (functionDefinition.returnParameters?.length > 0) {
    return `returns (${functionDefinition.returnParameters
      .map((p) => {
        let type = getType(graph, p.typeName);
        //se struct, coloca nome completo
        const struct = graph.structs[type];
        if (struct) {
          type = struct.contract.name.concat('.', struct.name);
        }
        return `${type} ${p.storageLocation ? 'memory' : ''}`; //como é uma função pública, se tiver um data location storage, vamos ter que reverter para 'memory'
      })
      .join(', ')})`;
  }
  return '';
}
