import { IContract } from '../../models/IContract';
import { IScenario } from '../../models/scenario/IScenario';
import { FunctionDefinition } from 'solidity-parser-antlr';
import { isPayable } from '../Utils';

/**
 *
 * @param prjBaseName
 * @param contract
 * @param scenarios scenarios gerados para a função corrente dentro da interação por funções lá no testFunctions.ts
 */
export function updateTestedScenariosSummary(
  prjBaseName: string,
  contract: IContract,
  scenarios: IScenario[],
  functionDefinition: FunctionDefinition
) {
  //total geral de scenarios
  global['generalResults'][prjBaseName].qtyTestedScenarios =
    global['generalResults'][prjBaseName].qtyTestedScenarios + scenarios.length;
  //total por KIND: contract/library
  global['generalResults'][prjBaseName].scenariosSummary[contract.contractDefinition.kind].total =
    global['generalResults'][prjBaseName].scenariosSummary[contract.contractDefinition.kind].total + scenarios.length;

  //total por contrato/library
  if (
    !global['generalResults'][prjBaseName].scenariosSummary[contract.contractDefinition.kind].contracts[
      contract.contractDefinition.name
    ]
  ) {
    global['generalResults'][prjBaseName].scenariosSummary[contract.contractDefinition.kind].contracts[
      contract.contractDefinition.name
    ] = { total: 0, functions: {} };
  }
  global['generalResults'][prjBaseName].scenariosSummary[contract.contractDefinition.kind].contracts[
    contract.contractDefinition.name
  ].total =
    global['generalResults'][prjBaseName].scenariosSummary[contract.contractDefinition.kind].contracts[
      contract.contractDefinition.name
    ].total + scenarios.length;
  //total por função
  const functionName = functionDefinition.isConstructor
    ? 'constructor'
    : (isPayable(functionDefinition) ? 'fallback ' : '') + functionDefinition.name;
  if (
    !global['generalResults'][prjBaseName].scenariosSummary[contract.contractDefinition.kind].contracts[
      contract.contractDefinition.name
    ].functions[functionName]
  ) {
    global['generalResults'][prjBaseName].scenariosSummary[contract.contractDefinition.kind].contracts[
      contract.contractDefinition.name
    ].functions[functionName] = { total: 0, visibility: functionDefinition.visibility };
  }
  global['generalResults'][prjBaseName].scenariosSummary[contract.contractDefinition.kind].contracts[
    contract.contractDefinition.name
  ].functions[functionName].total =
    global['generalResults'][prjBaseName].scenariosSummary[contract.contractDefinition.kind].contracts[
      contract.contractDefinition.name
    ].functions[functionName].total + scenarios.length;
}
