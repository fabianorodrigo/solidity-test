import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { ContractDefinition } from 'solidity-parser-antlr';
import { getFunctions } from '../solidityParser';
import { getNameTypeName } from './solidity/getNameTypeName';

export * from './javascript';
export * from './solidity';

/**
 * Para as funções com visibilidade 'internal' e com algum parâmetro incluímos a cláusula 'using for' para o tipo do primeiro parâmetro
 * De acordo com esse link: https://itnext.io/libraries-and-using-for-in-solidity-5c954da04128,
 * mesmo se nao for do mesmo tipo, vai atachar a função ao tipo
 *
 * @param {contractDefinition} library A contract of kind 'library' which functions will be analysed
 * @param {object} structsPool A object with the Solidity structs identified in the project
 * @returns {string} Statement 'using Library for Type' in the Solidity syntax
 */
export function getUsingForStatementQuestionavel(
  truffleProject: TruffleProjectIceFactoryFunction,
  library: ContractDefinition
) {
  if (library.kind != 'library') {
    throw new Error('getUsingForStatement has to receive a contractDefinition of kind "library"');
  }

  const functions = getFunctions(library);
  const controleUsingFor = {};
  return functions
    .map((fd) => {
      const keyControleUsing = fd.parameters.length == 0 ? null : getKeyControleUsing(fd.parameters[0].typeName);
      if (fd.parameters.length > 0 && !controleUsingFor[keyControleUsing]) {
        controleUsingFor[keyControleUsing] = true;
        // using x for y
        return `  using ${library.name} for ${getNameTypeName(truffleProject, fd.parameters[0].typeName)};\n`; //TODO: substituir por solidityParsser.getType
      }
    })
    .filter((result) => result != null)
    .join('')
    .concat('\n');
}

function getKeyControleUsing(typeName) {
  if (typeName.name) return typeName.name;
  if (typeName.namePath) return typeName.namePath;
  if (typeName.baseTypeName) return `Array`.concat(getKeyControleUsing(typeName.baseTypeName));
}
