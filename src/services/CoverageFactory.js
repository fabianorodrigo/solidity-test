const fs = require('fs');
const path = require('path');
const colors = require('colors');
const fsextra = require('fs-extra');
const shelljs = require('shelljs');

const { ganacheMneumonic } = require('../services/ganache');

const jsdom = require('jsdom');
const { JSDOM } = jsdom;

const REPORT_FILE_NAME = 'solidity-test-report.json';
const workdir = path.resolve(__dirname, process.env.WORKDIR || '../workdir');
const backupdir = path.resolve(__dirname, process.env.BACKUPDIR || '../backupResults');

const { info, debug, error, warn } = require('./Utils');

/**
 * Retornará uma instância imutável de um objeto com funcionalidades para medição de cobertura de testes
 *
 * @param {string} prjBaseName Base name of Truffle Project under test
 * @returns {object} Objeto com utilidades para medição de cobertura
 */
module.exports = function CoverageIceFactory({ prjBaseName }) {
  if (!prjBaseName) {
    throw new Error(`Truffle Project Home is undefined`);
  }
  const truffleProjectHome = path.resolve(workdir, prjBaseName);
  if (!fs.existsSync(truffleProjectHome)) {
    throw new Error(`Truffle Project does not exist: ${truffleProjectHome}`);
  }
  /*if (!fs.existsSync(path.join(process.env.DEFAULT_SOLIDITY_DIRECTORY, prjBaseName, '.solcover.js'))) {
    writeSolCover(truffleProjectHome); //arquivo de configuração do solidity-coverage
  }*/
  if (global.generalResults == null) {
    global.generalResults = {};
    if (fs.existsSync(path.join(workdir, REPORT_FILE_NAME))) {
      global.generalResults = JSON.parse(fs.readFileSync(path.join(workdir, REPORT_FILE_NAME)));
    }
  }

  return Object.freeze({
    run,
  });

  /**
   * Execute solidity-coverage
   */
  async function run() {
    if (fs.lstatSync(truffleProjectHome).isDirectory()) {
      shelljs.cd(truffleProjectHome);
      let commandLine = '';
      const coverageJSON = path.join(truffleProjectHome, 'coverage.json');
      const coverageDir = path.join(truffleProjectHome, 'coverage');
      fsextra.emptyDirSync(coverageDir);
      fs.rmdir(coverageDir, (err) => {
        if (err) {
          error(`Falha ao apagar ${coverageDir}: ${err.message}`);
        }
      });
      if (fs.existsSync(coverageJSON)) {
        fs.unlink(coverageJSON, (err) => {
          if (err) {
            error(`Falha ao apagar ${coverageJSON}: ${err.message}`);
          }
        });
      }
      try {
        const filePathPackagejson = path.join(truffleProjectHome, 'package.json');
        let packageJSON = null;
        if (fs.existsSync(filePathPackagejson)) {
          packageJSON = JSON.parse(fs.readFileSync(filePathPackagejson));
        }
        if (packageJSON && packageJSON.scripts && packageJSON.scripts.coverage) {
          if (packageJSON.scripts.coverage == 'solidity-coverage') {
            commandLine = 'solidity-coverage';
          } else {
            commandLine = 'npm run coverage';
          }
        } else if (
          packageJSON &&
          packageJSON.dependencies &&
          packageJSON.dependencies['solidity-coverage'] &&
          prjBaseName != 'simple-storage'
          /*
          Esse trecho foi comentado por causa do debug-solidity, que dava
          o seguinte erro quando rodamos o solidity-coverage sem o npx:
          Error: invalid reporter "eth-gas-reporter"
          &&
          !packageJSON.dependencies['solidity-coverage'].startsWith('^0.6') &&
          !packageJSON.dependencies['solidity-coverage'].startsWith('0.6')*/
        ) {
          commandLine = 'npx solidity-coverage';
        } else {
          commandLine = 'solidity-coverage';
        }
      } catch (e) {
        shelljs.touch(path.join(truffleProjectHome, 'failSolidityCoverage'));
        global.generalResults[prjBaseName].coverageSuccessful = false;
        fs.writeFileSync(path.join(workdir, REPORT_FILE_NAME), JSON.stringify(global.generalResults));
        return;
      }
      //vai incluir no início do coverage.out o comando executado, data e hora
      commandLine = `(echo ${commandLine} && date && npx truffle version &&  `.concat(commandLine, ') > coverage.out');
      global.generalResults[prjBaseName].dhInitCoverage = new Date();
      info(`solidity-coverage ...`, commandLine);
      shelljs.exec(commandLine.concat('; fuser -k 8555/tcp'));
      global.generalResults[prjBaseName].coverageSuccessful = true;
      global.generalResults[prjBaseName].dhFinishCoverage = null;
      global.generalResults[prjBaseName].coverage = {};
      if (global.generalResults[prjBaseName].coverageHistory == null) {
        global.generalResults[prjBaseName].coverageHistory = [];
      }
      const indexCoverageFile = path.join(truffleProjectHome, 'coverage', 'index.html');
      if (fs.existsSync(indexCoverageFile)) {
        const data = fs.readFileSync(indexCoverageFile);

        const dom = new JSDOM(data);
        const divSummary = dom.window.document.querySelector('div').querySelector('div').querySelector('div');
        divSummary.childNodes.forEach((child) => {
          if (child.childNodes.length > 0) {
            global.generalResults[prjBaseName].coverage[child.childNodes[3].textContent] = {
              percent: parseFloat(child.childNodes[1].textContent.replace('%', '')),
              total: child.childNodes[5].textContent.split('/')[1],
              covered: child.childNodes[5].textContent.split('/')[0],
            };
          }
        });
        //Só faz o push no histórico se não deu aquele erro de conexão intermitente com o ganache
        if (
          fs.readFileSync(path.join(truffleProjectHome, 'coverage.out')).toString().indexOf('Could not connect') == -1
        ) {
          const cov = global.generalResults[prjBaseName].coverage;
          backupResult(prjBaseName, global.generalResults[prjBaseName].coverageHistory.length || 0);
          global.generalResults[prjBaseName].coverageHistory.push(cov);
          const history = global.generalResults[prjBaseName].coverageHistory;
          info(
            indexCoverageFile,
            `${cov.Branches.covered}/${cov.Branches.total}: ${cov.Branches.percent}%`,
            'Média:',
            (
              (history.map((cov) => parseInt(cov.Branches.covered)).reduce((a, b) => a + b) /
                history.map((cov) => parseInt(cov.Branches.total)).reduce((a, b) => a + b)) *
              100
            )
              .toFixed(2)
              .concat('%')
          );
        } else {
          warn(
            indexCoverageFile,
            'Houve problemas de conexão, histórico não atualizado',
            `${global.generalResults[prjBaseName].coverage.Branches.covered}/${global.generalResults[prjBaseName].coverage.Branches.total}: ${global.generalResults[prjBaseName].coverage.Branches.percent}%`
          );
        }
      } else {
        global.generalResults[prjBaseName].coverageSuccessful = false;
      }
      global.generalResults[prjBaseName].dhFinishCoverage = new Date();
    }
    fs.writeFileSync(path.join(workdir, REPORT_FILE_NAME), JSON.stringify(global.generalResults));
    return {
      coverageSuccessful: global.generalResults[prjBaseName].coverageSuccessful,
      dhFinishCoverage: global.generalResults[prjBaseName].dhFinishCoverage,
      coverage: global.generalResults[prjBaseName].coverage,
    };
  }

  function writeSolCover(target) {
    fs.writeFileSync(
      path.join(target, '.solcover.js'),
      `module.exports = {
 testrpcOptions: '--mnemonic="${ganacheMneumonic}" --port 8555',
 copyPackages: ['openzeppelin-solidity']
};`
    );
  }
};

function backupResult(prjBaseName, i) {
  try {
    const pathbackup = path.resolve(backupdir, prjBaseName, 'history', i.toString());
    if (!fs.existsSync(pathbackup)) {
      fsextra.mkdirsSync(pathbackup);
    } else {
      fsextra.emptyDirSync(pathbackup);
    }
    shelljs.exec(
      `rsync -rvm --include='${workdir}/${prjBaseName}' --exclude='node_modules/*' --exclude='build/*' --exclude='.git/*' ${workdir}/${prjBaseName}/* ${pathbackup}`
    );
  } catch (e) {
    error(e.message);
    throw e;
  }
}
