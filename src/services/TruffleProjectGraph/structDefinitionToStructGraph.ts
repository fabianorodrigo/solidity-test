import { IStructGraph, IContractGraph } from '../../models/graph';
import { getType } from '../solidityParser';
import { IStruct } from '../../models/IStructsPool';
import { isArray } from 'util';
import { ArrayTypeName } from 'solidity-parser-antlr';
/**
 * Creates a instance of IStructGraph based on {struct} passed
 *
 * @param contractGraph Instance of contractGraph in which will be inserted the IStructGraph returned
 * @param struct Struct source of data to construct IStructGraph
 */
export function structDefinitionToStructGraph(contractGraph: IContractGraph, struct: IStruct): IStructGraph {
  const structGraph: IStructGraph = {} as IStructGraph;
  structGraph.name = struct.name;
  structGraph.contract = contractGraph;
  structGraph.parameters = struct.members.map((param) => {
    const type = getType(contractGraph.projectGraph, param.typeName);
    if (type == null) throw new Error(`struct '${struct.name}' param '${param.name}' type is null`);
    return {
      name: param.name,
      isUserDefinedType: param.typeName.type == 'UserDefinedTypeName',
      type,
      isArray: Array.isArray(param.typeName),
      isStruct: contractGraph.projectGraph.structs[getType(contractGraph.projectGraph, param.typeName)] != null, //Se o membro da struct for outro struct, existe uma chance de não ter sido mapeado ainda esse valor ser colocado como FALSE erroneamente
      isEnum:
        contractGraph.projectGraph.enums[type] != null ||
        (isArray(param.typeName) &&
          contractGraph.projectGraph.enums[
            getType(contractGraph.projectGraph, (<ArrayTypeName>param.typeName).baseTypeName)
          ] != null),
      isAddressContract: false,
    };
  });
  return structGraph;
}
