import { IInheritance } from '../../models/graph';

/**
 * Ordena as heranças baseando-se nesta ordem:
 * 1) o nível de herança, herança direta vem antes de herança indireta
 * 2) contratos do projeto vêm antes de contratos de bibliotecas importadas em "node_modules"
 * @param ancA
 * @param ancB
 */
export function sortAncestrais(ancA: IInheritance, ancB: IInheritance) {
  if (ancA.level - ancB.level == 0) {
    if (ancA.contract.isThirdPartyLib > ancB.contract.isThirdPartyLib) return 1;
    else if (ancA.contract.isThirdPartyLib < ancB.contract.isThirdPartyLib) return -1;
  } else {
    return ancA.level - ancB.level;
  }
  return 0;
}
