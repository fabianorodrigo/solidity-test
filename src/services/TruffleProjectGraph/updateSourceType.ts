import {
  FunctionDefinition,
  Identifier,
  MemberAccess,
  IndexAccess,
  NumberLiteral,
  FunctionCall,
} from 'solidity-parser-antlr';
import { SourceVariableSetType, SourceVariableSet } from '../../models/graph';
import {
  isLiteral,
  isAddressFunctionCall,
  isIdentifier,
  isTransactionParameterMemberAccess,
  isIndexAccess,
  isMemberAccess,
} from '../solidityParser/statements';

export function updateSourceType(funcDef: FunctionDefinition, statement, source: SourceVariableSet): void {
  if (isIdentifier(statement) && funcDef.parameters.findIndex((p) => p.name == (<Identifier>statement).name) > -1) {
    source.type = SourceVariableSetType.parameter;
  } else if (isTransactionParameterMemberAccess(statement)) {
    source.type = SourceVariableSetType.transactionParameter;
    source.aditionalInfo = (<MemberAccess>statement).memberName;
  } else if (isMemberAccess(statement) && funcDef.parameters.findIndex((p) => p.name == source.name) > -1) {
    source.type = SourceVariableSetType.parameterMemberAccess;
    source.aditionalInfo = (<MemberAccess>statement).memberName;
  } else if (isIndexAccess(statement) && funcDef.parameters.findIndex((p) => p.name == source.name) > -1) {
    source.type = SourceVariableSetType.parameterIndexAccess;
    source.aditionalInfo = (<IndexAccess>statement).index.toString();
  } else if (isLiteral(statement)) {
    source.type = SourceVariableSetType.literal;
    source.aditionalInfo = statement.hasOwnProperty('number')
      ? (statement as NumberLiteral).number
      : (statement as any).value;
  } else if (isAddressFunctionCall(statement)) {
    source.type = SourceVariableSetType.literal;
    source.aditionalInfo =
      ((statement as FunctionCall).arguments[0] as NumberLiteral).number == '0'
        ? '0x0000000000000000000000000000000000000000'
        : ((statement as FunctionCall).arguments[0] as NumberLiteral).number;
  }
}
