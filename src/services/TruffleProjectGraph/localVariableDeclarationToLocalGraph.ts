import { IFunctionGraph, ILocalVariableGraph } from '../../models/graph';
import { VariableDeclaration, VariableDeclarationStatement } from 'solidity-parser-antlr';
import { getRestrictionValue, getType, isArray } from '../solidityParser';
import { getLiteralValue } from './getLiteralValue';

import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
/**
 * Creates a instance of ILocalVariableGraph based on {localVar} passed
 *
 * @param functionGraph Instance of functionGraph that the local variable belongs
 * @param localVar VariableDeclaration source of data to construct ILocalVariable
 */
export function localVariableDeclarationToLocalGraph(
  truffleProject: TruffleProjectIceFactoryFunction,
  functionGraph: IFunctionGraph,
  variableDeclarationStatement: VariableDeclarationStatement,
  localVar: VariableDeclaration
): ILocalVariableGraph {
  const localGraph: ILocalVariableGraph = {
    name: localVar.name,
    functionSignature: functionGraph.signature,
    type: getType(functionGraph.contract.projectGraph, localVar.typeName),
    isArray: isArray(localVar.typeName),
    initialRestrictionValue:
      variableDeclarationStatement.initialValue != null
        ? getRestrictionValue(truffleProject, functionGraph, variableDeclarationStatement.initialValue)
        : null,
    isConstant: localVar.isDeclaredConst,
    isAddressContract: false,
  };
  return localGraph;
}
