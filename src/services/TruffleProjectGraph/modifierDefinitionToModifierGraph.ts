import { IContractGraph, IModifierGraph } from '../../models/graph';
import { getType } from '../solidityParser';
/**
 * Creates a instance of IModifierGraph based on {modDef} passed
 *
 * @param contractGraph Instance of contractGraph in which will be inserted the IModifierGraph returned
 * @param modDef modifierDefinition source of data to construct IModifierGraph
 */
export function modifierDefinitionToModifierGraph(
  contractGraph: IContractGraph,
  modDef: any //A definição de ModifierDefinition não está apresentando os atributos: body e parameters
): IModifierGraph {
  const modifierGraph: IModifierGraph = {} as IModifierGraph;
  modifierGraph.name = modDef.name;
  modifierGraph.functionDefinition = modDef;
  modifierGraph.contract = contractGraph;
  modifierGraph.parameters = modDef.parameters
    ? modDef.parameters.map((param) => {
        const type = getType(contractGraph.projectGraph, param);
        if (type == null) throw new Error(`modifier '${modDef.name}' param '${param.name}' type is null`);
        return {
          name: param.name,
          type,
        };
      })
    : [];
  return modifierGraph;
}
