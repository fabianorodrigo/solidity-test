import { FunctionDefinition } from 'solidity-parser-antlr';
import { IContractGraph, IFunctionGraph, IStructGraph, ITruffleProjectGraph } from '../../models/graph';
import { IContract } from '../../models/IContract';
import { isAbstractContract } from '../solidityParser';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { enumDefinitionToEnumGraph } from './enumDefinitionToEnumGraph';
import { functionDefinitionToFunctionGraph } from './functionDefinitionToFunctionGraph';
import { structDefinitionToStructGraph } from './structDefinitionToStructGraph';

/**
 * Adiciona o função no grafo
 *
 * @param contract
 */
export function appendFunctionGraph(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractGraph: IContractGraph,
  functionDefinition: FunctionDefinition
): IFunctionGraph {
  let functionGraph: IFunctionGraph = functionDefinitionToFunctionGraph(
    truffleProject,
    contractGraph,
    functionDefinition
  );
  //Se a função tiver nome de um elemento que seja herdade de Object, por exemplo, 'toString',
  //Ela não entrará neste IF e ao chamar 'push' no ELSE, lança exceção.
  //Este OR isArray foi pra tentar contornar isso ...
  if (!contractGraph.functions[functionGraph.name] || !Array.isArray(contractGraph.functions[functionGraph.name])) {
    contractGraph.functions[functionGraph.name] = [functionGraph];
  } else {
    contractGraph.functions[functionGraph.name].push(functionGraph);
  }
  return functionGraph;
}
