import { contractList, ITruffleProjectGraph } from '../../models/graph';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { IContract } from '../../models/IContract';

export function getDependenciesImported(
  truffleProject: TruffleProjectIceFactoryFunction,
  contract: IContract,
  graph: ITruffleProjectGraph
): contractList {
  let retorno: contractList = {};
  //libraries imported for contract
  truffleProject.getDependencies(contract).forEach((lib) => {
    retorno[lib.contractDefinition.name] = graph.contracts[lib.contractDefinition.name];
  });
  return retorno;
}
