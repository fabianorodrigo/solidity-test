import { IContractGraph, IFunctionGraph, IModifierGraph } from '../../models/graph';
import { FunctionDefinition, ArrayTypeName } from 'solidity-parser-antlr';
import { getContractConstructor, getType, isArray } from '../solidityParser';
import getFunctionSignature from './getFunctionSignature';
import { isPayable, warn } from '../Utils';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { getFunctionName } from './getFunctionName';

/**
 * Creates a instance of IFunctionGraph based on {funcDef} passed
 *
 * @param contractGraph Instance of contractGraph in which will be inserted the IFunctionGraph returned
 * @param funcDef FunctionDefinition source of data to construct IFunctionGraph
 */
export function functionDefinitionToFunctionGraph(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractGraph: IContractGraph,
  funcDef: FunctionDefinition
): IFunctionGraph {
  const functionGraph: IFunctionGraph = {} as IFunctionGraph;
  functionGraph.name = getFunctionName(funcDef);
  functionGraph.signature = getFunctionSignature(contractGraph.projectGraph, funcDef, functionGraph.name);
  functionGraph.isPayable = isPayable(funcDef);
  functionGraph.visibility = funcDef.visibility;
  functionGraph.contract = contractGraph;
  functionGraph.isConstructor = funcDef.isConstructor;
  functionGraph.functionDefinition = funcDef;
  functionGraph.localVariables = [];
  functionGraph.modifiers = {};

  funcDef.modifiers.forEach((m) => {
    let modGraph: IModifierGraph | IFunctionGraph = null;
    if (contractGraph.modifiers[m.name] != null) {
      modGraph = contractGraph.modifiers[m.name];
    } else if (contractGraph.inheritsFrom[m.name] != null) {
      //Neste ponto ainda não foram carregadas as funções do contrato, por isso não é possível pegar
      //dados de IFunctionGraph direto do contractGraph pois está vazio
      /*const fg: IFunctionGraph = contractGraph.inheritsFrom[m.name].contract.functions[functionGraph.name].find(
            (f) => f.signature == functionGraph.signature
          );*/
      const cg = contractGraph.inheritsFrom[m.name].contract;
      const constructor = getContractConstructor(cg.contractDefinition, true);
      if (constructor == null) {
        warn(`${contractGraph.name}: Modificador/Construtor não localizado`, m.name);
      } else {
        modGraph = functionDefinitionToFunctionGraph(truffleProject, cg, constructor);
      }
    } else {
      warn(`${contractGraph.name}.${funcDef.name}: Modificador não localizado`, m.name);
    }
    functionGraph.modifiers[m.name] = modGraph;
  });
  functionGraph.return = !funcDef.returnParameters
    ? []
    : funcDef.returnParameters.map((param) => {
        const type = getType(contractGraph.projectGraph, param.typeName);
        if (type == null) throw new Error(`function '${funcDef.name}' return param '${param.name}' type is null`);
        return {
          name: param.name,
          type,
          isUserDefinedType: param.typeName.type == 'UserDefinedTypeName',
          isArray: isArray(param.typeName),
          isStruct:
            contractGraph.projectGraph.structs[type] != null ||
            (isArray(param.typeName) &&
              contractGraph.projectGraph.structs[
                getType(contractGraph.projectGraph, (<ArrayTypeName>param.typeName).baseTypeName)
              ] != null),
          isEnum:
            contractGraph.projectGraph.enums[type] != null ||
            (isArray(param.typeName) &&
              contractGraph.projectGraph.enums[
                getType(contractGraph.projectGraph, (<ArrayTypeName>param.typeName).baseTypeName)
              ] != null),
          isAddressContract: false,
        };
      });
  functionGraph.parameters = funcDef.parameters.map((param) => {
    const type = getType(contractGraph.projectGraph, param.typeName);
    if (type == null) throw new Error(`function '${funcDef.name}' param '${param.name}' type is null`);
    return {
      name: param.name,
      type,
      isUserDefinedType: param.typeName.type == 'UserDefinedTypeName',
      isArray: isArray(param.typeName),
      isStruct:
        contractGraph.projectGraph.structs[type] != null ||
        (isArray(param.typeName) &&
          contractGraph.projectGraph.structs[
            getType(contractGraph.projectGraph, (<ArrayTypeName>param.typeName).baseTypeName)
          ] != null),
      isEnum:
        contractGraph.projectGraph.enums[type] != null ||
        (isArray(param.typeName) &&
          contractGraph.projectGraph.enums[
            getType(contractGraph.projectGraph, (<ArrayTypeName>param.typeName).baseTypeName)
          ] != null),
      isAddressContract: false,
    };
  });
  functionGraph.stateVariablesWritten = {};
  functionGraph.calledFunctions = {};
  functionGraph.functionCallings = {};
  return functionGraph;
}
