import { ISearch, TypeSearchResultList } from '../../models';
import { IContractGraph, IFunctionGraph, IModifierGraph } from '../../models/graph';
import { searchFunctionCallReferencingNames, searchThroughoutStatements } from '../solidityParser/searcher';
import { searchFunctionCallReferencingNamesIndexMember } from '../solidityParser/searcher/searchFunctions/searchFunctionCallReferencingNamesIndexMember';
import { searchVariableSimpleSet } from '../solidityParser/searcher/searchFunctions/searchVariableSimpleSet';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';

export function getContractAddressStateVariableReference(
  truffleProject: TruffleProjectIceFactoryFunction,
  contract: IContractGraph
): { [stateVariableName: string]: IContractGraph[] } {
  let result: { [stateVariableName: string]: IContractGraph[] } = {};

  const stateVariablesAddress = Object.values(contract.stateVariables)
    .filter((p) => p.type == 'address')
    .map((p) => p.name);
  const stateVariablesAddressArray = Object.values(contract.stateVariables)
    .filter((p) => p.isArray == true && p.type == 'address[]')
    .map((p) => p.name);

  const searchName = 'funcoesQueRecebemStateVariableAddressComoUnicoParametro';

  if (stateVariablesAddress.length > 0 || stateVariablesAddressArray.length > 0) {
    //percorre as funções
    Object.values(contract.functions).forEach((array) => {
      array.forEach((functionGraph) => {
        //configuração da busca de funções que recebem como único parâmetro uma variável de estado do tipo address
        const searchResult = searchFCRN(
          truffleProject,
          contract,
          functionGraph,
          searchName,
          stateVariablesAddress,
          stateVariablesAddressArray
        );
        //transfere resultado da busca pelas funções dentro da função analisada que recebem as state variables
        //address para o resultado final
        transferSearchResultToFinalResult(contract, searchName, searchResult, result);
      });
    });

    //percorre os modificadores
    Object.values(contract.modifiers).forEach((modifierGraph) => {
      //configuração da busca de funções que recebem como único parâmetro uma variável de estado do tipo address
      const searchResult = searchFCRN(
        truffleProject,
        contract,
        modifierGraph,
        searchName,
        stateVariablesAddress,
        stateVariablesAddressArray
      );
      //transfere resultado da busca pelas funções dentro do modificador que recebem as state variables address para o resultado final
      transferSearchResultToFinalResult(contract, searchName, searchResult, result);
    });
  }
  return result;
}

function transferSearchResultToFinalResult(
  contract: IContractGraph,
  searchName: string,
  searchResult: any,
  result: { [stateVariableName: string]: IContractGraph[] }
): void {
  if (searchResult[searchName] != null && searchResult[searchName].length > 0) {
    searchResult[searchName].forEach((r) => {
      //Se o nome da função for exatamente o nome de um contrato,
      //trata-se de uma instância do tipo do contrato a partir de um endereço
      if (contract.projectGraph.contracts[r.functionName] != null) {
        if (result[r.name] == null) result[r.name] = [];
        //só inclui se já não existir para o parâmetro
        if (result[r.name].find((c) => Object.is(c, contract.projectGraph.contracts[r.functionName])) == null) {
          result[r.name].push(contract.projectGraph.contracts[r.functionName]);
        }
      }
    });
  }
}

function searchFCRN(
  truffleProject: TruffleProjectIceFactoryFunction,
  contract: IContractGraph,
  functionOrModifierGraph: IFunctionGraph | IModifierGraph,
  searchName: string,
  names: string[],
  namesArray: string[]
) {
  const searchAddress: ISearch = {
    name: searchName,
    searchFunction: searchFunctionCallReferencingNames,
    functionParametersNames: names,
    args: {
      parameterCount: 1,
    },
  };
  const searchAddressArray: ISearch = {
    name: searchName,
    searchFunction: searchFunctionCallReferencingNamesIndexMember,
    functionParametersNames: namesArray,
    args: {
      parameterCount: 1,
    },
  };
  const searchResult = searchThroughoutStatements(
    truffleProject,
    functionOrModifierGraph,
    functionOrModifierGraph.functionDefinition,
    [searchAddress, searchAddressArray]
  );
  return searchResult;
}

export function getContractAddressParameterReference(
  truffleProject: TruffleProjectIceFactoryFunction,
  contract: IContractGraph,
  functionORmodifierGraph: IFunctionGraph | IModifierGraph
): { [paramName: string]: IContractGraph[] } {
  let result: { [paramName: string]: IContractGraph[] } = {};

  const searchs: ISearch[] = [];
  //configuração da busca de funções que recebem como única entrada um parâmetro da função do tipo address
  const searchName = 'funcoesQueRecebemParametrosAddressComoUnicoParametro';
  //address
  const functionParametersAddress = Object.values(functionORmodifierGraph.parameters)
    .filter((p) => p.type == 'address')
    .map((p) => p.name);
  if (functionParametersAddress.length > 0) {
    const search: ISearch = {
      name: searchName,
      searchFunction: searchFunctionCallReferencingNames,
      functionParametersNames: functionParametersAddress,
      args: {
        parameterCount: 1,
      },
    };
    searchs.push(search);
  }
  //array de address
  const functionParametersAddressArray = Object.values(functionORmodifierGraph.parameters)
    .filter((p) => p.isArray == true && p.type == 'address[]')
    .map((p) => p.name);
  if (functionParametersAddressArray.length > 0) {
    const search: ISearch = {
      name: searchName,
      searchFunction: searchFunctionCallReferencingNamesIndexMember,
      functionParametersNames: functionParametersAddressArray,
      args: {
        parameterCount: 1,
      },
    };
    searchs.push(search);
  }

  //configuração da busca de variáveis de estado que já se sabe que são endereços de contrato
  //e a qual é atribuído o valor vindo de um parâmetro de função
  const search2Name = 'variaveisDeEstadoSetadas';
  const stateVariablesAddressContract = Object.values(contract.stateVariables)
    .filter((sv) => sv.isAddressContract == true)
    .map((sv) => sv.name);
  if (stateVariablesAddressContract.length > 0) {
    const search2: ISearch = {
      name: search2Name,
      searchFunction: searchVariableSimpleSet,
      functionParametersNames: functionParametersAddress.concat(functionParametersAddressArray),
      args: {
        variableNames: stateVariablesAddressContract,
      },
    };
    searchs.push(search2);
  }

  let searchResult: TypeSearchResultList = null;
  if (searchs.length > 0) {
    searchResult = searchThroughoutStatements(
      truffleProject,
      functionORmodifierGraph,
      functionORmodifierGraph.functionDefinition,
      searchs
    );
    //transfere resultado da busca pelas funções dentro da função analisada que recebem as state variables
    //address para o resultado final
    transferSearchResultToFinalResult(contract, searchName, searchResult, result);
    //resultado da busca pelas funções que recebem os variáveis de estadoaddress
    //A lógica é que para as variáveis de estados que são address e
    //já se identificou que são do endereços de contrato (se houver), verifica-se se
    //a função em análise atribui o parâmetro com nome {name} a esta variável de estado
    //SE for o caso, o parâmetro também será setado como um endereço de contrato
    if (searchResult[search2Name] != null && searchResult[search2Name].length > 0) {
      //o que retornar, é por que houve atribuição de um parâmetro a uma variável que já se sabe que é endereço de contrato
      searchResult[search2Name].forEach((r) => {
        if (result[r.paramName] == null) result[r.paramName] = [];
        contract.stateVariables[r.stateVariableName].contractAddressOf.forEach((cao) => {
          //só inclui se já não existir para o parâmetro
          if (result[r.paramName].find((c) => Object.is(c, cao)) == null) {
            result[r.paramName].push(cao);
          }
        });
      });
    }
  }
  return result;
}
