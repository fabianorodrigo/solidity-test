import { FunctionDefinition } from 'solidity-parser-antlr';

export function getFunctionName(functionDefinition: FunctionDefinition): string {
  return functionDefinition.isConstructor ? 'constructor' : functionDefinition.name;
}
