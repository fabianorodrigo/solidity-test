import { IContractGraph, IStructGraph, ITruffleProjectGraph } from '../../models/graph';
import { IContract } from '../../models/IContract';
import { isAbstractContract } from '../solidityParser';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { enumDefinitionToEnumGraph } from './enumDefinitionToEnumGraph';
import { structDefinitionToStructGraph } from './structDefinitionToStructGraph';

/**
 * Adiciona o contrato no grafo
 *
 * @param contract
 */
export function appendContract(
  truffleProject: TruffleProjectIceFactoryFunction,
  graph: ITruffleProjectGraph,
  contract: IContract
): IContractGraph {
  //Se já existir um IContractGraph com o mesmo nome e o corrente for de biblioteca externa, retorna
  if (graph.contracts[contract.contractDefinition.name] != null && contract.isThirdPartyLib) return;

  let contractGraph: IContractGraph = {
    projectGraph: graph,
    name: contract.contractDefinition.name,
    kind: contract.contractDefinition.kind,
    isAbstract: isAbstractContract(contract.contractDefinition),
    functions: {},
    modifiers: {},
    stateVariables: {},
    dependencies: {},
    inheritsFrom: {},
    structs: {},
    enums: {},
    isThirdPartyLib: contract.isThirdPartyLib,
    isOnlyForTest: contract.isOnlyForTest,
    contractDefinition: contract.contractDefinition,
    instanceNeeded: false,
    solFilePath: contract.solFilePath,
  };
  contractGraph.instanceNeeded = !contractGraph.isAbstract && !contractGraph.isThirdPartyLib; //for default, instancia todos e somente os contratos do projeto

  //structs definition of contract
  Object.values(truffleProject.getStructs())
    .filter((s) => s.fullName === false && s.contractName == contract.contractDefinition.name)
    .forEach((struct) => {
      let structGraph: IStructGraph = structDefinitionToStructGraph(contractGraph, struct);
      contractGraph.structs[structGraph.name] = structGraph;
      //No grafo, vamos indexar as structs tanto pelo nome simples quanto pelo composto (contrato.struct)
      //abarcando, assim, cenários em que haja structs com mesmo nome em contratos distintos
      graph.structs[structGraph.name] = structGraph;
      graph.structs[`${structGraph.contract.name}.${structGraph.name}`] = structGraph;
    });

  //enums definition of contract
  const contractEnumPool = truffleProject.getEnums()[contract.contractDefinition.name];
  if (contractEnumPool) {
    Object.keys(contractEnumPool).forEach((enumName) => {
      let enumGraph = enumDefinitionToEnumGraph(contractGraph, contractEnumPool, enumName);
      contractGraph.enums[enumGraph.name] = enumGraph;
      //No grafo, vamos indexar as enums tanto pelo nome simples quanto pelo composto (contrato.enum)
      //abarcando, assim, cenários em que haja structs com mesmo nome em contratos distintos
      graph.enums[enumGraph.name] = enumGraph;
      graph.enums[`${enumGraph.contract.name}.${enumGraph.name}`] = enumGraph;
    });
  }
  graph.contracts[contractGraph.name] = contractGraph;
  return contractGraph;
}
