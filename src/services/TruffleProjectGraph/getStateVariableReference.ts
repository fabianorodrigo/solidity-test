import { IStateVariableGraph, IContractGraph } from '../../models/graph';

/**
 * Busca no grafo do contrato a referência para a variável de estado com o nome {stateVariableName}
 * Não encontrando, buscará em seus ancestrais
 *
 * @param contractGraph grafo do contrato que será analisado
 * @param stateVariableName nome da variável de estado que se deseja obter
 */
export function getStateVariableReference(
  contractGraph: IContractGraph,
  stateVariableName: string
): IStateVariableGraph {
  //primeiro tenta encontrar no próprio contrato
  let retorno: IStateVariableGraph = contractGraph.stateVariables[stateVariableName];
  if (retorno) return retorno;

  //depois tenta cada ancestral
  const ancestrais = Object.values(contractGraph.inheritsFrom);

  for (let i = 0; i < ancestrais.length; i++) {
    retorno = getStateVariableReference(ancestrais[i].contract, stateVariableName);
    if (retorno) return retorno;
  }
  return retorno;
}
