import { contractList, ITruffleProjectGraph, IContractGraph, inheritanceList } from '../../models/graph';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { IContract } from '../../models/IContract';
import { ContractDefinition } from 'solidity-parser-antlr';

export function getContractPredecessors(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractGraph: IContractGraph
): inheritanceList {
  let retorno: inheritanceList = {};
  //contract and interfaces implementeds/inheriteds by {contract}
  const contractMap = {};
  Object.values(contractGraph.projectGraph.contracts).forEach((c) => {
    contractMap[c.name] = c.contractDefinition;
  });

  getRecursiveInheritance(contractMap, contractMap[contractGraph.name]).forEach((inheritance) => {
    if (contractGraph.projectGraph.contracts[inheritance.contractName] != null) {
      retorno[inheritance.contractName] = {
        contract: contractGraph.projectGraph.contracts[inheritance.contractName],
        parameters: inheritance.arguments,
        level: inheritance.level,
      };
    }
  });
  return retorno;
}

function getRecursiveInheritance(
  contractMap: { [contractName: string]: ContractDefinition },
  contractLeaf: ContractDefinition,
  level = 0
): { contractName: string; arguments: any[]; level: number }[] {
  let result: { contractName: string; arguments: any[]; level: number }[] = [];
  for (let i = 0; i < contractLeaf.baseContracts.length; i++) {
    if (!result.find((c) => c.contractName == contractLeaf.baseContracts[i].baseName.namePath)) {
      result.push({
        contractName: contractLeaf.baseContracts[i].baseName.namePath,
        arguments: contractLeaf.baseContracts[i].arguments,
        level: level,
      });
    }
    if (contractMap[contractLeaf.baseContracts[i].baseName.namePath]) {
      result = result.concat(
        getRecursiveInheritance(contractMap, contractMap[contractLeaf.baseContracts[i].baseName.namePath], level + 1)
      );
    }
  }
  return result;
}
