import { IContractGraph, IFunctionGraph, IModifierGraph } from '../../models/graph';

export function getModifierGraph(contractGraph: IContractGraph, modifierName: string): IModifierGraph | IFunctionGraph {
  if (contractGraph.modifiers[modifierName] != null) {
    return contractGraph.modifiers[modifierName];
  } else {
    const ancestrais = Object.values(contractGraph.inheritsFrom);
    for (let i = 0; i < ancestrais.length; i++) {
      const subResult = getModifierGraph(ancestrais[i].contract, modifierName);
      if (subResult != null) {
        return subResult;
      }
    }
  }
  return null;
}
