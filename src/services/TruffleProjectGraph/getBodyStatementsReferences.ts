import { SourceVariableSet, IContractGraph, StateVariableSetOperator } from '../../models/graph';
import { SourceVariableSetType } from '../../models/graph/SourceVariableSetType';
import {
  FunctionDefinition,
  BinaryOperation,
  Identifier,
  IfStatement,
  Block,
  ContractDefinition,
  ForStatement,
} from 'solidity-parser-antlr';
import {
  isFunctionCall,
  isIf,
  isFor,
  isVariableDeclaration,
  isIdentifier,
  isBOExpressionStatement,
  isRequireCall,
  isIfWithRevert,
} from '../solidityParser';
import { IFunctionReferences } from '../../models/graph/IFunctionReferences';
import getFunctionSignature from './getFunctionSignature';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { debug } from '../Utils';
import {
  isAnotherContractFunctionCall,
  isSuperFunctionCall,
  getContractStateVariableReferenceGraph,
  isPushArray,
} from '../solidityParser/statements';
import { updateSourceType } from './updateSourceType';
import { localVariableDeclarationToLocalGraph } from './localVariableDeclarationToLocalGraph';
import { getFunctionName } from './getFunctionName';

export function getBodyStatementsReferences(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractGraph: IContractGraph,
  contractDefinition: ContractDefinition,
  funcDef: FunctionDefinition,
  body,
  localVariablesParam?: string[]
): IFunctionReferences {
  let hasRequire = false; //Se tiver algum require antes da chamada, a atribuição é condicional
  const funcGraph = contractGraph.functions[getFunctionName(funcDef)].find(
    (f) => f.signature == getFunctionSignature(contractGraph.projectGraph, funcDef, getFunctionName(funcDef))
  );
  if (funcGraph == null) {
    throw new Error(`FunctionGraph não encontrada: ${contractGraph.name}.${funcDef.name}`);
  }
  let retorno: IFunctionReferences = {
    stateVariables: [],
    functionsCalled: [],
    functionCallings: [],
    name: funcGraph.name,
    signature: funcGraph.signature,
  } as IFunctionReferences;
  let localVariables: string[] = localVariablesParam == null ? [] : localVariablesParam;
  for (let i = 0; i < body.statements.length; i++) {
    let st = body.statements[i];
    if (isVariableDeclaration(st)) {
      st.variables
        .filter((v) => v != null && v.isStateVar == false)
        .forEach((v) => {
          funcGraph.localVariables.push(localVariableDeclarationToLocalGraph(truffleProject, funcGraph, st, v));
          localVariables.push(v.name);
        });
    } else if (isRequireCall(st) || isIfWithRevert(st)) {
      hasRequire = true;
    } else if (isFunctionCall(st) || isSuperFunctionCall(st)) {
      const isSuper = isSuperFunctionCall(st);
      const fName = isSuper ? st.expression.expression.memberName : st.expression.expression.name;
      const functionContractGraph =
        !isSuper &&
        contractGraph.functions.hasOwnProperty(fName) &&
        contractGraph.functions[fName].find((f) => f.parameters.length == st.expression.arguments.length) != null
          ? contractGraph
          : Object.values(contractGraph.inheritsFrom).find((cg) =>
              Object.values(cg.contract.functions).find((array) =>
                array.some((fg) => fg.name == fName && fg.parameters.length == 1)
              )
            )?.contract;
      //somente se já não existir em 'retorno'
      //TODO: Tem que considerar a assinatura e não o nome somente
      if (
        functionContractGraph &&
        !retorno.functionsCalled.find((f) => f.name == fName && f.parent == functionContractGraph.name)
      ) {
        retorno.functionsCalled.push({
          name: fName,
          conditional: false,
          signature: '',
          parent: functionContractGraph.name,
        });
        const functionCalled = functionContractGraph.functions[fName]?.find(
          (f) => f.parameters.length == st.expression.arguments.length
        );
        if (functionCalled != null) {
          retorno.functionsCalled[retorno.functionsCalled.length - 1].signature = functionCalled.signature;
          hasRequire = hasRequireIn(truffleProject, contractDefinition, functionCalled.functionDefinition);
        } else if (!['require', 'assert', 'revert', 'selfdestruct'].includes((<any>st).expression.expression.name)) {
          debug(`functionCalled not found: ${contractDefinition.name} ${(<any>st).expression.expression.name}`);
        }
        //FUNCTION CALLING
        retorno.functionCallings.push({
          name: retorno.functionsCalled[retorno.functionsCalled.length - 1].name,
          conditional: retorno.functionsCalled[retorno.functionsCalled.length - 1].conditional,
          signature: retorno.functionsCalled[retorno.functionsCalled.length - 1].signature,
          parent: functionContractGraph.name,
          parameters: st.expression.arguments,
        });
      }
    } else if (isAnotherContractFunctionCall(contractGraph.projectGraph, st)) {
      const anotherContractName = st.expression.expression.expression.name;
      const anotherFunction = st.expression.expression.memberName;
      //TODO: Tem que considerar a assinatura e não o nome somente
      if (!retorno.functionsCalled.find((f) => f.name == anotherFunction && f.parent == anotherContractName)) {
        retorno.functionsCalled.push({
          name: anotherFunction,
          conditional: false,
          signature: '',
          parent: anotherContractName,
        });
        if (contractGraph.projectGraph.contracts[anotherContractName]) {
          const functionCalled = contractGraph.projectGraph.contracts[anotherContractName].functions[
            anotherFunction
          ].find((f) => f.parameters.length == st.expression.arguments.length);
          if (functionCalled != null) {
            retorno.functionsCalled[retorno.functionsCalled.length - 1].signature = functionCalled.signature;
            hasRequire = hasRequireIn(truffleProject, contractDefinition, functionCalled.functionDefinition);
          } else if (!['require', 'assert', 'revert', 'selfdestruct'].includes((<any>st).expression.expression.name)) {
            debug(`functionCalled not found: ${contractDefinition.name} ${(<any>st).expression.expression.name}`);
          }
          //FUNCTION CALLING
          retorno.functionCallings.push({
            name: retorno.functionsCalled[retorno.functionsCalled.length - 1].name,
            conditional: retorno.functionsCalled[retorno.functionsCalled.length - 1].conditional,
            signature: retorno.functionsCalled[retorno.functionsCalled.length - 1].signature,
            parent: retorno.functionsCalled[retorno.functionsCalled.length - 1].parent,
            parameters: st.expression.arguments,
          });
        } else {
          debug(`contract memberCalled not found: ${anotherContractName} ${anotherFunction}`);
        }
      }
    } else if (isIf(st)) {
      let bodyIf = getIfBody(st);
      copyReferencesFromSubbody(
        truffleProject,
        contractGraph,
        contractDefinition,
        funcDef,
        bodyIf,
        localVariables,
        retorno
      );
      if (st.falseBody) {
        let bodyElse = getIfBody(st.falseBody);
        if (bodyElse == null) {
          bodyElse = st.falseBody;
        }
        copyReferencesFromSubbody(
          truffleProject,
          contractGraph,
          contractDefinition,
          funcDef,
          bodyElse,
          localVariables,
          retorno
        );
      }
    } else if (isFor(st) && st.body && st.body.statements) {
      const forReference = getBodyStatementsReferences(
        truffleProject,
        contractGraph,
        contractDefinition,
        funcDef,
        (<ForStatement>st).body,
        localVariables
      );
      retorno.stateVariables = retorno.stateVariables.concat(forReference.stateVariables);
      retorno.functionsCalled = retorno.functionsCalled.concat(forReference.functionsCalled);
    } else if (isBOExpressionStatement(st) && st.expression.operator == '=') {
      //Se for variável local, ignora. Do contrário, inclui na lista de state variables
      const bo: BinaryOperation = st.expression;
      //somente se já não existir em 'retorno'
      if (!retorno.stateVariables.find((sv) => sv.name == (<Identifier>bo.left).name)) {
        if (
          isIdentifier(bo.left) &&
          bo.operator == '=' &&
          !localVariables.includes((<Identifier>bo.left).name) &&
          //contractGraph.stateVariables[(<Identifier>bo.left).name] != null
          getContractStateVariableReferenceGraph(truffleProject, contractGraph, (<Identifier>bo.left).name) != null
        ) {
          let source: SourceVariableSet = {
            type: SourceVariableSetType.others,
            name: (<any>bo.right).base
              ? (<any>bo.right).base.name
              : (<any>bo.right).name
              ? (<any>bo.right).name
              : (<any>bo.right).expression && (<any>bo.right).expression.name
              ? (<any>bo.right).expression.name
              : '',
            aditionalInfo: bo.right.type,
          };
          updateSourceType(funcDef, bo.right, source);

          retorno.stateVariables.push({
            contractName: getContractStateVariableReferenceGraph(
              truffleProject,
              contractGraph,
              (<Identifier>bo.left).name
            ).name,
            name: (<Identifier>bo.left).name,
            conditional: hasRequire,
            operator: StateVariableSetOperator.simpleSet,
            source: source,
          });
        }
      }
    } else if (isPushArray(st)) {
      const svArrayName = (<Identifier>st.expression.expression.expression).name;
      const pushParam = st.expression.arguments[0];
      //somente se já não existir em 'retorno'
      if (!retorno.stateVariables.find((sv) => sv.name == svArrayName)) {
        const contractDeclarantStateVariable = getContractStateVariableReferenceGraph(
          truffleProject,
          contractGraph,
          svArrayName
        );
        if (
          !localVariables.includes((<Identifier>st.expression.expression.expression).name) &&
          //contractGraph.stateVariables[(<Identifier>bo.left).name] != null
          contractDeclarantStateVariable != null &&
          contractDeclarantStateVariable.stateVariables[svArrayName].isArray
        ) {
          let source: SourceVariableSet = {
            type: SourceVariableSetType.others,
            name: pushParam.base
              ? pushParam.base.name
              : pushParam.name
              ? pushParam.name
              : pushParam.expression && pushParam.expression.name
              ? pushParam.expression.name
              : '',
            aditionalInfo: pushParam.type,
          };
          updateSourceType(funcDef, pushParam, source);

          retorno.stateVariables.push({
            contractName: getContractStateVariableReferenceGraph(truffleProject, contractGraph, svArrayName).name,
            name: svArrayName,
            conditional: hasRequire,
            operator: StateVariableSetOperator.arrayPush,
            source: source,
          });
        }
      }
    } else {
      //console.log(JSON.stringify(st));
    }
  }
  return retorno;
}

function getIfBody(st: IfStatement) {
  if (st.trueBody) {
    return st.trueBody;
  } else if (st.falseBody && isIf(st.falseBody)) {
    return getIfBody(<IfStatement>st.falseBody);
  }
}

/**
 * Identify references to state variables and functions called in the {subBody} and copy them to {target}
 * @param funcDef Function in analysis
 * @param subBody Body of IF, ELSEIF or ELSE in the function analysed
 * @param localVariables Local variables identified in the {funcDef} already
 * @param target Instance of IFunctionReferences to where the references found in the subBody will be copied to
 */
function copyReferencesFromSubbody(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractGraph: IContractGraph,
  contractDefinition: ContractDefinition,
  funcDef: FunctionDefinition,
  subBody: Block,
  localVariables: string[],
  target: IFunctionReferences
): void {
  if (subBody != null && subBody.statements != null) {
    const conditionalReference = getBodyStatementsReferences(
      truffleProject,
      contractGraph,
      contractDefinition,
      funcDef,
      subBody,
      localVariables
    );
    conditionalReference.stateVariables.forEach((stVar) => {
      //somente se já não existir em 'retorno'
      if (!target.stateVariables.find((sv) => sv.name == stVar.name)) {
        stVar.conditional = true;
        target.stateVariables.push(stVar);
      }
    });
    conditionalReference.functionsCalled.forEach((funcs) => {
      //somente se já não existir em 'retorno'
      if (!target.functionsCalled.find((f) => f.name == funcs.name)) {
        funcs.conditional = true;
        target.functionsCalled.push(funcs);
      }
    });
  }
}

function hasRequireIn(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractDefinition: ContractDefinition,
  funcDef: FunctionDefinition
): boolean {
  for (let i = 0; i < funcDef.body?.statements.length; i++) {
    let st = funcDef.body.statements[i];
    if (isRequireCall(st) || isIfWithRevert(st)) {
      return true;
    } else if (isFunctionCall(st)) {
      const { contract, funcDef: functionCalled } = truffleProject.getFunctionFromContractDefinition(
        contractDefinition,
        (<any>st).expression.expression.name,
        (<any>st).expression.arguments.length
      );
      if (functionCalled != null) {
        return hasRequireIn(truffleProject, contract, functionCalled);
      } else {
        console.warn(
          `functionCalled not found (hasRequireIn): ${contractDefinition.name} ${(<any>st).expression.expression.name}`
        );
      }
    }
  }
  return false;
}
