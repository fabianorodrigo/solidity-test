import { IFunctionGraph, ITruffleProjectGraph } from '../../models/graph';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { getType } from '../solidityParser';
import { getFunctionName } from './getFunctionName';
import { sortAncestrais } from './sortAncestrais';

export function getFunctionGraph(
  graph: ITruffleProjectGraph,
  contractDefinition: ContractDefinition,
  functionDefinition: FunctionDefinition,
  throwsExceptionIfNotFound: boolean = true
): IFunctionGraph {
  let r = null;
  if (functionDefinition.type == 'FunctionDefinition') {
    if (
      !functionDefinition.isConstructor ||
      graph.contracts[contractDefinition.name].functions.hasOwnProperty('constructor')
    ) {
      const arrayFunctionGraph =
        graph.contracts[contractDefinition.name].functions[getFunctionName(functionDefinition)];
      r = arrayFunctionGraph?.find(
        (fg) =>
          fg.parameters.length == functionDefinition.parameters.length &&
          fg.parameters.map((pg) => pg.type).toString() ==
            functionDefinition.parameters.map((p) => getType(graph, p.typeName)).toString()
      );
    }
    //Se tratar de uma função herdada
    if (r == null && Object.keys(graph.contracts[contractDefinition.name].inheritsFrom).length > 0) {
      //pega os contratos ordenando pelo nível da herança (A herda de B, que herda de C. Em um cenário
      //desses, o ancestral B viria antes do C); e em níveis iguais, dá preferência por contratos do projeto
      const ancestrais = Object.values(graph.contracts[contractDefinition.name].inheritsFrom).sort(sortAncestrais);
      for (let i = 0; i < ancestrais.length; i++) {
        r = getFunctionGraph(graph, ancestrais[i].contract.contractDefinition, functionDefinition, false);
        if (r != null) {
          break;
        }
      }
    }
  } else if (functionDefinition.type == 'ModifierDefinition') {
    r = graph.contracts[contractDefinition.name].modifiers[functionDefinition.name];
  } else {
    throw new Error(`functionDefinition.type not expected: ${functionDefinition.type}`);
  }
  if (r == null && throwsExceptionIfNotFound) {
    let fs = graph.contracts[contractDefinition.name].functions[functionDefinition.name];
    let length = null;
    if (fs != null) length = fs.length;
    throw new Error(
      `${contractDefinition.name}.${functionDefinition.name}${functionDefinition.parameters.map(
        (p) => `${p.name}:${(p.typeName as any).name}`
      )} ===> ${length} ====> ${fs[0].functionDefinition.parameters.map(
        (p) => `${p.name}:${(p.typeName as any).name}`
      )}`
    );
  }
  return r;
}
