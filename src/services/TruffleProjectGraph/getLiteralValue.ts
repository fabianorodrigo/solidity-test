import { NumberLiteral, ElementaryTypeName } from 'solidity-parser-antlr';
import { isNumberLiteral } from '../solidityParser';

export function getLiteralValue(stateVar) {
  //quando a declaração da variável já não atribui um valor inicial
  if (stateVar.expression == null) {
    //default de booleano é false
    if ((<ElementaryTypeName>stateVar.typeName).name == 'bool') {
      return 'false';
    }
    //default dos inteiros é zero
    else if (
      [
        'int',
        'int8',
        'int16',
        'int32',
        'int48',
        'int96',
        'int128',
        'int256',
        'uint',
        'uint8',
        'uint16',
        'uint32',
        'uint48',
        'uint96',
        'uint128',
        'uint256',
      ].includes((<ElementaryTypeName>stateVar.typeName).name)
    ) {
      return 0;
    } else if (stateVar.typeName.type == 'ArrayTypeName') {
      return [];
    }
    return null;
  } else if (isNumberLiteral(stateVar.expression)) {
    let intValueRestriction: number | BigInt = parseInt((<NumberLiteral>stateVar.expression).number);
    if (intValueRestriction > Number.MAX_SAFE_INTEGER || intValueRestriction < Number.MIN_SAFE_INTEGER)
      intValueRestriction = BigInt(intValueRestriction);
    return intValueRestriction;
  } else {
    return (stateVar.expression as any).value;
  }
}
