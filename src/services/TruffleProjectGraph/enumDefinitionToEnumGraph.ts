import { IStructGraph, IContractGraph, IEnumGraph } from '../../models/graph';
import { getType } from '../solidityParser';
import { IEnumPool, EnumContract } from '../../models/IEnumPool';
import { isArray } from 'util';
import { ArrayTypeName } from 'solidity-parser-antlr';
/**
 * Creates a instance of IEnumBraph based on {enumPool} and {enumName} passed
 *
 * @param contractGraph Instance of contractGraph in which will be inserted the IEnumGraph returned
 * @param enumPool Enum pool source of data to construct IEnumGraph
 * @param enumName Name of the enum that will be extracted from {enumPool}
 */
export function enumDefinitionToEnumGraph(
  contractGraph: IContractGraph,
  enumPool: EnumContract,
  enumName: string
): IEnumGraph {
  const enumGraph: IEnumGraph = {} as IEnumGraph;
  enumGraph.name = enumName;
  enumGraph.contract = contractGraph;
  enumGraph.members = enumPool[enumName].members.map((param) => {
    return param.name;
  });
  return enumGraph;
}
