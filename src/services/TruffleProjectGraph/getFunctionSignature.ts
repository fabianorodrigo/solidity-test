import { FunctionDefinition } from 'solidity-parser-antlr';
import { getType } from '../solidityParser';
import { ITruffleProjectGraph } from '../../models/graph';

export default function getFunctionSignature(
  graph: ITruffleProjectGraph,
  functionDefinition: FunctionDefinition,
  functionName?: string
) {
  return (functionDefinition.name ? functionDefinition.name : functionName).concat(
    '(',
    functionDefinition.parameters.map((p) => getType(graph, p.typeName)).join(','),
    ')'
  );
}
