import { IContractGraph, IInheritance } from '../../models/graph';

/**
 * Verifica se existe a função com o nome {functionName} no grafo de contrato passado e se ela
 * sofre algum overload no próprio contrato diretamente ou mesmo via herança (implementa o contrato de um ancestral com assinatura disferente)
 *
 * @param contractGraph grafo do contrato que será analisado
 * @param functionName nome da função que se deseja obter a informação se ela é ou não sobrecarregada
 */
export function isFunctionOverloaded(contractGraph: IContractGraph, functionName: string): boolean {
  //O nome do contrato ancestral é a chave/key na propriedade inheritsFrom
  //quando o contrato do qual se herda não faz parte do projeto (vem de alguma biblioteca),
  //o value estará 'undefined', por isso o filtro
  const parents: IInheritance[] = Object.values(contractGraph.inheritsFrom).filter((cp) => cp != undefined);
  const functionSignatures = contractGraph.functions[functionName];
  return (
    functionSignatures?.length > 1 ||
    (functionSignatures != null &&
      parents.length > 0 &&
      parents.some(
        (cp) =>
          cp.contract.functions[functionName] &&
          cp.contract.functions[functionName].some(
            (cpf) => JSON.stringify(cpf.parameters) != JSON.stringify(functionSignatures[0].parameters)
          )
      ))
  );
}
