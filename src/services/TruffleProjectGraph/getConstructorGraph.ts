import { IContractGraph, IFunctionGraph } from '../../models/graph';

/**
 * Retorna o construtor do contrato. Se não tiver mas algum dos seus ancestaris tiver,
 * retorna o do ancestral
 *
 * @param contractGraph
 */
export function getConstructorGraph(
  contractGraph: IContractGraph
): { constructor: IFunctionGraph; parameters?: any[] } {
  if (Array.isArray(contractGraph.functions['constructor']) && contractGraph.functions['constructor'].length > 0) {
    return { constructor: contractGraph.functions['constructor'][0] };
  }
  const ancestrais = Object.values(contractGraph.inheritsFrom);
  for (let i = 0; i < ancestrais.length; i++) {
    const subResult = getConstructorGraph(ancestrais[i].contract);
    if (subResult != null) {
      if (ancestrais[i].parameters != null && ancestrais[i].parameters.length > 0) {
        return { constructor: subResult.constructor, parameters: ancestrais[i].parameters };
      }
      return subResult;
    }
  }
  return null;
}
