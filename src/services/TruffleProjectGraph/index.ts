import {
  ITruffleProjectGraph,
  IContractGraph,
  IFunctionGraph,
  IStateVariableGraph,
  IModifierGraph,
  IStateVariableSetGraph,
  SourceVariableSetType,
} from '../../models/graph';
import { TruffleProjectIceFactoryFunction } from '../TruffleFactory';
import { IContract } from '../../models/IContract';
import { getContractConstructor, getType } from '../solidityParser';
import { functionDefinitionToFunctionGraph } from './functionDefinitionToFunctionGraph';
import { getBodyStatementsReferences } from './getBodyStatementsReferences';
import { IFunctionReferences } from '../../models/graph/IFunctionReferences';
import { getDependenciesImported } from './dependenciesImported';
import { modifierDefinitionToModifierGraph } from './modifierDefinitionToModifierGraph';
import getFunctionSignature from './getFunctionSignature';
import { getContractPredecessors } from './getContractPredecessors';
import { mark, warn } from '../Utils';
import { getLiteralValue } from './getLiteralValue';
import {
  getContractAddressParameterReference,
  getContractAddressStateVariableReference,
} from './getContractAddressReference';
import path from 'path';
import fs from 'fs';
import { appendContract } from './appendContractGraph';
import { appendFunctionGraph } from './appendFunctionGraph';
import { isLiteral } from '../solidityParser/statements';
import { NumberLiteral } from 'solidity-parser-antlr';

export { getFunctionSignature };
export * from './isFunctionOverloaded';
export * from './getStateVariableReference';
export * from './getConstructorGraph';
export * from './getFunctionGraph';
export * from './getFunctionName';
export * from './getModifierGraph';
export * from './sortAncestrais';
export * from './appendContractGraph';
export * from './appendFunctionGraph';
export * from './getContractPredecessors';

export function extractTruffleProjectGraph(truffleProject: TruffleProjectIceFactoryFunction): ITruffleProjectGraph {
  let graph: ITruffleProjectGraph = {
    truffleProjectHome: truffleProject.truffleProjectHome,
    contracts: {},
    structs: {},
    enums: {},
  };
  const contracts: IContract[] = truffleProject.getContracts(false);
  //variável para armazenar ao longo das iterações referẽncia para os contratos que
  //terão que ter a propriedade 'instanceNeeded' setada pra true (para contratos de terceiros utilizados como parâmetros)
  //Por que "potencialmente"? Porque pode ser que o contrato não seja deployavel e também deve-se considerar os
  //contratos que herdam dos contratos neste array
  const contractsPotencialNeedInstance: IContractGraph[] = [];
  //1: carrega somente contratos, suas structs e seus enums
  contracts.forEach((contract) => {
    appendContract(truffleProject, graph, contract);
  });

  //2: carrega as dependências, os ancestrais, as variáveis de estado,
  //modificadores, construtor e funções
  contracts.forEach((contract) => {
    //2.0. Depois de carregar todos os contratos, localiza as dependências Importadas
    // e também carrega informação sobre contratos dos quais cada contrato herda
    graph.contracts[contract.contractDefinition.name].dependencies = getDependenciesImported(
      truffleProject,
      contract,
      graph
    );
    Object.values(graph.contracts[contract.contractDefinition.name].dependencies)
      .filter((cg) => cg.isAbstract == false)
      .forEach((cg) => contractsPotencialNeedInstance.push(cg));

    graph.contracts[contract.contractDefinition.name].inheritsFrom = getContractPredecessors(
      truffleProject,
      graph.contracts[contract.contractDefinition.name]
    );

    let contractGraph: IContractGraph = graph.contracts[contract.contractDefinition.name];
    //2.1 state variables of contract without functionsWrite
    truffleProject.getStateVariables(contract).forEach((stateVar) => {
      const type = getType(graph, stateVar.typeName);
      let stateVarGraph: IStateVariableGraph = {
        functionsWrite: {},
        contract: contractGraph,
        name: stateVar.name,
        isArray: stateVar.typeName.type == 'ArrayTypeName',
        initialValue: getLiteralValue(stateVar),
        type: type,
        visibility: stateVar.visibility,
        storageLocation: stateVar.storageLocation,
        isConstant: stateVar.isDeclaredConst,
        isAddressContract: false,
      };
      contractGraph.stateVariables[stateVarGraph.name] = stateVarGraph;
    });

    //2.2 modifiers of contract
    truffleProject.getModifiers(contract).forEach((modDef) => {
      let modifierGraph: IModifierGraph = modifierDefinitionToModifierGraph(contractGraph, modDef);
      contractGraph.modifiers[modifierGraph.name] = modifierGraph;
    });

    //2.3 constructor of contract
    const constructor = getContractConstructor(contract.contractDefinition, false);
    if (constructor) {
      let constrGraph: IFunctionGraph = functionDefinitionToFunctionGraph(truffleProject, contractGraph, constructor);
      constrGraph.name = 'constructor';
      contractGraph.functions[constrGraph.name] = [constrGraph];
    }

    //2.4 functions of contract
    truffleProject.getFunctions(contract).forEach((funcDef) => {
      appendFunctionGraph(truffleProject, contractGraph, funcDef);
    });
  });

  //3. Quando o contrato não passa os parâmetros solicitados pelo seu pai, seja no IS ou seja no
  //seu construtor, ele apresenta o seguinte erro:
  //contract binary not set. Can't deploy new instance. This contract may be abstract, not implement an abstract parent's methods completely
  //OR NOT INVOKE AN INHERIRED CONSTRACT'S CONSTRUCTOR CORRECTLY
  //Nesses casos, marcaremos o contrato como abstrato
  contracts.forEach((contract) => {
    let contractGraph: IContractGraph = graph.contracts[contract.contractDefinition.name];
    //o contrato analisado não é abstrato e possui construtor
    if (
      !contractGraph.isAbstract &&
      Array.isArray(contractGraph.functions['constructor']) && //o a.contract.functions['constructor'] retorna em alguns casos um objeto que não faz parte deste projeto
      contractGraph.functions['constructor']?.length > 0
    ) {
      const ancestrais = Object.values(contractGraph.inheritsFrom).filter((ih) => ih.level == 0); //avalia apenas as heranças diretas
      if (ancestrais.length > 0) {
        ancestrais.forEach((a) => {
          //se o pai tem construtor que recebe parâmetros
          if (
            Array.isArray(a.contract.functions['constructor']) && //o a.contract.functions['constructor'] retorna em alguns casos um objeto que não faz parte deste projeto
            a.contract.functions['constructor']?.length > 0 &&
            a.contract.functions['constructor'][0].parameters.length > 0
          ) {
            //a passagem de parâmetros para o pai pode ser feita tanto diretamente no comando IS
            //quanto no construtor
            //os parâmetros do pai tem que ser passados em UM ou em OUTRO
            //www.bitdegree.org/learn/solidity-inheritance
            if (
              a.contract.functions['constructor'][0].parameters.length != a.parameters.length &&
              contractGraph.functions['constructor'][0].modifiers[a.contract.name] == null
            ) {
              contractGraph.isAbstract = true;
              if (!contractGraph.isThirdPartyLib) {
                mark(`${contractGraph.name} não repassa seus parâmetros de entrada para o pai`);
                console.log(
                  `${contractGraph.name}`,
                  a.contract.functions['constructor'][0].parameters.map((p) => p.type)
                );
                console.log(
                  `${a.contract.name}`,
                  a.parameters.map((p) => p.type)
                );
              }
            }
          }
        });
      }
    }
  });

  //4. percorre novamente as variáveis de estado para replicar dos contratos herdados para os filhos
  contracts.forEach((contract) => {
    let contractGraph: IContractGraph = graph.contracts[contract.contractDefinition.name];
    Object.values(contractGraph.stateVariables).forEach((stateVarGraph) => {
      replicaStateVariableToAllChildren(contractGraph, stateVarGraph);
    });

    //replica a StateVariable para quem herda dele
    function replicaStateVariableToAllChildren(contractGraph: IContractGraph, stateVar: IStateVariableGraph) {
      //pega os contratos NÃO ABSTRATOS em que um dos pais seja o {contractGraph} recebido
      Object.values(graph.contracts)
        .filter(
          (cg) =>
            !Object.is(cg, contractGraph) &&
            !cg.isAbstract &&
            Object.values(cg.inheritsFrom).some(
              (ih) => ih.contract.name == contractGraph.name && ih.contract.solFilePath == contractGraph.solFilePath
            )
        )
        .forEach((child) => {
          if (graph.contracts[child.name].stateVariables[stateVar.name] == null) {
            graph.contracts[child.name].stateVariables[stateVar.name] = {
              functionsWrite: {},
              contract: child,
              name: stateVar.name,
              isArray: stateVar.isArray,
              initialValue: stateVar.initialValue,
              type: stateVar.type,
              visibility: stateVar.visibility,
              storageLocation: stateVar.storageLocation,
              isConstant: stateVar.isConstant,
              isAddressContract: false,
            };
          }
          replicaStateVariableToAllChildren(child, stateVar);
        });
    }
  });

  //5: Depois de carregar variáveis de estado dos contratos, itera novamente
  // pelas variáveis de estado do tipo 'address' para adicionar a informação se trata-se
  //de um endereço de contrato e qual contrato.
  contracts.forEach((contract) => {
    let contractGraph: IContractGraph = graph.contracts[contract.contractDefinition.name];
    //4.1 itera novamente pelas state variables de endereço para verificar se estas são
    //usadas como input para instância de algum contrato e, se for o caso, setar as
    // propriedades 'isAddressContract' e 'contractAddressOf'
    const contractsByStateVarAddress = getContractAddressStateVariableReference(truffleProject, contractGraph);
    Object.keys(contractsByStateVarAddress).forEach((svname) => {
      Object.keys(contractsByStateVarAddress).forEach((stateVar) => {
        if (contractsByStateVarAddress[stateVar].length > 0) {
          contractGraph.stateVariables[stateVar].isAddressContract = true;
          contractGraph.stateVariables[stateVar].contractAddressOf = contractsByStateVarAddress[stateVar];
          //incluir na lista de potencialmente instanciáveis se já não estiver lá
          contractsByStateVarAddress[stateVar].forEach((cg) => {
            if (!contractsPotencialNeedInstance.some((innercg) => Object.is(innercg, cg))) {
              contractsPotencialNeedInstance.push(cg);
            }
          });
        }
      });
    });
  });

  //6: depois de carregar construtor e funções e variáveis de estado, itera
  //novamente pelas funções e seus parâmetros para determinar se algum é
  //endereço de contrato OU se é um UserDefinedTypeName mesmo
  contracts.forEach((contract) => {
    let contractGraph: IContractGraph = graph.contracts[contract.contractDefinition.name];
    Object.values(contractGraph.functions).forEach((array) => {
      array.forEach((functionGraph) => {
        //busca por parâmetros que sejam UserDefinedTypeName
        functionGraph.parameters
          .filter((p) => p.isUserDefinedType == true)
          .forEach((p) => {
            if (
              !contractsPotencialNeedInstance.some((innercg) =>
                Object.is(innercg, contractGraph.projectGraph.contracts[p.type])
              )
            ) {
              contractsPotencialNeedInstance.push(contractGraph.projectGraph.contracts[p.type]);
            }
          });
        //para parâmetros do tipo address, busca por FunctionCalls que sejam um NomeDeContrato(address)
        const contractsByParameterAddress = getContractAddressParameterReference(
          truffleProject,
          contractGraph,
          functionGraph
        );
        Object.keys(contractsByParameterAddress).forEach((paramName) => {
          if (contractsByParameterAddress[paramName].length > 0) {
            const paramGraph = functionGraph.parameters.find((p) => p.name == paramName);
            paramGraph.isAddressContract = true;
            paramGraph.contractAddressOf = contractsByParameterAddress[paramName].map((c) => c.name);
            //incluir na lista de potencialmente instanciáveis se já não estiver lá
            contractsByParameterAddress[paramName].forEach((cg) => {
              if (!contractsPotencialNeedInstance.some((innercg) => Object.is(innercg, cg))) {
                contractsPotencialNeedInstance.push(cg);
              }
            });
          }
        });
      });
    });
  });

  //7. percorre todos os contratos com potencial necessidade de instanciar a fim de se
  //setar a propriedade instanceNeeded
  contractsPotencialNeedInstance.forEach((cpni) => {
    //pega o próprio contrato cpni, seus herdeiros e dependências que por ventura existirem (somente não abstratos e de terceiros)
    //afinal, os contratos do projeto não abstratos já serão necessariamente instanciados
    const escopo = Object.values(graph.contracts).filter(
      (cg) =>
        !cg.isAbstract &&
        cg.isThirdPartyLib &&
        (Object.is(cg, cpni) ||
          Object.values(cg.inheritsFrom).some((innercg) => Object.is(cpni, innercg.contract)) ||
          Object.values(cg.dependencies).some((innercg) => Object.is(cpni, innercg)))
    );
    escopo.forEach((cg) => {
      //só será setado com intanceNeeded se existir um arquivo JSON no subdiretório build, o artifacts.require olha pra este JSON
      const end = cg.solFilePath.indexOf('/node_modules/');
      const buildJSONPath = path.join(cg.solFilePath.substr(0, end), `build/contracts/${cg.name}.json`);
      cg.instanceNeeded = fs.existsSync(buildJSONPath);
    });
  });

  //8: Depois de carregar todas as funções e variáveis de estado dos contratos,
  // percorre novamente construtor e as funções para buscar referências (somente de contratos do proprio projeto)
  contracts.forEach((contract) => {
    let contractGraph: IContractGraph = graph.contracts[contract.contractDefinition.name];
    //5.1 percorre novamente construtor e as funções para buscar referências (somente de contratos do proprio projeto)
    const constructor = getContractConstructor(contract.contractDefinition, false);
    if (constructor) {
      const refs: IFunctionReferences = getBodyStatementsReferences(
        truffleProject,
        contractGraph,
        contract.contractDefinition,
        constructor,
        constructor.body
      );
      //Popula as variáveis de estado alteradas no construtor
      if (refs.stateVariables) {
        refs.stateVariables.forEach((sv) => {
          contractGraph.functions['constructor'].find((f) => f.signature == refs.signature).stateVariablesWritten[
            sv.name
          ] = { ...sv, functionName: refs.name, functionSignature: refs.signature };
          contractGraph.projectGraph.contracts[sv.contractName].stateVariables[sv.name].functionsWrite['constructor'] =
            contractGraph.functions['constructor'];
        });
      }
      //Popula as funções invocadas no construtor
      loadCalledFunctionsInGraph(refs, contractGraph);
      loadFunctionCallingsInGraph(refs, contractGraph);
    }
    //demais funções
    truffleProject.getFunctions(contract).forEach((funcDef) => {
      const refs: IFunctionReferences = getBodyStatementsReferences(
        truffleProject,
        contractGraph,
        contract.contractDefinition,
        funcDef,
        funcDef.body
      );
      //console.log(contract.contractDefinition.name, funcDef.name, refs.stateVariables.length);
      if (refs.stateVariables) {
        refs.stateVariables.forEach((sv) => {
          contractGraph.functions[funcDef.name].find((f) => f.signature == refs.signature).stateVariablesWritten[
            sv.name
          ] = { ...sv, functionName: refs.name, functionSignature: refs.signature };
          if (
            !contractGraph.projectGraph.contracts[sv.contractName].stateVariables[sv.name].functionsWrite[funcDef.name]
          ) {
            contractGraph.projectGraph.contracts[sv.contractName].stateVariables[sv.name].functionsWrite[
              funcDef.name
            ] = [contractGraph.functions[funcDef.name].find((fg) => fg.signature == refs.signature)];
          } else {
            contractGraph.projectGraph.contracts[sv.contractName].stateVariables[sv.name].functionsWrite[
              funcDef.name
            ].push(contractGraph.functions[funcDef.name].find((fg) => fg.signature == refs.signature));
          }
        });
      }
      //Popula as funções invocadas em funcDef
      loadCalledFunctionsInGraph(refs, contractGraph);
      loadFunctionCallingsInGraph(refs, contractGraph);
    });

    graph.contracts[contractGraph.name] = contractGraph;
  });

  //9: Depois de carregar associar as referências de funcoes e  variáveis de estado
  //percorre novamente para fazer as associações indiretas das variáveis de estado
  //somente contratos que herdem de algum
  contracts.forEach((contract) => {
    let contractGraph: IContractGraph = graph.contracts[contract.contractDefinition.name];
    //Se não herda de nenhum, sai
    if (Object.keys(contractGraph.inheritsFrom).length == 0) return;
    //somente funcoes que chamem alguma função
    const functionCallersArrays = Object.values(contractGraph.functions).filter((array) =>
      array.some((fg) => Object.keys(fg.calledFunctions).length > 0)
    );
    functionCallersArrays.forEach((fCallerArray) => {
      fCallerArray.forEach((fgCaller) => {
        //se sobrou algum overload de função que não chama outra função, já sai
        if (Object.keys(fgCaller.calledFunctions).length == 0) return;
        //itera pelas chamadas de função que a chamadora tem
        Object.values(fgCaller.functionCallings).forEach((fCallingArray) => {
          fCallingArray.forEach((fCalling) => {
            //Só analisa se escreve em alguma variável de estado que pertença
            Object.values(fCalling.functionCalled.stateVariablesWritten).forEach((sv) => {
              if (contractGraph.stateVariables[sv.name] != null && fgCaller.stateVariablesWritten[sv.name] == null) {
                const originalWritten: IStateVariableSetGraph = JSON.parse(
                  JSON.stringify(fCalling.functionCalled.stateVariablesWritten[sv.name])
                );
                if (
                  originalWritten.source.type == SourceVariableSetType.parameter ||
                  originalWritten.source.type == SourceVariableSetType.parameterMemberAccess
                ) {
                  const paramIndex = fCalling.functionCalled.parameters.findIndex(
                    (p) => p.name == originalWritten.source.name
                  );
                  if (paramIndex > -1) {
                    const paramCalling = fCalling.parameters[paramIndex];
                    if (paramCalling != null) {
                      const param = fgCaller.parameters.find((p) => p.name == paramCalling.name);
                      if (param != null) {
                        originalWritten.source.type = SourceVariableSetType.parameter;
                        originalWritten.source.name = param.name;
                      } else if (isLiteral(paramCalling)) {
                        originalWritten.source.type = SourceVariableSetType.literal;
                        originalWritten.source.aditionalInfo = paramCalling.hasOwnProperty('number')
                          ? (paramCalling as NumberLiteral).number
                          : (paramCalling as any).value;
                        delete originalWritten.source.name;
                      }
                    }
                  }
                }
                fgCaller.stateVariablesWritten[sv.name] = originalWritten;
              }
            });
          });
        });
      });
    });

    graph.contracts[contractGraph.name] = contractGraph;
  });
  return graph;
}

/**
 * Popula a propriedade {calledFunction} da FunctionGraph dentro de {contraGraph} com as referências presentes em {refs}
 * @param refs Instância que representa referências a variáveis de estado e chamadas de função dentro de uma função específica
 * @param contractGraph grafo do contrato em análise
 */
function loadCalledFunctionsInGraph(refs: IFunctionReferences, contractGraph: IContractGraph) {
  if (refs.functionsCalled) {
    refs.functionsCalled.forEach((fCalled) => {
      //encontra a função/constructor com a mesma assinatura ao que o refs faz referência
      const funcCaller = contractGraph.functions[refs.name].find((c) => c.signature == refs.signature);
      //verifica se entre as function called já existe função com o mesmo nome. Não existindo, cria array vazio
      if (!Object.keys(funcCaller.calledFunctions).find((functionName) => functionName == fCalled.name)) {
        funcCaller.calledFunctions[fCalled.name] = [];
      }
      //Verifica se debaixo daquele nome em calledFunctions já não existe aquela assinatura
      //Nâo existindo, faz o push
      if (
        funcCaller &&
        funcCaller.calledFunctions[fCalled.name] &&
        !funcCaller.calledFunctions[fCalled.name].find((cf) => cf.signature == fCalled.signature)
      ) {
        //Os dados vindos de {fCalled} precisam existir no grafo para conseguirmos
        //buscar suas referências
        if (
          contractGraph.projectGraph.contracts[fCalled.parent] &&
          contractGraph.projectGraph.contracts[fCalled.parent].functions[fCalled.name]
        ) {
          const fgCalled = contractGraph.projectGraph.contracts[fCalled.parent].functions[fCalled.name].find(
            (fg) => fg.signature == fCalled.signature
          );
          if (fgCalled) {
            funcCaller.calledFunctions[fCalled.name].push(fgCalled);
          } else {
            warn(`Função chamada no grafo não localizada: ${fCalled.signature}`);
          }
        }
      }
    });
  }
}

/**
 * Popula a propriedade {functionCallings} da FunctionGraph dentro de {contraGraph} com as referências presentes em {refs}
 * @param refs Instância que representa referências a variáveis de estado e chamadas de função dentro de uma função específica
 * @param contractGraph grafo do contrato em análise
 */
function loadFunctionCallingsInGraph(refs: IFunctionReferences, contractGraph: IContractGraph) {
  if (refs.functionsCalled) {
    refs.functionCallings.forEach((fCalled) => {
      //encontra a função/constructor com a mesma assinatura ao que o refs faz referência
      const funcCaller = contractGraph.functions[refs.name].find((c) => c.signature == refs.signature);
      //verifica se entre as function callings já existe função com o mesmo nome. Não existindo, cria array vazio
      if (funcCaller.functionCallings[fCalled.name]) {
        funcCaller.functionCallings[fCalled.name] = [];
      }
      if (funcCaller) {
        if (!funcCaller.functionCallings[fCalled.signature]) {
          funcCaller.functionCallings[fCalled.signature] = [];
        }
        if (
          contractGraph.projectGraph.contracts[fCalled.parent] &&
          contractGraph.projectGraph.contracts[fCalled.parent].functions[fCalled.name]
        ) {
          //Os dados vindos de {fCalled} precisam existir no grafo para conseguirmos
          //buscar suas referências
          const fgCalled = contractGraph.projectGraph.contracts[fCalled.parent].functions[fCalled.name].find(
            (fg) => fg.signature == fCalled.signature
          );
          if (fgCalled) {
            funcCaller.functionCallings[fCalled.signature].push({
              functionCalled: fgCalled,
              parameters: fCalled.parameters,
            });
          } else {
            warn(`Função chamada no grafo não localizada: ${fCalled.signature}`);
          }
        }
      }
    });
  }
}
