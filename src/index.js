const fs = require('fs');
const path = require('path');
const fsextra = require('fs-extra');

const shelljs = require('shelljs');
const os = require('os');
const colors = require('colors');
const readlineSync = require('readline-sync');
const loaders = require('./loaders');
const ExceptionDrivenFactory = require('./services/ExceptionDrivenFactory');

const REPORT_FILE_NAME = 'solidity-test-report.json';

global.generalResults = {};

loaders.init();

(async () => {
  let pathTruffleProjects = process.env.DEFAULT_SOLIDITY_DIRECTORY;
  console.log('Path Projetos:', pathTruffleProjects);
  console.log('Nível debug:', process.env.DEBUG_LEVEL);
  // Interacao usuario se a variável de ambiente não está setada
  //if (pathTruffleProjects == '' || pathTruffleProjects == null) {
  if (process.argv.length < 3 || !fs.existsSync(process.argv[2])) {
    pathTruffleProjects = readlineSync.question('Caminho onde se encontram os projetos a serem analisados:');
  } else {
    pathTruffleProjects = process.argv[2];
  }
  //}

  //Identificando projetos truffle dentro do diretório informado
  shelljs.cd(pathTruffleProjects);
  //versão para contornar Javascript heap out of memory
  const isTruffleProject = (p) => {
    return fs.existsSync(path.join(p, 'truffle.js')) || fs.existsSync(path.join(p, 'truffle-config.js'));
  };

  const truffleProjects = [];
  if (isTruffleProject(pathTruffleProjects)) {
    truffleProjects.push(pathTruffleProjects);
  } else {
    fs.readdirSync(pathTruffleProjects).forEach((f) => {
      const dirPath = path.join(pathTruffleProjects, f);
      if (f != 'node_modules' && fs.lstatSync(dirPath).isDirectory() && isTruffleProject(dirPath)) {
        truffleProjects.push(dirPath);
      }
    });
  }

  //cria diretório para trabalhar localmente com os projetos truffle
  const workdir = path.join(__dirname, process.env.WORKDIR || './workdir');
  if (!fs.existsSync(workdir)) {
    fs.mkdirSync(workdir);
  }
  if (fs.existsSync(path.join(workdir, REPORT_FILE_NAME))) {
    global.generalResults = JSON.parse(fs.readFileSync(path.join(workdir, REPORT_FILE_NAME)));
  }

  //fazer cópia dos projetos
  for (let p = 0; p < truffleProjects.length; p++) {
    const prj = truffleProjects[p];
    const prjBaseName = path.basename(prj);
    //Se o diretório do projeto existir dentro do workdir
    if (fs.existsSync(path.join(workdir, prjBaseName))) {
      //Se existir o arquivo "ignore", pula pra próxima interação
      //Senão, limpa o diretório e realiza a geração dos testes
      if (
        fs.existsSync(path.join(workdir, prjBaseName, 'successfulFinished')) ||
        fs.existsSync(path.join(workdir, prjBaseName, 'failedFinished'))
      ) {
        continue;
      } else {
        fsextra.emptyDirSync(path.join(workdir, prjBaseName));
      }
    }
    shelljs.cp('-r', prj, workdir);
    //Limpar diretório de testes
    fsextra.emptyDirSync(path.join(workdir, prjBaseName, 'test'));
    //apaga o diretórios com dados sobre cobertura de código
    const coverageJSON = path.join(workdir, prjBaseName, 'coverage.json');
    const coverageDir = path.join(workdir, prjBaseName, 'coverage');
    fsextra.emptyDirSync(coverageDir);
    fs.rmdir(coverageDir, (err) => {
      if (err) {
        console.error(colors.red(`Falha ao apagar ${coverageDir}: ${err.message}`));
      }
    });
    fs.unlink(coverageJSON, (err) => {
      if (err) {
        console.error(colors.red(`Falha ao apagar ${coverageJSON}: ${err.message}`));
      }
    });
    /*const randomDriven = RandomDrivenFactory({
      truffleProjectHome: path.join(workdir, prjBaseName),
    });
    randomDriven.generateSequences();*/
    const exceptionDriven = ExceptionDrivenFactory({
      truffleProjectHome: path.join(workdir, prjBaseName),
    });
    //Se o arquivo não contémo projeto, cria a estrutura
    if (global.generalResults[prjBaseName] == null) {
      global.generalResults[prjBaseName] = {};
      global.generalResults[prjBaseName].project = truffleProjects[p];
    }
    global.generalResults[prjBaseName].successfull = false;
    global.generalResults[prjBaseName].dhFinish = null;
    global.generalResults[prjBaseName].dhInit = new Date();
    await exceptionDriven.generateSequences();
    global.generalResults[prjBaseName].dhFinish = new Date();
    global.generalResults[prjBaseName].successfull = true;
    shelljs.touch(path.join(workdir, prjBaseName, 'successfulFinished'));
    shelljs.cd(path.join(workdir, prjBaseName));
    fs.writeFileSync(path.join(workdir, REPORT_FILE_NAME), JSON.stringify(global.generalResults));
  }
  fs.writeFileSync(path.join(workdir, REPORT_FILE_NAME), JSON.stringify(global.generalResults));
  //shelljs.exec('rhythmbox-client --play-uri assets/Beethoven-Symphony5-1.mp3');
})();
