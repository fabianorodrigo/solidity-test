export interface IValuesPool {
  [key: string]: any[];
}
