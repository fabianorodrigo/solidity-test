export enum RestrictionTypexxxxx {
  functionParameter_Literal, //a comparação é entre um parâmetro de entrada da função e um valor literal
  functionParameter_functionParameter, // a comparação é entre dois parâmetros de entrada da função
  functionParameterMember_Literal, //a comparação é entre uma propriedade de um parâmetro de entrada da função e um valor literal
  functionParameterMember_functionParameter, //a comparação é entre uma propriedade de um parâmetro de entrada da função e um outro parâmetro da função
  functionParameterMember_functionParameterMember, //a comparação é entre duas propriedades de um ou dois parâmetros de entrada da função
  transactionParameter_Literal, //a comparação é entre um transaction parameter (eg. msg.value) e um valor literal
  transactionParameter_functionParameter, //a comparação é entre um transaction parameter (eg. msg.value) e um outro parâmetro de entrada da função
  transactionParameter_functionParameterMember, //a comparação é entre um transaction parameter (eg. msg.value) e um atributo de um parâmetro de entrada da função
  stateVariable_Literal, //a comparação é entre uma variável de estado e um valor literal
  stateVariable_functionParameter, //a comparação é entre uma variável de estado e um parâmetro de entrada da função
  stateVariable_functionParameterMember, //a comparação é entre uma variável de estado e um atributo de um parâmetro de entrada da função
  stateVariable_transactionParameter, //a comparação é entre uma variável de estado e um atributo do transactionparameter (msg.value)
  //TODO: Testes unitários para validar identificação dos tipos abaixo
  stateVariable_stateVariable, //a comparação é entre duas variáveis de estado
  stateVariableMember_Literal, //a comparação é entre um atributo de uma variável de estado e um valor literal
  stateVariableMember_stateVariable, //a comparação é entre um atributo de uma variável de estado e uma oura variável de estado
  stateVariableMember_stateVariableMember, //a comparação é entre um atributo de uma variável de estado e um atributo de outra variável de estado
  stateVariableMember_functionParameter, //a comparação é entre um atributo de uma variável de estado e um parâmetro de entrada da função
  stateVariableMember_functionParameterMember, //a comparação é entre um atributo de uma variável de estado e um atributo de um parâmetro de entrada da função
  stateVariableMember_transactionParameter, //a comparação é entre um atributo de uma variável de estado e um atributo do transactionparameter (msg.value)
}
