export interface IBeforeEach {
  variableDeclare: string;
  result: string;
  contractsDeployed: IDeployedContract[];
}

export interface IFinalBeforeEach {
  code: string;
  contractsDeployed: IDeployedContract[];
}

export interface IDeployedContract {
  contractName: string;
  stringfieldParams: string[];
}
