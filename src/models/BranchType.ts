export enum BranchType {
  require = 'require',
  ifWithRevert = 'ifWithRevert',
  else = 'else',
  notDefined = '',
}
