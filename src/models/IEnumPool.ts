import { EnumValue } from 'solidity-parser-antlr';

export type EnumContract = { [enumName: string]: IEnum };

export interface IEnum {
  members: EnumValue[];
}

export interface IEnumPool {
  [contractName: string]: EnumContract;
}
