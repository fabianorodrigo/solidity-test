import { IContractGraph, IFunctionGraph } from './graph';
import { ITypeValue } from './scenario';

export interface IFunctionCalling {
  contractInstanceTarget: string; //o target pode ser um herdeiro de functionCalled.functionGraph.contract
  functionCalled: IFunctionGraph;
  parametersValues: ITypeValue[];
  stateVariablesAffected: { name: string; memberName?: string }[];
}
