import { Location } from 'solidity-parser-antlr';
import { IRestriction } from './IResctriction';
import { BranchType } from './BranchType';

export interface IBranch {
  loc: Location;
  parent: any;
  restrictions: IRestriction[];
  type: BranchType;
}

/**
 * Branch without restrictions
 */
export const EMPTY_BRANCH = { restrictions: [] } as IBranch;
