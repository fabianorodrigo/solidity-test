import { TypeName } from 'solidity-parser-antlr';

export interface IValue {
  isCode: boolean;
  js: string;
  sol: string;
  obj: any;
  length?: number; //metadado usado quando o tipo é array
  memberName?: string;
  index?: any;
  typeName?: TypeName;
}
