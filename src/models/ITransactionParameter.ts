import { IValue } from './IValue';

export interface ITransactionParameter {
  sender: IValue;
  value: IValue;
}
