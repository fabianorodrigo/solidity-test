import { ITypeValue } from './ITypeValue';

export type TypeReferences = { [name: string]: ITypeValue };
