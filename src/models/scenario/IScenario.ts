import { ITypeValue } from './ITypeValue';
import { IFunctionCalling } from '../IFunctionCalling';

export interface IScenario {
  success: boolean;
  scenarioDescription: string;
  parametersValues: ITypeValue[];
  //Indica se o cenário contempla alguma restrição relacionada ao transaction parameter
  transactionParameterRestriction: boolean;
  //functions and it's parameters to be executed before the
  //scenarios test is run (they prepare the environment)
  preFunctionCallings?: IFunctionCalling[];
}

export interface IScenarioFail extends IScenario {
  paramNameRestriction: string;
}
