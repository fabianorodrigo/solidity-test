import { IValue } from '../IValue';
import { IRestriction } from '../IResctriction';
import { UserDefinedTypeName, ContractDefinition, Mapping, FunctionDefinition, TypeName } from 'solidity-parser-antlr';
import { TruffleProjectIceFactoryFunction } from '../../services/TruffleFactory';
import { ITypeValue } from './ITypeValue';

type FunctionRandom = (
  truffleProject: TruffleProjectIceFactoryFunction,
  restrictions: IRestriction[],
  typeName?: UserDefinedTypeName | Mapping,
  contractDefinition?: ContractDefinition,
  getRandomValue?: Function
) => IValue;

type FunctionGetExtraBoundary = (
  truffleProject: TruffleProjectIceFactoryFunction,
  contractDefinition: ContractDefinition,
  functionDefinition: FunctionDefinition,
  restriction: IRestriction,
  typeName: ITypeValue
) => IValue;

export interface ITypeValueGenerator {
  random: FunctionRandom;
  getExtraBoundary: FunctionGetExtraBoundary;
}
