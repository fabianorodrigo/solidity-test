import { IValue } from '../IValue';

export interface ITypeValue {
  type: string;
  subtype: string;
  stringfieldValue: IValue;
}
