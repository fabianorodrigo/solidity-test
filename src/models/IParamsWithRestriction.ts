import { IValue } from './IValue';
import { BranchType } from './BranchType';
import { IRestriction } from './IResctriction';

export interface IParamsWithRestriction {
  [indice: string]: IRestriction[];
}

export interface ITransactionParamsWithRestriction {
  transaction: {
    [atributo: string]: IRestriction[];
  };
}
