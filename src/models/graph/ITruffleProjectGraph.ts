import { SourceVariableSetType } from './SourceVariableSetType';
import { FunctionDefinition, ContractDefinition, ModifierDefinition } from 'solidity-parser-antlr';
import { StateVariableSetOperator } from './StateVariableSetOperator';
import { ITypeValue } from '../scenario';
import { IRestrictionValue } from '../IResctriction';

export type contractList = { [name: string]: IContractGraph };
export type inheritanceList = { [name: string]: IInheritance };

type stateVariableList = { [name: string]: IStateVariableGraph };
type stateVariableSetList = { [name: string]: IStateVariableSetGraph };
type functionList = { [name: string]: IFunctionGraph[] };
type functionCallingList = { [signature: string]: IFunctionCallingGraph[] };
type structList = { [name: string]: IStructGraph };
type enumList = { [name: string]: IEnumGraph };
type modifierList = { [name: string]: IModifierGraph | IFunctionGraph };

export interface ITruffleProjectGraph {
  truffleProjectHome: string;
  contracts: contractList;
  structs: structList;
  enums: enumList;
}

export interface IContractGraph {
  projectGraph: ITruffleProjectGraph;
  name: string;
  kind: string;
  isAbstract: boolean;
  inheritsFrom: inheritanceList;
  stateVariables: stateVariableList;
  functions: functionList;
  modifiers: modifierList;
  dependencies: contractList;
  structs: structList;
  enums: enumList;
  isThirdPartyLib: boolean; //If the contract was taken from some SOL file under node_modules
  isOnlyForTest: boolean;
  contractDefinition: ContractDefinition;
  solFilePath: string;
  instanceNeeded: boolean; //a instance is needed for tests and have to be created by beforeEach function
}

export interface IStructGraph {
  contract: IContractGraph;
  name: string;
  parameters: IParameterGraph[];
}

export interface IEnumGraph {
  contract: IContractGraph;
  name: string;
  members: string[];
}

export interface IStateVariableGraph {
  contract: IContractGraph;
  name: string;
  initialValue: any;
  type: string;
  visibility: string;
  isConstant: boolean;
  isArray: boolean;
  storageLocation: string;
  functionsWrite: functionList;
  isAddressContract: boolean;
  contractAddressOf?: IContractGraph[]; // se for um endereço de contrato, esse atributo deve existir
}

export interface ILocalVariableGraph {
  functionSignature: string;
  name: string;
  initialRestrictionValue: IRestrictionValue;
  type: string;
  isConstant: boolean;
  isArray: boolean;
  isAddressContract: boolean;
  contractAddressOf?: IContractGraph[]; // se for um endereço de contrato, esse atributo deve existir
}

export interface IStateVariableSetGraph {
  contractName: string;
  functionName: string;
  functionSignature: string;
  name: string;
  conditional: boolean;
  operator: StateVariableSetOperator;
  source: SourceVariableSet;
}

export interface SourceVariableSet {
  type: SourceVariableSetType;
  name: string;
  aditionalInfo: string;
}

export interface IFunctionGraph {
  contract: IContractGraph;
  name: string;
  isConstructor: boolean;
  isPayable: boolean;
  visibility: string;
  parameters: IParameterGraph[];
  return: IParameterGraph[];
  modifiers: modifierList;
  calledFunctions: functionList;
  functionCallings: functionCallingList;
  stateVariablesWritten: stateVariableSetList;
  signature: string;
  functionDefinition: FunctionDefinition;
  localVariables: ILocalVariableGraph[];
}

export interface IParameterGraph {
  name: string;
  type: string;
  isArray: boolean;
  isStruct: boolean;
  isEnum: boolean;
  isUserDefinedType: boolean;
  isAddressContract: boolean;
  contractAddressOf?: string[]; // se for um endereço de contrato, esse atributo deve existir com o nome dos contratos
}

export interface IModifierGraph {
  name: string;
  contract: IContractGraph;
  parameters: IParameterGraph[];
  functionDefinition: ModifierDefinition;
}

export interface IFunctionCallingGraph {
  functionCalled: IFunctionGraph;
  parameters: any[];
}

export interface IInheritance {
  contract: IContractGraph;
  parameters: any[];
  level: number;
}
