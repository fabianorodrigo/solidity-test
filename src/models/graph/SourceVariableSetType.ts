export enum SourceVariableSetType {
  parameter = 'parameter',
  transactionParameter = 'transactionParameter',
  parameterMemberAccess = 'parameterMemberAccess',
  parameterIndexAccess = 'parameterIndexAccess',
  literal = 'literal',
  others = 'others',
}
