import { SourceVariableSet } from './ITruffleProjectGraph';
import { StateVariableSetOperator } from './StateVariableSetOperator';
export interface IFunctionReferences {
  name: string;
  signature: string;
  stateVariables: {
    contractName: string;
    name: string;
    conditional: boolean;
    operator: StateVariableSetOperator;
    source: SourceVariableSet;
  }[];
  functionsCalled: {
    name: string;
    signature: string;
    conditional: boolean;
    parent: string;
  }[];
  functionCallings: {
    name: string;
    signature: string;
    conditional: boolean;
    parent: string;
    parameters: any[];
  }[];
}
