import { BaseASTNode, ContractDefinition, Statement } from 'solidity-parser-antlr';
import { TruffleProjectIceFactoryFunction } from '../../services/TruffleFactory';
import { TypeSearchResultList } from './ISearchResult';

export type SearchFunction = (
  //projeto analisado
  truffleProject: TruffleProjectIceFactoryFunction,
  //contrato analisado
  contract: ContractDefinition,
  //Statement em análise
  statement: Statement,
  //array a ser populado com resultado
  result: any[],
  //parâmetros de entrada de função relacionados à busca. Necessários quando um parâmetro de entrada é repassado
  //para uma outra função e é necessário fazer a tradução de nomes para continuar a busca
  functionParametersNames?: string[],
  //parâmetros de entrada de função relacionados à busca original. Quando um parâmetro de entrada é repassado
  //para uma outra função, ocorre a tradução de nomes para continuar a busca. Contudo, o resultado deve ser retornado
  //com o nome do parâmetro ou da variável de estado à qual a busca original fazia referência
  originalParametersNames?: string[],
  //parâmetros adicionais serem utilizados na função
  args?: any
) => boolean;
