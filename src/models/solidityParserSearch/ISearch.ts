import { Statement } from 'solidity-parser-antlr';
import { SearchFunction } from './SearchFunction';

export interface ISearch {
  //nome para agrupar os resultdos de busca quando se enviar várias buscas ao mesmo tempo para ganhar desempenho
  name: string;
  //Função que receberá os statements filtrados e conterá a lógica necessária para obter os resultados desejados
  searchFunction: SearchFunction;
  //fixme: ao invés de array de string, poderia ser array de um objeto que ao menos informasse se é parâmeto ou variável de estado
  //parâmetros de entrada de função relacionados à busca. Necessários quando um parâmetro de entrada é repassado
  //para uma outra função e é necessário fazer a tradução de nomes para continuar a busca
  functionParametersNames?: string[];
  //Quando há chamadas aninhadas com repassagem de parâmetros, é necessário manter um mapeamento dos parâmetos
  //repassados à função sendo analisada e o nome do parâmeto/state informado na busca original
  originalFunctionParametersNames?: string[];
  //parâmetros adicionais a serem utilizados na função
  args?: any;
}
