export interface ISearchResult {
  result: [];
}

export type TypeSearchResultList = { [name: string]: any[] };
