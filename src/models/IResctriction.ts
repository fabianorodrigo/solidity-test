import { IValue } from './IValue';
import { BranchType } from './BranchType';
import { RestrictionElementType } from './RestrictionElementType';

export interface IRestrictionValue {
  value: IValue;
  elementType: RestrictionElementType;
}

export interface IRestriction {
  operator: string;
  from: BranchType;
  right: IRestrictionValue;
  left: IRestrictionValue;
  level?: number; //aplicável na identificação de branches
}
