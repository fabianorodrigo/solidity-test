import { StructDefinition } from 'solidity-parser-antlr';

export interface IStruct extends StructDefinition {
  parentName: string;
  fullName: boolean;
  contractName: string;
}

export interface IStructsPool {
  [key: string]: IStruct;
}
