export interface ITestResult {
  stdout: string;
  stderr: string;
  code: number;
}
