import { ContractDefinition } from 'solidity-parser-antlr';

export interface IContract {
  contractDefinition: ContractDefinition;
  solFilePath: string;
  isThirdPartyLib?: boolean; //true if the contract is from some solidity under node_modules
  isOnlyForTest: boolean;
  directives?: { [name: string]: string };
}
