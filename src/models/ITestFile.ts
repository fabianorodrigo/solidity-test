export interface ITestFile {
  testFilePath: string;
  testFileContent: string;
}
