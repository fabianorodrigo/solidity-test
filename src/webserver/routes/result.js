/************************************************************************
 Módulo responsável pela resposta as requisições relacionadas aos resultados
*************************************************************************/
const path = require('path');
//Faz requires fora do módulo para que não seja importada novamente toda
//as vezes que a função exportada for utilizada
//Isto por que o modulo, uma vez importado, fica em cache
const ResultController = require('../controllers/ResultController');

module.exports = function (app) {
  //Instanciando controller
  let controller = new ResultController();

  //GET home
  app.get('/', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../public/index.html'));
  });

  //GET result list
  app.get('/Projects', controller.list);

  //POST get contracts of project
  app.post('/Contracts', controller.contracts);

  //POST get contracts of project
  app.post('/Tests', controller.tests);

  app.post('/CoverageLog', controller.coverageLog);
};
