/************************************************************************
 Módulo responsável pela resposta as requisições relacionadas aos resultados
*************************************************************************/
const path = require('path');
//Faz requires fora do módulo para que não seja importada novamente toda
//as vezes que a função exportada for utilizada
//Isto por que o modulo, uma vez importado, fica em cache
const ActionController = require('../controllers/ActionController');

module.exports = function(app) {
  //Instanciando controller
  let controller = new ActionController();

  //POST run solidity-coverage
  app.post('/Coverage', controller.coverage);

  //POST generate automated tests
  app.post('/GenerateTests', controller.generateTests);
};
