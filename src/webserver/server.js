/************************************************************************
 Módulo principal de launch da aplicação
*************************************************************************/

//constante que determina a porta
const porta = process.env.PORT || 3001;
const loaders = require('../loaders');
loaders.init();
//Importando módulo que permite pegar informações do servidor
let os = require('os');
//Importando o módulo de customização/configuração do middleware Express
let app = require('./custom-express')();
//Cria um servidor passando o middleware (webfilter) Express para que seja utilizado
let http = require('http').createServer(app);
//Atribui a instância app a um objeto global (tornando-o, consequentemente, global)
global.app = app;

//Levanta o servidor HTTP na porta 3001
var server = http.listen(porta, function () {
  let ifs = os.networkInterfaces();
  let result = Object.keys(ifs)
    .map((x) => [x, ifs[x].filter((x) => x.family === 'IPv4' && !x.internal)[0]])
    .filter((x) => x[1])
    .map((x) => x[1].address);

  let msg = 'Servidor ouvindo em http://' + result + ':' + porta;
  console.log('Path Projetos:', process.env.DEFAULT_SOLIDITY_DIRECTORY);
  console.log('Nível debug:', process.env.DEBUG_LEVEL);
  console.log(msg);
});
