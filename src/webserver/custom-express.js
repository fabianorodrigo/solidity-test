/************************************************************************
 Módulo de configuração/customização do framework Express
*************************************************************************/

let exp = require('express');
let bodyParser = require('body-parser');
const path = require('path');

module.exports = function() {
  let app = exp();
  //Definindo Engine de Templates
  //app.set('view engine', 'ejs');
  //Definindo localização de conteúdo estático (CSS, imagens, etc)
  app.use('/public', exp.static('./public'));
  app.use('/workdir', exp.static(path.resolve(__dirname, '../../workdir')));
  //This tells express to log via morgan
  //and morgan to log in the "combined" pre-defined format
  //app.use(morgan('combined'))
  //Ativando o middleware do bodyparser para facilitar o tratamento de POST de formulários
  app.use(bodyParser.urlencoded({ extended: true }));
  //Ativando o middleware do bodyparser para permitir recebimento de POST de JSON
  app.use(bodyParser.json());

  //Importando arquivo que define as rotas de api
  require('./routes/result')(app);
  require('./routes/action')(app);

  //Adicionando um middleware customizado que redireciona o usuário caso nenhuma das rotas acima seja encontrada
  app.use(function(req, res, next) {
    res.sendFile(path.resolve(__dirname, `./public/${req.url}`));
  });
  return app;
};
