const fs = require('fs');
const path = require('path');

class ResultController {
  constructor() {}

  list(req, res) {
    fs.readFile(
      path.resolve(__dirname, `../${process.env.WORKDIR || '../workdir'}/solidity-test-report.json`),
      { encoding: 'UTF8' },
      (err, data) => {
        res.format({
          json() {
            if (err) {
              res.json(err);
            } else {
              res.json(JSON.parse(data));
            }
          },
        });
      }
    );
  }

  contracts(req, res) {
    let project = req.body;
    const workdirProjectContracts = path.resolve(
      __dirname,
      `../${process.env.WORKDIR || 'workdir'}`,
      project.truffleProjectHome,
      'contracts'
    );
    if (!fs.existsSync(workdirProjectContracts)) {
      throw new Error(`${workdirProjectContracts} not found`);
    }
    try {
      res.json(readFilesDir(workdirProjectContracts));
    } catch (e) {
      res.status(500).json({ message: e.message });
    }
  }

  tests(req, res) {
    let project = req.body;
    const workdirProjectTests = path.resolve(
      __dirname,
      `../${process.env.WORKDIR || 'workdir'}`,
      project.truffleProjectHome,
      'test'
    );
    if (!fs.existsSync(workdirProjectTests)) {
      throw new Error(`${workdirProjectTests} not found`);
    }
    try {
      res.json(readFilesDir(workdirProjectTests));
    } catch (e) {
      res.status(500).json({ message: e.message });
    }
  }

  coverageLog(req, res) {
    let project = req.body;
    const workdirProject = path.resolve(
      __dirname,
      `../${process.env.WORKDIR || 'workdir'}`,
      project.truffleProjectHome
    );
    if (!fs.existsSync(workdirProject)) {
      throw new Error(`${workdirProject} not found`);
    }
    try {
      res.json({ 'coverage.out': fs.readFileSync(path.join(workdirProject, 'coverage.out'), { encoding: 'UTF8' }) });
    } catch (e) {
      res.status(500).json({ message: e.message });
    }
  }
}

function readFilesDir(dir) {
  const retorno = {};
  const contractFiles = fs.readdirSync(dir);
  contractFiles.forEach((f) => {
    const filePath = path.join(dir, f);
    if (fs.lstatSync(filePath).isDirectory()) {
      const subRetorno = readFilesDir(filePath);
      Object.keys(subRetorno).forEach((sr) => {
        retorno[`${f}/${path.basename(sr)}`] = subRetorno[sr];
      });
    } else if (!filePath.toLocaleLowerCase().endsWith('/migrations.sol')) {
      const data = fs.readFileSync(filePath, { encoding: 'UTF8' });
      retorno[f] = data;
    }
  });
  return retorno;
}

// Já exporta uma instância do controller
module.exports = ResultController;
