const fs = require('fs');
const net = require('net');
const { spawn } = require('child_process');
const path = require('path');
const CoverageFactory = require('../../services/CoverageFactory');
const ExceptionDrivenFactory = require('../../services/ExceptionDrivenFactory');
const shelljs = require('shelljs');
const { ganacheMneumonic } = require('../../services/ganache');

const GANACHE_PORT = 8545;
class ActionController {
  constructor() {}

  async coverage(req, res) {
    let project = req.body;
    const coverage = CoverageFactory({ prjBaseName: project.truffleProjectHome });
    try {
      const result = await coverage.run();
      res.format({
        json() {
          res.json(result);
        },
      });
    } catch (e) {
      res.json(e);
    }
  }

  async generateTests(req, res) {
    //res.status(500).json({ message: 'teste' });
    let ganache = null;
    let portAlreadyInUse = await isPortBeingUsed(GANACHE_PORT);
    if (!portAlreadyInUse) {
      ganache = spawn('ganache-cli', [
        '-i',
        '5777',
        '--gasLimit',
        '0x1FFFFFFFFFFFFF',
        '--gasPrice',
        '2',
        '-p',
        GANACHE_PORT,
        '--mnemonic',
        `'${ganacheMneumonic}'`,
      ]);
    }

    let project = req.body;
    const workdir = path.resolve(__dirname, `../${process.env.WORKDIR || '../workdir'}`);
    if (!fs.existsSync(workdir)) {
      throw new Error(`${workdir} not found`);
    }
    const REPORT_FILE_NAME = 'solidity-test-report.json';
    if (fs.existsSync(path.join(workdir, REPORT_FILE_NAME))) {
      global.generalResults = JSON.parse(fs.readFileSync(path.join(workdir, REPORT_FILE_NAME)));
    }
    const exceptionDriven = ExceptionDrivenFactory({
      truffleProjectHome: path.join(workdir, project.truffleProjectHome),
    });
    try {
      if (global.generalResults[project.truffleProjectHome] == null) {
        global.generalResults[project.truffleProjectHome] = {};
      }
      global.generalResults[project.truffleProjectHome].successfull = false;
      global.generalResults[project.truffleProjectHome].dhFinish = null;
      global.generalResults[project.truffleProjectHome].dhInit = new Date();
      await exceptionDriven.generateSequences();
      //killing ganache
      if (!portAlreadyInUse && ganache != null) {
        ganache.kill();
      }
      //collect return data
      global.generalResults[project.truffleProjectHome].dhFinish = new Date();
      global.generalResults[project.truffleProjectHome].successfull = true;
      shelljs.touch(path.join(workdir, project.truffleProjectHome, 'successfulFinished'));
      shelljs.cd(path.join(workdir, project.truffleProjectHome));
      fs.writeFileSync(path.join(workdir, REPORT_FILE_NAME), JSON.stringify(global.generalResults));
      res.format({
        json() {
          res.json({
            successfull: global.generalResults[project.truffleProjectHome].successfull,
            dhFinishTestFilesGeneration: global.generalResults[project.truffleProjectHome].dhFinishTestFilesGeneration,
            qtyTestedFunctions: global.generalResults[project.truffleProjectHome].qtyTestedFunctions,
            qtyTestedScenarios: global.generalResults[project.truffleProjectHome].qtyTestedScenarios,
            scenariosSummary: global.generalResults[project.truffleProjectHome].scenariosSummary,
          });
        },
      });
    } catch (e) {
      console.error(e);
      res.status(500).json({ message: e.message });
    }
  }
}

async function isPortBeingUsed(port) {
  const server = net.createServer();
  let used = false;
  server.once('error', function (err) {
    if (err.code === 'EADDRINUSE') {
      used = true;
    }
  });

  server.once('listening', function () {
    server.close();
  });
  await server.listen(port);
  return used;
}

// Já exporta uma instância do controller
module.exports = ActionController;
