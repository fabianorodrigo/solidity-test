import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListAltIcon from '@material-ui/icons/ListAlt';
import BuildIcon from '@material-ui/icons/Build';
import ShowChartIcon from '@material-ui/icons/ShowChart';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested0: {
    paddingLeft: theme.spacing(3),
  },
  nested1: {
    paddingLeft: theme.spacing(6),
  },
  nested2: {
    paddingLeft: theme.spacing(9),
  },
}));

export default function mainListItems(props) {
  const [aberto, setAberto] = React.useState([0]);
  const [selectedIndex, setSelectedIndex] = React.useState(-1);

  const handleToggle = (value) => {
    const currentIndex = aberto.indexOf(value);
    const newAberto = [...aberto];

    if (currentIndex === -1) {
      newAberto.push(value);
    } else {
      newAberto.splice(currentIndex, 1);
    }

    setAberto(newAberto);
  };

  const grupos = props.dados.GruposHierarquica ? Object.values(props.dados.GruposHierarquica) : [];
  const classes = useStyles();
  return (
    <div>
      <List
        onClick={(event) => {
          setSelectedIndex(-1);
          props.setGrupoIdFiltro('');
        }}
        component="nav"
        aria-labelledby="nested-list-subheader"
        className={classes.root}
      >
        <ListItem button onClick={props.onClick.bind(null, 'generateTestsAndrunCoverage')}>
          <ListItemIcon>
            <ListAltIcon />
          </ListItemIcon>
          <ListItemText primary="Gerar Testes e Medir" />
        </ListItem>
        <ListSubheader>{`Ações`}</ListSubheader>
        <ListItem disabled={props.dados.loading} button onClick={props.onClick.bind(null, 'generateTests')}>
          <ListItemIcon>
            <BuildIcon />
          </ListItemIcon>
          <ListItemText primary="Gerar Testes Automatizados" />
        </ListItem>

        <ListItem disabled={props.dados.loading} button onClick={props.onClick.bind(null, 'runCoverage')}>
          <ListItemIcon>
            <ShowChartIcon />
          </ListItemIcon>
          <ListItemText primary="Medir Cobertura" />
        </ListItem>
      </List>
    </div>
  );
}
