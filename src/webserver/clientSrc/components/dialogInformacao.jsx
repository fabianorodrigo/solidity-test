import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import React from 'react';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import { Transicao } from './Transicao';

export function DialogConfirmacao(props) {
  const { mensagem, detalhesMensagem, funcaoFecharCallback } = props;
  const fechar = (sim) => {
    //setOpen(false);
    if (funcaoFecharCallback) {
      funcaoFecharCallback(sim);
    }
  };

  return (
    <Dialog
      open={mensagem != null && mensagem.trim() != ''}
      onClose={fechar.bind(null, false)}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      TransitionComponent={Transicao}
    >
      <DialogTitle id="alert-dialog-title">
        <InfoOutlinedIcon fontSize="large" style={{ float: 'left' }} />
        {mensagem}
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {detalhesMensagem?.map((msg, i) => {
            const msgSplit = msg.split(':');
            return (
              <React.Fragment key={`msgDetalhe${i}`}>
                {msgSplit.length == 2 ? (
                  <React.Fragment>
                    {lb(msgSplit[0])} {msgSplit[1]}
                  </React.Fragment>
                ) : (
                  { msg }
                )}
                <br key={`BRmsgDetalhe${i}`} />
              </React.Fragment>
            );
          })}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={fechar.bind(null, null)} color="primary" autoFocus>
          OK
        </Button>
      </DialogActions>
    </Dialog>
  );
}

function lb(tx) {
  if (!tx.startsWith(' ')) {
    return <b>{tx}: </b>;
  } else {
    return (
      <span style={{ marginLeft: `${tx.indexOf(tx.trim()) * 5}px` }}>
        <b>{tx}: </b>
      </span>
    );
  }
}
