import React, { Component } from 'react';
import path from 'path';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import ReactMarkdown from 'react-markdown';
import CodeBlock from './CodeBlock';

/***
 * If you need a state in your component you will either need to create a class component or you
 * lift the state up to the parent component and pass it down the functional component via props.
 */
export default class FormContratos extends Component {
  constructor(props, context) {
    super(props, context);
    this.timeout = 0;
    this.state = {
      visible: false,
      title: '',
      project: '',
      files: {},
      value: 0,
    };
  }

  async abreForm(title, project, functionName) {
    this.setState({ value: 0, files: {}, title, project: path.basename(project) });
    const files = await this.props.app.controllers.Project[functionName]({ truffleProjectHome: project });
    this.setState({ visible: true, files });
  }

  fechaForm() {
    this.setState({ visible: false });
  }

  handleChange(event, newValue) {
    this.setState({ value: newValue });
  }

  a11yProps(index) {
    return {
      id: `scrollable-auto-tab-${index}`,
      'aria-controls': `scrollable-auto-tabpanel-${index}`,
    };
  }

  render() {
    return (
      <Dialog open={this.state.visible} onClose={this.fechaForm} aria-labelledby="form-dialog-title" maxWidth="lg">
        <DialogTitle id="form-dialog-title">
          {this.state.title}: {this.state.project}
        </DialogTitle>
        <DialogContent>
          <AppBar position="static" color="default">
            <Tabs
              value={this.state.value}
              onChange={this.handleChange.bind(this)}
              indicatorColor="primary"
              textColor="primary"
              variant="scrollable"
              scrollButtons="auto"
              aria-label="scrollable auto tabs example"
            >
              {Object.keys(this.state.files).map((file, i) => {
                return <Tab key={`tab${i}`} label={file} {...this.a11yProps(i)} />;
              })}
            </Tabs>
          </AppBar>
          {Object.keys(this.state.files).map((file, i) => {
            const markDownSource = '```javascript\n'.concat(this.state.files[file], '\n```');
            return (
              <TabPanel key={`tabPanel${i}`} value={this.state.value} index={i}>
                <ReactMarkdown source={markDownSource} renderers={{ code: CodeBlock }} />
              </TabPanel>
            );
          })}
        </DialogContent>
        <DialogActions>
          <Button onClick={this.fechaForm.bind(this)} color="primary">
            Fechar
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};
