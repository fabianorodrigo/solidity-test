import React, { useRef, Dispatch, useContext, useState } from 'react';
import { makeStyles, styled } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import ShowChartIcon from '@material-ui/icons/ShowChart';
import DescriptionIcon from '@material-ui/icons/Description';
import CodeIcon from '@material-ui/icons/Code';
import Tooltip from '@material-ui/core/Tooltip';
import Checkbox from '@material-ui/core/Checkbox';
import { DialogConfirmacao } from './dialogInformacao';
import { AppContext } from './AppContext';

import path from 'path';
import moment from 'moment';
import { TableFooter } from '@material-ui/core';

import PopupContratos from './PopupArquivos';

const useStyles = buildStyles();

export default function ListaProjetos(props) {
  const projects = props.app.getState('Projects');
  //hooks
  const { state, dispatch: appDispatch } = useContext(AppContext);

  const [allChecked, setAllChecked] = React.useState(false);
  const handleChangeAllChecked = (event) => {
    setAllChecked(event.target.checked);
  };
  //quando mudar o allChecked, atualiza a lista de projetos checked
  React.useEffect(() => {
    if (allChecked == false) {
      props.app.setState('selectedProjects', []);
    } else {
      const newChecked = Object.values(projects)
        .filter((p) => !p.ignore)
        .map((p) => {
          return p.project;
        });
      props.app.setState('selectedProjects', newChecked);
    }
  }, [allChecked]);

  const handleCheckProject = (value) => {
    const currentIndex = props.dados.selectedProjects.indexOf(value);
    const newChecked = [...props.dados.selectedProjects];
    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }
    //setChecked(newChecked);
    props.app.setState('selectedProjects', newChecked);
  };
  //mensagem a ser exibida na dialog
  const [mensagem, setMensagem] = React.useState(undefined);
  const [detalhesMensagem, setDetalhesMensagem] = React.useState([]);

  const popupContratos = useRef();

  //counters
  let totalContracts = 0;
  let totalProjectsGeneration = 0;
  let totalProjectsSuccessful = 0;
  let totalProjectsFailed = 0;
  let totalFunctionsTested = 0;
  let totalScenariosTested = 0;
  let totalProjectsCoverageSuccessful = 0;
  let totalProjectsCoverageFailed = 0;
  let benchmarkingOuMais = 0;
  let metadeBenchmarkingAcima = 0;
  let menosMetadeBenchmarking = 0;

  const classes = useStyles();

  if (projects == null) {
    return null;
  } else {
    const sortedProjects = Object.keys(projects).sort((a, b) => {
      const pba = path.basename(a).toLowerCase();
      const pbb = path.basename(b).toLowerCase();
      if (pba < pbb) return -1;
      if (pba > pbb) return 1;
      return 0;
    });
    return (
      <div className={classes.root}>
        <PopupContratos ref={popupContratos} app={props.app} />
        <Table className={classes.table} key={`tbProjects`} size="small" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <TableCell>
                <Checkbox
                  disabled={props.dados.loading}
                  checked={allChecked}
                  onChange={handleChangeAllChecked}
                  color="default"
                  size="small"
                  inputProps={{ 'aria-label': 'checkbox with small size' }}
                  style={{ padding: '0px' }}
                />
              </TableCell>
              <TableCell>Projeto</TableCell>
              <TableCell>Contratos *</TableCell>
              <TableCell>Bibliotecas</TableCell>
              <TableCell>Geração Testes</TableCell>
              <TableCell>Funções Testadas</TableCell>
              <TableCell>Cenários</TableCell>
              <TableCell>Medição Cobertura</TableCell>
              <TableCell>Cobertura Branches</TableCell>
              <TableCell>Média</TableCell>
              <TableCell>Benchmarking</TableCell>
              <TableCell></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {sortedProjects.map((prjName, iPrj) => {
              const p = projects[prjName];
              if (p.ignore == true) return;

              totalContracts += p.quantityContracts + (p.quantityLibraries ? p.quantityLibraries : 0);
              totalProjectsGeneration++;
              p.successfull ? totalProjectsSuccessful++ : totalProjectsFailed++;
              if (p.successfull) {
                totalFunctionsTested += p.qtyTestedFunctions ? p.qtyTestedFunctions : 0;
                totalScenariosTested += p.qtyTestedScenarios ? p.qtyTestedScenarios : 0;
                p.coverageSuccessful ? totalProjectsCoverageSuccessful++ : totalProjectsCoverageFailed++;
                p.duration = Math.round(moment(p.dhFinish).diff(moment(p.dhInit), 'minutes', true) * 100) / 100;

                //comparaão com benchmarking
                const percProj = p.coverage && p.coverage.Branches ? p.coverage.Branches.percent : 0;
                const percBench =
                  p.benchmarkingCoverage && p.benchmarkingCoverage.Branches
                    ? p.benchmarkingCoverage.Branches.percent
                    : 0;
                const razao = percBench > 0 ? percProj / percBench : Infinity;
                if (razao >= 1) benchmarkingOuMais++;
                else if (razao >= 0.5) metadeBenchmarkingAcima++;
                else menosMetadeBenchmarking++;
              }
              return (
                <TableRow key={`tr_${p.project}`} style={iPrj % 2 == 0 ? { backgroundColor: '#F0F0F0' } : {}}>
                  <TableCell>
                    <Checkbox
                      disabled={props.dados.loading}
                      checked={props.dados.selectedProjects.indexOf(p.project) > -1}
                      onChange={handleCheckProject.bind(null, p.project)}
                      color="default"
                      size="small"
                      value={p.project}
                      inputProps={{ 'aria-label': 'checkbox with small size' }}
                      style={{ padding: '0px' }}
                    />
                  </TableCell>
                  <TableCell>{path.basename(prjName)}</TableCell>
                  <TableCell align="center">
                    <Link
                      href="#"
                      onClick={() => {
                        popupContratos.current.abreForm('Contratos', p.project, 'getContracts');
                      }}
                    >
                      {p.quantityContracts}
                    </Link>
                  </TableCell>
                  <TableCell align="center">
                    <Link
                      href="#"
                      onClick={() => {
                        popupContratos.current.abreForm('Contratos', p.project, 'getContracts');
                      }}
                    >
                      {p.quantityLibraries}
                    </Link>
                  </TableCell>
                  <TableCell>
                    <svg height="20" width="25">
                      <circle
                        cx="10"
                        cy="10"
                        r="8"
                        stroke="black"
                        strokeWidth="1"
                        fill={
                          props.dados.loading && props.dados.selectedProjects.indexOf(p.project) > -1
                            ? 'gray'
                            : p.successfull
                            ? 'green'
                            : 'red'
                        }
                      />
                      Sorry, your browser does not support inline SVG.
                    </svg>
                    <Tooltip title={p.duration ? `Em ${p.duration} minutos` : ''}>
                      <span>{moment(p.dhFinishTestFilesGeneration).format('DD/MM/YY HH:mm')}</span>
                    </Tooltip>
                  </TableCell>
                  <TableCell align="center">
                    <Tooltip
                      title={`Contract: ${p.qtyContractTestedFunctions} - Library: ${p.qtyLibraryTestedFunctions}`}
                    >
                      <span>{p.qtyTestedFunctions}</span>
                    </Tooltip>
                  </TableCell>
                  <TableCell align="center">{summaryTestedScenarios(p)}</TableCell>
                  <TableCell>
                    {p.successfull && (
                      <svg height="20" width="25">
                        <circle
                          cx="10"
                          cy="10"
                          r="8"
                          stroke="black"
                          strokeWidth="1"
                          fill={
                            props.dados.loading && props.dados.selectedProjects.indexOf(p.project) > -1
                              ? 'gray'
                              : p.coverageSuccessful
                              ? isCoverageUptodate(p)
                                ? 'green'
                                : 'yellow'
                              : 'red'
                          }
                        />
                        Sorry, your browser does not support inline SVG.
                      </svg>
                    )}
                    <Link href={`workdir/${path.basename(p.project)}/coverage/index.html`} target="_blank">
                      {p.successfull ? moment(p.dhFinishCoverage).format('DD/MM/YY HH:mm') : ''}
                    </Link>
                  </TableCell>
                  <TableCell align="right">{coverageData(p.coverage, p.benchmarkingCoverage)}</TableCell>
                  <TableCell align="right">{coverageHistoryData(p.coverageHistory)}</TableCell>
                  <TableCell align="right">{coverageData(p.benchmarkingCoverage)}</TableCell>
                  <TableCell>
                    <Tooltip title="Visualizar arquivos de testes gerados">
                      <span>
                        <IconButton
                          disabled={props.dados.loading}
                          aria-label="view"
                          className={classes.margin}
                          size="small"
                          onClick={() => {
                            popupContratos.current.abreForm('Testes', p.project, 'getTests');
                          }}
                        >
                          <CodeIcon fontSize="inherit" />
                        </IconButton>
                      </span>
                    </Tooltip>
                    <Tooltip title="Visualizar log de execução">
                      <span>
                        <IconButton
                          disabled={props.dados.loading}
                          aria-label="coverage"
                          className={classes.margin}
                          size="small"
                          onClick={() => {
                            popupContratos.current.abreForm('Log', p.project, 'getCoverageLog');
                          }}
                        >
                          <DescriptionIcon fontSize="inherit" />
                        </IconButton>
                      </span>
                    </Tooltip>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
          <TableFooter>
            <TableRow>
              <TableCell colSpan={2}>Total</TableCell>
              <TableCell colSpan={2} align="center">
                {totalContracts}
              </TableCell>
              <TableCell align="center">
                {totalProjectsSuccessful}/{totalProjectsGeneration}
              </TableCell>
              <TableCell align="center">{totalFunctionsTested}</TableCell>
              <TableCell align="center">{totalScenariosTested}</TableCell>
              <TableCell align="center">
                {totalProjectsCoverageSuccessful}/{totalProjectsGeneration}
              </TableCell>
              <TableCell align="center" colSpan={2}>
                {`>= BENCHMARKING:`} {benchmarkingOuMais}
                <br />
                {`>= METADE BENCHMARKING:`} {metadeBenchmarkingAcima}
                <br />
                {`< METADE BENCHMARKING:`} {menosMetadeBenchmarking}
              </TableCell>
              <TableCell>-</TableCell>
            </TableRow>
          </TableFooter>
        </Table>
        * Contagem exclui contratos que não tenham funções e também aqueles que possuem alguma função sem corpo
        (contratos abstratos)
        {mensagem && (
          <DialogConfirmacao
            mensagem={mensagem}
            detalhesMensagem={detalhesMensagem}
            funcaoFecharCallback={async (sim) => {
              setMensagem(undefined);
              setDetalhesMensagem([]);
            }}
          />
        )}
      </div>
    );
  }

  function summaryTestedScenarios(project) {
    return (
      <Link
        href="#"
        onClick={() => {
          setMensagem(`Total de cenários de teste identificados ${project.qtyTestedScenarios}`);
          setDetalhesMensagem(getDetalhes(project.scenariosSummary));
        }}
      >
        {project.qtyTestedScenarios}
      </Link>
    );

    function getDetalhes(summary) {
      if (!summary) return [];
      const retorno = [];
      //contract
      retorno.push(`Contracts: ${summary.contract.total}`);
      Object.keys(summary.contract.contracts).forEach((contractName) => {
        retorno.push(`    ${contractName}: ${summary.contract.contracts[contractName].total}`);
        //contract functions
        Object.keys(summary.contract.contracts[contractName].functions).forEach((fName) => {
          const obj = summary.contract.contracts[contractName].functions[fName];
          retorno.push(`        ${fName}${obj.visibility ? ' '.concat(obj.visibility) : ''}: ${obj.total}`);
        });
      });
      //library
      retorno.push(`Libraries: ${summary.library.total}`);
      Object.keys(summary.library.contracts).forEach((libname) => {
        retorno.push(`    ${libname}: ${summary.library.contracts[libname].total}`);
        //library functions
        Object.keys(summary.library.contracts[libname].functions).forEach((fName) => {
          const obj = summary.library.contracts[libname].functions[fName];
          retorno.push(`        ${fName}${obj.visibility ? ' '.concat(obj.visibility) : ''}: ${obj.total}`);
        });
      });
      return retorno;
    }
  }
}

function coverageData(coverage, benchmarking, history) {
  if (coverage && coverage.Branches && coverage.Branches.percent != null) {
    return (
      <React.Fragment>
        <span
          style={{
            color: benchmarking != null && benchmarking.Branches.percent > coverage.Branches.percent ? 'red' : '',
          }}
        >
          {coverage.Branches.percent}%{' '}
          <sub>
            {coverage.Branches.covered}/{coverage.Branches.total}
            {history == null ? '' : `(${history.length})`}
          </sub>
        </span>
      </React.Fragment>
    );
  } else if (coverage && coverage.Branches) {
    return (
      <React.Fragment>
        <span>{coverage.Branches.percent}%</span>{' '}
        <sub>
          {coverage.Branches.covered}/{coverage.Branches.total}
          {history == null ? '' : `(${history.length})`}
        </sub>
      </React.Fragment>
    );
  }
}

function coverageHistoryData(history) {
  if (history && history.length > 0) {
    const media =
      history.map((cov) => parseInt(cov.Branches.covered)).reduce((a, b) => a + b) /
      history.map((cov) => parseInt(cov.Branches.total)).reduce((a, b) => a + b);

    return (
      <React.Fragment>
        <span>{(media * 100).toFixed(2)}%</span> <sub>{history.length}</sub>
      </React.Fragment>
    );
  }
}

function isCoverageUptodate(p) {
  return new Date(p.dhFinishCoverage).getTime() > new Date(p.dhFinishTestFilesGeneration).getTime();
}

function buildStyles() {
  return makeStyles((theme) => ({
    root: {
      width: '100%',
      maxWidth: '1800px',
      backgroundColor: theme.palette.background.paper,
      padding: '0px',
    },
    table: {
      display: 'block',
      backgroundColor: 'white',
      borderColor: 'black',
      border: '1px',
    },
    tooltip: {
      popper: {
        backgroundColor: 'yellow',
      },
      tooltip: {
        backgroundColor: 'yellow',
      },
    },
    processo: {
      display: 'block',
    },
    grupo: {
      color: 'black',
      fontWeight: 'bold',
    },
    title: {
      flexGrow: 1,
      textAlign: 'left',
    },
    table: {
      //display: 'block', com esta linha o 100% abaixo não preenchia a tela toda. Com sua remoção, ocupou todo o container pai
      width: '100%',
    },
    tdPassos: {
      paddingTop: '1px',
      paddingBottom: '1px',
    },
    nested: {
      paddingLeft: theme.spacing(4),
    },
    mesmoLinha: {
      display: 'inline-block',
      padding: '0px',
    },
  }));
}
