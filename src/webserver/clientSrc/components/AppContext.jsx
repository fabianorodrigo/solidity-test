import React, { createContext, ReactNode, useReducer } from 'react';

export const AppContext = createContext({
  state: { benchmarkingOuMais: 0, metadeBenchmarkingAcima: 0, menosMetadeBenchmarking: 0 },
  dispatch: () => null,
});

export const AppContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, {
    benchmarkingOuMais: 0,
    metadeBenchmarkingAcima: 0,
    menosMetadeBenchmarking: 0,
  });
  return <AppContext.Provider value={{ state, dispatch }}>{children}</AppContext.Provider>;
};

/**
 * A reducer function receives two arguments, the first one is the state, that we are passing when
 * using useReducer hook, and the second one is an object that represents that events and some data
 * that will change the state (action).
 * @param state
 * @param acao
 */
const reducer = (state, acao) => {
  const retorno = { ...state };
  switch (acao.tipo) {
    case 'ATUALIZAR': {
      retorno.benchmarkingOuMais = acao.benchmarkingOuMais;
      retorno.metadeBenchmarkingAcima = acao.metadeBenchmarkingAcima;
      retorno.menosMetadeBenchmarking = acao.menosMetadeBenchmarking;
      return retorno;
    }
    default:
      return state;
  }
};
