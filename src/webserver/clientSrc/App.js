import React, { Component } from 'react';
import Main from './components/main';
import ReactDOM from 'react-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import Snackbar from '@material-ui/core/Snackbar';
//Controller da aplicação
import AppController from './AppController';
import { AppContextProvider } from './components/AppContext';

//componentes
import Mensagem from './components/mensagem';
//import Popup from "./components/popup";

//serviços

class App extends Component {
  constructor(props, context) {
    super(props, context);
    this._appController = new AppController(this);

    this.state = {
      mensagemSnack: '',
      dados: {
        pagina: 1,
        loading: false,
        adminMode: false,
        selectedProjects: [],
      },
    };
  }

  componentDidMount() {
    //if (this.state.usuarioLogado) { TODO: Implementar essa budega
    this.cargaInicialDados();
    //}
  }

  async cargaInicialDados() {
    //Carga
    this._appController.cargaEntidades();
  }

  getStateDados(chave) {
    return this.state.dados[chave];
  }

  atualizaStateDados(chave, valor) {
    const dados = this.state.dados;
    dados[chave] = valor;
    this.setState({ dados: dados });
  }

  mostraMensagem(mensagemSnack, tipo, cb) {
    this.setState({ mensagemSnack });
    this.setState({ tipoSnack: tipo });
    if (cb) {
      cb();
    }
  }

  onLoginOut(logado) {
    this.setState({ usuarioLogado: logado });
    if (logado) {
      this.cargaInicialDados();
    }
  }

  render() {
    return (
      <AppContextProvider>
        <div className="App">
          <CssBaseline />
          <Snackbar
            anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
            key="snackBar"
            open={this.state.mensagemSnack !== ''}
            autoHideDuration={4500}
            onClose={() => {
              this.setState({ mensagemSnack: '' });
            }}
            ContentProps={{
              'aria-describedby': 'message-id',
            }}
            message={<span id="message-id">{this.state.mensagemSnack}</span>}
          />
          <div className="App-body">
            <Main app={this._appController} dados={this.state.dados} />
          </div>
        </div>
      </AppContextProvider>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));
