//serviços
import { Service } from '../lib/service';
import path from 'path';

class ProjectController {
  constructor(appController) {
    this.app = appController;
  }

  /**
   * Get the list of contracts and it source code
   * @param {string} truffleProjectHome Home directory of truffle project
   */
  async getContracts({ truffleProjectHome }) {
    this.app.setState('loading', true);
    try {
      const response = await Service.insert({
        nomeModeloPlural: 'Contracts',
        instancia: { truffleProjectHome: path.basename(truffleProjectHome) },
      });
      return response.data;
    } catch (e) {
      console.error(`Exceção capturada em 'getContracts'`, e);
      this.app.mostraMensagem(`Falha ao executar getContracts em ${truffleProjectHome}: ${e.data.message}`, 'error');
    } finally {
      this.app.setState('loading', false);
    }
  }

  /**
   * Get the list of test files and it source code
   * @param {string} truffleProjectHome Home directory of truffle project
   */
  async getTests({ truffleProjectHome }) {
    this.app.setState('loading', true);
    try {
      const response = await Service.insert({
        nomeModeloPlural: 'Tests',
        instancia: { truffleProjectHome: path.basename(truffleProjectHome) },
      });
      return response.data;
    } catch (e) {
      console.error(`Exceção capturada em 'getTests'`, e);
      this.app.mostraMensagem(`Falha ao executar getTests em ${truffleProjectHome}: ${e.data.message}`, 'error');
    } finally {
      this.app.setState('loading', false);
    }
  }

  /**
   * Get the list of contracts and it source code
   * @param {string} truffleProjectHome Home directory of truffle project
   */
  async getCoverageLog({ truffleProjectHome }) {
    this.app.setState('loading', true);
    try {
      const response = await Service.insert({
        nomeModeloPlural: 'CoverageLog',
        instancia: { truffleProjectHome: path.basename(truffleProjectHome) },
      });
      return response.data;
    } catch (e) {
      console.error(`Exceção capturada em 'getCoverageLog'`, e);
      this.app.mostraMensagem(`Falha ao executar getCoverageLog em ${truffleProjectHome}: ${e.data.message}`, 'error');
    } finally {
      this.app.setState('loading', false);
    }
  }

  /**
   * Execute the solidity-coverage for the project
   * @param {string} truffleProjectHome Home directory of truffle project to run solidity-coverage
   */
  async generateTests({ truffleProjectHome }) {
    this.app.setState('loading', true);
    try {
      const response = await Service.insert({
        nomeModeloPlural: 'GenerateTests',
        instancia: { truffleProjectHome: path.basename(truffleProjectHome) },
      });
      if (this.app.controllers.Project.mantemState) {
        const dadoState = this.app.getState('Projects');
        dadoState[truffleProjectHome].successfull = response.data.successfull;
        dadoState[truffleProjectHome].dhFinishTestFilesGeneration = response.data.dhFinishTestFilesGeneration;
        dadoState[truffleProjectHome].qtyTestedFunctions = response.data.qtyTestedFunctions;
        dadoState[truffleProjectHome].qtyTestedScenarios = response.data.qtyTestedScenarios;
        (dadoState[truffleProjectHome].scenariosSummary = response.data.scenariosSummary),
          this.app.setState('Projects', dadoState);
      }
      return response.data;
    } catch (e) {
      console.error(`Exceção capturada em 'generateTests'`, JSON.stringify(e.response));
      this.app.mostraMensagem(`Falha ao gerar testes para ${truffleProjectHome}: ${e.response.data.message}`, 'error');
      return false;
    } finally {
      this.app.setState('loading', false);
    }
  }

  /**
   * Execute the solidity-coverage for the project
   * @param {string} truffleProjectHome Home directory of truffle project to run solidity-coverage
   */
  async runCoverage({ truffleProjectHome, desmarcaApos = false }) {
    this.app.setState('loading', true);
    try {
      const response = await Service.insert({
        nomeModeloPlural: 'Coverage',
        instancia: { truffleProjectHome: path.basename(truffleProjectHome) },
      });
      if (this.app.controllers.Project.mantemState) {
        const dadoState = this.app.getState('Projects');
        dadoState[truffleProjectHome].coverageSuccessful = response.data.coverageSuccessful;
        dadoState[truffleProjectHome].dhFinishCoverage = response.data.dhFinishCoverage;
        dadoState[truffleProjectHome].coverage = response.data.coverage;
        this.app.setState('Projects', dadoState);
        //desmarca o projeto se executou com sucesso
        const selectedProjects = this.app.getState('selectedProjects');
        const currentIndex = selectedProjects.indexOf(truffleProjectHome);
        const newChecked = [...selectedProjects];
        if (desmarcaApos == true && currentIndex !== -1) {
          newChecked.splice(currentIndex, 1);
          this.app.setState('selectedProjects', newChecked);
        }
      }
      return response.data;
    } catch (e) {
      console.error(`Exceção capturada em 'runCoverage'`, e);
      this.app.mostraMensagem(
        `Falha ao executar solidity-coverage em ${truffleProjectHome}: ${e.data.message}`,
        'error'
      );
    } finally {
      this.app.setState('loading', false);
    }
  }
}

export default ProjectController;
