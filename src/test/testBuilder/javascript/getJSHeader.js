const path = require('path');
const chai = require('chai');
(assert = chai.assert), (should = chai.should());

const {
  getTruffleProject,
  getContractByName,
  getBOFromRequireCall,
  getFunctionByName,
  getRequireCalls,
  mini,
} = require('../../utils');

const { TestBuilder, getJSTestHeader } = require('../../../services/testBuilder');

describe('Test Builder - Javascript', function () {
  describe('Get Javascript Header', function () {
    it('Deve retornar somente o require de  "truffle-assertions" e a inicialização do teste "contract(...)=>{" quando não passar contratos', async function () {
      assert.equal(
        mini(getJSTestHeader([], 'olhaOteste')),
        'consttruffleAssert=require(\'truffle-assertions\');contract("olhaOteste",(accounts)=>{'
      );
    });

    it('Deve retornar o require de  "truffle-assertions" e artifacts.require dos contratos passados ', async function () {
      assert.equal(
        mini(getJSTestHeader([{ name: 'qualquer' }], 'olhaOteste')),
        'consttruffleAssert=require(\'truffle-assertions\');constqualquer=artifacts.require("qualquer");contract("olhaOteste",(accounts)=>{'
      );
    });
  });
});
