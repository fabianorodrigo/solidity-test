const chai = require('chai');
(assert = chai.assert), (should = chai.should());

const { mini, truffleProjectEmpty } = require('../../utils');

const { getUsingForStatementQuestionavel } = require('../../../services/testBuilder');

describe('Test Builder - Solidity', function () {
  describe('Get Using ... For ... Statement', function () {
    it('Deve lançar exceção se não for passado um contrato com kind "library"', async function () {
      chai
        .expect(
          getUsingForStatementQuestionavel.bind(null, truffleProjectEmpty, {
            kind: 'interface',
            contractDefinition: { subNodes: [] },
          })
        )
        .to.throw('getUsingForStatement has to receive a contractDefinition of kind "library"');
    });

    it('Não deve retornar nada se não for passada nenhuma função', async function () {
      assert.equal(mini(getUsingForStatementQuestionavel(truffleProjectEmpty, { kind: 'library', subNodes: [] })), '');
    });

    it('Deve retornar o statement "using library for type" para funções com visibilidade "internal" que tenham parâmetros e seu corpo seja diferente de null', async function () {
      assert.equal(
        mini(
          getUsingForStatementQuestionavel(truffleProjectEmpty, {
            kind: 'library',
            name: 'libraryUnitTest',
            subNodes: [
              {
                type: 'FunctionDefinition',
                body: {},
                visibility: 'internal',
                parameters: [{ typeName: { name: 'uint' } }],
              },
              {
                type: 'FunctionDefinition',
                body: {},
                visibility: 'external',
                parameters: [{ typeName: { name: 'uint' } }],
              },
              {
                type: 'FunctionDefinition',
                body: {},
                visibility: 'private',
                parameters: [{ typeName: { name: 'uint' } }],
              },
              {
                type: 'FunctionDefinition',
                body: {},
                visibility: 'public',
                parameters: [{ typeName: { name: 'uint' } }],
              },
              {
                type: 'FunctionDefinition',
                body: {},
                visibility: 'internal',
                parameters: [{ typeName: { name: 'baratas' } }],
              },
              { type: 'FunctionDefinition', body: {}, visibility: 'internal', parameters: [] },
            ],
          })
        ),
        'usinglibraryUnitTestforuint;usinglibraryUnitTestforbaratas;'
      );
    });
  });
});
