const chai = require('chai');
(assert = chai.assert), (should = chai.should());

const { truffleProjectEmpty, mini } = require('../../utils');

const { getSolidityHeader } = require('../../../services/testBuilder/solidity/getSolidityHeader');

describe('Test Builder - Solidity', function () {
  describe('Get Solidity Header', function () {
    it('Para solidity-coverage@0.6.7, deve retornar somente o import do Assert.sol do próprio Truffle quando não passar contratos', async function () {
      assert.equal(mini(getSolidityHeader(truffleProjectEmpty, [])), 'import"truffle/Assert.sol";');
    });

    it('Para solidity-coverage@0.6.7, Deve retornar o import do Assert.sol do próprio Truffle e, com caminho relativizado para subdiretório "tests", dos contratos passados ', async function () {
      console.log(truffleProjectEmpty.truffleProjectHome);
      assert.equal(
        mini(getSolidityHeader(truffleProjectEmpty, [{ solFilePath: 'qualquer.sol' }])),
        `import"truffle/Assert.sol";import"../../../../qualquer.sol";`
      );
    });
  });
});
