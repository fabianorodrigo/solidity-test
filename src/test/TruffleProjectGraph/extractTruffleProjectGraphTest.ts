import path from 'path';
import chai from 'chai';
import 'mocha';
import { extractTruffleProjectGraph } from '../../services/TruffleProjectGraph';
import { ITruffleProjectGraph } from '../../models/graph';
import { SourceVariableSetType } from '../../models/graph/SourceVariableSetType';
import { TruffleProjectIceFactory } from '../../services/TruffleFactory';

const assert = chai.assert;
const expect = chai.expect;

describe('Truffle Project Graph', function () {
  let grafo: ITruffleProjectGraph = null;

  before(function () {
    this.timeout(5000);
    const truffleProject = TruffleProjectIceFactory({
      truffleProjectHome: path.resolve('./dist/test/'),
    });
    grafo = extractTruffleProjectGraph(truffleProject);
  });

  describe('Contratos', function () {
    it('Deve retornar todos os contratos do projeto Truffle', async function () {
      assert.equal(Object.keys(grafo.contracts).length, 10);
      assert.equal(grafo.contracts['RequireUse'].name, 'RequireUse');
    });
  });
  describe('Funções, parâmetros e retorno', function () {
    it('Deve retornar todas as funções, parâmetros de entrada e parâmetros de saída (return)', async function () {
      //são 34 funções, mas a setDecimals é overloaded
      assert.equal(Object.keys(grafo.contracts['RequireUse'].functions).length, 33);
      Object.keys(grafo.contracts['RequireUse'].functions).forEach((functionName) => {
        //duas funções têm sobreposição: setDecimals e setName
        if (functionName == 'setDecimals' || functionName == 'setName') {
          assert.equal(grafo.contracts['RequireUse'].functions[functionName].length, 2);
        } else {
          assert.equal(grafo.contracts['RequireUse'].functions[functionName].length, 1);
        }
      });
      let f = 'constructor';
      //########################################## FUNÇÕES, PARÂMETROS E RETORNO ##################################
      assert.equal(grafo.contracts['RequireUse'].functions[f][0].parameters.length, 3);
      f = 'simpleRequires';
      assert.equal(grafo.contracts['RequireUse'].functions[f][0].parameters.length, 4);
      assert.equal(grafo.contracts['RequireUse'].functions[f][0].return.length, 1);
      assert.equal(grafo.contracts['RequireUse'].functions[f][0].parameters[0].name, 'recipient');
      assert.equal(grafo.contracts['RequireUse'].functions[f][0].parameters[0].type, 'address');
      assert.equal(grafo.contracts['RequireUse'].functions[f][0].parameters[1].name, 'value1');
      assert.equal(grafo.contracts['RequireUse'].functions[f][0].parameters[1].type, 'uint');
      assert.equal(grafo.contracts['RequireUse'].functions[f][0].parameters[2].name, 'value2');
      assert.equal(grafo.contracts['RequireUse'].functions[f][0].parameters[2].type, 'uint');
      assert.equal(grafo.contracts['RequireUse'].functions[f][0].parameters[3].name, 'value3');
      assert.equal(grafo.contracts['RequireUse'].functions[f][0].parameters[3].type, 'uint');
      expect(grafo.contracts['RequireUse'].functions[f][0].return[0].name).to.be.null;
      assert.equal(grafo.contracts['RequireUse'].functions[f][0].return[0].type, 'string');
      //requireWithConditionBasedOnStructMemberEnum
      assert.equal(
        grafo.contracts['RequireUse'].functions['requireWithConditionBasedOnStructMemberEnum'][0].parameters.length,
        1
      );
      assert.equal(
        grafo.contracts['RequireUse'].functions['requireWithConditionBasedOnStructMemberEnum'][0].return.length,
        0
      );
      assert.equal(
        grafo.contracts['RequireUse'].functions['requireWithConditionBasedOnStructMemberEnum'][0].parameters[0].name,
        'structWithEnum'
      );
      assert.equal(
        grafo.contracts['RequireUse'].functions['requireWithConditionBasedOnStructMemberEnum'][0].parameters[0].type,
        'RestrictionTypesTest.StructTeste'
      );
      //usingOraclize.stra2cbor
      assert.equal(grafo.contracts['usingOraclize'].functions['stra2cbor'][0].parameters.length, 1);
      assert.equal(grafo.contracts['usingOraclize'].functions['stra2cbor'][0].parameters[0].name, '_arr');
      assert.equal(grafo.contracts['usingOraclize'].functions['stra2cbor'][0].parameters[0].type, 'string[]');
    });
  });
  describe('Variáveis de estado', function () {
    it('Deve retornar todas as variáveis de estado', async function () {
      assert.equal(Object.keys(grafo.contracts['RequireUse'].stateVariables).length, 7);
      //simpleFunctionCallThatHasRequires
      assert.equal(grafo.contracts['RequireUse'].stateVariables['_balances'].isConstant, false);
      assert.equal(grafo.contracts['RequireUse'].stateVariables['_balances'].visibility, 'private');
    });
  });
  describe('Variáveis de estado referenciadas dentro das funções', function () {
    function asserts(
      contractName: string,
      functionName: string,
      stName: string,
      sourceName: string,
      qtyFunctionsNameWrite: number,
      qtyFunctionsSignatureWrite: number,
      conditional = true,
      sourceType: SourceVariableSetType = SourceVariableSetType.parameter,
      aditionalInfo?: string
    ) {
      assert.equal(
        Object.keys(grafo.contracts[contractName].stateVariables[stName].functionsWrite).length,
        qtyFunctionsNameWrite,
        `${contractName}.${functionName}.${stName}`
      );
      assert.equal(
        sumArrayLenghts(Object.values(grafo.contracts[contractName].stateVariables[stName].functionsWrite) as []),
        qtyFunctionsSignatureWrite
      );
      assert.isNotNull(
        grafo.contracts[contractName].stateVariables[stName].functionsWrite[functionName],
        `${contractName}.${functionName}.${stName}`
      );
      assert.equal(
        grafo.contracts[contractName].functions[functionName][0].stateVariablesWritten[stName].source.name,
        sourceName,
        `${contractName}.${functionName}.${stName}`
      );
      assert.equal(
        grafo.contracts[contractName].functions[functionName][0].stateVariablesWritten[stName].conditional,
        conditional,
        `${contractName}.${functionName}.${stName}`
      );
      assert.equal(
        grafo.contracts[contractName].functions[functionName][0].stateVariablesWritten[stName].source.type,
        sourceType,
        `${contractName}.${functionName}.${stName}`
      );
      if (aditionalInfo) {
        assert.equal(
          grafo.contracts[contractName].functions[functionName][0].stateVariablesWritten[stName].source.aditionalInfo,
          aditionalInfo,
          `${contractName}.${functionName}.${stName}`
        );
      }
    }

    it('Deve retornar todas as variáveis de estado do contrato setadas com valores de parâmetros das funções incondicionalmente (chama função que não tem require)', async function () {
      let c = 'RequireUse';
      let f = 'constructor';
      //############# CONSTRUTOR ##############
      asserts(c, f, '_name', 'name', 2, 3, false);
      asserts(c, f, '_symbol', 'symbol', 2, 2, false);
      asserts(c, f, '_decimals', 'decimals', 2, 2, false);
    });
    it('Deve retornar todas as variáveis de estado do contrato setadas com valores de parâmetros das funções condicionalmente', async function () {
      //############# simpleRequireCompareInteger ##############
      let c = 'RequireUse';
      let f = 'simpleRequireCompareInteger';
      asserts(c, f, 'testeState', 'teste', 6, 6);
    });
    it('Deve retornar todas as variáveis de estado do contrato setadas com variável de estado do tipo Mapping das funções', async function () {
      //############# simpleFunctionCallThatHasRequires ##############
      let c = 'RequireUse';
      let f = 'simpleFunctionCallThatHasRequires';
      asserts(c, f, 'testeState', '_balances', 6, 6, true, SourceVariableSetType.others);
    });
    it('Deve retornar todas as variáveis de estado do contrato setadas com membro de parâmetro de entrada das funções', async function () {
      //############# stateVariableWrittenConditionally ##############
      let c = 'RequireUse';
      let f = 'stateVariableWrittenConditionally';
      asserts(c, f, 'testeState', 'structWithEnum', 6, 6, true, SourceVariableSetType.parameterMemberAccess, 'a');
    });
    it('Deve retornar todas as variáveis de estado do contrato setadas dentro de ELSE com transactionParameter das funções', async function () {
      //############# stateVariableWrittenConditionallyInFalseBody ##############
      let c = 'RequireUse';
      let f = 'stateVariableWrittenConditionallyInFalseBody';
      asserts(c, f, 'testeState', 'msg', 6, 6, true, SourceVariableSetType.transactionParameter, 'value');
    });
    it('Deve retornar todas as variáveis de estado do contrato setadas dentro de ELSEIF com literal dentro das funções', async function () {
      //############# stateVariableWrittenConditionallyIfElseifElse ##############
      let c = 'RequireUse';
      let f = 'stateVariableWrittenConditionallyIfElseifElse';
      asserts(c, f, 'testeState', '', 6, 6, true, SourceVariableSetType.literal, '10');
    });
    it('Quando duas funções com mesmo nome (sobreposição) e ambas escrevem na variável de estado, todas devem ser retornadas', async function () {
      let c = 'RequireUse';
      let f = 'setName';
      //construtor mais duas funções 'setName' que alteram a '_name'
      asserts(c, f, '_name', 'n', 2, 3, false, SourceVariableSetType.parameter, 'Identifier');
    });
    it('Quando duas funções com mesmo nome (sobreposição) e apenas uma escreve na variável de estado, apenas aquela que escreve deve ser retornada', async function () {
      let c = 'RequireUse';
      let f = 'setDecimals';
      asserts(c, f, '_decimals', 'd', 2, 2, false, SourceVariableSetType.parameter, 'Identifier');
    });
  });
  describe('Chamadas de função', function () {
    it('Deve retornar todas as chamadas de função', async function () {
      Object.keys(grafo.contracts['RequireUse'].functions).forEach((functionName) => {
        //o construtor, mais um conjunto de funções e a uma das assinaturas de setDecimals têm chamadas a outras funções, as demais, não
        if (
          ['constructor', 'simpleFunctionCall', 'functionCallInsideELSEIF', 'functionCallInsideELSE'].includes(
            functionName
          )
        ) {
          assert.equal(
            Object.keys(grafo.contracts['RequireUse'].functions[functionName][0].calledFunctions).length,
            2,
            functionName
          );
        } else if (
          [
            'simpleFunctionCallThatHasRequires',
            'simpleFunctionCallThatHasRequiresTwiceTheSameParam',
            'requireWithConditionBasedOnStructMemberEnum',
            'stateVariableWrittenConditionallyInFalseBody',
            'stateVariableWrittenConditionallyIfElseifElse',
            'simpleFunctionCall',
            'simpleFunctionCallLiteral',
            'nestedFunctionCall',
            'nestedNestedFunctionCall',
            'functionCallInsideIF',
            'functionCallonIFConditional',
            'getStructTeste',
          ].includes(functionName)
        ) {
          assert.equal(
            Object.keys(grafo.contracts['RequireUse'].functions[functionName][0].calledFunctions).length,
            1,
            functionName
          );
        } else if (functionName == 'setDecimals') {
          const setDecimailsArray = grafo.contracts['RequireUse'].functions[functionName];
          setDecimailsArray.forEach((f) => {
            //somente a função setDecimals com 2 parâmetros de entrada que faz chamada
            //a outras funções: uma dentro do próprio contrato e outra do contrato pai
            if (f.parameters.length == 2) {
              assert.equal(Object.keys(f.calledFunctions).length, 2, functionName);
            } else {
              assert.equal(Object.keys(f.calledFunctions).length, 0, functionName);
            }
          });
        } else {
          grafo.contracts['RequireUse'].functions[functionName].forEach((f) => {
            //nenhuma função chama outra
            assert.equal(Object.keys(f.calledFunctions).length, 0, functionName);
          });
        }
      });
    });
    it('Deve retornar chamadas de funções pertencentes ao próprio contrato e funções do contrato pai', async function () {
      let c = 'RequireUse';
      let f = 'setDecimals';
      const func = grafo.contracts['RequireUse'].functions['setDecimals'].find(
        (f) => f.signature == 'setDecimals(uint8,string)'
      );
      //chama setName e RestrictionTypesTest.noRestriction
      assert.equal(Object.keys(func.calledFunctions).length, 2);
      assert.equal(func.calledFunctions['setName'][0].contract.name, 'RequireUse');
      assert.equal(func.calledFunctions['setName'][0].signature, 'setName(string)');
      assert.equal(func.calledFunctions['noRestriction'][0].contract.name, 'RestrictionTypesTest');
      assert.equal(func.calledFunctions['noRestriction'][0].signature, 'noRestriction(string)');
    });
  });
});

function sumArrayLenghts(a: []) {
  let r = 0;
  a.forEach((ai) => {
    r += (ai as any).length;
  });
  return r;
}
