import path from 'path';
import chai from 'chai';
import 'mocha';
import { fillsRestrictions } from '../../services/Restrictions';

import {
  r_eq_number7,
  r_uneq_number17,
  r_uneq_number7,
  r_geq_number7,
  r_geq_number17,
  r_leq_number7,
  r_leq_number17,
  r_gre_number7,
  r_gre_number17,
  r_les_number7,
  r_les_number17,
} from '../restrictionsConstants';

const assert = chai.assert;

import {
  getTruffleProject,
  getContractByName,
  getBOFromRequireCall,
  getFunctionByName,
  getRequireCalls,
} from '../utils';

describe('Restriction Factory: fillsOperationRestriction', function () {
  describe('Apenas uma restrição', function () {
    it('Quando array de restrições é NULL, sempre atende', async function () {
      assert.isTrue(fillsRestrictions(0, null));
      assert.isTrue(fillsRestrictions(null, null));
    });
    it('Quando array de restrições está vazio, sempre atende', async function () {
      assert.isTrue(fillsRestrictions(0, []));
      assert.isTrue(fillsRestrictions(null, []));
    });
    it('Valor inteiro, Operador ==', async function () {
      assert.isTrue(fillsRestrictions(7, [r_eq_number7]));
      assert.isFalse(fillsRestrictions(5, [r_eq_number7]));
    });
    it('Valor inteiro, Operador !=', async function () {
      assert.isTrue(fillsRestrictions(7, [r_uneq_number17]));
      assert.isFalse(fillsRestrictions(7, [r_uneq_number7]));
    });
    it('Valor inteiro, Operador >=', async function () {
      assert.isTrue(fillsRestrictions(17, [r_geq_number7]));
      assert.isFalse(fillsRestrictions(6, [r_geq_number7]));
    });
    it('Valor string, Operador >=', async function () {
      assert.isTrue(fillsRestrictions('7', [r_geq_number7]));
      assert.isFalse(fillsRestrictions('6', [r_geq_number7]));
    });
    it('Valor string forcing numericalValue, Operador >=', async function () {
      assert.isTrue(fillsRestrictions('17', [r_geq_number7], true));
      assert.isFalse(fillsRestrictions('7', [r_geq_number17], true));
    });
    it('Valor inteiro, Operador <=', async function () {
      assert.isTrue(fillsRestrictions(7, [r_leq_number7]));
      assert.isFalse(fillsRestrictions(8, [r_leq_number7]));
    });
    it('Valor string, Operador <= (vai olhar para o valor numérico em obj)', async function () {
      assert.isFalse(fillsRestrictions('17', [r_leq_number7]));
      assert.isFalse(fillsRestrictions('8', [r_leq_number7]));
    });
    it('Valor string forcing numericalValue, Operador <=', async function () {
      assert.isTrue(fillsRestrictions('7', [r_leq_number17], true));
      assert.isFalse(fillsRestrictions('18', [r_leq_number7], true));
    });
    it('Valor inteiro, Operador >', async function () {
      assert.isTrue(fillsRestrictions(8, [r_gre_number7]));
      assert.isFalse(fillsRestrictions(6, [r_gre_number7]));
    });
    it('Valor string, Operador > (vai olhar para o valor numérico em obj)', async function () {
      assert.isTrue(fillsRestrictions('8', [r_gre_number7]));
      assert.isTrue(fillsRestrictions('60', [r_gre_number7]));
    });
    it('Valor string forcing numericalValue, Operador >', async function () {
      assert.isTrue(fillsRestrictions('18', [r_gre_number7], true));
      assert.isFalse(fillsRestrictions('7', [r_gre_number17], true));
    });
    it('Valor inteiro, Operador <', async function () {
      assert.isTrue(fillsRestrictions(6, [r_les_number7]));
      assert.isFalse(fillsRestrictions(8, [r_les_number7]));
    });
    it('Valor string, Operador < (vai olhar para o valor numérico em obj)', async function () {
      assert.isFalse(fillsRestrictions('60', [r_les_number7]));
      assert.isTrue(fillsRestrictions('8', [r_les_number17]));
    });
    it('Valor string forcing numericalValue, Operador <', async function () {
      assert.isTrue(fillsRestrictions('6', [r_les_number17], true));
      assert.isFalse(fillsRestrictions('70', [r_les_number17], true));
    });
  });
});
