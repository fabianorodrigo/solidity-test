import path from 'path';
import chai from 'chai';
import 'mocha';
import { getFunctionParameterRestriction } from '../../services/Restrictions';
import { RestrictionElementType } from '../../models/RestrictionElementType';

const assert = chai.assert;

import {
  getTruffleProject,
  getContractByName,
  getBOFromRequireCall,
  getFunctionByName,
  getRequireCalls,
  truffleProjectEmpty,
} from '../utils';
import { TruffleProjectIceFactoryFunction } from '../../services/TruffleFactory';
import { extractTruffleProjectGraph } from '../../services/TruffleProjectGraph';

describe('Restriction Factory: getFunctionParameterRestriction', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../'));
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  this.timeout(5000);
  describe('Require comparando literais', function () {
    it('Função com apenas um require deve retornar apenas uma restrição', async function () {
      const contracts = truffleProject.getContracts();
      const contractDefinition = getContractByName(contracts, 'RequireUse');
      const functionUT = getFunctionByName(contractDefinition, 'simpleRequireCompareInteger');
      const paramName = 'teste';
      const result = getFunctionParameterRestriction(truffleProject, [functionUT], contractDefinition, null, {});
      assert.isArray(result);
      assert.equal(result.length, 1);
      assert.equal(result[0].operator, '==');
      assert.equal(result[0].right.value.js, 1979);
      assert.equal(result[0].left.value.js, paramName);
      assert.equal(result[0].right.elementType, RestrictionElementType.Literal);
      assert.equal(result[0].left.elementType, RestrictionElementType.functionParameter);
    });

    it('Função com vários requires aninhados em statements diferentes deve retornar todas as restrições', async function () {
      const contracts = truffleProject.getContracts();
      const contractDefinition = getContractByName(contracts, 'RequireUse');
      const functionUT = getFunctionByName(contractDefinition, 'simpleRequires');

      const result = getFunctionParameterRestriction(truffleProject, [functionUT], contractDefinition, null, {});
      assert.isArray(result);
      assert.equal(result.length, 4);
      assert.equal(result[0].operator, '>');
      assert.equal(result[0].right.value.js, 0);
      assert.equal(result[0].left.value.js, 'value1');
      assert.equal(result[0].right.elementType, RestrictionElementType.Literal);
      assert.equal(result[0].left.elementType, RestrictionElementType.functionParameter);

      assert.equal(result[1].operator, '>');
      assert.equal(result[1].right.value.js, 0);
      assert.equal(result[1].left.value.js, 'value2');
      assert.equal(result[1].right.elementType, RestrictionElementType.Literal);
      assert.equal(result[1].left.elementType, RestrictionElementType.functionParameter);

      assert.equal(result[2].operator, '<');
      assert.equal(result[2].right.value.js, 1000);
      assert.equal(result[2].left.value.js, 'value3');
      assert.equal(result[2].right.elementType, RestrictionElementType.Literal);
      assert.equal(result[2].left.elementType, RestrictionElementType.functionParameter);

      assert.equal(result[3].operator, '!=');
      assert.equal(result[3].right.value.js, '0x0000000000000000000000000000000000000000');
      assert.equal(result[3].left.value.js, 'recipient');
      assert.equal(result[3].right.elementType, RestrictionElementType.Literal);
      assert.equal(result[3].left.elementType, RestrictionElementType.functionParameter);
    });

    it('Função que chama outra função que contém os requires para o parâmetro solicitado e que também repassa msg.sender e um literal', async function () {
      const contracts = truffleProject.getContracts();
      const contractDefinition = getContractByName(contracts, 'RequireUse');
      const functionUT = getFunctionByName(contractDefinition, 'simpleFunctionCallThatHasRequires');
      const result = getFunctionParameterRestriction(truffleProject, [functionUT], contractDefinition, null, {});
      assert.isArray(result);
      assert.equal(result.length, 4, JSON.stringify(result));
      assert.equal(result[0].operator, '!=');
      assert.equal(result[0].right.value.js, '0x0000000000000000000000000000000000000000');
      assert.equal(result[0].left.value.js, 'sender');
      assert.equal(result[0].right.elementType, RestrictionElementType.Literal);
      assert.equal(result[0].left.elementType, RestrictionElementType.transactionParameter);

      assert.equal(result[1].operator, '!=');
      assert.equal(result[1].right.value.js, '0x0000000000000000000000000000000000000000');
      assert.equal(result[1].left.value.js, 'recipient');
      assert.equal(result[1].right.elementType, RestrictionElementType.Literal);
      assert.equal(result[1].left.elementType, RestrictionElementType.functionParameter);

      assert.equal(result[2].operator, '<=');
      assert.equal(result[2].right.value.js, 1979);
      assert.equal(result[2].left.value.js, 'value1');
      assert.equal(result[2].right.elementType, RestrictionElementType.Literal);
      assert.equal(result[2].left.elementType, RestrictionElementType.functionParameter);

      assert.equal(result[3].operator, '>');
      assert.equal(result[3].right.value.js, 16);
      assert.equal(result[3].left.value.js, '2');
      assert.equal(result[3].right.elementType, RestrictionElementType.Literal);
      assert.equal(result[3].left.elementType, RestrictionElementType.Literal);
    });

    it('Função que chama outra função que contém os requires para o parâmetro solicitado passando-o para mais de um argumento e que também repassa msg.sender', async function () {
      const contracts = truffleProject.getContracts();
      const contractDefinition = getContractByName(contracts, 'RequireUse');
      const functionUT = getFunctionByName(contractDefinition, 'simpleFunctionCallThatHasRequiresTwiceTheSameParam');
      const result = getFunctionParameterRestriction(truffleProject, [functionUT], contractDefinition, null, {});
      assert.isArray(result);
      assert.equal(result.length, 4);
      assert.equal(result[0].operator, '!=');
      assert.equal(result[0].right.value.js, '0x0000000000000000000000000000000000000000');
      assert.equal(result[0].right.elementType, RestrictionElementType.Literal);
      assert.equal(result[0].left.value.js, 'sender');
      assert.equal(result[0].left.elementType, RestrictionElementType.transactionParameter);

      assert.equal(result[1].operator, '!=');
      assert.equal(result[1].right.value.js, '0x0000000000000000000000000000000000000000');
      assert.equal(result[1].right.elementType, RestrictionElementType.Literal);
      assert.equal(result[1].left.value.js, 'recipient');
      assert.equal(result[1].left.elementType, RestrictionElementType.functionParameter);

      assert.equal(result[2].operator, '<=');
      assert.equal(result[2].right.value.js, 1979);
      assert.equal(result[2].left.value.js, 'value1');
      assert.equal(result[2].right.elementType, RestrictionElementType.Literal);
      assert.equal(result[2].left.elementType, RestrictionElementType.functionParameter);

      assert.equal(result[3].operator, '>');
      assert.equal(result[3].right.value.js, 16);
      assert.equal(result[3].left.value.js, 'value1');
      assert.equal(result[3].right.elementType, RestrictionElementType.Literal);
      assert.equal(result[3].left.elementType, RestrictionElementType.functionParameter);
    });
  });
  describe('Require structs e enums', () => {
    it('Deve retornar uma restrição para um require que compara atributo de parãmetro de entrada struct', async function () {
      const contracts = truffleProject.getContracts();
      const contractDefinition = getContractByName(contracts, 'RequireUse');
      const functionUT = getFunctionByName(contractDefinition, 'requireWithConditionBasedOnStructMember');
      const paramName = 'value1';
      const result = getFunctionParameterRestriction(truffleProject, [functionUT], contractDefinition, null, {});
      assert.isArray(result);
      assert.equal(result.length, 1);
      assert.equal(result[0].operator, '>');
      assert.equal(result[0].right.value.js, 100);
      assert.equal(result[0].right.value.sol, 100);
      assert.equal(result[0].left.value.js, paramName);
      assert.equal(result[0].left.value.memberName, 'a');
    });

    it('Deve retornar uma restrição para uma função interna que recebe um enum que é membro de um struct que é passado como parâmetro de entrada', async function () {
      const contracts = truffleProject.getContracts();
      const contractDefinition = getContractByName(contracts, 'RequireUse');
      const functionUT = getFunctionByName(contractDefinition, 'requireWithConditionBasedOnStructMemberEnum');
      const paramName = 'structWithEnum';
      const result = getFunctionParameterRestriction(truffleProject, [functionUT], contractDefinition, null, {});
      assert.isArray(result);
      assert.equal(result.length, 1);
      assert.equal(result[0].operator, '==');
      assert.equal(result[0].right.value.js, 'RequireUse.P');
      assert.equal(result[0].right.value.memberName, 'c');
      assert.equal(result[0].right.value.sol, 'RequireUse.P');
      assert.equal(result[0].left.value.js, paramName);
      assert.equal(result[0].left.value.memberName, 'b');
    });
  });
});
