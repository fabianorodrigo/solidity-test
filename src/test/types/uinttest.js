const path = require('path');
const chai = require('chai');
(assert = chai.assert), (should = chai.should());

const { Types } = require('../../services/types');
const uint = Types.uint;

describe('Type UINT - 256 bits', function () {
  it('Gerar novo número inteiro sem restrições', async function () {
    const valor = uint.getNewInteger([100000, 10, 0, 100001, 99999, 11, 9, 1, -1], []);
    assert.isNotNaN(valor);
    assert.isAtMost(parseInt(valor), Number.MAX_SAFE_INTEGER); //apesar do uint ser 256 bits, o Javascript suporta no máximo 53
  });
  it('Gerar novo número inteiro com restrição cujo valor não está no array deveria voltar o próprio valor da restrição', async function () {
    const valor = uint.getNewInteger(
      [100000, 10, 0, 100001, 99999, 11, 9, 1, -1],
      [{ right: { value: { js: '1979', obj: 1979 } } }]
    );
    assert.isNotNaN(valor);
    assert.equal(valor, 1979);
  });
  it('Gerar novo número inteiro com restrição cujo valor está no array mas o seu próximo superior deveria voltar o valor da restrição + 1', async function () {
    const valor = uint.getNewInteger(
      [100000, 10, 0, 100001, 99999, 11, 9, 1, -1],
      [{ right: { value: { js: '1', obj: 1 } } }]
    );
    assert.isNotNaN(valor);
    assert.equal(valor, 2);
  });
  it('Gerar novo número inteiro com restrição cujo valor está no array, bem como seu próximo superior, mas o anterior não está deveria voltar o valor da restrição - 1', async function () {
    const valor = uint.getNewInteger(
      [100000, 10, 0, 100001, 99999, 11, 9, 1, -1],
      [{ right: { value: { js: '9', obj: 9 } } }]
    );
    assert.isNotNaN(valor);
    assert.equal(valor, 8);
  });
});
