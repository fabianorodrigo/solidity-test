import path from 'path';
import chai from 'chai';
import 'mocha';
import { TruffleProjectIceFactoryFunction } from '../../services/TruffleFactory';
import { IContract } from '../../models/IContract';
import { IntegerFactory } from '../../services/types/IntegerFactory';

const assert = chai.assert;
const expect = chai.expect;

import { getTruffleProject } from '../utils';

const { Types } = require('../../services/types');
const uint = Types.uint;

describe('IntegerFactory', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contracts: IContract[];

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../'));
    contracts = truffleProject.getContracts();
  });

  it('Gerar novo número inteiro com restrição de que o valor seja maior que Number.MAX_SAFE_NUMBER', async function () {
    const valor = uint.random(
      truffleProject,
      [
        {
          left: {
            elementType: 'functionParameter',
            value: { isCode: false, js: '_value', sol: '_value', obj: '###null###' },
          },
          right: {
            value: { isCode: false, js: '0xFFFFFFFFFFFFFFFF', sol: '0xFFFFFFFFFFFFFFFF', obj: 18446744073709552000 },
            elementType: 'Literal',
          },
          from: '',
          level: 0,
          operator: '>',
        },
        {
          left: {
            elementType: 'functionParameter',
            value: { isCode: false, js: '_value', sol: '_value', obj: '###null###' },
          },
          right: {
            value: { isCode: false, js: '0xFFFFFFFF', sol: '0xFFFFFFFF', obj: 4294967295 },
            elementType: 'Literal',
          },
          from: '',
          level: 0,
          operator: '>',
        },
        {
          left: {
            elementType: 'functionParameter',
            value: { isCode: false, js: '_value', sol: '_value', obj: '###null###' },
          },
          right: { value: { isCode: false, js: '0xFFFF', sol: '0xFFFF', obj: 65535 }, elementType: 'Literal' },
          from: '',
          level: 0,
          operator: '>',
        },
        {
          left: {
            elementType: 'functionParameter',
            value: { isCode: false, js: '_value', sol: '_value', obj: '###null###' },
          },
          right: { value: { isCode: false, js: '0xFF', sol: '0xFF', obj: 255 }, elementType: 'Literal' },
          from: '',
          level: 0,
          operator: '>',
        },
        {
          left: {
            elementType: 'functionParameter',
            value: { isCode: false, js: '_value', sol: '_value', obj: '###null###' },
          },
          right: { value: { isCode: false, js: '23', sol: '23', obj: 23 }, elementType: 'Literal' },
          from: '',
          level: 0,
          operator: '>',
        },
      ],
      { type: 'ElementaryTypeName', name: 'uint' },
      contracts[0].contractDefinition
    );
    assert.isNotNaN(valor.obj);
    assert.isTrue(valor.obj > BigInt(18446744073709552000));
  });
});
