const path = require('path');
const chai = require('chai');
(assert = chai.assert), (should = chai.should());

const { Types } = require('../../services/types');
const uint = Types.uint8;

let pool = null;

describe('Type UINT8 - 8 bits', function () {
  beforeEach(async () => {
    pool = [100000, 10, 0, 100001, 99999, 11, 9, 1, -1];
  });

  it('Gerar novo número inteiro de 8 bits sem restrições, retorna algum dos itens do pool (não filtra mais pelo limite máximo do range de 8 bits pois causa loop infinito nos BigInts)', async function () {
    const valor = uint.getNewInteger(pool, []);
    assert.isNotNaN(valor);
    assert.isAtMost(parseInt(valor), 100001);
    assert.isTrue(pool.includes(valor), valor);
  });
  it('Gerar novo número inteiro de 8 bits com restrição compatível com 8 bits cujo valor não está no array deveria voltar o próprio valor da restrição', async function () {
    const valor = uint.getNewInteger(pool, [{ right: { value: { js: '250', obj: 250 } } }]);
    assert.isNotNaN(valor);
    assert.isAtMost(parseInt(valor), 100001);
    assert.equal(valor, 250);
  });
  it('Gerar novo número inteiro com restrição cujo valor está no array mas o seu próximo superior deveria voltar o valor da restrição + 1', async function () {
    const valor = uint.getNewInteger(pool, [{ right: { value: { js: '1', obj: 1 } } }]);
    assert.isNotNaN(valor);
    assert.equal(valor, 2);
    assert.isAtMost(parseInt(valor), 255);
  });
  it('Gerar novo número inteiro com restrição cujo valor está no array, bem como seu próximo superior, mas o anterior não está deveria voltar o valor da restrição - 1', async function () {
    const valor = uint.getNewInteger(
      [100000, 10, 0, 100001, 99999, 11, 9, 1, -1],
      [{ right: { value: { js: '9', obj: 9 } } }]
    );
    assert.isNotNaN(valor);
    assert.equal(valor, 8);
    assert.isAtMost(parseInt(valor), 255);
  });
});
