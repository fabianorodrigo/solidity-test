import path from 'path';
import chai from 'chai';
import 'mocha';
import { IScenario } from '../../models/scenario/IScenario';
import { TruffleProjectIceFactoryFunction } from '../../services/TruffleFactory';
import { IContract } from '../../models/IContract';
import { getFunctionParametersValuesScenarios, initReferences } from '../../services/scenarios';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { extractTruffleProjectGraph } from '../../services/TruffleProjectGraph';
import { getParametersValuesForTest } from '../utils';

const assert = chai.assert;

import { getTruffleProject, getContractByName, getFunctionByName } from '../utils';

describe('Scenarios: getFunctionParametersValuesScenarios - stateVar x TransactionParameter', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contract: ContractDefinition;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../'));
    contract = getContractByName(truffleProject.getContracts(), 'RestrictionTypesTest');
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  beforeEach(() => {
    //inicializando variáveis de estado
    global['defaultReferences'] = {};
    global['defaultReferences'][contract.name] = {};
    initReferences(truffleProject.getGraph().contracts[contract.name], global['defaultReferences'][contract.name]);
  });

  describe('Scenarios com branches sem REQUIREs', function () {
    it('Deve retornar um cenário para IF que compara parametro de transação (msg.value) e outro pro ELSE. Ambos SUCESSO', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(contract, 'ifWithCondition_stateVar_transactionParameter');

      const result: IScenario[] = getFunctionParametersValuesScenarios(truffleProject, funcTest, contract);
      assert.isArray(result);
      assert.equal(result.length, 2); //2 cenários de sucesso
      //cenário 1
      //PRE-CALLINGS
      assert.equal(result[0].preFunctionCallings.length, 1);
      assert.equal(result[0].preFunctionCallings[0].functionCalled.name, 'setStateAddress'); //setStateAddress invocado pra setar stateAddress
      //setStateAddress invocado para setar igual ao transactionParameter
      assert.equal(
        result[0].preFunctionCallings[0].parametersValues[0].stringfieldValue.obj,
        result[0].parametersValues[2].stringfieldValue.obj.from
      );
      //PARAMETERVALUES
      //valor pra entrar no IF
      assert.equal(result[0].parametersValues.length, 3); //2 parãmetro de função mais 1 de transação
      assert.equal(result[0].parametersValues[0].type, 'address');
      assert.isNull(result[0].parametersValues[0].subtype);
      assert.isNotNull(result[0].parametersValues[0].stringfieldValue.js);
      assert.equal(result[0].parametersValues[1].type, 'uint256');
      assert.isNull(result[0].parametersValues[1].subtype);
      assert.equal(result[0].parametersValues[2].type, 'TransactionParameter');
      assert.equal(
        result[0].preFunctionCallings[0].parametersValues[0].stringfieldValue.obj,
        result[0].parametersValues[2].stringfieldValue.obj.from
      );

      //cenário 2
      assert.isTrue(result[1].success);
      //PRE-CALLINGS
      assert.equal(result[1].preFunctionCallings.length, 1);
      assert.equal(result[1].preFunctionCallings[0].functionCalled.name, 'setStateAddress'); //setStateAddress invocado pra setar stateAddress
      //setStateAddress invocado para setar DIFERENTE ao transactionParameter
      assert.notEqual(
        result[1].preFunctionCallings[0].parametersValues[0].stringfieldValue.obj,
        result[1].parametersValues[2].stringfieldValue.obj.from
      );
      //PARAMETER VALUES
      assert.equal(result[1].parametersValues.length, 3); //2 parãmetro de função mais 1 de transação
      assert.equal(result[1].parametersValues[0].type, 'address');
      assert.isNull(result[1].parametersValues[0].subtype);
      assert.equal(result[1].parametersValues[1].type, 'uint256');
      assert.isNull(result[1].parametersValues[1].subtype);
      assert.equal(result[1].parametersValues[2].type, 'TransactionParameter');
      assert.notEqual(
        result[1].preFunctionCallings[0].parametersValues[0].stringfieldValue.obj,
        result[1].parametersValues[2].stringfieldValue.obj.from
      );
    });
  });
});
