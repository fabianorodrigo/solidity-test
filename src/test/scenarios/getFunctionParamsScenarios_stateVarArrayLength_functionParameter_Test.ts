import path from 'path';
import chai from 'chai';
import 'mocha';
import { IScenario } from '../../models/scenario/IScenario';
import { TruffleProjectIceFactoryFunction } from '../../services/TruffleFactory';
import { IContract } from '../../models/IContract';
import { getFunctionParametersValuesScenarios, initReferences } from '../../services/scenarios';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { extractTruffleProjectGraph } from '../../services/TruffleProjectGraph';
import { getParametersValuesForTest } from '../utils';

const assert = chai.assert;

import { getTruffleProject, getContractByName, getFunctionByName } from '../utils';

describe('Scenarios: getFunctionParametersValuesScenarios - state variable Array Length x functionParameter', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contracts: IContract[];
  let contract: ContractDefinition;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../'));
    contracts = truffleProject.getContracts();
    contract = getContractByName(contracts, 'RestrictionTypesTest');
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  beforeEach(() => {
    //inicializando variáveis de estado
    global['defaultReferences'] = {};
    global['defaultReferences'][contract.name] = {};
    initReferences(truffleProject.getGraph().contracts[contract.name], global['defaultReferences'][contract.name]);
  });

  describe('Basic tests', function () {
    it('Quando restrição de basear em stateVar Array Length que não tem função que faça atribuição, não haverá precalling', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(
        contract,
        'simpleRequires_StateVarArrayLength_functionParameter_withoutSetFunction'
      );

      const result: IScenario[] = getFunctionParametersValuesScenarios(truffleProject, funcTest, contract);
      assert.isArray(result);
      assert.equal(result.length, 1); //1 cenário de sucesso
      assert.isTrue(result[0].success);
      //PRE-CALLINGS
      assert.equal(result[0].preFunctionCallings.length, 0);
    });
    it('require(stateArray.length > functionParameter): retornará um cenário de sucesso, e um de falha para violar a restrição', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(
        contract,
        'simpleRequires_StateVarArrayLength_functionParameter'
      );

      const result: IScenario[] = getFunctionParametersValuesScenarios(truffleProject, funcTest, contract);
      assert.isArray(result);
      assert.equal(result.length, 2); //1 cenário de sucesso e 1 falha
      assert.isTrue(result[0].success);
      //PRE-CALLINGS
      assert.equal(result[0].preFunctionCallings.length, 1); //Uma função chamada antes de executar a função testada
      assert.equal(result[0].preFunctionCallings[0].functionCalled.name, 'pushStateArray'); //pushStateArray invocado antes de executar a função testada
      //PARAMETERSVALUES
      assert.equal(result[0].parametersValues.length, 2); //1 parãmetros de função mais 1 de transação
      assert.equal(result[0].parametersValues[0].type, 'uint');
      assert.isNull(result[0].parametersValues[0].subtype);
      assert.equal(result[0].parametersValues[0].stringfieldValue.obj, 0);
      // ### primeiro falho: require(stateUint > 0)
      assert.isFalse(result[1].success);
      //PRE-CALLINGS
      assert.equal(result[1].preFunctionCallings.length, 0); //o false não chamará nada antes justamente para ter tamanho zero o array
      //PARAMETERSVALUES
      assert.equal(result[1].parametersValues.length, 2); //1 parãmetros de função mais 1 de transação
      assert.isArray(result[1].parametersValues);
    });
  });
});
