import path from 'path';
import chai from 'chai';
import 'mocha';
import { IScenario } from '../../models/scenario/IScenario';
import { TruffleProjectIceFactoryFunction } from '../../services/TruffleFactory';
import { IContract } from '../../models/IContract';
import { getFunctionParametersValuesScenarios } from '../../services/scenarios';
import { GetRandomValueFunction } from '../../services/Utils/GetRandomValueFunction';
import { ElementaryTypeName, FunctionDefinition, ContractDefinition } from 'solidity-parser-antlr';

const assert = chai.assert;

import {
  truffleProjectEmpty,
  functionEmpty,
  contractEmpty,
  getTruffleProject,
  getContractByName,
  getFunctionByName,
  getParametersValuesForTest,
} from '../utils';
import { extractTruffleProjectGraph } from '../../services/TruffleProjectGraph';

const funcaoSemRestricoes = require('./funcaoSemRestricoes');
const funcaoComRestricoes = require('./funcaoComRestricoes');

const randomValueForTest: GetRandomValueFunction = (
  truffleProject: TruffleProjectIceFactoryFunction,
  type: ElementaryTypeName
) => {
  if (type.name == 'uint' || type.name == 'uint256') {
    return { isCode: false, js: '0', sol: '0', typeName: { type: 'ElementaryTypeName', name: 'uint' }, obj: 0 };
  } else {
    return {
      isCode: false,
      js: 'accounts[1]',
      typeName: { type: 'ElementaryTypeName', name: 'address' },
      sol: '0x0',
      obj: '###null###',
    };
  }
};

describe('Scenarios: getFunctionParametersValuesScenarios', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contracts: IContract[];

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../'));
    contracts = truffleProject.getContracts();
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  describe('Basic tests', function () {
    it('Se a função passada for null, gera exceção', async function () {
      //Não pode chamar, tem que deixar para o chai. Faz só o bind
      chai.expect(getFunctionParametersValuesScenarios.bind(null, null)).to.throw(`functionDefinition can't be null`);
    });
    it('Se a função passada não tiver parâmetros, o retorno conterá um cenário apenas com o parâmetro de transação', async function () {
      const result: IScenario[] = getFunctionParametersValuesScenarios(
        truffleProject,
        functionEmpty as FunctionDefinition,
        contractEmpty as ContractDefinition
      );
      assert.isArray(result);
      assert.equal(result.length, 1);
      assert.isTrue(result[0].success);
      assert.equal(result[0].parametersValues.length, 1);
    });
    it('Função sem restrições retornará apenas um cenário de teste com sucesso os parâmetros de função mais o parâmetro de transação', async function () {
      const result: IScenario[] = getFunctionParametersValuesScenarios(
        truffleProject,
        funcaoSemRestricoes,
        contractEmpty as ContractDefinition,
        getParametersValuesForTest
      );
      assert.isArray(result);
      assert.equal(result.length, 1);
      assert.isTrue(result[0].success);
      assert.equal(result[0].parametersValues.length, 2);
      assert.equal(result[0].parametersValues[0].type, 'uint256');
      assert.isNull(result[0].parametersValues[0].subtype);
      assert.equal(result[0].parametersValues[0].stringfieldValue.js, '1');
      assert.equal(result[0].parametersValues[1].type, 'TransactionParameter');
      //TODO: Passar uma getRandomVAlueForTest na getFunctionParametersValuesScenarios
      //assert.equal(JSON.parse(result[0].parametersValues[1].stringfieldValue.js).from, 'accounts[1]');
      //assert.equal(JSON.parse(result[0].parametersValues[1].stringfieldValue.js).value, '0');
    });

    it('Função com duas restrições retornará cenário de sucesso com os parâmetros, um cenário de falha para cada restrição', async function () {
      const contract = getContractByName(contracts, 'RequireUse');
      const funcUT = getFunctionByName(contract, '_internalFunction');
      const result: IScenario[] = getFunctionParametersValuesScenarios(
        truffleProject,
        funcUT,
        contract,
        getParametersValuesForTest
      );
      assert.isArray(result);
      assert.equal(result.length, 5); //1 cenário de sucesso e 4 falhos (1 para cada restrição)
      assert.isTrue(result[0].success);
      assert.equal(result[0].parametersValues.length, 5); //4 parãmetros de função mais 1 de transação
      assert.isNull(result[0].parametersValues[0].subtype);
      assert.equal(result[0].parametersValues[0].type, 'address');
      assert.equal(result[0].parametersValues[0].stringfieldValue.js, 'accounts[1]');

      assert.equal(result[0].parametersValues[1].type, 'address');
      assert.isNull(result[0].parametersValues[1].subtype);
      assert.equal(result[0].parametersValues[1].stringfieldValue.js, 'accounts[1]');

      assert.equal(result[0].parametersValues[2].type, 'uint256');
      assert.isNull(result[0].parametersValues[2].subtype);
      assert.equal(result[0].parametersValues[2].stringfieldValue.js, '1');
      // ### primeiro falho
      assert.isFalse(result[1].success);
      assert.isArray(result[1].parametersValues);
      assert.equal(result[1].parametersValues[0].type, 'address');
      assert.isNull(result[1].parametersValues[0].subtype);
      assert.equal(result[1].parametersValues[0].stringfieldValue.js, '"0x0000000000000000000000000000000000000000"');
      assert.equal(result[1].parametersValues[1].type, 'address');
      assert.isNull(result[1].parametersValues[1].subtype);
      assert.equal(
        result[1].parametersValues[1].stringfieldValue.js,
        'accounts[1]' //somente um dos parâmetros do paramsFail fica com valor que viola a restrição
      );
      assert.equal(result[1].parametersValues[2].type, 'uint256');
      assert.isNull(result[1].parametersValues[2].subtype);
      assert.equal(result[1].parametersValues[2].stringfieldValue.js, '1');
      //### segundo falho
      assert.isFalse(result[2].success);
      assert.isArray(result[2].parametersValues);
      assert.equal(result[2].parametersValues[0].type, 'address');
      assert.isNull(result[2].parametersValues[0].subtype);
      assert.equal(result[2].parametersValues[0].stringfieldValue.js, 'accounts[1]');

      assert.equal(result[2].parametersValues[1].type, 'address');
      assert.isNull(result[2].parametersValues[1].subtype);
      assert.equal(
        result[2].parametersValues[1].stringfieldValue.js,
        '"0x0000000000000000000000000000000000000000"' //somente um dos parâmetros do paramsFail fica com valor que viola a restrição
      );
      assert.equal(result[2].parametersValues[2].type, 'uint256');
      assert.isNull(result[2].parametersValues[2].subtype);
      assert.equal(
        result[2].parametersValues[2].stringfieldValue.js,
        '1' //somente um dos parâmetros do paramsFail fica com valor que viola a restrição
      );
    });
  });
  describe('Scenarios com branches sem REQUIREs', function () {
    it('Quando for identificado um branch válido baseado em um IF, deve retornar um cenário que entre neste IF e outro que não entre. Ambos SUCESSO', async function () {
      const result: IScenario[] = getFunctionParametersValuesScenarios(
        truffleProject,
        getFunctionByName(contracts[0].contractDefinition, 'simpleIf'),
        contracts[0].contractDefinition
      );
      assert.isArray(result);
      assert.equal(result.length, 2); //2 cenários de sucesso
      //cenário 1
      assert.isTrue(result[0].success);
      assert.equal(result[0].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
      assert.equal(result[0].parametersValues[0].type, 'uint');
      assert.equal(result[0].parametersValues[1].type, 'uint');
      assert.isNull(result[0].parametersValues[0].subtype);
      assert.isNull(result[0].parametersValues[1].subtype);
      assert.isAbove(parseInt(result[0].parametersValues[0].stringfieldValue.js), 10); //valor pra entrar no IF
      //cenário 2
      assert.isTrue(result[1].success);
      assert.equal(result[1].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
      assert.equal(result[1].parametersValues[0].type, 'uint');
      assert.equal(result[1].parametersValues[1].type, 'uint');
      assert.isNull(result[1].parametersValues[0].subtype);
      assert.isNull(result[1].parametersValues[1].subtype);
      assert.isAtMost(parseInt(result[1].parametersValues[0].stringfieldValue.js), 10); //valor pra NÃO entrar no IF
    });

    it('Quando for identificado um branch válido baseado em um IF e ELSE IF, deve retornar um cenário para cada if, else if e else. Todos SUCESSO', async function () {
      const result: IScenario[] = getFunctionParametersValuesScenarios(
        truffleProject,
        getFunctionByName(contracts[0].contractDefinition, '_getDaysInMonth'),
        contracts[0].contractDefinition
      );
      assert.isArray(result);
      assert.equal(result.length, 9); //3 cenários de sucesso

      //meses com 31 dias,  mais 1 de 30 e fevereiro
      [1, 3, 5, 7, 8, 10, 12, null, null].forEach((mes, i, arrayOrigem) => {
        assert.isTrue(result[i].success);
        assert.equal(result[i].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
        assert.equal(result[i].parametersValues[0].type, 'uint');
        assert.equal(result[i].parametersValues[1].type, 'uint');
        assert.isNull(result[i].parametersValues[0].subtype);
        assert.isNull(result[i].parametersValues[1].subtype);
        //meses 31 dias
        if (i <= 6) {
          assert.equal(parseInt(result[i].parametersValues[1].stringfieldValue.js), mes);
        } //qualquer mes 30 dias
        else if (i == 7) {
          assert.equal(
            arrayOrigem.indexOf(parseInt(result[i].parametersValues[1].stringfieldValue.js)),
            -1,
            `indexOf "${result[i].parametersValues[1].stringfieldValue.js}" in ${arrayOrigem} is ${arrayOrigem.indexOf(
              parseInt(result[i].parametersValues[1].stringfieldValue.js)
            )} when i == ${i}`
          );
        } //fevereiro
        else {
          assert.equal(parseInt(result[i].parametersValues[1].stringfieldValue.js), 2);
        }
      });
    });

    it('Deve retornar um cenário para IF que compara parametro de transação (msg.value) e outro pro ELSE. Ambos SUCESSO', async function () {
      const result: IScenario[] = getFunctionParametersValuesScenarios(
        truffleProject,
        getFunctionByName(contracts[0].contractDefinition, 'ifWithConditionBasedOnTransactionParameter'),
        contracts[0].contractDefinition
      );
      assert.isArray(result);
      assert.equal(result.length, 2); //2 cenários de sucesso
      //cenário 1
      assert.isTrue(result[0].success);
      assert.equal(result[0].parametersValues.length, 2); //1 parãmetro de função mais 1 de transação
      assert.equal(result[0].parametersValues[0].type, 'address');
      assert.isNull(result[0].parametersValues[0].subtype);
      assert.isNotNull(result[0].parametersValues[0].stringfieldValue.js); //valor pra entrar no IF
      assert.equal(result[0].parametersValues[1].type, 'TransactionParameter');
      assert.equal(result[0].parametersValues[1].stringfieldValue.obj.value, 15); //valor pra entrar no IF
      //cenário 2
      assert.isTrue(result[1].success);
      assert.equal(result[1].parametersValues.length, 2); //1 parãmetro de função mais 1 de transação
      assert.equal(result[1].parametersValues[0].type, 'address');
      assert.isNull(result[1].parametersValues[0].subtype);
      assert.equal(result[1].parametersValues[1].type, 'TransactionParameter');
      assert.notEqual(result[1].parametersValues[1].stringfieldValue.obj.value, 15); //valor pra entrar no IF
    });

    it('Deve retornar um cenário de teste para IF que compara atributo de parãmetro de entrada struct e outro para o else Ambos SUCESSO', async function () {
      const contractDefinition = getContractByName(contracts, 'fullOfBranchesAndRestrictions');

      const result: IScenario[] = getFunctionParametersValuesScenarios(
        truffleProject,
        getFunctionByName(contractDefinition, 'ifWithConditionBasedOnStructMember'),
        contractDefinition
      );
      assert.isArray(result);
      assert.equal(result.length, 2); //2 cenários de sucesso
      //cenário 1
      assert.isTrue(result[0].success);
      assert.equal(result[0].parametersValues.length, 2); //1 parãmetro de função mais 1 de transação
      assert.equal(result[0].parametersValues[0].type, 'UserDefinedTypeName');
      assert.equal(result[0].parametersValues[0].subtype, 'fullOfBranchesAndRestrictions.IPS');
      assert.isNotNull(result[0].parametersValues[0].stringfieldValue.js); //valor pra entrar no IF
      const regexSol = /fullOfBranchesAndRestrictions.IPS\(([0-9]{2,}),fullOfBranchesAndRestrictions\.P\.(D|W|M|Q|H|Y)\)/gm;
      const regexJs = /{"a": ([0-9]{2,}),"b": (0|1|2|3|4|5)}/gm;
      //as expressões regulares validam se está instanciando o struct com os valores que atendem ao IF
      //Como o IF é "">= 10", então o tamanho do número do primeiro parâmetro é de pelo menos 2 caracteres
      assert.isTrue(
        regexSol.test(result[0].parametersValues[0].stringfieldValue.sol),
        result[0].parametersValues[0].stringfieldValue.sol
      );
      assert.isTrue(
        regexJs.test(result[0].parametersValues[0].stringfieldValue.js),
        result[0].parametersValues[0].stringfieldValue.js
      );
      assert.equal(result[0].parametersValues[1].type, 'TransactionParameter');
      //cenário 2
      assert.isTrue(result[1].success);
      assert.equal(result[1].parametersValues.length, 2); //1 parãmetro de função mais 1 de transação
      assert.equal(result[1].parametersValues[0].type, 'UserDefinedTypeName');
      assert.equal(result[1].parametersValues[0].subtype, 'fullOfBranchesAndRestrictions.IPS');
      assert.isNotNull(result[1].parametersValues[0].stringfieldValue.js); //valor pra entrar no IF
      const regexElseSol = /fullOfBranchesAndRestrictions.IPS\(([0-9]{1}|0x01),fullOfBranchesAndRestrictions\.P\.(D|W|M|Q|H|Y)\)/gm;
      const regexElseJs = /{"a": ([0-9]{1}),"b": (0|1|2|3|4|5)}/gm;
      //as expressões regulares validam se está instanciando o struct com os valores que atendem ao IF
      //Como o IF é "">= 10", então o tamanho do número do primeiro parâmetro é de apenas 1 caracter
      assert.isTrue(
        regexElseSol.test(result[1].parametersValues[0].stringfieldValue.sol),
        result[1].parametersValues[0].stringfieldValue.sol
      );
      assert.isTrue(
        regexElseJs.test(result[1].parametersValues[0].stringfieldValue.js),
        result[1].parametersValues[0].stringfieldValue.js
      );
    });

    it('Deve retornar um cenário de teste para IF que compara enum de entrada e outro para o else Ambos SUCESSO', async function () {
      const result: IScenario[] = getFunctionParametersValuesScenarios(
        truffleProject,
        getFunctionByName(contracts[0].contractDefinition, 'ifWithConditionBasedOnEnum'),
        contracts[0].contractDefinition
      );
      assert.isArray(result);
      assert.equal(result.length, 2); //2 cenários de sucesso
      //cenário 1
      assert.isTrue(result[0].success);
      assert.equal(result[0].parametersValues.length, 2); //1 parãmetro de função mais 1 de transação
      assert.equal(result[0].parametersValues[0].type, 'UserDefinedTypeName');
      assert.equal(result[0].parametersValues[0].subtype, 'fullOfBranchesAndRestrictions.P');
      assert.equal(result[0].parametersValues[0].stringfieldValue.js, '0'); //no javascript pega o inteiro posição do elemento
      assert.equal(result[0].parametersValues[0].stringfieldValue.sol, 'fullOfBranchesAndRestrictions.P.D');
      assert.equal(result[0].parametersValues[1].type, 'TransactionParameter');
      //cenário 2
      assert.isTrue(result[1].success);
      assert.equal(result[1].parametersValues.length, 2); //1 parãmetro de função mais 1 de transação
      assert.equal(result[1].parametersValues[0].type, 'UserDefinedTypeName');
      assert.equal(result[1].parametersValues[0].subtype, 'fullOfBranchesAndRestrictions.P');
      assert.notEqual(result[1].parametersValues[0].stringfieldValue.js, 'fullOfBranchesAndRestrictions.P.D');
    });
    describe('Scenarios com branches IFs e REQUIREs', () => {
      it('Para cada branch válido baseado em um IF, deve retornar um cenário que entre neste IF e outro que não entre. Um cenário adicional deve ser gerado para quebrar o REQUIRE que o IF contém', async function () {
        const contractDefinition = getContractByName(contracts, 'RequireUse');
        const functionUT = getFunctionByName(contractDefinition, 'simpleIfWithRequire');
        const result = getFunctionParametersValuesScenarios(truffleProject, functionUT, contractDefinition);
        assert.isArray(result);
        //Continuar esses cenários pra entender como que as coisas se acumulam. Na prática, deveria ter 3 branches
        //1. entra no IF e passa no require (sucesso)
        //2. entra no IF e não passa no require (falha)
        //3. não entra no IF (sucesso)
        assert.equal(result.length, 3); //2 branches (IF/ELSE) de sucesso, sendo que o IF tem um require que gerará um terceiro cenário de falha para quebrá-lo
        //cenário 1 (entra no IF e passa pelo require)
        assert.isTrue(result[0].success);
        assert.equal(result[0].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
        assert.equal(result[0].parametersValues[0].type, 'uint');
        assert.equal(result[0].parametersValues[1].type, 'uint');
        assert.isNull(result[0].parametersValues[0].subtype);
        assert.isNull(result[0].parametersValues[1].subtype);
        assert.isAbove(
          parseInt(result[0].parametersValues[0].stringfieldValue.js),
          10,
          result[0].parametersValues[0].stringfieldValue.js
        ); //valor pra entrar no IF
        assert.isAbove(
          parseInt(result[0].parametersValues[1].stringfieldValue.js),
          100,
          result[0].parametersValues[1].stringfieldValue.js
        ); //valor pra passar pelo require dentro do IF
        //cenário 1.1 (entra no IF e quebra o require)
        assert.isFalse(result[1].success);
        assert.equal(result[1].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
        assert.equal(result[1].parametersValues[0].type, 'uint');
        assert.equal(result[1].parametersValues[1].type, 'uint');
        assert.isNull(result[1].parametersValues[0].subtype);
        assert.isNull(result[1].parametersValues[1].subtype);
        assert.isAbove(parseInt(result[0].parametersValues[0].stringfieldValue.js), 10); //valor pra entrar no IF
        assert.isAtMost(parseInt(result[1].parametersValues[1].stringfieldValue.js), 100); //valor pra quebrar o require dentro do IF
        //cenário 2 (não entra no)
        assert.isTrue(result[2].success);
        assert.equal(result[2].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
        assert.equal(result[2].parametersValues[0].type, 'uint');
        assert.equal(result[2].parametersValues[1].type, 'uint');
        assert.isNull(result[2].parametersValues[0].subtype);
        assert.isNull(result[2].parametersValues[1].subtype);
        assert.isAtMost(parseInt(result[2].parametersValues[0].stringfieldValue.js), 10); //valor pra NÃO entrar no IF
      });

      it('Deve retornar um branch do IF, um branch para cada ELSEIF e um branch que não entre em nenhum deles.  Um cenário adicional deve ser gerado para quebrar o REQUIRE que o IF contém e outro para quebrar o REQUIRE que o ELSEIF contém', async function () {
        const contractDefinition = getContractByName(contracts, 'RequireUse');
        const functionUT = getFunctionByName(contractDefinition, 'IfElseifWithRequire');
        const result: IScenario[] = getFunctionParametersValuesScenarios(
          truffleProject,
          functionUT,
          contractDefinition
        );
        assert.isArray(result);
        assert.equal(result.length, 6); //4 branches (IF/ELSEIF/ELSEIF/ELSE) de sucesso, sendo que o IF e um dos ELSEIF tem um
        //require que gerará um terceiro e um quarto cenário de falham para quebrá-los
        //cenário 1 (entra no IF e passa pelo require)
        assert.isTrue(result[0].success);
        assert.equal(result[0].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
        assert.equal(result[0].parametersValues[0].type, 'uint');
        assert.equal(result[0].parametersValues[1].type, 'uint');
        assert.isNull(result[0].parametersValues[0].subtype);
        assert.isNull(result[0].parametersValues[1].subtype);
        assert.isAbove(
          parseInt(result[0].parametersValues[0].stringfieldValue.js),
          10,
          result[0].parametersValues[0].stringfieldValue.js
        ); //valor pra entrar no IF
        assert.isAbove(
          parseInt(result[0].parametersValues[1].stringfieldValue.js),
          100,
          result[0].parametersValues[1].stringfieldValue.js
        ); //valor pra passar pelo require dentro do IF
        //cenário 1.1 (entra no IF e quebra o require)
        assert.isFalse(result[1].success);
        assert.equal(result[1].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
        assert.equal(result[1].parametersValues[0].type, 'uint');
        assert.equal(result[1].parametersValues[1].type, 'uint');
        assert.isNull(result[1].parametersValues[0].subtype);
        assert.isNull(result[1].parametersValues[1].subtype);
        assert.isAbove(parseInt(result[1].parametersValues[0].stringfieldValue.js), 10); //valor pra entrar no IF
        assert.isAtMost(parseInt(result[1].parametersValues[1].stringfieldValue.js), 100); //valor pra quebrar o require dentro do IF
        //cenário 2.1 (entra no ELSEIF e não quebra o require)
        assert.isTrue(result[2].success);
        assert.equal(result[2].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
        assert.equal(result[2].parametersValues[0].type, 'uint');
        assert.equal(result[2].parametersValues[1].type, 'uint');
        assert.isNull(result[2].parametersValues[0].subtype);
        assert.isNull(result[2].parametersValues[1].subtype);
        assert.equal(parseInt(result[2].parametersValues[1].stringfieldValue.js), 5); //valor pra entrar no ELSEIF
        assert.notEqual(parseInt(result[2].parametersValues[0].stringfieldValue.js), 0); //valor pra passar pelo require dentro do ELSEIF
        //cenário 2.2 (entra no ELSEIF e  quebra o require)
        assert.isFalse(result[3].success);
        assert.equal(result[3].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
        assert.equal(result[3].parametersValues[0].type, 'uint');
        assert.equal(result[3].parametersValues[1].type, 'uint');
        assert.isNull(result[3].parametersValues[0].subtype);
        assert.isNull(result[3].parametersValues[1].subtype);
        assert.equal(parseInt(result[3].parametersValues[1].stringfieldValue.js), 5); //valor pra entrar no ELSEIF
        assert.equal(parseInt(result[3].parametersValues[0].stringfieldValue.js), 0); //valor pra quebrar o require dentro do ELSEIF
        //cenário 3 (entra no segundo ELSEIF)
        assert.isTrue(result[4].success);
        assert.equal(result[4].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
        assert.equal(result[4].parametersValues[0].type, 'uint');
        assert.equal(result[4].parametersValues[1].type, 'uint');
        assert.isNull(result[4].parametersValues[0].subtype);
        assert.isNull(result[4].parametersValues[1].subtype);
        assert.equal(parseInt(result[4].parametersValues[1].stringfieldValue.js), 3); //valor pra entrar no ELSEIF
        //cenário 4 (não entra no IF nem ELSEIFs)
        assert.isTrue(result[5].success);
        assert.equal(result[5].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
        assert.equal(result[5].parametersValues[0].type, 'uint');
        assert.equal(result[5].parametersValues[1].type, 'uint');
        assert.isNull(result[5].parametersValues[0].subtype);
        assert.isNull(result[5].parametersValues[1].subtype);
        assert.isAtMost(
          parseInt(result[5].parametersValues[0].stringfieldValue.js),
          10,
          result[5].parametersValues[0].stringfieldValue.js
        ); //valor pra NÃO entrar no IF
        assert.notEqual(
          parseInt(result[5].parametersValues[1].stringfieldValue.js),
          5,
          result[5].parametersValues[1].stringfieldValue.js
        ); //valor pra não entrar no primeiro ELSEIF
        assert.notEqual(
          parseInt(result[5].parametersValues[1].stringfieldValue.js),
          3,
          result[5].parametersValues[1].stringfieldValue.js
        ); //valor pra não entrar no segundo ELSEIF
      });
    });
  });
});
