import path from 'path';
import chai from 'chai';
import 'mocha';
import { IScenario } from '../../models/scenario/IScenario';
import { TruffleProjectIceFactoryFunction } from '../../services/TruffleFactory';
import { IContract } from '../../models/IContract';
import { getFunctionParametersValuesScenarios } from '../../services/scenarios';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { extractTruffleProjectGraph } from '../../services/TruffleProjectGraph';
import { getParametersValuesForTest } from '../utils';

const assert = chai.assert;

import { getTruffleProject, getContractByName, getFunctionByName } from '../utils';

describe('Scenarios: getFunctionParametersValuesScenarios', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contracts: IContract[];
  let contract: ContractDefinition;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../'));
    contracts = truffleProject.getContracts();
    contract = getContractByName(contracts, 'RestrictionTypesTest');
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  describe('Basic tests', function () {
    it('Função sem restrições retornará apenas um cenário de teste com sucesso os parâmetros de função mais o parâmetro de transação', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(contract, 'noRestriction');

      const result: IScenario[] = getFunctionParametersValuesScenarios(
        truffleProject,
        funcTest,
        contract,
        getParametersValuesForTest
      );
      assert.isArray(result);
      assert.equal(result.length, 1);
      assert.isTrue(result[0].success);
      assert.equal(result[0].parametersValues.length, 2);
      assert.equal(result[0].parametersValues[0].type, 'string');
      assert.isNull(result[0].parametersValues[0].subtype);
      assert.equal(result[0].parametersValues[0].stringfieldValue.js, '"just a test"');
      assert.equal(result[0].parametersValues[1].type, 'TransactionParameter');
      //TODO: Passar uma getRandomVAlueForTest na getFunctionParametersValuesScenarios
      //assert.equal(JSON.parse(result[0].parametersValues[1].stringfieldValue.js).from, 'accounts[1]');
      //assert.equal(JSON.parse(result[0].parametersValues[1].stringfieldValue.js).value, '0');
    });

    it('require(teste == teste2);  require(teste > teste2 && teste > teste3): retornará cenário de sucesso com os parâmetros, um cenário de falha para cada restrição (um para o require do modifier e dois para a restrição composta no corpo da função)', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(
        contract,
        'simpleRequires_functionParameter_functionParameter'
      );

      const result: IScenario[] = getFunctionParametersValuesScenarios(
        truffleProject,
        funcTest,
        contract,
        getParametersValuesForTest
      );
      assert.isArray(result);
      assert.equal(result.length, 5); //1 cenário de sucesso e 4 falhos (1 para cada restrição)
      assert.isTrue(result[0].success);
      assert.equal(result[0].parametersValues.length, 4); //3 parãmetros de função mais 1 de transação
      assert.equal(result[0].parametersValues[0].type, 'uint256');
      assert.equal(result[0].parametersValues[1].type, 'uint256');
      assert.equal(result[0].parametersValues[2].type, 'uint256');
      assert.isNull(result[0].parametersValues[0].subtype);
      assert.isNull(result[0].parametersValues[1].subtype);
      assert.isNull(result[0].parametersValues[2].subtype);
      assert.equal(result[0].parametersValues[0].stringfieldValue.js, '1');
      assert.equal(result[0].parametersValues[1].stringfieldValue.js, '1');
      assert.equal(result[0].parametersValues[2].stringfieldValue.js, '1');
      // ### primeiro falho (modifier)
      assert.isFalse(result[1].success);
      assert.isArray(result[1].parametersValues);
      assert.equal(result[1].parametersValues[0].type, 'uint256');
      assert.isNull(result[1].parametersValues[0].subtype);
      //A primeira restrição é require(teste != 0). dentro do modifier
      assert.equal(result[1].parametersValues[0].stringfieldValue.js, '0');

      assert.equal(result[1].parametersValues[1].type, 'uint256');
      assert.isNull(result[1].parametersValues[1].subtype);
      assert.equal(result[1].parametersValues[1].stringfieldValue.js, '1');

      assert.equal(result[1].parametersValues[2].type, 'uint256');
      assert.isNull(result[1].parametersValues[2].subtype);
      assert.equal(result[1].parametersValues[2].stringfieldValue.js, '1');

      // ### segundo falho
      assert.isFalse(result[2].success);
      assert.isArray(result[2].parametersValues);
      assert.equal(result[2].parametersValues[0].type, 'uint256');
      assert.isNull(result[2].parametersValues[0].subtype);
      //A segunda restrição é require(teste == teste2). Como o valor do caso de sucesso é '1' e
      //a função que Integer.getExtraBoundary soma um ao valor atribuído a right.value.js
      //quando o operador  '==', o valor aqui será 2
      assert.equal(result[2].parametersValues[0].stringfieldValue.js, '2');

      assert.equal(result[2].parametersValues[1].type, 'uint256');
      assert.isNull(result[2].parametersValues[1].subtype);
      assert.equal(result[2].parametersValues[1].stringfieldValue.js, '1');

      assert.equal(result[2].parametersValues[2].type, 'uint256');
      assert.isNull(result[2].parametersValues[2].subtype);
      assert.equal(result[2].parametersValues[2].stringfieldValue.js, '1');

      //### segundo falho
      assert.isFalse(result[3].success);
      assert.isArray(result[3].parametersValues);
      assert.equal(result[3].parametersValues[0].type, 'uint256');
      assert.isNull(result[3].parametersValues[0].subtype);
      assert.equal(result[3].parametersValues[0].stringfieldValue.js, '1');

      assert.equal(result[3].parametersValues[1].type, 'uint256');
      assert.isNull(result[3].parametersValues[1].subtype);
      assert.equal(
        result[3].parametersValues[1].stringfieldValue.js,
        '1' //FICANDO IGUAL, QUEBRA A REGRA
      );
      assert.equal(result[3].parametersValues[2].type, 'uint256');
      assert.isNull(result[3].parametersValues[2].subtype);
      //A segunda restrição é require(teste > teste2). O cenário de falha deixará os valores iguais para quebrar no require
      assert.equal(
        result[3].parametersValues[2].stringfieldValue.js,
        '1' //FICANDO IGUAL, QUEBRA A REGRA
      );
      //### terceiro falho
      assert.isFalse(result[4].success);
      assert.isArray(result[4].parametersValues);
      assert.equal(result[4].parametersValues[0].type, 'uint256');
      assert.isNull(result[4].parametersValues[0].subtype);
      assert.equal(result[4].parametersValues[0].stringfieldValue.js, '1');

      assert.equal(result[4].parametersValues[1].type, 'uint256');
      assert.isNull(result[4].parametersValues[1].subtype);
      assert.equal(result[4].parametersValues[1].stringfieldValue.js, '1');
      assert.equal(result[4].parametersValues[2].type, 'uint256');
      assert.isNull(result[4].parametersValues[2].subtype);
      //A terceira restrição é require(teste > teste3). O cenário de falha deixará os valores iguais para quebrar no require
      assert.equal(result[4].parametersValues[2].stringfieldValue.js, '1'); //FICANDO IGUAL, QUEBRA A REGRA
    });
  });
  describe('Scenarios com branches sem REQUIREs', function () {
    it('if(param1 > param2){}: deve retornar um cenário que entre neste IF e outro que não entre. Ambos SUCESSO', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(contract, 'simpleIf_functionParameter_functionParameter');

      const result: IScenario[] = getFunctionParametersValuesScenarios(
        truffleProject,
        funcTest,
        contract,
        getParametersValuesForTest
      );
      assert.isArray(result);
      assert.equal(result.length, 2); //2 cenários de sucesso
      //cenário 1
      assert.isTrue(result[0].success);
      assert.equal(result[0].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
      assert.equal(result[0].parametersValues[0].type, 'uint256');
      assert.equal(result[0].parametersValues[1].type, 'uint256');
      assert.isNull(result[0].parametersValues[0].subtype);
      assert.isNull(result[0].parametersValues[1].subtype);
      assert.isAbove(
        parseInt(result[0].parametersValues[0].stringfieldValue.js),
        parseInt(result[0].parametersValues[1].stringfieldValue.js)
      ); //valor pra entrar no IF
      //cenário 2
      assert.isTrue(result[1].success);
      assert.equal(result[1].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
      assert.equal(result[1].parametersValues[0].type, 'uint256');
      assert.equal(result[1].parametersValues[1].type, 'uint256');
      assert.isNull(result[1].parametersValues[0].subtype);
      assert.isNull(result[1].parametersValues[1].subtype);
      assert.isAtMost(
        parseInt(result[1].parametersValues[0].stringfieldValue.js),
        parseInt(result[0].parametersValues[1].stringfieldValue.js)
      ); //valor pra NÃO entrar no IF
    });

    it('Quando for identificado um branch válido baseado em um IF e ELSE IF, deve retornar um cenário para cada if, else if e else. Todos SUCESSO', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(
        contract,
        'ifElseIfElse_functionParameter_functionParameter'
      );

      const result: IScenario[] = getFunctionParametersValuesScenarios(
        truffleProject,
        funcTest,
        contract,
        getParametersValuesForTest
      );
      assert.isArray(result);
      assert.equal(result.length, 3); //3 cenários de sucesso

      //param1 == param2
      assert.isTrue(result[0].success);
      assert.equal(result[0].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
      assert.equal(result[0].parametersValues[0].type, 'uint256');
      assert.equal(result[0].parametersValues[1].type, 'uint256');
      assert.isNull(result[0].parametersValues[0].subtype);
      assert.isNull(result[0].parametersValues[1].subtype);
      assert.equal(
        parseInt(result[0].parametersValues[0].stringfieldValue.js),
        parseInt(result[0].parametersValues[1].stringfieldValue.js)
      );
      //param2 < param1
      assert.isAtLeast(
        parseInt(result[1].parametersValues[0].stringfieldValue.js),
        parseInt(result[1].parametersValues[1].stringfieldValue.js) + 1
      );
      //else
      //param1 < param2
      assert.isAtMost(
        parseInt(result[2].parametersValues[0].stringfieldValue.js),
        parseInt(result[2].parametersValues[1].stringfieldValue.js) - 1,
        `param1: ${result[2].parametersValues[0].stringfieldValue.js}, param2: ${result[2].parametersValues[1].stringfieldValue.js}`
      );
    });

    it('Deve retornar um cenário de teste para IF que compara enum de entrada e outro para o else Ambos SUCESSO', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(
        contract,
        'ifWithConditionBasedOnEnum_functionParameter_functionParameter'
      );

      const result: IScenario[] = getFunctionParametersValuesScenarios(
        truffleProject,
        funcTest,
        contract,
        getParametersValuesForTest
      );
      assert.isArray(result);
      assert.equal(result.length, 2); //2 cenários de sucesso
      //cenário 1
      assert.isTrue(result[0].success);
      assert.equal(result[0].parametersValues.length, 3); //2 parãmetro de função mais 1 de transação
      assert.equal(result[0].parametersValues[0].type, 'UserDefinedTypeName');
      assert.equal(result[0].parametersValues[0].subtype, 'RestrictionTypesTest.COVID');
      assert.equal(
        result[0].parametersValues[0].stringfieldValue.js,
        result[0].parametersValues[1].stringfieldValue.js
      ); //no javascript pega o inteiro posição do elemento
      assert.equal(
        result[0].parametersValues[0].stringfieldValue.sol,
        result[0].parametersValues[1].stringfieldValue.sol
      );
      assert.equal(result[0].parametersValues[2].type, 'TransactionParameter');
      //cenário 2
      assert.isTrue(result[1].success);
      assert.equal(result[1].parametersValues.length, 3); //2 parãmetro de função mais 1 de transação
      assert.equal(result[1].parametersValues[0].type, 'UserDefinedTypeName');
      assert.equal(result[1].parametersValues[0].subtype, 'RestrictionTypesTest.COVID');
      assert.notEqual(
        result[1].parametersValues[0].stringfieldValue.js,
        result[1].parametersValues[1].stringfieldValue.js
      );
      assert.notEqual(
        result[1].parametersValues[0].stringfieldValue.sol,
        result[1].parametersValues[1].stringfieldValue.sol
      );
    });
    describe('Scenarios com branches IFs e REQUIREs', () => {
      it('Para cada branch válido baseado em um IF, deve retornar um cenário que entre neste IF e outro que não entre. Um cenário adicional deve ser gerado para quebrar o REQUIRE que o IF contém', async function () {
        const contractDefinition = getContractByName(contracts, 'RequireUse');
        const functionUT = getFunctionByName(contractDefinition, 'simpleIfWithRequire');
        const result = getFunctionParametersValuesScenarios(truffleProject, functionUT, contractDefinition);
        assert.isArray(result);
        //Continuar esses cenários pra entender como que as coisas se acumulam. Na prática, deveria ter 3 branches
        //1. entra no IF e passa no require (sucesso)
        //2. entra no IF e não passa no require (falha)
        //3. não entra no IF (sucesso)
        assert.equal(result.length, 3); //2 branches (IF/ELSE) de sucesso, sendo que o IF tem um require que gerará um terceiro cenário de falha para quebrá-lo
        //cenário 1 (entra no IF e passa pelo require)
        assert.isTrue(result[0].success);
        assert.equal(result[0].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
        assert.equal(result[0].parametersValues[0].type, 'uint');
        assert.equal(result[0].parametersValues[1].type, 'uint');
        assert.isNull(result[0].parametersValues[0].subtype);
        assert.isNull(result[0].parametersValues[1].subtype);
        assert.isAbove(
          parseInt(result[0].parametersValues[0].stringfieldValue.js),
          10,
          result[0].parametersValues[0].stringfieldValue.js
        ); //valor pra entrar no IF
        assert.isAbove(
          parseInt(result[0].parametersValues[1].stringfieldValue.js),
          100,
          result[0].parametersValues[1].stringfieldValue.js
        ); //valor pra passar pelo require dentro do IF
        //cenário 1.1 (entra no IF e quebra o require)
        assert.isFalse(result[1].success);
        assert.equal(result[1].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
        assert.equal(result[1].parametersValues[0].type, 'uint');
        assert.equal(result[1].parametersValues[1].type, 'uint');
        assert.isNull(result[1].parametersValues[0].subtype);
        assert.isNull(result[1].parametersValues[1].subtype);
        assert.isAbove(parseInt(result[0].parametersValues[0].stringfieldValue.js), 10); //valor pra entrar no IF
        assert.isAtMost(parseInt(result[1].parametersValues[1].stringfieldValue.js), 100); //valor pra quebrar o require dentro do IF
        //cenário 2 (não entra no)
        assert.isTrue(result[2].success);
        assert.equal(result[2].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
        assert.equal(result[2].parametersValues[0].type, 'uint');
        assert.equal(result[2].parametersValues[1].type, 'uint');
        assert.isNull(result[2].parametersValues[0].subtype);
        assert.isNull(result[2].parametersValues[1].subtype);
        assert.isAtMost(parseInt(result[2].parametersValues[0].stringfieldValue.js), 10); //valor pra NÃO entrar no IF
      });

      it('Deve retornar um branch do IF, um branch para cada ELSEIF e um branch que não entre em nenhum deles.  Um cenário adicional deve ser gerado para quebrar o REQUIRE que o IF contém e outro para quebrar o REQUIRE que o ELSEIF contém', async function () {
        const contractDefinition = getContractByName(contracts, 'RequireUse');
        const functionUT = getFunctionByName(contractDefinition, 'IfElseifWithRequire');
        const result: IScenario[] = getFunctionParametersValuesScenarios(
          truffleProject,
          functionUT,
          contractDefinition
        );
        assert.isArray(result);
        assert.equal(result.length, 6); //4 branches (IF/ELSEIF/ELSEIF/ELSE) de sucesso, sendo que o IF e um dos ELSEIF tem um
        //require que gerará um terceiro e um quarto cenário de falham para quebrá-los
        //cenário 1 (entra no IF e passa pelo require)
        assert.isTrue(result[0].success);
        assert.equal(result[0].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
        assert.equal(result[0].parametersValues[0].type, 'uint');
        assert.equal(result[0].parametersValues[1].type, 'uint');
        assert.isNull(result[0].parametersValues[0].subtype);
        assert.isNull(result[0].parametersValues[1].subtype);
        assert.isAbove(
          parseInt(result[0].parametersValues[0].stringfieldValue.js),
          10,
          result[0].parametersValues[0].stringfieldValue.js
        ); //valor pra entrar no IF
        assert.isAbove(
          parseInt(result[0].parametersValues[1].stringfieldValue.js),
          100,
          result[0].parametersValues[1].stringfieldValue.js
        ); //valor pra passar pelo require dentro do IF
        //cenário 1.1 (entra no IF e quebra o require)
        assert.isFalse(result[1].success);
        assert.equal(result[1].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
        assert.equal(result[1].parametersValues[0].type, 'uint');
        assert.equal(result[1].parametersValues[1].type, 'uint');
        assert.isNull(result[1].parametersValues[0].subtype);
        assert.isNull(result[1].parametersValues[1].subtype);
        assert.isAbove(parseInt(result[1].parametersValues[0].stringfieldValue.js), 10); //valor pra entrar no IF
        assert.isAtMost(parseInt(result[1].parametersValues[1].stringfieldValue.js), 100); //valor pra quebrar o require dentro do IF
        //cenário 2.1 (entra no ELSEIF e não quebra o require)
        assert.isTrue(result[2].success);
        assert.equal(result[2].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
        assert.equal(result[2].parametersValues[0].type, 'uint');
        assert.equal(result[2].parametersValues[1].type, 'uint');
        assert.isNull(result[2].parametersValues[0].subtype);
        assert.isNull(result[2].parametersValues[1].subtype);
        assert.equal(parseInt(result[2].parametersValues[1].stringfieldValue.js), 5); //valor pra entrar no ELSEIF
        assert.notEqual(parseInt(result[2].parametersValues[0].stringfieldValue.js), 0); //valor pra passar pelo require dentro do ELSEIF
        //cenário 2.2 (entra no ELSEIF e  quebra o require)
        assert.isFalse(result[3].success);
        assert.equal(result[3].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
        assert.equal(result[3].parametersValues[0].type, 'uint');
        assert.equal(result[3].parametersValues[1].type, 'uint');
        assert.isNull(result[3].parametersValues[0].subtype);
        assert.isNull(result[3].parametersValues[1].subtype);
        assert.equal(parseInt(result[3].parametersValues[1].stringfieldValue.js), 5); //valor pra entrar no ELSEIF
        assert.equal(parseInt(result[3].parametersValues[0].stringfieldValue.js), 0); //valor pra quebrar o require dentro do ELSEIF
        //cenário 3 (entra no segundo ELSEIF)
        assert.isTrue(result[4].success);
        assert.equal(result[4].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
        assert.equal(result[4].parametersValues[0].type, 'uint');
        assert.equal(result[4].parametersValues[1].type, 'uint');
        assert.isNull(result[4].parametersValues[0].subtype);
        assert.isNull(result[4].parametersValues[1].subtype);
        assert.equal(parseInt(result[4].parametersValues[1].stringfieldValue.js), 3); //valor pra entrar no ELSEIF
        //cenário 4 (não entra no IF nem ELSEIFs)
        assert.isTrue(result[5].success);
        assert.equal(result[5].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
        assert.equal(result[5].parametersValues[0].type, 'uint');
        assert.equal(result[5].parametersValues[1].type, 'uint');
        assert.isNull(result[5].parametersValues[0].subtype);
        assert.isNull(result[5].parametersValues[1].subtype);
        assert.isAtMost(
          parseInt(result[5].parametersValues[0].stringfieldValue.js),
          10,
          result[5].parametersValues[0].stringfieldValue.js
        ); //valor pra NÃO entrar no IF
        assert.notEqual(
          parseInt(result[5].parametersValues[1].stringfieldValue.js),
          5,
          result[5].parametersValues[1].stringfieldValue.js
        ); //valor pra não entrar no primeiro ELSEIF
        assert.notEqual(
          parseInt(result[5].parametersValues[1].stringfieldValue.js),
          3,
          result[5].parametersValues[1].stringfieldValue.js
        ); //valor pra não entrar no segundo ELSEIF
      });
    });
  });
});
