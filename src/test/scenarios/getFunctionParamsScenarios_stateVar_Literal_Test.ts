import path from 'path';
import chai from 'chai';
import 'mocha';
import { IScenario } from '../../models/scenario/IScenario';
import { TruffleProjectIceFactoryFunction } from '../../services/TruffleFactory';
import { IContract } from '../../models/IContract';
import { getFunctionParametersValuesScenarios, initReferences } from '../../services/scenarios';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { extractTruffleProjectGraph } from '../../services/TruffleProjectGraph';
import { getParametersValuesForTest } from '../utils';

const assert = chai.assert;

import { getTruffleProject, getContractByName, getFunctionByName } from '../utils';

describe('Scenarios: getFunctionParametersValuesScenarios - state variable x Literal', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contracts: IContract[];
  let contract: ContractDefinition;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../'));
    contracts = truffleProject.getContracts();
    contract = getContractByName(contracts, 'RestrictionTypesTest');
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  beforeEach(() => {
    //inicializando variáveis de estado
    global['defaultReferences'] = {};
    global['defaultReferences'][contract.name] = {};
    initReferences(truffleProject.getGraph().contracts[contract.name], global['defaultReferences'][contract.name]);
  });

  describe('Basic tests', function () {
    it('Quando restrição de basear em stateVar que não tem função que faça atribuição, não haverá precalling', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(
        contract,
        'simpleRequires_StateVar_Literal_withoutSetFunction'
      );

      const result: IScenario[] = getFunctionParametersValuesScenarios(truffleProject, funcTest, contract);
      assert.isArray(result);
      assert.equal(result.length, 1); //1 cenário de sucesso
      assert.isTrue(result[0].success);
      //PRE-CALLINGS
      assert.equal(result[0].preFunctionCallings.length, 0);
    });
    it('require(stateVar > 0);  0  < stateVar; stateVar2 =="teste"; functionParameter != 0: retornará cenário de sucesso com os parâmetros, 4 cenários de falha (um para cada restrição)', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(contract, 'simpleRequires_StateVar_Literal');

      const result: IScenario[] = getFunctionParametersValuesScenarios(
        truffleProject,
        funcTest,
        contract,
        getParametersValuesForTest
      );
      assert.isArray(result);
      assert.equal(result.length, 5); //1 cenário de sucesso e 4 falhos (1 para cada restrição)
      assert.isTrue(result[0].success);
      //PRE-CALLINGS
      assert.equal(result[0].preFunctionCallings.length, 1); //Uma função, o construtor, chamada antes de executar a função testada
      assert.equal(result[0].preFunctionCallings[0].functionCalled.name, 'constructor'); //construtor invocado antes de executar a função testada
      assert.isAtLeast(result[0].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj, 1); //construtor invocado intanciando stateUint com valor maior que zero
      assert.isAtMost(result[0].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj, 999); //construtor invocado intanciando stateUint com valor menor que 1000
      //PARAMETERSVALUES
      assert.equal(result[0].parametersValues.length, 2); //1 parãmetros de função mais 1 de transação
      assert.equal(result[0].parametersValues[0].type, 'address');
      assert.isNull(result[0].parametersValues[0].subtype);
      assert.equal(result[0].parametersValues[0].stringfieldValue.js, 'accounts[1]');
      // ### primeiro falho: require(stateUint > 0)
      assert.isFalse(result[1].success);
      //PRE-CALLINGS
      assert.equal(result[1].preFunctionCallings.length, 1); //Uma função, o construtor, chamada antes de executar a função testada
      assert.equal(result[1].preFunctionCallings[0].functionCalled.name, 'constructor'); //construtor invocado antes de executar a função testada
      assert.equal(result[1].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj, 0); //construtor invocado intanciando stateUint com valor igual a zero para falhar primeiro require
      //PARAMETERSVALUES
      assert.equal(result[1].parametersValues.length, 2); //1 parãmetros de função mais 1 de transação
      assert.isArray(result[1].parametersValues);
      assert.equal(result[1].parametersValues[0].type, 'address');
      assert.isNull(result[1].parametersValues[0].subtype);
      assert.equal(result[1].parametersValues[0].stringfieldValue.js, 'accounts[1]');
      // ### segundo falho: require(0 < stateUint)
      assert.isFalse(result[1].success);
      //PRE-CALLINGS
      assert.equal(result[2].preFunctionCallings.length, 1); //Uma função, o construtor, chamada antes de executar a função testada
      assert.equal(result[2].preFunctionCallings[0].functionCalled.name, 'constructor'); //construtor invocado antes de executar a função testada
      assert.equal(result[2].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj, 0); //construtor invocado intanciando stateUint com valor igual a zero para falhar primeiro require
      //PARAMETERSVALUES
      assert.equal(result[2].parametersValues.length, 2); //1 parãmetros de função mais 1 de transação
      assert.isArray(result[2].parametersValues);
      assert.equal(result[2].parametersValues[0].type, 'address');
      assert.isNull(result[2].parametersValues[0].subtype);
      assert.equal(result[2].parametersValues[0].stringfieldValue.js, 'accounts[1]');
      // ### terceiro falho: stateString == 'teste' && stateUint < 1000
      assert.isFalse(result[3].success);
      //PRE-CALLINGS
      //No contrato do teste não existe função que altere o state de stateString,
      //assim, restrição de stateString será ignorada e apenas a da stateUint será considerada (se existisse, seriam dois cenários distintos)
      assert.equal(result[3].preFunctionCallings.length, 1);
      assert.equal(result[3].preFunctionCallings.length, 1); //Uma função, o construtor, chamada antes de executar a função testada
      assert.equal(result[3].preFunctionCallings[0].functionCalled.name, 'constructor'); //construtor invocado antes de executar a função testada
      assert.isAtLeast(result[3].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj, 1000); //construtor invocado intanciando stateUint com valor igual a 1000 para falhar primeiro require
      //PARAMETERSVALUES
      assert.equal(result[3].parametersValues.length, 2); //1 parãmetros de função mais 1 de transação
      assert.isArray(result[3].parametersValues);
      assert.equal(result[3].parametersValues[0].type, 'address');
      assert.isNull(result[3].parametersValues[0].subtype);
      assert.equal(result[3].parametersValues[0].stringfieldValue.js, 'accounts[1]');
      // ### quarto falho: require(recipient != address(0)
      assert.isFalse(result[4].success);
      //PRE-CALLINGS
      assert.equal(result[4].preFunctionCallings.length, 1);
      //em cenários de falha que não envolvam variáveis de estado, o preFunctionCallings é idẽntico ao do cenário de sucesso
      assert.equal(
        JSON.stringify(result[4].preFunctionCallings[0].parametersValues),
        JSON.stringify(result[0].preFunctionCallings[0].parametersValues)
      );
      //PARAMETERSVALUES
      assert.equal(result[4].parametersValues.length, 2); //1 parãmetros de função mais 1 de transação
      assert.isArray(result[4].parametersValues);
      assert.equal(result[4].parametersValues[0].type, 'address');
      assert.isNull(result[4].parametersValues[0].subtype);
      assert.equal(result[4].parametersValues[0].stringfieldValue.js, '"0x0000000000000000000000000000000000000000"');
    });
  });
  describe('Scenarios com branches sem REQUIREs', function () {
    it('if(stateUint > 0){}: deve retornar um cenário que entre neste IF e outro que não entre. Ambos SUCESSO', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(contract, 'if_stateVar_Literal');

      const result: IScenario[] = getFunctionParametersValuesScenarios(truffleProject, funcTest, contract);
      assert.isArray(result);
      assert.equal(result.length, 2); //2 cenários de sucesso
      //cenário 1
      assert.isTrue(result[0].success);
      //PRE-CALLINGS
      assert.equal(result[0].preFunctionCallings.length, 1); //Uma função, o construtor, chamada antes de executar a função testada
      assert.equal(result[0].preFunctionCallings[0].functionCalled.name, 'constructor'); //construtor invocado antes de executar a função testada
      assert.isAtLeast(result[0].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj, 1); //construtor invocado intanciando stateUint com valor maior que zero
      //PARAMETERVALUES - só o transactionParameter
      assert.equal(result[0].parametersValues.length, 1);
      //cenário 2
      assert.isTrue(result[1].success);
      //PRE-CALLINGS
      //Não precisa de preCalling para não entrar no IF pois o valor inicial já atende
      assert.equal(result[1].preFunctionCallings.length, 0);
      //PARAMETERVALUES - só o transactionParameter
      assert.equal(result[1].parametersValues.length, 1);
    });

    it('Quando for identificado um branch válido baseado em um IF e ELSE IF, deve retornar um cenário para cada if, else if e else. Todos SUCESSO', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(contract, 'ifElseIfElse_stateVar_Literal');

      const result: IScenario[] = getFunctionParametersValuesScenarios(truffleProject, funcTest, contract);
      assert.isArray(result);
      assert.equal(result.length, 3); //3 cenários de sucesso

      //stateUint == 100
      assert.isTrue(result[0].success);
      //PRE-CALLINGS
      assert.equal(result[0].preFunctionCallings.length, 1); //Uma função, o construtor, chamada antes de executar a função testada
      assert.equal(result[0].preFunctionCallings[0].functionCalled.name, 'constructor'); //construtor invocado antes de executar a função testada
      assert.equal(result[0].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj, 100); //construtor invocado intanciando stateUint com valor igual a 100
      //PARAMETERVALUES
      assert.equal(result[0].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
      assert.equal(result[0].parametersValues[0].type, 'uint256');
      assert.equal(result[0].parametersValues[1].type, 'uint256');
      assert.isNull(result[0].parametersValues[0].subtype);
      assert.isNull(result[0].parametersValues[1].subtype);
      //stateUint > 100
      assert.isTrue(result[1].success);
      //PRE-CALLINGS
      assert.equal(result[1].preFunctionCallings.length, 1); //Uma função, o construtor, chamada antes de executar a função testada
      assert.equal(result[1].preFunctionCallings[0].functionCalled.name, 'constructor'); //construtor invocado antes de executar a função testada
      assert.isAtLeast(result[1].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj, 101); //construtor invocado intanciando stateUint com valor maior que 100
      //else
      //stateUint < 100
      assert.isTrue(result[2].success);
      //PRE-CALLINGS
      //Nâo precisa chamar o construtor pois o valor inicial já é menor que 100 e, assim, não precisa de nenhuma preCalling
      assert.equal(result[2].preFunctionCallings.length, 0);
    });

    it('Deve retornar um cenário de teste para IF que compara stateEnum e outro para o else Ambos SUCESSO', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(contract, 'ifWithConditionBasedOnEnum_stateVar_Literal');

      const result: IScenario[] = getFunctionParametersValuesScenarios(truffleProject, funcTest, contract);
      assert.isArray(result);
      assert.equal(result.length, 2); //2 cenários de sucesso
      //cenário 1
      assert.isTrue(result[0].success);
      //PRE-CALLINGS
      assert.equal(result[0].preFunctionCallings.length, 1); //Uma função, o setStateEnum, chamada antes de executar a função testada
      assert.equal(result[0].preFunctionCallings[0].functionCalled.name, 'setStateEnum'); //setStateEnum invocado antes de executar a função testada
      assert.equal(result[0].preFunctionCallings[0].parametersValues[0].stringfieldValue.obj, 0); //setStateEnum invocado intanciando stateEnum com valor igual a COVID.a (0)
      //PARAMETERVALUES
      assert.equal(result[0].parametersValues.length, 1); //transactionParameter
      assert.equal(result[0].parametersValues[0].type, 'TransactionParameter');
      //cenário 2
      assert.isTrue(result[1].success);
      //PRE-CALLINGS
      assert.equal(result[1].preFunctionCallings.length, 1); //Uma função, o setStateEnum, chamada antes de executar a função testada
      assert.equal(result[1].preFunctionCallings[0].functionCalled.name, 'setStateEnum'); //setStateEnum invocado antes de executar a função testada
      assert.notEqual(result[1].preFunctionCallings[0].parametersValues[0].stringfieldValue.obj, 0); //setStateEnum invocado intanciando stateEnum com valor diferente de COVID.a
      //PARAMETERVALUES
      assert.equal(result[1].parametersValues.length, 1); //transactionParameter
      assert.equal(result[1].parametersValues[0].type, 'TransactionParameter');
    });
  });
  describe('Scenarios com branches IFs e REQUIREs', () => {
    it('Para cada branch válido baseado em um IF, deve retornar um cenário que entre neste IF e outro que não entre. Um cenário adicional deve ser gerado para quebrar o REQUIRE que o IF contém', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(contract, 'ifWithRequire_stateVar_Literal');
      const result = getFunctionParametersValuesScenarios(truffleProject, funcTest, contract);

      assert.isArray(result);
      //Continuar esses cenários pra entender como que as coisas se acumulam. Na prática, deveria ter 3 branches
      //1. entra no IF e passa no require (sucesso)
      //2. entra no IF e não passa no require (falha)
      //3. não entra no IF (sucesso)
      assert.equal(result.length, 3); //2 branches (IF/ELSE) de sucesso, sendo que o IF tem um require que gerará um terceiro cenário de falha para quebrá-lo
      //cenário 1 (entra no IF e passa pelo require)
      assert.isTrue(result[0].success);
      //PRE-CALLINGS
      assert.equal(result[0].preFunctionCallings.length, 2);
      assert.equal(result[0].preFunctionCallings[0].functionCalled.name, 'constructor'); //construtor invocado pra setar stateUint
      assert.isAtLeast(result[0].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj, 11); //construtor invocado intanciando stateUint com valor maior que 10
      assert.equal(result[0].preFunctionCallings[1].functionCalled.name, 'setStateEnum'); //setStateEnum invocado pra setar stateEnum
      assert.equal(result[0].preFunctionCallings[1].parametersValues[0].stringfieldValue.obj, 0); //setStateEnum invocado para setar stateEnum para COVID.a
      //PARAMETERVALUES
      assert.equal(result[0].parametersValues.length, 1); //transaction parameter
      //cenário 1.1 (entra no IF e quebra o require)
      assert.isFalse(result[1].success);
      //PRE-CALLINGS
      assert.equal(result[1].preFunctionCallings.length, 2);
      assert.equal(result[1].preFunctionCallings[0].functionCalled.name, 'constructor'); //construtor invocado pra setar stateUint
      assert.isAtLeast(result[1].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj, 11); //construtor invocado intanciando stateUint com valor maior que 10
      assert.equal(result[1].preFunctionCallings[1].functionCalled.name, 'setStateEnum'); //setStateEnum invocado pra setar stateEnum
      assert.notEqual(result[1].preFunctionCallings[1].parametersValues[0].stringfieldValue.obj, 0); //setStateEnum invocado para setar stateEnum para valor diferente de COVID.a
      //PARAMETERVALUES
      assert.equal(result[1].parametersValues.length, 1); //transaction parameter

      //cenário 2 (não entra no IF)
      assert.isTrue(result[2].success);
      //PRE-CALLINGS
      // Não precisa setar stateEnum usada pelo require que está dentro do IF
      //nem vai setar o stateUint pois o valor inicial dele ZERO é suficiente para não entrar no IF sem precisar de precalling
      assert.equal(result[2].preFunctionCallings.length, 0);
      /*assert.equal(result[2].preFunctionCallings[0].functionCalled.name, 'constructor'); //construtor invocado pra setar stateUint
      assert.isAtMost(result[2].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj, 10); //construtor invocado intanciando stateUint com valor <= 10*/
      //PARAMETERVALUES
      assert.equal(result[2].parametersValues.length, 1); //transaction parameter
    });
    it('Deve retornar um branch do IF, um branch para cada ELSEIF e um branch que não entre em nenhum deles. Um cenário adicional deve ser gerado para quebrar o REQUIRE que o IF contém e outro para quebrar o REQUIRE que o ELSEIF contém', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(contract, 'ifElseifWithRequire_stateVar_Literal');
      const result = getFunctionParametersValuesScenarios(truffleProject, funcTest, contract);

      assert.isArray(result);
      assert.equal(result.length, 6); //4 branches (IF/ELSEIF/ELSEIF/ELSE) de sucesso, mais 2 requires dentro do IF e ELSEIF
      //cenário 1 (entra no IF e passa pelo require)
      assert.isTrue(result[0].success);
      //PRE-CALLINGS
      assert.equal(result[0].preFunctionCallings.length, 2);
      assert.equal(result[0].preFunctionCallings[0].functionCalled.name, 'constructor'); //construtor invocado pra setar stateUint
      assert.isAtLeast(result[0].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj, 11); //construtor invocado intanciando stateUint com valor maior que 10
      assert.equal(result[0].preFunctionCallings[1].functionCalled.name, 'setStateEnum'); //setStateEnum invocado pra setar stateEnum
      assert.equal(result[0].preFunctionCallings[1].parametersValues[0].stringfieldValue.obj, 0); //setStateEnum invocado para setar stateEnum para COVID.a
      //PARAMETERVALUES
      assert.equal(result[0].parametersValues.length, 1); //transaction parameter
      //cenário 1.1 (entra no IF e quebra o require)
      assert.isFalse(result[1].success);
      //PRE-CALLINGS
      assert.equal(result[1].preFunctionCallings.length, 2);
      assert.equal(result[1].preFunctionCallings[0].functionCalled.name, 'constructor'); //construtor invocado pra setar stateUint
      assert.isAtLeast(result[1].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj, 11); //construtor invocado intanciando stateUint com valor maior que 10
      assert.equal(result[1].preFunctionCallings[1].functionCalled.name, 'setStateEnum'); //setStateEnum invocado pra setar stateEnum
      assert.notEqual(result[1].preFunctionCallings[1].parametersValues[0].stringfieldValue.obj, 0); //setStateEnum invocado para setar stateEnum para valor diferente de COVID.a
      //PARAMETERVALUES
      assert.equal(result[1].parametersValues.length, 1); //transaction parameter

      //cenário 2 (entre no ELSEIF e passa no require)
      assert.isTrue(result[2].success);
      //PRE-CALLINGS
      assert.equal(result[2].preFunctionCallings.length, 2);
      assert.equal(result[2].preFunctionCallings[0].functionCalled.name, 'constructor'); //construtor invocado pra setar stateUint
      assert.notEqual(result[2].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj, 0); //construtor invocado intanciando stateUint com valor diferente de zero
      assert.equal(result[2].preFunctionCallings[1].functionCalled.name, 'setStateEnum'); //setStateEnum invocado pra setar stateEnum
      assert.equal(result[2].preFunctionCallings[1].parametersValues[0].stringfieldValue.obj, 1); //setStateEnum invocado para setar stateEnum para valor COVID.b
      //PARAMETERVALUES
      assert.equal(result[2].parametersValues.length, 1); //transaction parameter
      //cenário 2.1 (entre no ELSEIF e não passa no require)
      assert.isFalse(result[3].success);
      //PRE-CALLINGS
      assert.equal(result[3].preFunctionCallings.length, 2);
      assert.equal(result[3].preFunctionCallings[0].functionCalled.name, 'constructor'); //construtor invocado pra setar stateUint
      assert.equal(result[3].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj, 0); //construtor invocado intanciando stateUint com zero
      assert.equal(result[3].preFunctionCallings[1].functionCalled.name, 'setStateEnum'); //setStateEnum invocado pra setar stateEnum
      assert.equal(result[3].preFunctionCallings[1].parametersValues[0].stringfieldValue.obj, 1); //setStateEnum invocado para setar stateEnum para valor COVID.b
      //PARAMETERVALUES
      assert.equal(result[3].parametersValues.length, 1); //transaction parameter
      //cenário 3 (entre no ELSEIF)
      assert.isTrue(result[4].success);
      //PRE-CALLINGS
      assert.equal(result[4].preFunctionCallings.length, 1);
      assert.equal(result[4].preFunctionCallings[0].functionCalled.name, 'setStateEnum'); //setStateEnum invocado pra setar stateEnum
      assert.equal(result[4].preFunctionCallings[0].parametersValues[0].stringfieldValue.obj, 2); //setStateEnum invocado para setar stateEnum para valor COVID.c
      //PARAMETERVALUES
      assert.equal(result[4].parametersValues.length, 1); //transaction parameter
      //cenário 4 (não entra em nenhum dos IF/ELSEIFs)
      assert.isTrue(result[5].success);
      //PRE-CALLINGS
      assert.equal(result[5].preFunctionCallings.length, 1);
      //o valor inicial do stateUint é ZERO e, logo, é menor que 10, não precisa chamar novo construtor
      //assert.equal(result[5].preFunctionCallings[0].functionCalled.name, 'constructor'); //construtor invocado pra setar stateUint
      //assert.isAtMost(result[5].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj, 10); //construtor invocado intanciando stateUint valor <= 10
      assert.equal(result[5].preFunctionCallings[0].functionCalled.name, 'setStateEnum'); //setStateEnum invocado pra setar stateEnum
      assert.equal(result[5].preFunctionCallings[0].parametersValues[0].stringfieldValue.obj, 0); //setStateEnum invocado para setar stateEnum para valor COVID.a
      //PARAMETERVALUES
      assert.equal(result[5].parametersValues.length, 1); //transaction parameter
    });
  });
});
