import path from 'path';
import chai from 'chai';
import 'mocha';
import { IScenario } from '../../models/scenario/IScenario';
import { TruffleProjectIceFactoryFunction } from '../../services/TruffleFactory';
import { IContract } from '../../models/IContract';
import { getFunctionParametersValuesScenarios, initReferences } from '../../services/scenarios';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { extractTruffleProjectGraph } from '../../services/TruffleProjectGraph';
import { getParametersValuesForTest } from '../utils';

const assert = chai.assert;

import { getTruffleProject, getContractByName, getFunctionByName } from '../utils';

describe('Scenarios: getFunctionParametersValuesScenarios - state variable x functionParameter', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contracts: IContract[];
  let contract: ContractDefinition;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../'));
    contracts = truffleProject.getContracts();
    contract = getContractByName(contracts, 'RestrictionTypesTest');
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  beforeEach(() => {
    //inicializando variáveis de estado
    global['defaultReferences'] = {};
    global['defaultReferences'][contract.name] = {};
    global['defaultReferences'][contract.name][contract.name.concat('.', 'stateUint')] = {
      type: 'uint',
      subtype: null,
      stringfieldValue: {
        isCode: false,
        js: '10',
        sol: '10',
        obj: 10,
      },
    };
  });

  describe('Basic tests', function () {
    it('Quando restrição de basear em stateVar que não tem função que faça atribuição, não haverá precalling', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(
        contract,
        'simpleRequires_StateVar_functionParameter_withoutSetFunction'
      );

      const result: IScenario[] = getFunctionParametersValuesScenarios(
        truffleProject,
        funcTest,
        contract,
        getParametersValuesForTest
      );
      assert.isArray(result);
      assert.equal(result.length, 1); //1 cenário de sucesso
      assert.isTrue(result[0].success);
      //PRE-CALLINGS
      assert.equal(result[0].preFunctionCallings.length, 0);
    });
    it('require(stateVar > parameterUint);  parameterUint  < stateVar; stateVar2 == parameterString; functionParameter != 0: retornará cenário de sucesso com os parâmetros, 4 cenários de falha (um para cada restrição)', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(contract, 'simpleRequires_StateVar_functionParameter');
      global['defaultReferences'] = {};
      const result: IScenario[] = getFunctionParametersValuesScenarios(truffleProject, funcTest, contract);
      assert.isArray(result);
      assert.equal(result.length, 5); //1 cenário de sucesso e 4 falhos (1 para cada restrição)
      assert.isTrue(result[0].success);
      //PRE-CALLINGS
      assert.equal(result[0].preFunctionCallings.length, 1); //Uma função, o construtor, chamada antes de executar a função testada
      assert.equal(result[0].preFunctionCallings[0].functionCalled.name, 'constructor'); //construtor invocado antes de executar a função testada
      assert.isAtLeast(result[0].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj, 1); //construtor invocado intanciando stateUint com valor maior que zero
      assert.isAtMost(result[0].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj, 999); //construtor invocado intanciando stateUint com valor menor que 1000
      //PARAMETERSVALUES
      assert.equal(result[0].parametersValues.length, 5); //4 parãmetros de função mais 1 de transação
      assert.equal(result[0].parametersValues[0].type, 'address');
      assert.isNull(result[0].parametersValues[0].subtype);
      assert.equal(result[0].parametersValues[0].stringfieldValue.js.indexOf('0x0'), -1);
      assert.equal(result[0].parametersValues[1].type, 'uint256');
      assert.isNull(result[0].parametersValues[1].subtype);
      //o value1 tem que ser MENOR do que o valor passado no construtor e será atribuído a steateUint
      assert.isBelow(
        result[0].parametersValues[1].stringfieldValue.obj,
        result[0].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj
      );
      //o value2 tem que ser MAIOR do que o valor passado no construtor e será atribuído a steateUint
      assert.isAbove(
        result[0].parametersValues[2].stringfieldValue.obj,
        result[0].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj
      );

      // ### primeiro falho: require(stateUint > value1)
      assert.isFalse(result[1].success);
      //PRE-CALLINGS
      assert.equal(result[1].preFunctionCallings.length, 1); //Uma função, o construtor, chamada antes de executar a função testada
      assert.equal(result[1].preFunctionCallings[0].functionCalled.name, 'constructor'); //construtor invocado antes de executar a função testada
      //PARAMETERSVALUES
      assert.equal(result[1].parametersValues.length, 5); //4 parâmetros de função mais 1 de transação
      assert.isArray(result[1].parametersValues);
      assert.equal(result[1].parametersValues[0].type, 'address');
      assert.isNull(result[1].parametersValues[0].subtype);
      assert.equal(result[1].parametersValues[0].stringfieldValue.js.indexOf('0x0'), -1);
      //o value1 tem que ser MAIOR OU IGUAL do que o valor passado no construtor e será atribuído a steateUint
      //para que possa falhar exatamente neste require
      assert.isAtLeast(
        result[1].parametersValues[1].stringfieldValue.obj,
        result[1].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj
      );
      //o value2 tem que ser MAIOR do que o valor passado no construtor e será atribuído a steateUint
      assert.isAbove(
        result[1].parametersValues[2].stringfieldValue.obj,
        result[1].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj
      );
      // ### segundo falho: require(0 < stateUint)
      assert.isFalse(result[1].success);
      //PRE-CALLINGS
      assert.equal(result[2].preFunctionCallings.length, 1); //Uma função, o construtor, chamada antes de executar a função testada
      assert.equal(result[2].preFunctionCallings[0].functionCalled.name, 'constructor'); //construtor invocado antes de executar a função testada
      //PARAMETERSVALUES
      assert.equal(result[2].parametersValues.length, 5); //4 parãmetros de função mais 1 de transação
      //o value1 tem que ser MAIOR OU IGUAL do que o valor passado no construtor e será atribuído a steateUint
      //para que possa falhar exatamente neste require
      assert.isAtLeast(
        result[2].parametersValues[1].stringfieldValue.obj,
        result[2].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj
      );
      //o value2 tem que ser MAIOR do que o valor passado no construtor e será atribuído a steateUint
      assert.isAbove(
        result[2].parametersValues[2].stringfieldValue.obj,
        result[2].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj
      );
      // ### terceiro falho: stateString == value3 && stateUint < value2
      assert.isFalse(result[3].success);
      //PRE-CALLINGS
      //No contrato do teste não existe função que altere o state de stateString,
      //assim, restrição de stateString será ignorada e apenas a da stateUint será considerada (se existisse, seriam dois cenários distintos)
      assert.equal(result[3].preFunctionCallings.length, 1);
      assert.equal(result[3].preFunctionCallings.length, 1); //Uma função, o construtor, chamada antes de executar a função testada
      assert.equal(result[3].preFunctionCallings[0].functionCalled.name, 'constructor'); //construtor invocado antes de executar a função testada
      //PARAMETERSVALUES
      assert.equal(result[3].parametersValues.length, 5); //4 parãmetros de função mais 1 de transação
      //o value1 tem que ser MENOR do que o valor passado no construtor e será atribuído a steateUint
      assert.isBelow(
        result[3].parametersValues[1].stringfieldValue.obj,
        result[3].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj
      );
      //o value2 tem que ser MENOR OU IGUAL do que o valor passado no construtor e será atribuído a steateUint
      //para que possa falhar exatamente neste require
      assert.isAtMost(
        result[3].parametersValues[2].stringfieldValue.obj,
        result[3].preFunctionCallings[0].parametersValues[2].stringfieldValue.obj
      );
      // ### quarto falho: require(recipient != address(0)
      assert.isFalse(result[4].success);
      //PRE-CALLINGS
      assert.equal(result[4].preFunctionCallings.length, 1);
      //em cenários de falha que não envolvam variáveis de estado, o preFunctionCallings é idẽntico ao do cenário de sucesso
      assert.equal(
        JSON.stringify(result[4].preFunctionCallings[0].parametersValues),
        JSON.stringify(result[0].preFunctionCallings[0].parametersValues)
      );
      //PARAMETERSVALUES
      assert.equal(result[4].parametersValues.length, 5); //4 parãmetros de função mais 1 de transação
      assert.isArray(result[4].parametersValues);
      assert.equal(result[4].parametersValues[0].type, 'address');
      assert.isNull(result[4].parametersValues[0].subtype);
      assert.equal(result[4].parametersValues[0].stringfieldValue.js, '"0x0000000000000000000000000000000000000000"');
    });
  });
  describe('Scenarios com branches sem REQUIREs', function () {
    it('if(stateUint > paramUint){}: deve retornar um cenário que entre neste IF e outro que não entre. Ambos SUCESSO', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(contract, 'if_stateVar_functionParameter');

      const result: IScenario[] = getFunctionParametersValuesScenarios(truffleProject, funcTest, contract);
      assert.isArray(result);
      assert.equal(result.length, 2); //2 cenários de sucesso
      //cenário 1
      assert.isTrue(result[0].success);
      //PRE-CALLINGS
      //ao gerar valor para parâmetro na chamada da função, gera atendendo a restrição e não precisa de preCalling
      assert.equal(result[0].preFunctionCallings.length, 0);
      //PARAMETERVALUES - um parâmetro + o transactionParameter
      assert.equal(result[0].parametersValues.length, 2);
      //o param tem que ser MENOR do que o valor passado no construtor e será atribuído a steateUint
      assert.isBelow(result[0].parametersValues[0].stringfieldValue.obj, 10);
      //cenário 2
      assert.isTrue(result[1].success);
      //PRE-CALLINGS
      //ao gerar valor para parâmetro na chamada da função, gera atendendo a restrição e não precisa de preCalling
      assert.equal(result[1].preFunctionCallings.length, 0);
      //PARAMETERVALUES
      //o param tem que ser MAIOR OU IGUAL do que o valor passado no construtor e será atribuído a steateUint
      assert.isAtLeast(result[1].parametersValues[0].stringfieldValue.obj, 10);
    });

    it('Quando for identificado um branch válido baseado em um IF e ELSE IF, deve retornar um cenário para cada if, else if e else. Todos SUCESSO', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(contract, 'ifElseIfElse_stateVar_functionParameter');

      const result: IScenario[] = getFunctionParametersValuesScenarios(truffleProject, funcTest, contract);
      assert.isArray(result);
      assert.equal(result.length, 3); //3 cenários de sucesso

      //stateUint == param1
      assert.isTrue(result[0].success);
      //PRE-CALLINGS
      //ao gerar valor do parâmetro na chamada da função, gera de forma a atender a restrição e não precisa de preCalling
      assert.equal(result[0].preFunctionCallings.length, 0);
      //PARAMETERVALUES
      assert.equal(result[0].parametersValues.length, 3); //2 parãmetros de função mais 1 de transação
      assert.equal(result[0].parametersValues[0].type, 'uint256');
      assert.equal(result[0].parametersValues[1].type, 'uint256');
      assert.isNull(result[0].parametersValues[0].subtype);
      assert.isNull(result[0].parametersValues[1].subtype);
      //o param1 tem que ser IGUAL ao valor passado no construtor e será atribuído a steateUint
      assert.equal(result[0].parametersValues[0].stringfieldValue.obj, 10);
      //stateUint > param2
      assert.isTrue(result[1].success);
      //PRE-CALLINGS
      //ao gerar valor do parâmetro na chamada da função, gera de forma a atender a restrição e não precisa de preCalling
      assert.equal(result[1].preFunctionCallings.length, 0);
      //o param1 não pode ser igual ao stateUint, senão entrará no IF
      //e o param2 deve ser menor que o stateUint para entrar no elseIf
      assert.notEqual(result[1].parametersValues[0].stringfieldValue.obj, 10);
      assert.isBelow(result[1].parametersValues[1].stringfieldValue.obj, 10);
      //else
      //stateUint != param1 && stateUint <= param2
      assert.isTrue(result[2].success);
      //PRE-CALLINGS
      ////ao gerar valor do parâmetro na chamada da função, gera de forma a atender a restrição e não precisa de preCalling
      assert.equal(result[2].preFunctionCallings.length, 0);
      //o param1 não pode ser igual ao stateUint, senão entrará no IF
      //e o param2 deve ser MAIOR OU IGUAL que o stateUint, senão entrará no elseIf
      assert.notEqual(result[2].parametersValues[0].stringfieldValue.obj, 10);
      assert.isAtLeast(result[2].parametersValues[1].stringfieldValue.obj, 10);
    });

    it('Deve retornar um cenário de teste para IF que compara stateEnum e outro para o else Ambos SUCESSO', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(
        contract,
        'ifWithConditionBasedOnEnum_stateVar_functionParameter'
      );

      const result: IScenario[] = getFunctionParametersValuesScenarios(truffleProject, funcTest, contract);
      assert.isArray(result);
      assert.equal(result.length, 2); //2 cenários de sucesso
      //cenário 1
      assert.isTrue(result[0].success);
      //PRE-CALLINGS
      assert.equal(result[0].preFunctionCallings.length, 1); //Uma função, o setStateEnum, chamada antes de executar a função testada
      assert.equal(result[0].preFunctionCallings[0].functionCalled.name, 'setStateEnum'); //setStateEnum invocado antes de executar a função testada
      assert.equal(result[0].preFunctionCallings[0].parametersValues[0].stringfieldValue.obj, 0); //setStateEnum invocado intanciando stateEnum com valor igual a COVID.a (0)
      //PARAMETERVALUES
      assert.equal(result[0].parametersValues.length, 2); //enumParam + transactionParameter
      assert.equal(result[0].parametersValues[0].type, 'UserDefinedTypeName');
      assert.equal(result[0].parametersValues[1].type, 'TransactionParameter');
      //o enumParam tem que ser igual ao parâmetro setado no preCalling para entrar no IF
      assert.equal(
        result[0].parametersValues[0].stringfieldValue.obj,
        result[0].preFunctionCallings[0].parametersValues[0].stringfieldValue.obj
      );
      //cenário 2
      assert.isTrue(result[1].success);
      //PRE-CALLINGS
      assert.equal(result[1].preFunctionCallings.length, 1); //Uma função, o setStateEnum, chamada antes de executar a função testada
      assert.equal(result[1].preFunctionCallings[0].functionCalled.name, 'setStateEnum'); //setStateEnum invocado antes de executar a função testada
      assert.notEqual(result[1].preFunctionCallings[0].parametersValues[0].stringfieldValue.obj, 0); //setStateEnum invocado intanciando stateEnum com valor diferente de COVID.a
      //PARAMETERVALUES
      assert.equal(result[1].parametersValues.length, 2); //enumParam + transactionParameter
      assert.equal(result[1].parametersValues[0].type, 'UserDefinedTypeName');
      assert.equal(result[1].parametersValues[1].type, 'TransactionParameter');
      //o enumParam tem que ser DIFERENTE do parâmetro setado no preCalling para entrar no ELSE
      assert.notEqual(
        result[0].parametersValues[0].stringfieldValue.obj,
        result[0].preFunctionCallings[0].parametersValues[0].stringfieldValue.obj
      );
    });
  });

  describe('Scenarios com branches IFs e REQUIREs', () => {
    it('Para cada branch válido baseado em um IF, deve retornar um cenário que entre neste IF e outro que não entre. Um cenário adicional deve ser gerado para quebrar o REQUIRE que o IF contém', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(
        contract,
        'ifElseifWithRequire_stateVar_functionParameter'
      );
      const result = getFunctionParametersValuesScenarios(truffleProject, funcTest, contract);

      assert.isArray(result);
      //Continuar esses cenários pra entender como que as coisas se acumulam. Na prática, deveria ter 3 branches
      //1. entra no IF e passa no require (sucesso)
      //2. entra no IF e não passa no require (falha)
      //3. não entra no IF (sucesso)
      assert.equal(result.length, 6); //4 branches (IF/ELSEIF/ELSEIF/ELSE) de sucesso, mais 2 requires dentro do IF e ELSEIF
      //cenário 1 (entra no IF e passa pelo require)
      assert.isTrue(result[0].success);
      //PRE-CALLINGS
      assert.equal(result[0].preFunctionCallings.length, 1);
      assert.equal(result[0].preFunctionCallings[0].functionCalled.name, 'setStateEnum'); //setStateEnum invocado pra setar stateEnum
      //PARAMETERVALUES
      assert.equal(result[0].parametersValues.length, 2); // param + transaction parameter
      //o param tem que ser MENOR que a state setada no beforeEach para entrar no IF
      assert.isBelow(result[0].parametersValues[0].stringfieldValue.obj, 10);
      //cenário 1.1 (entra no IF e quebra o require)
      assert.isFalse(result[1].success);
      //PRE-CALLINGS
      assert.equal(result[1].preFunctionCallings.length, 1);
      assert.equal(result[1].preFunctionCallings[0].functionCalled.name, 'setStateEnum'); //setStateEnum invocado pra setar stateEnum
      assert.notEqual(result[1].preFunctionCallings[0].parametersValues[0].stringfieldValue.obj, 0); //setStateEnum invocado para setar stateEnum para valor diferente de COVID.a
      //PARAMETERVALUES
      assert.equal(result[1].parametersValues.length, 2); //param + transaction parameter
      //o param tem que ser MENOR que a state setada no beforeEach
      assert.isBelow(result[1].parametersValues[0].stringfieldValue.obj, 10);

      //cenário 2 (não entra no IF)
      //cenário 2 (entre no ELSEIF e passa no require)
      assert.isTrue(result[2].success);
      //PRE-CALLINGS
      assert.equal(result[2].preFunctionCallings.length, 1);
      assert.equal(result[2].preFunctionCallings[0].functionCalled.name, 'setStateEnum'); //setStateEnum invocado pra setar stateEnum
      assert.equal(result[2].preFunctionCallings[0].parametersValues[0].stringfieldValue.obj, 1); //setStateEnum invocado para setar stateEnum para valor COVID.b
      //PARAMETERVALUES
      assert.equal(result[2].parametersValues.length, 2); //param + transaction parameter
      assert.isAtLeast(result[2].parametersValues[0].stringfieldValue.obj, 10);
    });
  });
});
