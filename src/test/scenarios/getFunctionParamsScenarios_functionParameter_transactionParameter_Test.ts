import path from 'path';
import chai from 'chai';
import 'mocha';
import { IScenario } from '../../models/scenario/IScenario';
import { TruffleProjectIceFactoryFunction } from '../../services/TruffleFactory';
import { IContract } from '../../models/IContract';
import { getFunctionParametersValuesScenarios } from '../../services/scenarios';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { extractTruffleProjectGraph } from '../../services/TruffleProjectGraph';
import { getParametersValuesForTest } from '../utils';

const assert = chai.assert;

import { getTruffleProject, getContractByName, getFunctionByName } from '../utils';

describe('Scenarios: getFunctionParametersValuesScenarios - functionParameter x TransactionParameter', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contract: ContractDefinition;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../'));
    contract = getContractByName(truffleProject.getContracts(), 'RestrictionTypesTest');
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  describe('Scenarios com branches sem REQUIREs', function () {
    it('Deve retornar um cenário para IF que compara parametro de transação (msg.value) e outro pro ELSE. Ambos SUCESSO', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(
        contract,
        'ifWithCondition_transactionParameter_functionParameter'
      );

      const result: IScenario[] = getFunctionParametersValuesScenarios(
        truffleProject,
        funcTest,
        contract,
        getParametersValuesForTest
      );
      assert.isArray(result);
      assert.equal(result.length, 2); //2 cenários de sucesso
      //cenário 1
      assert.isTrue(result[0].success);
      assert.equal(result[0].parametersValues.length, 3); //2 parãmetro de função mais 1 de transação
      assert.equal(result[0].parametersValues[0].type, 'address');
      assert.isNull(result[0].parametersValues[0].subtype);
      assert.isNotNull(result[0].parametersValues[0].stringfieldValue.js);
      assert.equal(result[0].parametersValues[1].type, 'uint256');
      assert.isNull(result[0].parametersValues[1].subtype);
      assert.isAtMost(
        result[0].parametersValues[1].stringfieldValue.obj,
        result[0].parametersValues[2].stringfieldValue.obj.value
      ); //valor pra entrar no IF
      assert.equal(result[0].parametersValues[2].type, 'TransactionParameter');
      //cenário 2
      assert.isTrue(result[1].success);
      assert.equal(result[1].parametersValues.length, 3); //2 parãmetro de função mais 1 de transação
      assert.equal(result[1].parametersValues[0].type, 'address');
      assert.isNull(result[1].parametersValues[0].subtype);
      assert.equal(result[1].parametersValues[1].type, 'uint256');
      assert.isNull(result[1].parametersValues[1].subtype);
      assert.isAtMost(
        result[1].parametersValues[2].stringfieldValue.obj.value,
        result[1].parametersValues[1].stringfieldValue.obj
      );
      assert.equal(result[1].parametersValues[2].type, 'TransactionParameter');
    });
  });
});
