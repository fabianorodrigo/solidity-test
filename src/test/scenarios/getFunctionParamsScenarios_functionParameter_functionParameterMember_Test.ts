import path from 'path';
import chai from 'chai';
import 'mocha';
import { IScenario } from '../../models/scenario/IScenario';
import { TruffleProjectIceFactoryFunction } from '../../services/TruffleFactory';
import { IContract } from '../../models/IContract';
import { getFunctionParametersValuesScenarios } from '../../services/scenarios';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { extractTruffleProjectGraph } from '../../services/TruffleProjectGraph';
import { getParametersValuesForTest } from '../utils';

const assert = chai.assert;

import { getTruffleProject, getContractByName, getFunctionByName } from '../utils';

describe('Scenarios: getFunctionParametersValuesScenarios', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contract: ContractDefinition;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../'));
    contract = getContractByName(truffleProject.getContracts(), 'RestrictionTypesTest');
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  describe('Scenarios com branches sem REQUIREs', function () {
    it('Deve retornar um cenário de teste para IF que compara atributo de parãmetro de entrada struct e outro para o else Ambos SUCESSO', async function () {
      const contracts: IContract[] = truffleProject.getContracts();
      const contractDefinition = getContractByName(contracts, 'fullOfBranchesAndRestrictions');

      const result: IScenario[] = getFunctionParametersValuesScenarios(
        truffleProject,
        getFunctionByName(contractDefinition, 'ifWithConditionBasedOnStructMember'),
        contractDefinition
      );
      assert.isArray(result);
      assert.equal(result.length, 2); //2 cenários de sucesso
      //cenário 1
      assert.isTrue(result[0].success);
      assert.equal(result[0].parametersValues.length, 2); //1 parãmetro de função mais 1 de transação
      assert.equal(result[0].parametersValues[0].type, 'UserDefinedTypeName');
      assert.equal(result[0].parametersValues[0].subtype, 'fullOfBranchesAndRestrictions.IPS');
      assert.isNotNull(result[0].parametersValues[0].stringfieldValue.js); //valor pra entrar no IF
      const regexSol = /fullOfBranchesAndRestrictions.IPS\(([0-9]{2,}),fullOfBranchesAndRestrictions\.P\.(D|W|M|Q|H|Y)\)/gm;
      const regexJs = /{"a": ([0-9]{2,}),"b": (0|1|2|3|4|5)}/gm;
      //as expressões regulares validam se está instanciando o struct com os valores que atendem ao IF
      //Como o IF é "">= 10", então o tamanho do número do primeiro parâmetro é de pelo menos 2 caracteres
      assert.isTrue(
        regexSol.test(result[0].parametersValues[0].stringfieldValue.sol),
        result[0].parametersValues[0].stringfieldValue.sol
      );
      assert.isTrue(
        regexJs.test(result[0].parametersValues[0].stringfieldValue.js),
        result[0].parametersValues[0].stringfieldValue.js
      );
      assert.equal(result[0].parametersValues[1].type, 'TransactionParameter');
      //cenário 2
      assert.isTrue(result[1].success);
      assert.equal(result[1].parametersValues.length, 2); //1 parãmetro de função mais 1 de transação
      assert.equal(result[1].parametersValues[0].type, 'UserDefinedTypeName');
      assert.equal(result[1].parametersValues[0].subtype, 'fullOfBranchesAndRestrictions.IPS');
      assert.isNotNull(result[1].parametersValues[0].stringfieldValue.js); //valor pra entrar no IF
      const regexElseSol = /fullOfBranchesAndRestrictions.IPS\(([0-9]{1}|0x01),fullOfBranchesAndRestrictions\.P\.(D|W|M|Q|H|Y)\)/gm;
      const regexElseJs = /{"a": ([0-9]{1}),"b": (0|1|2|3|4|5)}/gm;
      //as expressões regulares validam se está instanciando o struct com os valores que atendem ao IF
      //Como o IF é "">= 10", então o tamanho do número do primeiro parâmetro é de apenas 1 caracter
      assert.isTrue(
        regexElseSol.test(result[1].parametersValues[0].stringfieldValue.sol),
        result[1].parametersValues[0].stringfieldValue.sol
      );
      assert.isTrue(
        regexElseJs.test(result[1].parametersValues[0].stringfieldValue.js),
        result[1].parametersValues[0].stringfieldValue.js
      );
    });
  });
});
