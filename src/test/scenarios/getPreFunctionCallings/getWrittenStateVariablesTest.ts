import path from 'path';
import chai from 'chai';
import 'mocha';
import { TruffleProjectIceFactoryFunction } from '../../../services/TruffleFactory';
import { IContract } from '../../../models/IContract';
import { ContractDefinition } from 'solidity-parser-antlr';

const assert = chai.assert;

import { getTruffleProject, getContractByName, getFunctionByName } from '../../utils';
import { extractTruffleProjectGraph } from '../../../services/TruffleProjectGraph';
import { TypeReferences } from '../../../models/scenario';
import {
  r_msgValue_leq_paramMax,
  r_stateVariableX_gre_number7,
  r_stateVariable_source_eq_stringUnirio,
} from '../../restrictionsConstants';
import { getWrittenStateVariables } from '../../../services/scenarios/preCallings/getWrittenStateVariables';
import { ITruffleProjectGraph, IStateVariableSetGraph, SourceVariableSetType } from '../../../models/graph';

describe('Scenarios.PreFunctionCallings: getWrittenStateVariables', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let references: TypeReferences = {};
  let grafo: ITruffleProjectGraph;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
    //setando apenas transactionParameter como null
    references = {};
    references['transactionParameter'] = null;
    //apurando e setando o grafo do projeto
    grafo = extractTruffleProjectGraph(truffleProject);
    truffleProject.setGraph(grafo);
  });

  it('Se a FunctionGraph não escreve em nenhuma variável, retorna vazio', async function () {
    const fg = grafo.contracts['RequireUse'].functions['simpleRequires'];
    const result: { [functionSignature: string]: IStateVariableSetGraph[] } = getWrittenStateVariables(fg[0]);
    assert.equal(Object.keys(result).length, 0);
  });

  it('Se a FunctionGraph escreve apenas em variável local, retorna vazio', async function () {
    const fg = grafo.contracts['RequireUse'].functions['simpleLocalWrite'];
    const result: { [functionSignature: string]: IStateVariableSetGraph[] } = getWrittenStateVariables(fg[0]);
    assert.equal(Object.keys(result).length, 0);
  });

  it('Se a FunctionGraph escreve em variáveis de estado incondicionalmente, deve retorná-las', async function () {
    const fg = grafo.contracts['RequireUse'].functions['setDecimals'];
    const result: IStateVariableSetGraph[] = getWrittenStateVariables(fg.find((f) => f.parameters.length == 1))[
      'setDecimals(uint8)'
    ];
    assert.isArray(result);
    assert.equal(result.length, 2);
    assert.equal(result[0].name, '_decimals');
    assert.equal(result[0].conditional, false);
    assert.equal(result[0].source.name, 'd');
    assert.equal(result[0].source.type, SourceVariableSetType.parameter);
    assert.equal(result[1].name, '_symbol');
    assert.equal(result[1].conditional, false);
    assert.equal(result[1].source.name, '');
    assert.equal(result[1].source.type, SourceVariableSetType.literal);
    assert.equal(result[1].source.aditionalInfo, 'x');
  });

  it('Se a FunctionGraph escreve em uma única variável de estado mas de forma diferentes baseado em condição, vai retornar uma única vez', async function () {
    const fg = grafo.contracts['RequireUse'].functions['stateVariableWrittenConditionally'];
    const result: IStateVariableSetGraph[] = getWrittenStateVariables(fg[0])[
      'stateVariableWrittenConditionally(RestrictionTypesTest.StructTeste)'
    ];
    assert.isArray(result);
    assert.equal(result.length, 1);
    assert.equal(result[0].name, 'testeState');
    assert.equal(result[0].conditional, true);
    assert.equal(result[0].source.name, 'structWithEnum');
    assert.equal(result[0].source.type, SourceVariableSetType.parameterMemberAccess);
    /*TODO: Não deveria retornar as duas? Com suas respectivas restrições?
    assert.equal(result[1].name, 'testeState');
    assert.equal(result[1].conditional, true);
    assert.equal(result[1].source.name, 'value');
    assert.equal(result[1].source.type, SourceVariableSetType.transactionParameter);
    assert.equal(result[1].source.aditionalInfo, 'x');*/
  });

  it('Se a FunctionGraph não escreve em variável de estado mas chama uma função do próprio contrato que escreve em duas, deve retornar essas ocorrências', async function () {
    const fg = grafo.contracts['RequireUse'].functions['simpleFunctionCallThatHasRequiresTwiceTheSameParam'];
    const resultObj = getWrittenStateVariables(fg[0]);
    const result: IStateVariableSetGraph[] = resultObj['_internalFunction(address,address,uint256,uint256)'];
    assert.isArray(result);
    assert.equal(result.length, 2);
    assert.equal(result[0].name, 'testeState');
    assert.equal(result[0].conditional, true);
    assert.equal(result[0].source.name, 'value1'); //fica o nome do parâmetro da função de origem
    assert.equal(result[0].source.type, SourceVariableSetType.parameter);
    assert.equal(result[1].name, 'addressState');
    assert.equal(result[1].conditional, true);
    assert.equal(result[1].source.name, 'sender');
    assert.equal(result[1].source.type, SourceVariableSetType.transactionParameter); //fica o tipo do parâmetro da função de origem
  });

  it('Se a FunctionGraph não escreve variável de estado mas chama uma função do ancestral que escreve, deve retornar essa ocorrência', async function () {
    const fg = grafo.contracts['RequireUse'].functions['setDecimals'];
    const resultObj: { [functionSignature: string]: IStateVariableSetGraph[] } = getWrittenStateVariables(
      fg.find((f) => f.parameters.length == 2)
    );
    const resultSetName: IStateVariableSetGraph[] = resultObj['setName(string)'];
    assert.isArray(resultSetName);
    assert.equal(resultSetName.length, 1);
    assert.equal(resultSetName[0].name, '_name');
    assert.equal(resultSetName[0].conditional, false);
    assert.equal(resultSetName[0].source.name, 'n');
    assert.equal(resultSetName[0].source.aditionalInfo, 'teste');
    assert.equal(resultSetName[0].source.type, SourceVariableSetType.literal);
    const resultNoRestriction: IStateVariableSetGraph[] = resultObj['noRestriction(string)'];
    assert.equal(resultNoRestriction.length, 1);
    assert.equal(resultNoRestriction[0].name, '_name');
    assert.equal(resultNoRestriction[0].conditional, false);
    assert.equal(resultNoRestriction[0].source.name, 'name');
    assert.equal(resultNoRestriction[0].source.aditionalInfo, 'testeParent');
    assert.equal(resultNoRestriction[0].source.type, SourceVariableSetType.literal);
  });
});
