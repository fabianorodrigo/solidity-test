import path from 'path';
import chai from 'chai';
import 'mocha';
import { IScenario } from '../../../models/scenario/IScenario';
import { TruffleProjectIceFactoryFunction } from '../../../services/TruffleFactory';
import { IContract } from '../../../models/IContract';
import { getFunctionParametersValuesScenarios } from '../../../services/scenarios';
import { GetRandomValueFunction } from '../../../services/Utils/GetRandomValueFunction';
import { ElementaryTypeName, FunctionDefinition, ContractDefinition } from 'solidity-parser-antlr';

const assert = chai.assert;

import {
  truffleProjectEmpty,
  functionEmpty,
  contractEmpty,
  getTruffleProject,
  getContractByName,
  getFunctionByName,
  getParametersValuesForTest,
} from '../../utils';
import { extractTruffleProjectGraph } from '../../../services/TruffleProjectGraph';
import { IFunctionCalling } from '../../../models';
import { getPreFunctionCallings } from '../../../services/scenarios/preCallings';
import { TypeReferences } from '../../../models/scenario';
import { r_uneq_number7, r_msgSender_neq_to, r_msgValue_leq_paramMax } from '../../restrictionsConstants';

describe('Scenarios: getPreFunctionCallings', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contracts: IContract[];
  let contract: ContractDefinition;
  const references: TypeReferences = {};

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
    contracts = truffleProject.getContracts();
    contract = getContractByName(contracts, 'RequireUse');
    //setando apenas transactionParameter como null
    references['transactionParameter'] = null;
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  it('Se nenhuma restrição do tipo stateVariable for passada, deve retornar um array vazio', async function () {
    const result: IFunctionCalling[] = getPreFunctionCallings(
      truffleProject,
      contract,
      getFunctionByName(contract, 'simpleRequires'),
      [r_uneq_number7, r_msgSender_neq_to, r_msgValue_leq_paramMax],
      references
    );
    assert.isArray(result);
    assert.equal(result.length, 0);
  });
});
