import path from 'path';
import chai from 'chai';
import 'mocha';
import { TruffleProjectIceFactoryFunction } from '../../../services/TruffleFactory';

const assert = chai.assert;

import { getTruffleProject } from '../../utils';
import { extractTruffleProjectGraph } from '../../../services/TruffleProjectGraph';
import { TypeReferences } from '../../../models/scenario';
import { ITruffleProjectGraph, IStateVariableSetGraph, SourceVariableSetType } from '../../../models/graph';
import { IFunctionCalling } from '../../../models';
import { updateReferencesFromFunctionCalling } from '../../../services/scenarios';
import { TransactionParameterType } from '../../../services/Utils';

describe('Scenarios.PreFunctionCallings: updateReferencesFromFunctionCalling', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let references: TypeReferences = {};
  let grafo: ITruffleProjectGraph;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
    //setando apenas transactionParameter como null
    references = {};
    references['transactionParameter'] = null;
    //apurando e setando o grafo do projeto
    grafo = extractTruffleProjectGraph(truffleProject);
    truffleProject.setGraph(grafo);
  });

  it('Se a FunctionGraph não escreve em nenhuma variável, retorna apenas transaction parameter', async function () {
    references = {};
    const fg = grafo.contracts['RequireUse'].functions['simpleRequires'];
    const fgCalling: IFunctionCalling = {
      contractInstanceTarget: fg[0].contract.name,
      functionCalled: fg[0],
      parametersValues: simpleRequiresParameters,
      stateVariablesAffected: [],
    };
    const result: boolean = updateReferencesFromFunctionCalling(fgCalling, references);
    assert.isTrue(result);
    assert.equal(Object.keys(references).length, 1);
    assert.equal(references['transactionParameter'].stringfieldValue.obj.from, 'accounts[9]');
  });

  it('Se a FunctionGraph escreve apenas em variável local, retorna vazio', async function () {
    references = {};
    const fg = grafo.contracts['RequireUse'].functions['simpleLocalWrite'];
    const fgCalling: IFunctionCalling = {
      contractInstanceTarget: fg[0].contract.name,
      functionCalled: fg[0],
      parametersValues: [paramUint20],
      stateVariablesAffected: [],
    };
    const result: boolean = updateReferencesFromFunctionCalling(fgCalling, references);
    assert.isFalse(result);
    assert.equal(Object.keys(references).length, 0);
  });

  it('Se a FunctionGraph escreve em variáveis de estado incondicionalmente, deve retorná-las', async function () {
    references = {};
    const fg = grafo.contracts['RequireUse'].functions['setDecimals'].find((f) => f.parameters.length == 1);
    const fgCalling: IFunctionCalling = {
      contractInstanceTarget: fg.contract.name,
      functionCalled: fg,
      parametersValues: [paramUint20],
      stateVariablesAffected: [],
    };
    const result: boolean = updateReferencesFromFunctionCalling(fgCalling, references);
    assert.isTrue(result);
    assert.equal(Object.keys(references).length, 2);
    assert.equal(references[fg.contract.name.concat('.', '_decimals')].stringfieldValue.obj, 20);
    assert.equal(references[fg.contract.name.concat('.', '_symbol')].stringfieldValue.obj, 'x');
  });

  it('Se a FunctionGraph escreve em uma única variável de estado mas de forma diferentes baseado em condição, vai retornar uma única vez', async function () {
    references = {};
    const fg = grafo.contracts['RequireUse'].functions['stateVariableWrittenConditionally'][0];
    const fgCalling: IFunctionCalling = {
      contractInstanceTarget: fg.contract.name,
      functionCalled: fg,
      parametersValues: [paramStruct as any],
      stateVariablesAffected: [],
    };
    const result: boolean = updateReferencesFromFunctionCalling(fgCalling, references);
    assert.isTrue(result);
    assert.equal(Object.keys(references).length, 1);
    assert.equal(references[fg.contract.name.concat('.', 'testeState')].stringfieldValue.obj, 0);

    /*TODO: Não deveria retornar as duas? Com suas respectivas restrições?
    assert.equal(result[1].name, 'testeState');
    assert.equal(result[1].conditional, true);
    assert.equal(result[1].source.name, 'value');
    assert.equal(result[1].source.type, SourceVariableSetType.transactionParameter);
    assert.equal(result[1].source.aditionalInfo, 'x');*/
  });

  it('Se a FunctionGraph não escreve variável de estado mas chama uma função do próprio contrato que escreve (repassando parâmetro), deve retornar essa ocorrência', async function () {
    references = {};
    const fg = grafo.contracts['RequireUse'].functions['simpleFunctionCallThatHasRequiresTwiceTheSameParam'][0];
    const fgCalling: IFunctionCalling = {
      contractInstanceTarget: fg.contract.name,
      functionCalled: fg,
      parametersValues: simpleRequiresParameters,
      stateVariablesAffected: [],
    };
    const result: boolean = updateReferencesFromFunctionCalling(fgCalling, references);
    assert.isTrue(result);
    assert.equal(Object.keys(references).length, 3);
    assert.equal(references[fg.contract.name.concat('.', 'testeState')].stringfieldValue.obj, 10);
    assert.equal(references[fg.contract.name.concat('.', 'addressState')].stringfieldValue.obj, 'accounts[9]');
    assert.equal(references['transactionParameter'].stringfieldValue.obj.from, 'accounts[9]');
  });

  it('Se a FunctionGraph não escreve variável de estado mas chama uma função do próprio contrato que escreve (repassando transaction parameter em um e literal em outro), deve retornar essa ocorrência', async function () {
    references = {};
    const fg = grafo.contracts['RequireUse'].functions['simpleFunctionCallThatHasRequires'][0];
    const fgCalling: IFunctionCalling = {
      contractInstanceTarget: fg.contract.name,
      functionCalled: fg,
      parametersValues: simpleRequiresParameters,
      stateVariablesAffected: [],
    };
    const result: boolean = updateReferencesFromFunctionCalling(fgCalling, references);
    assert.isTrue(result);
    assert.equal(Object.keys(references).length, 3);
    assert.equal(references[fg.contract.name.concat('.', 'testeState')].stringfieldValue.obj, 2);
    assert.equal(references[fg.contract.name.concat('.', 'addressState')].stringfieldValue.obj, 'accounts[9]');
    assert.equal(references['transactionParameter'].stringfieldValue.obj.from, 'accounts[9]');
  });

  it('Se a FunctionGraph não escreve variável de estado mas chama uma função do próprio contrato que escreve e depois uma função do ancestral que escreve na mesma variável, deve retornar a última ocorrência', async function () {
    references = {};
    const fg = grafo.contracts['RequireUse'].functions['setDecimals'].find((f) => f.parameters.length == 2);
    const fgCalling: IFunctionCalling = {
      contractInstanceTarget: fg.contract.name,
      functionCalled: fg,
      parametersValues: [paramUint20, paramStringUnirio],
      stateVariablesAffected: [],
    };
    const result: boolean = updateReferencesFromFunctionCalling(fgCalling, references);
    assert.isTrue(result);
    assert.equal(Object.keys(references).length, 1);
    assert.equal(references[fg.contract.name.concat('.', '_name')].stringfieldValue.obj, 'testeParent');
  });
});

const paramUint0 = {
  type: 'uint',
  subtype: null,
  stringfieldValue: { isCode: false, js: '0', sol: '0', obj: 0 },
};
const paramUint20 = {
  type: 'uint',
  subtype: null,
  stringfieldValue: { isCode: false, js: '20', sol: '20', obj: 20 },
};
const paramStringUnirio = {
  type: 'string',
  subtype: null,
  stringfieldValue: { isCode: false, js: '"Unirio"', sol: '"Unirio"', obj: 'Unirio' },
};
const paramStruct = {
  type: 'UserDefinedTypeName',
  subtype: 'RequireUse.StructTeste',
  stringfieldValue: {
    isCode: false,
    js: '{"a": 0,"P": 1}',
    sol: 'RequireUse.StructTeste(0,RequireUse.P.a)',
    obj: { a: 0, b: 1 },
    typeName: {
      type: 'UserDefinedTypeName',
      namePath: 'StructTeste',
    },
  },
};
const simpleRequiresParameters = [
  {
    type: 'address',
    subtype: null,
    stringfieldValue: { isCode: false, js: 'accounts[0]', sol: '0x0', obj: 'accounts[0]' },
  },
  {
    type: 'uint',
    subtype: null,
    stringfieldValue: { isCode: false, js: '10', sol: '10', obj: 10 },
  },
  paramUint20,
  {
    type: 'uint',
    subtype: null,
    stringfieldValue: { isCode: false, js: '30', sol: '30', obj: 30 },
  },
  {
    type: TransactionParameterType,
    subtype: null,
    stringfieldValue: { isCode: false, js: "{ from: 'accounts[9]' }", sol: '?', obj: { from: 'accounts[9]' } },
  },
];
