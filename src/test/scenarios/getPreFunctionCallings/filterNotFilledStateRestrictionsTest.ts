import path from 'path';
import chai from 'chai';
import 'mocha';
import { IScenario } from '../../../models/scenario/IScenario';
import { TruffleProjectIceFactoryFunction } from '../../../services/TruffleFactory';
import { IContract } from '../../../models/IContract';
import { getFunctionParametersValuesScenarios } from '../../../services/scenarios';
import { GetRandomValueFunction } from '../../../services/Utils/GetRandomValueFunction';
import { ElementaryTypeName, FunctionDefinition, ContractDefinition } from 'solidity-parser-antlr';

const assert = chai.assert;

import {
  truffleProjectEmpty,
  functionEmpty,
  contractEmpty,
  getTruffleProject,
  getContractByName,
  getFunctionByName,
  getParametersValuesForTest,
} from '../../utils';
import { extractTruffleProjectGraph } from '../../../services/TruffleProjectGraph';
import { IFunctionCalling } from '../../../models';
import { getPreFunctionCallings } from '../../../services/scenarios/preCallings';
import { TypeReferences } from '../../../models/scenario';
import {
  r_uneq_number7,
  r_msgSender_neq_to,
  r_msgValue_leq_paramMax,
  r_stateVariableX_gre_paramMax,
  r_stateVariableX_eq_stateVariableY,
  r_stateVariableX_gre_number7,
  r_number7_les_stateVariableX,
} from '../../restrictionsConstants';
import { filterNotFilledStateRestrictions } from '../../../services/scenarios/preCallings/filterNotFilledStateRestrictions';

describe('Scenarios.PreFunctionCallings: filterNotFilledStateRestrictions', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contracts: IContract[];
  let contract: ContractDefinition;
  let references: TypeReferences = {};

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
    contracts = truffleProject.getContracts();
    contract = getContractByName(contracts, 'RequireUse');
    //setando apenas transactionParameter como null
    references = {};
    references['transactionParameter'] = null;
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  it('Se a restrição não tiver nenhum dos lados como tipo stateVariable, deve retornar FALSE', async function () {
    const result: boolean = filterNotFilledStateRestrictions(
      truffleProject.getGraph().contracts[contract.name],
      r_uneq_number7,
      references
    );
    assert.equal(result, false);
  });

  it('Se a restrição tiver os lados como tipo stateVariable o RIGHT for null em {references} e o LEFT tiver valor, deve retornar TRUE', async function () {
    references[contract.name.concat('.', 'X')] = {
      type: 'uint',
      subtype: null,
      stringfieldValue: { isCode: false, js: '10', sol: '10', obj: 10 },
    };
    const result: boolean = filterNotFilledStateRestrictions(
      truffleProject.getGraph().contracts[contract.name],
      r_stateVariableX_eq_stateVariableY,
      references
    );
    assert.equal(result, true);
  });

  it('Se a restrição tiver os lados como tipo stateVariable o RIGHT tiver valor em {references} e o LEFT não, deve retornar TRUE', async function () {
    references[contract.name.concat('.', 'X')] = {
      type: 'uint',
      subtype: null,
      stringfieldValue: { isCode: false, js: '10', sol: '10', obj: 10 },
    };
    const result: boolean = filterNotFilledStateRestrictions(
      truffleProject.getGraph().contracts[contract.name],
      r_stateVariableX_eq_stateVariableY,
      references
    );
    assert.equal(result, true);
  });

  it('Se a restrição tiver o LEFT como tipo stateVariable e com valor setado em {references} E este valor em {references} NÃO atende à restrição, deve retornar TRUE', async function () {
    references[contract.name.concat('.', 'X')] = {
      type: 'uint',
      subtype: null,
      stringfieldValue: { isCode: false, js: '7', sol: '7', obj: 7 },
    };
    const result: boolean = filterNotFilledStateRestrictions(
      truffleProject.getGraph().contracts[contract.name],
      r_stateVariableX_gre_number7,
      references
    );
    assert.equal(result, true);
  });

  it('Se a restrição tiver o LEFT como tipo stateVariable e com valor setado em {references} E este valor em {references} atende à restrição, deve retornar FALSE', async function () {
    references[contract.name.concat('.', 'X')] = {
      type: 'uint',
      subtype: null,
      stringfieldValue: { isCode: false, js: '8', sol: '8', obj: 8 },
    };
    const result: boolean = filterNotFilledStateRestrictions(
      truffleProject.getGraph().contracts[contract.name],
      r_stateVariableX_gre_number7,
      references
    );
    assert.equal(result, false);
  });

  it('Se a restrição tiver o RIGHT como tipo stateVariable e com valor setado em {references} E este valor em {references} NÃO atende à restrição, deve retornar TRUE', async function () {
    references[contract.name.concat('.', 'X')] = {
      type: 'uint',
      subtype: null,
      stringfieldValue: { isCode: false, js: '7', sol: '7', obj: 7 },
    };
    const result: boolean = filterNotFilledStateRestrictions(
      truffleProject.getGraph().contracts[contract.name],
      r_number7_les_stateVariableX,
      references
    );
    assert.equal(result, true);
  });

  it('Se a restrição tiver o RIGHT como tipo stateVariable e com valor setado em {references} E este valor em {references} atende à restrição, deve retornar FALSE', async function () {
    references[contract.name.concat('.', 'X')] = {
      type: 'uint',
      subtype: null,
      stringfieldValue: { isCode: false, js: '8', sol: '8', obj: 8 },
    };
    const result: boolean = filterNotFilledStateRestrictions(
      truffleProject.getGraph().contracts[contract.name],
      r_number7_les_stateVariableX,
      references
    );
    assert.equal(result, false);
  });
});
