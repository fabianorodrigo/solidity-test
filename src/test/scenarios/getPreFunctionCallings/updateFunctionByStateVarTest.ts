import path from 'path';
import chai from 'chai';
import 'mocha';
import { TruffleProjectIceFactoryFunction } from '../../../services/TruffleFactory';
import { IContract } from '../../../models/IContract';
import { ContractDefinition } from 'solidity-parser-antlr';

const assert = chai.assert;

import { getTruffleProject, getContractByName, getFunctionByName } from '../../utils';
import { extractTruffleProjectGraph } from '../../../services/TruffleProjectGraph';
import { TypeReferences } from '../../../models/scenario';
import {
  r_msgValue_leq_paramMax,
  r_stateVariableX_gre_number7,
  r_stateVariable_source_eq_stringUnirio,
} from '../../restrictionsConstants';
import { updateFunctionByStateVar } from '../../../services/scenarios/preCallings/updateFunctionByStateVar';
import { TStateVariableByFunction } from '../../../services/scenarios/preCallings/TStateVariableByFunction';

describe('Scenarios.PreFunctionCallings: filterNotFilledStateRestrictions', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contracts: IContract[];
  let contract: ContractDefinition;
  let references: TypeReferences = {};
  let stateByFunction: TStateVariableByFunction = {};
  let uniqueStateVariables: Set<string> = new Set<string>();

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
    contracts = truffleProject.getContracts();
    contract = getContractByName(contracts, 'RequireUse');
    //setando apenas transactionParameter como null
    references = {};
    references['transactionParameter'] = null;
    //setando variáveis que guardam relação de funções e variáveis de estado
    //e lista de variáveis de estado para vazio
    stateByFunction = {};
    uniqueStateVariables = new Set<string>();
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  it('Se o {side:RestrictionValue} da restrição não for do tipo stateVariable, stateVariableMember ou stateVariableIndex, deve retornar FALSE', async function () {
    const result: boolean = updateFunctionByStateVar(
      truffleProject,
      truffleProject.getGraph().contracts[contract.name],
      getFunctionByName(contract, 'simpleRequires'),
      r_msgValue_leq_paramMax.left,
      stateByFunction,
      uniqueStateVariables,
      r_msgValue_leq_paramMax,
      {}
    );
    assert.equal(result, false);
    assert.equal(uniqueStateVariables.size, 0);
    assert.equal(Object.keys(stateByFunction).length, 0);
  });

  it('Se o {side:RestrictionValue} da restrição for stateVariable mas não existe função que escreva nela, deve retornar FALSE', async function () {
    const result: boolean = updateFunctionByStateVar(
      truffleProject,
      truffleProject.getGraph().contracts[contract.name],
      getFunctionByName(contract, 'simpleRequires'),
      r_stateVariableX_gre_number7.left,
      stateByFunction,
      uniqueStateVariables,
      r_stateVariableX_gre_number7,
      {}
    );
    assert.equal(result, false);
    assert.equal(uniqueStateVariables.size, 0);
    assert.equal(Object.keys(stateByFunction).length, 0);
  });

  it('Se o {side:RestrictionValue} da restrição for stateVariable mas não existe função que escreva nela no contrato mas existe em um contrato do qual herde, deve retornar TRUE', async function () {
    const result: boolean = updateFunctionByStateVar(
      truffleProject,
      truffleProject.getGraph().contracts[contract.name],
      getFunctionByName(contract, 'simpleRequires'),
      r_stateVariable_source_eq_stringUnirio.left,
      stateByFunction,
      uniqueStateVariables,
      r_stateVariable_source_eq_stringUnirio,
      {}
    );
    assert.equal(result, true);
    assert.equal(uniqueStateVariables.size, 1);
    assert.equal(Object.keys(stateByFunction).length, 1);
    assert.equal(Object.keys(stateByFunction)[0], 'setSource(string)');
    assert.isArray(stateByFunction['setSource(string)'].stateVariables);
    assert.equal(stateByFunction['setSource(string)'].stateVariables.length, 1);
  });
});
