import path from 'path';
import chai from 'chai';
import 'mocha';
import { TruffleProjectIceFactoryFunction } from '../../services/TruffleFactory';
import { Types } from '../../services/types';
import { ContractDefinition, FunctionDefinition, VariableDeclaration } from 'solidity-parser-antlr';
import { getTruffleProject, getContractByName, getFunctionByName } from '../utils';
import { extractTruffleProjectGraph } from '../../services/TruffleProjectGraph';
import { RestrictionElementType } from '../../models/RestrictionElementType';
import { IValue } from '../../models/IValue';
import {
  r_gre_number7,
  r_leq_number17,
  r_Z_les_P,
  r_A_geq_Z,
  r_Z_gre_P,
  r_msgSender_neq_0x0,
  r_msgValue_gre_0,
  r_msgValue_leq_paramMax,
  r_leq_number17_fordebug,
} from '../restrictionsConstants';
import { IRestriction } from '../../models/IResctriction';
import { getRandomParameterTypeValue } from '../../services/Utils/getRandomParameterValue';
import { ITypeValue } from '../../models/scenario/ITypeValue';

const assert = chai.assert;

describe('Utils - getRandomParameterTypeValue', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contract: ContractDefinition;
  const references = {} as { [name: string]: IValue };

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../'));
    contract = getContractByName(truffleProject.getContracts(), 'RestrictionTypesTest');
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  it('Se informar o nome do parâmetro a ter valor gerado como NULL, deve lançar exceção', async function () {
    chai
      .expect(
        getRandomParameterTypeValue.bind(null, truffleProject, contract, {} as FunctionDefinition, null, [], references)
      )
      .to.throw(`A parameter is required`);
  });

  it('Sem restrições e sem referências, deve retornar qualquer valor randômico', async function () {
    const refLocal = JSON.parse(JSON.stringify(references));
    testOtherParametersRestrictions(truffleProject, refLocal, []);
  });

  describe('Com restrições de outros parâmetros distintos do analisado', function () {
    describe('RestrictionElementType.functionParameter / RestrictionElementType.Literal', function () {
      it('Sem referências, deve retornar qualquer valor randômico', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        testOtherParametersRestrictions(truffleProject, refLocal, [r_gre_number7, r_leq_number17]);
      });
      it('Com referências nulas, deve retornar qualquer valor randômico', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        refLocal.value1 = { stringfieldValue: { js: null, sol: null } };
        refLocal.P = { stringfieldValue: { js: null, sol: null } };
        testOtherParametersRestrictions(truffleProject, refLocal, [r_gre_number7, r_leq_number17]);
      });
      it('Com referências informadas, deve retornar qualquer valor randômico', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        refLocal.value1 = { stringfieldValue: { js: '7', sol: '7' } };
        refLocal.P = { stringfieldValue: { js: '17', sol: '17' } };
        testOtherParametersRestrictions(truffleProject, refLocal, [r_gre_number7, r_leq_number17]);
      });
    });
    describe('RestrictionElementType.functionParameter / RestrictionElementType.functionParameter', function () {
      it('Sem referências, deve retornar qualquer valor randômico', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        testOtherParametersRestrictions(truffleProject, refLocal, [r_Z_les_P, r_A_geq_Z]);
      });
      it('Com referências nulas, deve retornar qualquer valor randômico', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        refLocal.value1 = { stringfieldValue: { js: null, sol: null } };
        refLocal.P = { stringfieldValue: { js: null, sol: null } };
        testOtherParametersRestrictions(truffleProject, refLocal, [r_Z_les_P, r_A_geq_Z]);
      });
      it('Com referências informadas, deve retornar qualquer valor randômico', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        refLocal.value1 = { stringfieldValue: { js: '7', sol: '7' } };
        refLocal.P = { stringfieldValue: { js: '17', sol: '17' } };
        refLocal.A = { stringfieldValue: { js: '170', sol: '170' } };
        refLocal.Z = { stringfieldValue: { js: '7', sol: '7' } };
        testOtherParametersRestrictions(truffleProject, refLocal, [r_Z_les_P, r_A_geq_Z]);
      });
    });
    describe('RestrictionElementType.functionParameter / RestrictionElementType.transactionParameter', function () {
      it('Sem referências, deve retornar qualquer valor randômico', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        testOtherParametersRestrictions(truffleProject, refLocal, [r_msgSender_neq_0x0, r_msgValue_gre_0]);
      });
      it('Com referências nulas, deve retornar qualquer valor randômico', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        refLocal.value1 = { stringfieldValue: { js: null, sol: null } };
        refLocal.P = { stringfieldValue: { js: null, sol: null } };
        testOtherParametersRestrictions(truffleProject, refLocal, [r_msgSender_neq_0x0, r_msgValue_gre_0]);
      });
      it('Com referências informadas, deve retornar qualquer valor randômico', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        refLocal.value1 = { stringfieldValue: { js: '7', sol: '7' } };
        refLocal.P = { stringfieldValue: { js: '17', sol: '17' } };
        refLocal.A = { stringfieldValue: { js: '170', sol: '170' } };
        refLocal.Z = { stringfieldValue: { js: '7', sol: '7' } };
        testOtherParametersRestrictions(truffleProject, refLocal, [r_msgSender_neq_0x0, r_msgValue_gre_0]);
      });
    });
  });
  describe('Com restrições ao parâmetro analisado', function () {
    describe('RestrictionElementType.functionParameter / RestrictionElementType.Literal', function () {
      it('Sem referências, deve retornar valor randômico que atenda às restrições', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        let value: ITypeValue = getRandomParameterTypeValue(
          truffleProject,
          contract,
          {} as FunctionDefinition,
          { name: 'P', typeName: { name: 'uint' } } as VariableDeclaration,
          [r_gre_number7, r_leq_number17],
          refLocal
        );
        assert.isNotNull(value);
        assert.isNotNull(value.stringfieldValue);
        assert.isAtLeast(parseInt(value.stringfieldValue.js), 7);
        assert.isAtMost(parseInt(value.stringfieldValue.js), 17);
        assert.equal(value.stringfieldValue.js, value.stringfieldValue.sol);
      });

      it('Com referências nulas, deve retornar valor randômico que atenda às restrições', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        refLocal.value1 = { js: null, sol: null };
        refLocal.P = { js: null, sol: null };
        let value: ITypeValue = getRandomParameterTypeValue(
          truffleProject,
          contract,
          {} as FunctionDefinition,
          { name: 'P', typeName: { name: 'uint' } } as VariableDeclaration,
          [r_gre_number7, r_leq_number17],
          refLocal
        );
        assert.isNotNull(value);
        assert.isAtLeast(parseInt(value.stringfieldValue.js), 7);
        assert.isAtMost(parseInt(value.stringfieldValue.js), 17);
        assert.equal(value.stringfieldValue.js, value.stringfieldValue.sol);
      });
      it('Com referências informadas, deve retornar valor randômico que atenda às restrições', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        refLocal.value1 = { stringfieldValue: { js: '7', sol: '7', obj: 7 } };
        refLocal.P = { stringfieldValue: { js: '17', sol: '17', obj: 17 } };
        let value: ITypeValue = getRandomParameterTypeValue(
          truffleProject,
          contract,
          {} as FunctionDefinition,
          { name: 'P', typeName: { name: 'uint' } } as VariableDeclaration,
          [r_gre_number7, r_leq_number17],
          refLocal
        );
        assert.isNotNull(value);
        assert.isAtLeast(parseInt(value.stringfieldValue.js), 7);
        assert.isAtMost(parseInt(value.stringfieldValue.js), 17);
        assert.equal(value.stringfieldValue.js, value.stringfieldValue.sol);
      });
    });
    describe('RestrictionElementType.functionParameter / RestrictionElementType.functionParameter', function () {
      it('Sem referências, deve retornar qualquer valor randômico', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        let value: ITypeValue = getRandomParameterTypeValue(
          truffleProject,
          contract,
          {} as FunctionDefinition,
          { name: 'Z', typeName: { name: 'uint' } } as VariableDeclaration,
          [r_Z_les_P, r_A_geq_Z],
          refLocal
        );
        assert.isNotNull(value);
        assert.isNumber(parseInt(value.stringfieldValue.js));
        assert.isNumber(parseInt(value.stringfieldValue.sol));
        assert.equal(value.stringfieldValue.js, value.stringfieldValue.sol);
      });

      it('Com referências nulas, deve retornar qualquer valor randômico', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        refLocal.Z = { stringfieldValue: { js: null, sol: null } };
        refLocal.P = { stringfieldValue: { js: null, sol: null } };
        let value: ITypeValue = getRandomParameterTypeValue(
          truffleProject,
          contract,
          {} as FunctionDefinition,
          { name: 'Z', typeName: { name: 'uint' } } as VariableDeclaration,
          [r_Z_les_P, r_A_geq_Z],
          refLocal
        );
        assert.isNotNull(value);
        assert.isNumber(parseInt(value.stringfieldValue.js));
        assert.isNumber(parseInt(value.stringfieldValue.sol));
        assert.equal(value.stringfieldValue.js, value.stringfieldValue.sol);
      });
      it('Com referências informadas, deve retornar valor randômico que atenda às restrições', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        refLocal.Z = { stringfieldValue: { js: null, sol: null } };
        refLocal.P = { stringfieldValue: { js: '7', sol: '7', obj: 7 } };
        refLocal.A = { stringfieldValue: { js: '17', sol: '17', obj: 17 } };
        let value: ITypeValue = getRandomParameterTypeValue(
          truffleProject,
          contract,
          {} as FunctionDefinition,
          { name: 'Z', typeName: { name: 'uint' } } as VariableDeclaration,
          [r_Z_gre_P, r_A_geq_Z],
          refLocal
        );
        assert.isNotNull(value);
        assert.isAtLeast(parseInt(value.stringfieldValue.js), 8);
        assert.isAtMost(parseInt(value.stringfieldValue.js), 17);
        assert.equal(value.stringfieldValue.js, value.stringfieldValue.sol);
      });
    });
    describe('RestrictionElementType.functionParameter / RestrictionElementType.TransactionParameter', function () {
      it('Sem referências, deve retornar valor randômico que atenda às restrições', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        let value: ITypeValue = getRandomParameterTypeValue(
          truffleProject,
          contract,
          {} as FunctionDefinition,
          { name: 'max', typeName: { name: 'uint' } } as VariableDeclaration,
          [r_msgValue_leq_paramMax],
          refLocal
        );
        assert.isNotNull(value);
        assert.isNumber(parseInt(value.stringfieldValue.js));
        assert.isNumber(parseInt(value.stringfieldValue.sol));
        assert.equal(value.stringfieldValue.js.replace(/"/g, ''), value.stringfieldValue.sol);
      });

      it('Com referências nulas, deve retornar valor randômico que atenda às restrições', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        refLocal.value1 = { js: null, sol: null };
        refLocal.P = { js: null, sol: null };
        let value: ITypeValue = getRandomParameterTypeValue(
          truffleProject,
          contract,
          {} as FunctionDefinition,
          { name: 'max', typeName: { name: 'uint' } } as VariableDeclaration,
          [r_msgValue_leq_paramMax],
          refLocal
        );
        assert.isNotNull(value);
        assert.isNumber(parseInt(value.stringfieldValue.js));
        assert.isNumber(parseInt(value.stringfieldValue.sol));
        assert.equal(value.stringfieldValue.js.replace(/"/g, ''), value.stringfieldValue.sol);
      });
    });
  });
});

function testOtherParametersRestrictions(truffleProject, refLocal, restrictions: IRestriction[]) {
  let value: ITypeValue = getRandomParameterTypeValue(
    truffleProject,
    {} as ContractDefinition,
    {} as FunctionDefinition,
    { name: 'value2', typeName: { name: 'uint' } } as VariableDeclaration,
    restrictions,
    refLocal
  );
  assert.isNotNull(value);
  assert.isNumber(parseInt(value.stringfieldValue.js));
  assert.isNumber(parseInt(value.stringfieldValue.sol));
  assert.equal(value.stringfieldValue.js, value.stringfieldValue.sol);
}
