import path from 'path';
import chai from 'chai';
import 'mocha';
import { TruffleProjectIceFactoryFunction } from '../../services/TruffleFactory';
import { Types } from '../../services/types';
import { ContractDefinition, FunctionDefinition, VariableDeclaration } from 'solidity-parser-antlr';
import { getTruffleProject, getContractByName, getFunctionByName } from '../utils';
import { extractTruffleProjectGraph } from '../../services/TruffleProjectGraph';
import { RestrictionElementType } from '../../models/RestrictionElementType';
import { IValue } from '../../models/IValue';
import {
  r_gre_number7,
  r_leq_number17,
  r_Z_les_P,
  r_A_geq_Z,
  r_Z_gre_P,
  r_msgSender_neq_0x0,
  r_msgValue_gre_0,
  r_msgValue_leq_paramMax,
  r_leq_number17_fordebug,
  r_msgSender_neq_to,
} from '../restrictionsConstants';
import { IRestriction } from '../../models/IResctriction';
import { getRandomTransactionParameterTypeValue } from '../../services/Utils/getRandomParameterValue';
import { ITypeValue } from '../../models/scenario/ITypeValue';

const assert = chai.assert;

describe('Utils - getRandomTransactionParameterTypeValue', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contract: ContractDefinition;
  const references = {} as { [name: string]: IValue };

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../'));
    contract = getContractByName(truffleProject.getContracts(), 'RestrictionTypesTest');
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  it('Sem restrições e sem referências, deve retornar qualquer valor randômico', async function () {
    const refLocal = JSON.parse(JSON.stringify(references));
    testOtherParametersRestrictions(truffleProject, refLocal, []);
  });

  describe('Com restrições de outros parâmetros distintos do analisado', function () {
    describe('RestrictionElementType.transactionParameter / RestrictionElementType.Literal', function () {
      it('Sem referências, deve retornar qualquer valor randômico', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        testOtherParametersRestrictions(truffleProject, refLocal, [r_gre_number7, r_leq_number17]);
      });
      it('Com referências nulas, deve retornar qualquer valor randômico', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        refLocal.value1 = { js: null, sol: null };
        refLocal.P = { js: null, sol: null };
        testOtherParametersRestrictions(truffleProject, refLocal, [r_gre_number7, r_leq_number17]);
      });
      it('Com referências informadas, deve retornar qualquer valor randômico', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        refLocal.value1 = { js: '7', sol: '7' };
        refLocal.P = { js: '17', sol: '17' };
        testOtherParametersRestrictions(truffleProject, refLocal, [r_gre_number7, r_leq_number17]);
      });
    });
    describe('RestrictionElementType.functionParameter / RestrictionElementType.functionParameter', function () {
      it('Sem referências, deve retornar qualquer valor randômico', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        testOtherParametersRestrictions(truffleProject, refLocal, [r_Z_les_P, r_A_geq_Z]);
      });
      it('Com referências nulas, deve retornar qualquer valor randômico', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        refLocal.value1 = { js: null, sol: null };
        refLocal.P = { js: null, sol: null };
        testOtherParametersRestrictions(truffleProject, refLocal, [r_Z_les_P, r_A_geq_Z]);
      });
      it('Com referências informadas, deve retornar qualquer valor randômico', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        refLocal.value1 = { js: '7', sol: '7' };
        refLocal.P = { js: '17', sol: '17' };
        refLocal.A = { js: '170', sol: '170' };
        refLocal.Z = { js: '7', sol: '7' };
        testOtherParametersRestrictions(truffleProject, refLocal, [r_Z_les_P, r_A_geq_Z]);
      });
    });
  });
  describe('Com restrições ao parâmetro analisado', function () {
    describe('RestrictionElementType.transactionParameter / RestrictionElementType.Literal', function () {
      it('Sem referências, deve retornar valor randômico que atenda às restrições', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        let value: ITypeValue = getRandomTransactionParameterTypeValue(
          truffleProject,
          contract,
          [r_msgSender_neq_0x0, r_msgValue_gre_0],
          refLocal
        );
        assert.isNotNull(value);
        assert.isNotNull(value.stringfieldValue);
        assert.isAtLeast(value.stringfieldValue.obj.value, 1);
        assert.notEqual(value.stringfieldValue.obj.from, '"0x0000000000000000000000000000000000000000"');
      });

      it('Com referências nulas, deve retornar valor randômico que atenda às restrições', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        refLocal.value1 = { stringfieldValue: { js: null, sol: null } };
        refLocal.P = { stringfieldValue: { js: null, sol: null } };
        let value: ITypeValue = getRandomTransactionParameterTypeValue(
          truffleProject,
          contract,
          [r_msgSender_neq_0x0, r_msgValue_gre_0],
          refLocal
        );
        assert.isNotNull(value);
        assert.isNotNull(value.stringfieldValue);
        assert.isAtLeast(value.stringfieldValue.obj.value, 1);
        assert.notEqual(
          value.stringfieldValue.obj.from,
          '"0x0000000000000000000000000000000000000000"',
          'js.from must be different from account zero'
        );
      });
      it('Com referências informadas, deve retornar valor randômico que atenda às restrições', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        refLocal.value1 = { stringfieldValue: { js: '7', sol: '7', obj: 7 } };
        refLocal.P = { stringfieldValue: { js: '17', sol: '17', obj: 17 } };
        let value: ITypeValue = getRandomTransactionParameterTypeValue(
          truffleProject,
          contract,
          [r_msgSender_neq_0x0, r_msgValue_gre_0],
          refLocal
        );
        assert.isNotNull(value);
        assert.isNotNull(value.stringfieldValue);
        assert.isAtLeast(value.stringfieldValue.obj.value, 1);
        assert.notEqual(value.stringfieldValue.obj.from, '"0x0000000000000000000000000000000000000000"');
      });
    });
    describe('RestrictionElementType.transactionParameter / RestrictionElementType.functionParameter', function () {
      it('Sem referências, deve retornar qualquer valor randômico', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        let value: ITypeValue = getRandomTransactionParameterTypeValue(
          truffleProject,
          contract,
          [r_msgValue_leq_paramMax, r_msgSender_neq_to],
          refLocal
        );
        assert.isNotNull(value);
        assert.isNotNull(value.stringfieldValue);
        //Sem restrição para transactionParameter.value, não retorna nenhum value
        assert.isUndefined(value.stringfieldValue.obj.value);
        assert.isNotNull(value.stringfieldValue.obj.from);
      });

      it('Com referências nulas, deve retornar qualquer valor randômico', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        refLocal.Z = { stringfieldValue: { js: null, sol: null } };
        refLocal.P = { stringfieldValue: { js: null, sol: null } };
        let value: ITypeValue = getRandomTransactionParameterTypeValue(
          truffleProject,
          contract,
          [r_msgValue_leq_paramMax, r_msgSender_neq_to],
          refLocal
        );
        assert.isNotNull(value);
        assert.isNotNull(value.stringfieldValue);
        //Sem restrição para transactionParameter.value, não retorna nenhum value
        assert.isUndefined(value.stringfieldValue.obj.value);
        assert.isNotNull(value.stringfieldValue.obj.from);
      });
      it('Com referências informadas, deve retornar valor randômico que atenda às restrições', async function () {
        const refLocal = JSON.parse(JSON.stringify(references));
        refLocal.to = { stringfieldValue: { js: '0x0', sol: '0x0' } };
        refLocal.max = { stringfieldValue: { js: '17', sol: '18', obj: 18 } };
        let value: ITypeValue = getRandomTransactionParameterTypeValue(
          truffleProject,
          contract,
          [r_msgValue_leq_paramMax, r_msgSender_neq_to],
          refLocal
        );
        assert.isNotNull(value);
        assert.isNotNull(value.stringfieldValue);
        assert.isAtMost(value.stringfieldValue.obj.value, 16);
        assert.isNotNull(value.stringfieldValue.obj.from);
        assert.notEqual(value.stringfieldValue.obj.from, '"0x0000000000000000000000000000000000000000"');
        assert.notEqual(value.stringfieldValue.obj.from, '"0x0"');
      });
    });
  });
});

function testOtherParametersRestrictions(truffleProject, refLocal, restrictions: IRestriction[]) {
  let value: ITypeValue = getRandomTransactionParameterTypeValue(
    truffleProject,
    {} as ContractDefinition,
    restrictions,
    refLocal
  );
  assert.isNotNull(value);
  assert.equal(value.type, 'TransactionParameter');
  assert.isNull(value.subtype);
  if (
    restrictions.some(
      (r) =>
        (r.left.elementType == RestrictionElementType.transactionParameter && r.left.value.js == 'value') ||
        (r.right.elementType == RestrictionElementType.transactionParameter && r.right.value.js == 'value')
    )
  ) {
    assert.isNumber(value.stringfieldValue.obj.value);
  } else {
    //Sem restrição para transactionParameter.value, não retorna nenhum value
    assert.isUndefined(value.stringfieldValue.obj.value);
  }
}
