import path from 'path';
import chai from 'chai';
import 'mocha';
import { TruffleProjectIceFactoryFunction } from '../../services/TruffleFactory';
import { Types } from '../../services/types';
import { ContractDefinition, FunctionDefinition, VariableDeclaration } from 'solidity-parser-antlr';
import { getTruffleProject, getContractByName, getFunctionByName } from '../utils';
import { extractTruffleProjectGraph } from '../../services/TruffleProjectGraph';
import { RestrictionElementType } from '../../models/RestrictionElementType';
import { IValue } from '../../models/IValue';
import {
  r_gre_number7,
  r_leq_number17,
  r_Z_les_P,
  r_A_geq_Z,
  r_Z_gre_P,
  r_msgSender_neq_0x0,
  r_msgValue_gre_0,
  r_msgValue_leq_paramMax,
  r_leq_number17_fordebug,
} from '../restrictionsConstants';
import { IRestriction } from '../../models/IResctriction';
import { getRandomParameterTypeValue } from '../../services/Utils/getRandomParameterValue';
import { ITypeValue } from '../../models/scenario/ITypeValue';

const assert = chai.assert;

describe('RestrictionElementType.functionParameter / RestrictionElementType.TransactionParameter', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contract: ContractDefinition;
  const references = {} as { [name: string]: IValue };

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../'));
    contract = getContractByName(truffleProject.getContracts(), 'RestrictionTypesTest');
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });
  it('Sem referências, deve retornar valor randômico que atenda às restrições', async function () {
    const refLocal = JSON.parse(JSON.stringify(references));
    let value: ITypeValue = getRandomParameterTypeValue(
      truffleProject,
      contract,
      {} as FunctionDefinition,
      { name: 'max', typeName: { name: 'uint' } } as VariableDeclaration,
      [r_msgValue_leq_paramMax],
      refLocal
    );
    assert.isNotNull(value);
    assert.isNumber(parseInt(value.stringfieldValue.js));
    assert.isNumber(parseInt(value.stringfieldValue.sol));
    assert.equal(value.stringfieldValue.js.replace(/"/g, ''), value.stringfieldValue.sol);
  });
});
