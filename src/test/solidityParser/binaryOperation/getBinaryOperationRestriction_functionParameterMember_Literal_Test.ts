import path from 'path';
import chai from 'chai';
import 'mocha';
import { getBinaryOperationRestriction } from '../../../services/solidityParser';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { TruffleProjectIceFactoryFunction } from '../../../services/TruffleFactory';
import { IRestriction } from '../../../models/IResctriction';
import { BranchType } from '../../../models/BranchType';
import { RestrictionElementType } from '../../../models/RestrictionElementType';

const assert = chai.assert;

import {
  getTruffleProject,
  getContractByName,
  getBOFromRequireCall,
  getFunctionByName,
  getRequireCalls,
} from '../../utils';
import { extractTruffleProjectGraph } from '../../../services/TruffleProjectGraph';

describe('solidityParser.BinaryOperation - RestrictionType.functionParameterMember_Literal: getBinaryOperationRestriction', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  it('param1.member > 0', async function () {
    const contract: ContractDefinition = getContractByName(truffleProject.getContracts(), 'RestrictionTypesTest');
    const funcTest: FunctionDefinition = getFunctionByName(contract, 'simpleRequires_functionParameterMember_Literal');
    const requireCalls = getRequireCalls(funcTest);
    const result: IRestriction[] = getBinaryOperationRestriction(
      truffleProject,
      contract,
      getBOFromRequireCall(requireCalls[0]),
      funcTest
    );
    assert.isArray(result);
    assert.equal(result.length, 1);
    assert.equal(result[0].left.value.js, 'structWithEnum');
    assert.equal(result[0].left.value.memberName, 'a');
    assert.equal(result[0].operator, '>');
    assert.equal(result[0].right.value.js, 0);
    assert.equal(result[0].from, BranchType.notDefined);
    assert.equal(result[0].right.elementType, RestrictionElementType.Literal);
    assert.equal(result[0].left.elementType, RestrictionElementType.functionParameterMember);
  });
  it('param.member > 0 && param.member2 == Enum.member', async function () {
    const contract: ContractDefinition = getContractByName(truffleProject.getContracts(), 'RestrictionTypesTest');
    const funcTest: FunctionDefinition = getFunctionByName(contract, 'simpleRequires_functionParameterMember_Literal');
    const requireCalls = getRequireCalls(funcTest);
    const result: IRestriction[] = getBinaryOperationRestriction(
      truffleProject,
      contract,
      getBOFromRequireCall(requireCalls[1]),
      funcTest
    );
    assert.isArray(result);
    assert.equal(result.length, 2);
    assert.equal(result[0].left.value.js, 'structWithEnum');
    assert.equal(result[0].left.value.memberName, 'a');
    assert.equal(result[0].operator, '>');
    assert.equal(result[0].right.value.js, 0);
    assert.equal(result[0].from, BranchType.notDefined);
    assert.equal(result[0].left.elementType, RestrictionElementType.functionParameterMember);
    assert.equal(result[0].right.elementType, RestrictionElementType.Literal);
    assert.equal(result[1].left.value.js, 'structWithEnum');
    assert.equal(result[1].left.value.memberName, 'b');
    assert.equal(result[1].operator, '==');
    assert.equal(result[1].right.value.js, 'RestrictionTypesTest.COVID');
    assert.equal(result[1].right.value.memberName, 'c');
    assert.equal(result[1].from, BranchType.notDefined);
    assert.equal(result[1].left.elementType, RestrictionElementType.functionParameterMember);
    assert.equal(result[1].right.elementType, RestrictionElementType.EnumMember);
  });
});
