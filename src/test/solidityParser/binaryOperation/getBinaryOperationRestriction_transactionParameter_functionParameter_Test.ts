import path from 'path';
import chai from 'chai';
import 'mocha';
import { getBinaryOperationRestriction } from '../../../services/solidityParser';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { TruffleProjectIceFactoryFunction } from '../../../services/TruffleFactory';
import { IRestriction } from '../../../models/IResctriction';
import { BranchType } from '../../../models/BranchType';
import { extractTruffleProjectGraph } from '../../../services/TruffleProjectGraph';
import { RestrictionElementType } from '../../../models/RestrictionElementType';

const assert = chai.assert;

import {
  getTruffleProject,
  getContractByName,
  getBOFromRequireCall,
  getFunctionByName,
  getRequireCalls,
} from '../../utils';

describe('solidityParser.BinaryOperation - RestrictionType.transactionParameter_functionParameter: getBinaryOperationRestriction', function() {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contract: ContractDefinition;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
    contract = getContractByName(truffleProject.getContracts(), 'RestrictionTypesTest');
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });
  describe('Obtenção de restrições  de um determinado atributo de parâmetro de entrada de função em um BinaryOperation', function() {
    it('msg.value > param', async function() {
      const funcTest: FunctionDefinition = getFunctionByName(
        contract,
        'simpleRequires_transactionParameter_functionParameter'
      );
      const requireCalls = getRequireCalls(funcTest);
      const bo = getBOFromRequireCall(requireCalls[0]);
      const paramName = 'teste';
      const result: IRestriction[] = getBinaryOperationRestriction(truffleProject, contract, bo, funcTest);
      assert.isArray(result);
      assert.equal(result.length, 1);
      assert.equal(result[0].left.value.js, 'value');
      assert.equal(result[0].operator, '>');
      assert.equal(result[0].right.value.js, 'param');
      assert.equal(result[0].from, BranchType.notDefined);
      assert.equal(result[0].left.elementType, RestrictionElementType.transactionParameter);
      assert.equal(result[0].right.elementType, RestrictionElementType.functionParameter);
    });
  });

  describe('Obtenção de restrições de um determinado parâmetro de função em um BinaryOperations aninhados', function() {
    it('msg.value > param && msg.sender != paramAddress', async function() {
      const funcTest: FunctionDefinition = getFunctionByName(
        contract,
        'simpleRequires_transactionParameter_functionParameter'
      );
      const requireCalls = getRequireCalls(funcTest);
      const bo = getBOFromRequireCall(requireCalls[1]);
      const paramName = 'teste';
      const result: IRestriction[] = getBinaryOperationRestriction(truffleProject, contract, bo, funcTest);
      assert.isArray(result);
      assert.equal(result.length, 2);
      assert.equal(result[0].left.value.js, 'value');
      assert.equal(result[0].operator, '>');
      assert.equal(result[0].right.value.js, 'param');
      assert.equal(result[0].from, BranchType.notDefined);
      assert.equal(result[0].left.elementType, RestrictionElementType.transactionParameter);
      assert.equal(result[0].right.elementType, RestrictionElementType.functionParameter);
      assert.equal(result[1].left.value.js, 'sender');
      assert.equal(result[1].operator, '==');
      assert.equal(result[1].right.value.js, 'to');
      assert.equal(result[1].from, BranchType.notDefined);
      assert.equal(result[1].left.elementType, RestrictionElementType.transactionParameter);
      assert.equal(result[1].right.elementType, RestrictionElementType.functionParameter);
    });
  });
});
