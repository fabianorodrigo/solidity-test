import path from 'path';
import chai from 'chai';
import 'mocha';
import { getBinaryOperationRestriction } from '../../../services/solidityParser';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { TruffleProjectIceFactoryFunction } from '../../../services/TruffleFactory';
import { IRestriction } from '../../../models/IResctriction';
import { BranchType } from '../../../models/BranchType';
import { RestrictionElementType } from '../../../models/RestrictionElementType';
import { extractTruffleProjectGraph } from '../../../services/TruffleProjectGraph';

const assert = chai.assert;

import {
  getTruffleProject,
  getContractByName,
  getBOFromRequireCall,
  getFunctionByName,
  getRequireCalls,
} from '../../utils';

describe('solidityParser.BinaryOperation - RestrictionType.functionParameterMember_functionParameter: getBinaryOperationRestriction', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contract: ContractDefinition;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
    contract = getContractByName(truffleProject.getContracts(), 'RestrictionTypesTest');
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  describe('Obtenção de restrições  de um determinado atributo de parâmetro de entrada de função em um BinaryOperation', function () {
    it('param1.member > param2', async function () {
      const contract: ContractDefinition = getContractByName(truffleProject.getContracts(), 'RestrictionTypesTest');
      const funcTest: FunctionDefinition = getFunctionByName(
        contract,
        'simpleRequires_functionParameterMember_functionParameter'
      );
      const requireCalls = getRequireCalls(funcTest);
      const bo = getBOFromRequireCall(requireCalls[0]);
      const result: IRestriction[] = getBinaryOperationRestriction(truffleProject, contract, bo, funcTest);
      assert.isArray(result);
      assert.equal(result.length, 1);
      assert.equal(result[0].left.value.js, 'structWithEnum');
      assert.equal(result[0].left.value.memberName, 'a');
      assert.equal(result[0].operator, '>');
      assert.equal(result[0].right.value.js, 'param');
      assert.equal(result[0].from, BranchType.notDefined);
      assert.equal(result[0].left.elementType, RestrictionElementType.functionParameterMember);
    });
  });

  describe('Obtenção de restrições de um determinado parâmetro de função em um BinaryOperations aninhados', function () {
    it('param.member > 0 && param.member2 == Enum.member', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(
        contract,
        'simpleRequires_functionParameterMember_functionParameter'
      );
      const requireCalls = getRequireCalls(funcTest);
      const bo = getBOFromRequireCall(requireCalls[1]);
      const result: IRestriction[] = getBinaryOperationRestriction(truffleProject, contract, bo, funcTest);
      assert.isArray(result);
      assert.equal(result.length, 2);
      assert.equal(result[0].left.value.js, 'structWithEnum');
      assert.equal(result[0].left.value.memberName, 'a');
      assert.equal(result[0].operator, '>');
      assert.equal(result[0].right.value.js, 'param2');
      assert.equal(result[0].from, BranchType.notDefined);
      assert.equal(result[0].right.elementType, RestrictionElementType.functionParameter);
      assert.equal(result[0].left.elementType, RestrictionElementType.functionParameterMember);
      assert.equal(result[1].left.value.js, 'structWithEnum');
      assert.equal(result[1].left.value.memberName, 'b');
      assert.equal(result[1].operator, '==');
      assert.equal(result[1].right.value.js, 'e');
      assert.equal(result[1].from, BranchType.notDefined);
      assert.equal(result[1].right.elementType, RestrictionElementType.functionParameter);
      assert.equal(result[1].left.elementType, RestrictionElementType.functionParameterMember);
    });
  });
});
