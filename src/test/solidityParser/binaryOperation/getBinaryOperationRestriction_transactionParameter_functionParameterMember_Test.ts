import path from 'path';
import chai from 'chai';
import 'mocha';
import { getBinaryOperationRestriction } from '../../../services/solidityParser';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { TruffleProjectIceFactoryFunction } from '../../../services/TruffleFactory';
import { IRestriction } from '../../../models/IResctriction';
import { BranchType } from '../../../models/BranchType';
import { extractTruffleProjectGraph } from '../../../services/TruffleProjectGraph';
import { RestrictionElementType } from '../../../models/RestrictionElementType';

const assert = chai.assert;

import {
  getTruffleProject,
  getContractByName,
  getBOFromRequireCall,
  getFunctionByName,
  getRequireCalls,
} from '../../utils';

describe('solidityParser.BinaryOperation - RestrictionType.transactionParameter_functionParameterMember: getBinaryOperationRestriction', function() {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contract: ContractDefinition;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
    contract = getContractByName(truffleProject.getContracts(), 'RestrictionTypesTest');
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  it('msg.value > param.member', async function() {
    const funcTest: FunctionDefinition = getFunctionByName(
      contract,
      'simpleRequires_transactionParameter_functionParameterMember'
    );
    const requireCalls = getRequireCalls(funcTest);
    const bo = getBOFromRequireCall(requireCalls[0]);
    const result: IRestriction[] = getBinaryOperationRestriction(truffleProject, contract, bo, funcTest);
    assert.isArray(result);
    assert.equal(result.length, 1);
    assert.equal(result[0].left.value.js, 'value');
    assert.equal(result[0].operator, '>');
    assert.equal(result[0].right.value.js, 'param1');
    assert.equal(result[0].right.value.memberName, 'a');
    assert.equal(result[0].from, BranchType.notDefined);
    assert.equal(result[0].left.elementType, RestrictionElementType.transactionParameter);
    assert.equal(result[0].right.elementType, RestrictionElementType.functionParameterMember);
  });

  it('msg.value > param.member && msg.sender != param2.member', async function() {
    const funcTest: FunctionDefinition = getFunctionByName(
      contract,
      'simpleRequires_transactionParameter_functionParameterMember'
    );
    const requireCalls = getRequireCalls(funcTest);
    const bo = getBOFromRequireCall(requireCalls[1]);
    const result: IRestriction[] = getBinaryOperationRestriction(truffleProject, contract, bo, funcTest);
    assert.isArray(result);
    assert.equal(result.length, 2);
    assert.equal(result[0].left.value.js, 'value');
    assert.equal(result[0].operator, '>');
    assert.equal(result[0].right.value.js, 'param1');
    assert.equal(result[0].right.value.memberName, 'a');
    assert.equal(result[0].from, BranchType.notDefined);
    assert.equal(result[0].left.elementType, RestrictionElementType.transactionParameter);
    assert.equal(result[0].right.elementType, RestrictionElementType.functionParameterMember);
    assert.equal(result[1].left.value.js, 'sender');
    assert.equal(result[1].operator, '==');
    assert.equal(result[1].right.value.js, 'param1');
    assert.equal(result[1].right.value.memberName, 'to');
    assert.equal(result[1].from, BranchType.notDefined);
    assert.equal(result[1].left.elementType, RestrictionElementType.transactionParameter);
    assert.equal(result[1].right.elementType, RestrictionElementType.functionParameterMember);
  });
});
