import path from 'path';
import chai from 'chai';
import 'mocha';
import { getBinaryOperationRestriction } from '../../../services/solidityParser';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { TruffleProjectIceFactoryFunction } from '../../../services/TruffleFactory';
import { IRestriction } from '../../../models/IResctriction';
import { BranchType } from '../../../models/BranchType';
import { RestrictionElementType } from '../../../models/RestrictionElementType';
import { extractTruffleProjectGraph } from '../../../services/TruffleProjectGraph';
import {
  getTruffleProject,
  getContractByName,
  getBOFromRequireCall,
  getFunctionByName,
  getRequireCalls,
} from '../../utils';

const assert = chai.assert;

describe('solidityParser.BinaryOperation - RestrictionType.functionParameter_functionParameter: getBinaryOperationRestriction', function() {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contract: ContractDefinition;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
    contract = getContractByName(truffleProject.getContracts(), 'RestrictionTypesTest');
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  it('param1 > param2', async function() {
    const funcTest: FunctionDefinition = getFunctionByName(
      contract,
      'simpleRequires_functionParameter_functionParameter'
    );
    const requireCalls = getRequireCalls(funcTest);
    const bo = getBOFromRequireCall(requireCalls[0]);
    const result: IRestriction[] = getBinaryOperationRestriction(truffleProject, contract, bo, funcTest);
    assert.isArray(result);
    assert.equal(result.length, 1);
    assert.equal(result[0].left.value.js, 'teste');
    assert.equal(result[0].operator, '==');
    assert.equal(result[0].right.value.js, 'teste2');
    assert.equal(result[0].from, BranchType.notDefined);
    assert.equal(result[0].right.elementType, RestrictionElementType.functionParameter);
    assert.equal(result[0].left.elementType, RestrictionElementType.functionParameter);
  });

  it('param1 > param2 && param1 > param3', async function() {
    const funcTest: FunctionDefinition = getFunctionByName(
      contract,
      'simpleRequires_functionParameter_functionParameter'
    );
    const requireCalls = getRequireCalls(funcTest);
    const bo = getBOFromRequireCall(requireCalls[1]);
    const result: IRestriction[] = getBinaryOperationRestriction(truffleProject, contract, bo, funcTest);
    assert.isArray(result);
    assert.equal(result.length, 2);
    assert.equal(result[0].left.value.js, 'teste');
    assert.equal(result[0].operator, '>');
    assert.equal(result[0].right.value.js, 'teste2');
    assert.equal(result[0].from, BranchType.notDefined);
    assert.equal(result[0].right.elementType, RestrictionElementType.functionParameter);
    assert.equal(result[0].left.elementType, RestrictionElementType.functionParameter);
    assert.equal(result[1].left.value.js, 'teste');
    assert.equal(result[1].operator, '>');
    assert.equal(result[1].right.value.js, 'teste3');
    assert.equal(result[1].from, BranchType.notDefined);
    assert.equal(result[1].right.elementType, RestrictionElementType.functionParameter);
    assert.equal(result[1].left.elementType, RestrictionElementType.functionParameter);
  });
});
