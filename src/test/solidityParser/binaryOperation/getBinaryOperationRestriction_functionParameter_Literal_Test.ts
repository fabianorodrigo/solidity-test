import path from 'path';
import chai from 'chai';
import 'mocha';
import { getBinaryOperationRestriction } from '../../../services/solidityParser';
import { BinaryOperation, StringLiteral, ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { TruffleProjectIceFactoryFunction } from '../../../services/TruffleFactory';
import { extractTruffleProjectGraph } from '../../../services/TruffleProjectGraph';
import { RestrictionElementType } from '../../../models/RestrictionElementType';
import { BranchType } from '../../../models/BranchType';

const assert = chai.assert;

import {
  getTruffleProject,
  getContractByName,
  getBOFromRequireCall,
  getFunctionByName,
  getRequireCalls,
  truffleProjectEmpty,
  functionEmpty,
} from '../../utils';

describe('solidityParser.BinaryOperation - RestrictionType.functionParameter_Literal: getBinaryOperationRestriction', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contract: ContractDefinition;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
    contract = getContractByName(truffleProject.getContracts(), 'RestrictionTypesTest');
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });
  describe('Tipo: INTEIRO', function () {
    it('param > literal', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(contract, 'simpleRequires_functionParameter_Literal');
      const requireCalls = getRequireCalls(funcTest);
      const bo = getBOFromRequireCall(requireCalls[1]);
      const result = getBinaryOperationRestriction(truffleProject, contract, bo, funcTest);
      assert.isArray(result);
      assert.equal(result.length, 1);
      assert.equal(result[0].operator, '>');
      assert.equal(result[0].right.value.js, 0);
      assert.equal(result[0].right.elementType, RestrictionElementType.Literal);
      assert.equal(result[0].left.elementType, RestrictionElementType.functionParameter);
    });
    it('Literal< param', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(contract, 'simpleRequires_functionParameter_Literal');
      const requireCalls = getRequireCalls(funcTest);
      const bo = getBOFromRequireCall(requireCalls[5]);
      const result = getBinaryOperationRestriction(truffleProject, contract, bo, funcTest);
      assert.isArray(result);
      assert.equal(result.length, 1);
      assert.equal(result[0].operator, '<');
      assert.equal(result[0].right.value.js, 'value2');
      assert.equal(result[0].left.value.js, 1979);
      assert.equal(result[0].right.elementType, RestrictionElementType.functionParameter);
      assert.equal(result[0].left.elementType, RestrictionElementType.Literal);
    });

    it('param == "literal" && param < 1000', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(contract, 'simpleRequires_functionParameter_Literal');
      const requireCalls = getRequireCalls(funcTest);
      const bo = getBOFromRequireCall(requireCalls[6]);
      const result = getBinaryOperationRestriction(truffleProject, contract, bo, funcTest);
      assert.isArray(result);
      assert.equal(result.length, 2);
      assert.equal(result[0].left.value.js, 'paramString');
      assert.equal(result[0].operator, '==');
      assert.equal(result[0].right.value.js, 'teste');
      assert.equal(result[0].from, BranchType.notDefined);
      assert.equal(result[0].right.elementType, RestrictionElementType.Literal);
      assert.equal(result[0].left.elementType, RestrictionElementType.functionParameter);
      assert.equal(result[1].left.value.js, 'value1');
      assert.equal(result[1].operator, '<');
      assert.equal(result[1].right.value.js, 1000);
      assert.equal(result[1].from, BranchType.notDefined);
      assert.equal(result[1].right.elementType, RestrictionElementType.Literal);
      assert.equal(result[1].left.elementType, RestrictionElementType.functionParameter);
    });
  });

  describe('Tipo: ADDRESS', function () {
    describe('Obtenção de restrições de um determinado parâmetro de entrada de função em um BinaryOperation', function () {
      let truffleProject: TruffleProjectIceFactoryFunction;
      let contract: ContractDefinition;

      before(() => {
        truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
        contract = getContractByName(truffleProject.getContracts(), 'RestrictionTypesTest');
        //apurando e setando o grafo do projeto
        truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
      });

      it('Para parâmetro na esquerda, o operador relacional', async function () {
        const funcTest: FunctionDefinition = getFunctionByName(contract, 'simpleRequires_functionParameter_Literal');
        const requireCalls = getRequireCalls(funcTest);
        const bo = getBOFromRequireCall(requireCalls[4]);
        const result = getBinaryOperationRestriction(truffleProject, contract, bo, funcTest);
        assert.isArray(result);
        assert.equal(result.length, 1, 'Restrictions quantity not matching');
        assert.equal(result[0].operator, '==');
        assert.equal(result[0].right.value.js, 0x0000000000000000000000000000000000000000);
      });
    });
  });

  describe('Tipo: BOOLEAN', function () {
    describe('Obtenção de restrições de um determinado parâmetro de entrada de função em um BinaryOperation', function () {
      it('Para parâmetro na esquerda, o operador relacional', async function () {
        const contract: ContractDefinition = getContractByName(truffleProject.getContracts(), 'RestrictionTypesTest');
        const funcTest: FunctionDefinition = getFunctionByName(contract, 'simpleRequires_functionParameter_Literal');
        const requireCalls = getRequireCalls(funcTest);

        const result = getBinaryOperationRestriction(
          truffleProject,
          contract,
          getBOFromRequireCall(requireCalls[2]),
          funcTest
        );
        assert.isArray(result);
        assert.equal(result.length, 1, 'Restrictions quantity not matching');
        assert.equal(result[0].operator, '==');
        assert.equal(result[0].right.value.js, 'true');
        assert.equal(result[0].right.value.sol, 'true');
      });
    });
  });

  describe('Tipo: STRING', function () {
    let truffleProject: TruffleProjectIceFactoryFunction;
    let contract: ContractDefinition;

    before(() => {
      truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
      contract = getContractByName(truffleProject.getContracts(), 'RestrictionTypesTest');
      //apurando e setando o grafo do projeto
      truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
    });
    it('Para parâmetro na esquerda, o operador relacional', async function () {
      const funcTest: FunctionDefinition = getFunctionByName(contract, 'simpleRequires_functionParameter_Literal');
      const requireCalls = getRequireCalls(funcTest);
      const bo = getBOFromRequireCall(requireCalls[3]);

      const result = getBinaryOperationRestriction(truffleProject, contract, bo, funcTest);
      assert.isArray(result);
      assert.equal(result.length, 1, 'Restrictions quantity not matching');
      assert.equal(result[0].operator, '==');
      assert.equal(result[0].right.value.js, 'The Test');
    });
  });
});
