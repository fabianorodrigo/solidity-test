import path from 'path';
import chai from 'chai';
import 'mocha';
import { getBinaryOperationRestriction } from '../../../services/solidityParser';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { TruffleProjectIceFactoryFunction } from '../../../services/TruffleFactory';
import { IRestriction } from '../../../models/IResctriction';
import { BranchType } from '../../../models/BranchType';
import { extractTruffleProjectGraph } from '../../../services/TruffleProjectGraph';
import { RestrictionElementType } from '../../../models/RestrictionElementType';

const assert = chai.assert;

import {
  getTruffleProject,
  getContractByName,
  getBOFromRequireCall,
  getFunctionByName,
  getRequireCalls,
} from '../../utils';

describe('solidityParser.BinaryOperation - RestrictionType.stateVar_Literal: getBinaryOperationRestriction', function() {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contract: ContractDefinition;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
    contract = getContractByName(truffleProject.getContracts(), 'RestrictionTypesTest');
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  it('stateVariable > 0', async function() {
    const funcTest: FunctionDefinition = getFunctionByName(contract, 'simpleRequires_StateVar_Literal');
    const requireCalls = getRequireCalls(funcTest);
    const bo = getBOFromRequireCall(requireCalls[0]);

    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));

    const result: IRestriction[] = getBinaryOperationRestriction(truffleProject, contract, bo, funcTest);
    assert.isArray(result);
    assert.equal(result.length, 1);
    assert.equal(result[0].left.value.js, 'stateUint');
    assert.equal(result[0].operator, '>');
    assert.equal(result[0].right.value.js, 0);
    assert.equal(result[0].from, BranchType.notDefined);
    assert.equal(result[0].right.elementType, RestrictionElementType.Literal);
    assert.equal(result[0].left.elementType, RestrictionElementType.stateVariable);
  });
  it('0 < stateVariable', async function() {
    const funcTest: FunctionDefinition = getFunctionByName(contract, 'simpleRequires_StateVar_Literal');
    const requireCalls = getRequireCalls(funcTest);
    const bo = getBOFromRequireCall(requireCalls[1]);

    const result: IRestriction[] = getBinaryOperationRestriction(truffleProject, contract, bo, funcTest);
    assert.isArray(result);
    assert.equal(result.length, 1);
    assert.equal(result[0].left.value.js, 0);
    assert.equal(result[0].operator, '<');
    assert.equal(result[0].right.value.js, 'stateUint');
    assert.equal(result[0].from, BranchType.notDefined);
    assert.equal(result[0].right.elementType, RestrictionElementType.stateVariable);
    assert.equal(result[0].left.elementType, RestrictionElementType.Literal);
  });

  it('stateVariable == "teste" && stateVariable2 < 1000', async function() {
    const funcTest: FunctionDefinition = getFunctionByName(contract, 'simpleRequires_StateVar_Literal');
    const requireCalls = getRequireCalls(funcTest);
    const bo = getBOFromRequireCall(requireCalls[2]);
    const result: IRestriction[] = getBinaryOperationRestriction(truffleProject, contract, bo, funcTest);
    assert.isArray(result);
    assert.equal(result.length, 2);
    assert.equal(result[0].left.value.js, 'stateString');
    assert.equal(result[0].operator, '==');
    assert.equal(result[0].right.value.js, 'teste');
    assert.equal(result[0].from, BranchType.notDefined);
    assert.equal(result[0].right.elementType, RestrictionElementType.Literal);
    assert.equal(result[0].left.elementType, RestrictionElementType.stateVariable);
    assert.equal(result[1].left.value.js, 'stateUint');
    assert.equal(result[1].operator, '<');
    assert.equal(result[1].right.value.js, 1000);
    assert.equal(result[1].from, BranchType.notDefined);
    assert.equal(result[1].right.elementType, RestrictionElementType.Literal);
    assert.equal(result[1].left.elementType, RestrictionElementType.stateVariable);
  });
});
