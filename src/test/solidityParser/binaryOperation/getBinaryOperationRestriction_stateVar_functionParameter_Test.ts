import path from 'path';
import chai from 'chai';
import 'mocha';
import { getBinaryOperationRestriction } from '../../../services/solidityParser';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { TruffleProjectIceFactoryFunction } from '../../../services/TruffleFactory';
import { IRestriction } from '../../../models/IResctriction';
import { BranchType } from '../../../models/BranchType';
import { RestrictionElementType } from '../../../models/RestrictionElementType';
import { extractTruffleProjectGraph } from '../../../services/TruffleProjectGraph';

const assert = chai.assert;

import {
  getTruffleProject,
  getContractByName,
  getBOFromRequireCall,
  getFunctionByName,
  getRequireCalls,
} from '../../utils';

describe('solidityParser.BinaryOperation - RestrictionType.stateVar_functionParameter: getBinaryOperationRestriction', function() {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contract: ContractDefinition;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
    contract = getContractByName(truffleProject.getContracts(), 'RestrictionTypesTest');
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  it('stateVariable > param', async function() {
    const funcTest: FunctionDefinition = getFunctionByName(contract, 'simpleRequires_StateVar_functionParameter');
    const requireCalls = getRequireCalls(funcTest);
    const bo = getBOFromRequireCall(requireCalls[0]);

    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));

    const result: IRestriction[] = getBinaryOperationRestriction(truffleProject, contract, bo, funcTest);
    assert.isArray(result);
    assert.equal(result.length, 1);
    assert.equal(result[0].left.value.js, 'stateUint');
    assert.equal(result[0].operator, '>');
    assert.equal(result[0].right.value.js, 'value1');
    assert.equal(result[0].from, BranchType.notDefined);
    assert.equal(result[0].left.elementType, RestrictionElementType.stateVariable);
    assert.equal(result[0].right.elementType, RestrictionElementType.functionParameter);

    it('param < stateVariable', async function() {
      const funcTest: FunctionDefinition = getFunctionByName(contract, 'simpleRequires_StateVar_functionParameter');
      const requireCalls = getRequireCalls(funcTest);
      const bo = getBOFromRequireCall(requireCalls[1]);

      //apurando e setando o grafo do projeto
      truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));

      const result: IRestriction[] = getBinaryOperationRestriction(truffleProject, contract, bo, funcTest);
      assert.isArray(result);
      assert.equal(result.length, 1);
      assert.equal(result[0].left.value.js, 'value1');
      assert.equal(result[0].operator, '<');
      assert.equal(result[0].right.value.js, 'stateUint');
      assert.equal(result[0].from, BranchType.notDefined);
      assert.equal(result[0].left.elementType, RestrictionElementType.functionParameter);
      assert.equal(result[0].right.elementType, RestrictionElementType.stateVariable);
    });
  });

  it('stateVariable == param3 && stateVariable2 < param', async function() {
    const funcTest: FunctionDefinition = getFunctionByName(contract, 'simpleRequires_StateVar_functionParameter');
    const requireCalls = getRequireCalls(funcTest);
    const bo = getBOFromRequireCall(requireCalls[2]);

    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));

    const result: IRestriction[] = getBinaryOperationRestriction(truffleProject, contract, bo, funcTest);
    assert.isArray(result);
    assert.equal(result.length, 2);
    assert.equal(result[0].left.value.js, 'stateString');
    assert.equal(result[0].operator, '==');
    assert.equal(result[0].right.value.js, 'value3');
    assert.equal(result[0].from, BranchType.notDefined);
    assert.equal(result[0].left.elementType, RestrictionElementType.stateVariable);
    assert.equal(result[0].right.elementType, RestrictionElementType.functionParameter);
    assert.equal(result[1].left.value.js, 'stateUint');
    assert.equal(result[1].operator, '<');
    assert.equal(result[1].right.value.js, 'value2');
    assert.equal(result[1].from, BranchType.notDefined);
    assert.equal(result[1].left.elementType, RestrictionElementType.stateVariable);
    assert.equal(result[1].right.elementType, RestrictionElementType.functionParameter);
  });
});
