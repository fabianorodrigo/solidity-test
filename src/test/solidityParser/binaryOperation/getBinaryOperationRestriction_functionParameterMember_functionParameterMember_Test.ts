import path from 'path';
import chai from 'chai';
import 'mocha';
import { getBinaryOperationRestriction } from '../../../services/solidityParser';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { TruffleProjectIceFactoryFunction } from '../../../services/TruffleFactory';
import { IRestriction } from '../../../models/IResctriction';
import { BranchType } from '../../../models/BranchType';
import { RestrictionElementType } from '../../../models/RestrictionElementType';

const assert = chai.assert;

import {
  getTruffleProject,
  getContractByName,
  getBOFromRequireCall,
  getFunctionByName,
  getRequireCalls,
} from '../../utils';
import { extractTruffleProjectGraph } from '../../../services/TruffleProjectGraph';

describe('solidityParser.BinaryOperation - RestrictionType.functionParameterMember_functionParameterMember: getBinaryOperationRestriction', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  describe('Obtenção de restrições  de um determinado atributo de parâmetro de entrada de função em um BinaryOperation', function () {
    it('param1.member > param2.member', async function () {
      const contract: ContractDefinition = getContractByName(truffleProject.getContracts(), 'RestrictionTypesTest');
      const funcTest: FunctionDefinition = getFunctionByName(
        contract,
        'simpleRequires_functionParameterMember_functionParameterMember'
      );
      const requireCalls = getRequireCalls(funcTest);
      const bo = getBOFromRequireCall(requireCalls[0]);
      const result: IRestriction[] = getBinaryOperationRestriction(truffleProject, contract, bo, funcTest);
      assert.isArray(result);
      assert.equal(result.length, 1);
      assert.equal(result[0].left.value.js, 'param1');
      assert.equal(result[0].left.value.memberName, 'a');
      assert.equal(result[0].operator, '>');
      assert.equal(result[0].right.value.js, 'param2');
      assert.equal(result[0].right.value.memberName, 'a');
      assert.equal(result[0].from, BranchType.notDefined);
      assert.equal(result[0].right.elementType, RestrictionElementType.functionParameterMember);
      assert.equal(result[0].left.elementType, RestrictionElementType.functionParameterMember);
    });
  });

  describe('Obtenção de restrições de um determinado parâmetro de função em um BinaryOperations aninhados', function () {
    it('param2.member > param3.member && param3.member < param1.member', async function () {
      const contract: ContractDefinition = getContractByName(truffleProject.getContracts(), 'RestrictionTypesTest');
      const funcTest: FunctionDefinition = getFunctionByName(
        contract,
        'simpleRequires_functionParameterMember_functionParameterMember'
      );
      const requireCalls = getRequireCalls(funcTest);
      const bo = getBOFromRequireCall(requireCalls[1]);
      const result: IRestriction[] = getBinaryOperationRestriction(truffleProject, contract, bo, funcTest);
      assert.isArray(result);
      assert.equal(result.length, 2);
      assert.equal(result[0].left.value.js, 'param2');
      assert.equal(result[0].left.value.memberName, 'a');
      assert.equal(result[0].operator, '>');
      assert.equal(result[0].right.value.js, 'param3');
      assert.equal(result[0].right.value.memberName, 'a');
      assert.equal(result[0].from, BranchType.notDefined);
      assert.equal(result[0].right.elementType, RestrictionElementType.functionParameterMember);
      assert.equal(result[0].left.elementType, RestrictionElementType.functionParameterMember);
      assert.equal(result[1].left.value.js, 'param3');
      assert.equal(result[1].left.value.memberName, 'a');
      assert.equal(result[1].operator, '<');
      assert.equal(result[1].right.value.js, 'param1');
      assert.equal(result[1].right.value.memberName, 'a');
      assert.equal(result[1].from, BranchType.notDefined);
      assert.equal(result[1].right.elementType, RestrictionElementType.functionParameterMember);
      assert.equal(result[1].left.elementType, RestrictionElementType.functionParameterMember);
    });
  });
});
