import path from 'path';
import chai from 'chai';
import 'mocha';
import { getBinaryOperationRestriction } from '../../../services/solidityParser';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import { TruffleProjectIceFactoryFunction } from '../../../services/TruffleFactory';
import { IRestriction } from '../../../models/IResctriction';
import { BranchType } from '../../../models/BranchType';
import { RestrictionElementType } from '../../../models/RestrictionElementType';

const assert = chai.assert;

import {
  getTruffleProject,
  getContractByName,
  getBOFromRequireCall,
  getFunctionByName,
  getRequireCalls,
} from '../../utils';
import { extractTruffleProjectGraph } from '../../../services/TruffleProjectGraph';

describe('solidityParser.BinaryOperation - RestrictionType.transactionParameter_Literal: getBinaryOperationRestriction', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  it('msg.value > 0', async function () {
    const contract: ContractDefinition = getContractByName(truffleProject.getContracts(), 'RestrictionTypesTest');
    const funcTest: FunctionDefinition = getFunctionByName(contract, 'simpleRequires_transactionParameter_Literal');
    const requireCalls = getRequireCalls(funcTest);
    const bo = getBOFromRequireCall(requireCalls[0]);
    const result: IRestriction[] = getBinaryOperationRestriction(truffleProject, contract, bo, funcTest);
    assert.isArray(result);
    assert.equal(result.length, 1);
    assert.equal(result[0].left.value.js, 'value');
    assert.equal(result[0].operator, '>');
    assert.equal(result[0].right.value.js, 0);
    assert.equal(result[0].from, BranchType.notDefined);
    assert.equal(result[0].right.elementType, RestrictionElementType.Literal);
    assert.equal(result[0].left.elementType, RestrictionElementType.transactionParameter);
  });

  it('msg.value > 0 && msg.sender != 0x0', async function () {
    const contract: ContractDefinition = getContractByName(truffleProject.getContracts(), 'RestrictionTypesTest');
    const funcTest: FunctionDefinition = getFunctionByName(contract, 'simpleRequires_transactionParameter_Literal');
    const requireCalls = getRequireCalls(funcTest);
    const bo = getBOFromRequireCall(requireCalls[1]);
    const result: IRestriction[] = getBinaryOperationRestriction(truffleProject, contract, bo, funcTest);
    assert.isArray(result);
    assert.equal(result.length, 2);
    assert.equal(result[0].left.value.js, 'value');
    assert.equal(result[0].operator, '>');
    assert.equal(result[0].right.value.js, 0);
    assert.equal(result[0].from, BranchType.notDefined);
    assert.equal(result[0].right.elementType, RestrictionElementType.Literal);
    assert.equal(result[0].left.elementType, RestrictionElementType.transactionParameter);
    assert.equal(result[1].left.value.js, 'sender');
    assert.equal(result[1].operator, '!=');
    assert.equal(result[1].right.value.js, '0x0000000000000000000000000000000000000000');
    assert.equal(result[1].from, BranchType.notDefined);
    assert.equal(result[1].right.elementType, RestrictionElementType.Literal);
    assert.equal(result[1].left.elementType, RestrictionElementType.transactionParameter);
  });
});
