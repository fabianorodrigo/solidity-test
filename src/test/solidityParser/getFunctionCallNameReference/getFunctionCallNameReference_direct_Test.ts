import path from 'path';
import chai from 'chai';
import 'mocha';
import { getBranches, getFunctionCallNameReference } from '../../../services/solidityParser';
import { extractTruffleProjectGraph } from '../../../services/TruffleProjectGraph';
import { TruffleProjectIceFactoryFunction } from '../../../services/TruffleFactory';
import { IContract } from '../../../models/IContract';
import {
  getTruffleProject,
  getContractByName,
  getBOFromRequireCall,
  getFunctionByName,
  getRequireCalls,
} from '../../utils';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';

const assert = chai.assert;

describe('Solidity Parser', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contracts: IContract[];
  let contract: ContractDefinition;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
    contracts = truffleProject.getContracts();
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
    contract = getContractByName(contracts, 'RequireUse');
  });

  describe('Get FunctionCallNameReference', function () {
    describe('Chamada de função simples', function () {
      describe('SEM filtro da quantidade de parâmetros de entrada das funções', function () {
        it('Chama função passando, não um parâmetro, mas um literal: retorna array vazio', async function () {
          const func: FunctionDefinition = getFunctionByName(contract, 'simpleFunctionCallLiteral');
          const calls = getFunctionCallNameReference(truffleProject, contract, func, 'd');
          assert.isArray(calls);
          assert.equal(calls.length, 0);
        });

        it('Chama 2x a mesma função com 1 parâmetro de entrada, uma passando literal e a outra passando o parâmetro de entrada em análise, e uma outra função com 3 parâmetros de entrada passando o mesmo parâmetro em análise: retorna 2 ocorrências, apenas a que usa literal não retorna', async function () {
          const func: FunctionDefinition = getFunctionByName(contract, 'simpleFunctionCall');
          const calls = getFunctionCallNameReference(truffleProject, contract, func, 'teste');
          assert.isArray(calls);
          assert.equal(calls.length, 2);
          assert.equal(calls[0], 'setName');
          assert.equal(calls[1], 'simpleLocalStringWrite');
        });
      });
      describe('COM filtro da quantidade de parâmetros de entrada das funções', function () {
        it('Chama 2x a mesma função com 1 parâmetro de entrada, uma passando literal e a outra passando o parâmetro de entrada em análise, e uma outra função com 3 parâmetros de entrada passando o mesmo parâmetro em análise: retorna apenas a ocorrência em que o número de parâmetros de entrada seja igual ao solicitado (1)', async function () {
          const func: FunctionDefinition = getFunctionByName(contract, 'simpleFunctionCall');
          const calls = getFunctionCallNameReference(truffleProject, contract, func, 'teste', 1);
          assert.isArray(calls);
          assert.equal(calls.length, 1);
          assert.equal(calls[0], 'setName');
        });
      });
    });
    describe('Chamada de função que chama outra função (aninhadas)', function () {
      describe('SEM filtro da quantidade de parâmetros de entrada das funções', function () {
        it('2 níveis - Chamada de função que chama outra função repassando um parâmetro de entrada: retorna a função chamada diretamente a função chamada indiretamente', async function () {
          const func: FunctionDefinition = getFunctionByName(contract, 'nestedFunctionCall');
          const calls = getFunctionCallNameReference(truffleProject, contract, func, 'a');
          assert.isArray(calls);
          assert.equal(calls.length, 1);
          assert.equal(calls[0], 'simpleFunctionCallLiteral');
        });

        it('Chamada de função que chama outras duas funções repassando um parâmetro de entrada: retorna a função chamada diretamente as duas funções chamadas indiretamente', async function () {
          const func: FunctionDefinition = getFunctionByName(contract, 'nestedNestedFunctionCall');
          const calls = getFunctionCallNameReference(truffleProject, contract, func, 'c');
          assert.isArray(calls);
          assert.equal(calls.length, 3);
          assert.equal(calls[0], 'simpleFunctionCall');
          assert.equal(calls[1], 'setName');
          assert.equal(calls[2], 'simpleLocalStringWrite');
        });
      });
    });
  });
});
