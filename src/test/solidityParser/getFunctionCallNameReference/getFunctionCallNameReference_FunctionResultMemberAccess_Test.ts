import path from 'path';
import chai from 'chai';
import 'mocha';
import { getBranches, getFunctionCallNameReference } from '../../../services/solidityParser';
import { extractTruffleProjectGraph } from '../../../services/TruffleProjectGraph';
import { TruffleProjectIceFactoryFunction } from '../../../services/TruffleFactory';
import { IContract } from '../../../models/IContract';
import {
  getTruffleProject,
  getContractByName,
  getBOFromRequireCall,
  getFunctionByName,
  getRequireCalls,
} from '../../utils';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';

const assert = chai.assert;

describe('Solidity Parser', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contracts: IContract[];
  let contract: ContractDefinition;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
    contracts = truffleProject.getContracts();
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
    contract = getContractByName(contracts, 'RequireUse');
  });

  describe('Get FunctionCallNameReference', function () {
    describe('Chamada de membro do resultado da função', function () {
      describe('COM filtro da quantidade de parâmetros de entrada das funções', function () {
        it('Acesso a membro do resultado da chamada de função sendo atribuído com valor inicial de variável: retorna a função chamada', async function () {
          const func: FunctionDefinition = getFunctionByName(
            contract,
            'functionMemberAccessFunctionResultVariableInitialValue'
          );
          const calls = getFunctionCallNameReference(truffleProject, contract, func, 'param1', 1);
          assert.isArray(calls);
          assert.equal(calls.length, 1);
          assert.equal(calls[0], 'getStructTeste');
        });

        it('Acesso a membro do resultado da chamada de função sendo atribuído a variável: retorna a função chamada', async function () {
          const func: FunctionDefinition = getFunctionByName(
            contract,
            'functionMemberAccessFunctionResultVariableSetValue'
          );
          const calls = getFunctionCallNameReference(truffleProject, contract, func, 'param1', 1);
          assert.isArray(calls);
          assert.equal(calls.length, 1);
          assert.equal(calls[0], 'getStructTeste');
        });

        it('Acesso a membro do resultado da chamada de função sendo utilizado em require: retorna a função chamada', async function () {
          const func: FunctionDefinition = getFunctionByName(contract, 'functionMemberAccessFunctionResultRequire');
          const calls = getFunctionCallNameReference(truffleProject, contract, func, 'param1', 1);
          assert.isArray(calls);
          assert.equal(calls.length, 1);
          assert.equal(calls[0], 'getStructTeste');
        });
      });
    });
  });
});
