import path from 'path';
import chai from 'chai';
import 'mocha';
import { getBranches, getFunctionCallNameReference } from '../../../services/solidityParser';
import { extractTruffleProjectGraph, getFunctionGraph } from '../../../services/TruffleProjectGraph';
import { TruffleProjectIceFactoryFunction } from '../../../services/TruffleFactory';
import { IContract } from '../../../models/IContract';
import {
  getTruffleProject,
  getContractByName,
  getBOFromRequireCall,
  getFunctionByName,
  getRequireCalls,
} from '../../utils';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import {
  searchFunctionCallReferencingNames,
  searchThroughoutStatements,
} from '../../../services/solidityParser/searcher';
import { TypeSearchResultList } from '../../../models';

const assert = chai.assert;

describe('Solidity Parser', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contracts: IContract[];
  let contract: ContractDefinition;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
    contracts = truffleProject.getContracts();
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
    contract = getContractByName(contracts, 'RequireUse');
  });

  describe('Get searchFunctionCallReferencingNames', function () {
    describe('Chamada de função simples', function () {
      describe('SEM filtro da quantidade de parâmetros de entrada das funções', function () {
        it('Chama função passando, não um parâmetro, mas um literal: retorna array vazio', async function () {
          const func: FunctionDefinition = getFunctionByName(contract, 'simpleFunctionCallLiteral');
          //const calls = getFunctionCallNameReference(truffleProject, contract, func, 'd');
          const searchName = 'functionsCallsReferencingName';
          const result: TypeSearchResultList = searchThroughoutStatements(
            truffleProject,
            getFunctionGraph(truffleProject.getGraph(), contract, func),
            func,
            [
              {
                name: searchName,

                searchFunction: searchFunctionCallReferencingNames,
                functionParametersNames: ['d'],
                args: {
                  /*parameterCount: 1,*/
                },
              },
            ]
          );

          assert.isArray(result[searchName]);
          assert.equal(result[searchName].length, 0);
        });

        it('Chama 2x a mesma função com 1 parâmetro de entrada, uma passando literal e a outra passando o parâmetro de entrada em análise, e uma outra função com 3 parâmetros de entrada passando o mesmo parâmetro em análise: retorna 2 ocorrências, apenas a que usa literal não retorna', async function () {
          const func: FunctionDefinition = getFunctionByName(contract, 'simpleFunctionCall');
          //const calls = getFunctionCallNameReference(truffleProject, contract, func, 'teste');
          const searchName = 'functionsCallsReferencingName';
          const result: TypeSearchResultList = searchThroughoutStatements(
            truffleProject,
            getFunctionGraph(truffleProject.getGraph(), contract, func),
            func,
            [
              {
                name: searchName,

                searchFunction: searchFunctionCallReferencingNames,
                functionParametersNames: ['teste'],
                args: {
                  /*parameterCount: 1,*/
                },
              },
            ]
          );

          assert.isArray(result[searchName]);
          assert.equal(result[searchName].length, 2);
          assert.equal(result[searchName][0].name, 'teste');
          assert.equal(result[searchName][0].functionName, 'setName');
          assert.equal(result[searchName][1].name, 'teste');
          assert.equal(result[searchName][1].functionName, 'simpleLocalStringWrite');
        });
      });
      describe('COM filtro da quantidade de parâmetros de entrada das funções', function () {
        it('Chama 2x a mesma função com 1 parâmetro de entrada, uma passando literal e a outra passando o parâmetro de entrada em análise, e uma outra função com 3 parâmetros de entrada passando o mesmo parâmetro em análise: retorna apenas a ocorrência em que o número de parâmetros de entrada seja igual ao solicitado (1)', async function () {
          const func: FunctionDefinition = getFunctionByName(contract, 'simpleFunctionCall');
          //const calls = getFunctionCallNameReference(truffleProject, contract, func, 'teste', 1);
          const searchName = 'functionsCallsReferencingName';
          const result: TypeSearchResultList = searchThroughoutStatements(
            truffleProject,
            getFunctionGraph(truffleProject.getGraph(), contract, func),
            func,
            [
              {
                name: searchName,
                searchFunction: searchFunctionCallReferencingNames,
                functionParametersNames: ['teste'],
                args: { parameterCount: 1 },
              },
            ]
          );

          assert.isArray(result[searchName]);
          assert.equal(result[searchName].length, 1);
          assert.equal(result[searchName][0].name, 'teste');
          assert.equal(result[searchName][0].functionName, 'setName');
        });
      });
    });
    describe('Chamada de função que chama outra função (aninhadas)', function () {
      describe('SEM filtro da quantidade de parâmetros de entrada das funções', function () {
        it('2 níveis - Chamada de função que chama outra função repassando um parâmetro de entrada: retorna a função chamada diretamente a função chamada indiretamente', async function () {
          const func: FunctionDefinition = getFunctionByName(contract, 'nestedFunctionCall');
          //const calls = getFunctionCallNameReference(truffleProject, contract, func, 'a');
          const searchName = 'functionsCallsReferencingName';
          const result: TypeSearchResultList = searchThroughoutStatements(
            truffleProject,
            getFunctionGraph(truffleProject.getGraph(), contract, func),
            func,
            [
              {
                name: searchName,
                searchFunction: searchFunctionCallReferencingNames,
                functionParametersNames: ['a'],
              },
            ]
          );

          assert.isArray(result[searchName]);
          assert.equal(result[searchName].length, 1);
          assert.equal(result[searchName][0].name, 'a');
          assert.equal(result[searchName][0].functionName, 'simpleFunctionCallLiteral');
        });

        it('Chamada de função que chama outras duas funções repassando um parâmetro de entrada: retorna a função chamada diretamente as duas funções chamadas indiretamente', async function () {
          const func: FunctionDefinition = getFunctionByName(contract, 'nestedNestedFunctionCall');
          //          const calls = getFunctionCallNameReference(truffleProject, contract, func, 'c');
          const searchName = 'functionsCallsReferencingName';
          const result: TypeSearchResultList = searchThroughoutStatements(
            truffleProject,
            getFunctionGraph(truffleProject.getGraph(), contract, func),
            func,
            [
              {
                name: searchName,
                searchFunction: searchFunctionCallReferencingNames,
                functionParametersNames: ['c'],
                args: {
                  /*parameterCount: 1,*/
                },
              },
            ]
          );
          assert.isArray(result[searchName]);
          assert.equal(result[searchName].length, 3);
          assert.equal(result[searchName][0].name, 'c');
          assert.equal(result[searchName][0].functionName, 'setName');
          assert.equal(result[searchName][1].name, 'c');
          assert.equal(result[searchName][1].functionName, 'simpleLocalStringWrite');
          assert.equal(result[searchName][2].name, 'c');
          assert.equal(result[searchName][2].functionName, 'simpleFunctionCall');
        });
      });
    });
  });
});
