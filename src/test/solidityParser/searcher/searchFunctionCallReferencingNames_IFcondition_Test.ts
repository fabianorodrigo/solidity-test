import path from 'path';
import chai from 'chai';
import 'mocha';
import { getFunctionCallNameReference } from '../../../services/solidityParser';
import { extractTruffleProjectGraph, getFunctionGraph } from '../../../services/TruffleProjectGraph';
import { TruffleProjectIceFactoryFunction } from '../../../services/TruffleFactory';
import { IContract } from '../../../models/IContract';
import { getTruffleProject, getContractByName, getFunctionByName } from '../../utils';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import {
  searchFunctionCallReferencingNames,
  searchThroughoutStatements,
} from '../../../services/solidityParser/searcher';
import { TypeSearchResultList } from '../../../models';

const assert = chai.assert;

describe('Solidity Parser', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contracts: IContract[];
  let contract: ContractDefinition;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
    contracts = truffleProject.getContracts();
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
    contract = getContractByName(contracts, 'RequireUse');
  });

  describe('Get FunctionCallNameReference', function () {
    describe('Chamada de função na CONDITION de IF, ELSEIF, ELSE', function () {
      describe('COM filtro da quantidade de parâmetros de entrada das funções', function () {
        it('Chamada de função na condição do IF: retorna a função chamada', async function () {
          const func: FunctionDefinition = getFunctionByName(contract, 'functionCallOnIFConditional');
          //const calls = getFunctionCallNameReference(truffleProject, contract, func, 'param1', 1);

          const searchName = 'functionsCallsReferencingName';
          const result: TypeSearchResultList = searchThroughoutStatements(
            truffleProject,
            getFunctionGraph(truffleProject.getGraph(), contract, func),
            func,
            [
              {
                name: searchName,

                searchFunction: searchFunctionCallReferencingNames,
                functionParametersNames: ['param1'],
                args: {
                  parameterCount: 1,
                },
              },
            ]
          );
          assert.isArray(result[searchName]);
          assert.equal(result[searchName].length, 1);
          assert.equal(result[searchName][0].name, 'param1');
          assert.equal(result[searchName][0].functionName, 'simpleFunctionCallLiteral');
        });

        it('Chamada de função na condição do ELSEIF: retorna a função chamada', async function () {
          const func: FunctionDefinition = getFunctionByName(contract, 'functionCallOnELSEIFConditional');
          //const calls = getFunctionCallNameReference(truffleProject, contract, func, 'param1', 1);

          const searchName = 'functionsCallsReferencingName';
          const result: TypeSearchResultList = searchThroughoutStatements(
            truffleProject,
            getFunctionGraph(truffleProject.getGraph(), contract, func),
            func,
            [
              {
                name: searchName,

                searchFunction: searchFunctionCallReferencingNames,
                functionParametersNames: ['param1'],
                args: {
                  parameterCount: 1,
                },
              },
            ]
          );
          assert.isArray(result[searchName]);
          assert.equal(result[searchName].length, 1);
          assert.equal(result[searchName][0].name, 'param1');
          assert.equal(result[searchName][0].functionName, 'simpleFunctionCallLiteral');
        });

        it('Chamada de função na condição do IF dentro de um ELSE: retorna a função chamada', async function () {
          const func: FunctionDefinition = getFunctionByName(contract, 'functionCallOnIFConditionalinsideELSE');
          //const calls = getFunctionCallNameReference(truffleProject, contract, func, 'param1', 1);

          const searchName = 'functionsCallsReferencingName';
          const result: TypeSearchResultList = searchThroughoutStatements(
            truffleProject,
            getFunctionGraph(truffleProject.getGraph(), contract, func),
            func,
            [
              {
                name: searchName,

                searchFunction: searchFunctionCallReferencingNames,
                functionParametersNames: ['param1'],
                args: {
                  parameterCount: 1,
                },
              },
            ]
          );
          assert.isArray(result[searchName]);
          assert.equal(result[searchName].length, 1);
          assert.equal(result[searchName][0].name, 'param1');
          assert.equal(result[searchName][0].functionName, 'simpleFunctionCallLiteral');
        });
      });
    });
  });
});
