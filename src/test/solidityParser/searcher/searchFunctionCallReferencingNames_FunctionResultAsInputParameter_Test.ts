import path from 'path';
import chai from 'chai';
import 'mocha';
import { getBranches, getFunctionCallNameReference } from '../../../services/solidityParser';
import { extractTruffleProjectGraph, getFunctionGraph } from '../../../services/TruffleProjectGraph';
import { TruffleProjectIceFactoryFunction } from '../../../services/TruffleFactory';
import { IContract } from '../../../models/IContract';
import {
  getTruffleProject,
  getContractByName,
  getBOFromRequireCall,
  getFunctionByName,
  getRequireCalls,
} from '../../utils';
import { ContractDefinition, FunctionDefinition } from 'solidity-parser-antlr';
import {
  searchFunctionCallReferencingNames,
  searchThroughoutStatements,
} from '../../../services/solidityParser/searcher';
import { TypeSearchResultList } from '../../../models';

const assert = chai.assert;

describe('Solidity Parser', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contracts: IContract[];
  let contract: ContractDefinition;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
    contracts = truffleProject.getContracts();
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
    contract = getContractByName(contracts, 'RequireUse');
  });

  describe('Get FunctionCallNameReference', function () {
    describe('Chamada de função como parâmetro de entrada de outra função', function () {
      describe('COM filtro da quantidade de parâmetros de entrada das funções', function () {
        it('Passagem de um FunctionCall que recebe o parâmetro solicitado como parâmetro de entrada de outra função: retorna a função chamada', async function () {
          const func: FunctionDefinition = getFunctionByName(contract, 'functionResultAsInputParameter');
          //const calls = getFunctionCallNameReference(truffleProject, contract, func, 'param1', 1);

          const searchName = 'functionsCallsReferencingName';
          const result: TypeSearchResultList = searchThroughoutStatements(
            truffleProject,
            getFunctionGraph(truffleProject.getGraph(), contract, func),
            func,
            [
              {
                name: searchName,

                searchFunction: searchFunctionCallReferencingNames,
                functionParametersNames: ['param1'],
                args: {
                  parameterCount: 1,
                },
              },
            ]
          );
          assert.isArray(result[searchName]);
          assert.equal(result[searchName].length, 1);
          assert.equal(result[searchName][0].name, 'param1');
          assert.equal(result[searchName][0].functionName, 'simpleFunctionCallLiteral');
        });
      });
    });
  });
});
