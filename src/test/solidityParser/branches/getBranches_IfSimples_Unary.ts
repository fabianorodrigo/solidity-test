import path from 'path';
import chai from 'chai';
import 'mocha';
import { getBranches } from '../../../services/solidityParser';
import { extractTruffleProjectGraph } from '../../../services/TruffleProjectGraph';
import { TruffleProjectIceFactoryFunction } from '../../../services/TruffleFactory';
import { IContract } from '../../../models/IContract';
import {
  getTruffleProject,
  getContractByName,
  getBOFromRequireCall,
  getFunctionByName,
  getRequireCalls,
} from '../../utils';

const assert = chai.assert;

describe('Solidity Parser', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contracts: IContract[];
  let contract: IContract;

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
    contracts = truffleProject.getContracts();
    contract = getContractByName(contracts, 'fullOfBranchesAndRestrictions');
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  describe('Get branches', function () {
    describe('If simples - Expressão Unária', function () {
      it('Quando fizer uma validação unária de um dos parametros de entrada booleano, deve retornar um branch que represente a entrada no IF e outro que não entre', async function () {
        const func = getFunctionByName(contracts[0].contractDefinition, 'simpleUnaryIf');
        const branches = getBranches(truffleProject, contracts[0].contractDefinition, func, func);
        assert.isArray(branches);
        assert.equal(branches.length, 2);
        //branch 1
        assert.isArray(branches[0].restrictions);
        assert.equal(branches[0].restrictions.length, 1);
        assert.equal(branches[0].restrictions[0].operator, '!=');
        assert.equal(branches[0].restrictions[0].right.value.js, 'true');
        assert.equal(branches[0].restrictions[0].right.value.sol, 'true');
        assert.equal(branches[0].restrictions[0].right.value.obj, true);
        assert.equal(branches[0].restrictions[0].left.value.js, 'param1');
        //branch 2
        assert.isArray(branches[1].restrictions);
        assert.equal(branches[1].restrictions.length, 1);
        assert.equal(branches[1].restrictions[0].operator, '==');
        assert.equal(branches[1].restrictions[0].right.value.js, 'true');
        assert.equal(branches[1].restrictions[0].right.value.sol, 'true');
        assert.equal(branches[1].restrictions[0].right.value.obj, true);
        assert.equal(branches[1].restrictions[0].left.value.js, 'param1');
      });

      it('Quando fizer uma validação de um Identificador de um dos parametros de entrada booleano, deve retornar um branch que represente a entrada no IF e outro que não entre', async function () {
        const func = getFunctionByName(contracts[0].contractDefinition, 'simpleIdentifierIf');
        const branches = getBranches(truffleProject, contracts[0].contractDefinition, func, func);
        assert.isArray(branches);
        assert.equal(branches.length, 2);
        //branch 1
        assert.isArray(branches[0].restrictions);
        assert.equal(branches[0].restrictions.length, 1);
        assert.equal(branches[0].restrictions[0].operator, '==');
        assert.equal(branches[0].restrictions[0].right.value.js, 'true');
        assert.equal(branches[0].restrictions[0].right.value.sol, 'true');
        assert.equal(branches[0].restrictions[0].right.value.obj, true);
        assert.equal(branches[0].restrictions[0].left.value.js, 'param1');
        //branch 2
        assert.isArray(branches[1].restrictions);
        assert.equal(branches[1].restrictions.length, 1);
        assert.equal(branches[1].restrictions[0].operator, '!=');
        assert.equal(branches[1].restrictions[0].right.value.js, 'true');
        assert.equal(branches[1].restrictions[0].right.value.sol, 'true');
        assert.equal(branches[1].restrictions[0].right.value.obj, true);
        assert.equal(branches[1].restrictions[0].left.value.js, 'param1');
      });
    });

    describe('If Sequencia - Expressão Unária', function () {
      it('Quando fizer uma validação unária de um dos parametros de entrada booleano no ELSEIF, deve retornar um branch do primeiro IF, um branch que represente a entrada no ELSEIF e outro que não entre em nenhum deles', async function () {
        const func = getFunctionByName(contracts[0].contractDefinition, 'UnaryElseIf');
        const branches = getBranches(truffleProject, contracts[0].contractDefinition, func, func);
        assert.isArray(branches);
        assert.equal(branches.length, 3);
        //branch 1
        assert.isArray(branches[0].restrictions);
        assert.equal(branches[0].restrictions.length, 1);
        assert.equal(branches[0].restrictions[0].operator, '==');
        assert.equal(branches[0].restrictions[0].right.value.js, '10');
        assert.equal(branches[0].restrictions[0].right.value.sol, '10');
        assert.equal(branches[0].restrictions[0].right.value.obj, 10);
        assert.equal(branches[0].restrictions[0].left.value.js, 'param');
        //branch 2
        assert.isArray(branches[1].restrictions);
        assert.equal(branches[1].restrictions.length, 1);
        assert.equal(branches[1].restrictions[0].operator, '!=');
        assert.equal(branches[1].restrictions[0].right.value.js, 'true');
        assert.equal(branches[1].restrictions[0].right.value.sol, 'true');
        assert.equal(branches[1].restrictions[0].right.value.obj, true);
        assert.equal(branches[1].restrictions[0].left.value.js, 'param1');
        //branch 3
        assert.isArray(branches[2].restrictions);
        assert.equal(branches[2].restrictions.length, 2);
        assert.equal(branches[2].restrictions[0].operator, '==');
        assert.equal(branches[2].restrictions[0].right.value.js, 'true');
        assert.equal(branches[2].restrictions[0].right.value.sol, 'true');
        assert.equal(branches[2].restrictions[0].right.value.obj, true);
        assert.equal(branches[2].restrictions[0].left.value.js, 'param1');
        assert.equal(branches[2].restrictions[1].operator, '!=');
        assert.equal(branches[2].restrictions[1].right.value.js, '10');
        assert.equal(branches[2].restrictions[1].right.value.sol, '10');
        assert.equal(branches[2].restrictions[1].right.value.obj, 10);
        assert.equal(branches[2].restrictions[1].left.value.js, 'param');
      });

      it('Quando fizer uma validação de um Identificador de um dos parametros de entrada booleano no ELSEIF, deve retornar um branch do primeiro IF, um branch que represente a entrada no IF e outro que não entre', async function () {
        const func = getFunctionByName(contracts[0].contractDefinition, 'IdentifierElseIf');
        const branches = getBranches(truffleProject, contracts[0].contractDefinition, func, func);
        assert.isArray(branches);
        assert.equal(branches.length, 3);
        //branch 1
        assert.isArray(branches[0].restrictions);
        assert.equal(branches[0].restrictions.length, 1);
        assert.equal(branches[0].restrictions[0].operator, '==');
        assert.equal(branches[0].restrictions[0].right.value.js, '10');
        assert.equal(branches[0].restrictions[0].right.value.sol, '10');
        assert.equal(branches[0].restrictions[0].right.value.obj, 10);
        assert.equal(branches[0].restrictions[0].left.value.js, 'param');
        //branch 2
        assert.isArray(branches[1].restrictions);
        assert.equal(branches[1].restrictions.length, 1);
        assert.equal(branches[1].restrictions[0].operator, '==');
        assert.equal(branches[1].restrictions[0].right.value.js, 'true');
        assert.equal(branches[1].restrictions[0].right.value.sol, 'true');
        assert.equal(branches[1].restrictions[0].right.value.obj, true);
        assert.equal(branches[1].restrictions[0].left.value.js, 'param1');
        //branch 3
        assert.isArray(branches[2].restrictions);
        assert.equal(branches[2].restrictions.length, 2);
        assert.equal(branches[2].restrictions[0].operator, '!=');
        assert.equal(branches[2].restrictions[0].right.value.js, 'true');
        assert.equal(branches[2].restrictions[0].right.value.sol, 'true');
        assert.equal(branches[2].restrictions[0].right.value.obj, true);
        assert.equal(branches[2].restrictions[0].left.value.js, 'param1');
        assert.equal(branches[2].restrictions[1].operator, '!=');
        assert.equal(branches[2].restrictions[1].right.value.js, '10');
        assert.equal(branches[2].restrictions[1].right.value.sol, '10');
        assert.equal(branches[2].restrictions[1].right.value.obj, 10);
        assert.equal(branches[2].restrictions[1].left.value.js, 'param');
      });
    });
  });
});
