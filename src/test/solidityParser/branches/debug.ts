import path from 'path';
import chai from 'chai';
import 'mocha';
import { TruffleProjectIceFactoryFunction } from '../../../services/TruffleFactory';
import { Types } from '../../../services/types';
import {
  ContractDefinition,
  FunctionDefinition,
  VariableDeclaration,
  ElementaryTypeName,
  UserDefinedTypeName,
} from 'solidity-parser-antlr';
import { getTruffleProject, getContractByName, getFunctionByName, getParametersValuesForTest } from '../../utils';
import { extractTruffleProjectGraph } from '../../../services/TruffleProjectGraph';
import { RestrictionElementType } from '../../../models/RestrictionElementType';
import { IValue } from '../../../models/IValue';
import {
  r_gre_number7,
  r_leq_number17,
  r_Z_les_P,
  r_A_geq_Z,
  r_Z_gre_P,
  r_msgSender_neq_0x0,
  r_msgValue_gre_0,
  r_msgValue_leq_paramMax,
  r_leq_number17_fordebug,
} from '../../restrictionsConstants';
import { IRestriction } from '../../../models/IResctriction';
import { getRandomParameterTypeValue } from '../../../services/Utils/getRandomParameterValue';
import { ITypeValue } from '../../../models/scenario/ITypeValue';
import { IScenario } from '../../../models/scenario/IScenario';
import { getFunctionParametersValuesScenarios } from '../../../services/scenarios';
import { getBranches } from '../../../services/solidityParser';

const assert = chai.assert;

describe('RestrictionElementType.functionParameter / RestrictionElementType.TransactionParameter', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contract: ContractDefinition;
  const references = {} as { [name: string]: IValue };

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
    contract = getContractByName(truffleProject.getContracts(), 'fullOfBranchesAndRestrictions');
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });
  describe('DEBUG', function () {
    it('Deve retornar um branch para cada IF e ELSEIF em sequência e outra para seus respectivos Elses com a inversão das restrições dos IFs e ELSEIFs anteriores acumuladas REMOVENDO REDUNDÂNCIAS DE RESTRIÇÕES', async function () {
      const func = getFunctionByName(contract, 'ifSequenceElseRedundantRestrictions');
      const branches = getBranches(truffleProject, contract, func, func);
      assert.isArray(branches);
      assert.equal(branches.length, 4);
      //if/elseifs
      assert.isArray(branches[0].restrictions);
      assert.equal(branches[0].restrictions.length, 1);
      assert.equal(branches[0].restrictions[0].operator, '<=');
      assert.equal(branches[0].restrictions[0].right.value.js, '23');
      assert.equal(branches[0].restrictions[0].left.value.js, '_value');
      assert.isArray(branches[1].restrictions);
      assert.equal(branches[1].restrictions.length, 1);
      assert.equal(branches[1].restrictions[0].operator, '<=');
      assert.equal(branches[1].restrictions[0].right.value.js, '0xFF');
      assert.equal(branches[1].restrictions[0].left.value.js, '_value');
      assert.isArray(branches[2].restrictions);
      assert.equal(branches[2].restrictions.length, 1);
      assert.equal(branches[2].restrictions[0].operator, '<=');
      assert.equal(branches[2].restrictions[0].right.value.js, '0xFFFFFFFFFFFFFFFF');
      assert.equal(branches[2].restrictions[0].left.value.js, '_value');
      //else
      assert.isArray(branches[3].restrictions);
      assert.equal(branches[3].restrictions.length, 1);
      assert.equal(branches[3].restrictions[0].operator, '>');
      assert.equal(branches[3].restrictions[0].right.value.js, '0xFFFFFFFFFFFFFFFF');
      assert.equal(branches[3].restrictions[0].left.value.js, '_value');
    });
  });
});
