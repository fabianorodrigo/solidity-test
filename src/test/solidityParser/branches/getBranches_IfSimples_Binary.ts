import path from 'path';
import chai from 'chai';
import 'mocha';
import { getBranches } from '../../../services/solidityParser';
import { extractTruffleProjectGraph } from '../../../services/TruffleProjectGraph';
import { TruffleProjectIceFactoryFunction } from '../../../services/TruffleFactory';
import { IContract } from '../../../models/IContract';
import {
  getTruffleProject,
  getContractByName,
  getBOFromRequireCall,
  getFunctionByName,
  getRequireCalls,
} from '../../utils';
import { RestrictionElementType } from '../../../models/RestrictionElementType';

const assert = chai.assert;

describe('Solidity Parser', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contracts: IContract[];

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
    contracts = truffleProject.getContracts();
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  describe('Get branches', function () {
    describe('If simples - BinaryOperation', function () {
      it('Quando referenciar um dos parametros de entrada contra um literal, deve retornar um branch que represente a entrada no IF e outro que não entre', async function () {
        const func = getFunctionByName(contracts[0].contractDefinition, 'simpleIf');
        const branches = getBranches(truffleProject, contracts[0].contractDefinition, func, func);
        assert.isArray(branches);
        assert.equal(branches.length, 2);
        //branch 1
        assert.isArray(branches[0].restrictions);
        assert.equal(branches[0].restrictions.length, 1);
        assert.equal(branches[0].restrictions[0].operator, '>');
        assert.equal(branches[0].restrictions[0].right.value.js, '10');
        assert.equal(branches[0].restrictions[0].right.value.sol, '10');
        assert.equal(branches[0].restrictions[0].left.value.js, 'param1');
        //branch 2
        assert.isArray(branches[1].restrictions);
        assert.equal(branches[1].restrictions.length, 1);
        assert.equal(branches[1].restrictions[0].operator, '<=');
        assert.equal(branches[1].restrictions[0].right.value.js, '10');
        assert.equal(branches[1].restrictions[0].right.value.sol, '10');
        assert.equal(branches[1].restrictions[0].left.value.js, 'param1');
      });
      it('Quando referenciar um dos parametros de entrada contra uma variável não identificada, retorna 2 branches com unhandled e, no valor, o tipo do elemento "Identifier"', async function () {
        const func = getFunctionByName(contracts[0].contractDefinition, 'simpleIfCompareWithStateVar');
        const branches = getBranches(truffleProject, contracts[0].contractDefinition, func, func);
        assert.isArray(branches);
        assert.equal(branches.length, 2);
        //branch que entraria no IF
        assert.equal(branches[0].restrictions.length, 1);
        assert.equal(branches[0].restrictions[0].operator, '>');
        assert.equal(branches[0].restrictions[0].right.elementType, RestrictionElementType.unhandled);
        assert.equal(branches[0].restrictions[0].right.value.js, 'Identifier');
        assert.equal(branches[0].restrictions[0].right.value.sol, 'Identifier');
        assert.equal(branches[0].restrictions[0].right.value.obj, 'Identifier');
        //branch que NÃO entraria no IF
        assert.equal(branches[1].restrictions.length, 1);
        assert.equal(branches[1].restrictions[0].operator, '<=');
        assert.equal(branches[1].restrictions[0].right.elementType, RestrictionElementType.unhandled);
        assert.equal(branches[1].restrictions[0].right.value.js, 'Identifier');
        assert.equal(branches[1].restrictions[0].right.value.sol, 'Identifier');
        assert.equal(branches[1].restrictions[0].right.value.obj, 'Identifier');
        assert.equal(branches[1].restrictions[0].left.value.js, 'param1');
        assert.equal(branches[1].restrictions[0].left.value.js, 'param1');
      });
      it('Quando referenciar um dos parametros de entrada e um AND com chamada de função, deve retornar apenas o branch deste IF e o branch do ELSE', async function () {
        const func = getFunctionByName(contracts[0].contractDefinition, 'simpleIfAndFunctionCall');
        const branches = getBranches(truffleProject, contracts[0].contractDefinition, func, func);
        assert.isArray(branches);
        assert.equal(branches.length, 2);
        //if
        assert.isArray(branches[0].restrictions);
        assert.equal(branches[0].restrictions.length, 1);
        assert.equal(branches[0].restrictions[0].operator, '>');
        assert.equal(branches[0].restrictions[0].right.value.js, '10');
        assert.equal(branches[0].restrictions[0].right.value.sol, '10');
        assert.equal(branches[0].restrictions[0].left.value.js, 'param1');
        //else
        assert.isArray(branches[1].restrictions);
        assert.equal(branches[1].restrictions.length, 1);
        assert.equal(branches[1].restrictions[0].operator, '<=');
        assert.equal(branches[1].restrictions[0].right.value.js, '10');
        assert.equal(branches[1].restrictions[0].right.value.sol, '10');
        assert.equal(branches[1].restrictions[0].left.value.js, 'param1');
      });
      it('Quando referenciar dois parametros de entrada com AND, deve retornar um branch com DUAS restrições e um branch do else', async function () {
        const func = getFunctionByName(contracts[0].contractDefinition, 'simpleIfReferencingTwoParametersAND');
        const branches = getBranches(truffleProject, contracts[0].contractDefinition, func, func);
        assert.isArray(branches);
        assert.equal(branches.length, 2);
        //branch if
        assert.isArray(branches[0].restrictions);
        assert.equal(branches[0].restrictions.length, 2);
        assert.equal(branches[0].restrictions[0].operator, '>');
        assert.equal(branches[0].restrictions[0].right.value.js, '10');
        assert.equal(branches[0].restrictions[0].right.value.sol, '10');
        assert.equal(branches[0].restrictions[0].left.value.js, 'param1');
        assert.equal(branches[0].restrictions[1].operator, '<');
        assert.equal(branches[0].restrictions[1].right.value.js, '1');
        assert.equal(branches[0].restrictions[1].left.value.js, 'param2');
        //branch else
        assert.isArray(branches[1].restrictions);
        //O ELSE passou a retornar apenas uma restrição pra cada branch anterior (if, elseif)
        //Como a restrição é escolhida de forma randômica, não é possível determinar qual retorna
        assert.equal(branches[1].restrictions.length, 1);
      });
      it('Quando referenciar dois parametros de entrada com OR, deve retornar DOIS branches com UMA restrição cada + UM branche do ELSE', async function () {
        const func = getFunctionByName(contracts[0].contractDefinition, 'simpleIfReferencingTwoParametersOR');
        const branches = getBranches(truffleProject, contracts[0].contractDefinition, func, func);
        assert.isArray(branches);
        assert.equal(branches.length, 3);
        //branch 1
        assert.isArray(branches[0].restrictions);
        assert.equal(branches[0].restrictions.length, 1);
        assert.equal(branches[0].restrictions[0].operator, '>');
        assert.equal(branches[0].restrictions[0].right.value.js, '10');
        assert.equal(branches[0].restrictions[0].left.value.js, 'param1');
        //branch 2
        assert.isArray(branches[1].restrictions);
        assert.equal(branches[1].restrictions.length, 1);
        assert.equal(branches[1].restrictions[0].operator, '<');
        assert.equal(branches[1].restrictions[0].right.value.js, '1');
        assert.equal(branches[1].restrictions[0].left.value.js, 'param2');
        //branch 3
        assert.isArray(branches[2].restrictions);
        assert.equal(branches[2].restrictions.length, 2);
        assert.equal(branches[2].restrictions[0].operator, '<=');
        assert.equal(branches[2].restrictions[0].right.value.js, '10');
        assert.equal(branches[2].restrictions[0].left.value.js, 'param1');
        assert.equal(branches[2].restrictions[1].operator, '>=');
        assert.equal(branches[2].restrictions[1].right.value.js, '1');
        assert.equal(branches[2].restrictions[1].left.value.js, 'param2');
      });

      it('Quando referenciar dois parametros de entrada e uma variável não identificada com AND, deve retornar um branch com TRÊS restrições, duas que referenciam os parâmetros de entrada e uma com unhandled, e outro branch com o ELSE', async function () {
        const func = getFunctionByName(
          contracts[0].contractDefinition,
          'simpleIfReferencingTwoParametersOR_ANDstateVariable'
        );
        const branches = getBranches(truffleProject, contracts[0].contractDefinition, func, func);
        assert.isArray(branches);
        assert.equal(branches.length, 2);
        //branch if
        assert.isArray(branches[0].restrictions);
        assert.equal(branches[0].restrictions.length, 3);
        assert.equal(branches[0].restrictions[0].operator, '>');
        assert.equal(branches[0].restrictions[0].right.value.js, '10');
        assert.equal(branches[0].restrictions[0].right.value.sol, '10');
        assert.equal(branches[0].restrictions[0].left.value.js, 'param1');
        assert.equal(branches[0].restrictions[1].operator, '<');
        assert.equal(branches[0].restrictions[1].right.value.js, '1');
        assert.equal(branches[0].restrictions[1].right.value.sol, '1');
        assert.equal(branches[0].restrictions[1].left.value.js, 'param2');
        //restrição unhandled
        assert.equal(branches[0].restrictions[2].operator, '>');
        assert.equal(branches[0].restrictions[2].left.elementType, RestrictionElementType.unhandled);
        assert.equal(branches[0].restrictions[2].left.value.js, 'Identifier');
        assert.equal(branches[0].restrictions[2].left.value.sol, 'Identifier');
        assert.equal(branches[0].restrictions[2].left.value.obj, 'Identifier');
        assert.equal(branches[0].restrictions[2].right.value.js, '1000');
        assert.equal(branches[0].restrictions[2].right.value.js, '1000');
        //branch else
        //O ELSE retorna apenas uma restrição pra cada branch anterior (if, elseif)
        //como a restrição do IF é AND, basta que uma seja negada. Como a restrição é escolhida
        //de forma randômica, não é possível determinar qual retorna
        assert.isArray(branches[1].restrictions);
        assert.equal(branches[1].restrictions.length, 1);
      });

      it('Quando referenciar um parametro de entrada em que há um AND com um range (o que leva o ELSE a exigir uma condição impossível) , deve ...', async function () {
        const func = getFunctionByName(getContractByName(contracts, 'fullOfBranchesAndRestrictions'), 'isValidDate');
        const branches = getBranches(truffleProject, contracts[0].contractDefinition, func, func);
        assert.isArray(branches);
        assert.equal(branches.length, 3);
        //branch 1
        assert.isArray(branches[0].restrictions);
        assert.equal(branches[0].restrictions.length, 5);
        assert.equal(branches[0].restrictions[0].operator, '>');
        assert.equal(branches[0].restrictions[0].right.value.js, '0');
        assert.equal(branches[0].restrictions[0].left.value.js, 'day');
        assert.equal(branches[0].restrictions[1].operator, '<=');
        assert.equal(branches[0].restrictions[1].right.elementType, RestrictionElementType.localVariable);
        assert.equal(branches[0].restrictions[1].right.value.js, 'daysInMonth');
        assert.equal(branches[0].restrictions[1].left.value.js, 'day');
        assert.equal(branches[0].restrictions[2].operator, '>=');
        assert.equal(branches[0].restrictions[2].right.value.js, '1970');
        assert.equal(branches[0].restrictions[2].left.value.js, 'year');
        assert.equal(branches[0].restrictions[3].operator, '>');
        assert.equal(branches[0].restrictions[3].right.value.js, '0');
        assert.equal(branches[0].restrictions[3].left.value.js, 'month');
        assert.equal(branches[0].restrictions[4].operator, '<=');
        assert.equal(branches[0].restrictions[4].right.value.js, '12');
        assert.equal(branches[0].restrictions[4].left.value.js, 'month');
        //branch 2 será entrar no IF e negar ou "day > 0" ou "day <= daysInMonth"
        assert.isArray(branches[1].restrictions);
        assert.equal(branches[1].restrictions.length, 4);
        assert.equal(branches[1].restrictions[0].left.value.js, 'day');
        assert.isTrue(
          (branches[1].restrictions[0].operator == '<=' && branches[1].restrictions[0].right.value.js == '0') ||
            (branches[1].restrictions[0].operator == '>' && branches[1].restrictions[0].right.value.js == 'daysInMonth')
        );

        assert.equal(branches[1].restrictions[1].operator, '>=');
        assert.equal(branches[1].restrictions[1].right.value.js, '1970');
        assert.equal(branches[1].restrictions[1].left.value.js, 'year');
        assert.equal(branches[1].restrictions[2].operator, '>');
        assert.equal(branches[1].restrictions[2].right.value.js, '0');
        assert.equal(branches[1].restrictions[2].left.value.js, 'month');
        assert.equal(branches[1].restrictions[3].operator, '<=');
        assert.equal(branches[1].restrictions[3].right.value.js, '12');
        assert.equal(branches[1].restrictions[3].left.value.js, 'month');
        //branch 3
        assert.isArray(branches[2].restrictions);
        //TODO: Analisar: Tem alguma situação não determinística na definição das restrições do ELSE
        /*assert.equal(branches[2].restrictions.length, 2);
        assert.equal(branches[2].restrictions[0].operator, '<');
        assert.equal(branches[2].restrictions[0].right.value.js, '1970');
        assert.equal(branches[2].restrictions[0].left.value.js, 'year');
        assert.equal(branches[2].restrictions[1].operator, '>');
        assert.equal(branches[2].restrictions[1].right.value.js, '12');
        assert.equal(branches[2].restrictions[1].left.value.js, 'month');*/
      });
    });
  });
});
