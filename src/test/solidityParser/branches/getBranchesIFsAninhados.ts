import path from 'path';
import chai from 'chai';
import 'mocha';
import { getBranches } from '../../../services/solidityParser';
import { extractTruffleProjectGraph } from '../../../services/TruffleProjectGraph';
import { TruffleProjectIceFactoryFunction } from '../../../services/TruffleFactory';
import { IContract } from '../../../models/IContract';
import {
  getTruffleProject,
  getContractByName,
  getBOFromRequireCall,
  getFunctionByName,
  getRequireCalls,
} from '../../utils';

const assert = chai.assert;

describe('Solidity Parser', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contracts: IContract[];

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
    contracts = truffleProject.getContracts();
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  describe('Get branches', function () {
    describe('Ifs aninhados (if{if, elseif, else}, elseif{if, else}, else)', function () {
      it('if(a==100){ if(b>100)elseif(b>90) } elseif (a==90){}: Deve retornar 3 branches pra o IF externo, 1 branch pro ELSEIF e 1 branch pro ELSE', async function () {
        const func = getFunctionByName(contracts[0].contractDefinition, 'nestedIF_ifelse_ELSEIF');
        const branches = getBranches(truffleProject, contracts[0].contractDefinition, func, func);
        assert.isArray(branches);
        assert.equal(branches.length, 5);
        //IF externo com IF interno
        assert.isArray(branches[0].restrictions);
        assert.equal(branches[0].restrictions.length, 2);
        assert.equal(branches[0].restrictions[0].operator, '>');
        assert.equal(branches[0].restrictions[0].right.value.js, '100');
        assert.equal(branches[0].restrictions[0].left.value.js, 'param2');
        assert.equal(branches[0].restrictions[1].operator, '==');
        assert.equal(branches[0].restrictions[1].right.value.js, '100');
        assert.equal(branches[0].restrictions[1].left.value.js, 'param1');
        //IF externo com ELSEIF interno
        assert.equal(branches[1].restrictions.length, 2);
        assert.equal(branches[1].restrictions[0].operator, '>');
        assert.equal(branches[1].restrictions[0].right.value.js, '90');
        assert.equal(branches[1].restrictions[0].left.value.js, 'param2');
        assert.equal(branches[1].restrictions[1].operator, '==');
        assert.equal(branches[1].restrictions[1].right.value.js, '100');
        assert.equal(branches[1].restrictions[1].left.value.js, 'param1');
        //IF externo com o ELSE interno
        assert.isArray(branches[2].restrictions);
        assert.equal(branches[2].restrictions.length, 2);
        assert.equal(branches[2].restrictions[0].operator, '<=');
        assert.equal(branches[2].restrictions[0].right.value.js, '90');
        assert.equal(branches[2].restrictions[0].left.value.js, 'param2');
        assert.equal(branches[2].restrictions[1].operator, '==');
        assert.equal(branches[2].restrictions[1].right.value.js, '100');
        assert.equal(branches[2].restrictions[1].left.value.js, 'param1');
        //ELSEIF externo
        assert.isArray(branches[3].restrictions);
        assert.equal(branches[3].restrictions.length, 1);
        assert.equal(branches[3].restrictions[0].operator, '==');
        assert.equal(branches[3].restrictions[0].right.value.js, '90');
        assert.equal(branches[3].restrictions[0].left.value.js, 'param1');
        //ELSE externo
        assert.isArray(branches[4].restrictions);
        assert.equal(branches[4].restrictions.length, 2);
        assert.equal(branches[4].restrictions[0].operator, '!=');
        assert.equal(branches[4].restrictions[0].right.value.js, '90');
        assert.equal(branches[4].restrictions[0].left.value.js, 'param1');
        assert.equal(branches[4].restrictions[1].operator, '!=');
        assert.equal(branches[4].restrictions[1].right.value.js, '100');
        assert.equal(branches[4].restrictions[1].left.value.js, 'param1');
      });

      it('if(a==100){} elseif (a==90){ if(b>100)elseif(b>90) }: Deve retornar 1 branch pra o IF externo, 3 branches pro ELSEIF e 1 branch pro ELSE', async function () {
        const func = getFunctionByName(contracts[0].contractDefinition, 'nestedIF_ELSEIF_ifelse');
        const branches = getBranches(truffleProject, contracts[0].contractDefinition, func, func);
        assert.isArray(branches);
        assert.equal(branches.length, 5);
        //IF externo
        assert.isArray(branches[0].restrictions);
        assert.equal(branches[0].restrictions.length, 1);
        assert.equal(branches[0].restrictions[0].operator, '==');
        assert.equal(branches[0].restrictions[0].right.value.js, '100');
        assert.equal(branches[0].restrictions[0].left.value.js, 'param1');
        //ELSEIF externo com IF interno
        assert.equal(branches[1].restrictions.length, 2);
        assert.equal(branches[1].restrictions[0].operator, '>');
        assert.equal(branches[1].restrictions[0].right.value.js, '100');
        assert.equal(branches[1].restrictions[0].left.value.js, 'param2');
        assert.equal(branches[1].restrictions[1].operator, '==');
        assert.equal(branches[1].restrictions[1].right.value.js, '90');
        assert.equal(branches[1].restrictions[1].left.value.js, 'param1');
        //ELSEIF externo com o ELSEIF interno
        assert.isArray(branches[2].restrictions);
        assert.equal(branches[2].restrictions.length, 2);
        assert.equal(branches[2].restrictions[0].operator, '>');
        assert.equal(branches[2].restrictions[0].right.value.js, '90');
        assert.equal(branches[2].restrictions[0].left.value.js, 'param2');
        assert.equal(branches[2].restrictions[1].operator, '==');
        assert.equal(branches[2].restrictions[1].right.value.js, '90');
        assert.equal(branches[2].restrictions[1].left.value.js, 'param1');
        //ELSEIF externo com o ELSE interno
        assert.isArray(branches[3].restrictions);
        assert.equal(branches[3].restrictions.length, 2);
        assert.equal(branches[3].restrictions[0].operator, '<=');
        assert.equal(branches[3].restrictions[0].right.value.js, '90');
        assert.equal(branches[3].restrictions[0].left.value.js, 'param2');
        assert.equal(branches[3].restrictions[1].operator, '==');
        assert.equal(branches[3].restrictions[1].right.value.js, '90');
        assert.equal(branches[3].restrictions[1].left.value.js, 'param1');
        //ELSE externo
        assert.isArray(branches[4].restrictions);
        assert.equal(branches[4].restrictions.length, 2);
        assert.equal(branches[4].restrictions[0].operator, '!=');
        assert.equal(branches[4].restrictions[0].right.value.js, '90');
        assert.equal(branches[4].restrictions[0].left.value.js, 'param1');
        assert.equal(branches[4].restrictions[1].operator, '!=');
        assert.equal(branches[4].restrictions[1].right.value.js, '100');
        assert.equal(branches[4].restrictions[1].left.value.js, 'param1');
      });

      it('if(a==100){} elseif (a==90){} else{  if(b>100)elseif(b>90) }: Deve retornar 1 branch pra o IF externo, 1 branch pro ELSEIF e 3 branches pro ELSE', async function () {
        const func = getFunctionByName(contracts[0].contractDefinition, 'nestedIF_ELSEIF_ELSE_ifelse');
        const branches = getBranches(truffleProject, contracts[0].contractDefinition, func, func);
        assert.isArray(branches);
        assert.equal(branches.length, 5);
        //IF externo
        assert.isArray(branches[0].restrictions);
        assert.equal(branches[0].restrictions.length, 1);
        assert.equal(branches[0].restrictions[0].operator, '==');
        assert.equal(branches[0].restrictions[0].right.value.js, '100');
        assert.equal(branches[0].restrictions[0].left.value.js, 'param1');
        //ELSE IF externo
        assert.isArray(branches[1].restrictions);
        assert.equal(branches[1].restrictions.length, 1);
        assert.equal(branches[1].restrictions[0].operator, '==');
        assert.equal(branches[1].restrictions[0].right.value.js, '90');
        assert.equal(branches[1].restrictions[0].left.value.js, 'param1');
        //ELSE externo com IF interno
        assert.isArray(branches[2].restrictions);
        assert.equal(branches[2].restrictions.length, 3);
        assert.equal(branches[2].restrictions[0].operator, '>');
        assert.equal(branches[2].restrictions[0].right.value.js, '100');
        assert.equal(branches[2].restrictions[0].left.value.js, 'param2');
        assert.equal(branches[2].restrictions[1].operator, '!=');
        assert.equal(branches[2].restrictions[1].right.value.js, '90');
        assert.equal(branches[2].restrictions[1].left.value.js, 'param1');
        assert.equal(branches[2].restrictions[2].operator, '!=');
        assert.equal(branches[2].restrictions[2].right.value.js, '100');
        assert.equal(branches[2].restrictions[2].left.value.js, 'param1');
        //ELSE externo com o ELSEIF interno
        assert.isArray(branches[3].restrictions);
        assert.equal(branches[3].restrictions.length, 3);
        assert.equal(branches[3].restrictions[0].operator, '>');
        assert.equal(branches[3].restrictions[0].right.value.js, '90');
        assert.equal(branches[3].restrictions[0].left.value.js, 'param2');
        assert.equal(branches[3].restrictions[1].operator, '!=');
        assert.equal(branches[3].restrictions[1].right.value.js, '90');
        assert.equal(branches[3].restrictions[1].left.value.js, 'param1');
        assert.equal(branches[3].restrictions[2].operator, '!=');
        assert.equal(branches[3].restrictions[2].right.value.js, '100');
        assert.equal(branches[3].restrictions[2].left.value.js, 'param1');
        //ELSE externo com else interno
        assert.isArray(branches[4].restrictions);
        assert.equal(branches[4].restrictions.length, 3);
        assert.equal(branches[4].restrictions[0].operator, '<=');
        assert.equal(branches[4].restrictions[0].right.value.js, '90');
        assert.equal(branches[4].restrictions[0].left.value.js, 'param2');
        assert.equal(branches[4].restrictions[1].operator, '!=');
        assert.equal(branches[4].restrictions[1].right.value.js, '90');
        assert.equal(branches[4].restrictions[1].left.value.js, 'param1');
        assert.equal(branches[4].restrictions[2].operator, '!=');
        assert.equal(branches[4].restrictions[2].right.value.js, '100');
        assert.equal(branches[4].restrictions[2].left.value.js, 'param1');
      });
    });
  });
});
