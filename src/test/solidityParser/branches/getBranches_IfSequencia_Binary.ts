import path from 'path';
import chai from 'chai';
import 'mocha';
import { getBranches } from '../../../services/solidityParser';
import { extractTruffleProjectGraph } from '../../../services/TruffleProjectGraph';
import { TruffleProjectIceFactoryFunction } from '../../../services/TruffleFactory';
import { IContract } from '../../../models/IContract';
import {
  getTruffleProject,
  getContractByName,
  getBOFromRequireCall,
  getFunctionByName,
  getRequireCalls,
} from '../../utils';

const assert = chai.assert;

describe('Solidity Parser', function () {
  let truffleProject: TruffleProjectIceFactoryFunction;
  let contracts: IContract[];

  before(() => {
    truffleProject = getTruffleProject(path.resolve(__dirname, '../../'));
    contracts = truffleProject.getContracts();
    //apurando e setando o grafo do projeto
    truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
  });

  describe('Get branches', function () {
    describe('Ifs em sequência em um nível (if, elseif, else)', function () {
      it('Para cada OR dentro do IF, cada elseif e o else, deve retornar um branch separado', async function () {
        const func = getFunctionByName(contracts[0].contractDefinition, '_getDaysInMonth');
        const branches = getBranches(truffleProject, contracts[0].contractDefinition, func, func);
        assert.isArray(branches);
        assert.equal(branches.length, 9);
        //branch 1
        assert.isArray(branches[0].restrictions);
        assert.equal(branches[0].restrictions.length, 1);
        assert.equal(branches[0].restrictions[0].operator, '==');
        assert.equal(branches[0].restrictions[0].right.value.js, '1');
        assert.equal(branches[0].restrictions[0].left.value.js, 'month');
        //branch 2
        assert.isArray(branches[1].restrictions);
        assert.equal(branches[1].restrictions.length, 1);
        assert.equal(branches[1].restrictions[0].operator, '==');
        assert.equal(branches[1].restrictions[0].right.value.js, '3');
        assert.equal(branches[1].restrictions[0].left.value.js, 'month');
        //branch 3
        assert.isArray(branches[2].restrictions);
        assert.equal(branches[2].restrictions.length, 1);
        assert.equal(branches[2].restrictions[0].operator, '==');
        assert.equal(branches[2].restrictions[0].right.value.js, '5');
        assert.equal(branches[2].restrictions[0].left.value.js, 'month');
        //branch 4
        assert.isArray(branches[3].restrictions);
        assert.equal(branches[3].restrictions.length, 1);
        assert.equal(branches[3].restrictions[0].operator, '==');
        assert.equal(branches[3].restrictions[0].right.value.js, '7');
        assert.equal(branches[3].restrictions[0].left.value.js, 'month');
        //branch 5
        assert.isArray(branches[4].restrictions);
        assert.equal(branches[4].restrictions.length, 1);
        assert.equal(branches[4].restrictions[0].operator, '==');
        assert.equal(branches[4].restrictions[0].right.value.js, '8');
        assert.equal(branches[4].restrictions[0].left.value.js, 'month');
        //branch 6
        assert.isArray(branches[5].restrictions);
        assert.equal(branches[5].restrictions.length, 1);
        assert.equal(branches[5].restrictions[0].operator, '==');
        assert.equal(branches[5].restrictions[0].right.value.js, '10');
        assert.equal(branches[5].restrictions[0].left.value.js, 'month');
        //branch 7
        assert.isArray(branches[6].restrictions);
        assert.equal(branches[6].restrictions.length, 1);
        assert.equal(branches[6].restrictions[0].operator, '==');
        assert.equal(branches[6].restrictions[0].right.value.js, '12');
        assert.equal(branches[6].restrictions[0].left.value.js, 'month');
        //branch 8
        assert.isArray(branches[7].restrictions);
        assert.equal(branches[7].restrictions.length, 1);
        assert.equal(branches[7].restrictions[0].operator, '!=');
        assert.equal(branches[7].restrictions[0].right.value.js, '2');
        assert.equal(branches[7].restrictions[0].left.value.js, 'month');
        //branch 9
        assert.isArray(branches[8].restrictions);
        //O else possui uma restrição inversa para cada if/elseif anterior.
        //A FUNÇÃO QUE LIMPA FOI COMENTADA: Contudo, se houver um operador '==', remove os outros
        assert.equal(branches[8].restrictions.length, 8, JSON.stringify(branches[8].restrictions));
        assert.equal(branches[8].restrictions[0].operator, '==');
        assert.equal(branches[8].restrictions[0].right.value.js, '2');
        assert.equal(branches[8].restrictions[0].left.value.js, 'month');
      });

      it('Deve retornar um branch para IF que compara parãmetro de entrada com atributo do parametro de transação (msg.sender)', async function () {
        const func = getFunctionByName(contracts[0].contractDefinition, 'ifWithConditionBasedOnTransactionParameter');
        const branches = getBranches(truffleProject, contracts[0].contractDefinition, func, func);
        assert.isArray(branches);
        assert.equal(branches.length, 2);
        //branch 1
        assert.isArray(branches[0].restrictions);
        assert.equal(branches[0].restrictions.length, 1);
        assert.equal(branches[0].restrictions[0].operator, '==');
        assert.equal(branches[0].restrictions[0].right.value.js, '15');
        assert.equal(branches[0].restrictions[0].left.value.js, 'value');
        //branch 1
        assert.isArray(branches[1].restrictions);
        assert.equal(branches[1].restrictions.length, 1);
        assert.equal(branches[1].restrictions[0].operator, '!=');
        assert.equal(branches[1].restrictions[0].right.value.js, '15');
        assert.equal(branches[1].restrictions[0].left.value.js, 'value');
      });

      it('Deve retornar um branch para IF que compara atributo de parãmetro de entrada struct e um branch para o else', async function () {
        const func = getFunctionByName(contracts[0].contractDefinition, 'ifWithConditionBasedOnStructMember');
        const branches = getBranches(truffleProject, contracts[0].contractDefinition, func, func);
        assert.isArray(branches);
        assert.equal(branches.length, 2);
        //branch 1
        assert.isArray(branches[0].restrictions);
        assert.equal(branches[0].restrictions.length, 1);
        assert.equal(branches[0].restrictions[0].operator, '>=');
        assert.equal(branches[0].restrictions[0].right.value.js, '10');
        assert.equal(branches[0].restrictions[0].left.value.js, 'x');
        assert.equal(branches[0].restrictions[0].left.value.memberName, 'a');
        //branch 1
        assert.isArray(branches[1].restrictions);
        assert.equal(branches[1].restrictions.length, 1);
        assert.equal(branches[1].restrictions[0].operator, '<');
        assert.equal(branches[1].restrictions[0].right.value.js, '10');
        assert.equal(branches[1].restrictions[0].left.value.js, 'x');
        assert.equal(branches[1].restrictions[0].left.value.memberName, 'a');
      });

      it('Deve retornar um branch para IF que compara enum parametro de entrada e um branch para o else', async function () {
        const func = getFunctionByName(contracts[0].contractDefinition, 'ifWithConditionBasedOnEnum');
        const branches = getBranches(truffleProject, contracts[0].contractDefinition, func, func);
        assert.isArray(branches);
        assert.equal(branches.length, 2);
        //branch 1
        assert.isArray(branches[0].restrictions);
        assert.equal(branches[0].restrictions.length, 1);
        assert.equal(branches[0].restrictions[0].operator, '==');
        assert.equal(branches[0].restrictions[0].right.value.js, 'fullOfBranchesAndRestrictions.P');
        assert.equal(branches[0].restrictions[0].right.value.memberName, 'D');
        assert.equal(branches[0].restrictions[0].left.value.js, 'meuEnum');
        //branch 1
        assert.isArray(branches[1].restrictions);
        assert.equal(branches[1].restrictions.length, 1);
        assert.equal(branches[1].restrictions[0].operator, '!=');
        assert.equal(branches[1].restrictions[0].right.value.js, 'fullOfBranchesAndRestrictions.P');
        assert.equal(branches[1].restrictions[0].right.value.memberName, 'D');
        assert.equal(branches[1].restrictions[0].left.value.js, 'meuEnum');
      });

      it('Deve retornar um branch para IF que compara atributo do tipo enum de parãmetro de entrada struct e um branch para o else', async function () {
        const func = getFunctionByName(contracts[0].contractDefinition, 'ifWithConditionBasedOnStructMemberEnum');
        const branches = getBranches(truffleProject, contracts[0].contractDefinition, func, func);
        assert.isArray(branches);
        assert.equal(branches.length, 2);
        //branch 1
        assert.isArray(branches[0].restrictions);
        assert.equal(branches[0].restrictions.length, 1);
        assert.equal(branches[0].restrictions[0].operator, '==');
        assert.equal(branches[0].restrictions[0].right.value.js, 'fullOfBranchesAndRestrictions.P');
        assert.equal(branches[0].restrictions[0].right.value.memberName, 'D');
        assert.equal(branches[0].restrictions[0].left.value.js, 'x');
        assert.equal(branches[0].restrictions[0].left.value.memberName, 'b');
        //branch 1
        assert.isArray(branches[1].restrictions);
        assert.equal(branches[1].restrictions.length, 1);
        assert.equal(branches[1].restrictions[0].operator, '!=');
        assert.equal(branches[1].restrictions[0].right.value.js, 'fullOfBranchesAndRestrictions.P');
        assert.equal(branches[1].restrictions[0].right.value.memberName, 'D');
        assert.equal(branches[1].restrictions[0].left.value.js, 'x');
        assert.equal(branches[1].restrictions[0].left.value.memberName, 'b');
      });

      it('Deve retornar um branch para cada IF e ELSEIF em sequência e outra para seus respectivos Elses com a inversão das restrições dos IFs e ELSEIFs anteriores acumuladas', async function () {
        const func = getFunctionByName(contracts[0].contractDefinition, 'ifSequence');
        const branches = getBranches(truffleProject, contracts[0].contractDefinition, func, func);
        assert.isArray(branches);
        assert.equal(branches.length, 6);
        //primeiro if/elseif
        assert.isArray(branches[0].restrictions);
        assert.equal(branches[0].restrictions.length, 1);
        assert.equal(branches[0].restrictions[0].operator, '==');
        assert.equal(branches[0].restrictions[0].right.value.js, '100');
        assert.equal(branches[0].restrictions[0].left.value.js, 'param1');
        assert.isArray(branches[1].restrictions);
        assert.equal(branches[1].restrictions.length, 1);
        assert.equal(branches[1].restrictions[0].operator, '==');
        assert.equal(branches[1].restrictions[0].right.value.js, '90');
        assert.equal(branches[1].restrictions[0].left.value.js, 'param1');
        assert.isArray(branches[2].restrictions);
        assert.equal(branches[2].restrictions.length, 2);
        assert.equal(branches[2].restrictions[0].operator, '!=');
        assert.equal(branches[2].restrictions[0].right.value.js, '90');
        assert.equal(branches[2].restrictions[0].left.value.js, 'param1');
        assert.equal(branches[2].restrictions[1].operator, '!=');
        assert.equal(branches[2].restrictions[1].right.value.js, '100');
        assert.equal(branches[2].restrictions[1].left.value.js, 'param1');
        //segundo if/elseif
        assert.isArray(branches[3].restrictions);
        assert.equal(branches[3].restrictions.length, 1);
        assert.equal(branches[3].restrictions[0].operator, '>');
        assert.equal(branches[3].restrictions[0].right.value.js, '100');
        assert.equal(branches[3].restrictions[0].left.value.js, 'param2');
        assert.isArray(branches[4].restrictions);
        assert.equal(branches[4].restrictions.length, 1);
        assert.equal(branches[4].restrictions[0].operator, '>');
        assert.equal(branches[4].restrictions[0].right.value.js, '90');
        assert.equal(branches[4].restrictions[0].left.value.js, 'param2');
        assert.isArray(branches[5].restrictions);
        assert.equal(branches[5].restrictions.length, 3);
        assert.equal(branches[5].restrictions[0].operator, '<=');
        assert.equal(branches[5].restrictions[0].right.value.js, '90');
        assert.equal(branches[5].restrictions[0].left.value.js, 'param2');
        //TODO: Analisar se esses dois abaixo deveriam realmente compor as restrições do ELSE
        assert.equal(branches[5].restrictions[1].operator, '!=');
        assert.equal(branches[5].restrictions[1].right.value.js, '100');
        assert.equal(branches[5].restrictions[1].left.value.js, 'param1');
        assert.equal(branches[5].restrictions[2].operator, '!=');
        assert.equal(branches[5].restrictions[2].right.value.js, '90');
        assert.equal(branches[5].restrictions[2].left.value.js, 'param1');
      });

      it('Deve retornar um branch para cada IF e ELSEIF em sequência e outra para seus respectivos Elses com a inversão das restrições dos IFs e ELSEIFs anteriores acumuladas REMOVENDO REDUNDÂNCIAS DE RESTRIÇÕES', async function () {
        const func = getFunctionByName(contracts[0].contractDefinition, 'ifSequenceElseRedundantRestrictions');
        const branches = getBranches(truffleProject, contracts[0].contractDefinition, func, func);
        assert.isArray(branches);
        assert.equal(branches.length, 4);
        //if/elseifs
        assert.isArray(branches[0].restrictions);
        assert.equal(branches[0].restrictions.length, 1);
        assert.equal(branches[0].restrictions[0].operator, '<=');
        assert.equal(branches[0].restrictions[0].right.value.js, '23');
        assert.equal(branches[0].restrictions[0].left.value.js, '_value');
        assert.isArray(branches[1].restrictions);
        assert.equal(branches[1].restrictions.length, 1);
        assert.equal(branches[1].restrictions[0].operator, '<=');
        assert.equal(branches[1].restrictions[0].right.value.js, '0xFF');
        assert.equal(branches[1].restrictions[0].left.value.js, '_value');
        assert.isArray(branches[2].restrictions);
        assert.equal(branches[2].restrictions.length, 1);
        assert.equal(branches[2].restrictions[0].operator, '<=');
        assert.equal(branches[2].restrictions[0].right.value.js, '0xFFFFFFFFFFFFFFFF');
        assert.equal(branches[2].restrictions[0].left.value.js, '_value');
        //else
        assert.isArray(branches[3].restrictions);
        assert.equal(branches[3].restrictions.length, 1);
        assert.equal(branches[3].restrictions[0].operator, '>');
        assert.equal(branches[3].restrictions[0].right.value.js, '0xFFFFFFFFFFFFFFFF');
        assert.equal(branches[3].restrictions[0].left.value.js, '_value');
      });

      it('Deve retornar um branch para cada IF em sequência e outra para seus respectivos Elses com a inversão das restrições dos IFs anteriores acumuladas', async function () {
        const func = getFunctionByName(contracts[0].contractDefinition, 'getEpochOffset');
        const branches = getBranches(truffleProject, contracts[0].contractDefinition, func, func);
        assert.isArray(branches);

        const eventTypesAbordados = [
          'IED',
          'IP',
          'IPCI',
          'FP',
          'DV',
          'PR',
          'MR',
          'RRY',
          'RR',
          'SC',
          'IPCB',
          'PRD',
          'TD',
          'STD',
          'MD',
          'SD',
          'AD',
          'Child',
        ];
        assert.equal(branches.length, eventTypesAbordados.length * 2);

        eventTypesAbordados.forEach((event, i) => {
          assert.isArray(branches[i * 2].restrictions, `If EventType.${event}`);
          assert.equal(branches[i * 2].restrictions.length, 1, `If EventType.${event}`);
          assert.equal(branches[i * 2].restrictions[0].operator, '==', `If EventType.${event}`);
          assert.equal(
            branches[i * 2].restrictions[0].right.value.js,
            `fullOfBranchesAndRestrictions.EventType`,
            `If EventType.${event}`
          );
          assert.equal(branches[i * 2].restrictions[0].right.value.memberName, event, `If EventType.${event}`);
          assert.equal(branches[i * 2].restrictions[0].left.value.js, 'eventType', `If EventType.${event}`);
          assert.isArray(branches[i * 2 + 1].restrictions, `else EventType.${event}`);
          assert.equal(
            //soma o length ao 'i' pois a medida que os if's em sequẽncia vão avançando,
            //vai se acumulando as condições os 'elses' dos ifs anteriores de forma que eles efetivamente sejam executados
            branches[i * 2 + 1].restrictions.length,
            i + 1,
            `else EventType.${event}: ${JSON.stringify(branches[i * 2 + 1].restrictions)}`
          );
          assert.equal(branches[i * 2 + 1].restrictions[0].operator, '!=', `else EventType.${event}`);
          assert.equal(
            branches[i * 2 + 1].restrictions[0].right.value.js,
            `fullOfBranchesAndRestrictions.EventType`,
            `else EventType.${event}`
          );
          assert.equal(branches[i * 2 + 1].restrictions[0].right.value.memberName, event, `else EventType.${event}`);
          assert.equal(branches[i * 2 + 1].restrictions[0].left.value.js, 'eventType', `else EventType.${event}`);
        });
      });

      it('Se a sequência de IFs usa operador "!=", deve retornar um branch para cada IF em sequência e outra para seus respectivos Elses, sem acúmulo de restrições dos elses anteriores', async function () {
        const functionUT = getFunctionByName(contracts[0].contractDefinition, 'ifSequenceInequality');
        const branches = getBranches(truffleProject, contracts[0].contractDefinition, functionUT, functionUT);
        assert.isArray(branches);

        const eventTypesAbordados = [
          'IED',
          'IP',
          'IPCI',
          'FP',
          'DV',
          'PR',
          'MR',
          'RRY',
          'RR',
          'SC',
          'IPCB',
          'PRD',
          'TD',
          'STD',
          'MD',
          'SD',
          'AD',
          'Child',
        ];
        assert.equal(branches.length, eventTypesAbordados.length * 2);

        eventTypesAbordados.forEach((event, i) => {
          assert.isArray(branches[i * 2].restrictions, `If EventType.${event}`);
          assert.equal(branches[i * 2].restrictions.length, 1, `If EventType.${event}`);
          //só o EventType.AD está com operador '=='
          if (event == 'AD') {
            assert.equal(branches[i * 2].restrictions[0].operator, '==', `If EventType.${event}`);
          } else {
            assert.equal(branches[i * 2].restrictions[0].operator, '!=', `If EventType.${event}`);
          }
          assert.equal(
            branches[i * 2].restrictions[0].right.value.js,
            `fullOfBranchesAndRestrictions.EventType`,
            `If EventType.${event}`
          );
          assert.equal(branches[i * 2].restrictions[0].right.value.memberName, event, `If EventType.${event}`);
          assert.equal(branches[i * 2].restrictions[0].left.value.js, 'eventType', `If EventType.${event}`);
          assert.isArray(branches[i * 2 + 1].restrictions, `else EventType.${event}`);
          //SOmente antes do child que temos uma comparação utilizando o operador que não seja '!='
          const restrictionLength = event == 'Child' ? 2 : 1;
          assert.equal(
            //soma o length ao 'i' pois a medida que os if's em sequẽncia vão avançando,
            //vai se acumulando as condições os 'elses' dos ifs anteriores de forma que eles efetivamente sejam executados
            branches[i * 2 + 1].restrictions.length,
            restrictionLength,
            `else EventType.${event}: ${JSON.stringify(branches[i * 2 + 1].restrictions)}`
          );
          //só o EventType.AD está com operador '=='
          if (event == 'AD') {
            assert.equal(branches[i * 2 + 1].restrictions[0].operator, '!=', `else EventType.${event}`);
          } else {
            assert.equal(branches[i * 2 + 1].restrictions[0].operator, '==', `else EventType.${event}`);
          }
          assert.equal(
            branches[i * 2 + 1].restrictions[0].right.value.js,
            `fullOfBranchesAndRestrictions.EventType`,
            `else EventType.${event}`
          );
          assert.equal(branches[i * 2 + 1].restrictions[0].right.value.memberName, event, `else EventType.${event}`);
          assert.equal(branches[i * 2 + 1].restrictions[0].left.value.js, 'eventType', `else EventType.${event}`);
        });
      });
    });
  });
});
