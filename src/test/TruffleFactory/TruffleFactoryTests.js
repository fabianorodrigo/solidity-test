const path = require('path');
const chai = require('chai');
(assert = chai.assert), (should = chai.should());

const { getTruffleProject } = require('../utils');

const { TruffleProjectIceFactory } = require('../../services/TruffleFactory');

describe('Truffle Factory', function () {
  describe('Obter arquivos de contrato', function () {
    it('Todos os arquivos .sol do subdiretório "contracts" devem ser retornados. Com exceção do "migrations.sol"', async function () {
      const truffleUtils = TruffleProjectIceFactory({
        truffleProjectHome: path.resolve('./dist/test/'),
      });
      assert.equal(truffleUtils.getContractsFiles().length, 5);
    });

    it('Todos os arquivos .sol do subdiretório "node_modules" devem ser retornados. Com exceção do "migrations.sol"', async function () {
      const truffleUtils = TruffleProjectIceFactory({
        truffleProjectHome: path.resolve('./dist/test/'),
      });
      assert.equal(truffleUtils.getThirdPartyContractsFiles().length, 1);
    });
  });

  describe('Enumerators', function () {
    it('Deve retornar todos os enumeradores existentes nos contratos do projeto', async function () {
      const truffleProject = getTruffleProject(path.resolve(__dirname, '../'));
      const enums = truffleProject.getEnums();
      assert.isNotNull(enums);
      //console.log(enums);
      assert.equal(Object.keys(enums).length, 3); //somente 3 contratos tem enumerator
      //libraryFullOfBranchesAndRestrictions.sol => P
      assert.isNotNull(enums.fullOfBranchesAndRestrictions.P.members);
      assert.isNotNull(enums.fullOfBranchesAndRestrictions.P.members.length, 6);
      assert.isNotNull(enums.fullOfBranchesAndRestrictions);
      //requireUse.sol => Teste
      assert.isNotNull(enums.RequireUse);
      assert.isNotNull(enums.RequireUse.P);
      assert.isNotNull(enums.RequireUse.P.members.length, 3);
      assert.isNotNull(enums.RequireUse.Booleano.members.length, 2);
    });

    it('Deve retornar todos os enumeradores nos contratos do projeto que tenham um determinado membro', async function () {
      const truffleProject = getTruffleProject(path.resolve(__dirname, '../'));
      const enums = truffleProject.getEnumsByName('P');
      assert.isNotNull(enums);
      assert.equal(enums.length, 2); //somente 2 enumerators tem o nome 'P'
      assert.equal(enums[0], 'fullOfBranchesAndRestrictions.P');
      assert.equal(enums[1], 'RequireUse.P');
    });
  });
});
