const path = require('path');
const chai = require('chai');
(assert = chai.assert), (should = chai.should());

const { mini, getContractsData } = require('../utils');

const { TruffleProjectIceFactory } = require('../../services/TruffleFactory');
const { TestBuilder, getJSTestHeader } = require('../../services/testBuilder');

const beforeEachTestBuilder = require('../../services/testBuilder/javascript/beforeEach');
const { extractTruffleProjectGraph } = require('../../services/TruffleProjectGraph');

describe('Test Factory', function () {
  describe('Geração de código BeforeEach', function () {
    it('Contratos sem construtor devem gerar o "new" apenas com o transactionParam.from pra account[0]', async function () {
      const dataTest = getContractsData(['./contracts/simplestUseless.sol']);
      const truffleProject = TruffleProjectIceFactory({ truffleProjectHome: path.resolve('./dist/test/') });
      truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
      const beforeEachCode = await beforeEachTestBuilder.generateMigrationBeforeEach(
        truffleProject,
        getJSTestHeader,
        dataTest.contracts,
        () => {
          return { code: 0, stdout: '' };
        }
      );
      assert.equal(
        mini(beforeEachCode.code),
        "lettrace=false;letcontractSimplestUseless=null;beforeEach(async()=>{contractSimplestUseless=awaitSimplestUseless.new({from:accounts[0]});if(trace)console.log('SUCESSO:SimplestUseless.new({from:accounts[0]}');});"
      );
    });
  });
});
