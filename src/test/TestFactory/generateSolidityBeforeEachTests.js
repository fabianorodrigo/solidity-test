const path = require('path');
const chai = require('chai');
(assert = chai.assert), (should = chai.should());

const { mini, getContractsData } = require('../utils');
const { TruffleProjectIceFactory } = require('../../services/TruffleFactory');

const { getSolidityHeader } = require('../../services/testBuilder/solidity/getSolidityHeader');

const beforeEachTestBuilder = require('../../services/testBuilder/solidity/beforeEach');

const { extractTruffleProjectGraph } = require('../../services/TruffleProjectGraph');

describe('Test Factory', function () {
  describe('Geração de código BeforeEach', function () {
    it('Contratos sem construtor devem gerar o "new" sem parâmetros', async function () {
      const dataTest = getContractsData(['./contracts/simplestUseless.sol']);
      const truffleProject = TruffleProjectIceFactory({
        truffleProjectHome: path.resolve('./dist/test/'),
      });
      //apurando e setando o grafo do projeto
      truffleProject.setGraph(extractTruffleProjectGraph(truffleProject));
      const beforeEachCode = await beforeEachTestBuilder.generateMigrationBeforeEach(
        truffleProject,
        getSolidityHeader,
        dataTest.contracts,
        () => {
          return {
            code: 0,
            stdout: '',
          };
        }
      );
      const regex = /uintnonce=([0-9]{1,});functionstringToBytes32\(stringmemorysource\)publicreturns\(bytes32result\)\{bytesmemorytempEmptyStringTest=bytes\(source\);if\(tempEmptyStringTest.length==0\)\{return0x0;\}assembly\{result:=mload\(add\(source,32\)\)\}\}SimplestUselesscontractSimplestUseless;functionbeforeEach\(\)public{contractSimplestUseless=newSimplestUseless\(\);}/gm;
      assert.isTrue(regex.test(mini(beforeEachCode.code)), mini(beforeEachCode.code));
    });
  });
});
