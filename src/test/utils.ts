import path from 'path';
import { TruffleProjectIceFactory, TruffleProjectIceFactoryFunction } from '../services/TruffleFactory';
import { isRequireCall, isBO } from '../services/solidityParser';
import { ElementaryTypeName, ContractDefinition, FunctionDefinition, UserDefinedTypeName } from 'solidity-parser-antlr';
import { IRestriction } from '../models/IResctriction';
import { IValue } from '../models/IValue';
import { ITypeValue } from '../models/scenario/ITypeValue';
import { RestrictionElementType } from '../models/RestrictionElementType';

const SolidityParserIceFactory = require('../services/SolidityParserFactory');

export const truffleProjectEmpty = {
  truffleProjectHome: path.join(__dirname, 'contracts'),
  getCompilerVersion: () => {
    return null;
  },
  getSolidityCoverageVersion: () => {
    return '0.6.7';
  },
  getGraph: () => {
    return { contracts: { empty: { functions: { _transfer: { parameters: [] } } } }, stateVariables: {} };
  },
  getValuesPool: () => {
    return { NumberLiteral: [0, 1] };
  },
  getEnumsByName: () => ['RequireUse.P'],
  getEnums: () => {
    return {
      RequireUse: {
        P: {
          members: [
            { type: 'Enum', name: 'a' },
            { type: 'Enum', name: 'b' },
            { type: 'Enum', name: 'c' },
          ],
        },
      },
    };
  },
};

export const contractEmpty = {
  type: 'ContractDefinition',
  name: 'empty',
  baseContracts: [],
  kind: 'contract',
  subNodes: [],
};

export const functionEmpty = {
  type: 'FunctionDefinition',
  parameters: [],
  modifiers: [],
  visibility: 'public',
  isConstructor: false,
  body: { type: 'Block', statements: [] },
};

export function getTruffleProject(truffleHome) {
  return TruffleProjectIceFactory({ truffleProjectHome: truffleHome });
}

/**
 * Generate test data based on the files passed
 *
 * @param {string[]} files Solidity files paths
 * @returns {object} retorns a objects with properties {contracts}, a array with contractDefinition in the AST,
 * and {valuesPool}, a objetct with literal values and UserDefinedTypeNames captured in the Solidity files passed
 */
export function getContractsData(files) {
  const retorno = { contracts: [], valuesPool: {} };
  const SolidityParsers = files.map((solFilePath) => {
    return SolidityParserIceFactory({
      solFilePath: path.resolve('dist/test', solFilePath),
    });
  });
  SolidityParsers.forEach((SolidityParser) => {
    retorno.contracts = retorno.contracts.concat(SolidityParser.getContracts());
    const pool = SolidityParser.getValuesPool();
    Object.keys(pool).forEach((tipo) => {
      if (retorno.valuesPool[tipo] == null) {
        retorno.valuesPool[tipo] = [];
      }
      if (tipo == 'structs' || tipo == 'contracts') {
        Object.keys(pool[tipo]).forEach((structORcontractName) => {
          if (retorno.valuesPool[tipo][structORcontractName] == null) {
            retorno.valuesPool[tipo][structORcontractName] = pool[tipo][structORcontractName];
          }
        });
      } else {
        pool[tipo].forEach((v) => {
          //Mesmo que o valor esteja várias vezes, será adicionado em duplicidade para que a chance de ser usado seja proporcional
          retorno.valuesPool[tipo].push(v);
        });
      }
    });
  });
  return retorno;
}

/**
 * Remove breaklines from {code}
 * @param {string} code
 */
export function mini(code) {
  return code.replace(/\n/g, '').replace(/ /g, '');
}

/**
 *
 * @param {Array} contracts Array contracts representantion
 * @param {string} name  Name of the contract persuid
 */
export function getContractByName(contracts, name) {
  for (let i = 0; i < contracts.length; i++) {
    if (contracts[i].contractDefinition.name == name) {
      return contracts[i].contractDefinition;
    }
  }
  throw new Error(`Contract "${name}" not found`);
}

/**
 *
 * @param {contractDefinition} contractDefinition The AST contract
 * @param {string} funcName  Name of the function persuid
 */
export function getFunctionByName(contractDefinition, funcName) {
  for (let i = 0; i < contractDefinition.subNodes.length; i++) {
    if (
      contractDefinition.subNodes[i].type == 'FunctionDefinition' &&
      contractDefinition.subNodes[i].name == funcName
    ) {
      return contractDefinition.subNodes[i];
    }
  }
  throw new Error(`Function "${funcName}" not found`);
}

export function getRequireCalls(functionDefinition) {
  const retorno = [];
  for (let i = 0; i < functionDefinition.body.statements.length; i++) {
    let st = functionDefinition.body.statements[i];

    //Se for chamada ao 'require' com uma operação binária (comparação ou combinação de comparações)
    if (isRequireCall(st) && isBO(st.expression.arguments[0])) {
      retorno.push(st);
    }
  }
  return retorno;
}

export function getBOFromRequireCall(requireCall) {
  return requireCall.expression.arguments[0];
}

export function getParametersValuesForTest(
  truffleProject: TruffleProjectIceFactoryFunction,
  contractDefinition: ContractDefinition,
  functionDefinition: FunctionDefinition,
  restrictions: IRestriction[]
): ITypeValue[] {
  const retorno: ITypeValue[] = functionDefinition.parameters.map((p) => {
    const paramRestrictions = restrictions.filter((r) => r.left.value.js == p.name);
    if (
      (<ElementaryTypeName>p.typeName).name == 'uint' ||
      (<ElementaryTypeName>p.typeName).name == 'uint256' ||
      (<ElementaryTypeName>p.typeName).name == 'uint8'
    ) {
      let value = '1';
      if (
        paramRestrictions.length == 1 &&
        paramRestrictions[0].right.elementType == RestrictionElementType.functionParameter
      ) {
        if (['!=', '>'].includes(paramRestrictions[0].operator)) {
          value = '2';
        } else if (paramRestrictions[0].operator == '<') {
          value = '0';
        }
      } else if (
        paramRestrictions.length == 2 &&
        paramRestrictions[0].right.elementType == RestrictionElementType.functionParameter &&
        paramRestrictions[1].right.elementType == RestrictionElementType.functionParameter
      ) {
        if (
          (paramRestrictions[0].operator == '!=' && paramRestrictions[1].operator.startsWith('<')) ||
          (paramRestrictions[1].operator == '!=' && paramRestrictions[0].operator.startsWith('<'))
        ) {
          value = '0';
        } else if (
          (paramRestrictions[0].operator == '!=' && paramRestrictions[1].operator.startsWith('>')) ||
          (paramRestrictions[1].operator == '!=' && paramRestrictions[0].operator.startsWith('>'))
        ) {
          value = '2';
        }
      }
      return {
        type: 'uint256',
        subtype: null,
        stringfieldValue: {
          isCode: false,
          js: value,
          sol: value,
          obj: parseInt(value),
          typeName: { type: 'ElementaryTypeName', name: 'uint' },
        },
      };
    } else if ((<ElementaryTypeName>p.typeName).name == 'string') {
      return {
        type: 'string',
        subtype: null,
        stringfieldValue: {
          isCode: false,
          js: '"just a test"',
          sol: '"just a test"',
          obj: 'just a test',
          typeName: { type: 'ElementaryTypeName', name: 'string' },
        },
      };
    } else if ((<UserDefinedTypeName>p.typeName).type == 'UserDefinedTypeName') {
      return {
        type: 'UserDefinedTypeName',
        subtype: 'RestrictionTypesTest.COVID',
        stringfieldValue: {
          isCode: false,
          js: paramRestrictions.length == 0 || paramRestrictions[0].operator == '==' ? '0' : '1',
          sol:
            paramRestrictions.length == 0 || paramRestrictions[0].operator == '=='
              ? 'RestrictionTypesTest.COVID.a'
              : 'RestrictionTypesTest.COVID.b',
          obj: '###null###',
          typeName: { type: 'UserDefinedTypeName', namePath: 'COVID' },
        },
      };
    } else {
      return {
        type: 'address',
        subtype: null,
        stringfieldValue: {
          isCode: false,
          js: 'accounts[1]',
          typeName: { type: 'ElementaryTypeName', name: 'address' },
          sol: '0x0',
          obj: 0x0,
        },
      };
    }
  });
  let valorTransacao = 7;
  let transactionParameterValue = restrictions.filter(
    (r) => r.left.elementType == RestrictionElementType.transactionParameter && r.left.value.js == 'value'
  );
  if (transactionParameterValue.length > 0 && ['<', '<='].includes(transactionParameterValue[0].operator)) {
    valorTransacao = 1;
  } else if (transactionParameterValue.length == 0) {
    transactionParameterValue = restrictions.filter(
      (r) => r.right.elementType == RestrictionElementType.transactionParameter && r.right.value.js == 'value'
    );
    if (transactionParameterValue.length > 0 && ['>', '>='].includes(transactionParameterValue[0].operator)) {
      valorTransacao = 1;
    }
  }
  retorno.push({
    type: 'TransactionParameter',
    subtype: null,
    stringfieldValue: {
      isCode: false,
      js: `{"from":"account[1]","value":${valorTransacao}}`,
      sol: `{"from":"account[1]","value":${valorTransacao}}`,
      obj: { from: 'account[1]', value: valorTransacao },
      typeName: { type: 'ElementaryTypeName', name: 'address' },
    },
  });
  return retorno;
}

export function randomValueForTest(
  truffleProject: TruffleProjectIceFactoryFunction,
  type: ElementaryTypeName,
  contractDefinition: ContractDefinition,
  restrictions: IRestriction[]
): IValue {
  if (type.name == 'uint' || type.name == 'uint256' || type.name == 'uint8') {
    return { isCode: false, js: '0', sol: '0', typeName: { type: 'ElementaryTypeName', name: 'uint' }, obj: 0 };
  } else if (type.name == 'string') {
    return {
      isCode: false,
      js: '"just a test"',
      sol: '"just a test"',
      obj: 'just a test',
      typeName: { type: 'ElementaryTypeName', name: 'string' },
    };
  } else {
    return {
      isCode: false,
      js: 'accounts[1]',
      typeName: { type: 'ElementaryTypeName', name: 'address' },
      sol: '0x0',
      obj: 0,
    };
  }
}
