pragma solidity ^0.4.24;

contract RestrictionTypesTest {
  enum COVID {a, b, c}
  enum Booleano {Y, N}

  struct StructTeste {
    uint256 a; // I=Integer
    COVID b;
    address to;
  }

  mapping(address => uint256) private _balances;

  mapping(address => mapping(address => uint256)) private _allowances;

  string private _name;
  string private _symbol;
  uint8 private _decimals;
  uint256 stateUint;
  string stateString;
  COVID stateEnum;
  address stateAddress;
  string source;
  
  uint[] private stateArray;
  uint[] private stateArrayImutavel;

  modifier addressRequireModifier(address recipient) {
    require(recipient != 0x0, 'Recipient must be valid');
    _;
  }

  modifier uintRequireModifier(uint256 x) {
    require(x != 0, 'x must be valid');
    _;
  }

  constructor(
    string memory name,
    string memory symbol,
    uint8 decimals
  ) public {
    IfElseifWithRequire(testeState, testeState);
    _name = name;
    _symbol = symbol;
    _decimals = decimals;
    stateUint = decimals;
  }

  function noRestriction(string name) {
    _name = name;
  }

  function setStateEnum(COVID pEnum) {
    stateEnum = pEnum;
  }
  
  function setStateAddress(address newAddress) {
    stateAddress = newAddress;
  }
  
  function pushStateArray(uint param){
    stateArray.push(param);
  }

  //--------- REQUIRES -----------

  //functionParameter x Literal
  function simpleRequires_functionParameter_Literal(
    address recipient,
    uint256 value1,
    uint256 value2,
    boolean booleano,
    string paramString
  ) public addressRequireModifier(recipient) {
    require(recipient != 0x0);
    require(value1 > 0);
    require(booleano == true);
    require(paramString == 'The Test');
    require(recipient == 0x0000000000000000000000000000000000000000);
    require(1979 < value2);
    require(paramString == 'teste' && value1 < 1000, 'StateUint maior que 1000 e stateString igual a teste');
  }

  //stateVar x Literal
  function simpleRequires_StateVar_Literal(address recipient) public view returns (string memory) {
    require(stateUint > 0, 'StateUint maior que zero');
    require(0 < stateUint, 'StateUint maior que zero');
    require(stateString == 'teste' && stateUint < 1000, 'StateUint maior que 1000 e stateString igual a teste');
    require(recipient != address(0), 'ERC20: transfer to the zero address');
    return _name;
  }

  function simpleRequires_StateVar_Literal_withoutSetFunction(address recipient) public view returns (string memory) {
    require(stateString == 'teste', 'stateString igual a teste');
    return _name;
  }
  
  //stateVar Array Length x Literal
  
  function simpleRequires_StateVarArrayLength_Literal_withoutSetFunction(address recipient) public view returns (string memory) {
    require(stateArrayImutavel.length > 0, 'Tamanho stateArray tem que ser maior que zero');
  }
  
  function simpleRequires_StateVarArrayLength_Literal(address recipient) public view returns (string memory) {
    require(stateArray.length > 0, 'Tamanho stateArray tem que ser maior que zero');
  }

  //stateVar Array Length x functionParameter
  
  function simpleRequires_StateVarArrayLength_functionParameter_withoutSetFunction(uint length) public view returns (string memory) {
    require(stateArrayImutavel.length > length, 'Tamanho stateArray tem que ser maior que zero');
  }
  
  function simpleRequires_StateVarArrayLength_functionParameter(uint length) public view returns (string memory) {
    require(stateArray.length > length, 'Tamanho stateArray tem que ser maior que zero');
  }


  //stateVar x functionParameter
  function simpleRequires_StateVar_functionParameter(
    address recipient,
    uint256 value1,
    uint256 value2,
    string value3
  ) public view returns (string memory) {
    require(stateUint > value1, 'StateUint maior que zero');
    require(value1 < stateUint, 'StateUint maior que zero');
    require(stateString == value3 && stateUint < value2, 'StateUint maior que 1000 e stateString igual a teste');
    require(recipient != address(0), 'ERC20: transfer to the zero address');
    return _name;
  }
  
  function simpleRequires_StateVar_functionParameter_withoutSetFunction(string param) public view returns (string memory) {
    require(stateString == param, 'stateString igual a param');
    return _name;
  }

  //stateVar x functionParameterMember
  function simpleRequires_StateVar_functionParameterMember(StructTeste param1) public view returns (string memory) {
    require(stateUint > param1.a, 'StateUint maior que zero');
    require(param1.a < stateUint, 'StateUint maior que zero');
    require(stateEnum == param1.b && stateUint < param1.a, 'StateUint maior que 1000 e stateString igual a teste');
    require(recipient != address(0), 'ERC20: transfer to the zero address');
    return _name;
  }

  //stateVar x transactionParameter
  function simpleRequires_StateVar_transactionParameter() public view returns (string memory) {
    require(stateUint >= msg.value, 'StateUint maior que zero');
    require(msg.value <= stateUint, 'StateUint maior que zero');
    require(
      stateAddress == msg.sender && stateUint < msg.value,
      'StateUint maior que 1000 e stateString igual a teste'
    );
    require(recipient != address(0), 'ERC20: transfer to the zero address');
    return _name;
  }

  function simpleRequires_functionParameter_functionParameter(
    uint256 teste,
    uint256 teste2,
    uint256 teste3
  ) public uintRequireModifier(teste) {
    require(teste == teste2);
    require(teste > teste2 && teste > teste3);
    testeState = teste;
  }

  function simpleRequires_functionParameterMember_Literal(StructTeste structWithEnum) {
    require(structWithEnum.a > 0);
    require(structWithEnum.a > 0 && structWithEnum.b == COVID.c);
    testeState = teste;
  }

  function simpleRequires_functionParameterMember_functionParameter(
    StructTeste structWithEnum,
    uint256 param,
    uint256 param2,
    COVID e
  ) {
    require(structWithEnum.a > param);
    require(structWithEnum.a > param2 && structWithEnum.b == e);
    testeState = teste;
  }

  function simpleRequires_functionParameterMember_functionParameterMember(
    StructTeste param1,
    StructTeste param2,
    StructTeste param3
  ) {
    require(param1.a > param2.a);
    require(param2.a > param3.a && param3.a < param1.a);
    testeState = teste;
  }

  function simpleRequires_transactionParameter_Literal() {
    require(msg.value > 0);
    require(msg.value > 0 && msg.sender != 0x0000000000000000000000000000000000000000);
    testeState = teste;
  }

  function simpleRequires_transactionParameter_functionParameter(uint256 param, address to) {
    require(msg.value > param);
    require(msg.value > param && msg.sender == to);
    testeState = teste;
  }

  function simpleRequires_transactionParameter_functionParameterMember(StructTeste param1) {
    require(msg.value > param1.a);
    require(msg.value > param1.a && msg.sender == param1.to);
    testeState = teste;
  }

  //--------- BRANCHES/IFS -----------

  //functionParameter x functionParameter
  function simpleIf_functionParameter_functionParameter(uint256 param1, uint256 param2) public pure {
    if (param1 > param2) {
      timestamp = 0;
    }
  }

  function ifElseIfElse_functionParameter_functionParameter(uint256 param1, uint256 param2)
    internal
    pure
    returns (uint256 retorno)
  {
    if (param1 == param2) {
      retorno = 31;
    } else if (param1 > param2) {
      retorno = 30;
    } else {
      retorno = 28;
    }
  }

  function ifWithConditionBasedOnEnum_functionParameter_functionParameter(COVID meuEnum, COVID seuEnum) {
    if (meuEnum == seuEnum) {
      timestamp = 0;
    } else {
      timestamp = 10;
    }
  }

  //functionParameter x transactionParameter

  function ifWithCondition_transactionParameter_functionParameter(address _to, uint256 maxValue) {
    if (msg.value > maxValue) {
      timestamp = 0;
    } else {
      timestamp = 10;
    }
  }

  //stateVar x Literal

  function if_stateVar_Literal() public view returns (string memory) {
    if (stateUint > 0) {
      timestamp = 0;
    }
  }

  function ifElseIfElse_stateVar_Literal(uint256 param1, uint256 param2) internal pure returns (uint256 retorno) {
    if (stateUint == 100) {
      retorno = 31;
    } else if (stateUint > 100) {
      retorno = 30;
    } else {
      retorno = 28;
    }
  }

  function ifWithConditionBasedOnEnum_stateVar_Literal() {
    if (stateEnum == COVID.a) {
      timestamp = 0;
    } else {
      timestamp = 10;
    }
  }

  function ifWithRequire_stateVar_Literal() public pure {
    if (stateUint > 10) {
      require(stateEnum == COVID.a, 'stateEnum has to be COVID.a');
    }
  }

  function ifElseifWithRequire_stateVar_Literal() public pure {
    if (stateUint > 10) {
      require(stateEnum == COVID.a);
    } else if (stateEnum == COVID.b) {
      require(stateUint != 0);
    } else if (stateEnum == COVID.c) {
      _name = 'teste';
    }
  }
  
  //stateVar x functionParameter
  
  function if_stateVar_functionParameter(uint param) public view returns (string memory) {
    if (stateUint > param) {
      timestamp = 0;
    }
  }

  function ifElseIfElse_stateVar_functionParameter(uint256 param1, uint256 param2) internal pure returns (uint256 retorno) {
    if (stateUint == param1) {
      retorno = 31;
    } else if (stateUint > param2) {
      retorno = 30;
    } else {
      retorno = 28;
    }
  }

  function ifWithConditionBasedOnEnum_stateVar_functionParameter(COVID enumParam) {
    if (stateEnum == enumParam) {
      timestamp = 0;
    } else {
      timestamp = 10;
    }
  }
  
  function ifElseifWithRequire_stateVar_functionParameter(uint param) public pure {
    if (stateUint > param) {
      require(stateEnum == COVID.a);
    } else if (stateEnum == COVID.b) {
      require(stateUint != param);
    } else if (stateEnum == COVID.c) {
      _name = 'teste';
    }
  }
  
  //stateVar x transactionParameter

  function ifWithCondition_stateVar_transactionParameter(address _to, uint256 maxValue) {
    if (msg.sender == stateAddress) {
      timestamp = 0;
    } else {
      timestamp = 10;
    }
  }

  function setSource(string s) {
    source = s;
  }
}
