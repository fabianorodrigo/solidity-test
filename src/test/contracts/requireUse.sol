pragma solidity ^0.4.24;

import './RestrictionTypesTest.sol';

contract RequireUse is RestrictionTypesTest {


    enum P {a, b, c}
    enum Booleano {Y, N}

    struct StructTeste {
      uint256 a; // I=Integer
      P b;
    }

    mapping (address => uint256) private _balances;

    mapping (address => mapping (address => uint256)) private _allowances;

    string private _name;
    string private _symbol;
    uint8 private _decimals;
    uint testeState;
	address addressState;

    constructor (string memory name, string memory symbol, uint8 decimals) public {
         IfElseifWithRequire(testeState, testeState);
        _name = name;
        _symbol = symbol;
        _decimals = decimals;
		setDecimals(0);
    }

    function simpleRequires(address recipient, uint value1, uint value2, uint value3) public view returns (string memory) {
        require(value1 > 0, "Valor maior que zero");
        require(value2 > 0 && value3 < 1000, "Valor2 maior que zero e valor3 menor que 1000");
        require(recipient != address(0), "ERC20: transfer to the zero address");
        return _name;
    }

    function simpleRequireCompareInteger(uint teste){
      require(teste == 1979);
      testeState = teste;
    }

    function simpleFunctionCallThatHasRequires(address recipient, uint value1, uint value2, uint value3) public view returns (string memory) {
        _internalFunction(msg.sender, recipient, value1, 2);
      testeState = _balances[address];
    }

    function simpleFunctionCallThatHasRequiresTwiceTheSameParam(address recipient, uint value1, uint value2, uint value3) public view returns (string memory) {
        _internalFunction(msg.sender, recipient, value1, value1);
    }

    function _internalFunction(address sender, address recipient, uint256 amount, uint256 montante) internal {
        require(sender != address(0), "ERC20: transfer from the zero address");
        require(recipient != address(0), "ERC20: transfer to the zero address");
        require(amount <= 1979, "Amount maior que 1979");
        require(montante > 16,"Montante menor ou igual a zero");
		testeState = montante;
		addressState = sender;
    }

    function requireWithConditionBasedOnStructMember(StructTeste value1){
      require(value1.a > 100);
    }

    function requireWithConditionBasedOnStructMemberEnum(StructTeste structWithEnum){
      internalRequireEnum(structWithEnum.b);
    }

    function stateVariableWrittenConditionally(StructTeste structWithEnum){
      if(structWithEnum.a > 10){
        testeState = structWithEnum.a;
      }else{
        testeState = msg.value;
      }
    }

    function stateVariableWrittenConditionallyInFalseBody(StructTeste structWithEnum){
      if(structWithEnum.a > 10){
        internalRequireEnum(structWithEnum.b);
      } else{
        testeState = msg.value;
      }
    }

    function stateVariableWrittenConditionallyIfElseifElse(StructTeste structWithEnum){
      if(structWithEnum.a > 10){
        internalRequireEnum(structWithEnum.b);
      } else if(structWithEnum.a > 100){
        testeState = 10;
      }
      else{
        _name = 'opa';
      }
    }

    function internalRequireEnum(P p) internal{
      require(p == P.c);
    }

    function simpleIfWithRequire(uint param1, uint param2) public pure{
      if(param1 > 10){
        require(param2 > 100, "param2 has to be greater than 100");
      }
    }

    function IfElseifWithRequire(uint param1, uint param2) public pure{
      if(param1 > 10){
        require(param2 > 100, "param2 has to be greater than 100");
      }else if(param2 == 5){
        require(param1 != 0, "param1 must to be different of zero");
      }else if(param2 == 3){
        _name = "teste";
      }
    }
	
	function setName(string n) {
      _name = n;
    }
	
	function setName(string n, string x) {
      _name = n + x;
	  _name = 'oi';
    }
	
    function setDecimals(uint8 d) {
      _decimals = d;
	  _symbol = 'x';
    }
	
	function setDecimals(uint8 d, string teste) {
      uint a = d + 1;
	  setName('teste');
	  RestrictionTypesTest.noRestriction('testeParent');
    }
	
    function simpleLocalWrite(uint teste){
      require(teste == 1979);
      uint local = teste;
    }

    //getFunctionCallNameReference
	function simpleFunctionCallLiteral(uint8 d) returns (uint memory) {
      setDecimals(0);
	  return d;
    }

    function simpleLocalStringWrite(string w, uint x, uint y){
      string local = w;
    }

    function simpleFunctionCall(uint8 d, string teste) {
      setName('algo');
	  setName(teste);
	  simpleLocalStringWrite(teste, d, d);
    }
	
	function nestedFunctionCall(uint8 a, uint b, uint c) {
	  simpleFunctionCallLiteral(a);
    }

	function nestedNestedFunctionCall(uint8 a, uint b, string c, byte z) {
	  simpleFunctionCall(0,c);
    }
	
	function functionCallInsideIF(uint param1) public pure{
      if(param1 > 10){
        setDecimals(param1);
      }
    }
	
	function functionCallInsideELSEIF(uint param1) public pure{
      if(param1 > 10){
        setDecimals(0);
      } else if(param == 10){
	    simpleLocalWrite(param1);
	  } else{
	  	setDecimals(10);
	  }
    }
	
	function functionCallInsideELSE(uint param1) public pure{
      if(param1 > 10){
        setDecimals(0);
      }else{
	    simpleLocalWrite(param1);
	  }
    }

	function functionCallOnIFConditional(uint param1) public pure{
	  uint local = 0;
      if(simpleFunctionCallLiteral(param1) > 10){
        local = 1;
      }
    }
	
	function functionCallOnELSEIFConditional(uint param1) public pure{
	  uint local = 0;
      if(param1 > 10){
        local = 1;
      } else if(simpleFunctionCallLiteral(param1) == 10){
	    local = 2;
	  } else{
	  	local = 3;
	  }
    }
	
	function functionCallOnIFConditionalinsideELSE(uint param1) public pure{
	  uint local = 0;
      if(param1 > 10){
        local = 1;
      } else{
	  	if(simpleFunctionCallLiteral(param1) == 10){
	    	local = 2;
		}else{
			local = 3;
		}
	  }
    }
	
	function getStructTeste(uint z) returns(StructTeste){
		return StructTeste(10,P.c);
	}
	
	function functionMemberAccessFunctionResultVariableInitialValue(uint param1) public pure{
	  uint local = 0;
	  uint x = getStructTeste(param1).a;
    }
	
	function functionMemberAccessFunctionResultVariableSetValue(uint param1) public pure{
	  uint local = 0;
	  local = getStructTeste(param1).a;
    }
	
	function functionMemberAccessFunctionResultRequire(uint param1) public pure{
	  require(getStructTeste(param1).a > 0);
    }
	
	function functionResultAsInputParameter(uint param1){
		uint local;
		local = param1 + simpleFunctionCallLiteral(param1);
	}
	

}
