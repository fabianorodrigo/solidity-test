import { RestrictionElementType } from '../models/RestrictionElementType';
import { BranchType } from '../models/BranchType';
import { IRestriction } from '../models/IResctriction';

const rv_functionParameter_A = {
  elementType: RestrictionElementType.functionParameter,
  value: { isCode: false, js: 'A', sol: 'A', obj: '###null###' },
};
const rv_functionParameter_P = {
  elementType: RestrictionElementType.functionParameter,
  value: { isCode: false, js: 'P', sol: 'P', obj: '###null###' },
};
const rv_functionParameter_Z = {
  elementType: RestrictionElementType.functionParameter,
  value: { isCode: false, js: 'Z', sol: 'Z', obj: '###null###' },
};
const rv_functionParameter_To = {
  elementType: RestrictionElementType.functionParameter,
  value: { isCode: false, js: 'to', sol: 'to', obj: '###null###' },
};
const rv_functionParameter_Max = {
  elementType: RestrictionElementType.functionParameter,
  value: { isCode: false, js: 'max', sol: 'max', obj: '###null###' },
};
const rv_number0 = {
  elementType: RestrictionElementType.Literal,
  value: { isCode: false, js: '0', sol: '0', obj: 0 },
};
const rv_number17 = {
  elementType: RestrictionElementType.Literal,
  value: { isCode: false, js: '17', sol: '17', obj: 17 },
};
const rv_number7 = {
  elementType: RestrictionElementType.Literal,
  value: { isCode: false, js: '7', sol: '7', obj: 7 },
};
const rv_string_Unirio = {
  elementType: RestrictionElementType.Literal,
  value: { isCode: false, js: '"Unirio"', sol: '"Unirio"', obj: 'Unirio' },
};
const rv_transactionParameter_sender = {
  elementType: RestrictionElementType.transactionParameter,
  value: { isCode: false, js: 'sender', sol: 'sender', obj: '###null###' },
};
const rv_transactionParameter_value = {
  elementType: RestrictionElementType.transactionParameter,
  value: { isCode: false, js: 'value', sol: 'value', obj: '###null###' },
};
const rv_address0x0 = {
  elementType: RestrictionElementType.Literal,
  value: {
    isCode: false,
    js: '0x0000000000000000000000000000000000000000',
    sol: '0x0000000000000000000000000000000000000000',
    obj: 0x0,
  },
};
const rv_stateVariable_X = {
  elementType: RestrictionElementType.stateVariable,
  value: { isCode: false, js: 'X', sol: 'X', obj: '###null###' },
};
const rv_stateVariable_Y = {
  elementType: RestrictionElementType.stateVariable,
  value: { isCode: false, js: 'Y', sol: 'Y', obj: '###null###' },
};
const rv_stateVariable_source = {
  elementType: RestrictionElementType.stateVariable,
  value: { isCode: false, js: 'source', sol: 'source', obj: '###null###' },
};
export const r_Z_les_P: IRestriction = {
  left: rv_functionParameter_Z,
  operator: '<',
  right: rv_functionParameter_P,
  from: BranchType.notDefined,
};
export const r_Z_gre_P: IRestriction = {
  left: rv_functionParameter_Z,
  operator: '>',
  right: rv_functionParameter_P,
  from: BranchType.notDefined,
};
export const r_A_geq_Z: IRestriction = {
  left: rv_functionParameter_A,
  operator: '>=',
  right: rv_functionParameter_Z,
  from: BranchType.notDefined,
};
export const r_eq_number7: IRestriction = {
  operator: '==',
  from: BranchType.notDefined,
  right: rv_number7,
  left: rv_functionParameter_P,
};
export const r_uneq_number7: IRestriction = {
  operator: '!=',
  from: BranchType.notDefined,
  right: rv_number7,
  left: rv_functionParameter_P,
};
export const r_geq_number7: IRestriction = {
  operator: '>=',
  from: BranchType.notDefined,
  right: rv_number7,
  left: rv_functionParameter_P,
};
export const r_gre_number7: IRestriction = {
  operator: '>',
  from: BranchType.notDefined,
  right: rv_number7,
  left: rv_functionParameter_P,
};
export const r_leq_number7: IRestriction = {
  operator: '<=',
  from: BranchType.notDefined,
  right: rv_number7,
  left: rv_functionParameter_P,
};
export const r_les_number7: IRestriction = {
  operator: '<',
  from: BranchType.notDefined,
  right: rv_number7,
  left: rv_functionParameter_P,
};
export const r_uneq_number17: IRestriction = {
  operator: '!=',
  from: BranchType.notDefined,
  right: rv_number17,
  left: rv_functionParameter_P,
};
export const r_geq_number17: IRestriction = {
  operator: '>=',
  from: BranchType.notDefined,
  right: rv_number17,
  left: rv_functionParameter_P,
};
export const r_gre_number17: IRestriction = {
  operator: '>',
  from: BranchType.notDefined,
  right: rv_number17,
  left: rv_functionParameter_P,
};
export const r_leq_number17: IRestriction = {
  operator: '<=',
  from: BranchType.notDefined,
  right: rv_number17,
  left: rv_functionParameter_P,
};
export const r_les_number17: IRestriction = {
  operator: '<',
  from: BranchType.notDefined,
  right: rv_number17,
  left: rv_functionParameter_P,
};
export const r_msgSender_neq_0x0: IRestriction = {
  left: rv_transactionParameter_sender,
  operator: '!=',
  right: rv_address0x0,
  from: BranchType.notDefined,
};
export const r_msgSender_neq_to: IRestriction = {
  left: rv_transactionParameter_sender,
  operator: '!=',
  right: rv_functionParameter_To,
  from: BranchType.notDefined,
};
export const r_msgValue_gre_0: IRestriction = {
  left: rv_transactionParameter_value,
  operator: '!=',
  right: rv_number0,
  from: BranchType.notDefined,
};
export const r_msgValue_leq_paramMax: IRestriction = {
  left: rv_transactionParameter_value,
  operator: '<',
  right: rv_functionParameter_Max,
  from: BranchType.notDefined,
};

export const r_stateVariableX_gre_paramMax: IRestriction = {
  left: rv_stateVariable_X,
  operator: '>',
  right: rv_functionParameter_Max,
  from: BranchType.notDefined,
};
export const r_stateVariableX_eq_stateVariableY: IRestriction = {
  left: rv_stateVariable_X,
  operator: '==',
  right: rv_stateVariable_Y,
  from: BranchType.notDefined,
};
export const r_stateVariableX_gre_number7: IRestriction = {
  left: rv_stateVariable_X,
  operator: '>',
  right: rv_number7,
  from: BranchType.notDefined,
};
export const r_number7_les_stateVariableX: IRestriction = {
  left: rv_number7,
  operator: '<',
  right: rv_stateVariable_X,
  from: BranchType.notDefined,
};
export const r_stateVariable_source_eq_stringUnirio: IRestriction = {
  left: rv_stateVariable_source,
  operator: '>',
  right: rv_string_Unirio,
  from: BranchType.notDefined,
};

const rv_number17_fordebug = {
  isCode: false,
  elementType: RestrictionElementType.Literal,
  value: { isCode: false, js: '17', sol: '19', obj: 19 },
};
export const r_leq_number17_fordebug: IRestriction = {
  operator: '<=',
  from: BranchType.notDefined,
  right: rv_number17_fordebug,
  left: rv_functionParameter_P,
};
