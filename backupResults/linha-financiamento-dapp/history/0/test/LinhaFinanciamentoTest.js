const truffleAssert = require('truffle-assertions');
const Gerenciavel = artifacts.require("Gerenciavel");
const LinhaFinanciamento = artifacts.require("LinhaFinanciamento");

contract("LinhaFinanciamento",(accounts)=>{
  let trace = false;
  let contractGerenciavel = null;
  let contractLinhaFinanciamento = null;
  beforeEach(async () => {
    contractGerenciavel = await Gerenciavel.new({from:accounts[0]});
    if(trace) console.log('SUCESSO: Gerenciavel.new({from:accounts[0]}');
    contractLinhaFinanciamento = await LinhaFinanciamento.new(20,{from:accounts[0]});
    if(trace) console.log('SUCESSO: LinhaFinanciamento.new(20,{from:accounts[0]}');
  });
  
  it('Should execute solicitar(uint8,uint256) WHEN msg.sender!=gerente,idade>=18', async () => {
    await contractLinhaFinanciamento.mudarGerente(accounts[5],{from:accounts[0]});
    let result = await contractLinhaFinanciamento.solicitar(21, 64,{from: accounts[2]});
  });
  it('Should fail solicitar(uint8,uint256) when NOT comply with: msg.sender != gerente', async () => {
    await contractLinhaFinanciamento.mudarGerente(accounts[5],{from:accounts[0]});
    let result = await truffleAssert.fails(contractLinhaFinanciamento.solicitar(21, 64,{from: accounts[5]}),'revert');
  });
  it('Should fail solicitar(uint8,uint256) when NOT comply with: idade >= 18', async () => {
    await contractLinhaFinanciamento.mudarGerente(accounts[5],{from:accounts[0]});
    let result = await truffleAssert.fails(contractLinhaFinanciamento.solicitar(17, 64,{from: accounts[2]}),'revert');
  });
  it('Should execute deliberar(uint256,bool) WHEN aprovado==true,msg.sender==gerente,financiamentos.length>=indiceArray', async () => {
    let result = await contractLinhaFinanciamento.deliberar(0, true,{from: accounts[0]});
  });
  it('Should fail deliberar(uint256,bool) when NOT comply with: msg.sender == gerente', async () => {
    let result = await truffleAssert.fails(contractLinhaFinanciamento.deliberar(0, true,{from: accounts[9]}),'revert');
  });
  it('Should fail deliberar(uint256,bool) when NOT comply with: financiamentos.length >= indiceArray', async () => {
    await contractLinhaFinanciamento.mudarGerente(accounts[9],{from:accounts[0]});
    let result = await truffleAssert.fails(contractLinhaFinanciamento.deliberar(0, true,{from: accounts[0]}),'revert');
  });
  it('Should execute deliberar(uint256,bool) WHEN aprovado!=true,msg.sender==gerente,financiamentos.length>=indiceArray', async () => {
    let result = await contractLinhaFinanciamento.deliberar(0, false,{from: accounts[0]});
  });
  it('Should fail deliberar(uint256,bool) when NOT comply with: msg.sender == gerente', async () => {
    let result = await truffleAssert.fails(contractLinhaFinanciamento.deliberar(0, false,{from: accounts[9]}),'revert');
  });
  it('Should fail deliberar(uint256,bool) when NOT comply with: financiamentos.length >= indiceArray', async () => {
    await contractLinhaFinanciamento.mudarGerente(accounts[6],{from:accounts[0]});
    let result = await truffleAssert.fails(contractLinhaFinanciamento.deliberar(0, false,{from: accounts[0]}),'revert');
  });
});
