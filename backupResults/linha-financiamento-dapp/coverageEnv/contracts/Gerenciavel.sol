pragma solidity 0.5.7;

contract Gerenciavel {event __CoverageGerenciavel(string fileName, uint256 lineNumber);
event __FunctionCoverageGerenciavel(string fileName, uint256 fnId);
event __StatementCoverageGerenciavel(string fileName, uint256 statementId);
event __BranchCoverageGerenciavel(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageGerenciavel(string fileName, uint256 branchId);
event __AssertPostCoverageGerenciavel(string fileName, uint256 branchId);

    /** atual gerente da dApp */
    address private gerente;

    constructor() public {emit __FunctionCoverageGerenciavel('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/Gerenciavel.sol',1);

emit __CoverageGerenciavel('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/Gerenciavel.sol',8);
        emit __StatementCoverageGerenciavel('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/Gerenciavel.sol',1);
gerente = msg.sender;
    }

    modifier somenteGerente {emit __FunctionCoverageGerenciavel('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/Gerenciavel.sol',2);

emit __CoverageGerenciavel('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/Gerenciavel.sol',12);
        emit __AssertPreCoverageGerenciavel('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/Gerenciavel.sol',1);
emit __StatementCoverageGerenciavel('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/Gerenciavel.sol',2);
require(
            msg.sender == gerente,
            "Somente o gerente desta dApp pode executar esta operacao"
        );emit __AssertPostCoverageGerenciavel('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/Gerenciavel.sol',1);

emit __CoverageGerenciavel('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/Gerenciavel.sol',16);
        _;
    }
    modifier naoGerente {emit __FunctionCoverageGerenciavel('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/Gerenciavel.sol',3);

emit __CoverageGerenciavel('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/Gerenciavel.sol',19);
        emit __AssertPreCoverageGerenciavel('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/Gerenciavel.sol',2);
emit __StatementCoverageGerenciavel('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/Gerenciavel.sol',3);
require(
            msg.sender != gerente,
            "O gerente desta dApp nao pode executar esta operacao"
        );emit __AssertPostCoverageGerenciavel('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/Gerenciavel.sol',2);

emit __CoverageGerenciavel('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/Gerenciavel.sol',23);
        _;
    }

    function mudarGerente(address novoGerente) public somenteGerente {emit __FunctionCoverageGerenciavel('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/Gerenciavel.sol',4);

emit __CoverageGerenciavel('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/Gerenciavel.sol',27);
        emit __StatementCoverageGerenciavel('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/Gerenciavel.sol',4);
gerente = novoGerente;
    }
}
