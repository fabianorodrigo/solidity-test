pragma solidity 0.5.7;

import "./Gerenciavel.sol";

contract LinhaFinanciamento is Gerenciavel {event __CoverageLinhaFinanciamento(string fileName, uint256 lineNumber);
event __FunctionCoverageLinhaFinanciamento(string fileName, uint256 fnId);
event __StatementCoverageLinhaFinanciamento(string fileName, uint256 statementId);
event __BranchCoverageLinhaFinanciamento(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageLinhaFinanciamento(string fileName, uint256 branchId);
event __AssertPostCoverageLinhaFinanciamento(string fileName, uint256 branchId);

  struct Financiamento {
    address origem;
    uint8 idade;
    uint256 valor;
    bool aprovado;
    uint8 taxa;
    address gerente;
  }

  uint256 private capital;

  constructor(uint256 capitalLF) public Gerenciavel() {emit __FunctionCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',1);

emit __CoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',18);
    emit __StatementCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',1);
capital = capitalLF;
  }

  Financiamento[] private financiamentos;

  function solicitar(uint8 idade, uint256 valor) public naoGerente {emit __FunctionCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',2);

emit __CoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',24);
    emit __AssertPreCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',1);
emit __StatementCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',2);
require(idade >= 18, "O proponente deve ser maior de idade");emit __AssertPostCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',1);

emit __CoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',25);
    emit __StatementCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',3);
financiamentos.push(Financiamento(msg.sender, idade, valor, false, 0, address(0)));
  }

  function deliberar(uint256 indiceArray, bool aprovado) public somenteGerente {emit __FunctionCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',3);

emit __CoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',29);
    emit __AssertPreCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',2);
emit __StatementCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',4);
require(financiamentos.length > indiceArray, "A solicitacao nao existe");emit __AssertPostCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',2);

emit __CoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',30);
    emit __AssertPreCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',3);
emit __StatementCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',5);
require(
      financiamentos[indiceArray].gerente == address(0) && financiamentos[indiceArray].taxa == 0,
      "Solicitacao ja deliberada"
    );emit __AssertPostCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',3);

emit __CoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',34);
    emit __StatementCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',6);
Financiamento memory f = financiamentos[indiceArray];
emit __CoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',35);
    emit __StatementCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',7);
if (aprovado) {emit __BranchCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',4,0);
emit __CoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',36);
      emit __StatementCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',8);
uint8 taxa;
emit __CoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',37);
      emit __StatementCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',9);
if (f.idade > 65) {emit __BranchCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',5,0);
emit __CoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',38);
        emit __StatementCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',10);
taxa = 15; //1,5% aa
      } else {emit __BranchCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',5,1);
emit __CoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',40);
        emit __StatementCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',11);
taxa = 20; //2,0% aa
      }
emit __CoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',42);
      emit __StatementCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',12);
f.aprovado = true;
emit __CoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',43);
      emit __StatementCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',13);
capital = capital - f.valor;
    } else {emit __BranchCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',4,1);
emit __CoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',45);
      emit __StatementCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',14);
f.aprovado = false;
    }
emit __CoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',47);
    emit __StatementCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',15);
f.gerente = msg.sender;
emit __CoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',48);
    emit __StatementCoverageLinhaFinanciamento('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/linha-financiamento-dapp/contracts/LinhaFinanciamento.sol',16);
financiamentos[indiceArray] = f;
  }
}
