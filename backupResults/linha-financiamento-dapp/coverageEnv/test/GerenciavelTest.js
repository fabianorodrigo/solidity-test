const truffleAssert = require('truffle-assertions');
const Gerenciavel = artifacts.require("Gerenciavel");
const LinhaFinanciamento = artifacts.require("LinhaFinanciamento");

contract("Gerenciavel",(accounts)=>{
  let trace = false;
  let contractGerenciavel = null;
  let contractLinhaFinanciamento = null;
  beforeEach(async () => {
    contractGerenciavel = await Gerenciavel.new({from:accounts[0]});
    if(trace) console.log('SUCESSO: Gerenciavel.new({from:accounts[0]}');
    contractLinhaFinanciamento = await LinhaFinanciamento.new(0,{from:accounts[0]});
    if(trace) console.log('SUCESSO: LinhaFinanciamento.new(0,{from:accounts[0]}');
  });
  
  it('Should execute mudarGerente(address) WHEN msg.sender==gerente', async () => {
    //transactionParameter: [object Object]
    //Gerenciavel.gerente: accounts[0]
    let result = await contractGerenciavel.mudarGerente(accounts[8],{from: accounts[0]});
  });
  it('Should fail mudarGerente(address) when NOT comply with: msg.sender == gerente', async () => {
    //transactionParameter: [object Object]
    //Gerenciavel.gerente: accounts[0]
    let result = await truffleAssert.fails(contractGerenciavel.mudarGerente(accounts[8],{from: accounts[9]}),'revert');
  });
});
