const truffleAssert = require('truffle-assertions');
const Gerenciavel = artifacts.require("Gerenciavel");
const LinhaFinanciamento = artifacts.require("LinhaFinanciamento");

contract("LinhaFinanciamento",(accounts)=>{
  let trace = false;
  let contractGerenciavel = null;
  let contractLinhaFinanciamento = null;
  beforeEach(async () => {
    contractGerenciavel = await Gerenciavel.new({from:accounts[0]});
    if(trace) console.log('SUCESSO: Gerenciavel.new({from:accounts[0]}');
    contractLinhaFinanciamento = await LinhaFinanciamento.new(0,{from:accounts[0]});
    if(trace) console.log('SUCESSO: LinhaFinanciamento.new(0,{from:accounts[0]}');
  });
  
  it('Should execute solicitar(uint8,uint256) WHEN msg.sender!=gerente,idade>=18', async () => {
    //transactionParameter: [object Object]
    //LinhaFinanciamento.capital: 0
    //LinhaFinanciamento.financiamentos: 
    //LinhaFinanciamento.gerente: accounts[0]
    await contractLinhaFinanciamento.mudarGerente(accounts[1],{from:accounts[0]});
    let result = await contractLinhaFinanciamento.solicitar(19, 15,{from: accounts[6]});
  });
  it('Should fail solicitar(uint8,uint256) when NOT comply with: msg.sender != gerente', async () => {
    //transactionParameter: [object Object]
    //LinhaFinanciamento.capital: 0
    //LinhaFinanciamento.financiamentos: 
    //LinhaFinanciamento.gerente: accounts[0]
    await contractLinhaFinanciamento.mudarGerente(accounts[1],{from:accounts[0]});
    let result = await truffleAssert.fails(contractLinhaFinanciamento.solicitar(19, 15,{from: accounts[1]}),'revert');
  });
  it('Should fail solicitar(uint8,uint256) when NOT comply with: idade >= 18', async () => {
    //transactionParameter: [object Object]
    //LinhaFinanciamento.capital: 0
    //LinhaFinanciamento.financiamentos: 
    //LinhaFinanciamento.gerente: accounts[0]
    await contractLinhaFinanciamento.mudarGerente(accounts[1],{from:accounts[0]});
    let result = await truffleAssert.fails(contractLinhaFinanciamento.solicitar(17, 15,{from: accounts[6]}),'revert');
  });
  it('Should execute deliberar(uint256,bool) WHEN MemberAccess.Identifier>65,aprovado==true,msg.sender==gerente,financiamentos.length>indiceArray,MemberAccess.stateVariableIndex==0x0000000000000000000000000000000000000000,MemberAccess.stateVariableIndex==0', async () => {
    //transactionParameter: [object Object]
    //LinhaFinanciamento.capital: 0
    //LinhaFinanciamento.financiamentos: 
    //LinhaFinanciamento.gerente: accounts[0]
    await contractLinhaFinanciamento.mudarGerente(accounts[6],{from:accounts[0]});
    await contractLinhaFinanciamento.solicitar(64,16,{from:accounts[8]});
    let result = await contractLinhaFinanciamento.deliberar(0, true,{from: accounts[6]});
  });
  it('Should fail deliberar(uint256,bool) when NOT comply with: msg.sender == gerente', async () => {
    //transactionParameter: [object Object]
    //LinhaFinanciamento.capital: 0
    //LinhaFinanciamento.financiamentos: 
    //LinhaFinanciamento.gerente: accounts[0]
    await contractLinhaFinanciamento.mudarGerente(accounts[6],{from:accounts[0]});
    await contractLinhaFinanciamento.solicitar(64,16,{from:accounts[8]});
    let result = await truffleAssert.fails(contractLinhaFinanciamento.deliberar(0, true,{from: accounts[9]}),'revert');
  });
  it('Should fail deliberar(uint256,bool) when NOT comply with: financiamentos.length > indiceArray', async () => {
    //transactionParameter: [object Object]
    //LinhaFinanciamento.capital: 0
    //LinhaFinanciamento.financiamentos: 
    //LinhaFinanciamento.gerente: accounts[0]
    await contractLinhaFinanciamento.mudarGerente(accounts[6],{from:accounts[0]});
    let result = await truffleAssert.fails(contractLinhaFinanciamento.deliberar(0, true,{from: accounts[6]}),'revert');
  });
  it('Should execute deliberar(uint256,bool) WHEN MemberAccess.Identifier<=65,aprovado==true,msg.sender==gerente,financiamentos.length>indiceArray,MemberAccess.stateVariableIndex==0x0000000000000000000000000000000000000000,MemberAccess.stateVariableIndex==0', async () => {
    //transactionParameter: [object Object]
    //LinhaFinanciamento.capital: 0
    //LinhaFinanciamento.financiamentos: 
    //LinhaFinanciamento.gerente: accounts[0]
    await contractLinhaFinanciamento.mudarGerente(accounts[7],{from:accounts[0]});
    await contractLinhaFinanciamento.solicitar(20,15,{from:accounts[0]});
    let result = await contractLinhaFinanciamento.deliberar(0, true,{from: accounts[7]});
  });
  it('Should fail deliberar(uint256,bool) when NOT comply with: msg.sender == gerente', async () => {
    //transactionParameter: [object Object]
    //LinhaFinanciamento.capital: 0
    //LinhaFinanciamento.financiamentos: 
    //LinhaFinanciamento.gerente: accounts[0]
    await contractLinhaFinanciamento.mudarGerente(accounts[7],{from:accounts[0]});
    await contractLinhaFinanciamento.solicitar(20,15,{from:accounts[0]});
    let result = await truffleAssert.fails(contractLinhaFinanciamento.deliberar(0, true,{from: accounts[9]}),'revert');
  });
  it('Should fail deliberar(uint256,bool) when NOT comply with: financiamentos.length > indiceArray', async () => {
    //transactionParameter: [object Object]
    //LinhaFinanciamento.capital: 0
    //LinhaFinanciamento.financiamentos: 
    //LinhaFinanciamento.gerente: accounts[0]
    await contractLinhaFinanciamento.mudarGerente(accounts[7],{from:accounts[0]});
    let result = await truffleAssert.fails(contractLinhaFinanciamento.deliberar(0, true,{from: accounts[7]}),'revert');
  });
  it('Should execute deliberar(uint256,bool) WHEN aprovado!=true,msg.sender==gerente,financiamentos.length>indiceArray,MemberAccess.stateVariableIndex==0x0000000000000000000000000000000000000000,MemberAccess.stateVariableIndex==0', async () => {
    //transactionParameter: [object Object]
    //LinhaFinanciamento.capital: 0
    //LinhaFinanciamento.financiamentos: 
    //LinhaFinanciamento.gerente: accounts[0]
    await contractLinhaFinanciamento.mudarGerente(accounts[8],{from:accounts[0]});
    await contractLinhaFinanciamento.solicitar(19,1,{from:accounts[5]});
    let result = await contractLinhaFinanciamento.deliberar(0, false,{from: accounts[8]});
  });
  it('Should fail deliberar(uint256,bool) when NOT comply with: msg.sender == gerente', async () => {
    //transactionParameter: [object Object]
    //LinhaFinanciamento.capital: 0
    //LinhaFinanciamento.financiamentos: 
    //LinhaFinanciamento.gerente: accounts[0]
    await contractLinhaFinanciamento.mudarGerente(accounts[8],{from:accounts[0]});
    await contractLinhaFinanciamento.solicitar(19,1,{from:accounts[5]});
    let result = await truffleAssert.fails(contractLinhaFinanciamento.deliberar(0, false,{from: accounts[9]}),'revert');
  });
  it('Should fail deliberar(uint256,bool) when NOT comply with: financiamentos.length > indiceArray', async () => {
    //transactionParameter: [object Object]
    //LinhaFinanciamento.capital: 0
    //LinhaFinanciamento.financiamentos: 
    //LinhaFinanciamento.gerente: accounts[0]
    await contractLinhaFinanciamento.mudarGerente(accounts[8],{from:accounts[0]});
    let result = await truffleAssert.fails(contractLinhaFinanciamento.deliberar(0, false,{from: accounts[8]}),'revert');
  });
});
