pragma experimental ABIEncoderV2;
import "../MerkleShip.sol";

contract ProxyMerkleShip  is MerkleShip  {event __CoverageProxyMerkleShip(string fileName, uint256 lineNumber);
event __FunctionCoverageProxyMerkleShip(string fileName, uint256 fnId);
event __StatementCoverageProxyMerkleShip(string fileName, uint256 statementId);
event __BranchCoverageProxyMerkleShip(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageProxyMerkleShip(string fileName, uint256 branchId);
event __AssertPostCoverageProxyMerkleShip(string fileName, uint256 branchId);


       function test_reveal(uint32  _id, bytes32[6] memory _proof, string memory _leafData) public  {emit __FunctionCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',1);

emit __CoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',7);
    emit __StatementCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',1);
_reveal(_id,_proof,_leafData);
   }

   function test_checkForVictoryByHit(uint32  _id) public  {emit __FunctionCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',2);

emit __CoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',11);
    emit __StatementCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',2);
_checkForVictoryByHit(_id);
   }

   function test_zeroOutStorage(uint32  _id) public  {emit __FunctionCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',3);

emit __CoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',15);
    emit __StatementCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',3);
_zeroOutStorage(_id);
   }

   function test_checkShipCount(string[64] memory _data) public      returns (bool ){emit __FunctionCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',4);

emit __CoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',19);
    emit __StatementCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',4);
return _checkShipCount(_data);
   }

   function test_checkShipLength(string[64] memory _data) public      returns (bool ){emit __FunctionCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',5);

emit __CoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',23);
    emit __StatementCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',5);
return _checkShipLength(_data);
   }

   function test_hashEach(string[64] memory _data) public      returns (bytes32[] memory){emit __FunctionCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',6);

emit __CoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',27);
    emit __StatementCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',6);
return _hashEach(_data);
   }

   function test_sortArray(bytes32[] memory _data) public      returns (bytes32[] memory){emit __FunctionCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',7);

emit __CoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',31);
    emit __StatementCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',7);
return _sortArray(_data);
   }

   function test_quickSort(bytes32[] memory _arr, int  _left, int  _right) public      {emit __FunctionCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',8);

emit __CoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',35);
    emit __StatementCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',8);
_quickSort(_arr,_left,_right);
   }

   function test_computeMerkleTree(bytes32[] memory _data) public      returns (bytes32 ){emit __FunctionCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',9);

emit __CoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',39);
    emit __StatementCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',9);
return _computeMerkleTree(_data);
   }

   function test_verifyMerkleProof(bytes32[6] memory _proof, bytes32  _root, string memory _leafData) public      returns (bool ){emit __FunctionCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',10);

emit __CoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',43);
    emit __StatementCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',10);
return _verifyMerkleProof(_proof,_root,_leafData);
   }

   function test_isCoordinateValid(uint8  _x, uint8  _y) public      returns (bool ){emit __FunctionCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',11);

emit __CoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',47);
    emit __StatementCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',11);
return _isCoordinateValid(_x,_y);
   }

   function test_coordinateToIndex(uint8  _x, uint8  _y) public      returns (uint8 ){emit __FunctionCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',12);

emit __CoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',51);
    emit __StatementCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',12);
return _coordinateToIndex(_x,_y);
   }

   function test_subString(string memory _inputStr, uint256  _index) public      returns (bytes1 ){emit __FunctionCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',13);

emit __CoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',55);
    emit __StatementCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',13);
return _subString(_inputStr,_index);
   }

   function test_isStringValid(string memory _inputStr) public      returns (bool ){emit __FunctionCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',14);

emit __CoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',59);
    emit __StatementCoverageProxyMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/proxy4tests/MerkleShip.sol',14);
return _isStringValid(_inputStr);
   }


}