pragma solidity ^0.5.0;
/** ABIEncoderV2 required for arguments with string arrays */
/** ignore compiler warnings about this feature (yolo) */
pragma experimental ABIEncoderV2; 

import "../contracts/SafeMath.sol";

/** @title MerkleShip */
contract MerkleShip {event __CoverageMerkleShip(string fileName, uint256 lineNumber);
event __FunctionCoverageMerkleShip(string fileName, uint256 fnId);
event __StatementCoverageMerkleShip(string fileName, uint256 statementId);
event __BranchCoverageMerkleShip(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageMerkleShip(string fileName, uint256 branchId);
event __AssertPostCoverageMerkleShip(string fileName, uint256 branchId);


  using SafeMath for *;

  ///////////////////
  //STATE VARIABLES//
  ///////////////////

  /** toggle for emergency circuit */
  bool public isStopped = false;
  /** admin address; cannot receive any ether */
  address public admin;
  /** global game id count */
  uint32 public gameCount;
  /** max turn length before a game is considered abandoned */ 
  uint32 public abandonThreshold = 48 hours;
  /** board width */
  uint8 public rows = 8;
  /** board height */
  uint8 public columns = 8;
  /** successful hits required for victory */
  uint8 public hitThreshold = 12;
  /** storage of game state by id */
  mapping (uint32 => Game) public games;
  /** ETH available for user withdrawal */
  mapping (address => uint256) public userBalance;
  /** separate timer to track VictoryPending claim time */
  mapping (uint32 => uint256) public claimTimer;

  /** eight possible game states */
  enum GameState { Ready, Cancelled, Active, Abandoned, VictoryPending, VictoryChallenged, Complete, Zeroed }
  /** three possible turn states */
  enum Turn { Inactive, PlayerA, PlayerB }
  /** four possible guess states */
  enum GuessState { Unknown, Pending, Hit, Miss }

  /** @dev primary storage of game state
   * @dev the variables up to playerAMerkleRoot maximize storage by packing tightly into three 256-bit words 
   */
  struct Game {
    /** global gameCount; will overflow if there are more than 4,294,967,295 games  */
    uint32 id; 
    /** turn start time; reset at start of each turn; will overflow February 7, 2106 */
    uint32 turnStartTime; 
    /** wager from one player (total prize is wager * 2) */
    /** cannot overflow as max uint96 is larger than total ETH supply unless vitalik goes wild */
    uint96 wager; 
    /** player who proposed the game */
    address payable playerA;
    /** player who accepted the game */
    address payable playerB;
    /** winner only set on game resolution */
    address payable winner;
    /** tracks number of hits on player A's board */
    uint8 playerAhitCount;
    /** tracks number of hits on player B's board */
    uint8 playerBhitCount;
    /** game state; takes 8 bits of storage */
    GameState state; 
    /** turn state; takes 8 bits of storage */
    Turn turn; 
    /** merkle root of playerA secret game board */
    bytes32 playerAMerkleRoot;
    /** merkle root of playerB secret game board */
    bytes32 playerBMerkleRoot;
    /** index of player A guess states */
    mapping (uint8 => GuessState) playerAguesses;  
    /** index of player B guess states */
    mapping (uint8 => GuessState) playerBguesses;  
    /** tracks square index of player A guesses; length property used to retrieve most recent guess */
    uint8[] playerAsquaresGuessed; 
    /** tracks square index of player B guesses; length property used to retrieve most recent guess */
    uint8[] playerBsquaresGuessed; 
  }

  /////////////
  //MODIFIERS//
  /////////////

  /** @dev controls whether it is the correct turn of the player attempting to call a game function
   * @dev blocks players who are not part of the game
   * @param _id Game ID
   */
  modifier turnControl(uint32 _id) {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1);
 
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',93);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1);
if (games[_id].turn == Turn.PlayerA) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',94);
      emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',2);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',2);
require (
        msg.sender == games[_id].playerA,
        "it must be your turn");emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',2);

    } 
    else {emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',3);
emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1,1);if (games[_id].turn == Turn.PlayerB) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',3,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',99);
      emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',4);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',4);
require (
        msg.sender == games[_id].playerB,
        "it must be your turn");emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',4);

    }else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',3,1);}
} 
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',103);
    _; 
  }

  /** @dev limits engagement to active games
   * @param _id Game ID
   */
  modifier isActive(uint32 _id) {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',2);
 
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',110);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',5);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',5);
require (
      games[_id].state == GameState.Active,
      "the game must be active");emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',5);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',113);
    _;
  }
  
  /** @dev limits engagement to games that have been proposed but are not yet active
   * @param _id Game ID
   */
  modifier isReady(uint32 _id) {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',3);
 
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',120);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',6);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',6);
require (
      games[_id].state == GameState.Ready,
      "the game must be waiting for a player");emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',6);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',123);
    _;
  }

  /** @dev blocks players who are not part of the game
   * @dev turn agnostic
   * @param _id Game ID
   */
  modifier isPlayer(uint32 _id) {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',4);
 
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',131);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',7);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',7);
require (
      msg.sender == games[_id].playerA || msg.sender == games[_id].playerB,
      "you must be a valid player");emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',7);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',134);
    _;
  }

  /** @dev circuit-breaker that blocks most functionality during an emergency
   */
  modifier notEmergency() {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',5);
 
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',140);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',8);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',8);
require (isStopped == false,
    "you can only use this function in a non-emergency");emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',8);
 
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',142);
    _; 
  }

  /** @dev restricts functions that can only be used in an emergency
   */
  modifier onlyEmergency() {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',6);
 
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',148);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',9);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',9);
require (isStopped == true,
    "you can only use this function in an emergency");emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',9);
 
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',150);
    _; 
  }
  
  /** @dev restricts functions that can accessed by the admin
   */
  modifier onlyAdmin() {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',7);
 
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',156);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',10);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',10);
require (msg.sender == admin,
    "you must be the admin");emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',10);
 
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',158);
    _; 
  }

  ///////////////
  //CONSTRUCTOR//
  ///////////////

  constructor() public {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',8);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',166);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',11);
admin = msg.sender;
  }

  //////////
  //EVENTS//
  //////////

  event LogProposedGame(
    uint256 indexed gameID,
    address indexed playerA,
    uint256 wager
  );

  event LogGameAccepted(
    uint256 indexed gameID,
    address indexed playerB,
    uint256 wager
  );

  event LogGameCancelled(
    uint256 indexed gameID,
    address indexed playerB
  );

  event LogSmackTalk(
    uint256 indexed gameID,
    address indexed sender,
    string smackTalk
  );

  event LogUserWithdraw(
    address indexed user,
    uint256 amount
  );
  
  event LogGuess(
    uint256 indexed gameID,
    address indexed user,
    uint256 square
  ); 

  event LogReveal(
    uint256 indexed gameID,
    address indexed user,
    bool verfied,
    uint256 square,
    bool isHit
  );

  event LogVictoryPending(
    uint256 indexed gameID,
    address indexed winner
  );

  event LogVictoryChallenged(
    uint256 indexed gameID,
    address indexed challenger
  );

  event LogWinner(
    uint256 indexed gameID,
    address indexed winner,
    string message
  );

  event LogEmergency(
    string message
  );

  //////////////////
  //ADMIN FUNCTION//
  //////////////////

  /** @dev emergency stop function to bring all games to safe halt 
   * @dev can only be called once and cannot be reversed
   * @param _message optional message to explain stoppage
   */
  function emergencyStop(string calldata _message) 
    external
    onlyAdmin()
    notEmergency()
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',9);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',248);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',12);
isStopped = true;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',249);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',13);
emit LogEmergency(_message);
  }

  //////////////////////
  //PRE-GAME FUNCTIONS//
  //////////////////////

  /**
   * @dev propose a new game
   * @param _wager amount in wei (0 is a valid amount)
   * @param  _playerAMerkleRoot merkle root of initial game state
   */
  function proposeGame(uint96 _wager, bytes32 _playerAMerkleRoot) 
    external 
    payable 
    notEmergency()
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',10);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',266);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',11);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',14);
require (msg.value == _wager, "you must send the right amount");emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',11);

    /** increment gameCount; the first game is 1, not 0 */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',268);
    gameCount++;
    /** initialize struct in storage */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',270);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',15);
Game storage g = games[gameCount];
    /** save initial game state */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',272);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',16);
g.id = gameCount;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',273);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',17);
g.wager = _wager;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',274);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',18);
g.playerA = msg.sender;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',275);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',19);
g.state = GameState.Ready;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',276);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',20);
g.playerAMerkleRoot = _playerAMerkleRoot;

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',278);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',21);
emit LogProposedGame(gameCount, msg.sender, msg.value);
  }

  /**
   * @dev accept a proposed game
   * @param _id game id
   * @param  _playerBMerkleRoot merkle root of initial game state
   */
  function acceptGame(uint32 _id, bytes32 _playerBMerkleRoot) 
    external 
    payable 
    isReady(_id) 
    notEmergency()
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',11);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',292);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',12);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',22);
require (msg.value == games[_id].wager, "you must match the wager");emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',12);

    /** initialize struct from storage */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',294);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',23);
Game storage g = games[_id];
    /** update game state */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',296);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',24);
g.turnStartTime = uint32(now);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',297);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',25);
g.playerB = msg.sender;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',298);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',26);
g.state = GameState.Active;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',299);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',27);
g.turn = Turn.PlayerA;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',300);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',28);
g.playerBMerkleRoot = _playerBMerkleRoot;

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',302);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',29);
emit LogGameAccepted(_id, msg.sender, msg.value);
  }

  /**
   * @dev cancel a proposed game
   * @param _id game id
   */
  function cancelProposedGame(uint32 _id) 
    external 
    isReady(_id) 
    notEmergency()
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',12);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',314);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',13);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',30);
require (msg.sender == games[_id].playerA, "you must have proposed this game");emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',13);

    /** local variable of balance to return */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',316);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',31);
uint256 balanceToReturn = games[_id].wager;
    /** initialize struct from storage */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',318);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',32);
Game storage g = games[_id];
    /** update state and set wager amount to 0 */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',320);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',33);
g.state = GameState.Cancelled;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',321);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',34);
g.wager = 0;
    /** update user balance */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',323);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',35);
userBalance[msg.sender] += balanceToReturn;

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',325);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',36);
emit LogGameCancelled(_id, msg.sender);
  }

  /////////////////////
  //IN-GAME FUNCTIONS//
  /////////////////////

  /** @dev submit a guess and reveal result of other player's previous guess
   * @param _id game id
   * @param _square x,y coordinates of guess
   * @param _proof merkle proof which consists of six hashes 
   * @param _leafData unhashed leaf being revealed (concatenated string in specified format)
   * @param _smackTalk optional message to other player
   */
  function guessAndReveal(
    uint32 _id, 
    uint8[2] memory _square, 
    bytes32[6] memory _proof, 
    string memory _leafData,
    string memory _smackTalk
    ) 
    public 
    turnControl(_id) 
    isActive(_id) 
    notEmergency()
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',13);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',351);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',14);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',37);
require (_isStringValid(_smackTalk) || bytes(_smackTalk).length == 0, "smack talk must be a valid string");emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',14);

    /** convert coordinates to index */
    /** this function also checks that the coordinate are inbounds */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',354);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',38);
uint8 square = _coordinateToIndex(_square[0], _square[1]);
    /** initialize struct from storage */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',356);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',39);
Game storage g = games[_id];
    /** process reveal and update state with playerA guess */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',358);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',40);
if (msg.sender == games[_id].playerA) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',15,0);
      /** nothing to reveal on playerA's first turn */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',360);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',41);
if (g.playerBsquaresGuessed.length > 0) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',16,0);
        /** reveal square from playerB's previous guess */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',362);
        emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',42);
_reveal(_id, _proof, _leafData);
      }else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',16,1);}

      /** guess coordinates stored in sequential array */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',365);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',43);
g.playerAsquaresGuessed.push(square);
      /** guess state pending until revealed */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',367);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',44);
g.playerAguesses[uint8(g.playerBsquaresGuessed.length)] = GuessState.Pending;
      /** reset turn start time */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',369);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',45);
g.turnStartTime = uint32(now);
      /** update turn state  */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',371);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',46);
g.turn = Turn.PlayerB;
    } 
    /** process reveal and update state with playerB guess */
    else {emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',47);
emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',15,1);if (msg.sender == games[_id].playerB) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',17,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',375);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',48);
_reveal(_id, _proof, _leafData);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',376);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',49);
g.playerBsquaresGuessed.push(square);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',377);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',50);
g.playerAguesses[uint8(g.playerAsquaresGuessed.length)] = GuessState.Pending;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',378);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',51);
g.turnStartTime = uint32(now);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',379);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',52);
g.turn = Turn.PlayerA;
    }else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',17,1);}
}

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',382);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',53);
emit LogGuess(_id, msg.sender, square);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',383);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',54);
emit LogSmackTalk(_id, msg.sender, _smackTalk);
  } 

  /** @dev validate and reveal the previous guess
   * @param _id game id
   * @param _proof merkle proof with six hashes 
   * @param _leafData unhashed leaf being revealed (concatenated string in specified format)
   */
  function _reveal(
    uint32 _id, 
    bytes32[6] memory _proof, 
    string memory _leafData
  ) 
    internal 
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',14);
    
    /** initialize game from storage */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',399);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',55);
Game storage g = games[_id];
    /** retreive appropriate merkle root from game state  */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',401);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',56);
bytes32 root;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',402);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',57);
if (msg.sender == games[_id].playerA) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',18,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',403);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',58);
root = g.playerAMerkleRoot;
    } 
    else {emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',59);
emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',18,1);if (msg.sender == games[_id].playerB) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',19,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',406);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',60);
root = g.playerBMerkleRoot;
    }else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',19,1);}
}
    /** reveal and verify merkle proof from the other player's last guess */
    /** if the player calling this function can't provide the verification they will not be able to advance their turn */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',410);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',20);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',61);
require (
      _verifyMerkleProof(_proof, root, _leafData) == true, 
      "you must provide a valid Merkle Proof"
    );emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',20);

    /** retrieve index of square about to be revealed */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',415);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',62);
uint8 guessToReveal;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',416);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',63);
if (msg.sender == games[_id].playerA) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',21,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',417);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',64);
guessToReveal = uint8(g.playerBsquaresGuessed[g.playerBsquaresGuessed.length.sub(1)]);
    } 
    else {emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',65);
emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',21,1);if (msg.sender == games[_id].playerB) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',22,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',420);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',66);
guessToReveal = uint8(g.playerAsquaresGuessed[g.playerAsquaresGuessed.length.sub(1)]);
    }else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',22,1);}
}
    /** check the first byte of the revealed data to confirm if there was a ship in that square */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',423);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',67);
bytes1 isHit = _subString(_leafData, 0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',424);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',23);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',68);
require (isHit == 0x31 || isHit == 0x30, "leaf data must be in correct format");emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',23);

    /** update state if there was a hit */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',426);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',69);
bool hit;
    /** 0x31 is the character "1" in bytes1 */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',428);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',70);
if (isHit == 0x31) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',24,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',429);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',71);
hit = true;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',430);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',72);
if (msg.sender == games[_id].playerA) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',25,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',431);
        emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',73);
g.playerBguesses[guessToReveal] = GuessState.Hit;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',432);
        g.playerBhitCount++;
      } 
      else {emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',74);
emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',25,1);if (msg.sender == games[_id].playerB) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',26,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',435);
        emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',75);
g.playerAguesses[guessToReveal] = GuessState.Hit;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',436);
        g.playerAhitCount++;
      }else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',26,1);}
}
    } 
    /** update state if there was a miss */
    /** 0x30 is the character "0" in bytes1 */
    else {emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',76);
emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',24,1);if (isHit == 0x30) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',27,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',442);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',77);
hit = false;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',443);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',78);
if (msg.sender == games[_id].playerA) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',28,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',444);
        emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',79);
g.playerBguesses[guessToReveal] = GuessState.Miss;
      } 
      else {emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',80);
emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',28,1);if (msg.sender == games[_id].playerB) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',29,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',447);
        emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',81);
g.playerAguesses[guessToReveal] = GuessState.Miss;
      }else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',29,1);}
}
    }else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',27,1);}
} 

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',451);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',82);
_checkForVictoryByHit(_id);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',453);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',83);
emit LogReveal(_id, msg.sender, true, guessToReveal, hit);
  }

  /** @dev check if a victory by hit was achieved
   * @param _id game id
   */
  function _checkForVictoryByHit(uint32 _id) 
    internal
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',15);

    /** initialize game from storage */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',463);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',84);
Game storage g = games[_id];
    /** check for winner */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',465);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',85);
if (g.playerAhitCount == hitThreshold) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',30,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',466);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',86);
g.state = GameState.VictoryPending;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',467);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',87);
g.winner = games[_id].playerB;
    } 
    else {emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',88);
emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',30,1);if (g.playerBhitCount == hitThreshold) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',31,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',470);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',89);
g.state = GameState.VictoryPending;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',471);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',90);
g.winner = games[_id].playerA;
    }else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',31,1);}
} 
    /** process winner */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',474);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',91);
if (g.state == GameState.VictoryPending) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',32,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',475);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',92);
claimTimer[_id] = now;
      
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',477);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',93);
emit LogVictoryPending(_id, msg.sender);
    }else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',32,1);}

  }

  /** @dev can be called to resolve a game abandoned by the other player
   * @param _id game id
   */
  function resolveAbandonedGame(uint32 _id) 
    external 
    isActive(_id)
    isPlayer(_id) 
    notEmergency()
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',16);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',490);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',33);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',94);
require (
      now >= games[_id].turnStartTime + abandonThreshold,
      "the game must be stale for at least 48 hours"
    );emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',33);
 

    /** can only be called if it is NOT your turn */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',496);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',95);
if (msg.sender == games[_id].playerA) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',34,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',497);
      emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',35);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',96);
require (games[_id].turn == Turn.PlayerB, "it must be the other player's turn");emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',35);

    } 
    else {emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',97);
emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',34,1);if (msg.sender == games[_id].playerB) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',36,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',500);
      emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',37);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',98);
require (games[_id].turn == Turn.PlayerA, "it must be the other player's turn");emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',37);

    }else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',36,1);}
}
    /** initialize struct from storage */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',503);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',99);
Game storage g = games[_id];
    /** update game state */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',505);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',100);
g.state = GameState.Abandoned;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',506);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',101);
g.winner = msg.sender;
    /** calcuate prize */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',508);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',102);
uint prize = g.wager * 2;
    /** zero out wager just in case */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',510);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',103);
g.wager = 0;
    /** update winner balance */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',512);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',104);
userBalance[msg.sender] += prize;

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',514);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',105);
emit LogWinner(_id, g.winner, "victory by abandonment");
  }

  /** @dev either player can call this function to concede
   * @dev the loser recovers 20% of their wager, incentivizing quick resolution of games
   * @param _id game id
   */
  function concedeGame(uint32 _id) 
    external 
    isActive(_id) 
    isPlayer(_id) 
    notEmergency()
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',17);
 
    /** assign loser status to player conceding */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',528);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',106);
address payable loser = msg.sender;
    /** assign winner status to other player */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',530);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',107);
address payable winner;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',531);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',108);
if (msg.sender == games[_id].playerA) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',38,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',532);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',109);
winner = games[_id].playerB;
    } 
    else {emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',110);
emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',38,1);if (msg.sender == games[_id].playerB) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',39,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',535);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',111);
winner = games[_id].playerA;
    }else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',39,1);}
}
    /** initialize struct in storage */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',538);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',112);
Game storage g = games[_id];
    /** update game state */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',540);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',113);
g.state = GameState.Complete;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',541);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',114);
g.winner = winner;
    /** calculate winner prize (90% of pot) */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',543);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',115);
uint256 prize = (g.wager.mul(180)).div(100);
    /** calculate loser prize (10% of pot) */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',545);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',116);
uint256 concession = g.wager.mul(2).sub(prize);
    /** zero out wager */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',547);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',117);
g.wager = 0;
    /** update user balances */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',549);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',118);
userBalance[winner] += prize;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',550);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',119);
userBalance[loser] += concession;

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',552);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',120);
emit LogWinner(_id, g.winner, "victory by concession");
  }

  /** @dev players can challenge pending victories, requiring full validation of starting game state
   * @dev on-chain reconstruction and validation of the full proof is expensive and makes up ~30% of the code in this contract
   * @dev this is all to handle to the single edge case of a player who computes a merkle root based on a dishonest initial game state
   * @param _id game id
   */
  function challengeVictory(uint32 _id)
    external
    isPlayer(_id)
    notEmergency()
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',18);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',565);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',121);
Game storage g = games[_id];

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',567);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',40);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',122);
require (g.state == GameState.VictoryPending, "this game must be pending proof of honest play");emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',40);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',568);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',41);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',123);
require (msg.sender != g.winner);emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',41);


emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',570);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',124);
claimTimer[_id] = now;
    
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',572);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',125);
g.state = GameState.VictoryChallenged;

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',574);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',126);
emit LogVictoryChallenged(_id, msg.sender);
  }

  /** @dev if challenge, the proposed victor must provide and validate a full proof of their starting game state
   * @dev on-chain reconstruction and validation of the full proof is expensive and makes up ~30% of the code in this contract
   * @dev this is all to handle to the single edge case of a player who computes a merkle root based on a dishonest initial game state
   * @param _id game id
   * @param _leafData full unhashed array of starting game state for on-chain validation
   */
  function answerChallenge(uint32 _id, string[64] memory _leafData) 
    public
    isPlayer(_id) 
    notEmergency()
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',19);

    /** initialize struct from storage */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',589);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',127);
Game storage g = games[_id];

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',591);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',42);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',128);
require (
      g.state == GameState.VictoryChallenged, 
      "this game must be in the VictoryChallenged state"
    );emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',42);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',595);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',43);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',129);
require (
      _checkShipCount(_leafData) == true, 
      "you must have set the correct number of ship squares"
    );emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',43);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',599);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',44);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',130);
require (
      _checkShipLength(_leafData) == true, 
      "you must have set the correct ship lengths "
    );emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',44);


emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',604);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',131);
bytes32 root;

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',606);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',132);
if (msg.sender == games[_id].playerA) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',45,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',607);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',133);
root = g.playerAMerkleRoot;
    } 
    else {emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',134);
emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',45,1);if (msg.sender == games[_id].playerB) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',46,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',610);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',135);
root = g.playerBMerkleRoot;
    }else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',46,1);}
}

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',613);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',47);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',136);
require (
      _computeMerkleTree(
        _sortArray(
          _hashEach(
            _leafData
          )
        )
      ) == root, 
      "the merkle root must match"
    );emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',47);


    /** calculate prize */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',625);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',137);
uint256 prize = g.wager * 2;
    /** zero out wager */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',627);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',138);
g.wager = 0;
    /** update winner balance */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',629);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',139);
userBalance[g.winner] += prize;

    /** offset gas cost of this process by zeroing out as much storage as possible */
    /** this takes advantage of gas refunds gained from setting non-zero storage values to zero */
    /** only valid winners can use this to offset gas costs */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',634);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',140);
_zeroOutStorage(_id);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',636);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',141);
emit LogWinner(_id, g.winner, "verified victory by hit count");
  }

  /** @dev helper function to reduce gas costs of full validation by earning storage refunds
   * @param _id game id
   */
  function _zeroOutStorage(uint32 _id)
    internal
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',20);

    /** initialize struct from storage */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',646);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',142);
Game storage g = games[_id]; 

    /** g.id maintained for history */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',649);
    delete g.turnStartTime;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',650);
    delete g.wager;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',651);
    delete g.playerA;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',652);
    delete g.playerB;
    /** g.winner maintained for history */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',654);
    delete g.playerAhitCount;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',655);
    delete g.playerBhitCount;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',656);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',143);
g.state = GameState.Zeroed;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',657);
    delete g.turn; 
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',658);
    delete g.playerAMerkleRoot;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',659);
    delete g.playerBMerkleRoot;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',660);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',144);
for (uint8 i = 0; i < g.playerAsquaresGuessed.length; i++) {
      /** loop required to delete individual mapping keys */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',662);
      delete g.playerAguesses[i]; 
    }
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',664);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',145);
for (uint8 i = 0; i < g.playerBsquaresGuessed.length; i++) {
      /** loop required to delete individual mapping keys */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',666);
      delete g.playerBguesses[i]; 
    } 
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',668);
    delete g.playerAsquaresGuessed; 
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',669);
    delete g.playerBsquaresGuessed; 
  }

  /** @dev function to claim abandoned game in VictoryPending state
   * @param _id game id
   */
  function resolveUnclaimedVictory(uint32 _id) 
    external
    isPlayer(_id) 
    notEmergency()
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',21);
 
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',680);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',146);
Game storage g = games[_id];

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',682);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',48);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',147);
require (
      now >= claimTimer[_id] + abandonThreshold, 
      "the potential victor has 48h to validate their claim"
    );emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',48);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',686);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',49);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',148);
require (
      g.state == GameState.VictoryPending, 
      "this game must be pending proof of honest play"
    );emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',49);

    
    /** change game state */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',692);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',149);
g.state = GameState.Complete;
    /** calculate prize */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',694);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',150);
uint256 prize = g.wager * 2;
    /** zero out wager */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',696);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',151);
g.wager = 0;
    /** update winner balance */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',698);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',152);
userBalance[g.winner] += prize;

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',700);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',153);
emit LogWinner(_id, g.winner, "victory by unchallenged hit count");
  }

  /** @dev function to claim unproven challenge in VictoryChallenged state
   * @param _id game id
   */
  function resolveUnansweredChallenge(uint32 _id) 
    external
    isPlayer(_id) 
    notEmergency()
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',22);
 
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',711);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',154);
Game storage g = games[_id];

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',713);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',50);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',155);
require (
      now >= claimTimer[_id] + abandonThreshold, 
      "the potential victor has 48h to validate their claim"
    );emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',50);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',717);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',51);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',156);
require (
      g.state == GameState.VictoryChallenged,
      "this game must be in VictoryChallenged state"
    );emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',51);


    /** change victor */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',723);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',157);
if (g.winner == games[_id].playerA) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',52,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',724);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',158);
g.winner = games[_id].playerB;
    } 
    else {emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',159);
emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',52,1);if (g.winner == games[_id].playerB) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',53,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',727);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',160);
g.winner = games[_id].playerA;
    }else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',53,1);}
} 

    /** change game state */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',731);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',161);
g.state = GameState.Complete;
    /** calculate prize */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',733);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',162);
uint256 prize = g.wager * 2;
    /** zero out wager */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',735);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',163);
g.wager = 0;
    /** update winner balance */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',737);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',164);
userBalance[g.winner] += prize;

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',739);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',165);
emit LogWinner(_id, g.winner, "victory by unanswered challenge");
  }
  
  /** @dev this function is only way ETH can leave the contract
   */
  function withdraw() 
    external 
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',23);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',747);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',54);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',166);
require (userBalance[msg.sender] > 0, "user must have a balance to withdraw");emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',54);

    /** set balance as local variable */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',749);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',167);
uint256 balance = userBalance[msg.sender];
    /** change state balance to 0 */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',751);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',168);
userBalance[msg.sender] = 0;
    /** safely transfer without potential for reentrancy */
    /** for smart contracts trying to change state in their fallback function or reverting transfers...don't ¯\_(ツ)_/¯ */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',754);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',169);
msg.sender.transfer(balance);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',756);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',170);
emit LogUserWithdraw(msg.sender, balance);
  }

  /** @dev emergencyResolve credits wagers in any game state so users can withdraw
   * @param _id game id
   */
  function emergencyResolve(uint32 _id)
    external
    onlyEmergency()
    isPlayer(_id) 
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',24);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',767);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',171);
Game storage g = games[_id];
    
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',769);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',172);
if (g.state == GameState.Ready) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',55,0);
      /** return staked funds */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',771);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',173);
g.state = GameState.Complete;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',772);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',174);
uint256 refund = g.wager;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',773);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',175);
g.wager = 0;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',774);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',176);
userBalance[g.playerA] += refund;
    } else {emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',177);
emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',55,1);if (
      g.state == GameState.Active || 
      g.state == GameState.VictoryPending ||
      g.state == GameState.VictoryChallenged
    ) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',56,0);
      /** split funds */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',781);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',178);
g.state = GameState.Complete;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',782);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',179);
uint256 refund = g.wager;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',783);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',180);
g.wager = 0;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',784);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',181);
userBalance[g.playerA] += refund;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',785);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',182);
userBalance[g.playerB] += refund;
    }else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',56,1);}
}

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',788);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',183);
emit LogWinner(_id, address(0), "game resolved in an emergency");  
  }

  //////////////////////////////
  //VALIDATE VICTORY FUNCTIONS//
  //////////////////////////////

  /** @dev the functions in this section exist exclusively to valid the full game state in the rare event a victory is challenged */

  /** @dev confirms whether the winner set the correct number of ships if their victory is challenged
   * @param _data array of initial state
   * @return true if condition met; false if not
   */
  function _checkShipCount(string[64] memory _data) 
    internal
        
    returns(bool)
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',25);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',806);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',184);
uint256 shipCount;

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',808);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',185);
for (uint256 i = 0; i < 64; i++) {
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',809);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',186);
if (_subString(_data[i], 0) == 0x31) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',57,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',810);
        shipCount++;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',811);
        emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',187);
if (shipCount == hitThreshold) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',58,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',812);
          emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',188);
return true;
        }else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',58,1);}

      }else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',57,1);}

    }
  }

  /** @dev confirms whether the winner set the correct lengths of ships if their victory is challenged
   * @param _data array of initial state
   * @return true if condition met; false if not
   */
  function _checkShipLength(string[64] memory _data) 
    internal
        
    returns(bool)
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',26);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',827);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',189);
uint256 longShipsVerfied;

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',829);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',190);
for (uint256 i = 0; i < 64; i++) {
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',830);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',191);
bytes1 shipLength = _subString(_data[i], 3);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',831);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',192);
bool fourFound;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',832);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',193);
bool threeFound;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',833);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',194);
bool twoFound;

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',835);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',195);
if (shipLength == 0x34 && fourFound == false) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',59,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',836);
        emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',60);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',196);
require (
          _subString(_data[i + 1], 3) == 0x34 &&
          _subString(_data[i + 2], 3) == 0x34 &&
          _subString(_data[i + 3], 3) == 0x34 ||
          _subString(_data[i + rows], 3) == 0x34 &&
          _subString(_data[i + 2 * rows], 3) == 0x34 &&
          _subString(_data[i + 3 * rows], 3) == 0x34,
          "you must have one ship that is four squares long" 
        );emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',60);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',845);
        longShipsVerfied++;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',846);
        emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',197);
fourFound = true;
      }else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',59,1);}


emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',849);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',198);
if (shipLength == 0x33 && threeFound == false) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',61,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',850);
        emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',62);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',199);
require (
          _subString(_data[i + 1], 3) == 0x33 &&
          _subString(_data[i + 2], 3) == 0x33 ||
          _subString(_data[i + rows], 3) == 0x33 &&
          _subString(_data[i + 2 * rows], 3) == 0x33,
          "you must have one ship that is three squares long" 
        );emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',62);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',857);
        longShipsVerfied++;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',858);
        emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',200);
threeFound = true;
      }else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',61,1);}


emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',861);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',201);
if (shipLength == 0x32 && twoFound == false) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',63,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',862);
        emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',64);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',202);
require (
          _subString(_data[i + 1], 3) == 0x32 ||
          _subString(_data[i + rows], 3) == 0x32,
          "you must have one ship that is two squares long" 
        );emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',64);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',867);
        longShipsVerfied++;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',868);
        emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',203);
twoFound = true;
      }else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',63,1);}


emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',871);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',204);
if (longShipsVerfied == 3) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',65,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',872);
        emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',205);
return true;
      }else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',65,1);}

    }
  }

  /** @dev helper function for recomputing merkle tree
   * @param _data unhashed leaves from initial game state
   * @return array of hashed leaves
   */
  function _hashEach(string[64] memory _data) 
    internal 
         
    returns(bytes32[] memory)
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',27);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',886);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',206);
bytes32[] memory arr = new bytes32[](64);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',887);
       emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',207);
for (uint i = 0; i < _data.length; i++) {
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',888);
           emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',208);
arr[i] = keccak256(abi.encodePacked(_data[i]));
       }

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',891);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',209);
return arr;
  } 

  /** @dev helper function for sorting merkle tree leaves
   * @param _data hashed leaves
   * @return array of sorted leaves
   */
  function _sortArray(bytes32[] memory _data) 
    internal 
         
    returns(bytes32[] memory) 
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',28);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',903);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',210);
_quickSort(_data, int(0), int(_data.length.sub(1)));
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',904);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',211);
return _data;
  }
  
  /** @dev helper function for sorting merkle tree leaves
   * @dev based on https://gist.github.com/subhodi/b3b86cc13ad2636420963e692a4d896f
   * @param _arr hashed leaves
   * @param _left initial left-most array index
   * @param _right initial right-most array index
   * @return array of sorted leaves
   */
  function _quickSort(bytes32[] memory _arr, int _left, int _right) 
    internal 
         
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',29);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',918);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',212);
int i = _left;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',919);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',213);
int j = _right;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',920);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',214);
if(i==j) {emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',215);
emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',66,0);return;}else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',66,1);}

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',921);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',216);
bytes32 pivot = _arr[uint(_left + (_right - _left) / 2)];
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',922);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',217);
while (i <= j) {
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',923);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',218);
while (_arr[uint(i)] < pivot) {i++;}
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',924);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',219);
while (pivot < _arr[uint(j)]) {j--;}
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',925);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',220);
if (i <= j) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',67,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',926);
        emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',221);
(_arr[uint(i)], _arr[uint(j)]) = (_arr[uint(j)], _arr[uint(i)]);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',927);
        i++;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',928);
        j--;
      }else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',67,1);}

    }
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',931);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',222);
if (_left < j)
      {emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',223);
emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',68,0);emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',932);
_quickSort(_arr, _left, j);}else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',68,1);}

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',933);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',224);
if (i < _right)
      {emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',225);
emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',69,0);emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',934);
_quickSort(_arr, i, _right);}else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',69,1);}

  }

  /** @dev recompute merkle tree
   * @param _data sorted hashed leaves
   * @return merkle root
   */
  function _computeMerkleTree(bytes32[] memory _data) 
    internal 
         
    returns(bytes32) 
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',30);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',946);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',70);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',226);
require (_data.length.mod(2) == 0, "even sets only");emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',70);


emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',948);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',227);
if (_data.length >= 2) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',71,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',949);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',228);
bytes32[] memory newData = new bytes32[](_data.length / 2);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',950);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',229);
uint256 j = 0;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',951);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',230);
for (uint256 i = 0; i < _data.length; i+=2) {
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',952);
        emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',231);
newData[j] = keccak256(abi.encodePacked(_data[i], _data[i+1]));
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',953);
        j++;
      }
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',955);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',232);
if (newData.length > 2) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',72,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',956);
        emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',233);
return _computeMerkleTree(newData);
      }
      else {emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',234);
emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',72,1);if (newData.length == 2) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',73,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',959);
        emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',235);
return keccak256(abi.encodePacked(newData[0], newData[1]));
      }else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',73,1);}
}
    } 
    else {emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',236);
emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',71,1);if (_data.length == 2) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',74,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',963);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',237);
return keccak256(abi.encodePacked(_data[0], _data[1]));
    }else { emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',74,1);}
}
  }

  //////////////////
  //VIEW FUNCTIONS//
  //////////////////

  /** @dev returns contract balance
   * @return the current balance of the contract in wei
   */
  function getBalance() 
    external
        
    returns(uint256)
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',31);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',979);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',238);
return address(this).balance;
  }

  /////////////
  //UTILITIES//
  /////////////

  /** @dev verifies merkle proof against stored root
   * @dev requires the merkle tree be a sorted, balanced array of hashed leaves
   * @dev adapted from https://github.com/OpenZeppelin/openzeppelin-solidity/blob/master/contracts/cryptography/MerkleProof.sol
   * @param _proof six hash merkle proof
   * @param _root merkle root saved in game storage
   * @param _leafData unhashed leaf data being revealed
   * @return true if proof is validated; false if not
   */
  function _verifyMerkleProof(
    bytes32[6] memory _proof, 
    bytes32 _root, 
    string memory _leafData
  ) 
    internal 
         
    returns(bool) 
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',32);

    /** hash leaf data */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1004);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',239);
bytes32 computedHash = keccak256(abi.encodePacked(_leafData));
    /** loop through proof to compute merkle root */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1006);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',240);
for (uint256 i = 0; i < _proof.length; i++) {
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1007);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',241);
bytes32 proofElement = _proof[i];
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1008);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',242);
computedHash = keccak256(abi.encodePacked(proofElement, computedHash));
    }
    /** return true if proof matches stored merkle roof */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1011);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',243);
return computedHash == _root;
  }

  /** @dev utility function to check if coordinates are within a valid game board
   * @param _x x coordinate
   * @param _y y coordinate
   * @return true if coordinates are valid; false if not
   */
  function _isCoordinateValid(uint8 _x, uint8 _y) 
    internal 
         
    returns(bool) 
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',33);
 
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1024);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',244);
if (_x <= columns - 1 && _y <= rows - 1) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',75,0);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1025);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',245);
return true;
    } 
    else {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',75,1);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1028);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',246);
return false;
    }
  }

  /** @dev utility function to convert coordinate to array index
   * @param _x x coordinate
   * @param _y y coordinate
   * @return index from 0-63
   */
  function _coordinateToIndex(uint8 _x, uint8 _y) 
    internal 
         
    returns(uint8) 
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',34);

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1042);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',76);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',247);
require (_isCoordinateValid(_x, _y), "coordinate must be valid");emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',76);

    /** move starting index from 0 to 1 so multiplication works properly */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1044);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',248);
uint8 xShifted = _x + 1;
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1045);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',249);
uint8 yShifted = _y + 1;

emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1047);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',250);
if (yShifted > 1) {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',77,0);
      /** if not in first row, multiply by row height and add reshifted x value */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1049);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',251);
return uint8(yShifted * columns + xShifted - 1);
    } 
    else {emit __BranchCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',77,1);
      /** if in first row, return reshifted x value */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1053);
      emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',252);
return uint8(xShifted - 1);
    }
  }

 /** @dev utility function to return a substring
   * @param _inputStr input string
   * @param _index index of desired substring character
   * @return one byte substring
   */
  function _subString(string memory _inputStr, uint256 _index) 
    internal 
         
    returns(bytes1) 
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',35);
 
    /** convert to bytes to access substring index */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1068);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',253);
bytes memory _str = bytes(_inputStr);
    /** return first character */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1070);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',254);
return _str[_index];
  }

  /** @dev utility function to make sure no one xss attacks the front end with invalid strings
   * @param _inputStr input string
   * @return true if string is valid; false if not
   */
  function _isStringValid(string memory _inputStr)
    internal 
        
    returns(bool)
  {emit __FunctionCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',36);
 
    /** convert to bytes to access length property */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1083);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',255);
bytes memory _str = bytes(_inputStr);
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1084);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',256);
uint256 _length = _str.length;
    
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1086);
    emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',78);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',257);
require (_length <= 40,"string cannot be longer than 40 characters");emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',78);

    /** check each character */
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1088);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',258);
for (uint256 i = 0; i < _length; i++) {
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1089);
      emit __AssertPreCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',79);
emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',259);
require ( 
        /** allowed: a-z lowercase && " " (a normal space character) */
        (_str[i] > 0x60 && _str[i] < 0x7b) || _str[i] == 0x20,
        "string contains invalid characters"
      );emit __AssertPostCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',79);

    }
    
emit __CoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',1096);
    emit __StatementCoverageMerkleShip('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/MerkleShip.sol',260);
return true;
  }
}
