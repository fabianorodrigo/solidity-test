pragma solidity ^0.5.0;

/**
 * @title SafeMath
 * @dev Unsigned math operations with safety checks that revert on error
 */
library SafeMath {event __CoverageSafeMath(string fileName, uint256 lineNumber);
event __FunctionCoverageSafeMath(string fileName, uint256 fnId);
event __StatementCoverageSafeMath(string fileName, uint256 statementId);
event __BranchCoverageSafeMath(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageSafeMath(string fileName, uint256 branchId);
event __AssertPostCoverageSafeMath(string fileName, uint256 branchId);

    /**
     * @dev Multiplies two unsigned integers, reverts on overflow.
     */
    function mul(uint256 a, uint256 b) internal      returns (uint256) {emit __FunctionCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',1);

        // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
        // benefit is lost if 'b' is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-solidity/pull/522
emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',15);
        emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',1);
if (a == 0) {emit __BranchCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',1,0);
emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',16);
            emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',2);
return 0;
        }else { emit __BranchCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',1,1);}


emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',19);
        emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',3);
uint256 c = a * b;
emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',20);
        emit __AssertPreCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',2);
emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',4);
require(c / a == b);emit __AssertPostCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',2);


emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',22);
        emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',5);
return c;
    }

    /**
     * @dev Integer division of two unsigned integers truncating the quotient, reverts on division by zero.
     */
    function div(uint256 a, uint256 b) internal      returns (uint256) {emit __FunctionCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',2);

        // Solidity only automatically asserts when dividing by 0
emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',30);
        emit __AssertPreCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',3);
emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',6);
require(b > 0);emit __AssertPostCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',3);

emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',31);
        emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',7);
uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold

emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',34);
        emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',8);
return c;
    }

    /**
     * @dev Subtracts two unsigned integers, reverts on overflow (i.e. if subtrahend is greater than minuend).
     */
    function sub(uint256 a, uint256 b) internal      returns (uint256) {emit __FunctionCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',3);

emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',41);
        emit __AssertPreCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',4);
emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',9);
require(b <= a);emit __AssertPostCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',4);

emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',42);
        emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',10);
uint256 c = a - b;

emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',44);
        emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',11);
return c;
    }

    /**
     * @dev Adds two unsigned integers, reverts on overflow.
     */
    function add(uint256 a, uint256 b) internal      returns (uint256) {emit __FunctionCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',4);

emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',51);
        emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',12);
uint256 c = a + b;
emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',52);
        emit __AssertPreCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',5);
emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',13);
require(c >= a);emit __AssertPostCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',5);


emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',54);
        emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',14);
return c;
    }

    /**
     * @dev Divides two unsigned integers and returns the remainder (unsigned integer modulo),
     * reverts when dividing by zero.
     */
    function mod(uint256 a, uint256 b) internal      returns (uint256) {emit __FunctionCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',5);

emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',62);
        emit __AssertPreCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',6);
emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',15);
require(b != 0);emit __AssertPostCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',6);

emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',63);
        emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/truffle/contracts/SafeMath.sol',16);
return a % b;
    }
}
