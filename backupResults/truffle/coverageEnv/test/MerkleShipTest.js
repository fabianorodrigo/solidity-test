const truffleAssert = require('truffle-assertions');
const MerkleShip = artifacts.require("MerkleShip");
const SafeMath = artifacts.require("SafeMath");
const ProxySafeMath = artifacts.require("ProxySafeMath");

contract("MerkleShip",(accounts)=>{
  let trace = false;
  let contractSafeMath = null;
  let contractMerkleShip = null;
  beforeEach(async () => {
    contractSafeMath = await SafeMath.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: SafeMath.new({from: accounts[0]}');
    MerkleShip.link("SafeMath",contractSafeMath.address);
    contractMerkleShip = await MerkleShip.new({from:accounts[0]});
    if(trace) console.log('SUCESSO: MerkleShip.new({from:accounts[0]}');
  });
  
  it('Should execute emergencyStop(string) WHEN msg.sender==admin,isStopped==false', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await contractMerkleShip.emergencyStop("victory by unchallenged hit count",{from: accounts[0]});
  });
  it('Should fail emergencyStop(string) when NOT comply with: msg.sender == admin', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await truffleAssert.fails(contractMerkleShip.emergencyStop("victory by unchallenged hit count",{from: accounts[9]}),'revert');
  });
  it('Should execute proposeGame(uint96,bytes32) WHEN isStopped==false,msg.value==_wager', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await contractMerkleShip.proposeGame(1, [229,123,26,98,128,103,44,155,33,88,239,145,220,151,48,174,117,218,227,79,60,236,87,129,154,231,21,161,200,250,164,235],{from: accounts[0],value:1});
  });
  it('Should fail proposeGame(uint96,bytes32) when NOT comply with: isStopped == false', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("fk92ys",{from:accounts[0]});
    let result = await truffleAssert.fails(contractMerkleShip.proposeGame(1, [229,123,26,98,128,103,44,155,33,88,239,145,220,151,48,174,117,218,227,79,60,236,87,129,154,231,21,161,200,250,164,235],{from: accounts[0],value:1}),'revert');
  });
  it('Should fail proposeGame(uint96,bytes32) when NOT comply with: msg.value == _wager', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await truffleAssert.fails(contractMerkleShip.proposeGame(2, [229,123,26,98,128,103,44,155,33,88,239,145,220,151,48,174,117,218,227,79,60,236,87,129,154,231,21,161,200,250,164,235],{from: accounts[0],value:1}),'revert');
  });
  it('Should execute acceptGame(uint32,bytes32) WHEN isStopped==false,msg.value==MemberAccess.stateVariableIndex', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await contractMerkleShip.acceptGame(41, [73,24,147,43,238,29,14,49,116,13,27,168,126,238,12,3,185,236,110,113,166,47,109,156,38,59,180,253,115,81,77,207],{from: accounts[0],value:63});
  });
  it('Should fail acceptGame(uint32,bytes32) when NOT comply with: isStopped == false', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("game resolved in an emergency",{from:accounts[0]});
    let result = await truffleAssert.fails(contractMerkleShip.acceptGame(41, [73,24,147,43,238,29,14,49,116,13,27,168,126,238,12,3,185,236,110,113,166,47,109,156,38,59,180,253,115,81,77,207],{from: accounts[0],value:63}),'revert');
  });
  it('Should execute cancelProposedGame(uint32) WHEN isStopped==false,msg.sender==MemberAccess.stateVariableIndex', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await contractMerkleShip.cancelProposedGame(48,{from: accounts[4]});
  });
  it('Should fail cancelProposedGame(uint32) when NOT comply with: isStopped == false', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("victory by concession",{from:accounts[0]});
    let result = await truffleAssert.fails(contractMerkleShip.cancelProposedGame(48,{from: accounts[4]}),'revert');
  });
  it('Should execute guessAndReveal(uint32,uint8[2],bytes32[6],string,string) WHEN MemberAccess.MemberAccess.Identifier>0,msg.sender==MemberAccess.stateVariableIndex,isStopped==false,MemberAccess.FunctionCall==0', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await contractMerkleShip.guessAndReveal(179, [39,1], [[111,107,237,49,168,190,209,131,129,149,222,155,224,111,68,86,210,80,251,34,153,61,180,8,249,167,221,131,55,223,109,182],[132,106,197,149,244,54,177,163,159,58,238,184,43,162,112,215,150,85,110,92,192,103,101,211,129,84,1,99,51,224,38,50],[9,205,240,188,54,237,166,229,179,190,126,236,85,220,235,29,33,169,23,174,154,17,119,111,28,242,14,101,42,106,133,231],[72,158,39,46,17,4,126,254,208,244,139,211,217,118,57,148,3,49,92,98,209,14,80,107,236,167,251,109,199,244,47,196],[255,50,92,237,101,181,27,208,88,221,80,158,107,61,247,75,149,44,16,242,35,94,206,246,37,248,184,46,234,172,35,174],[251,14,236,235,73,114,164,147,246,91,174,178,166,187,46,3,129,221,128,225,54,26,64,164,148,98,205,149,77,120,62,115]], "victory by concession", "vl0lr",{from: accounts[4]});
  });
  it('Should fail guessAndReveal(uint32,uint8[2],bytes32[6],string,string) when NOT comply with: isStopped == false', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("verified victory by hit count",{from:accounts[0]});
    let result = await truffleAssert.fails(contractMerkleShip.guessAndReveal(179, [39,1], [[111,107,237,49,168,190,209,131,129,149,222,155,224,111,68,86,210,80,251,34,153,61,180,8,249,167,221,131,55,223,109,182],[132,106,197,149,244,54,177,163,159,58,238,184,43,162,112,215,150,85,110,92,192,103,101,211,129,84,1,99,51,224,38,50],[9,205,240,188,54,237,166,229,179,190,126,236,85,220,235,29,33,169,23,174,154,17,119,111,28,242,14,101,42,106,133,231],[72,158,39,46,17,4,126,254,208,244,139,211,217,118,57,148,3,49,92,98,209,14,80,107,236,167,251,109,199,244,47,196],[255,50,92,237,101,181,27,208,88,221,80,158,107,61,247,75,149,44,16,242,35,94,206,246,37,248,184,46,234,172,35,174],[251,14,236,235,73,114,164,147,246,91,174,178,166,187,46,3,129,221,128,225,54,26,64,164,148,98,205,149,77,120,62,115]], "victory by concession", "vl0lr",{from: accounts[4]}),'revert');
  });
  it('Should execute guessAndReveal(uint32,uint8[2],bytes32[6],string,string) WHEN MemberAccess.MemberAccess.Identifier<=0,msg.sender==MemberAccess.stateVariableIndex,isStopped==false,MemberAccess.FunctionCall==0', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await contractMerkleShip.guessAndReveal(13, [48,3], [[144,128,45,52,59,105,232,145,130,78,12,191,166,161,229,136,106,240,46,190,113,144,19,48,63,74,160,166,101,61,84,92],[194,81,205,248,106,109,56,44,179,183,148,188,234,135,10,22,197,168,175,231,120,156,182,185,29,1,250,190,173,141,178,241],[126,3,58,111,76,58,24,200,101,85,44,114,23,177,163,145,52,248,68,131,244,239,14,41,11,12,88,41,202,189,0,33],[205,210,167,116,252,151,102,77,9,82,65,202,31,109,102,2,204,76,76,161,53,76,234,69,110,238,78,81,39,172,230,212],[56,198,140,102,185,178,40,66,35,115,179,146,43,43,78,52,209,50,194,21,231,100,71,195,236,81,175,51,224,219,2,35],[185,201,246,44,249,202,171,172,32,217,95,27,231,165,0,162,95,129,110,158,106,75,250,84,173,111,164,206,227,92,112,141]], "victory by abandonment", "t6gtzb",{from: accounts[1]});
  });
  it('Should fail guessAndReveal(uint32,uint8[2],bytes32[6],string,string) when NOT comply with: isStopped == false', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("fk92ys",{from:accounts[0]});
    let result = await truffleAssert.fails(contractMerkleShip.guessAndReveal(13, [48,3], [[144,128,45,52,59,105,232,145,130,78,12,191,166,161,229,136,106,240,46,190,113,144,19,48,63,74,160,166,101,61,84,92],[194,81,205,248,106,109,56,44,179,183,148,188,234,135,10,22,197,168,175,231,120,156,182,185,29,1,250,190,173,141,178,241],[126,3,58,111,76,58,24,200,101,85,44,114,23,177,163,145,52,248,68,131,244,239,14,41,11,12,88,41,202,189,0,33],[205,210,167,116,252,151,102,77,9,82,65,202,31,109,102,2,204,76,76,161,53,76,234,69,110,238,78,81,39,172,230,212],[56,198,140,102,185,178,40,66,35,115,179,146,43,43,78,52,209,50,194,21,231,100,71,195,236,81,175,51,224,219,2,35],[185,201,246,44,249,202,171,172,32,217,95,27,231,165,0,162,95,129,110,158,106,75,250,84,173,111,164,206,227,92,112,141]], "victory by abandonment", "t6gtzb",{from: accounts[1]}),'revert');
  });
  it('Should execute guessAndReveal(uint32,uint8[2],bytes32[6],string,string) WHEN msg.sender==MemberAccess.stateVariableIndex,isStopped==false,MemberAccess.FunctionCall==0', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await contractMerkleShip.guessAndReveal(101, [0,49], [[145,245,221,44,140,46,104,205,115,189,112,141,1,200,239,22,79,23,238,93,254,127,95,189,213,65,183,63,229,227,229,207],[15,180,34,117,187,188,43,98,67,32,11,198,202,187,195,219,216,5,252,24,154,26,127,61,152,121,34,247,187,70,110,185],[245,177,159,248,219,208,91,103,195,188,183,92,253,69,203,3,154,115,73,71,205,26,61,75,202,165,34,75,33,254,81,194],[199,80,235,8,139,202,86,101,25,113,8,194,254,7,153,14,158,174,20,187,69,20,80,211,178,162,188,23,224,131,228,64],[219,66,189,19,158,220,133,67,105,154,238,111,6,60,236,52,216,59,177,84,16,184,118,187,98,233,181,171,240,92,24,249],[207,247,65,203,74,225,249,65,43,3,101,41,246,218,152,249,249,132,249,118,165,230,151,140,51,148,224,228,147,254,181,63]], "victory by unanswered challenge", "victory by unchallenged hit count",{from: accounts[2]});
  });
  it('Should fail guessAndReveal(uint32,uint8[2],bytes32[6],string,string) when NOT comply with: isStopped == false', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("fk92ys",{from:accounts[0]});
    let result = await truffleAssert.fails(contractMerkleShip.guessAndReveal(101, [0,49], [[145,245,221,44,140,46,104,205,115,189,112,141,1,200,239,22,79,23,238,93,254,127,95,189,213,65,183,63,229,227,229,207],[15,180,34,117,187,188,43,98,67,32,11,198,202,187,195,219,216,5,252,24,154,26,127,61,152,121,34,247,187,70,110,185],[245,177,159,248,219,208,91,103,195,188,183,92,253,69,203,3,154,115,73,71,205,26,61,75,202,165,34,75,33,254,81,194],[199,80,235,8,139,202,86,101,25,113,8,194,254,7,153,14,158,174,20,187,69,20,80,211,178,162,188,23,224,131,228,64],[219,66,189,19,158,220,133,67,105,154,238,111,6,60,236,52,216,59,177,84,16,184,118,187,98,233,181,171,240,92,24,249],[207,247,65,203,74,225,249,65,43,3,101,41,246,218,152,249,249,132,249,118,165,230,151,140,51,148,224,228,147,254,181,63]], "victory by unanswered challenge", "victory by unchallenged hit count",{from: accounts[2]}),'revert');
  });
  it('Should execute guessAndReveal(uint32,uint8[2],bytes32[6],string,string) WHEN msg.sender!=MemberAccess.stateVariableIndex,isStopped==false,MemberAccess.FunctionCall==0', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await contractMerkleShip.guessAndReveal(7, [65,180], [[54,69,249,42,76,180,135,45,198,112,158,170,201,89,40,4,94,162,213,57,191,15,199,108,228,214,212,130,72,151,134,159],[233,252,76,62,30,122,77,20,47,165,153,139,91,238,42,199,183,134,233,207,220,183,191,194,112,147,186,182,239,156,13,56],[222,123,140,142,94,47,165,71,71,27,127,51,192,86,155,28,75,123,238,255,20,115,26,73,1,178,91,38,167,80,180,156],[39,164,16,250,37,26,1,28,2,92,67,169,15,215,204,60,84,9,72,213,89,253,88,74,43,63,102,57,11,188,113,72],[120,22,68,203,209,223,29,14,80,163,36,57,190,242,46,183,159,172,206,144,189,116,184,188,8,127,49,184,154,19,120,32],[42,25,200,199,1,68,228,40,219,193,170,226,174,134,28,120,139,240,176,223,204,228,6,133,16,144,4,31,253,198,245,180]], "eb49xg", "game resolved in an emergency",{from: accounts[5]});
  });
  it('Should fail guessAndReveal(uint32,uint8[2],bytes32[6],string,string) when NOT comply with: isStopped == false', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("game resolved in an emergency",{from:accounts[0]});
    let result = await truffleAssert.fails(contractMerkleShip.guessAndReveal(7, [65,180], [[54,69,249,42,76,180,135,45,198,112,158,170,201,89,40,4,94,162,213,57,191,15,199,108,228,214,212,130,72,151,134,159],[233,252,76,62,30,122,77,20,47,165,153,139,91,238,42,199,183,134,233,207,220,183,191,194,112,147,186,182,239,156,13,56],[222,123,140,142,94,47,165,71,71,27,127,51,192,86,155,28,75,123,238,255,20,115,26,73,1,178,91,38,167,80,180,156],[39,164,16,250,37,26,1,28,2,92,67,169,15,215,204,60,84,9,72,213,89,253,88,74,43,63,102,57,11,188,113,72],[120,22,68,203,209,223,29,14,80,163,36,57,190,242,46,183,159,172,206,144,189,116,184,188,8,127,49,184,154,19,120,32],[42,25,200,199,1,68,228,40,219,193,170,226,174,134,28,120,139,240,176,223,204,228,6,133,16,144,4,31,253,198,245,180]], "eb49xg", "game resolved in an emergency",{from: accounts[5]}),'revert');
  });
  it('Should execute resolveAbandonedGame(uint32) WHEN msg.sender==MemberAccess.stateVariableIndex,msg.sender==MemberAccess.stateVariableIndex,isStopped==false,(await web3.eth.getBlock(await web3.eth.getBlockNumber())).timestamp>=MemberAccess.stateVariableIndex + abandonThreshold,MemberAccess.stateVariableIndex==MerkleShip.Turn.PlayerB', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await contractMerkleShip.resolveAbandonedGame(12,{from: accounts[6]});
  });
  it('Should fail resolveAbandonedGame(uint32) when NOT comply with: isStopped == false', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("fk92ys",{from:accounts[0]});
    let result = await truffleAssert.fails(contractMerkleShip.resolveAbandonedGame(12,{from: accounts[6]}),'revert');
  });
  it('Should execute resolveAbandonedGame(uint32) WHEN msg.sender==MemberAccess.stateVariableIndex,msg.sender==MemberAccess.stateVariableIndex,isStopped==false,(await web3.eth.getBlock(await web3.eth.getBlockNumber())).timestamp>=MemberAccess.stateVariableIndex + abandonThreshold,MemberAccess.stateVariableIndex==MerkleShip.Turn.PlayerA', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await contractMerkleShip.resolveAbandonedGame(41,{from: accounts[3]});
  });
  it('Should fail resolveAbandonedGame(uint32) when NOT comply with: isStopped == false', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("victory by unchallenged hit count",{from:accounts[0]});
    let result = await truffleAssert.fails(contractMerkleShip.resolveAbandonedGame(41,{from: accounts[3]}),'revert');
  });
  it('Should execute resolveAbandonedGame(uint32) WHEN msg.sender!=MemberAccess.stateVariableIndex,msg.sender==MemberAccess.stateVariableIndex,isStopped==false,(await web3.eth.getBlock(await web3.eth.getBlockNumber())).timestamp>=MemberAccess.stateVariableIndex + abandonThreshold', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await contractMerkleShip.resolveAbandonedGame(180,{from: accounts[4]});
  });
  it('Should fail resolveAbandonedGame(uint32) when NOT comply with: isStopped == false', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("vl0lr",{from:accounts[0]});
    let result = await truffleAssert.fails(contractMerkleShip.resolveAbandonedGame(180,{from: accounts[4]}),'revert');
  });
  it('Should execute concedeGame(uint32) WHEN msg.sender==MemberAccess.stateVariableIndex,msg.sender==MemberAccess.stateVariableIndex,isStopped==false', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await contractMerkleShip.concedeGame(179,{from: accounts[2]});
  });
  it('Should fail concedeGame(uint32) when NOT comply with: isStopped == false', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("eb49xg",{from:accounts[0]});
    let result = await truffleAssert.fails(contractMerkleShip.concedeGame(179,{from: accounts[2]}),'revert');
  });
  it('Should execute concedeGame(uint32) WHEN msg.sender==MemberAccess.stateVariableIndex,msg.sender==MemberAccess.stateVariableIndex,isStopped==false', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await contractMerkleShip.concedeGame(99,{from: accounts[3]});
  });
  it('Should fail concedeGame(uint32) when NOT comply with: isStopped == false', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("eb49xg",{from:accounts[0]});
    let result = await truffleAssert.fails(contractMerkleShip.concedeGame(99,{from: accounts[3]}),'revert');
  });
  it('Should execute concedeGame(uint32) WHEN msg.sender!=MemberAccess.stateVariableIndex,msg.sender==MemberAccess.stateVariableIndex,isStopped==false', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await contractMerkleShip.concedeGame(49,{from: accounts[5]});
  });
  it('Should fail concedeGame(uint32) when NOT comply with: isStopped == false', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("verified victory by hit count",{from:accounts[0]});
    let result = await truffleAssert.fails(contractMerkleShip.concedeGame(49,{from: accounts[5]}),'revert');
  });
  it('Should execute challengeVictory(uint32) WHEN msg.sender==MemberAccess.stateVariableIndex,isStopped==false,MemberAccess.Identifier==MerkleShip.GameState.VictoryPending,msg.sender!=MemberAccess.Identifier', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await contractMerkleShip.challengeVictory(7,{from: accounts[1]});
  });
  it('Should fail challengeVictory(uint32) when NOT comply with: isStopped == false', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("game resolved in an emergency",{from:accounts[0]});
    let result = await truffleAssert.fails(contractMerkleShip.challengeVictory(7,{from: accounts[1]}),'revert');
  });
  it('Should execute answerChallenge(uint32,string[64]) WHEN msg.sender==MemberAccess.stateVariableIndex,msg.sender==MemberAccess.stateVariableIndex,isStopped==false,MemberAccess.Identifier==MerkleShip.GameState.VictoryChallenged,FunctionCall==true,FunctionCall==root', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await contractMerkleShip.answerChallenge(63, ["t6gtzb","kmknyq","vl0lr","kmknyq","kmknyq","vl0lr","3pqjy","victory by concession","victory by abandonment","victory by unanswered challenge","victory by unanswered challenge","uys4gm","t6gtzb","verified victory by hit count","fk92ys","t6gtzb","kmknyq","victory by unanswered challenge","victory by unchallenged hit count","kmknyq","3pqjy","vl0lr","game resolved in an emergency","3pqjy","uys4gm","kmknyq","t6gtzb","fk92ys","3pqjy","kmknyq","victory by abandonment","3pqjy","4ad6s7","fk92ys","kmknyq","victory by unchallenged hit count","eb49xg","uys4gm","victory by abandonment","fk92ys","uys4gm","verified victory by hit count","t6gtzb","t6gtzb","t6gtzb","verified victory by hit count","kmknyq","kmknyq","verified victory by hit count","verified victory by hit count","victory by unanswered challenge","vl0lr","t6gtzb","verified victory by hit count","fk92ys","3pqjy","victory by abandonment","flcnll","t6gtzb","t6gtzb","kmknyq","verified victory by hit count","vl0lr","t6gtzb"],{from: accounts[0]});
  });
  it('Should fail answerChallenge(uint32,string[64]) when NOT comply with: isStopped == false', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("verified victory by hit count",{from:accounts[0]});
    let result = await truffleAssert.fails(contractMerkleShip.answerChallenge(63, ["t6gtzb","kmknyq","vl0lr","kmknyq","kmknyq","vl0lr","3pqjy","victory by concession","victory by abandonment","victory by unanswered challenge","victory by unanswered challenge","uys4gm","t6gtzb","verified victory by hit count","fk92ys","t6gtzb","kmknyq","victory by unanswered challenge","victory by unchallenged hit count","kmknyq","3pqjy","vl0lr","game resolved in an emergency","3pqjy","uys4gm","kmknyq","t6gtzb","fk92ys","3pqjy","kmknyq","victory by abandonment","3pqjy","4ad6s7","fk92ys","kmknyq","victory by unchallenged hit count","eb49xg","uys4gm","victory by abandonment","fk92ys","uys4gm","verified victory by hit count","t6gtzb","t6gtzb","t6gtzb","verified victory by hit count","kmknyq","kmknyq","verified victory by hit count","verified victory by hit count","victory by unanswered challenge","vl0lr","t6gtzb","verified victory by hit count","fk92ys","3pqjy","victory by abandonment","flcnll","t6gtzb","t6gtzb","kmknyq","verified victory by hit count","vl0lr","t6gtzb"],{from: accounts[0]}),'revert');
  });
  it('Should execute answerChallenge(uint32,string[64]) WHEN msg.sender==MemberAccess.stateVariableIndex,msg.sender==MemberAccess.stateVariableIndex,isStopped==false,MemberAccess.Identifier==MerkleShip.GameState.VictoryChallenged,FunctionCall==true,FunctionCall==root', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await contractMerkleShip.answerChallenge(2, ["victory by concession","4ad6s7","victory by concession","victory by unchallenged hit count","kmknyq","flcnll","1p67wq","eb49xg","game resolved in an emergency","victory by unchallenged hit count","flcnll","victory by concession","verified victory by hit count","2codsm","victory by unchallenged hit count","2codsm","kmknyq","game resolved in an emergency","victory by unanswered challenge","flcnll","4ad6s7","vl0lr","victory by unchallenged hit count","3pqjy","victory by abandonment","2codsm","kmknyq","game resolved in an emergency","flcnll","4ad6s7","4ad6s7","t6gtzb","uys4gm","wo1gyn","flcnll","2codsm","uys4gm","victory by abandonment","victory by abandonment","victory by unanswered challenge","verified victory by hit count","wo1gyn","vl0lr","victory by concession","eb49xg","game resolved in an emergency","kmknyq","vl0lr","victory by unchallenged hit count","kmknyq","vl0lr","eg7kb9","t6gtzb","victory by unchallenged hit count","4ad6s7","fk92ys","vl0lr","victory by unchallenged hit count","flcnll","kmknyq","bkbzh","4ad6s7","victory by unchallenged hit count","uys4gm"],{from: accounts[4]});
  });
  it('Should fail answerChallenge(uint32,string[64]) when NOT comply with: isStopped == false', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("vl0lr",{from:accounts[0]});
    let result = await truffleAssert.fails(contractMerkleShip.answerChallenge(2, ["victory by concession","4ad6s7","victory by concession","victory by unchallenged hit count","kmknyq","flcnll","1p67wq","eb49xg","game resolved in an emergency","victory by unchallenged hit count","flcnll","victory by concession","verified victory by hit count","2codsm","victory by unchallenged hit count","2codsm","kmknyq","game resolved in an emergency","victory by unanswered challenge","flcnll","4ad6s7","vl0lr","victory by unchallenged hit count","3pqjy","victory by abandonment","2codsm","kmknyq","game resolved in an emergency","flcnll","4ad6s7","4ad6s7","t6gtzb","uys4gm","wo1gyn","flcnll","2codsm","uys4gm","victory by abandonment","victory by abandonment","victory by unanswered challenge","verified victory by hit count","wo1gyn","vl0lr","victory by concession","eb49xg","game resolved in an emergency","kmknyq","vl0lr","victory by unchallenged hit count","kmknyq","vl0lr","eg7kb9","t6gtzb","victory by unchallenged hit count","4ad6s7","fk92ys","vl0lr","victory by unchallenged hit count","flcnll","kmknyq","bkbzh","4ad6s7","victory by unchallenged hit count","uys4gm"],{from: accounts[4]}),'revert');
  });
  it('Should execute answerChallenge(uint32,string[64]) WHEN msg.sender!=MemberAccess.stateVariableIndex,msg.sender==MemberAccess.stateVariableIndex,isStopped==false,MemberAccess.Identifier==MerkleShip.GameState.VictoryChallenged,FunctionCall==true,FunctionCall==root', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await contractMerkleShip.answerChallenge(64, ["victory by unanswered challenge","victory by concession","game resolved in an emergency","4ad6s7","victory by unchallenged hit count","fk92ys","uys4gm","eg7kb9","vl0lr","3pqjy","victory by unchallenged hit count","kmknyq","6suttm","uys4gm","4ad6s7","4ad6s7","4ad6s7","wo1gyn","vl0lr","victory by concession","eb49xg","1p67wq","victory by concession","t6gtzb","2codsm","w3vcie","t6gtzb","t6gtzb","vl0lr","6suttm","vl0lr","victory by concession","wo1gyn","victory by concession","victory by concession","2codsm","game resolved in an emergency","bkbzh","3pqjy","wo1gyn","flcnll","eg7kb9","victory by abandonment","victory by unanswered challenge","eg7kb9","kmknyq","eg7kb9","eb49xg","4ad6s7","game resolved in an emergency","w3vcie","4ad6s7","victory by unanswered challenge","1p67wq","uys4gm","victory by concession","flcnll","83uqo","fk92ys","uys4gm","w3vcie","t6gtzb","83uqo","eg7kb9"],{from: accounts[9]});
  });
  it('Should fail answerChallenge(uint32,string[64]) when NOT comply with: isStopped == false', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("vl0lr",{from:accounts[0]});
    let result = await truffleAssert.fails(contractMerkleShip.answerChallenge(64, ["victory by unanswered challenge","victory by concession","game resolved in an emergency","4ad6s7","victory by unchallenged hit count","fk92ys","uys4gm","eg7kb9","vl0lr","3pqjy","victory by unchallenged hit count","kmknyq","6suttm","uys4gm","4ad6s7","4ad6s7","4ad6s7","wo1gyn","vl0lr","victory by concession","eb49xg","1p67wq","victory by concession","t6gtzb","2codsm","w3vcie","t6gtzb","t6gtzb","vl0lr","6suttm","vl0lr","victory by concession","wo1gyn","victory by concession","victory by concession","2codsm","game resolved in an emergency","bkbzh","3pqjy","wo1gyn","flcnll","eg7kb9","victory by abandonment","victory by unanswered challenge","eg7kb9","kmknyq","eg7kb9","eb49xg","4ad6s7","game resolved in an emergency","w3vcie","4ad6s7","victory by unanswered challenge","1p67wq","uys4gm","victory by concession","flcnll","83uqo","fk92ys","uys4gm","w3vcie","t6gtzb","83uqo","eg7kb9"],{from: accounts[9]}),'revert');
  });
  it('Should execute resolveUnclaimedVictory(uint32) WHEN msg.sender==MemberAccess.stateVariableIndex,isStopped==false,(await web3.eth.getBlock(await web3.eth.getBlockNumber())).timestamp>=claimTimer + abandonThreshold,MemberAccess.Identifier==MerkleShip.GameState.VictoryPending', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await contractMerkleShip.resolveUnclaimedVictory(48,{from: accounts[5]});
  });
  it('Should fail resolveUnclaimedVictory(uint32) when NOT comply with: isStopped == false', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("2codsm",{from:accounts[0]});
    let result = await truffleAssert.fails(contractMerkleShip.resolveUnclaimedVictory(48,{from: accounts[5]}),'revert');
  });
  it('Should execute resolveUnansweredChallenge(uint32) WHEN MemberAccess.Identifier==MemberAccess.stateVariableIndex,msg.sender==MemberAccess.stateVariableIndex,isStopped==false,(await web3.eth.getBlock(await web3.eth.getBlockNumber())).timestamp>=claimTimer + abandonThreshold,MemberAccess.Identifier==MerkleShip.GameState.VictoryChallenged', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await contractMerkleShip.resolveUnansweredChallenge(13,{from: accounts[2]});
  });
  it('Should fail resolveUnansweredChallenge(uint32) when NOT comply with: isStopped == false', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("victory by abandonment",{from:accounts[0]});
    let result = await truffleAssert.fails(contractMerkleShip.resolveUnansweredChallenge(13,{from: accounts[2]}),'revert');
  });
  it('Should execute resolveUnansweredChallenge(uint32) WHEN MemberAccess.Identifier==MemberAccess.stateVariableIndex,msg.sender==MemberAccess.stateVariableIndex,isStopped==false,(await web3.eth.getBlock(await web3.eth.getBlockNumber())).timestamp>=claimTimer + abandonThreshold,MemberAccess.Identifier==MerkleShip.GameState.VictoryChallenged', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await contractMerkleShip.resolveUnansweredChallenge(179,{from: accounts[3]});
  });
  it('Should fail resolveUnansweredChallenge(uint32) when NOT comply with: isStopped == false', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("kmknyq",{from:accounts[0]});
    let result = await truffleAssert.fails(contractMerkleShip.resolveUnansweredChallenge(179,{from: accounts[3]}),'revert');
  });
  it('Should execute resolveUnansweredChallenge(uint32) WHEN MemberAccess.Identifier!=MemberAccess.stateVariableIndex,msg.sender==MemberAccess.stateVariableIndex,isStopped==false,(await web3.eth.getBlock(await web3.eth.getBlockNumber())).timestamp>=claimTimer + abandonThreshold,MemberAccess.Identifier==MerkleShip.GameState.VictoryChallenged', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await contractMerkleShip.resolveUnansweredChallenge(7,{from: accounts[7]});
  });
  it('Should fail resolveUnansweredChallenge(uint32) when NOT comply with: isStopped == false', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("flcnll",{from:accounts[0]});
    let result = await truffleAssert.fails(contractMerkleShip.resolveUnansweredChallenge(7,{from: accounts[7]}),'revert');
  });
  it('Should execute withdraw() WHEN userBalance>0', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await contractMerkleShip.withdraw({from: accounts[0]});
  });
  it('Should execute emergencyResolve(uint32) WHEN MemberAccess.Identifier==MerkleShip.GameState.Ready,isStopped==true,msg.sender==MemberAccess.stateVariableIndex', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("uys4gm",{from:accounts[0]});
    let result = await contractMerkleShip.emergencyResolve(64,{from: accounts[2]});
  });
  it('Should fail emergencyResolve(uint32) when NOT comply with: isStopped == true', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await truffleAssert.fails(contractMerkleShip.emergencyResolve(64,{from: accounts[2]}),'revert');
  });
  it('Should execute emergencyResolve(uint32) WHEN MemberAccess.Identifier==MerkleShip.GameState.Active,isStopped==true,msg.sender==MemberAccess.stateVariableIndex', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("fk92ys",{from:accounts[0]});
    let result = await contractMerkleShip.emergencyResolve(7,{from: accounts[9]});
  });
  it('Should fail emergencyResolve(uint32) when NOT comply with: isStopped == true', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await truffleAssert.fails(contractMerkleShip.emergencyResolve(7,{from: accounts[9]}),'revert');
  });
  it('Should execute emergencyResolve(uint32) WHEN MemberAccess.Identifier==MerkleShip.GameState.VictoryPending,isStopped==true,msg.sender==MemberAccess.stateVariableIndex', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("1p67wq",{from:accounts[0]});
    let result = await contractMerkleShip.emergencyResolve(100,{from: accounts[8]});
  });
  it('Should fail emergencyResolve(uint32) when NOT comply with: isStopped == true', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await truffleAssert.fails(contractMerkleShip.emergencyResolve(100,{from: accounts[8]}),'revert');
  });
  it('Should execute emergencyResolve(uint32) WHEN MemberAccess.Identifier==MerkleShip.GameState.VictoryChallenged,isStopped==true,msg.sender==MemberAccess.stateVariableIndex', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("uys4gm",{from:accounts[0]});
    let result = await contractMerkleShip.emergencyResolve(101,{from: accounts[6]});
  });
  it('Should fail emergencyResolve(uint32) when NOT comply with: isStopped == true', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await truffleAssert.fails(contractMerkleShip.emergencyResolve(101,{from: accounts[6]}),'revert');
  });
  it('Should execute emergencyResolve(uint32) WHEN MemberAccess.Identifier!=MerkleShip.GameState.Active,MemberAccess.Identifier!=MerkleShip.GameState.VictoryPending,MemberAccess.Identifier!=MerkleShip.GameState.VictoryChallenged,MemberAccess.Identifier!=MerkleShip.GameState.Ready,isStopped==true,msg.sender==MemberAccess.stateVariableIndex', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    await contractMerkleShip.emergencyStop("victory by abandonment",{from:accounts[0]});
    let result = await contractMerkleShip.emergencyResolve(49,{from: accounts[2]});
  });
  it('Should fail emergencyResolve(uint32) when NOT comply with: isStopped == true', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await truffleAssert.fails(contractMerkleShip.emergencyResolve(49,{from: accounts[2]}),'revert');
  });
  it('Should execute getBalance()', async () => {
    //transactionParameter: [object Object]
    //MerkleShip.isStopped: false
    //MerkleShip.gameCount: 0
    //MerkleShip.abandonThreshold: 48
    //MerkleShip.rows: 8
    //MerkleShip.columns: 8
    //MerkleShip.hitThreshold: 12
    //MerkleShip.admin: accounts[0]
    let result = await contractMerkleShip.getBalance({from: accounts[0]});
  });
});
