pragma experimental ABIEncoderV2;
import "../ERC721Mintable.sol";

contract ProxyERC165  is ERC165 {

       function testsupportsInterface0(bytes4  interfaceId) public view returns (bool ){
    return this.supportsInterface(interfaceId);
   }

   function test_registerInterface0(bytes4  interfaceId) public  {
    _registerInterface(interfaceId);
   }


}