const truffleAssert = require('truffle-assertions');
const Ownable = artifacts.require("Ownable");
const Pausable = artifacts.require("Pausable");
const ERC165 = artifacts.require("ERC165");
const ERC721 = artifacts.require("ERC721");
const ERC721Enumerable = artifacts.require("ERC721Enumerable");
const ERC721Metadata = artifacts.require("ERC721Metadata");
const ERC721MintableComplete = artifacts.require("ERC721MintableComplete");
const solcChecker = artifacts.require("solcChecker");
const OraclizeI = artifacts.require("OraclizeI");
const OraclizeAddrResolverI = artifacts.require("OraclizeAddrResolverI");
const Buffer = artifacts.require("Buffer");
const CBOR = artifacts.require("CBOR");
const usingOraclize = artifacts.require("usingOraclize");

contract("ERC721",(accounts)=>{
  let trace = false;
  let contractBuffer = null;
  let contractCBOR = null;
  let contractusingOraclize = null;
  let contractOwnable = null;
  let contractPausable = null;
  let contractERC165 = null;
  let contractERC721 = null;
  let contractERC721Enumerable = null;
  let contractERC721Metadata = null;
  let contractERC721MintableComplete = null;
  beforeEach(async () => {
    contractBuffer = await Buffer.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: Buffer.new({from: accounts[0]}');
    contractCBOR = await CBOR.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: CBOR.new({from: accounts[0]}');
    contractusingOraclize = await usingOraclize.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: usingOraclize.new({from: accounts[0]}');
    Ownable.link("Buffer",contractBuffer.address);
     Ownable.link("CBOR",contractCBOR.address);
    contractOwnable = await Ownable.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: Ownable.new({from: accounts[0]}');
    ERC721.link("Buffer",contractBuffer.address);
     ERC721.link("CBOR",contractCBOR.address);
    contractERC721 = await ERC721.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC721.new({from: accounts[0]}');
    ERC721Enumerable.link("Buffer",contractBuffer.address);
     ERC721Enumerable.link("CBOR",contractCBOR.address);
    contractERC721Enumerable = await ERC721Enumerable.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC721Enumerable.new({from: accounts[0]}');
    ERC721Metadata.link("Buffer",contractBuffer.address);
     ERC721Metadata.link("CBOR",contractCBOR.address);
    contractERC721Metadata = await ERC721Metadata.new("ERC20: burn amount exceeds allowance","\x19Ethereum Signed Message:\n32","P",{from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC721Metadata.new("ERC20: burn amount exceeds allowance","\x19Ethereum Signed Message:\n32","P",{from: accounts[0]}');
    ERC721MintableComplete.link("Buffer",contractBuffer.address);
     ERC721MintableComplete.link("CBOR",contractCBOR.address);
    contractERC721MintableComplete = await ERC721MintableComplete.new("L","",{from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC721MintableComplete.new("L","",{from: accounts[0]}');
  });
  
  it('Should execute balanceOf(address)', async () => {
    let result = await contractERC721.methods["balanceOf(address)"](accounts[3],{from: accounts[0]});
  });
  it('Should execute ownerOf(uint256)', async () => {
    let result = await contractERC721.methods["ownerOf(uint256)"](29,{from: accounts[0]});
  });
  it('Should execute approve(address,uint256)', async () => {
    let result = await contractERC721.methods["approve(address,uint256)"](accounts[4], 56,{from: accounts[0]});
  });
  it('Should execute getApproved(uint256)', async () => {
    let result = await contractERC721.methods["getApproved(uint256)"](26,{from: accounts[0]});
  });
  it('Should execute setApprovalForAll(address,bool)', async () => {
    let result = await contractERC721.methods["setApprovalForAll(address,bool)"](accounts[5], false,{from: accounts[0]});
  });
  it('Should fail setApprovalForAll(address,bool) when NOT comply with: to != sender', async () => {
    let result = await truffleAssert.fails(contractERC721.methods["setApprovalForAll(address,bool)"]("0xe1a1d3637eE02391ac4035e72456Ca7448c73FD4", false,{from: 0xe1a1d3637eE02391ac4035e72456Ca7448c73FD4,value:55}),'revert');
  });
  it('Should fail setApprovalForAll(address,bool) when NOT comply with: to != sender', async () => {
    let result = await truffleAssert.fails(contractERC721.methods["setApprovalForAll(address,bool)"]("0xe1a1d3637eE02391ac4035e72456Ca7448c73FD4", false,{from: 0xe1a1d3637eE02391ac4035e72456Ca7448c73FD4,value:55}),'revert');
  });
  it('Should execute isApprovedForAll(address,address)', async () => {
    let result = await contractERC721.methods["isApprovedForAll(address,address)"](accounts[1], accounts[2],{from: accounts[0]});
  });
  it('Should execute transferFrom(address,address,uint256)', async () => {
    let result = await contractERC721.methods["transferFrom(address,address,uint256)"](accounts[6], accounts[2], 64,{from: accounts[0]});
  });
  it('Should fail transferFrom(address,address,uint256) when NOT comply with: to != 0x0000000000000000000000000000000000000000', async () => {
    let result = await truffleAssert.fails(contractERC721.methods["transferFrom(address,address,uint256)"](accounts[6], "0x0000000000000000000000000000000000000000", 64,{from: accounts[0]}),'revert');
  });
  it('Should execute safeTransferFrom(address,address,uint256)', async () => {
    let result = await contractERC721.methods["safeTransferFrom(address,address,uint256)"](accounts[8], accounts[2], 200000,{from: accounts[0]});
  });
  it('Should fail safeTransferFrom(address,address,uint256) when NOT comply with: to != 0x0000000000000000000000000000000000000000', async () => {
    let result = await truffleAssert.fails(contractERC721.methods["safeTransferFrom(address,address,uint256)"](accounts[8], "0x0000000000000000000000000000000000000000", 200000,{from: accounts[0]}),'revert');
  });
  it('Should execute safeTransferFrom(address,address,uint256,bytes)', async () => {
    let result = await contractERC721.methods["safeTransferFrom(address,address,uint256,bytes)"](accounts[7], accounts[3], "1532892062", [238,99,198,62,223,230,69,69,60,242,106,9,184,114,181,81,46,184,190,131,32,212,86,69,79,36,81,154,67,203,129,151],{from: accounts[0]});
  });
  it('Should fail safeTransferFrom(address,address,uint256,bytes) when NOT comply with: to != 0x0000000000000000000000000000000000000000', async () => {
    let result = await truffleAssert.fails(contractERC721.methods["safeTransferFrom(address,address,uint256,bytes)"](accounts[7], "0x0000000000000000000000000000000000000000", "1532892062", [238,99,198,62,223,230,69,69,60,242,106,9,184,114,181,81,46,184,190,131,32,212,86,69,79,36,81,154,67,203,129,151],{from: accounts[0]}),'revert');
  });
});
