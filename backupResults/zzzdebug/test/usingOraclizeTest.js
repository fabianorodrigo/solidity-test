const truffleAssert = require('truffle-assertions');
const Ownable = artifacts.require("Ownable");
const Pausable = artifacts.require("Pausable");
const ERC165 = artifacts.require("ERC165");
const ERC721 = artifacts.require("ERC721");
const ERC721Enumerable = artifacts.require("ERC721Enumerable");
const ERC721Metadata = artifacts.require("ERC721Metadata");
const ERC721MintableComplete = artifacts.require("ERC721MintableComplete");
const solcChecker = artifacts.require("solcChecker");
const OraclizeI = artifacts.require("OraclizeI");
const OraclizeAddrResolverI = artifacts.require("OraclizeAddrResolverI");
const Buffer = artifacts.require("Buffer");
const CBOR = artifacts.require("CBOR");
const usingOraclize = artifacts.require("usingOraclize");

contract("usingOraclize",(accounts)=>{
  let trace = false;
  let contractBuffer = null;
  let contractCBOR = null;
  let contractusingOraclize = null;
  let contractOwnable = null;
  let contractPausable = null;
  let contractERC165 = null;
  let contractERC721 = null;
  let contractERC721Enumerable = null;
  let contractERC721Metadata = null;
  let contractERC721MintableComplete = null;
  beforeEach(async () => {
    contractBuffer = await Buffer.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: Buffer.new({from: accounts[0]}');
    contractCBOR = await CBOR.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: CBOR.new({from: accounts[0]}');
    contractusingOraclize = await usingOraclize.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: usingOraclize.new({from: accounts[0]}');
    Ownable.link("Buffer",contractBuffer.address);
     Ownable.link("CBOR",contractCBOR.address);
    contractOwnable = await Ownable.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: Ownable.new({from: accounts[0]}');
    ERC721.link("Buffer",contractBuffer.address);
     ERC721.link("CBOR",contractCBOR.address);
    contractERC721 = await ERC721.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC721.new({from: accounts[0]}');
    ERC721Enumerable.link("Buffer",contractBuffer.address);
     ERC721Enumerable.link("CBOR",contractCBOR.address);
    contractERC721Enumerable = await ERC721Enumerable.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC721Enumerable.new({from: accounts[0]}');
    ERC721Metadata.link("Buffer",contractBuffer.address);
     ERC721Metadata.link("CBOR",contractCBOR.address);
    contractERC721Metadata = await ERC721Metadata.new("ERC20: burn amount exceeds allowance","\x19Ethereum Signed Message:\n32","P",{from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC721Metadata.new("ERC20: burn amount exceeds allowance","\x19Ethereum Signed Message:\n32","P",{from: accounts[0]}');
    ERC721MintableComplete.link("Buffer",contractBuffer.address);
     ERC721MintableComplete.link("CBOR",contractCBOR.address);
    contractERC721MintableComplete = await ERC721MintableComplete.new("L","",{from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC721MintableComplete.new("L","",{from: accounts[0]}');
  });
  
  it('Should execute __callback(bytes32,string)', async () => {
    let result = await contractusingOraclize.methods["__callback(bytes32,string)"]([73,121,44,185,71,222,0,188,6,243,197,75,83,72,112,42,185,80,207,229,223,234,235,218,125,29,152,73,19,60,153,225], "zhp29u",{from: accounts[0]});
  });
  it('Should execute __callback(bytes32,string,bytes)', async () => {
    let result = await contractusingOraclize.methods["__callback(bytes32,string,bytes)"]([112,104,116,187,174,182,201,17,200,151,54,140,80,88,107,101,56,215,165,168,8,34,61,135,83,58,220,72,80,33,239,115], "", [246,19,75,161,96,190,79,24,10,76,128,223,181,207,204,69,95,79,99,203,67,62,82,111,89,244,197,196,51,244,243,126],{from: accounts[0]});
  });
});
