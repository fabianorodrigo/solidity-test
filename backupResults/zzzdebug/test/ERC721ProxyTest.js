const truffleAssert = require('truffle-assertions');
const Ownable = artifacts.require("Ownable");
const Pausable = artifacts.require("Pausable");
const ERC165 = artifacts.require("ERC165");
const ERC721 = artifacts.require("ERC721");
const ERC721Enumerable = artifacts.require("ERC721Enumerable");
const ERC721Metadata = artifacts.require("ERC721Metadata");
const ERC721MintableComplete = artifacts.require("ERC721MintableComplete");
const solcChecker = artifacts.require("solcChecker");
const OraclizeI = artifacts.require("OraclizeI");
const OraclizeAddrResolverI = artifacts.require("OraclizeAddrResolverI");
const Buffer = artifacts.require("Buffer");
const CBOR = artifacts.require("CBOR");
const usingOraclize = artifacts.require("usingOraclize");
const ProxyERC721 = artifacts.require("ProxyERC721");

contract("contractProxy4TestERC721",(accounts)=>{
    let contractProxy4TestERC721 = null;
  let trace = false;
  let contractBuffer = null;
  let contractCBOR = null;
  let contractusingOraclize = null;
  let contractOwnable = null;
  let contractPausable = null;
  let contractERC165 = null;
  let contractERC721 = null;
  let contractERC721Enumerable = null;
  let contractERC721Metadata = null;
  let contractERC721MintableComplete = null;
  beforeEach(async () => {
    contractBuffer = await Buffer.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: Buffer.new({from: accounts[0]}');
    contractCBOR = await CBOR.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: CBOR.new({from: accounts[0]}');
    contractusingOraclize = await usingOraclize.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: usingOraclize.new({from: accounts[0]}');
    Ownable.link("Buffer",contractBuffer.address);
     Ownable.link("CBOR",contractCBOR.address);
    contractOwnable = await Ownable.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: Ownable.new({from: accounts[0]}');
    ERC721.link("Buffer",contractBuffer.address);
     ERC721.link("CBOR",contractCBOR.address);
    contractERC721 = await ERC721.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC721.new({from: accounts[0]}');
    ERC721Enumerable.link("Buffer",contractBuffer.address);
     ERC721Enumerable.link("CBOR",contractCBOR.address);
    contractERC721Enumerable = await ERC721Enumerable.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC721Enumerable.new({from: accounts[0]}');
    ERC721Metadata.link("Buffer",contractBuffer.address);
     ERC721Metadata.link("CBOR",contractCBOR.address);
    contractERC721Metadata = await ERC721Metadata.new("ERC20: burn amount exceeds allowance","\x19Ethereum Signed Message:\n32","P",{from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC721Metadata.new("ERC20: burn amount exceeds allowance","\x19Ethereum Signed Message:\n32","P",{from: accounts[0]}');
    ERC721MintableComplete.link("Buffer",contractBuffer.address);
     ERC721MintableComplete.link("CBOR",contractCBOR.address);
    contractERC721MintableComplete = await ERC721MintableComplete.new("L","",{from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC721MintableComplete.new("L","",{from: accounts[0]}');
      contractProxy4TestERC721 = await ProxyERC721.new({ from: accounts[0] });
});
  
  it('Should execute _exists(uint256)', async () => {
    let result = await contractProxy4TestERC721.test_exists0(22,{from: accounts[0]});
  });
  it('Should execute _isApprovedOrOwner(address,uint256)', async () => {
    let result = await contractProxy4TestERC721.test_isApprovedOrOwner0(accounts[0], "2014223716",{from: accounts[0]});
  });
  it('Should execute _mint(address,uint256)', async () => {
    let result = await contractProxy4TestERC721.test_mint0(accounts[0], 129,{from: accounts[0]});
  });
  it('Should fail _mint(address,uint256) when NOT comply with: to != 0x0000000000000000000000000000000000000000', async () => {
    let result = await truffleAssert.fails(contractProxy4TestERC721.test_mint0("0x0000000000000000000000000000000000000000", 129,{from: accounts[0]}),'revert');
  });
  it('Should execute _transferFrom(address,address,uint256)', async () => {
    let result = await contractProxy4TestERC721.test_transferFrom0(accounts[3], accounts[9], 61,{from: accounts[0]});
  });
  it('Should fail _transferFrom(address,address,uint256) when NOT comply with: to != 0x0000000000000000000000000000000000000000', async () => {
    let result = await truffleAssert.fails(contractProxy4TestERC721.test_transferFrom0(accounts[3], "0x0000000000000000000000000000000000000000", 61,{from: accounts[0]}),'revert');
  });
  it('Should execute _checkOnERC721Received(address,address,uint256,bytes)', async () => {
    let result = await contractProxy4TestERC721.test_checkOnERC721Received0(accounts[3], accounts[0], 199999, [146,130,224,69,61,103,106,55,49,128,58,241,213,137,44,147,190,178,70,103,89,215,107,102,71,74,46,187,89,199,216,82],{from: accounts[0]});
  });
});
