const truffleAssert = require('truffle-assertions');
const Ownable = artifacts.require("Ownable");
const Pausable = artifacts.require("Pausable");
const ERC165 = artifacts.require("ERC165");
const ERC721 = artifacts.require("ERC721");
const ERC721Enumerable = artifacts.require("ERC721Enumerable");
const ERC721Metadata = artifacts.require("ERC721Metadata");
const ERC721MintableComplete = artifacts.require("ERC721MintableComplete");
const solcChecker = artifacts.require("solcChecker");
const OraclizeI = artifacts.require("OraclizeI");
const OraclizeAddrResolverI = artifacts.require("OraclizeAddrResolverI");
const Buffer = artifacts.require("Buffer");
const CBOR = artifacts.require("CBOR");
const usingOraclize = artifacts.require("usingOraclize");
const ProxyCBOR = artifacts.require("ProxyCBOR");

contract("contractProxy4TestCBOR",(accounts)=>{
    let contractProxy4TestCBOR = null;
  let trace = false;
  let contractBuffer = null;
  let contractCBOR = null;
  let contractusingOraclize = null;
  let contractOwnable = null;
  let contractPausable = null;
  let contractERC165 = null;
  let contractERC721 = null;
  let contractERC721Enumerable = null;
  let contractERC721Metadata = null;
  let contractERC721MintableComplete = null;
  beforeEach(async () => {
    contractBuffer = await Buffer.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: Buffer.new({from: accounts[0]}');
    contractCBOR = await CBOR.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: CBOR.new({from: accounts[0]}');
    contractusingOraclize = await usingOraclize.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: usingOraclize.new({from: accounts[0]}');
    Ownable.link("Buffer",contractBuffer.address);
     Ownable.link("CBOR",contractCBOR.address);
    contractOwnable = await Ownable.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: Ownable.new({from: accounts[0]}');
    ERC721.link("Buffer",contractBuffer.address);
     ERC721.link("CBOR",contractCBOR.address);
    contractERC721 = await ERC721.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC721.new({from: accounts[0]}');
    ERC721Enumerable.link("Buffer",contractBuffer.address);
     ERC721Enumerable.link("CBOR",contractCBOR.address);
    contractERC721Enumerable = await ERC721Enumerable.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC721Enumerable.new({from: accounts[0]}');
    ERC721Metadata.link("Buffer",contractBuffer.address);
     ERC721Metadata.link("CBOR",contractCBOR.address);
    contractERC721Metadata = await ERC721Metadata.new("ERC20: burn amount exceeds allowance","\x19Ethereum Signed Message:\n32","P",{from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC721Metadata.new("ERC20: burn amount exceeds allowance","\x19Ethereum Signed Message:\n32","P",{from: accounts[0]}');
    ERC721MintableComplete.link("Buffer",contractBuffer.address);
     ERC721MintableComplete.link("CBOR",contractCBOR.address);
    contractERC721MintableComplete = await ERC721MintableComplete.new("L","",{from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC721MintableComplete.new("L","",{from: accounts[0]}');
      ProxyCBOR.link('CBOR', contractCBOR.address);
    contractProxy4TestCBOR = await ProxyCBOR.new({ from: accounts[0] });
});
  
  it('Should execute encodeUInt(Buffer.buffer,uint)', async () => {
    let result = await contractProxy4TestCBOR.testencodeUInt({"buf": [61,124,20,243,78,163,206,170,222,46,173,146,221,97,202,214,234,180,65,216,235,58,247,143,247,238,91,144,150,104,51,96],"capacity": 5}, 3,{from: accounts[0]});
  });
  it('Should execute encodeInt(Buffer.buffer,int) WHEN _value>=0', async () => {
    let result = await contractProxy4TestCBOR.testencodeInt({"buf": [200,98,35,230,12,70,229,103,151,162,10,204,77,147,45,185,107,36,116,49,191,155,247,46,58,170,11,176,157,15,122,190],"capacity": 200001}, 1,{from: accounts[0]});
  });
  it('Should execute encodeInt(Buffer.buffer,int) WHEN _value<0', async () => {
    let result = await contractProxy4TestCBOR.testencodeInt({"buf": [155,181,53,97,198,57,28,63,158,20,254,110,30,48,46,198,59,127,10,157,98,249,98,14,3,167,175,239,40,32,196,101],"capacity": 97}, -1,{from: accounts[0]});
  });
  it('Should execute encodeBytes(Buffer.buffer,bytes)', async () => {
    let result = await contractProxy4TestCBOR.testencodeBytes({"buf": [73,121,44,185,71,222,0,188,6,243,197,75,83,72,112,42,185,80,207,229,223,234,235,218,125,29,152,73,19,60,153,225],"capacity": 9999}, [148,35,181,40,226,177,252,166,133,12,138,41,84,122,13,126,135,170,243,153,97,248,220,220,85,135,74,153,11,87,77,244],{from: accounts[0]});
  });
  it('Should execute encodeString(Buffer.buffer,string)', async () => {
    let result = await contractProxy4TestCBOR.testencodeString({"buf": [181,164,27,158,171,184,73,7,33,204,208,101,236,171,230,207,106,82,156,170,65,53,45,127,57,132,84,97,154,39,45,93],"capacity": 26}, "P",{from: accounts[0]});
  });
  it('Should execute startArray(Buffer.buffer)', async () => {
    let result = await contractProxy4TestCBOR.teststartArray({"buf": [177,224,163,15,228,162,51,169,66,168,170,187,232,19,136,116,90,121,224,80,188,177,92,4,85,161,138,74,255,154,108,7],"capacity": 2},{from: accounts[0]});
  });
  it('Should execute startMap(Buffer.buffer)', async () => {
    let result = await contractProxy4TestCBOR.teststartMap({"buf": [112,104,116,187,174,182,201,17,200,151,54,140,80,88,107,101,56,215,165,168,8,34,61,135,83,58,220,72,80,33,239,115],"capacity": 254},{from: accounts[0]});
  });
  it('Should execute endSequence(Buffer.buffer)', async () => {
    let result = await contractProxy4TestCBOR.testendSequence({"buf": [54,252,51,236,49,45,80,156,238,213,72,162,44,168,189,239,114,184,129,81,245,208,56,27,254,218,12,235,105,101,145,247],"capacity": 1024},{from: accounts[0]});
  });
});
