const truffleAssert = require('truffle-assertions');
const Ownable = artifacts.require("Ownable");
const Pausable = artifacts.require("Pausable");
const ERC165 = artifacts.require("ERC165");
const ERC721 = artifacts.require("ERC721");
const ERC721Enumerable = artifacts.require("ERC721Enumerable");
const ERC721Metadata = artifacts.require("ERC721Metadata");
const ERC721MintableComplete = artifacts.require("ERC721MintableComplete");
const solcChecker = artifacts.require("solcChecker");
const OraclizeI = artifacts.require("OraclizeI");
const OraclizeAddrResolverI = artifacts.require("OraclizeAddrResolverI");
const Buffer = artifacts.require("Buffer");
const CBOR = artifacts.require("CBOR");
const usingOraclize = artifacts.require("usingOraclize");
const ProxyBuffer = artifacts.require("ProxyBuffer");

contract("contractProxy4TestBuffer",(accounts)=>{
    let contractProxy4TestBuffer = null;
  let trace = false;
  let contractBuffer = null;
  let contractCBOR = null;
  let contractusingOraclize = null;
  let contractOwnable = null;
  let contractPausable = null;
  let contractERC165 = null;
  let contractERC721 = null;
  let contractERC721Enumerable = null;
  let contractERC721Metadata = null;
  let contractERC721MintableComplete = null;
  beforeEach(async () => {
    contractBuffer = await Buffer.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: Buffer.new({from: accounts[0]}');
    contractCBOR = await CBOR.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: CBOR.new({from: accounts[0]}');
    contractusingOraclize = await usingOraclize.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: usingOraclize.new({from: accounts[0]}');
    Ownable.link("Buffer",contractBuffer.address);
     Ownable.link("CBOR",contractCBOR.address);
    contractOwnable = await Ownable.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: Ownable.new({from: accounts[0]}');
    ERC721.link("Buffer",contractBuffer.address);
     ERC721.link("CBOR",contractCBOR.address);
    contractERC721 = await ERC721.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC721.new({from: accounts[0]}');
    ERC721Enumerable.link("Buffer",contractBuffer.address);
     ERC721Enumerable.link("CBOR",contractCBOR.address);
    contractERC721Enumerable = await ERC721Enumerable.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC721Enumerable.new({from: accounts[0]}');
    ERC721Metadata.link("Buffer",contractBuffer.address);
     ERC721Metadata.link("CBOR",contractCBOR.address);
    contractERC721Metadata = await ERC721Metadata.new("ERC20: burn amount exceeds allowance","\x19Ethereum Signed Message:\n32","P",{from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC721Metadata.new("ERC20: burn amount exceeds allowance","\x19Ethereum Signed Message:\n32","P",{from: accounts[0]}');
    ERC721MintableComplete.link("Buffer",contractBuffer.address);
     ERC721MintableComplete.link("CBOR",contractCBOR.address);
    contractERC721MintableComplete = await ERC721MintableComplete.new("L","",{from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC721MintableComplete.new("L","",{from: accounts[0]}');
      ProxyBuffer.link('Buffer', contractBuffer.address);
    contractProxy4TestBuffer = await ProxyBuffer.new({ from: accounts[0] });
});
  
  it('Should execute init(Buffer.buffer,uint)', async () => {
    let result = await contractProxy4TestBuffer.testinit({"buf": [59,135,57,186,16,116,74,24,246,189,12,219,199,138,142,29,21,135,61,127,255,37,228,160,162,98,84,153,230,99,146,38],"capacity": 101}, 129,{from: accounts[0]});
  });
  it('Should execute append(Buffer.buffer,bytes)', async () => {
    let result = await contractProxy4TestBuffer.testappend0({"buf": [30,241,219,232,135,19,47,39,124,239,125,248,95,54,142,101,234,211,46,6,239,163,129,121,24,166,210,191,185,66,131,59],"capacity": 1}, [145,227,248,68,159,196,247,101,195,1,184,233,236,154,200,50,226,51,232,155,6,42,170,162,131,235,149,227,48,19,138,251],{from: accounts[0]});
  });
  it('Should execute append(Buffer.buffer,uint8)', async () => {
    let result = await contractProxy4TestBuffer.testappend1({"buf": [161,147,154,225,199,177,192,245,164,161,22,147,138,248,103,245,226,114,38,175,242,33,89,216,0,167,63,85,198,23,123,251],"capacity": 11}, 48,{from: accounts[0]});
  });
  it('Should execute appendInt(Buffer.buffer,uint,uint)', async () => {
    let result = await contractProxy4TestBuffer.testappendInt({"buf": [192,27,29,193,154,85,107,106,250,180,217,222,240,135,76,131,191,42,28,244,246,173,91,160,198,181,225,204,67,201,245,43],"capacity": 26}, 25, 10,{from: accounts[0]});
  });
});
