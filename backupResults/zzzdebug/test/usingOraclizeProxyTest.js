const truffleAssert = require('truffle-assertions');
const Ownable = artifacts.require("Ownable");
const Pausable = artifacts.require("Pausable");
const ERC165 = artifacts.require("ERC165");
const ERC721 = artifacts.require("ERC721");
const ERC721Enumerable = artifacts.require("ERC721Enumerable");
const ERC721Metadata = artifacts.require("ERC721Metadata");
const ERC721MintableComplete = artifacts.require("ERC721MintableComplete");
const solcChecker = artifacts.require("solcChecker");
const OraclizeI = artifacts.require("OraclizeI");
const OraclizeAddrResolverI = artifacts.require("OraclizeAddrResolverI");
const Buffer = artifacts.require("Buffer");
const CBOR = artifacts.require("CBOR");
const usingOraclize = artifacts.require("usingOraclize");
const ProxyusingOraclize = artifacts.require("ProxyusingOraclize");

contract("contractProxy4TestusingOraclize",(accounts)=>{
    let contractProxy4TestusingOraclize = null;
  let trace = false;
  let contractBuffer = null;
  let contractCBOR = null;
  let contractusingOraclize = null;
  let contractOwnable = null;
  let contractPausable = null;
  let contractERC165 = null;
  let contractERC721 = null;
  let contractERC721Enumerable = null;
  let contractERC721Metadata = null;
  let contractERC721MintableComplete = null;
  beforeEach(async () => {
    contractBuffer = await Buffer.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: Buffer.new({from: accounts[0]}');
    contractCBOR = await CBOR.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: CBOR.new({from: accounts[0]}');
    contractusingOraclize = await usingOraclize.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: usingOraclize.new({from: accounts[0]}');
    Ownable.link("Buffer",contractBuffer.address);
     Ownable.link("CBOR",contractCBOR.address);
    contractOwnable = await Ownable.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: Ownable.new({from: accounts[0]}');
    ERC721.link("Buffer",contractBuffer.address);
     ERC721.link("CBOR",contractCBOR.address);
    contractERC721 = await ERC721.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC721.new({from: accounts[0]}');
    ERC721Enumerable.link("Buffer",contractBuffer.address);
     ERC721Enumerable.link("CBOR",contractCBOR.address);
    contractERC721Enumerable = await ERC721Enumerable.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC721Enumerable.new({from: accounts[0]}');
    ERC721Metadata.link("Buffer",contractBuffer.address);
     ERC721Metadata.link("CBOR",contractCBOR.address);
    contractERC721Metadata = await ERC721Metadata.new("ERC20: burn amount exceeds allowance","\x19Ethereum Signed Message:\n32","P",{from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC721Metadata.new("ERC20: burn amount exceeds allowance","\x19Ethereum Signed Message:\n32","P",{from: accounts[0]}');
    ERC721MintableComplete.link("Buffer",contractBuffer.address);
     ERC721MintableComplete.link("CBOR",contractCBOR.address);
    contractERC721MintableComplete = await ERC721MintableComplete.new("L","",{from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC721MintableComplete.new("L","",{from: accounts[0]}');
      contractProxy4TestusingOraclize = await ProxyusingOraclize.new({ from: accounts[0] });
});
  
  it('Should execute oraclize_setNetwork(uint8)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_setNetwork0(60,{from: accounts[0]});
  });
  it('Should execute oraclize_setNetworkName(string)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_setNetworkName("1.0.0",{from: accounts[0]});
  });
  it('Should execute oraclize_getNetworkName()', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_getNetworkName({from: accounts[0]});
  });
  it('Should execute oraclize_setNetwork()', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_setNetwork1({from: accounts[0]});
  });
  it('Should execute oraclize_getPrice(string)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_getPrice0("ERC20: burn amount exceeds balance",{from: accounts[0]});
  });
  it('Should execute oraclize_getPrice(string,uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_getPrice1("ERC777: burn amount exceeds balance", 128,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,string)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query0("L", "6vx62p",{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,string)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query1(4, "ERC20: burn amount exceeds balance", "https://s3-us-west-2.amazonaws.com/udacity-blockchain/capstone/",{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,string,uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query2(86, "ERC20: burn amount exceeds balance", "ERC777: transfer amount exceeds balance", 254,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,string,uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query3("ERC20: decreased allowance below zero", "SafeERC20: decreased allowance below zero", 65,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,string,string)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query4("SafeERC20: decreased allowance below zero", "ERC20: transfer amount exceeds balance", "ERC777: burn amount exceeds balance",{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,string,string)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query5(101, "\x19Ethereum Signed Message:\n32", "SafeERC20: decreased allowance below zero", "ERC20: decreased allowance below zero",{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,string,string,uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query6(71, "ERC777: burn amount exceeds balance", "6vx62p", "1.0.0", 28,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,string,string,uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query7("SafeERC20: decreased allowance below zero", "ERC777: transfer amount exceeds balance", "ERC777: burn amount exceeds balance", 162,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,string[])', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query8("ERC20: burn amount exceeds balance", ["\x19Ethereum Signed Message:\n32","35qhe","ERC1820_ACCEPT_MAGIC","zhp29u","xnyqu5","rodl59","https://s3-us-west-2.amazonaws.com/udacity-blockchain/capstone/"],{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,string[])', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query9(69, "0", ["P"],{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,string[],uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query10(100000, "ERC20: burn amount exceeds balance", ["0","ERC20: decreased allowance below zero","xnyqu5","rodl59"], "2014223714",{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,string[],uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query11("ERC777: burn amount exceeds balance", ["rwvaz","SafeERC20: decreased allowance below zero","ERC20: transfer amount exceeds balance","itpf5o","SafeERC20: decreased allowance below zero","rodl59","xnyqu5","rodl59","0","itpf5o"], 65,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,string[1])', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query12("ERC20: burn amount exceeds balance", ["35qhe"],{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,string[1])', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query13("2014223716", "ERC20: burn amount exceeds allowance", ["ERC20: transfer amount exceeds allowance"],{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,string[1],uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query14(100000, "ERC1820_ACCEPT_MAGIC", ["rodl59"], 128,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,string[1],uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query15("P", ["0"], 69,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,string[2])', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query16("xnyqu5", ["35qhe","\x19Ethereum Signed Message:\n32"],{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,string[2])', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query17(26, "itpf5o", ["ERC20: transfer amount exceeds allowance","ERC777: transfer amount exceeds allowance"],{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,string[2],uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query18(27, "ERC20: burn amount exceeds balance", ["L","rwvaz"], 58,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,string[2],uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query19("", ["rodl59","ERC20: transfer amount exceeds balance"], 26,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,string[3])', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query20("1.0.0", ["1.0.0","xnyqu5","ERC777: transfer amount exceeds balance"],{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,string[3])', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query21("2014223715", "SafeERC20: decreased allowance below zero", ["ijv1oy","ijv1oy","rwvaz"],{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,string[3],uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query22(60, "ERC1820_ACCEPT_MAGIC", ["SafeERC20: decreased allowance below zero","ERC20: transfer amount exceeds balance","P"], 71,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,string[3],uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query23("rwvaz", ["L","35qhe","ERC777: transfer amount exceeds allowance"], 69,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,string[4])', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query24("ERC20: decreased allowance below zero", ["ERC20: transfer amount exceeds allowance","ERC777: transfer amount exceeds allowance","ERC20: decreased allowance below zero","https://s3-us-west-2.amazonaws.com/udacity-blockchain/capstone/"],{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,string[4])', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query25("1532892062", "https://s3-us-west-2.amazonaws.com/udacity-blockchain/capstone/", ["6vx62p","ijv1oy","xnyqu5","ERC20: burn amount exceeds balance"],{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,string[4],uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query26(4, "rwvaz", ["https://s3-us-west-2.amazonaws.com/udacity-blockchain/capstone/","https://s3-us-west-2.amazonaws.com/udacity-blockchain/capstone/","https://s3-us-west-2.amazonaws.com/udacity-blockchain/capstone/",""], 98,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,string[4],uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query27("965yf", ["ERC777: transfer amount exceeds balance","xnyqu5","965yf","P"], 86,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,string[5])', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query28("ERC20: decreased allowance below zero", ["SafeERC20: decreased allowance below zero","rwvaz","itpf5o","P","ERC20: transfer amount exceeds allowance"],{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,string[5])', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query29(28, "6vx62p", ["ERC1820_ACCEPT_MAGIC","SafeERC20: decreased allowance below zero","ERC777: transfer amount exceeds allowance","ERC777: transfer amount exceeds allowance","ERC777: transfer amount exceeds balance"],{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,string[5],uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query30(4, "\x19Ethereum Signed Message:\n32", ["rwvaz","L","zhp29u","rodl59",""], 66,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,string[5],uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query31("ERC20: decreased allowance below zero", ["ERC777: transfer amount exceeds allowance","SafeERC20: decreased allowance below zero","ERC777: transfer amount exceeds balance","SafeERC20: decreased allowance below zero","ERC777: transfer amount exceeds allowance"], 15,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,bytes[])', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query32("zhp29u", [[7,93,58,56,87,113,203,165,250,67,88,162,112,234,243,109,72,9,5,104,225,144,158,144,246,106,184,165,83,0,79,119],[162,104,122,6,248,216,78,233,141,218,236,161,119,191,104,197,141,196,207,87,182,105,172,207,12,57,82,147,188,105,168,88],[202,159,130,36,224,211,41,124,165,63,204,59,212,192,191,2,47,124,179,252,208,111,120,7,190,68,108,164,228,183,47,24],[90,108,235,116,72,233,137,50,176,52,29,95,170,77,218,120,240,145,243,210,253,26,72,58,78,26,18,191,172,86,8,21],[167,183,233,131,43,49,95,144,239,187,227,165,229,13,147,252,153,144,57,116,48,232,29,27,134,52,33,215,151,224,100,238],[86,187,229,119,210,235,180,33,159,37,105,30,91,188,204,228,35,179,230,152,114,210,140,38,220,167,243,115,225,245,80,53],[72,193,47,145,131,4,236,109,149,14,74,169,112,21,205,40,49,248,20,85,9,2,197,166,84,79,58,178,35,49,1,95]],{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,bytes[])', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query33(33, "ERC20: transfer amount exceeds balance", [[189,141,231,103,35,199,158,199,177,162,89,226,78,204,117,167,191,11,104,3,65,16,130,162,254,233,24,118,63,239,255,137]],{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,bytes[],uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query34(99999, "965yf", [[30,91,163,75,80,186,122,82,243,21,77,246,210,62,3,13,201,180,195,80,35,85,73,164,148,90,46,74,102,23,255,182],[50,140,5,16,10,36,171,83,152,163,73,3,105,74,186,59,49,221,54,47,22,187,95,222,183,71,228,170,137,186,102,241],[217,59,106,6,125,115,129,24,120,43,62,242,49,21,0,176,207,124,206,159,103,226,147,119,50,214,212,194,90,212,64,75],[64,125,233,121,179,163,245,47,105,244,225,18,148,104,200,79,216,135,64,244,205,157,102,14,145,199,193,97,177,55,63,204],[199,20,40,108,41,73,236,114,230,81,255,201,255,188,213,133,42,101,213,80,171,115,94,229,76,192,47,16,37,76,102,189],[231,89,194,182,151,67,75,204,118,209,117,238,125,97,14,105,97,180,113,126,196,13,101,203,230,147,107,187,156,192,204,140],[246,36,174,178,118,236,20,159,162,115,140,58,103,177,50,167,138,61,222,37,13,189,54,200,109,92,50,153,2,228,4,63]], 33,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,bytes[],uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query35("ERC20: burn amount exceeds balance", [[188,232,255,95,189,49,40,245,143,16,234,143,68,199,86,55,245,76,29,68,159,118,177,137,64,233,217,246,199,155,14,61],[89,129,1,102,183,189,209,164,61,142,245,117,117,18,32,28,226,182,253,23,198,3,220,80,161,209,143,100,159,108,56,226],[48,115,15,48,190,162,51,124,24,177,67,249,3,248,78,228,228,142,219,213,92,4,67,242,232,8,28,71,107,225,234,172],[51,52,44,253,58,132,149,239,52,82,253,0,26,56,250,81,36,143,203,152,53,182,2,31,114,233,138,47,91,34,177,49],[17,157,4,109,9,135,14,149,21,146,139,155,250,56,200,196,253,84,9,147,50,118,8,98,35,251,107,172,191,114,149,216],[248,56,43,36,62,171,243,71,73,110,129,199,93,169,186,196,111,227,253,184,72,157,22,76,220,40,33,186,244,33,214,216],[17,10,68,44,34,86,156,163,162,121,226,110,94,76,176,7,195,199,95,242,15,238,20,196,147,198,102,1,162,115,146,210],[112,43,91,129,17,29,187,185,106,199,73,201,114,230,255,220,56,254,202,52,15,0,23,165,200,50,202,118,154,49,137,38],[237,240,46,199,238,136,193,247,0,73,23,123,49,115,155,252,89,205,53,189,37,128,27,6,108,14,139,47,205,232,48,15],[78,219,89,27,65,46,23,249,226,223,206,204,54,201,216,155,32,83,178,104,142,104,197,38,195,98,152,49,99,0,97,88]], 12,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,bytes[1])', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query36("", [[99,179,185,12,26,23,61,153,99,83,193,188,6,225,250,43,194,156,69,148,29,89,245,85,15,180,140,103,35,10,183,3]],{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,bytes[1])', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query37(257, "P", [[75,252,137,213,120,199,239,214,131,226,82,55,247,239,221,95,238,98,14,34,149,205,207,63,217,31,236,233,184,0,228,136]],{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,bytes[1],uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query38(30, "itpf5o", [[86,93,120,89,81,117,45,167,231,99,149,79,253,186,197,185,200,129,73,3,223,70,59,226,142,231,180,65,94,72,28,15]], 56,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,bytes[1],uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query39("0", [[47,25,50,142,144,61,192,15,241,252,128,201,68,82,246,117,180,110,43,178,111,22,121,103,105,85,45,8,227,134,205,217]], 48,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,bytes[2])', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query40("1.0.0", [[100,207,56,239,64,43,114,108,36,191,45,180,206,28,152,179,47,150,157,22,164,25,111,103,136,126,187,67,251,136,146,45],[130,246,206,152,112,111,130,217,254,103,242,221,32,100,201,219,160,154,166,189,232,247,99,25,213,249,105,185,62,95,11,113]],{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,bytes[2])', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query41(102, "35qhe", [[179,95,244,81,51,230,139,57,215,243,248,17,200,126,13,105,253,236,23,191,215,242,119,102,129,197,186,147,204,180,65,192],[70,217,122,235,26,56,225,248,253,66,131,151,237,91,80,69,19,125,131,45,60,238,217,204,1,47,244,159,136,165,125,119]],{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,bytes[2],uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query42(127, "ERC777: transfer amount exceeds allowance", [[111,199,13,117,104,236,145,162,227,91,70,119,183,14,136,18,215,129,178,31,76,201,143,250,100,54,210,46,196,219,155,170],[181,118,28,143,228,22,32,74,2,60,229,218,237,44,14,78,63,136,149,240,75,5,79,131,137,156,227,233,185,125,119,229]], 1025,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,bytes[2],uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query43("ERC20: decreased allowance below zero", [[215,184,160,6,22,153,48,109,1,187,184,182,100,114,236,99,188,106,172,100,169,196,89,14,205,43,66,52,19,100,227,6],[104,220,98,172,243,200,223,72,60,103,194,124,105,58,210,28,230,188,213,37,2,108,222,85,20,168,32,8,73,133,0,74]], 1023,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,bytes[3])', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query44("https://s3-us-west-2.amazonaws.com/udacity-blockchain/capstone/", [[123,192,132,74,33,98,30,24,172,226,175,159,198,137,186,169,232,75,59,235,232,224,46,37,161,244,119,123,141,52,3,140],[29,248,64,222,151,77,99,95,157,173,134,12,148,58,8,198,156,82,31,219,115,66,89,248,136,97,59,108,253,231,162,111],[240,30,193,119,161,68,196,207,129,166,209,202,210,52,223,227,46,128,17,99,133,76,125,185,25,162,156,189,107,44,51,170]],{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,bytes[3])', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query45(1025, "6vx62p", [[111,47,40,152,120,241,66,249,75,50,178,235,81,216,234,222,37,143,67,94,52,180,171,249,2,79,144,202,6,95,172,82],[4,172,203,118,141,246,213,27,79,189,167,64,88,126,130,211,6,127,228,6,210,156,248,29,49,248,134,20,49,93,61,243],[12,123,23,40,106,71,154,87,134,63,226,233,189,29,146,133,199,9,135,124,121,68,76,212,89,157,146,137,206,94,164,200]],{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,bytes[3],uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query46(30, "https://s3-us-west-2.amazonaws.com/udacity-blockchain/capstone/", [[35,179,67,71,8,107,186,139,164,172,94,6,53,254,205,203,184,230,222,174,32,253,37,172,193,64,34,5,188,14,134,91],[65,190,22,185,127,96,159,172,50,254,147,180,250,77,26,138,158,170,205,246,195,5,22,49,198,16,19,136,43,228,231,188],[76,237,23,204,144,92,166,188,7,13,215,203,20,154,81,115,198,19,23,248,86,147,228,122,142,142,190,223,101,110,0,10]], 60,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,bytes[3],uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query47("", [[178,68,139,140,94,58,168,161,159,196,51,86,56,151,195,16,164,201,95,219,157,157,28,236,18,171,192,62,107,216,195,121],[34,178,63,242,104,138,43,114,186,44,62,255,1,164,176,38,13,81,228,208,205,175,54,6,52,74,58,16,20,245,180,1],[234,72,145,248,135,39,243,252,99,174,52,9,195,194,133,245,74,85,87,25,76,99,228,224,120,47,127,145,157,55,184,159]], 55,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,bytes[4])', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query48("ERC20: decreased allowance below zero", [[243,233,52,20,139,18,57,22,246,32,206,56,65,127,122,21,231,19,25,101,185,86,102,177,56,194,170,159,138,215,167,142],[239,146,245,76,77,202,83,229,202,126,225,24,236,8,15,51,140,24,87,162,110,153,249,42,242,220,226,178,109,77,227,207],[4,86,106,177,120,225,8,44,119,190,224,159,164,9,50,210,57,201,86,60,73,203,59,99,44,38,132,123,23,68,217,245],[119,108,36,220,243,133,51,190,247,143,36,176,164,66,232,217,74,52,201,228,253,33,200,101,48,179,168,20,189,209,40,158]],{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,bytes[4])', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query49("2014223716", "0", [[105,144,76,114,103,34,121,70,142,94,229,184,171,218,6,187,200,35,204,114,0,247,175,132,149,18,42,12,166,27,206,36],[217,213,32,253,221,237,25,85,227,239,194,48,180,99,205,214,77,59,2,21,142,197,137,158,151,42,103,3,228,165,6,211],[241,3,49,124,27,40,224,139,96,123,87,62,46,143,243,46,78,126,202,91,191,195,142,237,19,250,45,161,185,92,183,209],[190,48,14,109,41,5,81,167,204,156,41,195,75,199,163,127,234,148,205,204,179,215,140,100,47,252,62,218,128,158,105,122]],{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,bytes[4],uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query50(1025, "ERC777: transfer amount exceeds balance", [[104,124,239,134,42,199,124,115,190,162,141,86,45,159,126,2,152,87,111,230,158,243,108,184,106,46,125,105,85,107,54,38],[3,66,249,16,21,236,156,180,172,241,176,248,203,149,118,173,255,240,105,45,186,201,254,86,31,253,103,107,10,134,205,209],[157,32,130,201,63,18,18,173,180,75,183,9,8,165,236,231,146,1,33,67,204,172,92,70,122,150,76,166,64,19,12,106],[233,47,32,154,51,28,172,174,127,255,219,78,155,241,169,18,176,98,91,212,220,197,39,126,83,236,53,213,77,85,135,153]], 160,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,bytes[4],uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query51("965yf", [[78,159,29,151,140,18,109,18,145,100,1,37,36,97,249,237,50,240,115,91,80,237,217,147,116,132,213,158,195,3,215,91],[194,65,30,206,64,183,139,78,156,129,40,81,29,205,182,38,176,81,15,100,144,81,1,232,255,222,226,48,71,75,41,136],[227,214,87,141,167,72,37,63,20,171,84,195,82,230,199,219,50,74,179,46,99,119,225,49,230,4,87,17,222,204,48,145],[31,90,28,82,25,240,29,154,62,174,235,163,17,68,28,180,176,84,151,187,209,41,142,144,253,217,6,224,176,14,52,218]], "2014223715",{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,bytes[5])', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query52("xnyqu5", [[90,193,102,185,179,202,192,29,18,99,35,140,204,70,146,59,209,42,54,96,16,248,94,48,88,39,10,184,170,65,99,25],[86,188,101,129,33,146,44,209,168,223,80,154,153,12,133,35,119,219,87,121,81,68,103,47,91,246,127,179,63,223,72,238],[30,27,81,18,92,69,240,10,112,174,230,45,44,208,195,125,126,112,34,187,133,151,129,212,5,183,246,183,75,88,13,31],[34,62,163,67,209,17,83,64,197,49,80,95,181,147,245,228,180,1,202,168,131,181,201,155,104,243,82,95,206,221,152,76],[184,120,244,193,140,20,60,101,47,141,111,123,123,217,28,208,220,194,243,131,61,112,253,92,26,24,15,108,190,11,244,102]],{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,bytes[5])', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query53(1025, "rwvaz", [[220,23,93,18,67,170,240,154,107,227,249,207,16,218,30,243,22,224,139,45,245,104,41,54,110,225,3,39,228,236,183,147],[160,16,165,81,214,62,114,239,238,10,210,57,112,131,22,240,237,153,31,74,142,226,84,118,189,205,205,209,232,9,0,127],[140,71,198,237,108,70,255,118,108,114,185,72,32,30,107,211,159,185,136,117,89,209,61,168,45,232,13,121,232,211,244,221],[114,231,64,130,83,183,245,183,188,85,198,49,64,139,33,5,146,85,149,10,122,18,131,147,131,95,11,0,239,130,181,13],[11,38,233,186,5,254,17,109,112,159,13,24,39,197,13,214,68,232,93,124,239,65,45,185,46,204,119,99,28,195,40,168]],{from: accounts[0]});
  });
  it('Should execute oraclize_query(uint,string,bytes[5],uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query54(97, "xnyqu5", [[195,223,136,164,155,145,163,241,125,70,23,141,12,77,105,89,240,253,253,72,113,171,166,147,191,198,45,122,76,54,216,239],[179,68,166,54,142,102,164,114,13,86,93,71,79,110,234,122,37,23,40,237,5,83,32,117,227,90,33,93,237,1,15,41],[11,185,250,101,165,12,66,215,1,132,239,252,56,153,196,138,152,144,8,166,130,233,111,140,41,187,98,31,201,68,150,202],[172,215,123,204,242,181,146,203,23,184,228,97,250,109,170,6,128,44,123,235,96,54,251,170,149,224,112,33,129,82,149,44],[97,54,102,131,34,101,68,66,88,189,82,70,255,18,164,231,184,13,4,73,128,203,200,179,160,200,196,111,144,199,62,154]], 1,{from: accounts[0]});
  });
  it('Should execute oraclize_query(string,bytes[5],uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_query55("ERC20: transfer amount exceeds allowance", [[4,202,119,83,100,240,96,95,82,1,249,99,175,16,50,2,163,227,187,234,93,150,163,96,206,176,48,184,51,207,157,28],[97,73,142,20,125,68,198,84,71,93,165,80,200,250,174,223,10,225,2,59,0,226,55,13,224,194,102,58,129,160,240,212],[225,68,24,153,154,207,32,172,99,51,94,200,110,28,207,236,86,208,62,59,174,210,130,95,200,189,210,194,14,14,111,148],[171,38,208,84,113,158,1,255,67,121,223,191,228,9,243,192,71,212,31,15,19,231,10,113,131,143,204,3,182,151,179,39],[117,8,76,110,77,85,61,100,67,41,111,86,63,104,211,144,8,3,47,226,26,236,26,132,234,241,210,3,73,89,142,166]], 27,{from: accounts[0]});
  });
  it('Should execute oraclize_setProof(byte)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_setProof([225],{from: accounts[0]});
  });
  it('Should execute oraclize_cbAddress()', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_cbAddress({from: accounts[0]});
  });
  it('Should execute getCodeSize(address)', async () => {
    let result = await contractProxy4TestusingOraclize.testgetCodeSize(accounts[1],{from: accounts[0]});
  });
  it('Should execute oraclize_setCustomGasPrice(uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_setCustomGasPrice(55,{from: accounts[0]});
  });
  it('Should execute oraclize_randomDS_getSessionPubKeyHash()', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_randomDS_getSessionPubKeyHash({from: accounts[0]});
  });
  it('Should execute parseAddr(string)', async () => {
    let result = await contractProxy4TestusingOraclize.testparseAddr("ERC20: transfer amount exceeds allowance",{from: accounts[0]});
  });
  it('Should execute strCompare(string,string)', async () => {
    let result = await contractProxy4TestusingOraclize.teststrCompare("ERC20: burn amount exceeds balance", "965yf",{from: accounts[0]});
  });
  it('Should execute indexOf(string,string)', async () => {
    let result = await contractProxy4TestusingOraclize.testindexOf("xnyqu5", "6vx62p",{from: accounts[0]});
  });
  it('Should execute strConcat(string,string)', async () => {
    let result = await contractProxy4TestusingOraclize.teststrConcat0("zhp29u", "ijv1oy",{from: accounts[0]});
  });
  it('Should execute strConcat(string,string,string)', async () => {
    let result = await contractProxy4TestusingOraclize.teststrConcat1("xnyqu5", "ERC20: decreased allowance below zero", "xnyqu5",{from: accounts[0]});
  });
  it('Should execute strConcat(string,string,string,string)', async () => {
    let result = await contractProxy4TestusingOraclize.teststrConcat2("", "", "zhp29u", "https://s3-us-west-2.amazonaws.com/udacity-blockchain/capstone/",{from: accounts[0]});
  });
  it('Should execute strConcat(string,string,string,string,string)', async () => {
    let result = await contractProxy4TestusingOraclize.teststrConcat3("ERC20: transfer amount exceeds allowance", "https://s3-us-west-2.amazonaws.com/udacity-blockchain/capstone/", "0", "ijv1oy", "ERC777: transfer amount exceeds allowance",{from: accounts[0]});
  });
  it('Should execute safeParseInt(string)', async () => {
    let result = await contractProxy4TestusingOraclize.testsafeParseInt0("0",{from: accounts[0]});
  });
  it('Should execute safeParseInt(string,uint) WHEN _b>0', async () => {
    let result = await contractProxy4TestusingOraclize.testsafeParseInt1("itpf5o", 10000,{from: accounts[0]});
  });
  it('Should execute safeParseInt(string,uint) WHEN _b<=0', async () => {
    let result = await contractProxy4TestusingOraclize.testsafeParseInt1("ERC777: transfer amount exceeds balance", 0,{from: accounts[0]});
  });
  it('Should execute parseInt(string)', async () => {
    let result = await contractProxy4TestusingOraclize.testparseInt0("rwvaz",{from: accounts[0]});
  });
  it('Should execute parseInt(string,uint) WHEN _b>0', async () => {
    let result = await contractProxy4TestusingOraclize.testparseInt1("\x19Ethereum Signed Message:\n32", 10,{from: accounts[0]});
  });
  it('Should execute parseInt(string,uint) WHEN _b<=0', async () => {
    let result = await contractProxy4TestusingOraclize.testparseInt1("rwvaz", 0,{from: accounts[0]});
  });
  it('Should execute uint2str(uint) WHEN _i==0', async () => {
    let result = await contractProxy4TestusingOraclize.testuint2str(0,{from: accounts[0]});
  });
  it('Should execute uint2str(uint) WHEN _i!=0', async () => {
    let result = await contractProxy4TestusingOraclize.testuint2str("1532892064",{from: accounts[0]});
  });
  it('Should execute stra2cbor(string[])', async () => {
    let result = await contractProxy4TestusingOraclize.teststra2cbor(["ERC777: transfer amount exceeds allowance","qoebo","35qhe","ERC1820_ACCEPT_MAGIC","itpf5o","ERC20: transfer amount exceeds balance","0","zhp29u"],{from: accounts[0]});
  });
  it('Should execute ba2cbor(bytes[])', async () => {
    let result = await contractProxy4TestusingOraclize.testba2cbor([[67,254,145,117,133,195,121,78,153,76,72,33,115,65,237,52,40,41,14,105,239,204,173,112,13,209,48,253,7,8,126,127],[120,192,126,60,132,241,125,206,46,78,86,38,47,57,101,171,100,37,227,206,110,171,44,255,155,113,132,231,31,15,187,119],[82,114,198,180,171,178,238,166,216,241,24,127,69,206,168,19,182,121,242,189,246,184,124,34,249,236,69,180,255,57,228,223],[189,58,160,43,26,240,85,82,127,69,190,141,196,165,248,165,152,56,16,183,98,134,106,245,12,200,168,222,75,77,253,147],[150,9,111,171,2,227,176,15,162,57,66,178,113,76,12,136,153,57,137,255,134,162,208,35,62,118,83,161,239,221,243,167],[68,193,90,231,194,119,103,182,85,4,104,129,239,22,128,239,94,106,197,71,223,116,46,165,22,252,154,155,140,66,153,146],[71,191,59,8,197,86,85,81,176,178,192,36,28,60,118,135,194,227,212,75,203,193,98,193,227,209,73,125,181,13,96,16],[189,149,237,2,72,153,53,29,240,47,73,51,126,30,191,227,4,233,40,26,126,128,245,55,253,8,111,95,97,43,237,128],[191,79,141,44,84,76,57,84,45,192,159,173,15,44,32,227,66,244,244,68,151,88,164,45,5,50,29,215,181,138,195,121]],{from: accounts[0]});
  });
  it('Should execute oraclize_newRandomDSQuery(uint,uint,uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_newRandomDSQuery(46, 29999, 17,{from: accounts[0]});
  });
  it('Should execute oraclize_randomDS_setCommitment(bytes32,bytes32)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_randomDS_setCommitment([86,187,229,119,210,235,180,33,159,37,105,30,91,188,204,228,35,179,230,152,114,210,140,38,220,167,243,115,225,245,80,53], [3,66,249,16,21,236,156,180,172,241,176,248,203,149,118,173,255,240,105,45,186,201,254,86,31,253,103,107,10,134,205,209],{from: accounts[0]});
  });
  it('Should execute verifySig(bytes32,bytes,bytes)', async () => {
    let result = await contractProxy4TestusingOraclize.testverifySig([29,248,64,222,151,77,99,95,157,173,134,12,148,58,8,198,156,82,31,219,115,66,89,248,136,97,59,108,253,231,162,111], [150,208,165,206,48,36,235,164,221,81,231,174,180,154,151,252,5,173,2,57,1,242,194,87,15,161,78,62,203,16,209,23], [196,2,219,75,200,252,250,208,38,239,100,0,39,184,176,44,23,135,173,139,84,169,41,188,127,96,156,253,4,133,90,46],{from: accounts[0]});
  });
  it('Should execute oraclize_randomDS_proofVerify__sessionKeyValidity(bytes,uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_randomDS_proofVerify__sessionKeyValidity([155,120,50,237,220,120,190,47,220,151,178,106,0,54,141,102,133,24,82,227,24,96,233,199,209,159,214,48,180,190,57,226], 0,{from: accounts[0]});
  });
  it('Should execute oraclize_randomDS_proofVerify__returnCode(bytes32,string,bytes)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_randomDS_proofVerify__returnCode([179,95,244,81,51,230,139,57,215,243,248,17,200,126,13,105,253,236,23,191,215,242,119,102,129,197,186,147,204,180,65,192], "ERC20: transfer amount exceeds balance", [253,53,135,115,20,152,115,65,136,118,163,157,255,233,24,116,59,154,42,203,126,81,217,162,98,28,176,28,98,60,66,245],{from: accounts[0]});
  });
  it('Should execute matchBytes32Prefix(bytes32,bytes,uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testmatchBytes32Prefix([48,115,15,48,190,162,51,124,24,177,67,249,3,248,78,228,228,142,219,213,92,4,67,242,232,8,28,71,107,225,234,172], [202,177,102,140,145,136,255,180,11,158,92,33,33,203,65,244,72,159,67,241,88,117,109,233,74,62,12,122,171,55,176,161], 32,{from: accounts[0]});
  });
  it('Should fail matchBytes32Prefix(bytes32,bytes,uint) when NOT comply with: _prefix.length == _nRandomBytes', async () => {
    let result = await truffleAssert.fails(contractProxy4TestusingOraclize.testmatchBytes32Prefix([48,115,15,48,190,162,51,124,24,177,67,249,3,248,78,228,228,142,219,213,92,4,67,242,232,8,28,71,107,225,234,172], [236,98,46,202,67,167,87,32,32,97,33,39,93,145,216,74,178,152,164,217,133,22,167,179,125,146,114,166,21,183,148,212,40], 32,{from: accounts[0]}),'revert');
  });
  it('Should execute oraclize_randomDS_proofVerify__main(bytes,bytes32,bytes,string)', async () => {
    let result = await contractProxy4TestusingOraclize.testoraclize_randomDS_proofVerify__main([194,80,127,246,137,132,89,95,182,150,10,200,234,253,204,171,11,54,25,9,82,49,135,238,191,234,98,108,212,125,9,213], [17,157,4,109,9,135,14,149,21,146,139,155,250,56,200,196,253,84,9,147,50,118,8,98,35,251,107,172,191,114,149,216], [126,191,233,63,203,203,94,161,203,22,226,150,146,164,226,70,69,122,118,17,16,226,151,81,82,140,22,51,109,125,60,32], "ERC20: transfer amount exceeds balance",{from: accounts[0]});
  });
  it('Should execute copyBytes(bytes,uint,uint,bytes,uint)', async () => {
    let result = await contractProxy4TestusingOraclize.testcopyBytes([112,15,166,135,176,16,25,59,78,178,221,74,193,38,219,71,52,103,54,3,183,217,53,45,92,125,16,210,72,206,150,81], 28, 8, [210,128,202,50,164,94,40,156,134,232,183,158,71,241,204,105,55,195,110,181,220,40,78,2,137,254,44,240,150,240,228,104], 256,{from: accounts[0]});
  });
  it('Should execute safer_ecrecover(bytes32,uint8,bytes32,bytes32)', async () => {
    let result = await contractProxy4TestusingOraclize.testsafer_ecrecover([30,241,219,232,135,19,47,39,124,239,125,248,95,54,142,101,234,211,46,6,239,163,129,121,24,166,210,191,185,66,131,59], 71, [217,59,106,6,125,115,129,24,120,43,62,242,49,21,0,176,207,124,206,159,103,226,147,119,50,214,212,194,90,212,64,75], [123,192,132,74,33,98,30,24,172,226,175,159,198,137,186,169,232,75,59,235,232,224,46,37,161,244,119,123,141,52,3,140],{from: accounts[0]});
  });
  it('Should execute ecrecovery(bytes32,bytes) WHEN _sig.length!=65', async () => {
    let result = await contractProxy4TestusingOraclize.testecrecovery([30,27,81,18,92,69,240,10,112,174,230,45,44,208,195,125,126,112,34,187,133,151,129,212,5,183,246,183,75,88,13,31], [220,44,108,15,177,184,243,66,54,110,242,255,158,221,32,51,151,68,82],{from: accounts[0]});
  });
  it('Should execute ecrecovery(bytes32,bytes) WHEN _sig.length==65', async () => {
    let result = await contractProxy4TestusingOraclize.testecrecovery([191,79,141,44,84,76,57,84,45,192,159,173,15,44,32,227,66,244,244,68,151,88,164,45,5,50,29,215,181,138,195,121], [91,146,163,229,217,124,90,193,191,241,182,20,60,126,2,222,188,81,112,62,13,187,220,83,98,229,205,25,70,172,106,117,95,10,255,245,176,138,239,147,162,209,200,16,108,172,52,120,195,69,236,83,175,118,156,221,103,47,57,225,46,237,247,183,253],{from: accounts[0]});
  });
  it('Should execute safeMemoryCleaner()', async () => {
    let result = await contractProxy4TestusingOraclize.testsafeMemoryCleaner({from: accounts[0]});
  });
});
