const TestContract = artifacts.require("TestBN256G2");
  const LibraryUnderTest = artifacts.require("BN256G2");

   async function doDeploy(deployer, network) {
       await deployer.deploy(LibraryUnderTest);
       await deployer.link(LibraryUnderTest, TestContract);
       await deployer.deploy(TestContract);
   }

 module.exports = (deployer, network) => {
      deployer.then(async () => {
              await doDeploy(deployer, network);
      });
  };