const TestContract = artifacts.require("TestPairing");
  const LibraryUnderTest = artifacts.require("Pairing");
  const BN256G2 = artifacts.require("BN256G2");


   async function doDeploy(deployer, network) {
       await deployer.deploy(LibraryUnderTest);
       await deployer.link(LibraryUnderTest, TestContract);
       await deployer.deploy(BN256G2);
       await deployer.link(BN256G2,TestContract);
       await deployer.deploy(TestContract);
   }

 module.exports = (deployer, network) => {
      deployer.then(async () => {
              await doDeploy(deployer, network);
      });
  };
