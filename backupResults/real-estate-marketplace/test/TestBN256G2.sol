pragma solidity ^0.5.0;
import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/ERC721Mintable.sol";
import "../contracts/Oraclize.sol";
import "../contracts/SolnSquareVerifier.sol";
import "../contracts/Verifier.sol";

contract TestBN256G2  {
uint nonce = 86;
function stringToBytes32(string memory source) public returns (bytes32 result) {
    bytes memory tempEmptyStringTest = bytes(source);
    if (tempEmptyStringTest.length == 0) {
        return 0x0;
    }

    assembly {
        result := mload(add(source, 32))
    }
}
  using BN256G2 for uint256;

  Buffer.buffer varbuffer;
  SolnSquareVerifier.Solution varSolution;
  Pairing.G1Point varG1Point;
  Pairing.G2Point varG2Point;
  Verifier.VerifyingKey varVerifyingKey;
  Verifier.Proof varProof;
  
  //Should execute ECTwistAdd WHEN pt1xx==0,pt1xy==0,pt1yx==0,pt1yy==0
  function test_ECTwistAdd0() public {
    uint256   param0 = 0;
    uint256   param1 = 0;
    uint256   param2 = 0;
    uint256   param3 = 0;
    uint256   param4 = 30;
    uint256   param5 = 26;
    uint256   param6 = 87;
    uint256   param7 = 1532892063;
    uint256   result0;
    uint256   result1;
    uint256   result2;
    uint256   result3;
    (result0, result1, result2, result3) = BN256G2.ECTwistAdd(param0, param1, param2, param3, param4, param5, param6, param7);
  }
  //Should execute ECTwistAdd WHEN pt2xx==0,pt2xy==0,pt2yx==0,pt2yy==0
  function test_ECTwistAdd1() public {
    uint256   param0 = 30;
    uint256   param1 = 56;
    uint256   param2 = 1532892064;
    uint256   param3 = 11;
    uint256   param4 = 0;
    uint256   param5 = 0;
    uint256   param6 = 0;
    uint256   param7 = 0;
    uint256   result0;
    uint256   result1;
    uint256   result2;
    uint256   result3;
    (result0, result1, result2, result3) = BN256G2.ECTwistAdd(param0, param1, param2, param3, param4, param5, param6, param7);
  }
  //Should execute ECTwistAdd WHEN pt2yx!=0,pt1yy!=0
  function test_ECTwistAdd2() public {
    uint256   param0 = 257;
    uint256   param1 = 160;
    uint256   param2 = 65;
    uint256   param3 = 1023;
    uint256   param4 = 97;
    uint256   param5 = 87;
    uint256   param6 = 1;
    uint256   param7 = 1025;
    uint256   result0;
    uint256   result1;
    uint256   result2;
    uint256   result3;
    (result0, result1, result2, result3) = BN256G2.ECTwistAdd(param0, param1, param2, param3, param4, param5, param6, param7);
  }
  //Should execute ECTwistMul WHEN pt1xx==0,pt1xy==0,pt1yx==0,pt1yy==0
  function test_ECTwistMul0() public {
    uint256   param0 = 63;
    uint256   param1 = 0;
    uint256   param2 = 0;
    uint256   param3 = 0;
    uint256   param4 = 0;
    uint256   result0;
    uint256   result1;
    uint256   result2;
    uint256   result3;
    (result0, result1, result2, result3) = BN256G2.ECTwistMul(param0, param1, param2, param3, param4);
  }
  //Should execute ECTwistMul WHEN pt1xy!=0
  function test_ECTwistMul1() public {
    uint256   param0 = 26;
    uint256   param1 = 20;
    uint256   param2 = 24;
    uint256   param3 = 21;
    uint256   param4 = 26;
    uint256   result0;
    uint256   result1;
    uint256   result2;
    uint256   result3;
    (result0, result1, result2, result3) = BN256G2.ECTwistMul(param0, param1, param2, param3, param4);
  }
  //Should execute GetFieldModulus
  function test_GetFieldModulus() public {
    uint256   result0;
    result0 = BN256G2.GetFieldModulus();
  }
  //Should execute submod
  function test_submod() public {
    uint256   varLib = 1023;
  varLib.submod(200000, 48);
  }
  //Should execute _FQ2Mul
  function test__FQ2Mul() public {
    uint256   varLib = 200001;
  varLib._FQ2Mul(63, 7, 16);
  }
  //Should execute _FQ2Muc
  function test__FQ2Muc() public {
    uint256   varLib = 24;
  varLib._FQ2Muc(2014223714, 26);
  }
  //Should execute _FQ2Add
  function test__FQ2Add() public {
    uint256   varLib = 128;
  varLib._FQ2Add(17, 1024, 3);
  }
  //Should execute _FQ2Sub
  function test__FQ2Sub() public {
    uint256   varLib = 160;
  varLib._FQ2Sub(28, 200001, 57);
  }
  //Should execute _FQ2Div
  function test__FQ2Div() public {
    uint256   varLib = 101;
  varLib._FQ2Div(161, 3, 88);
  }
  //Should execute _FQ2Inv
  function test__FQ2Inv() public {
    uint256   varLib = 1023;
  varLib._FQ2Inv(45);
  }
  //Should execute _isOnCurve
  function test__isOnCurve() public {
    uint256   varLib = 87;
  varLib._isOnCurve(70, 98, 23);
  }
  //Should execute _modInv
  function test__modInv() public {
    uint256   varLib = 7;
  varLib._modInv(199999);
  }
  //Should execute _fromJacobian
  function test__fromJacobian() public {
    uint256   varLib = 21;
  varLib._fromJacobian(69, 59, 101, 46, 103);
  }
  //Should execute _ECTwistAddJacobian WHEN pt1zx==0,pt1zy==0
  function test__ECTwistAddJacobian0() public {
    uint256   varLib = 58;
  varLib._ECTwistAddJacobian(15, 45, 63, 0, 0, 32, 48, 1532892064, 2, 3, 30);
  }
  //Should execute _ECTwistAddJacobian WHEN pt2zx==0,pt2zy==0
  function test__ECTwistAddJacobian1() public {
    uint256   varLib = 2014223715;
  varLib._ECTwistAddJacobian(1532892063, 46, 88, 1, 2, 101, 1532892064, 6, 32, 0, 0);
  }
  //Should execute _ECTwistAddJacobian WHEN pt2zx!=0,pt1zy!=0
  function test__ECTwistAddJacobian2() public {
    uint256   varLib = 1024;
  varLib._ECTwistAddJacobian(160, 46, 46, 7, 20, 87, 2, 70, 61, 24, 46);
  }
  //Should execute _ECTwistDoubleJacobian
  function test__ECTwistDoubleJacobian() public {
    uint256   varLib = 66;
  varLib._ECTwistDoubleJacobian(28, 1532892062, 1532892064, 4, 45);
  }
  //Should execute _ECTwistMulJacobian
  function test__ECTwistMulJacobian() public {
    uint256   varLib = 96;
  varLib._ECTwistMulJacobian(128, 103, 87, 1532892063, 200000, 47);
  }
}
