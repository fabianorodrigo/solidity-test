pragma solidity ^0.5.0;
import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/ERC721Mintable.sol";
import "../contracts/Oraclize.sol";
import "../contracts/SolnSquareVerifier.sol";
import "../contracts/Verifier.sol";

contract TestBuffer  {
uint nonce = 86;
function stringToBytes32(string memory source) public returns (bytes32 result) {
    bytes memory tempEmptyStringTest = bytes(source);
    if (tempEmptyStringTest.length == 0) {
        return 0x0;
    }

    assembly {
        result := mload(add(source, 32))
    }
}
  using Buffer for Buffer.buffer;

  Buffer.buffer varbuffer;
  SolnSquareVerifier.Solution varSolution;
  Pairing.G1Point varG1Point;
  Pairing.G2Point varG2Point;
  Verifier.VerifyingKey varVerifyingKey;
  Verifier.Proof varProof;
  
  //Should execute init
  function test_init() public {
    varbuffer = Buffer.buffer(abi.encode("151,109,216,99,143,244,208,0,0,0,0,0,0,0,0,0"),1023);
  varbuffer.init(200000);
  }
  //Should execute append
  function test_append() public {
    varbuffer = Buffer.buffer(abi.encode("181,128,112,254,224,32,72,0,0,0,0,0,0,0,0,0"),70);
  varbuffer.append(abi.encode("139,46,107,248,230,163,56,0,0,0,0,0,0,0,0,0"));
  }
  //Should execute append
  function test_append1() public {
    varbuffer = Buffer.buffer(abi.encode("230,67,254,130,135,82,32,0,0,0,0,0,0,0,0,0"),2014223716);
  varbuffer.append(70);
  }
  //Should execute appendInt
  function test_appendInt() public {
    varbuffer = Buffer.buffer(abi.encode("131,69,155,222,55,187,144,0,0,0,0,0,0,0,0,0"),200000);
  varbuffer.appendInt(60, 64);
  }
}
