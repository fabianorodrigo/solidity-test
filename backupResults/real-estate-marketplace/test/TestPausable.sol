pragma solidity ^0.5.0;
import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/ERC721Mintable.sol";
import "../contracts/Oraclize.sol";
import "../contracts/SolnSquareVerifier.sol";
import "../contracts/Verifier.sol";

contract TestPausable  is Pausable {
uint nonce = 86;
function stringToBytes32(string memory source) public returns (bytes32 result) {
    bytes memory tempEmptyStringTest = bytes(source);
    if (tempEmptyStringTest.length == 0) {
        return 0x0;
    }

    assembly {
        result := mload(add(source, 32))
    }
}
  Buffer.buffer varbuffer;
  SolnSquareVerifier.Solution varSolution;
  Pairing.G1Point varG1Point;
  Pairing.G2Point varG2Point;
  Verifier.VerifyingKey varVerifyingKey;
  Verifier.Proof varProof;
  
  //Should execute setPaused WHEN _paused==true
  function test_setPaused0() public {
    bool   param0 = true;
    this.setPaused(param0);
  }
  //Should execute setPaused WHEN _paused!=true
  function test_setPaused1() public {
    bool   param0 = false;
    this.setPaused(param0);
  }
}
