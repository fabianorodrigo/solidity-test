pragma solidity ^0.5.0;
import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/ERC721Mintable.sol";
import "../contracts/Oraclize.sol";
import "../contracts/SolnSquareVerifier.sol";
import "../contracts/Verifier.sol";

contract TestPairing  {
uint nonce = 86;
function stringToBytes32(string memory source) public returns (bytes32 result) {
    bytes memory tempEmptyStringTest = bytes(source);
    if (tempEmptyStringTest.length == 0) {
        return 0x0;
    }

    assembly {
        result := mload(add(source, 32))
    }
}
  using Pairing for Pairing.G1Point;
  using Pairing for Pairing.G2Point;
  using Pairing for Pairing.G1Point[];

  Buffer.buffer varbuffer;
  SolnSquareVerifier.Solution varSolution;
  Pairing.G1Point varG1Point;
  Pairing.G2Point varG2Point;
  Verifier.VerifyingKey varVerifyingKey;
  Verifier.Proof varProof;
  
  //Should execute P1
  function test_P1() public {
    Pairing.G1Point memory result0;
    result0 = Pairing.P1();
  }
  //Should execute P2
  function test_P2() public {
    Pairing.G2Point memory result0;
    result0 = Pairing.P2();
  }
  //Should execute negate WHEN pX==0,pY==0
  function test_negate0() public {
    varG1Point = Pairing.G1Point(0,0);
  varG1Point.negate();
  }
  //Should execute negate WHEN pY!=0
  function test_negate1() public {
    varG1Point = Pairing.G1Point(55,128);
  varG1Point.negate();
  }
  //Should execute addition
  function test_addition() public {
    varG1Point = Pairing.G1Point(0,10);
  varG1Point.addition(Pairing.G1Point(129,2014223715));
  }
  //Should execute addition
  function test_addition1() public {
    varG2Point = Pairing.G2Point([uint(48),uint(49)],[uint(71),uint(30)]);
  varG2Point.addition(Pairing.G2Point([uint(70),uint(10)],[uint(59),uint(4)]));
  }
  //Should execute scalar_mul
  function test_scalar_mul() public {
    varG1Point = Pairing.G1Point(8,66);
  varG1Point.scalar_mul(16);
  }
  //Should execute pairing
  function test_pairing0() public {
    Pairing.G1Point[] memory varLib = new Pairing.G1Point[](10);
  varLib[0] = Pairing.G1Point(27,66);
  varLib[1] = Pairing.G1Point(6,33);
  varLib[2] = Pairing.G1Point(257,49);
  varLib[3] = Pairing.G1Point(2014223715,32);
  varLib[4] = Pairing.G1Point(103,88);
  varLib[5] = Pairing.G1Point(10,199999);
  varLib[6] = Pairing.G1Point(54,26);
  varLib[7] = Pairing.G1Point(101,257);
  varLib[8] = Pairing.G1Point(103,28);
  varLib[9] = Pairing.G1Point(20,8);
      Pairing.G2Point[] memory varParam1 = new Pairing.G2Point[](10);
  varParam1[0] = Pairing.G2Point([uint(162),uint(25)],[uint(257),uint(1023)]);
  varParam1[1] = Pairing.G2Point([uint(96),uint(55)],[uint(129),uint(47)]);
  varParam1[2] = Pairing.G2Point([uint(59),uint(25)],[uint(71),uint(58)]);
  varParam1[3] = Pairing.G2Point([uint(7),uint(26)],[uint(103),uint(1025)]);
  varParam1[4] = Pairing.G2Point([uint(1),uint(162)],[uint(200000),uint(22)]);
  varParam1[5] = Pairing.G2Point([uint(26),uint(63)],[uint(61),uint(2014223714)]);
  varParam1[6] = Pairing.G2Point([uint(4),uint(1024)],[uint(1024),uint(46)]);
  varParam1[7] = Pairing.G2Point([uint(103),uint(20)],[uint(46),uint(161)]);
  varParam1[8] = Pairing.G2Point([uint(25),uint(29)],[uint(97),uint(161)]);
  varParam1[9] = Pairing.G2Point([uint(70),uint(31)],[uint(1532892063),uint(65)]);

varLib.pairing(varParam1);
  }
  //Should fail pairing when NOT comply with: p1 == p2
  function test_pairing1() public {
    Pairing.G1Point[] memory varLib = new Pairing.G1Point[](0);
      Pairing.G2Point[] memory varParam1 = new Pairing.G2Point[](10);
  varParam1[0] = Pairing.G2Point([uint(162),uint(25)],[uint(257),uint(1023)]);
  varParam1[1] = Pairing.G2Point([uint(96),uint(55)],[uint(129),uint(47)]);
  varParam1[2] = Pairing.G2Point([uint(59),uint(25)],[uint(71),uint(58)]);
  varParam1[3] = Pairing.G2Point([uint(7),uint(26)],[uint(103),uint(1025)]);
  varParam1[4] = Pairing.G2Point([uint(1),uint(162)],[uint(200000),uint(22)]);
  varParam1[5] = Pairing.G2Point([uint(26),uint(63)],[uint(61),uint(2014223714)]);
  varParam1[6] = Pairing.G2Point([uint(4),uint(1024)],[uint(1024),uint(46)]);
  varParam1[7] = Pairing.G2Point([uint(103),uint(20)],[uint(46),uint(161)]);
  varParam1[8] = Pairing.G2Point([uint(25),uint(29)],[uint(97),uint(161)]);
  varParam1[9] = Pairing.G2Point([uint(70),uint(31)],[uint(1532892063),uint(65)]);

varLib.pairing(varParam1);
  }
  //Should execute pairingProd2
  function test_pairingProd2() public {
    varG1Point = Pairing.G1Point(19,3);
  varG1Point.pairingProd2(Pairing.G2Point([uint(27),uint(60)],[uint(5),uint(98)]), Pairing.G1Point(30,1532892062), Pairing.G2Point([uint(1532892063),uint(255)],[uint(7),uint(257)]));
  }
  //Should execute pairingProd3
  function test_pairingProd3() public {
    varG1Point = Pairing.G1Point(9,256);
  varG1Point.pairingProd3(Pairing.G2Point([uint(2),uint(1532892062)],[uint(65),uint(30)]), Pairing.G1Point(26,48), Pairing.G2Point([uint(1),uint(200001)],[uint(16),uint(48)]), Pairing.G1Point(49,98), Pairing.G2Point([uint(200000),uint(127)],[uint(16),uint(63)]));
  }
  //Should execute pairingProd4
  function test_pairingProd4() public {
    varG1Point = Pairing.G1Point(2014223714,71);
  varG1Point.pairingProd4(Pairing.G2Point([uint(3),uint(70)],[uint(128),uint(64)]), Pairing.G1Point(1025,128), Pairing.G2Point([uint(97),uint(96)],[uint(20),uint(97)]), Pairing.G1Point(32,22), Pairing.G2Point([uint(1023),uint(256)],[uint(17),uint(17)]), Pairing.G1Point(58,30), Pairing.G2Point([uint(8),uint(10)],[uint(28),uint(31)]));
  }
}
