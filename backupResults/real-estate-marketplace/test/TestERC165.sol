pragma solidity ^0.5.0;
import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/ERC721Mintable.sol";
import "../contracts/Oraclize.sol";
import "../contracts/SolnSquareVerifier.sol";
import "../contracts/Verifier.sol";

contract TestERC165  is ERC165 {
uint nonce = 86;
function stringToBytes32(string memory source) public returns (bytes32 result) {
    bytes memory tempEmptyStringTest = bytes(source);
    if (tempEmptyStringTest.length == 0) {
        return 0x0;
    }

    assembly {
        result := mload(add(source, 32))
    }
}
  Buffer.buffer varbuffer;
  SolnSquareVerifier.Solution varSolution;
  Pairing.G1Point varG1Point;
  Pairing.G2Point varG2Point;
  Verifier.VerifyingKey varVerifyingKey;
  Verifier.Proof varProof;
  
  //Should execute supportsInterface
  function test_supportsInterface() public {
    bytes4   param0 = "40";
    bool   result0;
    result0 = this.supportsInterface(param0);
  }
}
