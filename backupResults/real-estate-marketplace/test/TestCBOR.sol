pragma solidity ^0.5.0;
import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/ERC721Mintable.sol";
import "../contracts/Oraclize.sol";
import "../contracts/SolnSquareVerifier.sol";
import "../contracts/Verifier.sol";

contract TestCBOR  {
uint nonce = 86;
function stringToBytes32(string memory source) public returns (bytes32 result) {
    bytes memory tempEmptyStringTest = bytes(source);
    if (tempEmptyStringTest.length == 0) {
        return 0x0;
    }

    assembly {
        result := mload(add(source, 32))
    }
}
  using CBOR for Buffer.buffer;

  Buffer.buffer varbuffer;
  SolnSquareVerifier.Solution varSolution;
  Pairing.G1Point varG1Point;
  Pairing.G2Point varG2Point;
  Verifier.VerifyingKey varVerifyingKey;
  Verifier.Proof varProof;
  
  //Should execute encodeUInt
  function test_encodeUInt() public {
    varbuffer = Buffer.buffer(abi.encode("83,150,30,103,216,8,116,0,0,0,0,0,0,0,0,0"),127);
  varbuffer.encodeUInt(256);
  }
  //Should execute encodeInt WHEN _value>=0
  function test_encodeInt0() public {
    varbuffer = Buffer.buffer(abi.encode("50,230,145,190,195,191,200,0,0,0,0,0,0,0,0,0"),2);
  varbuffer.encodeInt(127);
  }
  //Should execute encodeInt WHEN _value<0
  function test_encodeInt1() public {
    varbuffer = Buffer.buffer(abi.encode("119,237,127,15,164,134,16,0,0,0,0,0,0,0,0,0"),2014223716);
  varbuffer.encodeInt(-1);
  }
  //Should execute encodeBytes
  function test_encodeBytes() public {
    varbuffer = Buffer.buffer(abi.encode("74,22,151,149,50,243,16,0,0,0,0,0,0,0,0,0"),47);
  varbuffer.encodeBytes(abi.encode("126,71,49,104,216,73,8,0,0,0,0,0,0,0,0,0"));
  }
  //Should execute encodeString
  function test_encodeString() public {
    varbuffer = Buffer.buffer(abi.encode("175,25,145,208,197,68,232,0,0,0,0,0,0,0,0,0"),58);
  varbuffer.encodeString("L");
  }
  //Should execute startArray
  function test_startArray() public {
    varbuffer = Buffer.buffer(abi.encode("112,158,29,138,2,100,152,0,0,0,0,0,0,0,0,0"),161);
  varbuffer.startArray();
  }
  //Should execute startMap
  function test_startMap() public {
    varbuffer = Buffer.buffer(abi.encode("108,116,104,32,1,35,100,0,0,0,0,0,0,0,0,0"),103);
  varbuffer.startMap();
  }
  //Should execute endSequence
  function test_endSequence() public {
    varbuffer = Buffer.buffer(abi.encode("168,103,117,37,89,244,216,0,0,0,0,0,0,0,0,0"),1532892062);
  varbuffer.endSequence();
  }
}
