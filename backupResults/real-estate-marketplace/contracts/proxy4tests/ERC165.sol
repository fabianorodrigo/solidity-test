pragma experimental ABIEncoderV2;
import "../ERC721Mintable.sol";

contract ProxyERC165  is ERC165 {

       function testsupportsInterface(bytes4  interfaceId) public view returns (bool ){
    return this.supportsInterface(interfaceId);
   }

   function test_registerInterface(bytes4  interfaceId) public  {
    _registerInterface(interfaceId);
   }


}