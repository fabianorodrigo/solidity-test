pragma solidity ^0.4.24;
import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/BondingCurve.sol";
import "../contracts/PriorityQueue.sol";
import "../contracts/Token.sol";
import "../contracts/zeppelin/BasicToken.sol";
import "../contracts/zeppelin/EIP20.sol";
import "../contracts/zeppelin/EIP20Interface.sol";
import "../contracts/zeppelin/ERC20.sol";
import "../contracts/zeppelin/ERC20Basic.sol";
import "../contracts/zeppelin/Ownable.sol";
import "../contracts/zeppelin/SafeMath.sol";
import "../contracts/zeppelin/StandardToken.sol";

contract TestSafeMath  {
uint nonce = 56;
function stringToBytes32(string memory source) public returns (bytes32 result) {
    bytes memory tempEmptyStringTest = bytes(source);
    if (tempEmptyStringTest.length == 0) {
        return 0x0;
    }

    assembly {
        result := mload(add(source, 32))
    }
}
  using SafeMath for uint256;

  BondingCurve.Holder varHolder;
  PriorityQueue.Order varOrder;
  
  //Should execute mul
  function test_mul() public {
    uint256   varLib = 255;
  varLib.mul(0);
  }
  //Should execute div
  function test_div() public {
    uint256   varLib = 127;
  varLib.div(101);
  }
  //Should execute sub
  function test_sub() public {
    uint256   varLib = 128;
  varLib.sub(129);
  }
  //Should execute add
  function test_add() public {
    uint256   varLib = 100;
  varLib.add(100);
  }
}
