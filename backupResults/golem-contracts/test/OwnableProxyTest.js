const truffleAssert = require('truffle-assertions');
const Faucet = artifacts.require("Faucet");
const GNTDeposit = artifacts.require("GNTDeposit");
const GNTPaymentChannels = artifacts.require("GNTPaymentChannels");
const MigrationAgent = artifacts.require("MigrationAgent");
const GNTAllocation = artifacts.require("GNTAllocation");
const GolemNetworkToken = artifacts.require("GolemNetworkToken");
const GolemNetworkTokenBatching = artifacts.require("GolemNetworkTokenBatching");
const BasicToken = artifacts.require("BasicToken");
const BurnableToken = artifacts.require("BurnableToken");
const ERC20 = artifacts.require("ERC20");
const ERC20Basic = artifacts.require("ERC20Basic");
const Ownable = artifacts.require("Ownable");
const SafeMath = artifacts.require("SafeMath");
const StandardToken = artifacts.require("StandardToken");
const ReceivingContract = artifacts.require("ReceivingContract");
const Gate = artifacts.require("Gate");
const TokenProxy = artifacts.require("TokenProxy");
const ProxyOwnable = artifacts.require("ProxyOwnable");

contract("contractProxy4TestOwnable",(accounts)=>{
    let contractProxy4TestOwnable = null;
  let trace = false;
  let contractSafeMath = null;
  let contractBasicToken = null;
  let contractStandardToken = null;
  let contractBurnableToken = null;
  let contractGolemNetworkToken = null;
  let contractFaucet = null;
  let contractGNTPaymentChannels = null;
  let contractGNTAllocation = null;
  let contractOwnable = null;
  let contractGolemNetworkTokenBatching = null;
  let contractGNTDeposit = null;
  let contractTokenProxy = null;
  let contractGate = null;
  beforeEach(async () => {
    contractSafeMath = await SafeMath.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: SafeMath.new({from: accounts[0]}');
    BasicToken.link("SafeMath",contractSafeMath.address);
    contractBasicToken = await BasicToken.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: BasicToken.new({from: accounts[0]}');
    contractStandardToken = await StandardToken.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: StandardToken.new({from: accounts[0]}');
    contractBurnableToken = await BurnableToken.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: BurnableToken.new({from: accounts[0]}');
    contractGolemNetworkToken = await GolemNetworkToken.new(accounts[2],accounts[0],(await web3.eth.getBlockNumber())+254,(await web3.eth.getBlockNumber())+254+731,{from:accounts[0]});
    if(trace) console.log('SUCESSO: GolemNetworkToken.new(accounts[2],accounts[0],(await web3.eth.getBlockNumber())+254,(await web3.eth.getBlockNumber())+254+731,{from:accounts[0]}');
    contractFaucet = await Faucet.new(contractGolemNetworkToken.address,{from:accounts[0]});
    if(trace) console.log('SUCESSO: Faucet.new(contractGolemNetworkToken.address,{from:accounts[0]}');
    contractGNTPaymentChannels = await GNTPaymentChannels.new(contractBasicToken.address,14,{from:accounts[0]});
    if(trace) console.log('SUCESSO: GNTPaymentChannels.new(contractBasicToken.address,14,{from:accounts[0]}');
    contractGNTAllocation = await GNTAllocation.new(accounts[8],{from:accounts[0]});
    if(trace) console.log('SUCESSO: GNTAllocation.new(accounts[8],{from:accounts[0]}');
    contractGolemNetworkTokenBatching = await GolemNetworkTokenBatching.new(contractBurnableToken.address,{from:accounts[0]});
    if(trace) console.log('SUCESSO: GolemNetworkTokenBatching.new(contractBurnableToken.address,{from:accounts[0]}');
    contractGNTDeposit = await GNTDeposit.new(contractGolemNetworkTokenBatching.address,accounts[6],accounts[5],19,{from:accounts[0]});
    if(trace) console.log('SUCESSO: GNTDeposit.new(contractGolemNetworkTokenBatching.address,accounts[6],accounts[5],19,{from:accounts[0]}');
    contractTokenProxy = await TokenProxy.new(contractGolemNetworkTokenBatching.address,{from:accounts[0]});
    if(trace) console.log('SUCESSO: TokenProxy.new(contractGolemNetworkTokenBatching.address,{from:accounts[0]}');
    contractGate = await Gate.new(contractTokenProxy.address,accounts[5],{from:accounts[0]});
    if(trace) console.log('SUCESSO: Gate.new(contractTokenProxy.address,accounts[5],{from:accounts[0]}');
      contractProxy4TestOwnable = await ProxyOwnable.new({ from: accounts[0] });
});
  
  it('Should execute owner()', async () => {
    let result = await contractProxy4TestOwnable.testowner({from: accounts[0]});
  });
  it('Should execute isOwner()', async () => {
    let result = await contractProxy4TestOwnable.testisOwner({from: accounts[0]});
  });
  it('Should execute renounceOwnership() WHEN sender==_owner', async () => {
    let localcontractProxy4TestOwnable = await ProxyOwnable.new({from:accounts[0]});
    let result = await localcontractProxy4TestOwnable.testrenounceOwnership({from: accounts[0]});
  });
  it('Should fail renounceOwnership() when NOT comply with: sender == _owner', async () => {
    let localcontractProxy4TestOwnable = await ProxyOwnable.new({from:accounts[0]});
    let result = await truffleAssert.fails(localcontractProxy4TestOwnable.testrenounceOwnership({from: accounts[9]}),'revert');
  });
  it('Should execute transferOwnership(address) WHEN sender==_owner,newOwner!=0x0000000000000000000000000000000000000000', async () => {
    let localcontractProxy4TestOwnable = await ProxyOwnable.new({from:accounts[0]});
    let result = await localcontractProxy4TestOwnable.testtransferOwnership(accounts[9],{from: accounts[0]});
  });
  it('Should fail transferOwnership(address) when NOT comply with: sender == _owner', async () => {
    let localcontractProxy4TestOwnable = await ProxyOwnable.new({from:accounts[0]});
    let result = await truffleAssert.fails(localcontractProxy4TestOwnable.testtransferOwnership(accounts[9],{from: accounts[9]}),'revert');
  });
  it('Should fail transferOwnership(address) when NOT comply with: newOwner != 0x0000000000000000000000000000000000000000', async () => {
    let localcontractProxy4TestOwnable = await ProxyOwnable.new({from:accounts[0]});
    let result = await truffleAssert.fails(localcontractProxy4TestOwnable.testtransferOwnership("0x0000000000000000000000000000000000000000",{from: accounts[0]}),'revert');
  });
  it('Should execute _transferOwnership(address) WHEN newOwner!=0x0000000000000000000000000000000000000000', async () => {
    let result = await contractProxy4TestOwnable.test_transferOwnership(accounts[8],{from: accounts[0]});
  });
  it('Should fail _transferOwnership(address) when NOT comply with: newOwner != 0x0000000000000000000000000000000000000000', async () => {
    let result = await truffleAssert.fails(contractProxy4TestOwnable.test_transferOwnership("0x0000000000000000000000000000000000000000",{from: accounts[0]}),'revert');
  });
});
