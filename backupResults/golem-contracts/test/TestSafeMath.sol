pragma solidity ^0.5.3;
import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Faucet.sol";
import "../contracts/GNTDeposit.sol";
import "../contracts/GNTPaymentChannels.sol";
import "../contracts/GolemNetworkToken.sol";
import "../contracts/GolemNetworkTokenBatching.sol";
import "../contracts/open_zeppelin/BasicToken.sol";
import "../contracts/open_zeppelin/BurnableToken.sol";
import "../contracts/open_zeppelin/ERC20.sol";
import "../contracts/open_zeppelin/ERC20Basic.sol";
import "../contracts/open_zeppelin/Ownable.sol";
import "../contracts/open_zeppelin/SafeMath.sol";
import "../contracts/open_zeppelin/StandardToken.sol";
import "../contracts/ReceivingContract.sol";
import "../contracts/TokenProxy.sol";

contract TestSafeMath  {
uint nonce = 24;
function stringToBytes32(string memory source) public returns (bytes32 result) {
    bytes memory tempEmptyStringTest = bytes(source);
    if (tempEmptyStringTest.length == 0) {
        return 0x0;
    }

    assembly {
        result := mload(add(source, 32))
    }
}
  using SafeMath for uint256;

  GNTPaymentChannels.PaymentChannel varPaymentChannel;
  
  //Should execute mul WHEN a==0
  function test_mul0() public {
    uint256   varLib = 0;
  varLib.mul(999);
  }
  //Should execute mul WHEN a!=0
  function test_mul1() public {
    uint256   varLib = 99;
  varLib.mul(16);
  }
  //Should execute div
  function test_div() public {
    uint256   varLib = 9;
  varLib.div(1001);
  }
  //Should execute sub
  function test_sub() public {
    uint256   varLib = 161;
  varLib.sub(159);
  }
  //Should execute add
  function test_add() public {
    uint256   varLib = 1001;
  varLib.add(10);
  }
}
