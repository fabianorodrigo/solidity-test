const truffleAssert = require('truffle-assertions');
const Faucet = artifacts.require("Faucet");
const GNTDeposit = artifacts.require("GNTDeposit");
const GNTPaymentChannels = artifacts.require("GNTPaymentChannels");
const GNTAllocation = artifacts.require("GNTAllocation");
const GolemNetworkToken = artifacts.require("GolemNetworkToken");
const GolemNetworkTokenBatching = artifacts.require("GolemNetworkTokenBatching");
const BasicToken = artifacts.require("BasicToken");
const BurnableToken = artifacts.require("BurnableToken");
const SafeMath = artifacts.require("SafeMath");
const StandardToken = artifacts.require("StandardToken");
const Gate = artifacts.require("Gate");
const TokenProxy = artifacts.require("TokenProxy");
const ProxySafeMath = artifacts.require("ProxySafeMath");

contract("GNTDeposit",(accounts)=>{
  let trace = false;
  let contractSafeMath = null;
  let contractBasicToken = null;
  let contractStandardToken = null;
  let contractBurnableToken = null;
  let contractGNTAllocation = null;
  let contractGolemNetworkToken = null;
  let contractGNTPaymentChannels = null;
  let contractFaucet = null;
  let contractTokenProxy = null;
  let contractGolemNetworkTokenBatching = null;
  let contractGate = null;
  let contractGNTDeposit = null;
  beforeEach(async () => {
    contractSafeMath = await SafeMath.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: SafeMath.new({from: accounts[0]}');
    BasicToken.link("SafeMath",contractSafeMath.address);
    contractBasicToken = await BasicToken.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: BasicToken.new({from: accounts[0]}');
    contractStandardToken = await StandardToken.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: StandardToken.new({from: accounts[0]}');
    contractBurnableToken = await BurnableToken.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: BurnableToken.new({from: accounts[0]}');
    contractGNTAllocation = await GNTAllocation.new(accounts[1],{from:accounts[0]});
    if(trace) console.log('SUCESSO: GNTAllocation.new(accounts[1],{from:accounts[0]}');
    contractGolemNetworkToken = await GolemNetworkToken.new(accounts[4],accounts[6],(await web3.eth.getBlockNumber())+685,(await web3.eth.getBlockNumber())+685+124,{from:accounts[0]});
    if(trace) console.log('SUCESSO: GolemNetworkToken.new(accounts[4],accounts[6],(await web3.eth.getBlockNumber())+685,(await web3.eth.getBlockNumber())+685+124,{from:accounts[0]}');
    contractGNTPaymentChannels = await GNTPaymentChannels.new(accounts[5],2,{from:accounts[0]});
    if(trace) console.log('SUCESSO: GNTPaymentChannels.new(accounts[5],2,{from:accounts[0]}');
    contractFaucet = await Faucet.new(contractGolemNetworkToken.address,{from:accounts[0]});
    if(trace) console.log('SUCESSO: Faucet.new(contractGolemNetworkToken.address,{from:accounts[0]}');
    contractTokenProxy = await TokenProxy.new(contractStandardToken.address,{from:accounts[0]});
    if(trace) console.log('SUCESSO: TokenProxy.new(contractStandardToken.address,{from:accounts[0]}');
    contractGolemNetworkTokenBatching = await GolemNetworkTokenBatching.new(contractStandardToken.address,{from:accounts[0]});
    if(trace) console.log('SUCESSO: GolemNetworkTokenBatching.new(contractStandardToken.address,{from:accounts[0]}');
    contractGate = await Gate.new(contractGolemNetworkTokenBatching.address,accounts[2],{from:accounts[0]});
    if(trace) console.log('SUCESSO: Gate.new(contractGolemNetworkTokenBatching.address,accounts[2],{from:accounts[0]}');
    contractGNTDeposit = await GNTDeposit.new(contractGolemNetworkTokenBatching.address,accounts[9],accounts[7],16,{from:accounts[0]});
    if(trace) console.log('SUCESSO: GNTDeposit.new(contractGolemNetworkTokenBatching.address,accounts[9],accounts[7],16,{from:accounts[0]}');
  });
  
  it('Should execute balanceOf(address)', async () => {
    let result = await contractGNTDeposit.balanceOf(accounts[8],{from: accounts[0]});
  });
  it('Should execute isLocked(address)', async () => {
    let result = await contractGNTDeposit.isLocked(accounts[1],{from: accounts[0]});
  });
  it('Should execute isTimeLocked(address)', async () => {
    let result = await contractGNTDeposit.isTimeLocked(accounts[6],{from: accounts[0]});
  });
  it('Should execute isUnlocked(address)', async () => {
    let result = await contractGNTDeposit.isUnlocked(accounts[9],{from: accounts[0]});
  });
  it('Should execute getTimelock(address)', async () => {
    let result = await contractGNTDeposit.getTimelock(accounts[3],{from: accounts[0]});
  });
  it('Should execute isDepositPossible(address,uint256)', async () => {
    let result = await contractGNTDeposit.isDepositPossible(accounts[9], 101,{from: accounts[0]});
  });
  it('Should execute transferConcent(address) WHEN msg.sender==_owner,_newConcent!=0x0000000000000000000000000000000000000000', async () => {
    let result = await contractGNTDeposit.transferConcent(accounts[4],{from: accounts[0]});
  });
  it('Should fail transferConcent(address) when NOT comply with: msg.sender == _owner', async () => {
    let result = await truffleAssert.fails(contractGNTDeposit.transferConcent(accounts[4],{from: accounts[9]}),'revert');
  });
  it('Should fail transferConcent(address) when NOT comply with: _newConcent != 0x0000000000000000000000000000000000000000', async () => {
    let result = await truffleAssert.fails(contractGNTDeposit.transferConcent("0x0000000000000000000000000000000000000000",{from: accounts[0]}),'revert');
  });
  it('Should execute transferColdwallet(address) WHEN msg.sender==_owner,_newColdwallet!=0x0000000000000000000000000000000000000000', async () => {
    let result = await contractGNTDeposit.transferColdwallet(accounts[3],{from: accounts[0]});
  });
  it('Should fail transferColdwallet(address) when NOT comply with: msg.sender == _owner', async () => {
    let result = await truffleAssert.fails(contractGNTDeposit.transferColdwallet(accounts[3],{from: accounts[9]}),'revert');
  });
  it('Should fail transferColdwallet(address) when NOT comply with: _newColdwallet != 0x0000000000000000000000000000000000000000', async () => {
    let result = await truffleAssert.fails(contractGNTDeposit.transferColdwallet("0x0000000000000000000000000000000000000000",{from: accounts[0]}),'revert');
  });
  it('Should execute setMaximumDepositsTotal(uint256) WHEN msg.sender==_owner', async () => {
    let result = await contractGNTDeposit.setMaximumDepositsTotal(83,{from: accounts[0]});
  });
  it('Should fail setMaximumDepositsTotal(uint256) when NOT comply with: msg.sender == _owner', async () => {
    let result = await truffleAssert.fails(contractGNTDeposit.setMaximumDepositsTotal(83,{from: accounts[9]}),'revert');
  });
  it('Should execute setMaximumDepositAmount(uint256) WHEN msg.sender==_owner', async () => {
    let result = await contractGNTDeposit.setMaximumDepositAmount(2,{from: accounts[0]});
  });
  it('Should fail setMaximumDepositAmount(uint256) when NOT comply with: msg.sender == _owner', async () => {
    let result = await truffleAssert.fails(contractGNTDeposit.setMaximumDepositAmount(2,{from: accounts[9]}),'revert');
  });
  it('Should execute setDailyReimbursementLimit(uint256) WHEN msg.sender==_owner', async () => {
    let result = await contractGNTDeposit.setDailyReimbursementLimit(2,{from: accounts[0]});
  });
  it('Should fail setDailyReimbursementLimit(uint256) when NOT comply with: msg.sender == _owner', async () => {
    let result = await truffleAssert.fails(contractGNTDeposit.setDailyReimbursementLimit(2,{from: accounts[9]}),'revert');
  });
  it('Should execute unlock()', async () => {
    let result = await contractGNTDeposit.unlock({from: accounts[0]});
  });
  it('Should execute lock()', async () => {
    let result = await contractGNTDeposit.lock({from: accounts[0]});
  });
  it('Should execute onTokenReceived(address,uint256,bytes) WHEN FunctionCall!=true', async () => {
    let result = await contractGNTDeposit.onTokenReceived(accounts[8], 257, [89,90,120,189,59,207,226,119,92,172,133,130,134,150,143,152,40,20,179,8,255,124,176,49,1,223,218,156,73,162,81,57],{from: accounts[0]});
  });
  it('Should execute withdraw(address)', async () => {
    let result = await contractGNTDeposit.withdraw(accounts[1],{from: accounts[0]});
  });
  it('Should execute burn(address,uint256) WHEN balances==0,msg.sender==concent,balances>=_amount', async () => {
    let result = await contractGNTDeposit.burn(accounts[4], 2,{from: accounts[9]});
  });
  it('Should fail burn(address,uint256) when NOT comply with: msg.sender == concent', async () => {
    let result = await truffleAssert.fails(contractGNTDeposit.burn(accounts[4], 2,{from: accounts[8]}),'revert');
  });
  it('Should execute burn(address,uint256) WHEN balances!=0,msg.sender==concent,balances>=_amount', async () => {
    let result = await contractGNTDeposit.burn(accounts[6], 6,{from: accounts[9]});
  });
  it('Should fail burn(address,uint256) when NOT comply with: msg.sender == concent', async () => {
    let result = await truffleAssert.fails(contractGNTDeposit.burn(accounts[6], 6,{from: accounts[8]}),'revert');
  });
  it('Should execute reimburseForSubtask(address,address,uint256,bytes32,uint8,bytes32,bytes32,uint256) WHEN msg.sender==concent,_reimburse_amount<=_amount', async () => {
    let result = await contractGNTDeposit.reimburseForSubtask(accounts[1], accounts[2], 99, [201,103,142,69,48,112,102,4,136,140,202,108,136,68,123,144,224,215,84,105,86,229,139,7,66,34,146,196,242,243,205,176], 14, [16,69,217,83,238,109,200,102,5,3,108,195,235,70,209,186,9,150,62,100,182,149,7,24,233,125,112,251,127,56,94,59], [209,48,211,215,71,168,52,211,138,40,2,23,62,108,38,97,122,156,36,37,49,112,187,225,61,57,234,54,167,23,168,19], 9,{from: accounts[9]});
  });
  it('Should fail reimburseForSubtask(address,address,uint256,bytes32,uint8,bytes32,bytes32,uint256) when NOT comply with: msg.sender == concent', async () => {
    let result = await truffleAssert.fails(contractGNTDeposit.reimburseForSubtask(accounts[1], accounts[2], 99, [201,103,142,69,48,112,102,4,136,140,202,108,136,68,123,144,224,215,84,105,86,229,139,7,66,34,146,196,242,243,205,176], 14, [16,69,217,83,238,109,200,102,5,3,108,195,235,70,209,186,9,150,62,100,182,149,7,24,233,125,112,251,127,56,94,59], [209,48,211,215,71,168,52,211,138,40,2,23,62,108,38,97,122,156,36,37,49,112,187,225,61,57,234,54,167,23,168,19], 9,{from: accounts[8]}),'revert');
  });
  it('Should fail reimburseForSubtask(address,address,uint256,bytes32,uint8,bytes32,bytes32,uint256) when NOT comply with: _reimburse_amount <= _amount', async () => {
    let result = await truffleAssert.fails(contractGNTDeposit.reimburseForSubtask(accounts[1], accounts[2], 99, [201,103,142,69,48,112,102,4,136,140,202,108,136,68,123,144,224,215,84,105,86,229,139,7,66,34,146,196,242,243,205,176], 14, [16,69,217,83,238,109,200,102,5,3,108,195,235,70,209,186,9,150,62,100,182,149,7,24,233,125,112,251,127,56,94,59], [209,48,211,215,71,168,52,211,138,40,2,23,62,108,38,97,122,156,36,37,49,112,187,225,61,57,234,54,167,23,168,19], 100,{from: accounts[9]}),'revert');
  });
  it('Should execute reimburseForNoPayment(address,address,uint256[],bytes32[],uint8[],bytes32[],bytes32[],uint256,uint256) WHEN msg.sender==concent,_amount.length==_subtask_id.length,_amount.length==_v.length,_amount.length==_r.length,_amount.length==_s.length,_reimburse_amount<=total_amount', async () => {
    let result = await contractGNTDeposit.reimburseForNoPayment(accounts[5], accounts[7], [9,159,81,159], [[233,71,236,181,252,144,124,208,180,190,232,96,57,179,47,135,3,137,86,36,213,229,68,168,146,169,210,220,173,192,213,127],[241,140,160,183,107,0,14,124,98,237,162,232,177,88,49,209,14,43,140,72,31,180,21,68,216,140,8,198,213,48,190,114],[40,31,191,140,55,204,81,252,14,231,102,52,95,18,104,142,110,31,185,176,1,89,26,44,40,70,10,162,16,94,234,76],[128,98,8,72,22,26,62,14,76,219,18,145,64,12,31,116,236,90,29,176,94,97,83,51,90,14,71,59,39,178,51,133]], [20,0,6,18], [[198,220,76,64,227,243,135,94,1,181,30,30,157,106,193,193,43,162,236,10,72,49,188,94,121,122,185,144,174,216,19,50],[237,148,85,21,195,246,162,39,151,145,202,106,59,145,255,43,235,247,171,35,69,149,208,11,139,244,7,16,109,104,167,178],[90,167,180,60,25,63,12,246,77,220,17,19,16,69,49,127,186,222,255,3,251,100,219,117,165,87,182,191,162,171,100,66],[37,105,208,46,161,168,106,126,78,148,156,167,108,203,197,91,33,135,219,3,63,107,63,74,138,250,0,244,223,125,250,179]], [[163,84,151,87,61,185,148,181,119,216,231,106,244,23,173,239,215,100,2,252,135,118,103,111,84,71,80,213,102,242,104,64],[59,85,25,15,108,253,250,170,173,181,126,151,179,135,244,27,193,80,239,237,186,185,178,228,127,209,37,2,235,6,172,19],[208,69,194,60,217,77,106,33,123,69,51,225,129,96,183,20,228,130,76,124,37,149,220,246,97,75,206,190,15,45,243,7],[68,144,179,178,207,247,107,245,62,16,76,59,193,56,130,241,7,100,218,136,35,246,196,14,154,243,72,48,181,158,30,93]], 5, 3,{from: accounts[9]});
  });
  it('Should fail reimburseForNoPayment(address,address,uint256[],bytes32[],uint8[],bytes32[],bytes32[],uint256,uint256) when NOT comply with: msg.sender == concent', async () => {
    let result = await truffleAssert.fails(contractGNTDeposit.reimburseForNoPayment(accounts[5], accounts[7], [9,159,81,159], [[233,71,236,181,252,144,124,208,180,190,232,96,57,179,47,135,3,137,86,36,213,229,68,168,146,169,210,220,173,192,213,127],[241,140,160,183,107,0,14,124,98,237,162,232,177,88,49,209,14,43,140,72,31,180,21,68,216,140,8,198,213,48,190,114],[40,31,191,140,55,204,81,252,14,231,102,52,95,18,104,142,110,31,185,176,1,89,26,44,40,70,10,162,16,94,234,76],[128,98,8,72,22,26,62,14,76,219,18,145,64,12,31,116,236,90,29,176,94,97,83,51,90,14,71,59,39,178,51,133]], [20,0,6,18], [[198,220,76,64,227,243,135,94,1,181,30,30,157,106,193,193,43,162,236,10,72,49,188,94,121,122,185,144,174,216,19,50],[237,148,85,21,195,246,162,39,151,145,202,106,59,145,255,43,235,247,171,35,69,149,208,11,139,244,7,16,109,104,167,178],[90,167,180,60,25,63,12,246,77,220,17,19,16,69,49,127,186,222,255,3,251,100,219,117,165,87,182,191,162,171,100,66],[37,105,208,46,161,168,106,126,78,148,156,167,108,203,197,91,33,135,219,3,63,107,63,74,138,250,0,244,223,125,250,179]], [[163,84,151,87,61,185,148,181,119,216,231,106,244,23,173,239,215,100,2,252,135,118,103,111,84,71,80,213,102,242,104,64],[59,85,25,15,108,253,250,170,173,181,126,151,179,135,244,27,193,80,239,237,186,185,178,228,127,209,37,2,235,6,172,19],[208,69,194,60,217,77,106,33,123,69,51,225,129,96,183,20,228,130,76,124,37,149,220,246,97,75,206,190,15,45,243,7],[68,144,179,178,207,247,107,245,62,16,76,59,193,56,130,241,7,100,218,136,35,246,196,14,154,243,72,48,181,158,30,93]], 5, 3,{from: accounts[8]}),'revert');
  });
  it('Should fail reimburseForNoPayment(address,address,uint256[],bytes32[],uint8[],bytes32[],bytes32[],uint256,uint256) when NOT comply with: _amount.length == _subtask_id.length', async () => {
    let result = await truffleAssert.fails(contractGNTDeposit.reimburseForNoPayment(accounts[5], accounts[7], [3,17,83,1337,1337], [[233,71,236,181,252,144,124,208,180,190,232,96,57,179,47,135,3,137,86,36,213,229,68,168,146,169,210,220,173,192,213,127],[241,140,160,183,107,0,14,124,98,237,162,232,177,88,49,209,14,43,140,72,31,180,21,68,216,140,8,198,213,48,190,114],[40,31,191,140,55,204,81,252,14,231,102,52,95,18,104,142,110,31,185,176,1,89,26,44,40,70,10,162,16,94,234,76],[128,98,8,72,22,26,62,14,76,219,18,145,64,12,31,116,236,90,29,176,94,97,83,51,90,14,71,59,39,178,51,133]], [20,0,6,18], [[198,220,76,64,227,243,135,94,1,181,30,30,157,106,193,193,43,162,236,10,72,49,188,94,121,122,185,144,174,216,19,50],[237,148,85,21,195,246,162,39,151,145,202,106,59,145,255,43,235,247,171,35,69,149,208,11,139,244,7,16,109,104,167,178],[90,167,180,60,25,63,12,246,77,220,17,19,16,69,49,127,186,222,255,3,251,100,219,117,165,87,182,191,162,171,100,66],[37,105,208,46,161,168,106,126,78,148,156,167,108,203,197,91,33,135,219,3,63,107,63,74,138,250,0,244,223,125,250,179]], [[163,84,151,87,61,185,148,181,119,216,231,106,244,23,173,239,215,100,2,252,135,118,103,111,84,71,80,213,102,242,104,64],[59,85,25,15,108,253,250,170,173,181,126,151,179,135,244,27,193,80,239,237,186,185,178,228,127,209,37,2,235,6,172,19],[208,69,194,60,217,77,106,33,123,69,51,225,129,96,183,20,228,130,76,124,37,149,220,246,97,75,206,190,15,45,243,7],[68,144,179,178,207,247,107,245,62,16,76,59,193,56,130,241,7,100,218,136,35,246,196,14,154,243,72,48,181,158,30,93]], 5, 3,{from: accounts[9]}),'revert');
  });
  it('Should fail reimburseForNoPayment(address,address,uint256[],bytes32[],uint8[],bytes32[],bytes32[],uint256,uint256) when NOT comply with: _amount.length == _v.length', async () => {
    let result = await truffleAssert.fails(contractGNTDeposit.reimburseForNoPayment(accounts[5], accounts[7], [81,1338,10,21,14], [[233,71,236,181,252,144,124,208,180,190,232,96,57,179,47,135,3,137,86,36,213,229,68,168,146,169,210,220,173,192,213,127],[241,140,160,183,107,0,14,124,98,237,162,232,177,88,49,209,14,43,140,72,31,180,21,68,216,140,8,198,213,48,190,114],[40,31,191,140,55,204,81,252,14,231,102,52,95,18,104,142,110,31,185,176,1,89,26,44,40,70,10,162,16,94,234,76],[128,98,8,72,22,26,62,14,76,219,18,145,64,12,31,116,236,90,29,176,94,97,83,51,90,14,71,59,39,178,51,133]], [20,0,6,18], [[198,220,76,64,227,243,135,94,1,181,30,30,157,106,193,193,43,162,236,10,72,49,188,94,121,122,185,144,174,216,19,50],[237,148,85,21,195,246,162,39,151,145,202,106,59,145,255,43,235,247,171,35,69,149,208,11,139,244,7,16,109,104,167,178],[90,167,180,60,25,63,12,246,77,220,17,19,16,69,49,127,186,222,255,3,251,100,219,117,165,87,182,191,162,171,100,66],[37,105,208,46,161,168,106,126,78,148,156,167,108,203,197,91,33,135,219,3,63,107,63,74,138,250,0,244,223,125,250,179]], [[163,84,151,87,61,185,148,181,119,216,231,106,244,23,173,239,215,100,2,252,135,118,103,111,84,71,80,213,102,242,104,64],[59,85,25,15,108,253,250,170,173,181,126,151,179,135,244,27,193,80,239,237,186,185,178,228,127,209,37,2,235,6,172,19],[208,69,194,60,217,77,106,33,123,69,51,225,129,96,183,20,228,130,76,124,37,149,220,246,97,75,206,190,15,45,243,7],[68,144,179,178,207,247,107,245,62,16,76,59,193,56,130,241,7,100,218,136,35,246,196,14,154,243,72,48,181,158,30,93]], 5, 3,{from: accounts[9]}),'revert');
  });
  it('Should fail reimburseForNoPayment(address,address,uint256[],bytes32[],uint8[],bytes32[],bytes32[],uint256,uint256) when NOT comply with: _amount.length == _r.length', async () => {
    let result = await truffleAssert.fails(contractGNTDeposit.reimburseForNoPayment(accounts[5], accounts[7], [10001,1338,19,1338,4], [[233,71,236,181,252,144,124,208,180,190,232,96,57,179,47,135,3,137,86,36,213,229,68,168,146,169,210,220,173,192,213,127],[241,140,160,183,107,0,14,124,98,237,162,232,177,88,49,209,14,43,140,72,31,180,21,68,216,140,8,198,213,48,190,114],[40,31,191,140,55,204,81,252,14,231,102,52,95,18,104,142,110,31,185,176,1,89,26,44,40,70,10,162,16,94,234,76],[128,98,8,72,22,26,62,14,76,219,18,145,64,12,31,116,236,90,29,176,94,97,83,51,90,14,71,59,39,178,51,133]], [20,0,6,18], [[198,220,76,64,227,243,135,94,1,181,30,30,157,106,193,193,43,162,236,10,72,49,188,94,121,122,185,144,174,216,19,50],[237,148,85,21,195,246,162,39,151,145,202,106,59,145,255,43,235,247,171,35,69,149,208,11,139,244,7,16,109,104,167,178],[90,167,180,60,25,63,12,246,77,220,17,19,16,69,49,127,186,222,255,3,251,100,219,117,165,87,182,191,162,171,100,66],[37,105,208,46,161,168,106,126,78,148,156,167,108,203,197,91,33,135,219,3,63,107,63,74,138,250,0,244,223,125,250,179]], [[163,84,151,87,61,185,148,181,119,216,231,106,244,23,173,239,215,100,2,252,135,118,103,111,84,71,80,213,102,242,104,64],[59,85,25,15,108,253,250,170,173,181,126,151,179,135,244,27,193,80,239,237,186,185,178,228,127,209,37,2,235,6,172,19],[208,69,194,60,217,77,106,33,123,69,51,225,129,96,183,20,228,130,76,124,37,149,220,246,97,75,206,190,15,45,243,7],[68,144,179,178,207,247,107,245,62,16,76,59,193,56,130,241,7,100,218,136,35,246,196,14,154,243,72,48,181,158,30,93]], 5, 3,{from: accounts[9]}),'revert');
  });
  it('Should fail reimburseForNoPayment(address,address,uint256[],bytes32[],uint8[],bytes32[],bytes32[],uint256,uint256) when NOT comply with: _amount.length == _s.length', async () => {
    let result = await truffleAssert.fails(contractGNTDeposit.reimburseForNoPayment(accounts[5], accounts[7], [257,159,10000,14,256], [[233,71,236,181,252,144,124,208,180,190,232,96,57,179,47,135,3,137,86,36,213,229,68,168,146,169,210,220,173,192,213,127],[241,140,160,183,107,0,14,124,98,237,162,232,177,88,49,209,14,43,140,72,31,180,21,68,216,140,8,198,213,48,190,114],[40,31,191,140,55,204,81,252,14,231,102,52,95,18,104,142,110,31,185,176,1,89,26,44,40,70,10,162,16,94,234,76],[128,98,8,72,22,26,62,14,76,219,18,145,64,12,31,116,236,90,29,176,94,97,83,51,90,14,71,59,39,178,51,133]], [20,0,6,18], [[198,220,76,64,227,243,135,94,1,181,30,30,157,106,193,193,43,162,236,10,72,49,188,94,121,122,185,144,174,216,19,50],[237,148,85,21,195,246,162,39,151,145,202,106,59,145,255,43,235,247,171,35,69,149,208,11,139,244,7,16,109,104,167,178],[90,167,180,60,25,63,12,246,77,220,17,19,16,69,49,127,186,222,255,3,251,100,219,117,165,87,182,191,162,171,100,66],[37,105,208,46,161,168,106,126,78,148,156,167,108,203,197,91,33,135,219,3,63,107,63,74,138,250,0,244,223,125,250,179]], [[163,84,151,87,61,185,148,181,119,216,231,106,244,23,173,239,215,100,2,252,135,118,103,111,84,71,80,213,102,242,104,64],[59,85,25,15,108,253,250,170,173,181,126,151,179,135,244,27,193,80,239,237,186,185,178,228,127,209,37,2,235,6,172,19],[208,69,194,60,217,77,106,33,123,69,51,225,129,96,183,20,228,130,76,124,37,149,220,246,97,75,206,190,15,45,243,7],[68,144,179,178,207,247,107,245,62,16,76,59,193,56,130,241,7,100,218,136,35,246,196,14,154,243,72,48,181,158,30,93]], 5, 3,{from: accounts[9]}),'revert');
  });
  it('Should execute reimburseForVerificationCosts(address,uint256,bytes32,uint8,bytes32,bytes32,uint256) WHEN msg.sender==concent,_reimburse_amount<=_amount', async () => {
    let result = await contractGNTDeposit.reimburseForVerificationCosts(accounts[7], 82, [6,240,95,220,179,33,154,159,230,176,196,87,127,51,41,211,180,153,166,153,193,25,156,17,34,81,48,2,236,200,100,153], 15, [158,15,33,153,70,5,241,217,18,117,147,72,223,186,197,76,175,61,84,145,80,233,109,124,181,50,135,172,226,161,75,6], [223,23,80,125,214,97,177,42,246,179,130,11,67,25,152,124,23,157,137,33,203,158,141,222,73,247,192,40,71,26,100,116], 6,{from: accounts[9]});
  });
  it('Should fail reimburseForVerificationCosts(address,uint256,bytes32,uint8,bytes32,bytes32,uint256) when NOT comply with: msg.sender == concent', async () => {
    let result = await truffleAssert.fails(contractGNTDeposit.reimburseForVerificationCosts(accounts[7], 82, [6,240,95,220,179,33,154,159,230,176,196,87,127,51,41,211,180,153,166,153,193,25,156,17,34,81,48,2,236,200,100,153], 15, [158,15,33,153,70,5,241,217,18,117,147,72,223,186,197,76,175,61,84,145,80,233,109,124,181,50,135,172,226,161,75,6], [223,23,80,125,214,97,177,42,246,179,130,11,67,25,152,124,23,157,137,33,203,158,141,222,73,247,192,40,71,26,100,116], 6,{from: accounts[8]}),'revert');
  });
  it('Should fail reimburseForVerificationCosts(address,uint256,bytes32,uint8,bytes32,bytes32,uint256) when NOT comply with: _reimburse_amount <= _amount', async () => {
    let result = await truffleAssert.fails(contractGNTDeposit.reimburseForVerificationCosts(accounts[7], 82, [6,240,95,220,179,33,154,159,230,176,196,87,127,51,41,211,180,153,166,153,193,25,156,17,34,81,48,2,236,200,100,153], 15, [158,15,33,153,70,5,241,217,18,117,147,72,223,186,197,76,175,61,84,145,80,233,109,124,181,50,135,172,226,161,75,6], [223,23,80,125,214,97,177,42,246,179,130,11,67,25,152,124,23,157,137,33,203,158,141,222,73,247,192,40,71,26,100,116], 83,{from: accounts[9]}),'revert');
  });
  it('Should execute reimburseForCommunication(address,uint256) WHEN msg.sender==concent', async () => {
    let result = await contractGNTDeposit.reimburseForCommunication(accounts[9], 255,{from: accounts[9]});
  });
  it('Should fail reimburseForCommunication(address,uint256) when NOT comply with: msg.sender == concent', async () => {
    let result = await truffleAssert.fails(contractGNTDeposit.reimburseForCommunication(accounts[9], 255,{from: accounts[8]}),'revert');
  });
  it('Should execute _isValidSignature(address,address,uint256,bytes32,uint8,bytes32,bytes32)', async () => {
    let result = await contractGNTDeposit._isValidSignature(accounts[1], accounts[9], 3, [244,232,220,79,111,140,37,5,54,12,223,23,104,64,98,247,252,144,136,92,201,65,165,76,108,247,20,141,169,207,85,124], 82, [242,178,195,21,27,145,216,240,149,81,56,163,249,54,32,79,178,106,63,162,135,206,221,119,85,86,245,135,31,227,181,198], [176,183,106,110,168,75,249,17,146,156,221,157,252,154,25,170,195,35,144,138,160,31,20,58,38,242,210,205,163,162,4,130],{from: accounts[0]});
  });
  it('Should execute owner()', async () => {
    let result = await contractGNTDeposit.owner({from: accounts[0]});
  });
  it('Should execute isOwner()', async () => {
    let result = await contractGNTDeposit.isOwner({from: accounts[0]});
  });
  it('Should execute renounceOwnership() WHEN msg.sender==_owner', async () => {
    let result = await contractGNTDeposit.renounceOwnership({from: accounts[0]});
  });
  it('Should fail renounceOwnership() when NOT comply with: msg.sender == _owner', async () => {
    let result = await truffleAssert.fails(contractGNTDeposit.renounceOwnership({from: accounts[9]}),'revert');
  });
  it('Should execute transferOwnership(address) WHEN msg.sender==_owner,newOwner!=0x0000000000000000000000000000000000000000', async () => {
    let result = await contractGNTDeposit.transferOwnership(accounts[0],{from: accounts[0]});
  });
  it('Should fail transferOwnership(address) when NOT comply with: msg.sender == _owner', async () => {
    let result = await truffleAssert.fails(contractGNTDeposit.transferOwnership(accounts[0],{from: accounts[9]}),'revert');
  });
  it('Should fail transferOwnership(address) when NOT comply with: newOwner != 0x0000000000000000000000000000000000000000', async () => {
    let result = await truffleAssert.fails(contractGNTDeposit.transferOwnership("0x0000000000000000000000000000000000000000",{from: accounts[0]}),'revert');
  });
});
