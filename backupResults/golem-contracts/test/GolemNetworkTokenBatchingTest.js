const truffleAssert = require('truffle-assertions');
const Faucet = artifacts.require("Faucet");
const GNTDeposit = artifacts.require("GNTDeposit");
const GNTPaymentChannels = artifacts.require("GNTPaymentChannels");
const GNTAllocation = artifacts.require("GNTAllocation");
const GolemNetworkToken = artifacts.require("GolemNetworkToken");
const GolemNetworkTokenBatching = artifacts.require("GolemNetworkTokenBatching");
const BasicToken = artifacts.require("BasicToken");
const BurnableToken = artifacts.require("BurnableToken");
const SafeMath = artifacts.require("SafeMath");
const StandardToken = artifacts.require("StandardToken");
const Gate = artifacts.require("Gate");
const TokenProxy = artifacts.require("TokenProxy");
const ProxySafeMath = artifacts.require("ProxySafeMath");

contract("GolemNetworkTokenBatching",(accounts)=>{
  let trace = false;
  let contractSafeMath = null;
  let contractBasicToken = null;
  let contractStandardToken = null;
  let contractBurnableToken = null;
  let contractGNTAllocation = null;
  let contractGolemNetworkToken = null;
  let contractGNTPaymentChannels = null;
  let contractFaucet = null;
  let contractTokenProxy = null;
  let contractGolemNetworkTokenBatching = null;
  let contractGate = null;
  let contractGNTDeposit = null;
  beforeEach(async () => {
    contractSafeMath = await SafeMath.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: SafeMath.new({from: accounts[0]}');
    BasicToken.link("SafeMath",contractSafeMath.address);
    contractBasicToken = await BasicToken.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: BasicToken.new({from: accounts[0]}');
    contractStandardToken = await StandardToken.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: StandardToken.new({from: accounts[0]}');
    contractBurnableToken = await BurnableToken.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: BurnableToken.new({from: accounts[0]}');
    contractGNTAllocation = await GNTAllocation.new(accounts[1],{from:accounts[0]});
    if(trace) console.log('SUCESSO: GNTAllocation.new(accounts[1],{from:accounts[0]}');
    contractGolemNetworkToken = await GolemNetworkToken.new(accounts[4],accounts[6],(await web3.eth.getBlockNumber())+685,(await web3.eth.getBlockNumber())+685+124,{from:accounts[0]});
    if(trace) console.log('SUCESSO: GolemNetworkToken.new(accounts[4],accounts[6],(await web3.eth.getBlockNumber())+685,(await web3.eth.getBlockNumber())+685+124,{from:accounts[0]}');
    contractGNTPaymentChannels = await GNTPaymentChannels.new(accounts[5],2,{from:accounts[0]});
    if(trace) console.log('SUCESSO: GNTPaymentChannels.new(accounts[5],2,{from:accounts[0]}');
    contractFaucet = await Faucet.new(contractGolemNetworkToken.address,{from:accounts[0]});
    if(trace) console.log('SUCESSO: Faucet.new(contractGolemNetworkToken.address,{from:accounts[0]}');
    contractTokenProxy = await TokenProxy.new(contractStandardToken.address,{from:accounts[0]});
    if(trace) console.log('SUCESSO: TokenProxy.new(contractStandardToken.address,{from:accounts[0]}');
    contractGolemNetworkTokenBatching = await GolemNetworkTokenBatching.new(contractStandardToken.address,{from:accounts[0]});
    if(trace) console.log('SUCESSO: GolemNetworkTokenBatching.new(contractStandardToken.address,{from:accounts[0]}');
    contractGate = await Gate.new(contractGolemNetworkTokenBatching.address,accounts[2],{from:accounts[0]});
    if(trace) console.log('SUCESSO: Gate.new(contractGolemNetworkTokenBatching.address,accounts[2],{from:accounts[0]}');
    contractGNTDeposit = await GNTDeposit.new(contractGolemNetworkTokenBatching.address,accounts[9],accounts[7],16,{from:accounts[0]});
    if(trace) console.log('SUCESSO: GNTDeposit.new(contractGolemNetworkTokenBatching.address,accounts[9],accounts[7],16,{from:accounts[0]}');
  });
  
  it('Should execute batchTransfer(bytes32[],uint64) WHEN (await web3.eth.getBlock(await web3.eth.getBlockNumber())).timestamp>=closureTime', async () => {
    let result = await contractGolemNetworkTokenBatching.batchTransfer([[152,97,195,201,130,112,34,126,141,58,57,9,186,83,247,80,40,73,5,116,138,154,79,73,25,8,124,241,218,36,201,58],[170,17,42,37,183,83,20,14,254,25,32,185,148,171,164,196,179,64,107,203,104,84,32,141,61,137,91,230,109,145,194,139],[100,102,74,213,208,251,158,125,119,229,42,92,61,24,211,16,47,26,183,140,223,56,16,44,135,171,146,27,138,161,10,162],[57,231,151,55,29,240,46,105,170,32,248,20,158,61,107,225,4,29,36,207,164,105,31,28,20,208,255,25,145,71,225,91],[223,64,62,124,86,182,152,61,149,255,195,240,250,143,104,185,181,142,147,183,30,32,2,188,104,173,31,168,196,192,48,246],[75,60,253,19,152,83,144,28,210,172,60,253,27,88,141,228,93,151,166,69,76,24,21,96,94,31,235,9,164,196,88,216],[202,145,6,59,55,59,157,131,238,202,171,127,55,92,216,174,91,183,112,77,198,251,50,176,76,243,250,134,189,78,78,178]], (await web3.eth.getBlock(await web3.eth.getBlockNumber())).timestamp-722,{from: accounts[0]});
  });
  it('Should fail batchTransfer(bytes32[],uint64) when NOT comply with: (await web3.eth.getBlock(await web3.eth.getBlockNumber())).timestamp >= closureTime', async () => {
    let result = await truffleAssert.fails(contractGolemNetworkTokenBatching.batchTransfer([[152,97,195,201,130,112,34,126,141,58,57,9,186,83,247,80,40,73,5,116,138,154,79,73,25,8,124,241,218,36,201,58],[170,17,42,37,183,83,20,14,254,25,32,185,148,171,164,196,179,64,107,203,104,84,32,141,61,137,91,230,109,145,194,139],[100,102,74,213,208,251,158,125,119,229,42,92,61,24,211,16,47,26,183,140,223,56,16,44,135,171,146,27,138,161,10,162],[57,231,151,55,29,240,46,105,170,32,248,20,158,61,107,225,4,29,36,207,164,105,31,28,20,208,255,25,145,71,225,91],[223,64,62,124,86,182,152,61,149,255,195,240,250,143,104,185,181,142,147,183,30,32,2,188,104,173,31,168,196,192,48,246],[75,60,253,19,152,83,144,28,210,172,60,253,27,88,141,228,93,151,166,69,76,24,21,96,94,31,235,9,164,196,88,216],[202,145,6,59,55,59,157,131,238,202,171,127,55,92,216,174,91,183,112,77,198,251,50,176,76,243,250,134,189,78,78,178]], (await web3.eth.getBlock(await web3.eth.getBlockNumber())).timestamp+1,{from: accounts[0]}),'revert');
  });
  it('Should execute transferAndCall(address,uint256,bytes) WHEN to!=0x0000000000000000000000000000000000000000,value<=balances', async () => {
    let result = await contractGolemNetworkTokenBatching.transferAndCall(accounts[7], 1000, [187,183,119,9,250,38,180,197,234,90,191,136,136,61,138,196,69,65,199,115,5,115,240,76,180,160,122,144,135,163,5,233],{from: accounts[0]});
  });
  it('Should fail transferAndCall(address,uint256,bytes) when NOT comply with: to != 0x0000000000000000000000000000000000000000', async () => {
    let result = await truffleAssert.fails(contractGolemNetworkTokenBatching.transferAndCall("0x0000000000000000000000000000000000000000", 1000, [187,183,119,9,250,38,180,197,234,90,191,136,136,61,138,196,69,65,199,115,5,115,240,76,180,160,122,144,135,163,5,233],{from: accounts[0]}),'revert');
  });
});
