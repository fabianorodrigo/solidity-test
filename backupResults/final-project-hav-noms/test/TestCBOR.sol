pragma solidity ^0.5.0;
import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Address.sol";
import "../contracts/EternalStorage.sol";
import "../contracts/ETHPriceTicker.sol";
import "../contracts/Mortal.sol";
import "../contracts/oraclizeAPI_05.sol";
import "../contracts/Pausable.sol";
import "../contracts/Proxy.sol";
import "../contracts/Proxyable.sol";
import "../contracts/SafeDecimalMath.sol";
import "../contracts/ShartCoin.sol";
import "../contracts/State.sol";
import "../contracts/test-helpers/PublicSafeDecimalMath.sol";
import "../contracts/TokenExchange.sol";
import "../contracts/TokenExchangeState.sol";

contract TestCBOR  {
uint nonce = 17;
function stringToBytes32(string memory source) public returns (bytes32 result) {
    bytes memory tempEmptyStringTest = bytes(source);
    if (tempEmptyStringTest.length == 0) {
        return 0x0;
    }

    assembly {
        result := mload(add(source, 32))
    }
}
  using CBOR for Buffer.buffer;

  Buffer.buffer varbuffer;
  TokenExchange.TradeListing varTradeListing;
  
  //Should execute encodeUInt
  function test_encodeUInt() public {
    varbuffer = Buffer.buffer(abi.encode("95,10,133,197,230,196,100,0,0,0,0,0,0,0,0,0"),18);
varbuffer.encodeUInt(54);
  }
  //Should execute encodeInt WHEN _value>=0
  function test_encodeInt0() public {
    varbuffer = Buffer.buffer(abi.encode("19,5,165,242,151,220,99,0,0,0,0,0,0,0,0,0"),26);
varbuffer.encodeInt(5);
  }
  //Should execute encodeInt WHEN _value<0
  function test_encodeInt1() public {
    varbuffer = Buffer.buffer(abi.encode("31,213,33,242,47,178,232,0,0,0,0,0,0,0,0,0"),102);
varbuffer.encodeInt(-1);
  }
  //Should execute encodeBytes
  function test_encodeBytes() public {
    varbuffer = Buffer.buffer(abi.encode("154,57,110,137,31,38,88,0,0,0,0,0,0,0,0,0"),200001);
varbuffer.encodeBytes(abi.encode("170,163,40,75,189,87,192,0,0,0,0,0,0,0,0,0"));
  }
  //Should execute encodeString
  function test_encodeString() public {
    varbuffer = Buffer.buffer(abi.encode("193,251,63,163,14,64,136,0,0,0,0,0,0,0,0,0"),25);
varbuffer.encodeString("Oraclize query was sent, standing by for the answer...");
  }
  //Should execute startArray
  function test_startArray() public {
    varbuffer = Buffer.buffer(abi.encode("230,244,69,105,46,220,0,0,0,0,0,0,0,0,0,0"),200001);
varbuffer.startArray();
  }
  //Should execute startMap
  function test_startMap() public {
    varbuffer = Buffer.buffer(abi.encode("45,17,184,43,15,235,48,0,0,0,0,0,0,0,0,0"),57);
varbuffer.startMap();
  }
  //Should execute endSequence
  function test_endSequence() public {
    varbuffer = Buffer.buffer(abi.encode("123,158,40,207,97,191,160,0,0,0,0,0,0,0,0,0"),15);
varbuffer.endSequence();
  }
}
