pragma solidity ^0.5.0;
import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Address.sol";
import "../contracts/EternalStorage.sol";
import "../contracts/ETHPriceTicker.sol";
import "../contracts/Mortal.sol";
import "../contracts/oraclizeAPI_05.sol";
import "../contracts/Pausable.sol";
import "../contracts/Proxy.sol";
import "../contracts/Proxyable.sol";
import "../contracts/SafeDecimalMath.sol";
import "../contracts/ShartCoin.sol";
import "../contracts/State.sol";
import "../contracts/test-helpers/PublicSafeDecimalMath.sol";
import "../contracts/TokenExchange.sol";
import "../contracts/TokenExchangeState.sol";

contract TestAddress  {
uint nonce = 17;
function stringToBytes32(string memory source) public returns (bytes32 result) {
    bytes memory tempEmptyStringTest = bytes(source);
    if (tempEmptyStringTest.length == 0) {
        return 0x0;
    }

    assembly {
        result := mload(add(source, 32))
    }
}
  using Address for address;

  Buffer.buffer varbuffer;
  TokenExchange.TradeListing varTradeListing;
  
  //Should execute isContract
  function test_isContract() public {
    address   varLib = 0xc03A2615D5efaf5F49F60B7BB6583eaec212fdf1;

varLib.isContract();
  }
}
