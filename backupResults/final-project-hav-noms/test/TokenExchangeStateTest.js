const truffleAssert = require('truffle-assertions');
const Address = artifacts.require("Address");
const EternalStorage = artifacts.require("EternalStorage");
const ETHPriceTicker = artifacts.require("ETHPriceTicker");
const Mortal = artifacts.require("Mortal");
const solcChecker = artifacts.require("solcChecker");
const OraclizeI = artifacts.require("OraclizeI");
const OraclizeAddrResolverI = artifacts.require("OraclizeAddrResolverI");
const Buffer = artifacts.require("Buffer");
const CBOR = artifacts.require("CBOR");
const usingOraclize = artifacts.require("usingOraclize");
const Pausable = artifacts.require("Pausable");
const Proxy = artifacts.require("Proxy");
const Proxyable = artifacts.require("Proxyable");
const SafeDecimalMath = artifacts.require("SafeDecimalMath");
const ShartCoin = artifacts.require("ShartCoin");
const State = artifacts.require("State");
const PublicSafeDecimalMath = artifacts.require("PublicSafeDecimalMath");
const TokenExchange = artifacts.require("TokenExchange");
const TokenExchangeState = artifacts.require("TokenExchangeState");

contract("TokenExchangeState",(accounts)=>{
  let trace = false;
  let contractAddress = null;
  let contractBuffer = null;
  let contractCBOR = null;
  let contractSafeDecimalMath = null;
  let contractusingOraclize = null;
  let contractPublicSafeDecimalMath = null;
  let contractMortal = null;
  let contractPausable = null;
  let contractEternalStorage = null;
  let contractProxyable = null;
  let contractTokenExchangeState = null;
  let contractShartCoin = null;
  let contractState = null;
  let contractETHPriceTicker = null;
  let contractProxy = null;
  let contractTokenExchange = null;
  beforeEach(async () => {
    contractAddress = await Address.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: Address.new({from: accounts[0]}');
    contractBuffer = await Buffer.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: Buffer.new({from: accounts[0]}');
    contractCBOR = await CBOR.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: CBOR.new({from: accounts[0]}');
    contractSafeDecimalMath = await SafeDecimalMath.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: SafeDecimalMath.new({from: accounts[0]}');
    contractusingOraclize = await usingOraclize.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: usingOraclize.new({from: accounts[0]}');
    PublicSafeDecimalMath.link("SafeDecimalMath",contractSafeDecimalMath.address);
    contractPublicSafeDecimalMath = await PublicSafeDecimalMath.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: PublicSafeDecimalMath.new({from: accounts[0]}');
    contractMortal = await Mortal.new("0x146500cfd35B22E4A392Fe0aDc06De1a1368Ed48",{from: accounts[0]});
    if(trace) console.log('SUCESSO: Mortal.new("0x146500cfd35B22E4A392Fe0aDc06De1a1368Ed48",{from: accounts[0]}');
    contractPausable = await Pausable.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: Pausable.new({from: accounts[0]}');
    contractProxyable = await Proxyable.new("0x51efaF4c8B3C9AfBD5aB9F4bbC82784Ab6ef8fAA",{from: accounts[0]});
    if(trace) console.log('SUCESSO: Proxyable.new("0x51efaF4c8B3C9AfBD5aB9F4bbC82784Ab6ef8fAA",{from: accounts[0]}');
    contractTokenExchangeState = await TokenExchangeState.new("0x51efaF4c8B3C9AfBD5aB9F4bbC82784Ab6ef8fAA",{from: accounts[0]});
    if(trace) console.log('SUCESSO: TokenExchangeState.new("0x51efaF4c8B3C9AfBD5aB9F4bbC82784Ab6ef8fAA",{from: accounts[0]}');
    contractShartCoin = await ShartCoin.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: ShartCoin.new({from: accounts[0]}');
    contractState = await State.new("0x20e12A1F859B3FeaE5Fb2A0A32C18F5a65555bBF",{from: accounts[0]});
    if(trace) console.log('SUCESSO: State.new("0x20e12A1F859B3FeaE5Fb2A0A32C18F5a65555bBF",{from: accounts[0]}');
    ETHPriceTicker.link("Buffer",contractBuffer.address);
     ETHPriceTicker.link("CBOR",contractCBOR.address);
    contractETHPriceTicker = await ETHPriceTicker.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: ETHPriceTicker.new({from: accounts[0]}');
    contractProxy = await Proxy.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: Proxy.new({from: accounts[0]}');
    TokenExchange.link("SafeDecimalMath",contractSafeDecimalMath.address);
     TokenExchange.link("Address",contractAddress.address);
    contractTokenExchange = await TokenExchange.new("0xc03A2615D5efaf5F49F60B7BB6583eaec212fdf1", accounts[2], contractTokenExchangeState.address, "P",{from: accounts[0]});
    if(trace) console.log('SUCESSO: TokenExchange.new("0xc03A2615D5efaf5F49F60B7BB6583eaec212fdf1", accounts[2], contractTokenExchangeState.address, "P",{from: accounts[0]}');
  });
  
  it('Should execute setAssociatedContract - accounts[0]', async () => {
    let result = await contractTokenExchangeState.methods["setAssociatedContract(address)"](accounts[7],{from: accounts[0]});
  });
  it('Should execute setAssociatedContract - accounts[4]', async () => {
    let result = await contractTokenExchangeState.methods["setAssociatedContract(address)"](accounts[7],{from: accounts[4]});
  });
  it('Should execute getUIntValue', async () => {
    let result = await contractTokenExchangeState.methods["getUIntValue(bytes32)"]([156,166,28,196,2,246,136,0,0,0,0,0,0,0,0,0],{from: accounts[0]});
  });
  it('Should execute setUIntValue', async () => {
    let result = await contractTokenExchangeState.methods["setUIntValue(bytes32,uint256)"]([157,2,234,229,16,184,80,0,0,0,0,0,0,0,0,0], 27,{from: accounts[0]});
  });
  it('Should execute deleteUIntValue', async () => {
    let result = await contractTokenExchangeState.methods["deleteUIntValue(bytes32)"]([196,194,130,137,59,231,120,0,0,0,0,0,0,0,0,0],{from: accounts[0]});
  });
  it('Should execute getStringValue', async () => {
    let result = await contractTokenExchangeState.methods["getStringValue(bytes32)"]([247,81,106,91,254,44,0,0,0,0,0,0,0,0,0,0],{from: accounts[0]});
  });
  it('Should execute setStringValue', async () => {
    let result = await contractTokenExchangeState.methods["setStringValue(bytes32,string)"]([47,137,124,4,59,141,28,0,0,0,0,0,0,0,0,0], "call updateEthPrice",{from: accounts[0]});
  });
  it('Should execute deleteStringValue', async () => {
    let result = await contractTokenExchangeState.methods["deleteStringValue(bytes32)"]([158,52,41,80,212,39,0,0,0,0,0,0,0,0,0,0],{from: accounts[0]});
  });
  it('Should execute getAddressValue', async () => {
    let result = await contractTokenExchangeState.methods["getAddressValue(bytes32)"]([219,63,124,213,184,38,72,0,0,0,0,0,0,0,0,0],{from: accounts[0]});
  });
  it('Should execute setAddressValue', async () => {
    let result = await contractTokenExchangeState.methods["setAddressValue(bytes32,address)"]([165,70,2,202,120,221,64,0,0,0,0,0,0,0,0,0], "0x51efaF4c8B3C9AfBD5aB9F4bbC82784Ab6ef8fAA",{from: accounts[0]});
  });
  it('Should execute deleteAddressValue', async () => {
    let result = await contractTokenExchangeState.methods["deleteAddressValue(bytes32)"]([131,24,5,48,172,245,176,0,0,0,0,0,0,0,0,0],{from: accounts[0]});
  });
  it('Should execute getBytesValue', async () => {
    let result = await contractTokenExchangeState.methods["getBytesValue(bytes32)"]([153,228,116,137,23,12,144,0,0,0,0,0,0,0,0,0],{from: accounts[0]});
  });
  it('Should execute setBytesValue', async () => {
    let result = await contractTokenExchangeState.methods["setBytesValue(bytes32,bytes)"]([158,251,107,210,159,91,88,0,0,0,0,0,0,0,0,0], [98,162,124,168,154,118,16,0,0,0,0,0,0,0,0,0],{from: accounts[0]});
  });
  it('Should execute deleteBytesValue', async () => {
    let result = await contractTokenExchangeState.methods["deleteBytesValue(bytes32)"]([213,173,120,163,221,124,96,0,0,0,0,0,0,0,0,0],{from: accounts[0]});
  });
  it('Should execute getBytes32Value', async () => {
    let result = await contractTokenExchangeState.methods["getBytes32Value(bytes32)"]([183,252,163,22,93,61,24,0,0,0,0,0,0,0,0,0],{from: accounts[0]});
  });
  it('Should execute setBytes32Value', async () => {
    let result = await contractTokenExchangeState.methods["setBytes32Value(bytes32,bytes32)"]([129,185,168,25,106,113,144,0,0,0,0,0,0,0,0,0], [54,156,162,96,69,154,196,0,0,0,0,0,0,0,0,0],{from: accounts[0]});
  });
  it('Should execute deleteBytes32Value', async () => {
    let result = await contractTokenExchangeState.methods["deleteBytes32Value(bytes32)"]([88,121,73,88,125,53,160,0,0,0,0,0,0,0,0,0],{from: accounts[0]});
  });
  it('Should execute getBooleanValue', async () => {
    let result = await contractTokenExchangeState.methods["getBooleanValue(bytes32)"]([80,105,226,160,54,230,212,0,0,0,0,0,0,0,0,0],{from: accounts[0]});
  });
  it('Should execute setBooleanValue', async () => {
    let result = await contractTokenExchangeState.methods["setBooleanValue(bytes32,bool)"]([43,167,170,69,105,66,100,0,0,0,0,0,0,0,0,0], true,{from: accounts[0]});
  });
  it('Should execute deleteBooleanValue', async () => {
    let result = await contractTokenExchangeState.methods["deleteBooleanValue(bytes32)"]([231,214,123,141,98,104,64,0,0,0,0,0,0,0,0,0],{from: accounts[0]});
  });
  it('Should execute getIntValue', async () => {
    let result = await contractTokenExchangeState.methods["getIntValue(bytes32)"]([72,137,92,117,140,229,68,0,0,0,0,0,0,0,0,0],{from: accounts[0]});
  });
  it('Should execute setIntValue', async () => {
    let result = await contractTokenExchangeState.methods["setIntValue(bytes32,int256)"]([54,118,80,152,144,115,12,0,0,0,0,0,0,0,0,0], 101,{from: accounts[0]});
  });
  it('Should execute deleteIntValue', async () => {
    let result = await contractTokenExchangeState.methods["deleteIntValue(bytes32)"]([70,152,160,50,153,129,156,0,0,0,0,0,0,0,0,0],{from: accounts[0]});
  });
});
