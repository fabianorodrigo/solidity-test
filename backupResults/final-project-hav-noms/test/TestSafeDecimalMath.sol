pragma solidity ^0.5.0;
import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Address.sol";
import "../contracts/EternalStorage.sol";
import "../contracts/ETHPriceTicker.sol";
import "../contracts/Mortal.sol";
import "../contracts/oraclizeAPI_05.sol";
import "../contracts/Pausable.sol";
import "../contracts/Proxy.sol";
import "../contracts/Proxyable.sol";
import "../contracts/SafeDecimalMath.sol";
import "../contracts/ShartCoin.sol";
import "../contracts/State.sol";
import "../contracts/test-helpers/PublicSafeDecimalMath.sol";
import "../contracts/TokenExchange.sol";
import "../contracts/TokenExchangeState.sol";

contract TestSafeDecimalMath  {
uint nonce = 17;
function stringToBytes32(string memory source) public returns (bytes32 result) {
    bytes memory tempEmptyStringTest = bytes(source);
    if (tempEmptyStringTest.length == 0) {
        return 0x0;
    }

    assembly {
        result := mload(add(source, 32))
    }
}
  using SafeDecimalMath for uint;

  Buffer.buffer varbuffer;
  TokenExchange.TradeListing varTradeListing;
  
  //Should execute unit
  function test_unit() public {
    uint   result0;
    result0 = SafeDecimalMath.unit();
  }
  //Should execute preciseUnit
  function test_preciseUnit() public {
    uint   result0;
    result0 = SafeDecimalMath.preciseUnit();
  }
  //Should execute multiplyDecimal
  function test_multiplyDecimal() public {
    uint   varLib = 3;

varLib.multiplyDecimal(129);
  }
  //Should execute multiplyDecimalRoundPrecise
  function test_multiplyDecimalRoundPrecise() public {
    uint   varLib = 69;

varLib.multiplyDecimalRoundPrecise(15);
  }
  //Should execute multiplyDecimalRound
  function test_multiplyDecimalRound() public {
    uint   varLib = 256;

varLib.multiplyDecimalRound(129);
  }
  //Should execute divideDecimal
  function test_divideDecimal() public {
    uint   varLib = 1025;

varLib.divideDecimal(47);
  }
  //Should execute divideDecimalRound
  function test_divideDecimalRound() public {
    uint   varLib = 129;

varLib.divideDecimalRound(3);
  }
  //Should execute divideDecimalRoundPrecise
  function test_divideDecimalRoundPrecise() public {
    uint   varLib = 19;

varLib.divideDecimalRoundPrecise(8);
  }
  //Should execute decimalToPreciseDecimal
  function test_decimalToPreciseDecimal() public {
    uint   varLib = 86;

varLib.decimalToPreciseDecimal();
  }
  //Should execute preciseDecimalToDecimal
  function test_preciseDecimalToDecimal() public {
    uint   varLib = 86;

varLib.preciseDecimalToDecimal();
  }
}
