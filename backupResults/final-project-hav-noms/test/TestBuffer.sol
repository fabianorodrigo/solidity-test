pragma solidity ^0.5.0;
import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Address.sol";
import "../contracts/EternalStorage.sol";
import "../contracts/ETHPriceTicker.sol";
import "../contracts/Mortal.sol";
import "../contracts/oraclizeAPI_05.sol";
import "../contracts/Pausable.sol";
import "../contracts/Proxy.sol";
import "../contracts/Proxyable.sol";
import "../contracts/SafeDecimalMath.sol";
import "../contracts/ShartCoin.sol";
import "../contracts/State.sol";
import "../contracts/test-helpers/PublicSafeDecimalMath.sol";
import "../contracts/TokenExchange.sol";
import "../contracts/TokenExchangeState.sol";

contract TestBuffer  {
uint nonce = 17;
function stringToBytes32(string memory source) public returns (bytes32 result) {
    bytes memory tempEmptyStringTest = bytes(source);
    if (tempEmptyStringTest.length == 0) {
        return 0x0;
    }

    assembly {
        result := mload(add(source, 32))
    }
}
  using Buffer for Buffer.buffer;

  Buffer.buffer varbuffer;
  TokenExchange.TradeListing varTradeListing;
  
  //Should execute init
  function test_init() public {
    varbuffer = Buffer.buffer(abi.encode("62,75,55,75,220,68,240,0,0,0,0,0,0,0,0,0"),27);
varbuffer.init(200001);
  }
  //Should execute append
  function test_append() public {
    varbuffer = Buffer.buffer(abi.encode("138,131,42,107,80,219,64,0,0,0,0,0,0,0,0,0"),61);
varbuffer.append(abi.encode("156,170,172,162,133,165,224,0,0,0,0,0,0,0,0,0"));
  }
  //Should execute append
  function test_append1() public {
    varbuffer = Buffer.buffer(abi.encode("249,121,134,60,85,177,224,0,0,0,0,0,0,0,0,0"),16);
varbuffer.append(27);
  }
  //Should execute appendInt
  function test_appendInt() public {
    varbuffer = Buffer.buffer(abi.encode("191,61,255,253,97,120,104,0,0,0,0,0,0,0,0,0"),55);
varbuffer.appendInt(19, 59);
  }
}
