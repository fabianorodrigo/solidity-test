const truffleAssert = require('truffle-assertions');
const Address = artifacts.require("Address");
const EternalStorage = artifacts.require("EternalStorage");
const ETHPriceTicker = artifacts.require("ETHPriceTicker");
const Mortal = artifacts.require("Mortal");
const solcChecker = artifacts.require("solcChecker");
const OraclizeI = artifacts.require("OraclizeI");
const OraclizeAddrResolverI = artifacts.require("OraclizeAddrResolverI");
const Buffer = artifacts.require("Buffer");
const CBOR = artifacts.require("CBOR");
const usingOraclize = artifacts.require("usingOraclize");
const Pausable = artifacts.require("Pausable");
const Proxy = artifacts.require("Proxy");
const Proxyable = artifacts.require("Proxyable");
const SafeDecimalMath = artifacts.require("SafeDecimalMath");
const ShartCoin = artifacts.require("ShartCoin");
const State = artifacts.require("State");
const PublicSafeDecimalMath = artifacts.require("PublicSafeDecimalMath");
const TokenExchange = artifacts.require("TokenExchange");
const TokenExchangeState = artifacts.require("TokenExchangeState");

contract("EternalStorage",(accounts)=>{
  let trace = false;
  let contractAddress = null;
  let contractBuffer = null;
  let contractCBOR = null;
  let contractSafeDecimalMath = null;
  let contractusingOraclize = null;
  let contractPublicSafeDecimalMath = null;
  let contractMortal = null;
  let contractPausable = null;
  let contractEternalStorage = null;
  let contractProxyable = null;
  let contractTokenExchangeState = null;
  let contractShartCoin = null;
  let contractState = null;
  let contractETHPriceTicker = null;
  let contractProxy = null;
  let contractTokenExchange = null;
  beforeEach(async () => {
    contractAddress = await Address.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: Address.new({from: accounts[0]}');
    contractBuffer = await Buffer.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: Buffer.new({from: accounts[0]}');
    contractCBOR = await CBOR.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: CBOR.new({from: accounts[0]}');
    contractSafeDecimalMath = await SafeDecimalMath.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: SafeDecimalMath.new({from: accounts[0]}');
    contractusingOraclize = await usingOraclize.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: usingOraclize.new({from: accounts[0]}');
    PublicSafeDecimalMath.link("SafeDecimalMath",contractSafeDecimalMath.address);
    contractPublicSafeDecimalMath = await PublicSafeDecimalMath.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: PublicSafeDecimalMath.new({from: accounts[0]}');
    contractMortal = await Mortal.new(accounts[0],{from:accounts[0]});
    if(trace) console.log('SUCESSO: Mortal.new(accounts[0],{from:accounts[0]}');
    contractPausable = await Pausable.new({from:accounts[0]});
    if(trace) console.log('SUCESSO: Pausable.new({from:accounts[0]}');
    contractProxyable = await Proxyable.new(contractBuffer.address,{from:accounts[0]});
    if(trace) console.log('SUCESSO: Proxyable.new(contractBuffer.address,{from:accounts[0]}');
    contractTokenExchangeState = await TokenExchangeState.new(accounts[6],{from:accounts[0]});
    if(trace) console.log('SUCESSO: TokenExchangeState.new(accounts[6],{from:accounts[0]}');
    contractShartCoin = await ShartCoin.new({from:accounts[0]});
    if(trace) console.log('SUCESSO: ShartCoin.new({from:accounts[0]}');
    contractState = await State.new(accounts[9],{from:accounts[0]});
    if(trace) console.log('SUCESSO: State.new(accounts[9],{from:accounts[0]}');
    ETHPriceTicker.link("Buffer",contractBuffer.address);
     ETHPriceTicker.link("CBOR",contractCBOR.address);
    contractETHPriceTicker = await ETHPriceTicker.new({from:accounts[0]});
    if(trace) console.log('SUCESSO: ETHPriceTicker.new({from:accounts[0]}');
    contractProxy = await Proxy.new({from:accounts[0]});
    if(trace) console.log('SUCESSO: Proxy.new({from:accounts[0]}');
    TokenExchange.link("SafeDecimalMath",contractSafeDecimalMath.address);
     TokenExchange.link("Address",contractAddress.address);
    contractTokenExchange = await TokenExchange.new(accounts[2],accounts[0],contractTokenExchangeState.address,"listingID arg",{from:accounts[0]});
    if(trace) console.log('SUCESSO: TokenExchange.new(accounts[2],accounts[0],contractTokenExchangeState.address,"listingID arg",{from:accounts[0]}');
  });
  
  it('Should execute getUIntValue(bytes32)', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    let result = await contractEternalStorage.getUIntValue([194,108,81,225,8,54,10,245,192,124,201,223,137,228,16,96,89,13,29,247,214,211,173,78,125,101,74,91,52,5,235,22],{from: accounts[0]});
  });
  it('Should execute setUIntValue(bytes32,uint) WHEN sender==associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[9],{from:accounts[0]});
    let result = await contractEternalStorage.setUIntValue([189,9,88,71,138,129,169,18,75,106,4,220,233,114,150,125,56,157,17,72,230,100,155,24,144,84,207,4,98,229,180,80], 257,{from: accounts[9]});
  });
  it('Should fail setUIntValue(bytes32,uint) when NOT comply with: sender == associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[9],{from:accounts[0]});
    let result = await truffleAssert.fails(contractEternalStorage.setUIntValue([189,9,88,71,138,129,169,18,75,106,4,220,233,114,150,125,56,157,17,72,230,100,155,24,144,84,207,4,98,229,180,80], 257,{from: accounts[8]}),'revert');
  });
  it('Should execute deleteUIntValue(bytes32) WHEN sender==associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[1],{from:accounts[0]});
    let result = await contractEternalStorage.deleteUIntValue([243,177,67,3,169,2,176,253,206,93,0,30,38,23,103,58,166,234,72,84,208,16,38,52,54,149,37,214,67,249,68,242],{from: accounts[1]});
  });
  it('Should fail deleteUIntValue(bytes32) when NOT comply with: sender == associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[1],{from:accounts[0]});
    let result = await truffleAssert.fails(contractEternalStorage.deleteUIntValue([243,177,67,3,169,2,176,253,206,93,0,30,38,23,103,58,166,234,72,84,208,16,38,52,54,149,37,214,67,249,68,242],{from: accounts[9]}),'revert');
  });
  it('Should execute getStringValue(bytes32)', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    let result = await contractEternalStorage.getStringValue([79,16,60,241,4,126,8,194,10,58,92,221,84,79,226,246,165,94,208,241,99,77,162,97,237,113,192,127,181,143,225,122],{from: accounts[0]});
  });
  it('Should execute setStringValue(bytes32,string) WHEN sender==associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[5],{from:accounts[0]});
    let result = await contractEternalStorage.setStringValue([179,205,156,168,86,223,55,229,165,226,47,53,224,35,1,156,39,234,251,176,154,19,15,193,18,170,217,21,179,24,61,95], "ffvsm",{from: accounts[5]});
  });
  it('Should fail setStringValue(bytes32,string) when NOT comply with: sender == associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[5],{from:accounts[0]});
    let result = await truffleAssert.fails(contractEternalStorage.setStringValue([179,205,156,168,86,223,55,229,165,226,47,53,224,35,1,156,39,234,251,176,154,19,15,193,18,170,217,21,179,24,61,95], "ffvsm",{from: accounts[9]}),'revert');
  });
  it('Should execute deleteStringValue(bytes32) WHEN sender==associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[3],{from:accounts[0]});
    let result = await contractEternalStorage.deleteStringValue([204,37,51,59,243,12,94,220,243,214,17,210,32,184,208,198,211,152,51,138,227,107,247,87,109,76,33,181,130,37,237,83],{from: accounts[3]});
  });
  it('Should fail deleteStringValue(bytes32) when NOT comply with: sender == associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[3],{from:accounts[0]});
    let result = await truffleAssert.fails(contractEternalStorage.deleteStringValue([204,37,51,59,243,12,94,220,243,214,17,210,32,184,208,198,211,152,51,138,227,107,247,87,109,76,33,181,130,37,237,83],{from: accounts[9]}),'revert');
  });
  it('Should execute getAddressValue(bytes32)', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    let result = await contractEternalStorage.getAddressValue([113,129,110,112,28,159,2,70,157,133,75,230,240,93,168,234,203,238,153,127,12,112,29,66,252,173,250,129,170,201,29,231],{from: accounts[0]});
  });
  it('Should execute setAddressValue(bytes32,address) WHEN sender==associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[3],{from:accounts[0]});
    let result = await contractEternalStorage.setAddressValue([96,74,125,146,116,201,183,150,195,52,177,92,231,112,96,150,42,216,135,232,141,36,122,245,13,114,232,116,156,207,166,243], accounts[1],{from: accounts[3]});
  });
  it('Should fail setAddressValue(bytes32,address) when NOT comply with: sender == associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[3],{from:accounts[0]});
    let result = await truffleAssert.fails(contractEternalStorage.setAddressValue([96,74,125,146,116,201,183,150,195,52,177,92,231,112,96,150,42,216,135,232,141,36,122,245,13,114,232,116,156,207,166,243], accounts[1],{from: accounts[9]}),'revert');
  });
  it('Should execute deleteAddressValue(bytes32) WHEN sender==associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[3],{from:accounts[0]});
    let result = await contractEternalStorage.deleteAddressValue([100,181,14,40,217,224,238,204,99,49,124,162,3,211,200,164,240,113,222,141,9,101,8,127,217,153,118,132,87,253,178,164],{from: accounts[3]});
  });
  it('Should fail deleteAddressValue(bytes32) when NOT comply with: sender == associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[3],{from:accounts[0]});
    let result = await truffleAssert.fails(contractEternalStorage.deleteAddressValue([100,181,14,40,217,224,238,204,99,49,124,162,3,211,200,164,240,113,222,141,9,101,8,127,217,153,118,132,87,253,178,164],{from: accounts[9]}),'revert');
  });
  it('Should execute getBytesValue(bytes32)', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    let result = await contractEternalStorage.getBytesValue([36,197,193,55,172,225,34,76,198,68,55,13,107,93,34,51,229,39,169,2,134,134,177,227,38,118,45,189,165,44,63,231],{from: accounts[0]});
  });
  it('Should execute setBytesValue(bytes32,bytes) WHEN sender==associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[6],{from:accounts[0]});
    let result = await contractEternalStorage.setBytesValue([95,9,245,50,224,51,111,14,37,156,93,12,175,94,61,162,174,9,161,38,128,144,136,223,69,242,71,65,74,75,85,120], [42,48,38,169,144,253,86,6,207,49,8,109,129,121,170,31,101,85,154,62,226,145,138,9,69,134,146,215,225,102,19,142],{from: accounts[6]});
  });
  it('Should fail setBytesValue(bytes32,bytes) when NOT comply with: sender == associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[6],{from:accounts[0]});
    let result = await truffleAssert.fails(contractEternalStorage.setBytesValue([95,9,245,50,224,51,111,14,37,156,93,12,175,94,61,162,174,9,161,38,128,144,136,223,69,242,71,65,74,75,85,120], [42,48,38,169,144,253,86,6,207,49,8,109,129,121,170,31,101,85,154,62,226,145,138,9,69,134,146,215,225,102,19,142],{from: accounts[9]}),'revert');
  });
  it('Should execute deleteBytesValue(bytes32) WHEN sender==associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[6],{from:accounts[0]});
    let result = await contractEternalStorage.deleteBytesValue([55,191,84,177,182,231,42,66,134,106,45,8,160,45,98,176,82,157,241,88,55,253,192,228,58,164,43,126,8,16,19,245],{from: accounts[6]});
  });
  it('Should fail deleteBytesValue(bytes32) when NOT comply with: sender == associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[6],{from:accounts[0]});
    let result = await truffleAssert.fails(contractEternalStorage.deleteBytesValue([55,191,84,177,182,231,42,66,134,106,45,8,160,45,98,176,82,157,241,88,55,253,192,228,58,164,43,126,8,16,19,245],{from: accounts[9]}),'revert');
  });
  it('Should execute getBytes32Value(bytes32)', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    let result = await contractEternalStorage.getBytes32Value([145,68,159,16,32,51,103,100,68,91,231,79,248,228,138,213,0,141,247,196,144,73,184,97,120,149,116,241,15,97,147,194],{from: accounts[0]});
  });
  it('Should execute setBytes32Value(bytes32,bytes32) WHEN sender==associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[5],{from:accounts[0]});
    let result = await contractEternalStorage.setBytes32Value([82,119,90,76,81,190,196,160,110,112,84,100,125,56,255,89,188,155,30,172,29,190,206,134,94,176,9,247,12,197,144,13], [151,135,93,16,49,39,183,209,221,110,212,133,201,179,70,36,19,148,214,75,107,96,252,158,72,12,165,255,221,49,77,150],{from: accounts[5]});
  });
  it('Should fail setBytes32Value(bytes32,bytes32) when NOT comply with: sender == associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[5],{from:accounts[0]});
    let result = await truffleAssert.fails(contractEternalStorage.setBytes32Value([82,119,90,76,81,190,196,160,110,112,84,100,125,56,255,89,188,155,30,172,29,190,206,134,94,176,9,247,12,197,144,13], [151,135,93,16,49,39,183,209,221,110,212,133,201,179,70,36,19,148,214,75,107,96,252,158,72,12,165,255,221,49,77,150],{from: accounts[9]}),'revert');
  });
  it('Should execute deleteBytes32Value(bytes32) WHEN sender==associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[3],{from:accounts[0]});
    let result = await contractEternalStorage.deleteBytes32Value([43,239,13,117,92,147,53,235,170,200,221,180,35,164,199,54,112,184,217,10,79,215,63,52,154,141,177,152,204,53,7,86],{from: accounts[3]});
  });
  it('Should fail deleteBytes32Value(bytes32) when NOT comply with: sender == associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[3],{from:accounts[0]});
    let result = await truffleAssert.fails(contractEternalStorage.deleteBytes32Value([43,239,13,117,92,147,53,235,170,200,221,180,35,164,199,54,112,184,217,10,79,215,63,52,154,141,177,152,204,53,7,86],{from: accounts[9]}),'revert');
  });
  it('Should execute getBooleanValue(bytes32)', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    let result = await contractEternalStorage.getBooleanValue([241,198,155,145,239,245,245,69,148,241,150,67,145,230,233,87,113,41,74,182,223,82,46,125,51,154,219,9,119,121,169,65],{from: accounts[0]});
  });
  it('Should execute setBooleanValue(bytes32,bool) WHEN sender==associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[2],{from:accounts[0]});
    let result = await contractEternalStorage.setBooleanValue([234,16,103,246,105,151,75,99,38,15,235,58,163,167,163,156,160,213,68,4,239,162,114,30,96,61,251,140,153,236,75,200], false,{from: accounts[2]});
  });
  it('Should fail setBooleanValue(bytes32,bool) when NOT comply with: sender == associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[2],{from:accounts[0]});
    let result = await truffleAssert.fails(contractEternalStorage.setBooleanValue([234,16,103,246,105,151,75,99,38,15,235,58,163,167,163,156,160,213,68,4,239,162,114,30,96,61,251,140,153,236,75,200], false,{from: accounts[9]}),'revert');
  });
  it('Should execute deleteBooleanValue(bytes32) WHEN sender==associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[8],{from:accounts[0]});
    let result = await contractEternalStorage.deleteBooleanValue([95,9,245,50,224,51,111,14,37,156,93,12,175,94,61,162,174,9,161,38,128,144,136,223,69,242,71,65,74,75,85,120],{from: accounts[8]});
  });
  it('Should fail deleteBooleanValue(bytes32) when NOT comply with: sender == associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[8],{from:accounts[0]});
    let result = await truffleAssert.fails(contractEternalStorage.deleteBooleanValue([95,9,245,50,224,51,111,14,37,156,93,12,175,94,61,162,174,9,161,38,128,144,136,223,69,242,71,65,74,75,85,120],{from: accounts[9]}),'revert');
  });
  it('Should execute getIntValue(bytes32)', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    let result = await contractEternalStorage.getIntValue([63,186,69,41,187,238,221,100,71,173,73,150,55,182,28,38,29,171,24,58,102,167,247,94,232,29,236,13,16,202,146,254],{from: accounts[0]});
  });
  it('Should execute setIntValue(bytes32,int) WHEN sender==associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[1],{from:accounts[0]});
    let result = await contractEternalStorage.setIntValue([205,140,130,87,146,70,31,250,149,49,22,208,43,198,92,82,96,63,101,21,22,251,235,117,171,165,82,89,205,102,247,250], 18,{from: accounts[1]});
  });
  it('Should fail setIntValue(bytes32,int) when NOT comply with: sender == associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[1],{from:accounts[0]});
    let result = await truffleAssert.fails(contractEternalStorage.setIntValue([205,140,130,87,146,70,31,250,149,49,22,208,43,198,92,82,96,63,101,21,22,251,235,117,171,165,82,89,205,102,247,250], 18,{from: accounts[9]}),'revert');
  });
  it('Should execute deleteIntValue(bytes32) WHEN sender==associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[8],{from:accounts[0]});
    let result = await contractEternalStorage.deleteIntValue([164,209,154,123,219,160,36,16,165,102,218,172,7,201,165,255,226,2,225,22,5,230,98,186,132,101,70,45,48,163,2,189],{from: accounts[8]});
  });
  it('Should fail deleteIntValue(bytes32) when NOT comply with: sender == associatedContract', async () => {
    //transactionParameter: [object Object]
    //_owner: accounts[0]
    await contractEternalStorage.setAssociatedContract(accounts[8],{from:accounts[0]});
    let result = await truffleAssert.fails(contractEternalStorage.deleteIntValue([164,209,154,123,219,160,36,16,165,102,218,172,7,201,165,255,226,2,225,22,5,230,98,186,132,101,70,45,48,163,2,189],{from: accounts[9]}),'revert');
  });
});
