const TestContract = artifacts.require("TestSafeDecimalMath");
  const LibraryUnderTest = artifacts.require("SafeDecimalMath");

   async function doDeploy(deployer, network) {
       await deployer.deploy(LibraryUnderTest);
       await deployer.link(LibraryUnderTest, TestContract);
       await deployer.deploy(TestContract);
   }

 module.exports = (deployer, network) => {
      deployer.then(async () => {
              await doDeploy(deployer, network);
      });
  };