const truffleAssert = require('truffle-assertions');
const BusinessDayConvention = artifacts.require("BusinessDayConvention");
const ContractDefaultConvention = artifacts.require("ContractDefaultConvention");
const ContractRoleConvention = artifacts.require("ContractRoleConvention");
const DayCountConvention = artifacts.require("DayCountConvention");
const EndOfMonthConvention = artifacts.require("EndOfMonthConvention");
const Core = artifacts.require("Core");
const Definitions = artifacts.require("Definitions");
const FloatMath = artifacts.require("FloatMath");
const Schedule = artifacts.require("Schedule");
const Utils = artifacts.require("Utils");
const IEngine = artifacts.require("IEngine");
const PAMEngine = artifacts.require("PAMEngine");

contract("EndOfMonthConvention",(accounts)=>{
  let trace = false;
  let contractFloatMath = null;
  let contractBusinessDayConvention = null;
  let contractContractDefaultConvention = null;
  let contractContractRoleConvention = null;
  let contractDayCountConvention = null;
  let contractEndOfMonthConvention = null;
  let contractCore = null;
  let contractSchedule = null;
  let contractUtils = null;
  let contractPAMEngine = null;
  
  it('Should execute getEndOfMonthConvention(EndOfMonthConvention,uint256,Definitions.IPS) WHEN eomc==Definitions.EndOfMonthConvention.EOM', async () => {
    let result = await contractEndOfMonthConvention.getEndOfMonthConvention(0, 18, {"i": 7,"p": 5,"s": 0,"isSet": false},{from: accounts[0]});
  });
  it('Should execute getEndOfMonthConvention(EndOfMonthConvention,uint256,Definitions.IPS) WHEN eomc==Definitions.EndOfMonthConvention.SD', async () => {
    let result = await contractEndOfMonthConvention.getEndOfMonthConvention(1, 359, {"i": 29,"p": 5,"s": 0,"isSet": false},{from: accounts[0]});
  });
  it('Should execute getEndOfMonthConvention(EndOfMonthConvention,uint256,Definitions.IPS) WHEN eomc!=Definitions.EndOfMonthConvention.SD,eomc!=Definitions.EndOfMonthConvention.EOM', async () => {
    let result = await contractEndOfMonthConvention.getEndOfMonthConvention(99999, 111, {"i": 71,"p": 4,"s": 0,"isSet": false},{from: accounts[0]});
  });
});
