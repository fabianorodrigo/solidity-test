const truffleAssert = require('truffle-assertions');
const BusinessDayConvention = artifacts.require("BusinessDayConvention");
const ContractDefaultConvention = artifacts.require("ContractDefaultConvention");
const ContractRoleConvention = artifacts.require("ContractRoleConvention");
const DayCountConvention = artifacts.require("DayCountConvention");
const EndOfMonthConvention = artifacts.require("EndOfMonthConvention");
const Core = artifacts.require("Core");
const Definitions = artifacts.require("Definitions");
const FloatMath = artifacts.require("FloatMath");
const Schedule = artifacts.require("Schedule");
const Utils = artifacts.require("Utils");
const IEngine = artifacts.require("IEngine");
const PAMEngine = artifacts.require("PAMEngine");
const ProxyCore = artifacts.require("ProxyCore");

contract("contractProxy4TestCore",(accounts)=>{
    let contractProxy4TestCore = null;
  let trace = false;
  let contractFloatMath = null;
  let contractBusinessDayConvention = null;
  let contractContractDefaultConvention = null;
  let contractContractRoleConvention = null;
  let contractDayCountConvention = null;
  let contractEndOfMonthConvention = null;
  let contractCore = null;
  let contractSchedule = null;
  let contractUtils = null;
  let contractPAMEngine = null;    contractProxy4TestCore = await ProxyCore.new({ from: accounts[0] });

  
  it('Should execute signum(int) WHEN value>0', async () => {
    let result = await contractProxy4TestCore.testsignum(159,{from: accounts[0]});
  });
  it('Should execute signum(int) WHEN value<0', async () => {
    let result = await contractProxy4TestCore.testsignum(-1,{from: accounts[0]});
  });
  it('Should execute signum(int) WHEN value>=0,value<=0', async () => {
    let result = await contractProxy4TestCore.testsignum(0,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType==Definitions.EventType.IED', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(3,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType!=Definitions.EventType.IED', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(18,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType==Definitions.EventType.IP', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(4,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType!=Definitions.EventType.IP,eventType!=Definitions.EventType.IED', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(15,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType==Definitions.EventType.IPCI', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(11,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType!=Definitions.EventType.IPCI,eventType!=Definitions.EventType.IED,eventType!=Definitions.EventType.IP', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(5,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType==Definitions.EventType.FP', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(8,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType!=Definitions.EventType.FP,eventType!=Definitions.EventType.IED,eventType!=Definitions.EventType.IP,eventType!=Definitions.EventType.IPCI', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(6,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType==Definitions.EventType.DV', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(16,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType!=Definitions.EventType.DV,eventType!=Definitions.EventType.IED,eventType!=Definitions.EventType.IP,eventType!=Definitions.EventType.IPCI,eventType!=Definitions.EventType.FP', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(12,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType==Definitions.EventType.PR', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(5,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType!=Definitions.EventType.PR,eventType!=Definitions.EventType.IED,eventType!=Definitions.EventType.IP,eventType!=Definitions.EventType.IPCI,eventType!=Definitions.EventType.FP,eventType!=Definitions.EventType.DV', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(6,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType==Definitions.EventType.MR', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(17,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType!=Definitions.EventType.MR,eventType!=Definitions.EventType.IED,eventType!=Definitions.EventType.IP,eventType!=Definitions.EventType.IPCI,eventType!=Definitions.EventType.FP,eventType!=Definitions.EventType.DV,eventType!=Definitions.EventType.PR', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(2,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType==Definitions.EventType.RRY', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(13,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType!=Definitions.EventType.RRY,eventType!=Definitions.EventType.IED,eventType!=Definitions.EventType.IP,eventType!=Definitions.EventType.IPCI,eventType!=Definitions.EventType.FP,eventType!=Definitions.EventType.DV,eventType!=Definitions.EventType.PR,eventType!=Definitions.EventType.MR', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(10,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType==Definitions.EventType.RR', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(12,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType!=Definitions.EventType.RR,eventType!=Definitions.EventType.IED,eventType!=Definitions.EventType.IP,eventType!=Definitions.EventType.IPCI,eventType!=Definitions.EventType.FP,eventType!=Definitions.EventType.DV,eventType!=Definitions.EventType.PR,eventType!=Definitions.EventType.MR,eventType!=Definitions.EventType.RRY', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(9,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType==Definitions.EventType.SC', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(14,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType!=Definitions.EventType.SC,eventType!=Definitions.EventType.IED,eventType!=Definitions.EventType.IP,eventType!=Definitions.EventType.IPCI,eventType!=Definitions.EventType.FP,eventType!=Definitions.EventType.DV,eventType!=Definitions.EventType.PR,eventType!=Definitions.EventType.MR,eventType!=Definitions.EventType.RRY,eventType!=Definitions.EventType.RR', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(2,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType==Definitions.EventType.IPCB', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(18,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType!=Definitions.EventType.IPCB,eventType!=Definitions.EventType.IED,eventType!=Definitions.EventType.IP,eventType!=Definitions.EventType.IPCI,eventType!=Definitions.EventType.FP,eventType!=Definitions.EventType.DV,eventType!=Definitions.EventType.PR,eventType!=Definitions.EventType.MR,eventType!=Definitions.EventType.RRY,eventType!=Definitions.EventType.RR,eventType!=Definitions.EventType.SC', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(10,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType==Definitions.EventType.PRD', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(9,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType!=Definitions.EventType.PRD,eventType!=Definitions.EventType.IED,eventType!=Definitions.EventType.IP,eventType!=Definitions.EventType.IPCI,eventType!=Definitions.EventType.FP,eventType!=Definitions.EventType.DV,eventType!=Definitions.EventType.PR,eventType!=Definitions.EventType.MR,eventType!=Definitions.EventType.RRY,eventType!=Definitions.EventType.RR,eventType!=Definitions.EventType.SC,eventType!=Definitions.EventType.IPCB', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(10,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType==Definitions.EventType.TD', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(10,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType!=Definitions.EventType.TD,eventType!=Definitions.EventType.IED,eventType!=Definitions.EventType.IP,eventType!=Definitions.EventType.IPCI,eventType!=Definitions.EventType.FP,eventType!=Definitions.EventType.DV,eventType!=Definitions.EventType.PR,eventType!=Definitions.EventType.MR,eventType!=Definitions.EventType.RRY,eventType!=Definitions.EventType.RR,eventType!=Definitions.EventType.SC,eventType!=Definitions.EventType.IPCB,eventType!=Definitions.EventType.PRD', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(15,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType==Definitions.EventType.STD', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(19,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType!=Definitions.EventType.STD,eventType!=Definitions.EventType.IED,eventType!=Definitions.EventType.IP,eventType!=Definitions.EventType.IPCI,eventType!=Definitions.EventType.FP,eventType!=Definitions.EventType.DV,eventType!=Definitions.EventType.PR,eventType!=Definitions.EventType.MR,eventType!=Definitions.EventType.RRY,eventType!=Definitions.EventType.RR,eventType!=Definitions.EventType.SC,eventType!=Definitions.EventType.IPCB,eventType!=Definitions.EventType.PRD,eventType!=Definitions.EventType.TD', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(2,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType==Definitions.EventType.MD', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(1,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType!=Definitions.EventType.MD,eventType!=Definitions.EventType.IED,eventType!=Definitions.EventType.IP,eventType!=Definitions.EventType.IPCI,eventType!=Definitions.EventType.FP,eventType!=Definitions.EventType.DV,eventType!=Definitions.EventType.PR,eventType!=Definitions.EventType.MR,eventType!=Definitions.EventType.RRY,eventType!=Definitions.EventType.RR,eventType!=Definitions.EventType.SC,eventType!=Definitions.EventType.IPCB,eventType!=Definitions.EventType.PRD,eventType!=Definitions.EventType.TD,eventType!=Definitions.EventType.STD', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(15,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType==Definitions.EventType.SD', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(0,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType!=Definitions.EventType.SD,eventType!=Definitions.EventType.IED,eventType!=Definitions.EventType.IP,eventType!=Definitions.EventType.IPCI,eventType!=Definitions.EventType.FP,eventType!=Definitions.EventType.DV,eventType!=Definitions.EventType.PR,eventType!=Definitions.EventType.MR,eventType!=Definitions.EventType.RRY,eventType!=Definitions.EventType.RR,eventType!=Definitions.EventType.SC,eventType!=Definitions.EventType.IPCB,eventType!=Definitions.EventType.PRD,eventType!=Definitions.EventType.TD,eventType!=Definitions.EventType.STD,eventType!=Definitions.EventType.MD', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(6,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType==Definitions.EventType.AD', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(2,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType!=Definitions.EventType.AD,eventType!=Definitions.EventType.IED,eventType!=Definitions.EventType.IP,eventType!=Definitions.EventType.IPCI,eventType!=Definitions.EventType.FP,eventType!=Definitions.EventType.DV,eventType!=Definitions.EventType.PR,eventType!=Definitions.EventType.MR,eventType!=Definitions.EventType.RRY,eventType!=Definitions.EventType.RR,eventType!=Definitions.EventType.SC,eventType!=Definitions.EventType.IPCB,eventType!=Definitions.EventType.PRD,eventType!=Definitions.EventType.TD,eventType!=Definitions.EventType.STD,eventType!=Definitions.EventType.MD,eventType!=Definitions.EventType.SD', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(6,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType==Definitions.EventType.Child', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(20,{from: accounts[0]});
  });
  it('Should execute getEpochOffset(EventType) WHEN eventType!=Definitions.EventType.Child,eventType!=Definitions.EventType.IED,eventType!=Definitions.EventType.IP,eventType!=Definitions.EventType.IPCI,eventType!=Definitions.EventType.FP,eventType!=Definitions.EventType.DV,eventType!=Definitions.EventType.PR,eventType!=Definitions.EventType.MR,eventType!=Definitions.EventType.RRY,eventType!=Definitions.EventType.RR,eventType!=Definitions.EventType.SC,eventType!=Definitions.EventType.IPCB,eventType!=Definitions.EventType.PRD,eventType!=Definitions.EventType.TD,eventType!=Definitions.EventType.STD,eventType!=Definitions.EventType.MD,eventType!=Definitions.EventType.SD,eventType!=Definitions.EventType.AD', async () => {
    let result = await contractProxy4TestCore.testgetEpochOffset(7,{from: accounts[0]});
  });
});
