const truffleAssert = require('truffle-assertions');
const BusinessDayConvention = artifacts.require("BusinessDayConvention");
const ContractDefaultConvention = artifacts.require("ContractDefaultConvention");
const ContractRoleConvention = artifacts.require("ContractRoleConvention");
const DayCountConvention = artifacts.require("DayCountConvention");
const EndOfMonthConvention = artifacts.require("EndOfMonthConvention");
const Core = artifacts.require("Core");
const Definitions = artifacts.require("Definitions");
const FloatMath = artifacts.require("FloatMath");
const Schedule = artifacts.require("Schedule");
const Utils = artifacts.require("Utils");
const IEngine = artifacts.require("IEngine");
const PAMEngine = artifacts.require("PAMEngine");
const ProxyEndOfMonthConvention = artifacts.require("ProxyEndOfMonthConvention");

contract("contractProxy4TestEndOfMonthConvention",(accounts)=>{
    let contractProxy4TestEndOfMonthConvention = null;
  let trace = false;
  let contractFloatMath = null;
  let contractBusinessDayConvention = null;
  let contractContractDefaultConvention = null;
  let contractContractRoleConvention = null;
  let contractDayCountConvention = null;
  let contractEndOfMonthConvention = null;
  let contractCore = null;
  let contractSchedule = null;
  let contractUtils = null;
  let contractPAMEngine = null;    contractProxy4TestEndOfMonthConvention = await ProxyEndOfMonthConvention.new({ from: accounts[0] });

  
  it('Should execute getEndOfMonthConvention(EndOfMonthConvention,uint256,Definitions.IPS) WHEN eomc==Definitions.EndOfMonthConvention.EOM', async () => {
    let result = await contractProxy4TestEndOfMonthConvention.testgetEndOfMonthConvention(0, 101, {"i": 10,"p": 5,"s": 0,"isSet": true},{from: accounts[0]});
  });
  it('Should execute getEndOfMonthConvention(EndOfMonthConvention,uint256,Definitions.IPS) WHEN eomc==Definitions.EndOfMonthConvention.SD', async () => {
    let result = await contractProxy4TestEndOfMonthConvention.testgetEndOfMonthConvention(1, 120, {"i": 91,"p": 2,"s": 1,"isSet": true},{from: accounts[0]});
  });
  it('Should execute getEndOfMonthConvention(EndOfMonthConvention,uint256,Definitions.IPS) WHEN eomc!=Definitions.EndOfMonthConvention.SD,eomc!=Definitions.EndOfMonthConvention.EOM', async () => {
    let result = await contractProxy4TestEndOfMonthConvention.testgetEndOfMonthConvention(99999, 3, {"i": 1,"p": 5,"s": 1,"isSet": false},{from: accounts[0]});
  });
  it('Should execute shiftEndOfMonth(uint256)', async () => {
    let result = await contractProxy4TestEndOfMonthConvention.testshiftEndOfMonth(161,{from: accounts[0]});
  });
  it('Should execute shiftSameDay(uint256)', async () => {
    let result = await contractProxy4TestEndOfMonthConvention.testshiftSameDay(150,{from: accounts[0]});
  });
});
