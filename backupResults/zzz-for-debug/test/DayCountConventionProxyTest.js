const truffleAssert = require('truffle-assertions');
const BusinessDayConvention = artifacts.require("BusinessDayConvention");
const ContractDefaultConvention = artifacts.require("ContractDefaultConvention");
const ContractRoleConvention = artifacts.require("ContractRoleConvention");
const DayCountConvention = artifacts.require("DayCountConvention");
const EndOfMonthConvention = artifacts.require("EndOfMonthConvention");
const Core = artifacts.require("Core");
const Definitions = artifacts.require("Definitions");
const FloatMath = artifacts.require("FloatMath");
const Schedule = artifacts.require("Schedule");
const Utils = artifacts.require("Utils");
const IEngine = artifacts.require("IEngine");
const PAMEngine = artifacts.require("PAMEngine");
const ProxyDayCountConvention = artifacts.require("ProxyDayCountConvention");

contract("contractProxy4TestDayCountConvention",(accounts)=>{
    let contractProxy4TestDayCountConvention = null;
  let trace = false;
  let contractFloatMath = null;
  let contractBusinessDayConvention = null;
  let contractContractDefaultConvention = null;
  let contractContractRoleConvention = null;
  let contractDayCountConvention = null;
  let contractEndOfMonthConvention = null;
  let contractCore = null;
  let contractSchedule = null;
  let contractUtils = null;
  let contractPAMEngine = null;    contractProxy4TestDayCountConvention = await ProxyDayCountConvention.new({ from: accounts[0] });

  
  it('Should execute yearFraction(uint256,uint256,DayCountConvention) WHEN ipdc==Definitions.DayCountConvention.A_AISDA', async () => {
    let result = await contractProxy4TestDayCountConvention.testyearFraction(951, 86399, 0,{from: accounts[0]});
  });
  it('Should fail yearFraction(uint256,uint256,DayCountConvention) when NOT comply with: endTimestamp >= startTimestamp', async () => {
    let result = await truffleAssert.fails(contractProxy4TestDayCountConvention.testyearFraction(951, 950, 0,{from: accounts[0]}),'revert');
  });
  it('Should execute yearFraction(uint256,uint256,DayCountConvention) WHEN ipdc==Definitions.DayCountConvention.A_360', async () => {
    let result = await contractProxy4TestDayCountConvention.testyearFraction(109, 950, 1,{from: accounts[0]});
  });
  it('Should fail yearFraction(uint256,uint256,DayCountConvention) when NOT comply with: endTimestamp >= startTimestamp', async () => {
    let result = await truffleAssert.fails(contractProxy4TestDayCountConvention.testyearFraction(109, 108, 1,{from: accounts[0]}),'revert');
  });
  it('Should execute yearFraction(uint256,uint256,DayCountConvention) WHEN ipdc==Definitions.DayCountConvention.A_365', async () => {
    let result = await contractProxy4TestDayCountConvention.testyearFraction(59, 361, 2,{from: accounts[0]});
  });
  it('Should fail yearFraction(uint256,uint256,DayCountConvention) when NOT comply with: endTimestamp >= startTimestamp', async () => {
    let result = await truffleAssert.fails(contractProxy4TestDayCountConvention.testyearFraction(59, 58, 2,{from: accounts[0]}),'revert');
  });
  it('Should execute yearFraction(uint256,uint256,DayCountConvention) WHEN ipdc==Definitions.DayCountConvention._30E_360', async () => {
    let result = await contractProxy4TestDayCountConvention.testyearFraction(120, 254, 4,{from: accounts[0]});
  });
  it('Should fail yearFraction(uint256,uint256,DayCountConvention) when NOT comply with: endTimestamp >= startTimestamp', async () => {
    let result = await truffleAssert.fails(contractProxy4TestDayCountConvention.testyearFraction(120, 119, 4,{from: accounts[0]}),'revert');
  });
  it('Should execute yearFraction(uint256,uint256,DayCountConvention) WHEN ipdc!=Definitions.DayCountConvention._30E_360,ipdc!=Definitions.DayCountConvention.A_365,ipdc!=Definitions.DayCountConvention.A_360,ipdc!=Definitions.DayCountConvention.A_AISDA', async () => {
    let result = await contractProxy4TestDayCountConvention.testyearFraction(161, 366, 3,{from: accounts[0]});
  });
  it('Should fail yearFraction(uint256,uint256,DayCountConvention) when NOT comply with: endTimestamp >= startTimestamp', async () => {
    let result = await truffleAssert.fails(contractProxy4TestDayCountConvention.testyearFraction(161, 160, 3,{from: accounts[0]}),'revert');
  });
  it('Should execute actualActualISDA(uint256,uint256)', async () => {
    let result = await contractProxy4TestDayCountConvention.testactualActualISDA(40, 6,{from: accounts[0]});
  });
  it('Should execute actualThreeSixty(uint256,uint256)', async () => {
    let result = await contractProxy4TestDayCountConvention.testactualThreeSixty(141, 149,{from: accounts[0]});
  });
  it('Should execute actualThreeSixtyFive(uint256,uint256)', async () => {
    let result = await contractProxy4TestDayCountConvention.testactualThreeSixtyFive(119, 63,{from: accounts[0]});
  });
  it('Should execute thirtyEThreeSixty(uint256,uint256)', async () => {
    let result = await contractProxy4TestDayCountConvention.testthirtyEThreeSixty(900, 359,{from: accounts[0]});
  });
});
