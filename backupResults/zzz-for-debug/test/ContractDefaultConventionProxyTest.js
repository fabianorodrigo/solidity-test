const truffleAssert = require('truffle-assertions');
const BusinessDayConvention = artifacts.require("BusinessDayConvention");
const ContractDefaultConvention = artifacts.require("ContractDefaultConvention");
const ContractRoleConvention = artifacts.require("ContractRoleConvention");
const DayCountConvention = artifacts.require("DayCountConvention");
const EndOfMonthConvention = artifacts.require("EndOfMonthConvention");
const Core = artifacts.require("Core");
const Definitions = artifacts.require("Definitions");
const FloatMath = artifacts.require("FloatMath");
const Schedule = artifacts.require("Schedule");
const Utils = artifacts.require("Utils");
const IEngine = artifacts.require("IEngine");
const PAMEngine = artifacts.require("PAMEngine");
const ProxyContractDefaultConvention = artifacts.require("ProxyContractDefaultConvention");

contract("contractProxy4TestContractDefaultConvention",(accounts)=>{
    let contractProxy4TestContractDefaultConvention = null;
  let trace = false;
  let contractFloatMath = null;
  let contractBusinessDayConvention = null;
  let contractContractDefaultConvention = null;
  let contractContractRoleConvention = null;
  let contractDayCountConvention = null;
  let contractEndOfMonthConvention = null;
  let contractCore = null;
  let contractSchedule = null;
  let contractUtils = null;
  let contractPAMEngine = null;    contractProxy4TestContractDefaultConvention = await ProxyContractDefaultConvention.new({ from: accounts[0] });

  
  it('Should execute performanceIndicator(ContractStatus) WHEN contractStatus==Definitions.ContractStatus.DF', async () => {
    let result = await contractProxy4TestContractDefaultConvention.testperformanceIndicator(3,{from: accounts[0]});
  });
  it('Should execute performanceIndicator(ContractStatus) WHEN contractStatus!=Definitions.ContractStatus.DF', async () => {
    let result = await contractProxy4TestContractDefaultConvention.testperformanceIndicator(2,{from: accounts[0]});
  });
});
