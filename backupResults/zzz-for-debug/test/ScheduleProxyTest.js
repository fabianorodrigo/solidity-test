const truffleAssert = require('truffle-assertions');
const BusinessDayConvention = artifacts.require("BusinessDayConvention");
const ContractDefaultConvention = artifacts.require("ContractDefaultConvention");
const ContractRoleConvention = artifacts.require("ContractRoleConvention");
const DayCountConvention = artifacts.require("DayCountConvention");
const EndOfMonthConvention = artifacts.require("EndOfMonthConvention");
const Core = artifacts.require("Core");
const Definitions = artifacts.require("Definitions");
const FloatMath = artifacts.require("FloatMath");
const Schedule = artifacts.require("Schedule");
const Utils = artifacts.require("Utils");
const IEngine = artifacts.require("IEngine");
const PAMEngine = artifacts.require("PAMEngine");
const ProxySchedule = artifacts.require("ProxySchedule");

contract("contractProxy4TestSchedule",(accounts)=>{
    let contractProxy4TestSchedule = null;
  let trace = false;
  let contractFloatMath = null;
  let contractBusinessDayConvention = null;
  let contractContractDefaultConvention = null;
  let contractContractRoleConvention = null;
  let contractDayCountConvention = null;
  let contractEndOfMonthConvention = null;
  let contractCore = null;
  let contractSchedule = null;
  let contractUtils = null;
  let contractPAMEngine = null;    contractProxy4TestSchedule = await ProxySchedule.new({ from: accounts[0] });

  
  it('Should execute getNextCycleDate(Definitions.IPS,uint256,uint256) WHEN cycle.p==Definitions.P.D', async () => {
    let result = await contractProxy4TestSchedule.testgetNextCycleDate({"i": 20,"p": 0,"s": 0,"isSet": false}, 89, 49,{from: accounts[0]});
  });
  it('Should execute getNextCycleDate(Definitions.IPS,uint256,uint256) WHEN cycle.p==Definitions.P.W', async () => {
    let result = await contractProxy4TestSchedule.testgetNextCycleDate({"i": 901,"p": 1,"s": 0,"isSet": false}, 50, 951,{from: accounts[0]});
  });
  it('Should execute getNextCycleDate(Definitions.IPS,uint256,uint256) WHEN cycle.p==Definitions.P.M', async () => {
    let result = await contractProxy4TestSchedule.testgetNextCycleDate({"i": 61,"p": 2,"s": 1,"isSet": false}, 140, 9,{from: accounts[0]});
  });
  it('Should execute getNextCycleDate(Definitions.IPS,uint256,uint256) WHEN cycle.p==Definitions.P.Q', async () => {
    let result = await contractProxy4TestSchedule.testgetNextCycleDate({"i": 79,"p": 3,"s": 1,"isSet": false}, 364, 951,{from: accounts[0]});
  });
  it('Should execute getNextCycleDate(Definitions.IPS,uint256,uint256) WHEN cycle.p==Definitions.P.H', async () => {
    let result = await contractProxy4TestSchedule.testgetNextCycleDate({"i": 366,"p": 4,"s": 0,"isSet": false}, 365, 131,{from: accounts[0]});
  });
  it('Should execute getNextCycleDate(Definitions.IPS,uint256,uint256) WHEN cycle.p==Definitions.P.Y', async () => {
    let result = await contractProxy4TestSchedule.testgetNextCycleDate({"i": 60,"p": 5,"s": 1,"isSet": false}, 17, 15,{from: accounts[0]});
  });
  it('Should execute getNextCycleDate(Definitions.IPS,uint256,uint256) WHEN cycle.p!=Definitions.P.Y,cycle.p!=Definitions.P.H,cycle.p!=Definitions.P.Q,cycle.p!=Definitions.P.M,cycle.p!=Definitions.P.W,cycle.p!=Definitions.P.D', async () => {
    let result = await contractProxy4TestSchedule.testgetNextCycleDate({"i": 950,"p": 99999,"s": 1,"isSet": true}, 140, 160,{from: accounts[0]});
  });
  it('Should execute computeDatesFromCycleSegment(uint256,uint256,Definitions.IPS,EndOfMonthConvention,bool,uint256,uint256) WHEN cycle.isSet==false', async () => {
    let result = await contractProxy4TestSchedule.testcomputeDatesFromCycleSegment(111, 120, {"i": 81,"p": 4,"s": 0,"isSet": true}, 1, true, 160, 65,{from: accounts[0]});
  });
  it('Should execute computeDatesFromCycleSegment(uint256,uint256,Definitions.IPS,EndOfMonthConvention,bool,uint256,uint256) WHEN cycle.isSet!=false', async () => {
    let result = await contractProxy4TestSchedule.testcomputeDatesFromCycleSegment(1, 9, {"i": 69,"p": 5,"s": 0,"isSet": false}, 1, false, 110, 10,{from: accounts[0]});
  });
  it('Should execute computeDatesFromCycleSegment(uint256,uint256,Definitions.IPS,EndOfMonthConvention,bool,uint256,uint256) WHEN addEndTime==true', async () => {
    let result = await contractProxy4TestSchedule.testcomputeDatesFromCycleSegment(80, 63, {"i": 69,"p": 1,"s": 0,"isSet": false}, 0, false, 10, 100,{from: accounts[0]});
  });
  it('Should execute computeDatesFromCycleSegment(uint256,uint256,Definitions.IPS,EndOfMonthConvention,bool,uint256,uint256) WHEN addEndTime!=true,cycle.isSet!=false', async () => {
    let result = await contractProxy4TestSchedule.testcomputeDatesFromCycleSegment(151, 150, {"i": 149,"p": 3,"s": 0,"isSet": false}, 1, true, 359, 86401,{from: accounts[0]});
  });
});
