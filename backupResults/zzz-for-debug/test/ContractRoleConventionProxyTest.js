const truffleAssert = require('truffle-assertions');
const BusinessDayConvention = artifacts.require("BusinessDayConvention");
const ContractDefaultConvention = artifacts.require("ContractDefaultConvention");
const ContractRoleConvention = artifacts.require("ContractRoleConvention");
const DayCountConvention = artifacts.require("DayCountConvention");
const EndOfMonthConvention = artifacts.require("EndOfMonthConvention");
const Core = artifacts.require("Core");
const Definitions = artifacts.require("Definitions");
const FloatMath = artifacts.require("FloatMath");
const Schedule = artifacts.require("Schedule");
const Utils = artifacts.require("Utils");
const IEngine = artifacts.require("IEngine");
const PAMEngine = artifacts.require("PAMEngine");
const ProxyContractRoleConvention = artifacts.require("ProxyContractRoleConvention");

contract("contractProxy4TestContractRoleConvention",(accounts)=>{
    let contractProxy4TestContractRoleConvention = null;
  let trace = false;
  let contractFloatMath = null;
  let contractBusinessDayConvention = null;
  let contractContractDefaultConvention = null;
  let contractContractRoleConvention = null;
  let contractDayCountConvention = null;
  let contractEndOfMonthConvention = null;
  let contractCore = null;
  let contractSchedule = null;
  let contractUtils = null;
  let contractPAMEngine = null;    contractProxy4TestContractRoleConvention = await ProxyContractRoleConvention.new({ from: accounts[0] });

  
  it('Should execute roleSign(ContractRole) WHEN contractRole==Definitions.ContractRole.RPA', async () => {
    let result = await contractProxy4TestContractRoleConvention.testroleSign(0,{from: accounts[0]});
  });
  it('Should execute roleSign(ContractRole) WHEN contractRole!=Definitions.ContractRole.RPA', async () => {
    let result = await contractProxy4TestContractRoleConvention.testroleSign(1,{from: accounts[0]});
  });
  it('Should execute roleSign(ContractRole) WHEN contractRole==Definitions.ContractRole.RPL', async () => {
    let result = await contractProxy4TestContractRoleConvention.testroleSign(1,{from: accounts[0]});
  });
  it('Should execute roleSign(ContractRole) WHEN contractRole!=Definitions.ContractRole.RPL,contractRole!=Definitions.ContractRole.RPA', async () => {
    let result = await contractProxy4TestContractRoleConvention.testroleSign(6,{from: accounts[0]});
  });
});
