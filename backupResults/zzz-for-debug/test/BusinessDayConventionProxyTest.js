const truffleAssert = require('truffle-assertions');
const BusinessDayConvention = artifacts.require("BusinessDayConvention");
const ContractDefaultConvention = artifacts.require("ContractDefaultConvention");
const ContractRoleConvention = artifacts.require("ContractRoleConvention");
const DayCountConvention = artifacts.require("DayCountConvention");
const EndOfMonthConvention = artifacts.require("EndOfMonthConvention");
const Core = artifacts.require("Core");
const Definitions = artifacts.require("Definitions");
const FloatMath = artifacts.require("FloatMath");
const Schedule = artifacts.require("Schedule");
const Utils = artifacts.require("Utils");
const IEngine = artifacts.require("IEngine");
const PAMEngine = artifacts.require("PAMEngine");
const ProxyBusinessDayConvention = artifacts.require("ProxyBusinessDayConvention");

contract("contractProxy4TestBusinessDayConvention",(accounts)=>{
    let contractProxy4TestBusinessDayConvention = null;
  let trace = false;
  let contractFloatMath = null;
  let contractBusinessDayConvention = null;
  let contractContractDefaultConvention = null;
  let contractContractRoleConvention = null;
  let contractDayCountConvention = null;
  let contractEndOfMonthConvention = null;
  let contractCore = null;
  let contractSchedule = null;
  let contractUtils = null;
  let contractPAMEngine = null;    contractProxy4TestBusinessDayConvention = await ProxyBusinessDayConvention.new({ from: accounts[0] });

  
  it('Should execute shiftCalcTime(uint256,BusinessDayConvention,Calendar) WHEN convention==Definitions.BusinessDayConvention.CSF', async () => {
    let result = await contractProxy4TestBusinessDayConvention.testshiftCalcTime(17, 3, 1,{from: accounts[0]});
  });
  it('Should execute shiftCalcTime(uint256,BusinessDayConvention,Calendar) WHEN convention==Definitions.BusinessDayConvention.CSMF', async () => {
    let result = await contractProxy4TestBusinessDayConvention.testshiftCalcTime(151, 4, 0,{from: accounts[0]});
  });
  it('Should execute shiftCalcTime(uint256,BusinessDayConvention,Calendar) WHEN convention==Definitions.BusinessDayConvention.CSP', async () => {
    let result = await contractProxy4TestBusinessDayConvention.testshiftCalcTime(80, 7, 0,{from: accounts[0]});
  });
  it('Should execute shiftCalcTime(uint256,BusinessDayConvention,Calendar) WHEN convention==Definitions.BusinessDayConvention.CSMP', async () => {
    let result = await contractProxy4TestBusinessDayConvention.testshiftCalcTime(86400, 8, 1,{from: accounts[0]});
  });
  it('Should execute shiftCalcTime(uint256,BusinessDayConvention,Calendar) WHEN convention!=Definitions.BusinessDayConvention.CSF,convention!=Definitions.BusinessDayConvention.CSMF,convention!=Definitions.BusinessDayConvention.CSP,convention!=Definitions.BusinessDayConvention.CSMP', async () => {
    let result = await contractProxy4TestBusinessDayConvention.testshiftCalcTime(360, 1, 0,{from: accounts[0]});
  });
  it('Should execute shiftEventTime(uint256,BusinessDayConvention,Calendar) WHEN convention==Definitions.BusinessDayConvention.SCF', async () => {
    let result = await contractProxy4TestBusinessDayConvention.testshiftEventTime(81, 1, 1,{from: accounts[0]});
  });
  it('Should execute shiftEventTime(uint256,BusinessDayConvention,Calendar) WHEN convention==Definitions.BusinessDayConvention.CSF', async () => {
    let result = await contractProxy4TestBusinessDayConvention.testshiftEventTime(63, 3, 0,{from: accounts[0]});
  });
  it('Should execute shiftEventTime(uint256,BusinessDayConvention,Calendar) WHEN convention==Definitions.BusinessDayConvention.SCMF', async () => {
    let result = await contractProxy4TestBusinessDayConvention.testshiftEventTime(80, 2, 0,{from: accounts[0]});
  });
  it('Should execute shiftEventTime(uint256,BusinessDayConvention,Calendar) WHEN convention==Definitions.BusinessDayConvention.CSMF', async () => {
    let result = await contractProxy4TestBusinessDayConvention.testshiftEventTime(86401, 4, 0,{from: accounts[0]});
  });
  it('Should execute shiftEventTime(uint256,BusinessDayConvention,Calendar) WHEN convention==Definitions.BusinessDayConvention.SCP', async () => {
    let result = await contractProxy4TestBusinessDayConvention.testshiftEventTime(129, 5, 1,{from: accounts[0]});
  });
  it('Should execute shiftEventTime(uint256,BusinessDayConvention,Calendar) WHEN convention==Definitions.BusinessDayConvention.CSP', async () => {
    let result = await contractProxy4TestBusinessDayConvention.testshiftEventTime(5, 7, 1,{from: accounts[0]});
  });
  it('Should execute shiftEventTime(uint256,BusinessDayConvention,Calendar) WHEN convention==Definitions.BusinessDayConvention.SCMP', async () => {
    let result = await contractProxy4TestBusinessDayConvention.testshiftEventTime(19, 6, 1,{from: accounts[0]});
  });
  it('Should execute shiftEventTime(uint256,BusinessDayConvention,Calendar) WHEN convention==Definitions.BusinessDayConvention.CSMP', async () => {
    let result = await contractProxy4TestBusinessDayConvention.testshiftEventTime(79, 8, 1,{from: accounts[0]});
  });
  it('Should execute shiftEventTime(uint256,BusinessDayConvention,Calendar) WHEN convention!=Definitions.BusinessDayConvention.SCMP,convention!=Definitions.BusinessDayConvention.CSMP,convention!=Definitions.BusinessDayConvention.SCP,convention!=Definitions.BusinessDayConvention.CSP,convention!=Definitions.BusinessDayConvention.SCMF,convention!=Definitions.BusinessDayConvention.CSMF,convention!=Definitions.BusinessDayConvention.SCF,convention!=Definitions.BusinessDayConvention.CSF', async () => {
    let result = await contractProxy4TestBusinessDayConvention.testshiftEventTime(951, 0, 1,{from: accounts[0]});
  });
  it('Should execute getClosestBusinessDaySameDayOrFollowing(uint256,Calendar) WHEN calendar==Definitions.Calendar.MondayToFriday', async () => {
    let result = await contractProxy4TestBusinessDayConvention.testgetClosestBusinessDaySameDayOrFollowing(86400, 1,{from: accounts[0]});
  });
  it('Should execute getClosestBusinessDaySameDayOrFollowing(uint256,Calendar) WHEN calendar!=Definitions.Calendar.MondayToFriday', async () => {
    let result = await contractProxy4TestBusinessDayConvention.testgetClosestBusinessDaySameDayOrFollowing(99, 0,{from: accounts[0]});
  });
  it('Should execute getClosestBusinessDaySameDayOrPreceeding(uint256,Calendar) WHEN calendar==Definitions.Calendar.MondayToFriday', async () => {
    let result = await contractProxy4TestBusinessDayConvention.testgetClosestBusinessDaySameDayOrPreceeding(360, 1,{from: accounts[0]});
  });
  it('Should execute getClosestBusinessDaySameDayOrPreceeding(uint256,Calendar) WHEN calendar!=Definitions.Calendar.MondayToFriday', async () => {
    let result = await contractProxy4TestBusinessDayConvention.testgetClosestBusinessDaySameDayOrPreceeding(110, 0,{from: accounts[0]});
  });
});
