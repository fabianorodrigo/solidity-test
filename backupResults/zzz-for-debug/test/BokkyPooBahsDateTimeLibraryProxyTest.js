const truffleAssert = require('truffle-assertions');
const BusinessDayConvention = artifacts.require("BusinessDayConvention");
const ContractDefaultConvention = artifacts.require("ContractDefaultConvention");
const ContractRoleConvention = artifacts.require("ContractRoleConvention");
const DayCountConvention = artifacts.require("DayCountConvention");
const EndOfMonthConvention = artifacts.require("EndOfMonthConvention");
const Core = artifacts.require("Core");
const Definitions = artifacts.require("Definitions");
const FloatMath = artifacts.require("FloatMath");
const Schedule = artifacts.require("Schedule");
const Utils = artifacts.require("Utils");
const IEngine = artifacts.require("IEngine");
const PAMEngine = artifacts.require("PAMEngine");
const BokkyPooBahsDateTimeLibrary = artifacts.require("BokkyPooBahsDateTimeLibrary");
const ProxyBokkyPooBahsDateTimeLibrary = artifacts.require("ProxyBokkyPooBahsDateTimeLibrary");

contract("BokkyPooBahsDateTimeLibrary",(accounts)=>{
    let contractProxy4TestBokkyPooBahsDateTimeLibrary = null;
  let trace = false;
  let contractBokkyPooBahsDateTimeLibrary = null;
  let contractFloatMath = null;
  let contractContractRoleConvention = null;
  let contractDayCountConvention = null;
  let contractEndOfMonthConvention = null;
  let contractContractDefaultConvention = null;
  let contractBusinessDayConvention = null;
  let contractSchedule = null;
  let contractUtils = null;
  let contractPAMEngine = null;
  let contractCore = null;
  beforeEach(async () => {
    contractBokkyPooBahsDateTimeLibrary = await BokkyPooBahsDateTimeLibrary.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: BokkyPooBahsDateTimeLibrary.new({from: accounts[0]}');
    contractFloatMath = await FloatMath.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: FloatMath.new({from: accounts[0]}');
    contractContractRoleConvention = await ContractRoleConvention.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: ContractRoleConvention.new({from: accounts[0]}');
    DayCountConvention.link("BokkyPooBahsDateTimeLibrary",contractBokkyPooBahsDateTimeLibrary.address);
     DayCountConvention.link("FloatMath",contractFloatMath.address);
    contractDayCountConvention = await DayCountConvention.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: DayCountConvention.new({from: accounts[0]}');
    EndOfMonthConvention.link("BokkyPooBahsDateTimeLibrary",contractBokkyPooBahsDateTimeLibrary.address);
    contractEndOfMonthConvention = await EndOfMonthConvention.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: EndOfMonthConvention.new({from: accounts[0]}');
    contractContractDefaultConvention = await ContractDefaultConvention.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: ContractDefaultConvention.new({from: accounts[0]}');
    BusinessDayConvention.link("BokkyPooBahsDateTimeLibrary",contractBokkyPooBahsDateTimeLibrary.address);
    contractBusinessDayConvention = await BusinessDayConvention.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: BusinessDayConvention.new({from: accounts[0]}');
    Schedule.link("BokkyPooBahsDateTimeLibrary",contractBokkyPooBahsDateTimeLibrary.address);
    contractSchedule = await Schedule.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: Schedule.new({from: accounts[0]}');
    Utils.link("BokkyPooBahsDateTimeLibrary",contractBokkyPooBahsDateTimeLibrary.address);
    contractUtils = await Utils.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: Utils.new({from: accounts[0]}');
    PAMEngine.link("FloatMath",contractFloatMath.address);
    contractPAMEngine = await PAMEngine.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: PAMEngine.new({from: accounts[0]}');
    contractCore = await Core.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: Core.new({from: accounts[0]}');
      ProxyBokkyPooBahsDateTimeLibrary.link('BokkyPooBahsDateTimeLibrary', contractBokkyPooBahsDateTimeLibrary.address);
    contractProxy4TestBokkyPooBahsDateTimeLibrary = await ProxyBokkyPooBahsDateTimeLibrary.new({ from: accounts[0] });
});
  
  it('Should execute _daysFromDate', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.test_daysFromDate(4899, 86401, 365,{from: accounts[0]});
  });
  it('Should fail _daysFromDate when NOT comply with: year >= 1970', async () => {
    let result = await truffleAssert.fails(contractProxy4TestBokkyPooBahsDateTimeLibrary.test_daysFromDate(1969, 86401, 365,{from: accounts[0]}),'revert');
  });
  it('Should execute _daysToDate', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.test_daysToDate(111,{from: accounts[0]});
  });
  it('Should execute timestampFromDate', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testtimestampFromDate(4801, 81, 131,{from: accounts[0]});
  });
  it('Should execute timestampFromDateTime', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testtimestampFromDateTime(48, 146098, 1461000, 949, 901, 146098,{from: accounts[0]});
  });
  it('Should execute timestampToDate', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testtimestampToDate(146098,{from: accounts[0]});
  });
  it('Should execute timestampToDateTime', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testtimestampToDateTime(2440587,{from: accounts[0]});
  });
  it('Should execute isValidDate WHEN year>=1970,month>0,month<=12', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testisValidDate(4800, 4, 899,{from: accounts[0]});
  });
  it('Should execute isValidDate WHEN month>12', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testisValidDate(109, 161, 2447,{from: accounts[0]});
  });
  it('Should execute isValidDateTime', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testisValidDateTime(68570, 146096, 951, 10, 146098, 109,{from: accounts[0]});
  });
  it('Should execute isLeapYear', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testisLeapYear(130,{from: accounts[0]});
  });
  it('Should execute _isLeapYear', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.test_isLeapYear(59,{from: accounts[0]});
  });
  it('Should execute isWeekDay', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testisWeekDay(130,{from: accounts[0]});
  });
  it('Should execute isWeekEnd', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testisWeekEnd(4,{from: accounts[0]});
  });
  it('Should execute getDaysInMonth', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testgetDaysInMonth(86401,{from: accounts[0]});
  });
  it('Should execute _getDaysInMonth WHEN month==1', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.test_getDaysInMonth(2447, 1,{from: accounts[0]});
  });
  it('Should execute _getDaysInMonth WHEN month==3', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.test_getDaysInMonth(23, 3,{from: accounts[0]});
  });
  it('Should execute _getDaysInMonth WHEN month==5', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.test_getDaysInMonth(51, 5,{from: accounts[0]});
  });
  it('Should execute _getDaysInMonth WHEN month==7', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.test_getDaysInMonth(159, 7,{from: accounts[0]});
  });
  it('Should execute _getDaysInMonth WHEN month==8', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.test_getDaysInMonth(90, 8,{from: accounts[0]});
  });
  it('Should execute _getDaysInMonth WHEN month==10', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.test_getDaysInMonth(899, 10,{from: accounts[0]});
  });
  it('Should execute _getDaysInMonth WHEN month==12', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.test_getDaysInMonth(139, 12,{from: accounts[0]});
  });
  it('Should execute _getDaysInMonth WHEN month!=2', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.test_getDaysInMonth(3999, 3999,{from: accounts[0]});
  });
  it('Should execute _getDaysInMonth WHEN month==2,month!=1,month!=3,month!=5,month!=7,month!=8,month!=10,month!=12', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.test_getDaysInMonth(50, 2,{from: accounts[0]});
  });
  it('Should execute getDayOfWeek', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testgetDayOfWeek(2446,{from: accounts[0]});
  });
  it('Should execute getYear', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testgetYear(1969,{from: accounts[0]});
  });
  it('Should execute getMonth', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testgetMonth(68570,{from: accounts[0]});
  });
  it('Should execute getDay', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testgetDay(4801,{from: accounts[0]});
  });
  it('Should execute getHour', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testgetHour(161,{from: accounts[0]});
  });
  it('Should execute getMinute', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testgetMinute(4799,{from: accounts[0]});
  });
  it('Should execute getSecond', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testgetSecond(7,{from: accounts[0]});
  });
  it('Should execute addYears', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testaddYears(2446, 361,{from: accounts[0]});
  });
  it('Should execute addMonths', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testaddMonths(63, 256,{from: accounts[0]});
  });
  it('Should execute addDays', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testaddDays(1461000, 365,{from: accounts[0]});
  });
  it('Should execute addHours', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testaddHours(81, 901,{from: accounts[0]});
  });
  it('Should execute addMinutes', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testaddMinutes(101, 6,{from: accounts[0]});
  });
  it('Should execute addSeconds', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testaddSeconds(31, 1,{from: accounts[0]});
  });
  it('Should execute subYears', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testsubYears(400, 41,{from: accounts[0]});
  });
  it('Should execute subMonths', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testsubMonths(11, 111,{from: accounts[0]});
  });
  it('Should execute subDays', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testsubDays(101, 86401,{from: accounts[0]});
  });
  it('Should execute subHours', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testsubHours(17, 1462,{from: accounts[0]});
  });
  it('Should execute subMinutes', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testsubMinutes(1971, 9,{from: accounts[0]});
  });
  it('Should execute subSeconds', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testsubSeconds(400, 399,{from: accounts[0]});
  });
  it('Should execute diffYears', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testdiffYears(48, 2440589,{from: accounts[0]});
  });
  it('Should fail diffYears when NOT comply with: fromTimestamp <= toTimestamp', async () => {
    let result = await truffleAssert.fails(contractProxy4TestBokkyPooBahsDateTimeLibrary.testdiffYears(2440590, 2440589,{from: accounts[0]}),'revert');
  });
  it('Should execute diffMonths', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testdiffMonths(8, 63,{from: accounts[0]});
  });
  it('Should fail diffMonths when NOT comply with: fromTimestamp <= toTimestamp', async () => {
    let result = await truffleAssert.fails(contractProxy4TestBokkyPooBahsDateTimeLibrary.testdiffMonths(64, 63,{from: accounts[0]}),'revert');
  });
  it('Should execute diffDays', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testdiffDays(4001, 1461002,{from: accounts[0]});
  });
  it('Should fail diffDays when NOT comply with: fromTimestamp <= toTimestamp', async () => {
    let result = await truffleAssert.fails(contractProxy4TestBokkyPooBahsDateTimeLibrary.testdiffDays(1461003, 1461002,{from: accounts[0]}),'revert');
  });
  it('Should execute diffHours', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testdiffHours(1460, 32074,{from: accounts[0]});
  });
  it('Should fail diffHours when NOT comply with: fromTimestamp <= toTimestamp', async () => {
    let result = await truffleAssert.fails(contractProxy4TestBokkyPooBahsDateTimeLibrary.testdiffHours(32075, 32074,{from: accounts[0]}),'revert');
  });
  it('Should execute diffMinutes', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testdiffMinutes(1461000, 1461001,{from: accounts[0]});
  });
  it('Should fail diffMinutes when NOT comply with: fromTimestamp <= toTimestamp', async () => {
    let result = await truffleAssert.fails(contractProxy4TestBokkyPooBahsDateTimeLibrary.testdiffMinutes(1461002, 1461001,{from: accounts[0]}),'revert');
  });
  it('Should execute diffSeconds', async () => {
    let result = await contractProxy4TestBokkyPooBahsDateTimeLibrary.testdiffSeconds(1971, 4800,{from: accounts[0]});
  });
  it('Should fail diffSeconds when NOT comply with: fromTimestamp <= toTimestamp', async () => {
    let result = await truffleAssert.fails(contractProxy4TestBokkyPooBahsDateTimeLibrary.testdiffSeconds(4801, 4800,{from: accounts[0]}),'revert');
  });
});
