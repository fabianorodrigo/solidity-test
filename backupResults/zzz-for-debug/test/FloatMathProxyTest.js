const truffleAssert = require('truffle-assertions');
const BusinessDayConvention = artifacts.require("BusinessDayConvention");
const ContractDefaultConvention = artifacts.require("ContractDefaultConvention");
const ContractRoleConvention = artifacts.require("ContractRoleConvention");
const DayCountConvention = artifacts.require("DayCountConvention");
const EndOfMonthConvention = artifacts.require("EndOfMonthConvention");
const Core = artifacts.require("Core");
const Definitions = artifacts.require("Definitions");
const FloatMath = artifacts.require("FloatMath");
const Schedule = artifacts.require("Schedule");
const Utils = artifacts.require("Utils");
const IEngine = artifacts.require("IEngine");
const PAMEngine = artifacts.require("PAMEngine");
const ProxyFloatMath = artifacts.require("ProxyFloatMath");

contract("contractProxy4TestFloatMath",(accounts)=>{
    let contractProxy4TestFloatMath = null;
  let trace = false;
  let contractFloatMath = null;
  let contractBusinessDayConvention = null;
  let contractContractDefaultConvention = null;
  let contractContractRoleConvention = null;
  let contractDayCountConvention = null;
  let contractEndOfMonthConvention = null;
  let contractCore = null;
  let contractSchedule = null;
  let contractUtils = null;
  let contractPAMEngine = null;    ProxyFloatMath.link('FloatMath', contractFloatMath.address);
    contractProxy4TestFloatMath = await ProxyFloatMath.new({ from: accounts[0] });

  
  it('Should execute floatMult(int256,int256) WHEN a==0', async () => {
    let result = await contractProxy4TestFloatMath.testfloatMult(0, 120,{from: accounts[0]});
  });
  it('Should execute floatMult(int256,int256) WHEN b==0', async () => {
    let result = await contractProxy4TestFloatMath.testfloatMult(70, 0,{from: accounts[0]});
  });
  it('Should execute floatMult(int256,int256) WHEN a!=0,b!=0', async () => {
    let result = await contractProxy4TestFloatMath.testfloatMult(30, 29,{from: accounts[0]});
  });
  it('Should execute floatDiv(int256,int256) WHEN a==0', async () => {
    let result = await contractProxy4TestFloatMath.testfloatDiv(0, 900,{from: accounts[0]});
  });
  it('Should fail floatDiv(int256,int256) when NOT comply with: b != 0', async () => {
    let result = await truffleAssert.fails(contractProxy4TestFloatMath.testfloatDiv(0, 0,{from: accounts[0]}),'revert');
  });
  it('Should execute floatDiv(int256,int256) WHEN a!=0', async () => {
    let result = await contractProxy4TestFloatMath.testfloatDiv(99, 99,{from: accounts[0]});
  });
  it('Should fail floatDiv(int256,int256) when NOT comply with: b != 0', async () => {
    let result = await truffleAssert.fails(contractProxy4TestFloatMath.testfloatDiv(99, 0,{from: accounts[0]}),'revert');
  });
});
