pragma solidity 0.4.21;


contract SimpleCoin {event __CoverageSimpleCoin(string fileName, uint256 lineNumber);
event __FunctionCoverageSimpleCoin(string fileName, uint256 fnId);
event __StatementCoverageSimpleCoin(string fileName, uint256 statementId);
event __BranchCoverageSimpleCoin(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageSimpleCoin(string fileName, uint256 branchId);
event __AssertPostCoverageSimpleCoin(string fileName, uint256 branchId);


    mapping(address => uint256) private balances;
    mapping (address => mapping (address => uint256)) private allowed;
    string public name;
    string public symbol;
    uint public totalSupply;
    address public owner;
    address public newOwner;

    event LogTransfer(address indexed _from, address indexed _to, uint256 _value);
    event LogApproval(address indexed _owner, address indexed _spender, uint256 _value);
    event LogOwnershipTransferred(address indexed _from, address indexed _to);
    event LogOwnershipDeclined(address indexed _to);

    function SimpleCoin() public {emit __FunctionCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',1);

emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',20);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',1);
name = "Simple Coin";
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',21);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',2);
symbol = "XSC";
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',22);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',3);
totalSupply = 1000000;
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',23);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',4);
owner = msg.sender;
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',24);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',5);
balances[owner] = totalSupply;
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',25);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',6);
emit LogTransfer(address(0), owner, totalSupply);
    }

    // ensure the sender is the owner
    modifier onlyOwner() {emit __FunctionCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',2);

emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',30);
        emit __AssertPreCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',1);
emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',7);
require(msg.sender == owner);emit __AssertPostCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',1);

emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',31);
        _;
    }

    // getter for balance of specified owner
    function balanceOf(address _owner) public      returns (uint256 balance) {emit __FunctionCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',3);

emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',36);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',8);
return balances[_owner];
    }

    // getter for allowance of specified spender from specified owner
    function allowance(address _owner, address _spender) public      returns (uint256 remaining) {emit __FunctionCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',4);

emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',41);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',9);
return allowed[_owner][_spender];
    }

    // getter for total supply, deduct any zero-account balance
    function totalSupply() public      returns (uint) {emit __FunctionCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',5);

emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',46);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',10);
return totalSupply - balances[address(0)];
    }

    // transfer value from sender to specified address
    function transfer(address _to, uint256 _value) public returns (bool success) {emit __FunctionCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',6);

        // ensure the sender has enough coins
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',52);
        emit __AssertPreCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',2);
emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',11);
require(balances[msg.sender] >= _value);emit __AssertPostCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',2);

        // reduce senders balance by value
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',54);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',12);
balances[msg.sender] -= _value;
        // increase receivers balance by value
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',56);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',13);
balances[_to] += _value;
        // log transfer of value
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',58);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',14);
emit LogTransfer(msg.sender, _to, _value);
        // success
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',60);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',15);
return true;
    }

    // transfer value from sender to specified address as a spender with given allowance
    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success) {emit __FunctionCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',7);

        // get amount spender is allowed to spend
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',66);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',16);
uint256 remaining = allowed[_from][msg.sender];
        // ensure from account has enough coins and amount allowed to spend is more than value
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',68);
        emit __AssertPreCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',3);
emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',17);
require(balances[_from] >= _value && remaining >= _value);emit __AssertPostCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',3);

        // increase receivers balance by value
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',70);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',18);
balances[_to] += _value;
        // decrease from accounts balance by value
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',72);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',19);
balances[_from] -= _value;
        // decrease allowance by value
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',74);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',20);
allowed[_from][msg.sender] -= _value;
        // log transfer of value
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',76);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',21);
emit LogTransfer(_from, _to, _value);
        // success
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',78);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',22);
return true;
    }

    // approve specified address to spend the specified value on behalf of sender
    function approve(address _spender, uint256 _value) public returns (bool success) {emit __FunctionCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',8);

        // set allowance value
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',84);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',23);
allowed[msg.sender][_spender] = _value;
        // log approval
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',86);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',24);
emit LogApproval(msg.sender, _spender, _value);
        // success
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',88);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',25);
return true;
    }

    // essentially send request to potential new owner for them to accept
    function transferOwnership(address _newOwner) public onlyOwner {emit __FunctionCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',9);

        // set potential new owner
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',94);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',26);
newOwner = _newOwner;
    }

    // as the new potential owner, accept ownership
    function acceptOwnership() public returns (bool success) {emit __FunctionCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',10);

        // ensure sender is potential new owner
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',100);
        emit __AssertPreCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',4);
emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',27);
require(msg.sender == newOwner);emit __AssertPostCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',4);

        // change owner to new owner
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',102);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',28);
owner = newOwner;
        // set next potential new owner to zero-account
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',104);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',29);
newOwner = address(0);
        // log ownership transfer
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',106);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',30);
emit LogOwnershipTransferred(owner, newOwner);
        // success
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',108);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',31);
return true;
    }

    // as the new potential owner, decline ownership
    function declineOwnership() public returns (bool success) {emit __FunctionCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',11);

        // ensure sender is potential new owner
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',114);
        emit __AssertPreCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',5);
emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',32);
require(msg.sender == newOwner);emit __AssertPostCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',5);

        // set potential new owner to zero-account
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',116);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',33);
newOwner = address(0);
        // log ownership decline
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',118);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',34);
emit LogOwnershipDeclined(newOwner);
        // success
emit __CoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',120);
        emit __StatementCoverageSimpleCoin('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SimpleCoin/contracts/SimpleCoin.sol',35);
return true;
    }

}
