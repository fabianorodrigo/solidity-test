const truffleAssert = require('truffle-assertions');
const PaymentTokenMock = artifacts.require("PaymentTokenMock");
const RedeemableTokenMock = artifacts.require("RedeemableTokenMock");
const OffChainPayments = artifacts.require("OffChainPayments");
const OnChainPayments = artifacts.require("OnChainPayments");
const FixedPriceTender = artifacts.require("FixedPriceTender");
const FullRedemption = artifacts.require("FullRedemption");
const PartialRedemption = artifacts.require("PartialRedemption");
const RedeemableToken = artifacts.require("RedeemableToken");
const OnChainVoting = artifacts.require("OnChainVoting");

contract("RedeemableTokenMock",(accounts)=>{
  let trace = false;
  let contractRedeemableToken = null;
  let contractPaymentTokenMock = null;
  let contractRedeemableTokenMock = null;
  let contractOffChainPayments = null;
  let contractOnChainPayments = null;
  let contractFixedPriceTender = null;
  let contractFullRedemption = null;
  let contractPartialRedemption = null;
  let contractOnChainVoting = null;
  beforeEach(async () => {
    contractRedeemableToken = await RedeemableToken.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: RedeemableToken.new({from: accounts[0]}');
    contractPaymentTokenMock = await PaymentTokenMock.new(accounts[4], 2,{from: accounts[0]});
    if(trace) console.log('SUCESSO: PaymentTokenMock.new(accounts[4], 2,{from: accounts[0]}');
    contractRedeemableTokenMock = await RedeemableTokenMock.new(accounts[0], 4,{from: accounts[0]});
    if(trace) console.log('SUCESSO: RedeemableTokenMock.new(accounts[0], 4,{from: accounts[0]}');
    contractOffChainPayments = await OffChainPayments.new(accounts[2],{from: accounts[0]});
    if(trace) console.log('SUCESSO: OffChainPayments.new(accounts[2],{from: accounts[0]}');
    contractOnChainPayments = await OnChainPayments.new(contractRedeemableToken.address, contractPaymentTokenMock.address, accounts[0],{from: accounts[0]});
    if(trace) console.log('SUCESSO: OnChainPayments.new(contractRedeemableToken.address, contractPaymentTokenMock.address, accounts[0],{from: accounts[0]}');
    contractFixedPriceTender = await FixedPriceTender.new(4, contractOnChainPayments.address, contractRedeemableTokenMock.address, accounts[1], 3, 4,{from: accounts[0]});
    if(trace) console.log('SUCESSO: FixedPriceTender.new(4, contractOnChainPayments.address, contractRedeemableTokenMock.address, accounts[1], 3, 4,{from: accounts[0]}');
    contractFullRedemption = await FullRedemption.new(contractRedeemableToken.address, contractRedeemableTokenMock.address, accounts[1], 4,{from: accounts[0]});
    if(trace) console.log('SUCESSO: FullRedemption.new(contractRedeemableToken.address, contractRedeemableTokenMock.address, accounts[1], 4,{from: accounts[0]}');
    contractPartialRedemption = await PartialRedemption.new(contractFixedPriceTender.address, contractRedeemableToken.address, accounts[8], 2,{from: accounts[0]});
    if(trace) console.log('SUCESSO: PartialRedemption.new(contractFixedPriceTender.address, contractRedeemableToken.address, accounts[8], 2,{from: accounts[0]}');
    contractOnChainVoting = await OnChainVoting.new(contractFixedPriceTender.address, accounts[3], (await web3.eth.getBlock(await web3.eth.getBlockNumber())).timestamp+565,{from: accounts[0]});
    if(trace) console.log('SUCESSO: OnChainVoting.new(contractFixedPriceTender.address, accounts[3], (await web3.eth.getBlock(await web3.eth.getBlockNumber())).timestamp+565,{from: accounts[0]}');
  });
  
  it('Should execute setRedemption - accounts[0]', async () => {
    let result = await contractRedeemableTokenMock.methods["setRedemption(address)"](accounts[2],{from: accounts[0]});
  });
  it('Should execute setRedemption - accounts[1]', async () => {
    let result = await contractRedeemableTokenMock.methods["setRedemption(address)"](accounts[2],{from: accounts[1]});
  });
  it('Should fail setRedemption when NOT comply with: _redemption != 0x0000000000000000000000000000000000000000', async () => {
    let result = await truffleAssert.fails(contractRedeemableTokenMock.methods["setRedemption(address)"]("0x0000000000000000000000000000000000000000",{from: accounts[0]}),'revert');
  });
  it('Should execute redeemAllTokens', async () => {
    let result = await contractRedeemableTokenMock.methods["redeemAllTokens(address)"](accounts[2],{from: accounts[0]});
  });
  it('Should execute redeemPartialTokens - accounts[0]', async () => {
    let result = await contractRedeemableTokenMock.methods["redeemPartialTokens(address,uint256)"](accounts[0], 0,{from: accounts[0]});
  });
  it('Should execute redeemPartialTokens - accounts[7]', async () => {
    let result = await contractRedeemableTokenMock.methods["redeemPartialTokens(address,uint256)"](accounts[0], 0,{from: accounts[7]});
  });
});
