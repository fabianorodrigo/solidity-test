pragma solidity ^0.5.0;
import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/MathUtils.sol";
import "../contracts/mocks/RelevantTokenMock.sol";
import "../contracts/Power.sol";
import "../contracts/RelevantToken.sol";

contract TestMathUtils  {
uint nonce = 25;
function stringToBytes32(string memory source) public returns (bytes32 result) {
    bytes memory tempEmptyStringTest = bytes(source);
    if (tempEmptyStringTest.length == 0) {
        return 0x0;
    }

    assembly {
        result := mload(add(source, 32))
    }
}
  using MathUtils for uint256;

  
  //Should execute validPerc
  function test_validPerc() public {
    uint256   varLib = 61;

varLib.validPerc();
  }
  //Should execute percOf
  function test_percOf() public {
    uint256   varLib = 86;

varLib.percOf(54, 66);
  }
  //Should execute percOf
  function test_percOf1() public {
    uint256   varLib = 37;

varLib.percOf(93);
  }
  //Should execute percPoints
  function test_percPoints() public {
    uint256   varLib = 118;

varLib.percPoints(116);
  }
}
