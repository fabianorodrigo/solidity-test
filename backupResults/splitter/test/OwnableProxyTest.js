const truffleAssert = require('truffle-assertions');
const Ownable = artifacts.require("Ownable");
const SafeMath = artifacts.require("SafeMath");
const Splitter = artifacts.require("Splitter");
const Stoppable = artifacts.require("Stoppable");
const ProxyOwnable = artifacts.require("ProxyOwnable");

contract("contractProxy4TestOwnable",(accounts)=>{
    let contractProxy4TestOwnable = null;
  let trace = false;
  let contractSafeMath = null;
  let contractOwnable = null;
  let contractSplitter = null;
  let contractStoppable = null;
  beforeEach(async () => {
    contractSafeMath = await SafeMath.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: SafeMath.new({from: accounts[0]}');
    Splitter.link("SafeMath",contractSafeMath.address);
    contractSplitter = await Splitter.new({from:accounts[0]});
    if(trace) console.log('SUCESSO: Splitter.new({from:accounts[0]}');
    contractStoppable = await Stoppable.new({from:accounts[0]});
    if(trace) console.log('SUCESSO: Stoppable.new({from:accounts[0]}');
      contractProxy4TestOwnable = await ProxyOwnable.new({ from: accounts[0] });
});
  
  it('Should execute owner()', async () => {
    let result = await contractProxy4TestOwnable.testowner({from: accounts[0]});
  });
  it('Should execute isOwner()', async () => {
    let result = await contractProxy4TestOwnable.testisOwner({from: accounts[0]});
  });
  it('Should execute renounceOwnership() WHEN sender==_owner', async () => {
    let localcontractProxy4TestOwnable = await ProxyOwnable.new({from:accounts[0]});
    let result = await localcontractProxy4TestOwnable.testrenounceOwnership({from: accounts[0]});
  });
  it('Should fail renounceOwnership() when NOT comply with: sender == _owner', async () => {
    let localcontractProxy4TestOwnable = await ProxyOwnable.new({from:accounts[0]});
    let result = await truffleAssert.fails(localcontractProxy4TestOwnable.testrenounceOwnership({from: accounts[9]}),'revert');
  });
  it('Should execute transferOwnership(address) WHEN sender==_owner,newOwner!=0x0000000000000000000000000000000000000000', async () => {
    let localcontractProxy4TestOwnable = await ProxyOwnable.new({from:accounts[0]});
    let result = await localcontractProxy4TestOwnable.testtransferOwnership(accounts[0],{from: accounts[0]});
  });
  it('Should fail transferOwnership(address) when NOT comply with: sender == _owner', async () => {
    let localcontractProxy4TestOwnable = await ProxyOwnable.new({from:accounts[0]});
    let result = await truffleAssert.fails(localcontractProxy4TestOwnable.testtransferOwnership(accounts[0],{from: accounts[9]}),'revert');
  });
  it('Should fail transferOwnership(address) when NOT comply with: newOwner != 0x0000000000000000000000000000000000000000', async () => {
    let localcontractProxy4TestOwnable = await ProxyOwnable.new({from:accounts[0]});
    let result = await truffleAssert.fails(localcontractProxy4TestOwnable.testtransferOwnership("0x0000000000000000000000000000000000000000",{from: accounts[0]}),'revert');
  });
  it('Should execute _transferOwnership(address) WHEN newOwner!=0x0000000000000000000000000000000000000000', async () => {
    let result = await contractProxy4TestOwnable.test_transferOwnership(accounts[0],{from: accounts[0]});
  });
  it('Should fail _transferOwnership(address) when NOT comply with: newOwner != 0x0000000000000000000000000000000000000000', async () => {
    let result = await truffleAssert.fails(contractProxy4TestOwnable.test_transferOwnership("0x0000000000000000000000000000000000000000",{from: accounts[0]}),'revert');
  });
});
