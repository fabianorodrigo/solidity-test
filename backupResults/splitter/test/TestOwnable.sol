pragma solidity ^0.5.1;
import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Ownable.sol";
import "../contracts/SafeMath.sol";
import "../contracts/Splitter.sol";
import "../contracts/Stoppable.sol";

contract TestOwnable  is Ownable {
uint nonce = 15;
function stringToBytes32(string memory source) public returns (bytes32 result) {
    bytes memory tempEmptyStringTest = bytes(source);
    if (tempEmptyStringTest.length == 0) {
        return 0x0;
    }

    assembly {
        result := mload(add(source, 32))
    }
}
  
  //Should execute owner
  function test_owner() public {
    address   result0;
    result0 = this.owner();
  }
  //Should execute isOwner
  function test_isOwner() public {
    bool   result0;
    result0 = this.isOwner();
  }
  //Should execute renounceOwnership
  function test_renounceOwnership() public {
    this.renounceOwnership();
  }
  //Should execute transferOwnership
  function test_transferOwnership0() public {
    address   param0 = address(uint160(uint(keccak256(abi.encodePacked(nonce, blockhash(block.number))))));
    this.transferOwnership(param0);
  }
  //Should fail transferOwnership when NOT comply with: newOwner != 0x0000000000000000000000000000000000000000
  function test_transferOwnership1() public {
    address   param0 = 0x0000000000000000000000000000000000000000;
    this.transferOwnership(param0);
  }
}
