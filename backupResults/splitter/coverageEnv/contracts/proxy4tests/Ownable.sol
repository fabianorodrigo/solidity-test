import "../Ownable.sol";

contract ProxyOwnable  is Ownable {event __CoverageProxyOwnable(string fileName, uint256 lineNumber);
event __FunctionCoverageProxyOwnable(string fileName, uint256 fnId);
event __StatementCoverageProxyOwnable(string fileName, uint256 statementId);
event __BranchCoverageProxyOwnable(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageProxyOwnable(string fileName, uint256 branchId);
event __AssertPostCoverageProxyOwnable(string fileName, uint256 branchId);


       function testowner() public      returns (address ){emit __FunctionCoverageProxyOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/Ownable.sol',1);

emit __CoverageProxyOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/Ownable.sol',6);
    emit __StatementCoverageProxyOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/Ownable.sol',1);
return owner();
   }

   function testisOwner() public      returns (bool ){emit __FunctionCoverageProxyOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/Ownable.sol',2);

emit __CoverageProxyOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/Ownable.sol',10);
    emit __StatementCoverageProxyOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/Ownable.sol',2);
return isOwner();
   }

   function testrenounceOwnership() public  {emit __FunctionCoverageProxyOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/Ownable.sol',3);

emit __CoverageProxyOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/Ownable.sol',14);
    emit __StatementCoverageProxyOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/Ownable.sol',3);
renounceOwnership();
   }

   function testtransferOwnership(address  newOwner) public  {emit __FunctionCoverageProxyOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/Ownable.sol',4);

emit __CoverageProxyOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/Ownable.sol',18);
    emit __StatementCoverageProxyOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/Ownable.sol',4);
transferOwnership(newOwner);
   }

   function test_transferOwnership(address  newOwner) public  {emit __FunctionCoverageProxyOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/Ownable.sol',5);

emit __CoverageProxyOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/Ownable.sol',22);
    emit __StatementCoverageProxyOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/Ownable.sol',5);
_transferOwnership(newOwner);
   }


}