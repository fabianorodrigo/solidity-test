import "../SafeMath.sol";

contract ProxySafeMath  {event __CoverageProxySafeMath(string fileName, uint256 lineNumber);
event __FunctionCoverageProxySafeMath(string fileName, uint256 fnId);
event __StatementCoverageProxySafeMath(string fileName, uint256 statementId);
event __BranchCoverageProxySafeMath(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageProxySafeMath(string fileName, uint256 branchId);
event __AssertPostCoverageProxySafeMath(string fileName, uint256 branchId);


      using SafeMath for uint256;

   function testmul(uint256  a, uint256  b) public      returns (uint256 ){emit __FunctionCoverageProxySafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/SafeMath.sol',1);

emit __CoverageProxySafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/SafeMath.sol',8);
    emit __StatementCoverageProxySafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/SafeMath.sol',1);
return a.mul(b);
   }

   function testdiv(uint256  a, uint256  b) public      returns (uint256 ){emit __FunctionCoverageProxySafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/SafeMath.sol',2);

emit __CoverageProxySafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/SafeMath.sol',12);
    emit __StatementCoverageProxySafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/SafeMath.sol',2);
return a.div(b);
   }

   function testsub(uint256  a, uint256  b) public      returns (uint256 ){emit __FunctionCoverageProxySafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/SafeMath.sol',3);

emit __CoverageProxySafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/SafeMath.sol',16);
    emit __StatementCoverageProxySafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/SafeMath.sol',3);
return a.sub(b);
   }

   function testadd(uint256  a, uint256  b) public      returns (uint256 ){emit __FunctionCoverageProxySafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/SafeMath.sol',4);

emit __CoverageProxySafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/SafeMath.sol',20);
    emit __StatementCoverageProxySafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/SafeMath.sol',4);
return a.add(b);
   }

   function testmod(uint256  a, uint256  b) public      returns (uint256 ){emit __FunctionCoverageProxySafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/SafeMath.sol',5);

emit __CoverageProxySafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/SafeMath.sol',24);
    emit __StatementCoverageProxySafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/proxy4tests/SafeMath.sol',5);
return a.mod(b);
   }


}