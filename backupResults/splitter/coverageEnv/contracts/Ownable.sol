pragma solidity >=0.4.21 <0.6.0;

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be aplied to your functions to restrict their use to
 * the owner.
 */
contract Ownable {event __CoverageOwnable(string fileName, uint256 lineNumber);
event __FunctionCoverageOwnable(string fileName, uint256 fnId);
event __StatementCoverageOwnable(string fileName, uint256 statementId);
event __BranchCoverageOwnable(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageOwnable(string fileName, uint256 branchId);
event __AssertPostCoverageOwnable(string fileName, uint256 branchId);

    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor () internal {emit __FunctionCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',1);

emit __CoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',21);
        emit __StatementCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',1);
_owner = msg.sender;
emit __CoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',22);
        emit __StatementCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',2);
emit OwnershipTransferred(address(0), _owner);
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public      returns (address) {emit __FunctionCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',2);

emit __CoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',29);
        emit __StatementCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',3);
return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {emit __FunctionCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',3);

emit __CoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',36);
        emit __AssertPreCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',1);
emit __StatementCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',4);
require(isOwner(), "Ownable: caller is not the owner");emit __AssertPostCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',1);

emit __CoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',37);
        _;
    }

    /**
     * @dev Returns true if the caller is the current owner.
     */
    function isOwner() public      returns (bool) {emit __FunctionCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',4);

emit __CoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',44);
        emit __StatementCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',5);
return msg.sender == _owner;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * > Note: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public onlyOwner {emit __FunctionCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',5);

emit __CoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',55);
        emit __StatementCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',6);
emit OwnershipTransferred(_owner, address(0));
emit __CoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',56);
        emit __StatementCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',7);
_owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public onlyOwner {emit __FunctionCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',6);

emit __CoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',64);
        emit __StatementCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',8);
_transferOwnership(newOwner);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     */
    function _transferOwnership(address newOwner) internal {emit __FunctionCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',7);

emit __CoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',71);
        emit __AssertPreCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',2);
emit __StatementCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',9);
require(newOwner != address(0), "Ownable: new owner is the zero address");emit __AssertPostCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',2);

emit __CoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',72);
        emit __StatementCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',10);
emit OwnershipTransferred(_owner, newOwner);
emit __CoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',73);
        emit __StatementCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Ownable.sol',11);
_owner = newOwner;
    }
}