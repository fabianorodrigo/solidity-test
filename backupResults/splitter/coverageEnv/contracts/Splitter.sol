pragma solidity >=0.4.21 <0.6.0;

import './SafeMath.sol';
import './Stoppable.sol';

/// @title Splitter - Split payments to two parties 50/50
/// @author hilmarx
/// @notice You can use this contract to split up payments equally among two pre-defined parties
/// @dev This is a test version, please don't use in production

contract Splitter is Stoppable  {event __CoverageSplitter(string fileName, uint256 lineNumber);
event __FunctionCoverageSplitter(string fileName, uint256 fnId);
event __StatementCoverageSplitter(string fileName, uint256 statementId);
event __BranchCoverageSplitter(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageSplitter(string fileName, uint256 branchId);
event __AssertPostCoverageSplitter(string fileName, uint256 branchId);


    using SafeMath for uint256;

    // State variables

    // balance of alice, bob & carol
    mapping (address => uint) public balanceOf;

    // mapping to find respective remainders for splitting pairs with their hashes
    mapping (bytes32 => uint) public remainderPair; 

    // Events
    // Show that the remainder was distributed to the two accounts
    event LogRemainderClaimed(uint indexed remainder, bool indexed claimable);

    // Show that alice sent some Ether to the split function
    event LogSplit(uint indexed amount);

    // Show that either bob or carol successfully withdrew their balance
    event LogBalanceWithdrawn(address indexed withdrawer, uint indexed amount);

    // Modifiers
    
    ///@dev Check if message sender (Bob or Carol) have any balance which can be withdrawn
    modifier sufficientBalance() {emit __FunctionCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',1);

emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',37);
        emit __AssertPreCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',1);
emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',1);
require(balanceOf[msg.sender] > 0, "Your balance is 0");emit __AssertPostCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',1);

emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',38);
        _;
    }
    
    ///@dev Check that Alice is not sending a message with value 0 to a payable method
    modifier nonZero() {emit __FunctionCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',2);

emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',43);
        emit __AssertPreCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',2);
emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',2);
require(msg.value > 0, "You cannot send a transaction with value equal to 0");emit __AssertPostCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',2);

emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',44);
        _;
    }
    
    //@dev Constructor setting addresses & balances of Alice, Bob & Carol, where alice is the owner
    constructor() public {emit __FunctionCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',3);

        // alice = owner();
    }

    // Setter Functions

    ///@dev Split ether sent by Alice into two equal pieces and add them to receiver1s & receiver2s balance, equally.
    ///@dev If ether amount cannot be divided by two, store remainder in the 'remainder' state variable which is mapped to each unqiue splitting pair
    ///@dev If the ether stored in the remainder state variable is again divisible by 2, emit an event
    function splitEther(address receiver1, address receiver2) 
        public
        onlyOwner
        nonZero
        onlyIfRunning
        payable
    {emit __FunctionCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',4);
       
        // Require both address to not be 0;
emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',65);
        emit __AssertPreCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',3);
emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',3);
require( receiver1 != address(0) && receiver2 != address(0) );emit __AssertPostCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',3);


        // Store balance uint of carol in memory
emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',68);
        emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',4);
uint receiver1NewBalance = balanceOf[receiver1];

        // Store balance uint of bob in memory
emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',71);
        emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',5);
uint receiver2NewBalance = balanceOf[receiver2];

        // Divide Ether sent by Alice into two equal payouts
emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',74);
        emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',6);
uint payout = msg.value.div(2);

        // Check if remainer exists, if yes update remainder
emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',77);
        emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',7);
if (msg.value > payout.mul(2)) {emit __BranchCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',4,0);

            // Find pairHash of splitting Pair
emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',80);
            emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',8);
bytes32 pairHash = createPairHash(receiver1, receiver2);

            // Find remainder of splitting pair
emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',83);
            emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',9);
uint newRemainder = remainderPair[pairHash];

            // Update remainder
emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',86);
            emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',10);
newRemainder = newRemainder.add(msg.value - payout.mul(2));
            
            // If remainder is greater than 0 & divisible by two, trigger event and update carols and bobs balanceOf
emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',89);
            emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',11);
if (newRemainder > 0 && newRemainder.mod(2) == 0) 
            {emit __BranchCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',5,0);
                // Split existing remainder in two
emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',92);
                emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',12);
uint evenPayout = newRemainder.div(2);

                // update carols and bobs balance
emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',95);
                emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',13);
receiver1NewBalance = receiver1NewBalance.add(evenPayout);
emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',96);
                emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',14);
receiver2NewBalance = receiver2NewBalance.add(evenPayout);

emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',98);
                emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',15);
emit LogRemainderClaimed(newRemainder, true);

                // Set remainder to 0;
emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',101);
                emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',16);
newRemainder = 0;
            }else { emit __BranchCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',5,1);}

            // SSTORE new remainder
emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',104);
            emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',17);
remainderPair[pairHash] = newRemainder;
        }else { emit __BranchCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',4,1);}

        // SSTORE updated balance of Carol
emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',107);
        emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',18);
balanceOf[receiver1] = receiver1NewBalance.add(payout);
        // SSTORE updated balance of bob
emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',109);
        emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',19);
balanceOf[receiver2] = receiver2NewBalance.add(payout);

        // Emit event that ether was succesfully splitted amoung bob & carol
emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',112);
        emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',20);
emit LogSplit(msg.value);
    }
    
    ///@dev Enable Bob & Carol to withdraw the value of their contracts balance
    function withdraw() 
        public
        sufficientBalance
        onlyIfRunning
        returns (bool success)
    {emit __FunctionCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',5);

emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',122);
        emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',21);
uint withdrawAmount = balanceOf[msg.sender];
emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',123);
        emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',22);
balanceOf[msg.sender] = 0;
        // emit event that either carol or bob successfully withdrew their balance
emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',125);
        emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',23);
emit LogBalanceWithdrawn(msg.sender, withdrawAmount);
emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',126);
        emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',24);
msg.sender.transfer(withdrawAmount);
emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',127);
        emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',25);
return true;
    }

    function createPairHash(address receiver1, address receiver2) 
        internal 
             
        returns (bytes32 resultedPairHash) 
    {emit __FunctionCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',6);

emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',135);
        emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',26);
bytes32 pairHash;
emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',136);
        emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',27);
if (receiver1 > receiver2) {emit __BranchCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',6,0);
emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',137);
            emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',28);
pairHash = keccak256(abi.encodePacked(receiver1, receiver2));
        } else {emit __BranchCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',6,1);
emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',139);
            emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',29);
pairHash = keccak256(abi.encodePacked(receiver2, receiver1));
        }
emit __CoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',141);
        emit __StatementCoverageSplitter('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Splitter.sol',30);
return pairHash;
    }
}