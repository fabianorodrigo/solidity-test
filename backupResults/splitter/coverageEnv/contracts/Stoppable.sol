pragma solidity >=0.4.21 <0.6.0;

import './Ownable.sol';

contract Stoppable is Ownable {event __CoverageStoppable(string fileName, uint256 lineNumber);
event __FunctionCoverageStoppable(string fileName, uint256 fnId);
event __StatementCoverageStoppable(string fileName, uint256 statementId);
event __BranchCoverageStoppable(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageStoppable(string fileName, uint256 branchId);
event __AssertPostCoverageStoppable(string fileName, uint256 branchId);


    bool public running;

    event LogRunSwitch(address sender, bool switchSetting);

    modifier onlyIfRunning {emit __FunctionCoverageStoppable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Stoppable.sol',1);

emit __CoverageStoppable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Stoppable.sol',12);
        emit __AssertPreCoverageStoppable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Stoppable.sol',1);
emit __StatementCoverageStoppable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Stoppable.sol',1);
require(running);emit __AssertPostCoverageStoppable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Stoppable.sol',1);

emit __CoverageStoppable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Stoppable.sol',13);
        _;
    }

    constructor()
    public
    {emit __FunctionCoverageStoppable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Stoppable.sol',2);

emit __CoverageStoppable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Stoppable.sol',19);
        emit __StatementCoverageStoppable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Stoppable.sol',2);
running = true;
    }

    function runSwitch(bool onOff)
    public
    onlyOwner
    returns(bool success)
    {emit __FunctionCoverageStoppable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Stoppable.sol',3);

emit __CoverageStoppable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Stoppable.sol',27);
        emit __StatementCoverageStoppable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Stoppable.sol',3);
running = onOff;
emit __CoverageStoppable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Stoppable.sol',28);
        emit __StatementCoverageStoppable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Stoppable.sol',4);
emit LogRunSwitch(msg.sender, onOff);
emit __CoverageStoppable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Stoppable.sol',29);
        emit __StatementCoverageStoppable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/splitter/contracts/Stoppable.sol',5);
return true;
    }

}