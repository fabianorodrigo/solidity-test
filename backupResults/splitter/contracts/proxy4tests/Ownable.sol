import "../Ownable.sol";

contract ProxyOwnable  is Ownable {

       function testowner() public view returns (address ){
    return owner();
   }

   function testisOwner() public view returns (bool ){
    return isOwner();
   }

   function testrenounceOwnership() public  {
    renounceOwnership();
   }

   function testtransferOwnership(address  newOwner) public  {
    transferOwnership(newOwner);
   }

   function test_transferOwnership(address  newOwner) public  {
    _transferOwnership(newOwner);
   }


}