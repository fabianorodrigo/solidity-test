

pragma solidity ^0.5.4;

import "./SimpleToken.sol";

contract SimpleCrowdfund is SimpleToken {event __CoverageSimpleCrowdfund(string fileName, uint256 lineNumber);
event __FunctionCoverageSimpleCrowdfund(string fileName, uint256 fnId);
event __StatementCoverageSimpleCrowdfund(string fileName, uint256 statementId);
event __BranchCoverageSimpleCrowdfund(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageSimpleCrowdfund(string fileName, uint256 branchId);
event __AssertPostCoverageSimpleCrowdfund(string fileName, uint256 branchId);


    // Token purchase event
    event TokenPurchase(address indexed _buyer, uint256 _value);

    // OwnerWallet address, all ETH gets transfered to him automatically
    address payable ownerWallet;
    // StartBlock is the block where the contract gets mined
    uint256 startBlock;

    // Constructor function
    constructor(uint256 _maxSupply, uint256 _toMint) public {emit __FunctionCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',1);

emit __CoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',19);
        emit __StatementCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',1);
maxSupply = _maxSupply;
emit __CoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',20);
        emit __StatementCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',2);
ownerWallet = msg.sender;
emit __CoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',21);
        emit __AssertPreCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',1);
emit __StatementCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',3);
require(mint(msg.sender, _toMint));emit __AssertPostCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',1);

emit __CoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',22);
        emit __StatementCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',4);
startBlock = block.number;
    }

    // Function that actually buys the tokens
    function buyTokens(address _to) public payable returns (bool) {emit __FunctionCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',2);

        // Crowdsfund ends if current block number is above 2000
emit __CoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',28);
        emit __AssertPreCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',2);
emit __StatementCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',5);
require(block.number < startBlock + 2000);emit __AssertPostCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',2);

        // Ensure the address passed is valid
emit __CoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',30);
        emit __AssertPreCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',3);
emit __StatementCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',6);
require(address(_to) != address(0));emit __AssertPostCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',3);

        // Get the amount of tokens
emit __CoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',32);
        emit __StatementCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',7);
uint256 amount = msg.value.mul(getRate());
        // Ensure the minting works
emit __CoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',34);
        emit __AssertPreCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',4);
emit __StatementCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',8);
require(mint(_to, amount));emit __AssertPostCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',4);

        // Transfer to the owner wallet the ETH sent
emit __CoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',36);
        emit __StatementCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',9);
ownerWallet.transfer(msg.value);
        // Emist an event
emit __CoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',38);
        emit __StatementCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',10);
emit TokenPurchase(_to, amount);
emit __CoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',39);
        emit __StatementCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',11);
return true;
    }

    // GetRate returns the rate of the tokens based on the current block
    function getRate() public      returns (uint256) {emit __FunctionCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',3);


emit __CoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',45);
        emit __StatementCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',12);
if (block.number > (startBlock + 1000)) {emit __BranchCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',5,0);
emit __CoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',46);
            emit __StatementCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',13);
return 5;
        } else {emit __StatementCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',14);
emit __BranchCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',5,1);if (block.number > (startBlock + 750)) {emit __BranchCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',6,0);
emit __CoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',48);
            emit __StatementCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',15);
return 6;
        } else {emit __StatementCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',16);
emit __BranchCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',6,1);if (block.number > (startBlock + 500)) {emit __BranchCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',7,0);
emit __CoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',50);
            emit __StatementCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',17);
return 7;
        } else {emit __BranchCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',7,1);
emit __CoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',52);
            emit __StatementCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',18);
return 8;
        }}}
    }

    // If user simply send ETH, call buy tokens with the message sender
    function() external payable {emit __FunctionCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',4);

emit __CoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',58);
        emit __StatementCoverageSimpleCrowdfund('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleCrowdfund.sol',19);
buyTokens(msg.sender);
    }

}

