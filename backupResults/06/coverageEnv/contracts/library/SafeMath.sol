pragma solidity ^0.5.4;

/**
 * @dev Wrappers over Solidity's arithmetic operations with added overflow
 * checks.
 *
 * Arithmetic operations in Solidity wrap on overflow. This can easily result
 * in bugs, because programmers usually assume that an overflow raises an
 * error, which is the standard behavior in high level programming languages.
 * `SafeMath` restores this intuition by reverting the transaction when an
 * operation overflows.
 *
 * Using this library instead of the unchecked operations eliminates an entire
 * class of bugs, so it's recommended to use it always.
 */
library SafeMath {event __CoverageSafeMath(string fileName, uint256 lineNumber);
event __FunctionCoverageSafeMath(string fileName, uint256 fnId);
event __StatementCoverageSafeMath(string fileName, uint256 statementId);
event __BranchCoverageSafeMath(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageSafeMath(string fileName, uint256 branchId);
event __AssertPostCoverageSafeMath(string fileName, uint256 branchId);

    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `+` operator.
     *
     * Requirements:
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal      returns (uint256) {emit __FunctionCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',1);

emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',27);
        emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',1);
uint256 c = a + b;
emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',28);
        emit __AssertPreCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',1);
emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',2);
require(c >= a, "SafeMath: addition overflow");emit __AssertPostCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',1);


emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',30);
        emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',3);
return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal      returns (uint256) {emit __FunctionCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',2);

emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',43);
        emit __AssertPreCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',2);
emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',4);
require(b <= a, "SafeMath: subtraction overflow");emit __AssertPostCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',2);

emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',44);
        emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',5);
uint256 c = a - b;

emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',46);
        emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',6);
return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `*` operator.
     *
     * Requirements:
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal      returns (uint256) {emit __FunctionCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',3);

        // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
        // benefit is lost if 'b' is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-solidity/pull/522
emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',62);
        emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',7);
if (a == 0) {emit __BranchCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',3,0);
emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',63);
            emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',8);
return 0;
        }else { emit __BranchCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',3,1);}


emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',66);
        emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',9);
uint256 c = a * b;
emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',67);
        emit __AssertPreCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',4);
emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',10);
require(c / a == b, "SafeMath: multiplication overflow");emit __AssertPostCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',4);


emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',69);
        emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',11);
return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal      returns (uint256) {emit __FunctionCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',4);

        // Solidity only automatically asserts when dividing by 0
emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',85);
        emit __AssertPreCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',5);
emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',12);
require(b > 0, "SafeMath: division by zero");emit __AssertPostCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',5);

emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',86);
        emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',13);
uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold

emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',89);
        emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',14);
return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal      returns (uint256) {emit __FunctionCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',5);

emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',104);
        emit __AssertPreCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',6);
emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',15);
require(b != 0, "SafeMath: modulo by zero");emit __AssertPostCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',6);

emit __CoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',105);
        emit __StatementCoverageSafeMath('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/SafeMath.sol',16);
return a % b;
    }
}