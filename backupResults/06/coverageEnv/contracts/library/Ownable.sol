
pragma solidity ^0.5.4;

/**
 * @title Ownable
 * @dev The Ownable contract has an owner address, and provides basic authorization control
 * functions, this simplifies the implementation of "user permissions".
 */
contract Ownable {event __CoverageOwnable(string fileName, uint256 lineNumber);
event __FunctionCoverageOwnable(string fileName, uint256 fnId);
event __StatementCoverageOwnable(string fileName, uint256 statementId);
event __BranchCoverageOwnable(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageOwnable(string fileName, uint256 branchId);
event __AssertPostCoverageOwnable(string fileName, uint256 branchId);

    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev The Ownable constructor sets the original `owner` of the contract to the sender
     * account.
     */
    constructor () internal {emit __FunctionCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',1);

emit __CoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',19);
        emit __StatementCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',1);
_owner = msg.sender;
emit __CoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',20);
        emit __StatementCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',2);
emit OwnershipTransferred(address(0), _owner);
    }

    /**
     * @return the address of the owner.
     */
    function owner() public      returns (address) {emit __FunctionCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',2);

emit __CoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',27);
        emit __StatementCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',3);
return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {emit __FunctionCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',3);

emit __CoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',34);
        emit __AssertPreCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',1);
emit __StatementCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',4);
require(isOwner());emit __AssertPostCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',1);

emit __CoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',35);
        _;
    }

    /**
     * @return true if `msg.sender` is the owner of the contract.
     */
    function isOwner() public      returns (bool) {emit __FunctionCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',4);

emit __CoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',42);
        emit __StatementCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',5);
return msg.sender == _owner;
    }

    /**
     * @dev Allows the current owner to relinquish control of the contract.
     * It will not be possible to call the functions with the `onlyOwner`
     * modifier anymore.
     * @notice Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public onlyOwner {emit __FunctionCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',5);

emit __CoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',53);
        emit __StatementCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',6);
emit OwnershipTransferred(_owner, address(0));
emit __CoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',54);
        emit __StatementCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',7);
_owner = address(0);
    }

    /**
     * @dev Allows the current owner to transfer control of the contract to a newOwner.
     * @param newOwner The address to transfer ownership to.
     */
    function transferOwnership(address newOwner) public onlyOwner {emit __FunctionCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',6);

emit __CoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',62);
        emit __StatementCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',8);
_transferOwnership(newOwner);
    }

    /**
     * @dev Transfers control of the contract to a newOwner.
     * @param newOwner The address to transfer ownership to.
     */
    function _transferOwnership(address newOwner) internal {emit __FunctionCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',7);

emit __CoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',70);
        emit __AssertPreCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',2);
emit __StatementCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',9);
require(newOwner != address(0));emit __AssertPostCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',2);

emit __CoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',71);
        emit __StatementCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',10);
emit OwnershipTransferred(_owner, newOwner);
emit __CoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',72);
        emit __StatementCoverageOwnable('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/library/Ownable.sol',11);
_owner = newOwner;
    }
}