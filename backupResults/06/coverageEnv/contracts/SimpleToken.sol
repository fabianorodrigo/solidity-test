

pragma solidity ^0.5.4;

import "./library/SafeMath.sol";
import "./library/Ownable.sol";

contract SimpleToken is Ownable {event __CoverageSimpleToken(string fileName, uint256 lineNumber);
event __FunctionCoverageSimpleToken(string fileName, uint256 fnId);
event __StatementCoverageSimpleToken(string fileName, uint256 statementId);
event __BranchCoverageSimpleToken(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageSimpleToken(string fileName, uint256 branchId);
event __AssertPostCoverageSimpleToken(string fileName, uint256 branchId);


    using SafeMath for uint256;

    /* ERC20 INTERFACE */
    string public name = "TOKEN NAME";
    string public symbol = "SYMBOL";
    uint8 public decimals = 18;
    uint256 public totalSupply = 0;

    mapping (address => mapping (address => uint256)) internal allowed;
    mapping(address => uint256) balances;

    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
    event Mint(address indexed to, uint256 amount);


    // Not implemented
    function transferFrom(address _from, address _to, uint256 _value) public returns (bool) {emit __FunctionCoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',1);
}
    function approve(address _spender, uint256 _value) public returns (bool) {emit __FunctionCoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',2);
}
    function allowance(address _owner, address _spender) public      returns (uint256) {emit __FunctionCoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',3);
}

    function balanceOf(address _owner) public      returns (uint256 balance) {emit __FunctionCoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',4);
 emit __StatementCoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',1);
return balances[_owner];}

    function transfer(address _to, uint256 _value) public returns (bool) {emit __FunctionCoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',5);

emit __CoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',34);
        emit __AssertPreCoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',1);
emit __StatementCoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',2);
require(_to != address(0));emit __AssertPostCoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',1);

emit __CoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',35);
        emit __AssertPreCoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',2);
emit __StatementCoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',3);
require(_value <= balances[msg.sender]);emit __AssertPostCoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',2);

emit __CoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',36);
        emit __StatementCoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',4);
balances[msg.sender] = balances[msg.sender].sub(_value);
emit __CoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',37);
        emit __StatementCoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',5);
balances[_to] = balances[_to].add(_value);
emit __CoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',38);
        emit __StatementCoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',6);
emit Transfer(msg.sender, _to, _value);
emit __CoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',39);
        emit __StatementCoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',7);
return true;
    }

    /* END OF ERC20 INTERFACE */

    // Default max supply is 1000
    uint256 maxSupply = 1000;

    // Minting function, adds tokens to our total supply
    function mint(address _to, uint256 _amount) onlyOwner public returns (bool) {emit __FunctionCoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',6);

        // Add to the total supply
emit __CoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',50);
        emit __StatementCoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',8);
totalSupply = totalSupply.add(_amount);
        // Ensure it is less than the max supply
emit __CoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',52);
        emit __AssertPreCoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',3);
emit __StatementCoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',9);
require(totalSupply < maxSupply);emit __AssertPostCoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',3);

        // Add to balance
emit __CoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',54);
        emit __StatementCoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',10);
balances[_to] = balances[_to].add(_amount);
        // Emit events
emit __CoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',56);
        emit __StatementCoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',11);
emit Mint(_to, _amount);
emit __CoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',57);
        emit __StatementCoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',12);
emit Transfer(address(0), _to, _amount);
emit __CoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',58);
        emit __StatementCoverageSimpleToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/06/contracts/SimpleToken.sol',13);
return true;
    }

}