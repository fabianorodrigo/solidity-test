const truffleAssert = require('truffle-assertions');
const SafeMath = artifacts.require("SafeMath");
const SimpleCrowdfund = artifacts.require("SimpleCrowdfund");
const SimpleToken = artifacts.require("SimpleToken");
const ProxySafeMath = artifacts.require("ProxySafeMath");

contract("SimpleCrowdfund",(accounts)=>{
  let trace = false;
  let contractSafeMath = null;
  let contractSimpleToken = null;
  let contractSimpleCrowdfund = null;
  beforeEach(async () => {
    contractSafeMath = await SafeMath.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: SafeMath.new({from: accounts[0]}');
    SimpleToken.link("SafeMath",contractSafeMath.address);
    contractSimpleToken = await SimpleToken.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: SimpleToken.new({from: accounts[0]}');
    contractSimpleCrowdfund = await SimpleCrowdfund.new(999,1,{from:accounts[0]});
    if(trace) console.log('SUCESSO: SimpleCrowdfund.new(999,1,{from:accounts[0]}');
  });
  
  it('Should execute buyTokens(address) WHEN (await web3.eth.getBlockNumber())<startBlock + 2000', async () => {
    //transactionParameter: [object Object]
    //SimpleToken.name: TOKEN NAME
    //SimpleToken.symbol: SYMBOL
    //SimpleToken.decimals: 18
    //SimpleToken.totalSupply: 0
    //SimpleToken.maxSupply: 1000
    //SimpleCrowdfund.startBlock: 0
    //SimpleCrowdfund.name: TOKEN NAME
    //SimpleCrowdfund.symbol: SYMBOL
    //SimpleCrowdfund.decimals: 18
    //SimpleCrowdfund.totalSupply: 0
    //SimpleCrowdfund.maxSupply: 999
    //SimpleCrowdfund.ownerWallet: accounts[0]
    //SimpleCrowdfund._owner: accounts[0]
    let result = await contractSimpleCrowdfund.buyTokens(accounts[7],{from: accounts[0]});
  });
  it('Should execute getRate() WHEN (await web3.eth.getBlockNumber())>stateVariable + NumberLiteral', async () => {
    //transactionParameter: [object Object]
    //SimpleToken.name: TOKEN NAME
    //SimpleToken.symbol: SYMBOL
    //SimpleToken.decimals: 18
    //SimpleToken.totalSupply: 0
    //SimpleToken.maxSupply: 1000
    //SimpleCrowdfund.startBlock: 0
    //SimpleCrowdfund.name: TOKEN NAME
    //SimpleCrowdfund.symbol: SYMBOL
    //SimpleCrowdfund.decimals: 18
    //SimpleCrowdfund.totalSupply: 0
    //SimpleCrowdfund.maxSupply: 999
    //SimpleCrowdfund.ownerWallet: accounts[0]
    //SimpleCrowdfund._owner: accounts[0]
    let result = await contractSimpleCrowdfund.getRate({from: accounts[0]});
  });
  it('Should execute getRate() WHEN (await web3.eth.getBlockNumber())>stateVariable + NumberLiteral', async () => {
    //transactionParameter: [object Object]
    //SimpleToken.name: TOKEN NAME
    //SimpleToken.symbol: SYMBOL
    //SimpleToken.decimals: 18
    //SimpleToken.totalSupply: 0
    //SimpleToken.maxSupply: 1000
    //SimpleCrowdfund.startBlock: 0
    //SimpleCrowdfund.name: TOKEN NAME
    //SimpleCrowdfund.symbol: SYMBOL
    //SimpleCrowdfund.decimals: 18
    //SimpleCrowdfund.totalSupply: 0
    //SimpleCrowdfund.maxSupply: 999
    //SimpleCrowdfund.ownerWallet: accounts[0]
    //SimpleCrowdfund._owner: accounts[0]
    let result = await contractSimpleCrowdfund.getRate({from: accounts[0]});
  });
  it('Should execute getRate() WHEN (await web3.eth.getBlockNumber())>stateVariable + NumberLiteral', async () => {
    //transactionParameter: [object Object]
    //SimpleToken.name: TOKEN NAME
    //SimpleToken.symbol: SYMBOL
    //SimpleToken.decimals: 18
    //SimpleToken.totalSupply: 0
    //SimpleToken.maxSupply: 1000
    //SimpleCrowdfund.startBlock: 0
    //SimpleCrowdfund.name: TOKEN NAME
    //SimpleCrowdfund.symbol: SYMBOL
    //SimpleCrowdfund.decimals: 18
    //SimpleCrowdfund.totalSupply: 0
    //SimpleCrowdfund.maxSupply: 999
    //SimpleCrowdfund.ownerWallet: accounts[0]
    //SimpleCrowdfund._owner: accounts[0]
    let result = await contractSimpleCrowdfund.getRate({from: accounts[0]});
  });
  it('Should execute getRate() WHEN (await web3.eth.getBlockNumber())<=stateVariable + NumberLiteral', async () => {
    //transactionParameter: [object Object]
    //SimpleToken.name: TOKEN NAME
    //SimpleToken.symbol: SYMBOL
    //SimpleToken.decimals: 18
    //SimpleToken.totalSupply: 0
    //SimpleToken.maxSupply: 1000
    //SimpleCrowdfund.startBlock: 0
    //SimpleCrowdfund.name: TOKEN NAME
    //SimpleCrowdfund.symbol: SYMBOL
    //SimpleCrowdfund.decimals: 18
    //SimpleCrowdfund.totalSupply: 0
    //SimpleCrowdfund.maxSupply: 999
    //SimpleCrowdfund.ownerWallet: accounts[0]
    //SimpleCrowdfund._owner: accounts[0]
    let result = await contractSimpleCrowdfund.getRate({from: accounts[0]});
  });
  it('Should execute fallback() WHEN (await web3.eth.getBlockNumber())<startBlock + 2000', async () => {
    //transactionParameter: [object Object]
    //SimpleToken.name: TOKEN NAME
    //SimpleToken.symbol: SYMBOL
    //SimpleToken.decimals: 18
    //SimpleToken.totalSupply: 0
    //SimpleToken.maxSupply: 1000
    //SimpleCrowdfund.startBlock: 0
    //SimpleCrowdfund.name: TOKEN NAME
    //SimpleCrowdfund.symbol: SYMBOL
    //SimpleCrowdfund.decimals: 18
    //SimpleCrowdfund.totalSupply: 0
    //SimpleCrowdfund.maxSupply: 999
    //SimpleCrowdfund.ownerWallet: accounts[0]
    //SimpleCrowdfund._owner: accounts[0]
    let result = await contractSimpleCrowdfund.sendTransaction({from: accounts[0]});
  });
});
