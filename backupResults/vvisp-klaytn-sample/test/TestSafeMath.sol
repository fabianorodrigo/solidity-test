pragma solidity ^0.4.24;
import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/IERC20.sol";
import "../contracts/MyERC20.sol";
import "../contracts/SafeMath.sol";

contract TestSafeMath  {
uint nonce = 82;
function stringToBytes32(string memory source) public returns (bytes32 result) {
    bytes memory tempEmptyStringTest = bytes(source);
    if (tempEmptyStringTest.length == 0) {
        return 0x0;
    }

    assembly {
        result := mload(add(source, 32))
    }
}
  using SafeMath for uint256;

  
  //Should execute add
  function test_add() public {
    uint256   varLib = 1;

varLib.add(9);
  }
  //Should execute sub
  function test_sub0() public {
    uint256   varLib = 10;

varLib.sub(0);
  }
  //Should fail sub when NOT comply with: b <= a
  function test_sub1() public {
    uint256   varLib = 10;

varLib.sub(11);
  }
  //Should execute mul WHEN a==0
  function test_mul0() public {
    uint256   varLib = 0;

varLib.mul(1);
  }
  //Should execute mul WHEN a!=0
  function test_mul1() public {
    uint256   varLib = 1;

varLib.mul(100001);
  }
  //Should execute div
  function test_div0() public {
    uint256   varLib = 0;

varLib.div(10);
  }
  //Should fail div when NOT comply with: b > 0
  function test_div1() public {
    uint256   varLib = 0;

varLib.div(0);
  }
  //Should execute mod
  function test_mod0() public {
    uint256   varLib = 11;

varLib.mod(99999);
  }
  //Should fail mod when NOT comply with: b != 0
  function test_mod1() public {
    uint256   varLib = 11;

varLib.mod(0);
  }
}
