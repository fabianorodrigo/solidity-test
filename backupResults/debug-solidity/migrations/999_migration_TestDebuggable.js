const TestContract = artifacts.require("TestDebuggable");
  const LibraryUnderTest = artifacts.require("Debuggable");

   async function doDeploy(deployer, network) {
       await deployer.deploy(LibraryUnderTest);
       await deployer.link(LibraryUnderTest, TestContract);
       await deployer.deploy(TestContract);
   }

 module.exports = (deployer, network) => {
      deployer.then(async () => {
              await doDeploy(deployer, network);
      });
  };