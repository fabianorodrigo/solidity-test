pragma solidity ^0.4.24;
import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/DebugCounter.sol";
import "../contracts/Debuggable.sol";

contract TestDebuggable  {
uint nonce = 68;
function stringToBytes32(string memory source) public returns (bytes32 result) {
    bytes memory tempEmptyStringTest = bytes(source);
    if (tempEmptyStringTest.length == 0) {
        return 0x0;
    }

    assembly {
        result := mload(add(source, 32))
    }
}

  
  //Should execute debug
  function test_debug() public {
    string  memory  param0 = "tqd0gd";
    string  memory  result0;
    result0 = Debuggable.debug(param0);
  }
  //Should execute debug
  function test_debug1() public {
    bytes32   param0 = stringToBytes32("97,239,151,249,229,76,204,0,0,0,0,0,0,0,0,0");
    bytes32   result0;
    result0 = Debuggable.debug(param0);
  }
  //Should execute debug
  function test_debug2() public {
    uint256   param0 = 2427598086;
    uint256   result0;
    result0 = Debuggable.debug(param0);
  }
  //Should execute debug
  function test_debug3() public {
    address   param0 = address(uint160(uint(keccak256(abi.encodePacked(nonce, blockhash(block.number))))));
    address   result0;
    result0 = Debuggable.debug(param0);
  }
  //Should execute debugRevert
  function test_debugRevert() public {
    Debuggable.debugRevert();
  }
  //Should execute debugRevert
  function test_debugRevert1() public {
    string  memory  param0 = "tqd0gd";
    Debuggable.debugRevert(param0);
  }
  //Should execute debugNoop
  function test_debugNoop() public {
    Debuggable.debugNoop();
  }
  //Should execute debugNoopConstant
  function test_debugNoopConstant() public {
    Debuggable.debugNoopConstant();
  }
}
