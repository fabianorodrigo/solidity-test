const fs = require('fs');
  let prjMenorTempo = null;
  let prjMaiorTempo = null;
  let prjMenorTempoCoverage = null;
  let prjMaiorTempoCoverage = null;
  let menorTempo = 9999999999999999999999;
  let maiorTempo = 0;
  let menorTempoCoverage = 99999999999999999999;
  let maiorTempoCoverage = 0;

Object.values(JSON.parse(fs.readFileSync('./solidity-test-report.json'))).filter(p=>p.successfull&&p.coverageSuccessful && !p.project.endsWith('06') && p.project.indexOf('zzz-for-debug')==-1).forEach(p=>{
	const ini = new Date(p.dhInit);
	const fim = new Date(p.dhFinish);
	const fimCov = new Date(p.dhFinishCoverage);

	if(fim.getTime()-ini.getTime() < menorTempo){
		menorTempo = fim.getTime()-ini.getTime();
		prjMenorTempo = p.project;
	}
	if(fim.getTime()-ini.getTime() > maiorTempo){
		maiorTempo = fim.getTime()-ini.getTime();
		prjMaiorTempo = p.project;
	}

	if(fimCov.getTime()-ini.getTime() < menorTempoCoverage){
		menorTempoCoverage = fimCov.getTime()-ini.getTime();
		prjMenorTempoCoverage = p.project;
	}
	console.log(p.project.substr(p.project.lastIndexOf('/')),p.dhInit,p.dhFinish,p.dhFinishCoverage);

	if(fimCov.getTime()-ini.getTime() > maiorTempoCoverage){
		maiorTempoCoverage = fimCov.getTime()-ini.getTime();
		prjMaiorTempoCoverage = p.project;
	}
	console.log(p.project.substr(p.project.lastIndexOf('/')),p.dhInit,p.dhFinish,p.dhFinishCoverage);


});
	console.log('');
	console.log(prjMenorTempo,menorTempo,menorTempo/1000);
	console.log(prjMaiorTempo,maiorTempo,maiorTempo/1000);

	console.log(prjMenorTempoCoverage,menorTempoCoverage,menorTempoCoverage/1000);
	console.log(prjMaiorTempoCoverage,maiorTempoCoverage,maiorTempoCoverage/1000);
