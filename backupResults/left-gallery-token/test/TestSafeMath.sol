pragma solidity ^0.4.24;
import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Master.sol";
import "../contracts/Token.sol";
import "../contracts/works/Autobreeder.sol";
import "../contracts/works/BedroomCuboroStacks.sol";
import "../contracts/works/BubamaraOCR.sol";
import "../contracts/works/EventListeners.sol";
import "../contracts/works/File.sol";
import "../contracts/works/MonacoMix.sol";
import "../contracts/works/Tumpin.sol";
import "../contracts/works/Undelivered.sol";

contract TestSafeMath  {
uint nonce = 60;
function stringToBytes32(string memory source) public returns (bytes32 result) {
    bytes memory tempEmptyStringTest = bytes(source);
    if (tempEmptyStringTest.length == 0) {
        return 0x0;
    }

    assembly {
        result := mload(add(source, 32))
    }
}
  using SafeMath for uint256;

  
  //Should execute mul WHEN a==0
  function test_mul0() public {
    uint256   varLib = 0;
  varLib.mul(4038714810);
  }
  //Should execute mul WHEN a!=0
  function test_mul1() public {
    uint256   varLib = 4038714809;
  varLib.mul(33);
  }
  //Should execute div
  function test_div() public {
    uint256   varLib = 33;
  varLib.div(101);
  }
  //Should execute sub
  function test_sub() public {
    uint256   varLib = 4038714810;
  varLib.sub(1);
  }
  //Should execute add
  function test_add() public {
    uint256   varLib = 2;
  varLib.add(101);
  }
}
