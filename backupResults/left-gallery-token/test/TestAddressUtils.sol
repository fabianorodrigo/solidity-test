pragma solidity ^0.4.24;
import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Master.sol";
import "../contracts/Token.sol";
import "../contracts/works/Autobreeder.sol";
import "../contracts/works/BedroomCuboroStacks.sol";
import "../contracts/works/BubamaraOCR.sol";
import "../contracts/works/EventListeners.sol";
import "../contracts/works/File.sol";
import "../contracts/works/MonacoMix.sol";
import "../contracts/works/Tumpin.sol";
import "../contracts/works/Undelivered.sol";

contract TestAddressUtils  {
uint nonce = 60;
function stringToBytes32(string memory source) public returns (bytes32 result) {
    bytes memory tempEmptyStringTest = bytes(source);
    if (tempEmptyStringTest.length == 0) {
        return 0x0;
    }

    assembly {
        result := mload(add(source, 32))
    }
}
  using AddressUtils for address;

  
  //Should execute isContract
  function test_isContract() public {
    address   varLib = address(uint160(uint(keccak256(abi.encodePacked(nonce, blockhash(block.number))))));
  varLib.isContract();
  }
}
