const truffleAssert = require('truffle-assertions');
const Account = artifacts.require("Account");
const ConvergentBeta = artifacts.require("ConvergentBeta");
const DoubleCurveToken = artifacts.require("DoubleCurveToken");
const GasPriceOracle = artifacts.require("GasPriceOracle");
const MockERC20 = artifacts.require("MockERC20");
const SafeMath = artifacts.require("openzeppelin-eth/contracts/math/SafeMath.sol");
const Ownable = artifacts.require("openzeppelin-eth/contracts/ownership/Ownable.sol");
const ERC20 = artifacts.require("openzeppelin-eth/contracts/token/ERC20/ERC20.sol");
const ERC20Detailed = artifacts.require("openzeppelin-eth/contracts/token/ERC20/ERC20Detailed.sol");
const AddressUtils = artifacts.require("openzeppelin-solidity/contracts/AddressUtils.sol");
const Initializable = artifacts.require("zos-lib/contracts/Initializable.sol");
const AdminUpgradeabilityProxy = artifacts.require("zos-lib/contracts/upgradeability/AdminUpgradeabilityProxy.sol");
const UpgradeabilityProxy = artifacts.require("zos-lib/contracts/upgradeability/UpgradeabilityProxy.sol");

contract("Account",(accounts)=>{
  let trace = false;
  let contractAddressUtils = null;
  let contractSafeMath = null;
  let contractOwnable = null;
  let contractInitializable = null;
  let contractERC20Detailed = null;
  let contractERC20 = null;
  let contractAdminUpgradeabilityProxy = null;
  let contractUpgradeabilityProxy = null;
  let contractConvergentBeta = null;
  let contractGasPriceOracle = null;
  let contractDoubleCurveToken = null;
  let contractAccount = null;
  let contractMockERC20 = null;
  beforeEach(async () => {
    contractAddressUtils = await AddressUtils.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: AddressUtils.new({from: accounts[0]}');
    contractSafeMath = await SafeMath.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: SafeMath.new({from: accounts[0]}');
    contractOwnable = await Ownable.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: Ownable.new({from: accounts[0]}');
    contractInitializable = await Initializable.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: Initializable.new({from: accounts[0]}');
    ERC20.link("SafeMath",contractSafeMath.address);
    contractERC20 = await ERC20.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC20.new({from: accounts[0]}');
    contractConvergentBeta = await ConvergentBeta.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: ConvergentBeta.new({from: accounts[0]}');
    contractGasPriceOracle = await GasPriceOracle.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: GasPriceOracle.new({from: accounts[0]}');
    DoubleCurveToken.link("SafeMath",contractSafeMath.address);
    contractDoubleCurveToken = await DoubleCurveToken.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: DoubleCurveToken.new({from: accounts[0]}');
    contractAccount = await Account.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: Account.new({from: accounts[0]}');
    contractMockERC20 = await MockERC20.new(10000,{from:accounts[3]});
    if(trace) console.log('SUCESSO: MockERC20.new(10000,{from:accounts[3]}');
  });
  
  it('Should execute proxy(address,bytes) WHEN msg.sender==beneficiary', async () => {
    //transactionParameter: [object Object]
    //Initializable.initialized: false
    //Initializable.initializing: false
    //Initializable.______gap: 
    //ERC20._totalSupply: 0
    //ERC20.______gap: 
    //ERC20.initialized: false
    //ERC20.initializing: false
    //ERC20Detailed._decimals: 0
    //ERC20Detailed.______gap: 
    //ERC20Detailed.initialized: false
    //ERC20Detailed.initializing: false
    //DoubleCurveToken.reserve: 0
    //DoubleCurveToken.contributions: 0
    //DoubleCurveToken.slopeN: 0
    //DoubleCurveToken.slopeD: 0
    //DoubleCurveToken.exponent: 0
    //DoubleCurveToken.spreadN: 0
    //DoubleCurveToken.spreadD: 0
    //DoubleCurveToken._totalSupply: 0
    //DoubleCurveToken.______gap: 
    //DoubleCurveToken._decimals: 0
    //DoubleCurveToken.initialized: false
    //DoubleCurveToken.initializing: false
    //Account.curServiceIndex: 0
    //Account.reserve: 0
    //Account.contributions: 0
    //Account.slopeN: 0
    //Account.slopeD: 0
    //Account.exponent: 0
    //Account.spreadN: 0
    //Account.spreadD: 0
    //Account._totalSupply: 0
    //Account.______gap: 
    //Account._decimals: 0
    //Account.initialized: false
    //Account.initializing: false
    await contractAccount.methods["initialize(address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes32,string,string,address)"](accounts[4],accounts[3],257,1532892062,1532892062,27,28,17,[30,42,206,45,185,158,151,193,140,242,198,20,218,45,10,31,222,156,33,190,165,190,122,105,99,89,173,255,119,254,253,15],"RevertWithReason","IsLibrary",accounts[8],{from:accounts[0]});
    let result = await contractAccount.proxy(accounts[2], [196,198,165,197,164,180,158,251,210,0,181,29,45,77,128,174,108,117,53,191,95,108,37,3,246,182,80,158,174,60,219,249],{from: accounts[3]});
  });
  it('Should fail proxy(address,bytes) when NOT comply with: msg.sender == beneficiary', async () => {
    //transactionParameter: [object Object]
    //Initializable.initialized: false
    //Initializable.initializing: false
    //Initializable.______gap: 
    //ERC20._totalSupply: 0
    //ERC20.______gap: 
    //ERC20.initialized: false
    //ERC20.initializing: false
    //ERC20Detailed._decimals: 0
    //ERC20Detailed.______gap: 
    //ERC20Detailed.initialized: false
    //ERC20Detailed.initializing: false
    //DoubleCurveToken.reserve: 0
    //DoubleCurveToken.contributions: 0
    //DoubleCurveToken.slopeN: 0
    //DoubleCurveToken.slopeD: 0
    //DoubleCurveToken.exponent: 0
    //DoubleCurveToken.spreadN: 0
    //DoubleCurveToken.spreadD: 0
    //DoubleCurveToken._totalSupply: 0
    //DoubleCurveToken.______gap: 
    //DoubleCurveToken._decimals: 0
    //DoubleCurveToken.initialized: false
    //DoubleCurveToken.initializing: false
    //Account.curServiceIndex: 0
    //Account.reserve: 0
    //Account.contributions: 0
    //Account.slopeN: 0
    //Account.slopeD: 0
    //Account.exponent: 0
    //Account.spreadN: 0
    //Account.spreadD: 0
    //Account._totalSupply: 0
    //Account.______gap: 
    //Account._decimals: 0
    //Account.initialized: false
    //Account.initializing: false
    await contractAccount.methods["initialize(address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes32,string,string,address)"](accounts[4],accounts[3],257,1532892062,1532892062,27,28,17,[30,42,206,45,185,158,151,193,140,242,198,20,218,45,10,31,222,156,33,190,165,190,122,105,99,89,173,255,119,254,253,15],"RevertWithReason","IsLibrary",accounts[8],{from:accounts[0]});
    let result = await truffleAssert.fails(contractAccount.proxy(accounts[2], [196,198,165,197,164,180,158,251,210,0,181,29,45,77,128,174,108,117,53,191,95,108,37,3,246,182,80,158,174,60,219,249],{from: accounts[9]}),'revert');
  });
  it('Should execute initialize(address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes32,string,string,address) WHEN initialized!=true', async () => {
    //transactionParameter: [object Object]
    //Initializable.initialized: false
    //Initializable.initializing: false
    //Initializable.______gap: 
    //ERC20._totalSupply: 0
    //ERC20.______gap: 
    //ERC20.initialized: false
    //ERC20.initializing: false
    //ERC20Detailed._decimals: 0
    //ERC20Detailed.______gap: 
    //ERC20Detailed.initialized: false
    //ERC20Detailed.initializing: false
    //DoubleCurveToken.reserve: 0
    //DoubleCurveToken.contributions: 0
    //DoubleCurveToken.slopeN: 0
    //DoubleCurveToken.slopeD: 0
    //DoubleCurveToken.exponent: 0
    //DoubleCurveToken.spreadN: 0
    //DoubleCurveToken.spreadD: 0
    //DoubleCurveToken._totalSupply: 0
    //DoubleCurveToken.______gap: 
    //DoubleCurveToken._decimals: 0
    //DoubleCurveToken.initialized: false
    //DoubleCurveToken.initializing: false
    //Account.curServiceIndex: 0
    //Account.reserve: 0
    //Account.contributions: 0
    //Account.slopeN: 0
    //Account.slopeD: 0
    //Account.exponent: 0
    //Account.spreadN: 0
    //Account.spreadD: 0
    //Account._totalSupply: 0
    //Account.______gap: 
    //Account._decimals: 0
    //Account.initialized: false
    //Account.initializing: false
    let result = await contractAccount.methods["initialize(address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes32,string,string,address)"](accounts[4], accounts[2], 1336, 2014223715, 10, 1338, 65, 257, [25,3,174,215,190,41,16,152,114,205,236,12,45,60,39,239,167,176,186,120,186,82,98,29,159,121,174,246,210,223,122,139], "", "UsesExample", accounts[1],{from: accounts[0]});
  });
  it('Should execute addService(uint256) WHEN msg.sender==beneficiary', async () => {
    //transactionParameter: [object Object]
    //Initializable.initialized: false
    //Initializable.initializing: false
    //Initializable.______gap: 
    //ERC20._totalSupply: 0
    //ERC20.______gap: 
    //ERC20.initialized: false
    //ERC20.initializing: false
    //ERC20Detailed._decimals: 0
    //ERC20Detailed.______gap: 
    //ERC20Detailed.initialized: false
    //ERC20Detailed.initializing: false
    //DoubleCurveToken.reserve: 0
    //DoubleCurveToken.contributions: 0
    //DoubleCurveToken.slopeN: 0
    //DoubleCurveToken.slopeD: 0
    //DoubleCurveToken.exponent: 0
    //DoubleCurveToken.spreadN: 0
    //DoubleCurveToken.spreadD: 0
    //DoubleCurveToken._totalSupply: 0
    //DoubleCurveToken.______gap: 
    //DoubleCurveToken._decimals: 0
    //DoubleCurveToken.initialized: false
    //DoubleCurveToken.initializing: false
    //Account.curServiceIndex: 0
    //Account.reserve: 0
    //Account.contributions: 0
    //Account.slopeN: 0
    //Account.slopeD: 0
    //Account.exponent: 0
    //Account.spreadN: 0
    //Account.spreadD: 0
    //Account._totalSupply: 0
    //Account.______gap: 
    //Account._decimals: 0
    //Account.initialized: false
    //Account.initializing: false
    await contractAccount.methods["initialize(address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes32,string,string,address)"](accounts[1],accounts[8],9,4,97,64,66,26,[101,29,230,49,103,7,57,122,106,182,87,20,65,152,151,59,3,212,24,228,148,162,172,36,177,252,127,134,41,131,134,72],"initialize(address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes32,string,string,address)","bouncer",accounts[4],{from:accounts[0]});
    let result = await contractAccount.addService(10000,{from: accounts[8]});
  });
  it('Should fail addService(uint256) when NOT comply with: msg.sender == beneficiary', async () => {
    //transactionParameter: [object Object]
    //Initializable.initialized: false
    //Initializable.initializing: false
    //Initializable.______gap: 
    //ERC20._totalSupply: 0
    //ERC20.______gap: 
    //ERC20.initialized: false
    //ERC20.initializing: false
    //ERC20Detailed._decimals: 0
    //ERC20Detailed.______gap: 
    //ERC20Detailed.initialized: false
    //ERC20Detailed.initializing: false
    //DoubleCurveToken.reserve: 0
    //DoubleCurveToken.contributions: 0
    //DoubleCurveToken.slopeN: 0
    //DoubleCurveToken.slopeD: 0
    //DoubleCurveToken.exponent: 0
    //DoubleCurveToken.spreadN: 0
    //DoubleCurveToken.spreadD: 0
    //DoubleCurveToken._totalSupply: 0
    //DoubleCurveToken.______gap: 
    //DoubleCurveToken._decimals: 0
    //DoubleCurveToken.initialized: false
    //DoubleCurveToken.initializing: false
    //Account.curServiceIndex: 0
    //Account.reserve: 0
    //Account.contributions: 0
    //Account.slopeN: 0
    //Account.slopeD: 0
    //Account.exponent: 0
    //Account.spreadN: 0
    //Account.spreadD: 0
    //Account._totalSupply: 0
    //Account.______gap: 
    //Account._decimals: 0
    //Account.initialized: false
    //Account.initializing: false
    await contractAccount.methods["initialize(address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes32,string,string,address)"](accounts[1],accounts[8],9,4,97,64,66,26,[101,29,230,49,103,7,57,122,106,182,87,20,65,152,151,59,3,212,24,228,148,162,172,36,177,252,127,134,41,131,134,72],"initialize(address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes32,string,string,address)","bouncer",accounts[4],{from:accounts[0]});
    let result = await truffleAssert.fails(contractAccount.addService(10000,{from: accounts[9]}),'revert');
  });
  it('Should execute removeService(uint8) WHEN msg.sender==beneficiary,services!=0', async () => {
    //transactionParameter: [object Object]
    //Initializable.initialized: false
    //Initializable.initializing: false
    //Initializable.______gap: 
    //ERC20._totalSupply: 0
    //ERC20.______gap: 
    //ERC20.initialized: false
    //ERC20.initializing: false
    //ERC20Detailed._decimals: 0
    //ERC20Detailed.______gap: 
    //ERC20Detailed.initialized: false
    //ERC20Detailed.initializing: false
    //DoubleCurveToken.reserve: 0
    //DoubleCurveToken.contributions: 0
    //DoubleCurveToken.slopeN: 0
    //DoubleCurveToken.slopeD: 0
    //DoubleCurveToken.exponent: 0
    //DoubleCurveToken.spreadN: 0
    //DoubleCurveToken.spreadD: 0
    //DoubleCurveToken._totalSupply: 0
    //DoubleCurveToken.______gap: 
    //DoubleCurveToken._decimals: 0
    //DoubleCurveToken.initialized: false
    //DoubleCurveToken.initializing: false
    //Account.curServiceIndex: 0
    //Account.reserve: 0
    //Account.contributions: 0
    //Account.slopeN: 0
    //Account.slopeD: 0
    //Account.exponent: 0
    //Account.spreadN: 0
    //Account.spreadD: 0
    //Account._totalSupply: 0
    //Account.______gap: 
    //Account._decimals: 0
    //Account.initialized: false
    //Account.initializing: false
    await contractAccount.methods["initialize(address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes32,string,string,address)"](accounts[9],accounts[7],1336,10000,2014223716,1,97,10001,[56,172,255,47,89,18,222,119,252,50,172,218,254,146,36,99,223,52,210,127,58,11,92,101,179,133,74,189,139,50,230,146],"Example","minter",accounts[4],{from:accounts[0]});
    let result = await contractAccount.removeService(97,{from: accounts[7]});
  });
  it('Should fail removeService(uint8) when NOT comply with: msg.sender == beneficiary', async () => {
    //transactionParameter: [object Object]
    //Initializable.initialized: false
    //Initializable.initializing: false
    //Initializable.______gap: 
    //ERC20._totalSupply: 0
    //ERC20.______gap: 
    //ERC20.initialized: false
    //ERC20.initializing: false
    //ERC20Detailed._decimals: 0
    //ERC20Detailed.______gap: 
    //ERC20Detailed.initialized: false
    //ERC20Detailed.initializing: false
    //DoubleCurveToken.reserve: 0
    //DoubleCurveToken.contributions: 0
    //DoubleCurveToken.slopeN: 0
    //DoubleCurveToken.slopeD: 0
    //DoubleCurveToken.exponent: 0
    //DoubleCurveToken.spreadN: 0
    //DoubleCurveToken.spreadD: 0
    //DoubleCurveToken._totalSupply: 0
    //DoubleCurveToken.______gap: 
    //DoubleCurveToken._decimals: 0
    //DoubleCurveToken.initialized: false
    //DoubleCurveToken.initializing: false
    //Account.curServiceIndex: 0
    //Account.reserve: 0
    //Account.contributions: 0
    //Account.slopeN: 0
    //Account.slopeD: 0
    //Account.exponent: 0
    //Account.spreadN: 0
    //Account.spreadD: 0
    //Account._totalSupply: 0
    //Account.______gap: 
    //Account._decimals: 0
    //Account.initialized: false
    //Account.initializing: false
    await contractAccount.methods["initialize(address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes32,string,string,address)"](accounts[9],accounts[7],1336,10000,2014223716,1,97,10001,[56,172,255,47,89,18,222,119,252,50,172,218,254,146,36,99,223,52,210,127,58,11,92,101,179,133,74,189,139,50,230,146],"Example","minter",accounts[4],{from:accounts[0]});
    let result = await truffleAssert.fails(contractAccount.removeService(97,{from: accounts[9]}),'revert');
  });
  it('Should execute updateMetadata(bytes32) WHEN msg.sender==beneficiary', async () => {
    //transactionParameter: [object Object]
    //Initializable.initialized: false
    //Initializable.initializing: false
    //Initializable.______gap: 
    //ERC20._totalSupply: 0
    //ERC20.______gap: 
    //ERC20.initialized: false
    //ERC20.initializing: false
    //ERC20Detailed._decimals: 0
    //ERC20Detailed.______gap: 
    //ERC20Detailed.initialized: false
    //ERC20Detailed.initializing: false
    //DoubleCurveToken.reserve: 0
    //DoubleCurveToken.contributions: 0
    //DoubleCurveToken.slopeN: 0
    //DoubleCurveToken.slopeD: 0
    //DoubleCurveToken.exponent: 0
    //DoubleCurveToken.spreadN: 0
    //DoubleCurveToken.spreadD: 0
    //DoubleCurveToken._totalSupply: 0
    //DoubleCurveToken.______gap: 
    //DoubleCurveToken._decimals: 0
    //DoubleCurveToken.initialized: false
    //DoubleCurveToken.initializing: false
    //Account.curServiceIndex: 0
    //Account.reserve: 0
    //Account.contributions: 0
    //Account.slopeN: 0
    //Account.slopeD: 0
    //Account.exponent: 0
    //Account.spreadN: 0
    //Account.spreadD: 0
    //Account._totalSupply: 0
    //Account.______gap: 
    //Account._decimals: 0
    //Account.initialized: false
    //Account.initializing: false
    await contractAccount.methods["initialize(address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes32,string,string,address)"](accounts[7],accounts[4],1,2014223716,9999,19,10000,2014223715,[219,78,5,228,34,10,201,143,25,142,248,99,226,95,72,16,121,53,232,136,186,25,159,120,223,159,187,180,121,44,79,52],"\x19Ethereum Signed Message:\n32","",accounts[2],{from:accounts[0]});
    let result = await contractAccount.updateMetadata([177,107,102,116,128,35,6,30,42,151,185,247,154,21,116,96,140,32,233,134,252,189,58,96,217,24,34,166,111,59,108,130],{from: accounts[4]});
  });
  it('Should fail updateMetadata(bytes32) when NOT comply with: msg.sender == beneficiary', async () => {
    //transactionParameter: [object Object]
    //Initializable.initialized: false
    //Initializable.initializing: false
    //Initializable.______gap: 
    //ERC20._totalSupply: 0
    //ERC20.______gap: 
    //ERC20.initialized: false
    //ERC20.initializing: false
    //ERC20Detailed._decimals: 0
    //ERC20Detailed.______gap: 
    //ERC20Detailed.initialized: false
    //ERC20Detailed.initializing: false
    //DoubleCurveToken.reserve: 0
    //DoubleCurveToken.contributions: 0
    //DoubleCurveToken.slopeN: 0
    //DoubleCurveToken.slopeD: 0
    //DoubleCurveToken.exponent: 0
    //DoubleCurveToken.spreadN: 0
    //DoubleCurveToken.spreadD: 0
    //DoubleCurveToken._totalSupply: 0
    //DoubleCurveToken.______gap: 
    //DoubleCurveToken._decimals: 0
    //DoubleCurveToken.initialized: false
    //DoubleCurveToken.initializing: false
    //Account.curServiceIndex: 0
    //Account.reserve: 0
    //Account.contributions: 0
    //Account.slopeN: 0
    //Account.slopeD: 0
    //Account.exponent: 0
    //Account.spreadN: 0
    //Account.spreadD: 0
    //Account._totalSupply: 0
    //Account.______gap: 
    //Account._decimals: 0
    //Account.initialized: false
    //Account.initializing: false
    await contractAccount.methods["initialize(address,address,uint256,uint256,uint256,uint256,uint256,uint256,bytes32,string,string,address)"](accounts[7],accounts[4],1,2014223716,9999,19,10000,2014223715,[219,78,5,228,34,10,201,143,25,142,248,99,226,95,72,16,121,53,232,136,186,25,159,120,223,159,187,180,121,44,79,52],"\x19Ethereum Signed Message:\n32","",accounts[2],{from:accounts[0]});
    let result = await truffleAssert.fails(contractAccount.updateMetadata([177,107,102,116,128,35,6,30,42,151,185,247,154,21,116,96,140,32,233,134,252,189,58,96,217,24,34,166,111,59,108,130],{from: accounts[9]}),'revert');
  });
  it('Should execute requestService(uint256,string) WHEN FunctionCall>=price,msg.sender!=0', async () => {
    //transactionParameter: [object Object]
    //Initializable.initialized: false
    //Initializable.initializing: false
    //Initializable.______gap: 
    //ERC20._totalSupply: 0
    //ERC20.______gap: 
    //ERC20.initialized: false
    //ERC20.initializing: false
    //ERC20Detailed._decimals: 0
    //ERC20Detailed.______gap: 
    //ERC20Detailed.initialized: false
    //ERC20Detailed.initializing: false
    //DoubleCurveToken.reserve: 0
    //DoubleCurveToken.contributions: 0
    //DoubleCurveToken.slopeN: 0
    //DoubleCurveToken.slopeD: 0
    //DoubleCurveToken.exponent: 0
    //DoubleCurveToken.spreadN: 0
    //DoubleCurveToken.spreadD: 0
    //DoubleCurveToken._totalSupply: 0
    //DoubleCurveToken.______gap: 
    //DoubleCurveToken._decimals: 0
    //DoubleCurveToken.initialized: false
    //DoubleCurveToken.initializing: false
    //Account.curServiceIndex: 0
    //Account.reserve: 0
    //Account.contributions: 0
    //Account.slopeN: 0
    //Account.slopeD: 0
    //Account.exponent: 0
    //Account.spreadN: 0
    //Account.spreadD: 0
    //Account._totalSupply: 0
    //Account.______gap: 
    //Account._decimals: 0
    //Account.initialized: false
    //Account.initializing: false
    let result = await contractAccount.requestService(27, "IsLibrary",{from: accounts[4]});
  });
  it('Should fail requestService(uint256,string) when NOT comply with: msg.sender != 0', async () => {
    //transactionParameter: [object Object]
    //Initializable.initialized: false
    //Initializable.initializing: false
    //Initializable.______gap: 
    //ERC20._totalSupply: 0
    //ERC20.______gap: 
    //ERC20.initialized: false
    //ERC20.initializing: false
    //ERC20Detailed._decimals: 0
    //ERC20Detailed.______gap: 
    //ERC20Detailed.initialized: false
    //ERC20Detailed.initializing: false
    //DoubleCurveToken.reserve: 0
    //DoubleCurveToken.contributions: 0
    //DoubleCurveToken.slopeN: 0
    //DoubleCurveToken.slopeD: 0
    //DoubleCurveToken.exponent: 0
    //DoubleCurveToken.spreadN: 0
    //DoubleCurveToken.spreadD: 0
    //DoubleCurveToken._totalSupply: 0
    //DoubleCurveToken.______gap: 
    //DoubleCurveToken._decimals: 0
    //DoubleCurveToken.initialized: false
    //DoubleCurveToken.initializing: false
    //Account.curServiceIndex: 0
    //Account.reserve: 0
    //Account.contributions: 0
    //Account.slopeN: 0
    //Account.slopeD: 0
    //Account.exponent: 0
    //Account.spreadN: 0
    //Account.spreadD: 0
    //Account._totalSupply: 0
    //Account.______gap: 
    //Account._decimals: 0
    //Account.initialized: false
    //Account.initializing: false
    let result = await truffleAssert.fails(contractAccount.requestService(27, "IsLibrary",{from: 0}),'revert');
  });
  it('Should execute creator()', async () => {
    //transactionParameter: [object Object]
    //Initializable.initialized: false
    //Initializable.initializing: false
    //Initializable.______gap: 
    //ERC20._totalSupply: 0
    //ERC20.______gap: 
    //ERC20.initialized: false
    //ERC20.initializing: false
    //ERC20Detailed._decimals: 0
    //ERC20Detailed.______gap: 
    //ERC20Detailed.initialized: false
    //ERC20Detailed.initializing: false
    //DoubleCurveToken.reserve: 0
    //DoubleCurveToken.contributions: 0
    //DoubleCurveToken.slopeN: 0
    //DoubleCurveToken.slopeD: 0
    //DoubleCurveToken.exponent: 0
    //DoubleCurveToken.spreadN: 0
    //DoubleCurveToken.spreadD: 0
    //DoubleCurveToken._totalSupply: 0
    //DoubleCurveToken.______gap: 
    //DoubleCurveToken._decimals: 0
    //DoubleCurveToken.initialized: false
    //DoubleCurveToken.initializing: false
    //Account.curServiceIndex: 0
    //Account.reserve: 0
    //Account.contributions: 0
    //Account.slopeN: 0
    //Account.slopeD: 0
    //Account.exponent: 0
    //Account.spreadN: 0
    //Account.spreadD: 0
    //Account._totalSupply: 0
    //Account.______gap: 
    //Account._decimals: 0
    //Account.initialized: false
    //Account.initializing: false
    let result = await contractAccount.creator({from: accounts[0]});
  });
});
