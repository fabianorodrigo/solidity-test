const truffleAssert = require('truffle-assertions');
const EcTools = artifacts.require("EcTools");
const SignatureVerifier = artifacts.require("SignatureVerifier");
const ProxyEcTools = artifacts.require("ProxyEcTools");

contract("contractProxyEcTools",(accounts)=>{
    let contractProxyEcTools = null;
  let trace = false;
  let contractEcTools = null;
  let contractSignatureVerifier = null;
  beforeEach(async () => {
    contractEcTools = await EcTools.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: EcTools.new({from: accounts[0]}');
    SignatureVerifier.link("EcTools",contractEcTools.address);
    contractSignatureVerifier = await SignatureVerifier.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: SignatureVerifier.new({from: accounts[0]}');
      ProxyEcTools.link('EcTools', contractEcTools.address);
    contractProxyEcTools = await ProxyEcTools.new({ from: accounts[0] });
});
  
  it('Should execute testrecover(bytes32,bytes) WHEN signedMessage.length!=65', async () => {
    let result = await contractProxyEcTools.testrecover([184,128,143,100,47,203,52,51,208,31,220,35,107,216,86,73,195,42,148,213,108,206,79,105,207,190,198,102,126,117,108,62], [178,116,64,248,31,11,45,145,154,221,2,247,217,96,195,174,27,100,184,40,40,164,81,10,175,110,251],{from: accounts[0]});
  });
  it('Should execute testrecover(bytes32,bytes) WHEN signedMessage.length==65', async () => {
    let result = await contractProxyEcTools.testrecover([112,105,178,42,248,147,6,152,99,138,200,86,11,209,76,86,49,75,237,215,140,143,178,142,159,107,167,136,175,138,41,74], [212,32,172,77,225,81,174,9,210,44,76,112,105,60,148,67,130,53,232,224,183,156,189,9,43,19,80,160,212,167,13,181,244,107,155,41,152,21,8,119,239,57,96,58,53,81,148,105,3,150,21,143,73,195,139,54,187,133,71,96,163,123,87,30,90],{from: accounts[0]});
  });
  it('Should execute testrecover(bytes32,bytes) WHEN Identifier<27', async () => {
    let result = await contractProxyEcTools.testrecover([165,8,43,81,25,34,152,126,40,216,122,213,16,217,24,82,223,113,254,212,61,222,229,191,5,13,18,189,181,2,52,136], [83,51,22,118,155,182,168,61,154,240,116,155,55,243,40,155,194,210,172,55,84,207,75,180,21,4,30,75,111,177,81,106],{from: accounts[0]});
  });
  it('Should execute testrecover(bytes32,bytes) WHEN Identifier>=27', async () => {
    let result = await contractProxyEcTools.testrecover([21,108,52,214,123,102,216,246,252,249,228,136,185,45,172,238,215,53,46,248,229,160,73,227,35,91,55,20,239,146,167,37], [142,219,121,16,81,43,85,95,29,212,19,177,173,122,204,219,164,127,78,172,190,180,245,198,185,5,125,9,128,73,163,220],{from: accounts[0]});
  });
  it('Should execute testrecover(bytes32,bytes) WHEN Identifier!=27,Identifier!=28', async () => {
    let result = await contractProxyEcTools.testrecover([97,27,194,203,159,188,179,14,22,228,131,205,237,148,3,234,165,24,65,175,240,61,96,117,113,168,138,162,111,11,75,188], [76,77,114,115,142,200,25,225,186,210,77,214,136,115,75,239,238,119,25,130,227,240,194,7,134,59,66,149,157,190,156,211],{from: accounts[0]});
  });
  it('Should execute testrecover(bytes32,bytes) WHEN Identifier==28', async () => {
    let result = await contractProxyEcTools.testrecover([245,88,132,199,147,165,149,219,114,231,221,63,32,244,166,32,150,255,168,54,109,140,105,153,121,179,100,62,120,235,146,214], [108,25,37,48,55,151,53,55,24,131,75,198,76,145,43,2,28,205,219,118,246,138,212,179,85,102,34,46,187,177,180,119],{from: accounts[0]});
  });
  it('Should execute testtoEthereumSignedMessage(bytes32)', async () => {
    let result = await contractProxyEcTools.testtoEthereumSignedMessage([74,44,209,49,112,143,133,199,121,86,11,136,174,148,176,208,71,36,64,210,184,213,250,120,190,106,26,205,166,100,248,251],{from: accounts[0]});
  });
  it('Should execute testprefixedRecover(bytes32,bytes)', async () => {
    let result = await contractProxyEcTools.testprefixedRecover([195,25,171,241,223,222,213,221,245,155,240,198,80,156,79,158,126,95,101,93,156,108,35,50,175,57,210,194,112,52,27,223], [85,57,190,130,215,150,138,183,169,38,157,235,193,103,194,240,139,186,95,41,155,93,119,253,18,5,144,47,162,121,226,77],{from: accounts[0]});
  });
});
