const truffleAssert = require('truffle-assertions');
const EcTools = artifacts.require("EcTools");
const SignatureVerifier = artifacts.require("SignatureVerifier");
const ProxyEcTools = artifacts.require("ProxyEcTools");

contract("SignatureVerifier",(accounts)=>{
  let trace = false;
  let contractEcTools = null;
  let contractSignatureVerifier = null;
  beforeEach(async () => {
    contractEcTools = await EcTools.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: EcTools.new({from: accounts[0]}');
    SignatureVerifier.link("EcTools",contractEcTools.address);
    contractSignatureVerifier = await SignatureVerifier.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: SignatureVerifier.new({from: accounts[0]}');
  });
  
  it('Should execute hashBid(SignatureVerifier.Bid)', async () => {
    //transactionParameter: [object Object]
    //SignatureVerifier.chainId: 1
    //SignatureVerifier.verifyingContract: 161774172539488124409029051691695345682981322752
    //SignatureVerifier.salt: 109841955370565475639945010436510886454890538232827929402961050150771112280064
    //SignatureVerifier.EIP712_DOMAIN: EIP712Domain(string name,string version,uint256 chainId,address verifyingContract,bytes32 salt)
    //SignatureVerifier.IDENTITY_TYPE: Identity(uint256 userId,address wallet)
    //SignatureVerifier.BID_TYPE: Bid(uint256 amount,Identity bidder)Identity(uint256 userId,address wallet)
    let result = await contractSignatureVerifier.hashBid({"amount": 0,"bidder": {"userId": 66,"wallet": accounts[1]}},{from: accounts[0]});
  });
  it('Should execute verifyHardCoded()', async () => {
    //transactionParameter: [object Object]
    //SignatureVerifier.chainId: 1
    //SignatureVerifier.verifyingContract: 161774172539488124409029051691695345682981322752
    //SignatureVerifier.salt: 109841955370565475639945010436510886454890538232827929402961050150771112280064
    //SignatureVerifier.EIP712_DOMAIN: EIP712Domain(string name,string version,uint256 chainId,address verifyingContract,bytes32 salt)
    //SignatureVerifier.IDENTITY_TYPE: Identity(uint256 userId,address wallet)
    //SignatureVerifier.BID_TYPE: Bid(uint256 amount,Identity bidder)Identity(uint256 userId,address wallet)
    let result = await contractSignatureVerifier.verifyHardCoded({from: accounts[0]});
  });
  it('Should execute verifySpecificWithPrefix(SignatureVerifier.Bid,bytes)', async () => {
    //transactionParameter: [object Object]
    //SignatureVerifier.chainId: 1
    //SignatureVerifier.verifyingContract: 161774172539488124409029051691695345682981322752
    //SignatureVerifier.salt: 109841955370565475639945010436510886454890538232827929402961050150771112280064
    //SignatureVerifier.EIP712_DOMAIN: EIP712Domain(string name,string version,uint256 chainId,address verifyingContract,bytes32 salt)
    //SignatureVerifier.IDENTITY_TYPE: Identity(uint256 userId,address wallet)
    //SignatureVerifier.BID_TYPE: Bid(uint256 amount,Identity bidder)Identity(uint256 userId,address wallet)
    let result = await contractSignatureVerifier.verifySpecificWithPrefix({"amount": 100,"bidder": {"userId": 27,"wallet": accounts[2]}}, [36,81,238,77,157,143,187,162,114,37,77,103,135,62,217,91,47,183,197,193,129,172,227,187,112,45,44,21,168,248,169,136],{from: accounts[0]});
  });
  it('Should execute verifySpecificWithoutPrefix(SignatureVerifier.Bid,bytes)', async () => {
    //transactionParameter: [object Object]
    //SignatureVerifier.chainId: 1
    //SignatureVerifier.verifyingContract: 161774172539488124409029051691695345682981322752
    //SignatureVerifier.salt: 109841955370565475639945010436510886454890538232827929402961050150771112280064
    //SignatureVerifier.EIP712_DOMAIN: EIP712Domain(string name,string version,uint256 chainId,address verifyingContract,bytes32 salt)
    //SignatureVerifier.IDENTITY_TYPE: Identity(uint256 userId,address wallet)
    //SignatureVerifier.BID_TYPE: Bid(uint256 amount,Identity bidder)Identity(uint256 userId,address wallet)
    let result = await contractSignatureVerifier.verifySpecificWithoutPrefix({"amount": 66,"bidder": {"userId": 29,"wallet": accounts[3]}}, [201,234,35,152,233,191,115,148,4,168,5,74,170,147,40,206,206,96,23,236,26,148,206,10,121,118,203,253,124,163,110,129],{from: accounts[0]});
  });
});
