pragma experimental ABIEncoderV2;
import "../EcTools.sol";

contract ProxyEcTools  {event __CoverageProxyEcTools(string fileName, uint256 lineNumber);
event __FunctionCoverageProxyEcTools(string fileName, uint256 fnId);
event __StatementCoverageProxyEcTools(string fileName, uint256 statementId);
event __BranchCoverageProxyEcTools(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageProxyEcTools(string fileName, uint256 branchId);
event __AssertPostCoverageProxyEcTools(string fileName, uint256 branchId);


      using EcTools for bytes32;

   function testrecover(bytes32  originalMessage, bytes memory signedMessage) public      returns (address ){emit __FunctionCoverageProxyEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/proxy4tests/EcTools.sol',1);

emit __CoverageProxyEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/proxy4tests/EcTools.sol',9);
    emit __StatementCoverageProxyEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/proxy4tests/EcTools.sol',1);
return originalMessage.recover(signedMessage);
   }

   function testtoEthereumSignedMessage(bytes32  _msg) public      returns (bytes32 ){emit __FunctionCoverageProxyEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/proxy4tests/EcTools.sol',2);

emit __CoverageProxyEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/proxy4tests/EcTools.sol',13);
    emit __StatementCoverageProxyEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/proxy4tests/EcTools.sol',2);
return _msg.toEthereumSignedMessage();
   }

   function testprefixedRecover(bytes32  _msg, bytes memory sig) public      returns (address ){emit __FunctionCoverageProxyEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/proxy4tests/EcTools.sol',3);

emit __CoverageProxyEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/proxy4tests/EcTools.sol',17);
    emit __StatementCoverageProxyEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/proxy4tests/EcTools.sol',3);
return _msg.prefixedRecover(sig);
   }


}