pragma experimental ABIEncoderV2;
pragma solidity ^0.5.1;

import "./EcTools.sol";

/**
 * Copied from MetaMask's example code for EIP 712.
 * Modified to enable verification of non-hardcoded externally signed bid through "verifySpecific(bid, signature)" functions
 */
contract SignatureVerifier {event __CoverageSignatureVerifier(string fileName, uint256 lineNumber);
event __FunctionCoverageSignatureVerifier(string fileName, uint256 fnId);
event __StatementCoverageSignatureVerifier(string fileName, uint256 statementId);
event __BranchCoverageSignatureVerifier(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageSignatureVerifier(string fileName, uint256 branchId);
event __AssertPostCoverageSignatureVerifier(string fileName, uint256 branchId);


    uint256 constant chainId = 1;
    address constant verifyingContract = 0x1C56346CD2A2Bf3202F771f50d3D14a367B48070;
    bytes32 constant salt = 0xf2d857f4a3edcb9b78b4d503bfe733db1e3f6cdc2b7971ee739626c97e86a558;

    string private constant EIP712_DOMAIN = "EIP712Domain(string name,string version,uint256 chainId,address verifyingContract,bytes32 salt)";
    string private constant IDENTITY_TYPE = "Identity(uint256 userId,address wallet)";
    string private constant BID_TYPE = "Bid(uint256 amount,Identity bidder)Identity(uint256 userId,address wallet)";

    bytes32 private constant EIP712_DOMAIN_TYPEHASH = keccak256(abi.encodePacked(EIP712_DOMAIN));
    bytes32 private constant IDENTITY_TYPEHASH = keccak256(abi.encodePacked(IDENTITY_TYPE));
    bytes32 private constant BID_TYPEHASH = keccak256(abi.encodePacked(BID_TYPE));
    bytes32 private constant DOMAIN_SEPARATOR = keccak256(abi.encode(
            EIP712_DOMAIN_TYPEHASH,
            keccak256("My amazing dApp"),
            keccak256("2"),
            chainId,
            verifyingContract,
            salt
        ));

    struct Identity {
        uint256 userId;
        address wallet;
    }

    struct Bid {
        uint256 amount;
        Identity bidder;
    }

    function hashIdentity(Identity memory identity) private      returns (bytes32) {emit __FunctionCoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',1);

emit __CoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',43);
        emit __StatementCoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',1);
return keccak256(abi.encode(
                IDENTITY_TYPEHASH,
                identity.userId,
                identity.wallet
            ));
    }

    function hashBid(Bid memory bid) public      returns (bytes32){emit __FunctionCoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',2);

emit __CoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',51);
        emit __StatementCoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',2);
return keccak256(abi.encodePacked(
                "\x19\x01",
                DOMAIN_SEPARATOR,
                keccak256(abi.encode(
                    BID_TYPEHASH,
                    bid.amount,
                    hashIdentity(bid.bidder)
                ))
            ));
    }

    function verifyHardCoded() public      returns (bool) {emit __FunctionCoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',3);

emit __CoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',63);
        emit __StatementCoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',3);
Identity memory bidder = Identity({
            userId: 323,
            wallet: 0x3333333333333333333333333333333333333333
            });

emit __CoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',68);
        emit __StatementCoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',4);
Bid memory bid = Bid({
            amount: 100,
            bidder: bidder
            });

emit __CoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',73);
        emit __StatementCoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',5);
bytes32 sigR = 0x4ce0d4986630fd8d74b7f21f7c610965ec6c2f7cebb50ce22f0db218e93fe608;
emit __CoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',74);
        emit __StatementCoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',6);
bytes32 sigS = 0x1152fe28bd3db6244042a28a355b19aac2818b6ed8029822ecaa55c010ec4003;
emit __CoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',75);
        emit __StatementCoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',7);
uint8 sigV = 27;
emit __CoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',76);
        emit __StatementCoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',8);
address signer = 0x93889F441C03E6E8A662c9c60c750A9BfEcb00bd;

emit __CoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',78);
        emit __StatementCoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',9);
return signer == ecrecover(hashBid(bid), sigV, sigR, sigS);
    }

    function verifySpecificWithPrefix(Bid memory bid, bytes memory signature) public      returns (address) {emit __FunctionCoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',4);

emit __CoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',82);
        emit __StatementCoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',10);
return EcTools.prefixedRecover(hashBid(bid), signature);
    }

    function verifySpecificWithoutPrefix(Bid memory bid, bytes memory signature) public      returns (address) {emit __FunctionCoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',5);

emit __CoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',86);
        emit __StatementCoverageSignatureVerifier('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/SignatureVerifier.sol',11);
return EcTools.recover(hashBid(bid), signature);
    }
}