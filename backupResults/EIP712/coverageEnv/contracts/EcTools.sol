pragma solidity ^0.5.1;

library EcTools {event __CoverageEcTools(string fileName, uint256 lineNumber);
event __FunctionCoverageEcTools(string fileName, uint256 fnId);
event __StatementCoverageEcTools(string fileName, uint256 statementId);
event __BranchCoverageEcTools(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageEcTools(string fileName, uint256 branchId);
event __AssertPostCoverageEcTools(string fileName, uint256 branchId);


  /**
   * @dev Recover signer address from a message by using his signature
   * @param originalMessage bytes32 message, the originalMessage is the signed message. What is recovered is the signer address.
   * @param signedMessage bytes signature
   */
  function recover(bytes32 originalMessage, bytes memory signedMessage) public      returns (address) {emit __FunctionCoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',1);

emit __CoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',11);
    emit __StatementCoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',1);
bytes32 r;
emit __CoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',12);
    emit __StatementCoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',2);
bytes32 s;
emit __CoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',13);
    emit __StatementCoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',3);
uint8 v;

    //Check the signature length
emit __CoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',16);
    emit __StatementCoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',4);
if (signedMessage.length != 65) {emit __BranchCoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',1,0);
emit __CoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',17);
      emit __StatementCoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',5);
return (address(0));
    }else { emit __BranchCoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',1,1);}


    // Divide the signature in r, s and v variables
emit __CoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',21);
    assembly {
      r := mload(add(signedMessage, 32))
      s := mload(add(signedMessage, 64))
      v := byte(0, mload(add(signedMessage, 96)))
    }

    // Version of signature should be 27 or 28, but 0 and 1 are also possible versions
emit __CoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',28);
    emit __StatementCoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',6);
if (v < 27) {emit __BranchCoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',2,0);
emit __CoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',29);
      emit __StatementCoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',7);
v += 27;
    }else { emit __BranchCoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',2,1);}


    // If the version is correct return the signer address
emit __CoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',33);
    emit __StatementCoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',8);
if (v != 27 && v != 28) {emit __BranchCoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',3,0);
emit __CoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',34);
      emit __StatementCoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',9);
return (address(0));
    } else {emit __BranchCoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',3,1);
emit __CoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',36);
      emit __StatementCoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',10);
return ecrecover(originalMessage, v, r, s);
    }
  }

  function toEthereumSignedMessage(bytes32 _msg) public      returns (bytes32) {emit __FunctionCoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',2);

emit __CoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',41);
    emit __StatementCoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',11);
bytes memory prefix = "\x19Ethereum Signed Message:\n32";
emit __CoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',42);
    emit __StatementCoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',12);
return keccak256(abi.encodePacked(prefix, _msg));
  }

  function prefixedRecover(bytes32 _msg, bytes memory sig) public      returns (address) {emit __FunctionCoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',3);

emit __CoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',46);
    emit __StatementCoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',13);
bytes32 ethSignedMsg = toEthereumSignedMessage(_msg);
emit __CoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',47);
    emit __StatementCoverageEcTools('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/EIP712/contracts/EcTools.sol',14);
return recover(ethSignedMsg, sig);
  }
}