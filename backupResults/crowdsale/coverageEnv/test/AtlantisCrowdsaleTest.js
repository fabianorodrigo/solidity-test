const truffleAssert = require('truffle-assertions');
const AtlantisCrowdsale = artifacts.require("AtlantisCrowdsale");
const AtlantisToken = artifacts.require("AtlantisToken");
const MinLimitCrowdsale = artifacts.require("MinLimitCrowdsale");
const Crowdsale = artifacts.require("openzeppelin-solidity/contracts/crowdsale/Crowdsale.sol");
const SafeMath = artifacts.require("openzeppelin-solidity/contracts/math/SafeMath.sol");
const ERC20 = artifacts.require("openzeppelin-solidity/contracts/token/ERC20/ERC20.sol");
const ERC20Detailed = artifacts.require("openzeppelin-solidity/contracts/token/ERC20/ERC20Detailed.sol");
const SafeERC20 = artifacts.require("openzeppelin-solidity/contracts/token/ERC20/SafeERC20.sol");
const ProxyMinLimitCrowdsale = artifacts.require("ProxyMinLimitCrowdsale");

contract("AtlantisCrowdsale",(accounts)=>{
  let trace = false;
  let contractSafeMath = null;
  let contractSafeERC20 = null;
  let contractERC20 = null;
  let contractERC20Detailed = null;
  let contractAtlantisToken = null;
  let contractCrowdsale = null;
  let contractMinLimitCrowdsale = null;
  let contractAtlantisCrowdsale = null;
  beforeEach(async () => {
    contractSafeMath = await SafeMath.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: SafeMath.new({from: accounts[0]}');
    contractSafeERC20 = await SafeERC20.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: SafeERC20.new({from: accounts[0]}');
    ERC20.link("SafeMath",contractSafeMath.address);
    contractERC20 = await ERC20.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: ERC20.new({from: accounts[0]}');
    contractAtlantisToken = await AtlantisToken.new({from:accounts[4]});
    if(trace) console.log('SUCESSO: AtlantisToken.new({from:accounts[4]}');
    Crowdsale.link("SafeMath",contractSafeMath.address);
     Crowdsale.link("SafeERC20",contractSafeERC20.address);
    contractCrowdsale = await Crowdsale.new(6,accounts[2],contractAtlantisToken.address,{from:accounts[0]});
    if(trace) console.log('SUCESSO: Crowdsale.new(6,accounts[2],contractAtlantisToken.address,{from:accounts[0]}');
    contractAtlantisCrowdsale = await AtlantisCrowdsale.new(2014223716,accounts[6],contractAtlantisToken.address,1532892063,(await web3.eth.getBlock(await web3.eth.getBlockNumber())).timestamp+35,(await web3.eth.getBlock(await web3.eth.getBlockNumber())).timestamp+35+969,254,{from:accounts[0]});
    if(trace) console.log('SUCESSO: AtlantisCrowdsale.new(2014223716,accounts[6],contractAtlantisToken.address,1532892063,(await web3.eth.getBlock(await web3.eth.getBlockNumber())).timestamp+35,(await web3.eth.getBlock(await web3.eth.getBlockNumber())).timestamp+35+969,254,{from:accounts[0]}');
  });
  
  it('Should execute offchainSale(address,uint256) WHEN msg.sender==_owner', async () => {
    //transactionParameter: [object Object]
    //ReentrancyGuard._guardCounter: 0
    //Crowdsale._rate: 0
    //Crowdsale._weiRaised: 0
    //Crowdsale._guardCounter: 0
    //CappedCrowdsale._cap: 0
    //TimedCrowdsale._openingTime: 0
    //TimedCrowdsale._closingTime: 0
    //MinLimitCrowdsale._minLimit: 0
    //AtlantisCrowdsale._minLimit: 254
    //AtlantisCrowdsale._rate: 0
    //AtlantisCrowdsale._weiRaised: 0
    //AtlantisCrowdsale._cap: 0
    //AtlantisCrowdsale._openingTime: ###null###
    //AtlantisCrowdsale._closingTime: ###null###
    //AtlantisCrowdsale._guardCounter: 1
    //AtlantisCrowdsale._owner: accounts[0]
    //AtlantisCrowdsale._wallet: accounts[6]
    //AtlantisCrowdsale._token: ERC20TokenMetadata,ERC20,ERC20Detailed,IERC20
    let result = await contractAtlantisCrowdsale.offchainSale(accounts[1], 500,{from: accounts[0]});
  });
  it('Should fail offchainSale(address,uint256) when NOT comply with: msg.sender == _owner', async () => {
    //transactionParameter: [object Object]
    //ReentrancyGuard._guardCounter: 0
    //Crowdsale._rate: 0
    //Crowdsale._weiRaised: 0
    //Crowdsale._guardCounter: 0
    //CappedCrowdsale._cap: 0
    //TimedCrowdsale._openingTime: 0
    //TimedCrowdsale._closingTime: 0
    //MinLimitCrowdsale._minLimit: 0
    //AtlantisCrowdsale._minLimit: 254
    //AtlantisCrowdsale._rate: 0
    //AtlantisCrowdsale._weiRaised: 0
    //AtlantisCrowdsale._cap: 0
    //AtlantisCrowdsale._openingTime: ###null###
    //AtlantisCrowdsale._closingTime: ###null###
    //AtlantisCrowdsale._guardCounter: 1
    //AtlantisCrowdsale._owner: accounts[0]
    //AtlantisCrowdsale._wallet: accounts[6]
    //AtlantisCrowdsale._token: ERC20TokenMetadata,ERC20,ERC20Detailed,IERC20
    let result = await truffleAssert.fails(contractAtlantisCrowdsale.offchainSale(accounts[1], 500,{from: accounts[9]}),'revert');
  });
  it('Should execute minLimit()', async () => {
    //transactionParameter: [object Object]
    //ReentrancyGuard._guardCounter: 0
    //Crowdsale._rate: 0
    //Crowdsale._weiRaised: 0
    //Crowdsale._guardCounter: 0
    //CappedCrowdsale._cap: 0
    //TimedCrowdsale._openingTime: 0
    //TimedCrowdsale._closingTime: 0
    //MinLimitCrowdsale._minLimit: 0
    //AtlantisCrowdsale._minLimit: 254
    //AtlantisCrowdsale._rate: 0
    //AtlantisCrowdsale._weiRaised: 0
    //AtlantisCrowdsale._cap: 0
    //AtlantisCrowdsale._openingTime: ###null###
    //AtlantisCrowdsale._closingTime: ###null###
    //AtlantisCrowdsale._guardCounter: 1
    //AtlantisCrowdsale._owner: accounts[0]
    //AtlantisCrowdsale._wallet: accounts[6]
    //AtlantisCrowdsale._token: ERC20TokenMetadata,ERC20,ERC20Detailed,IERC20
    let result = await contractAtlantisCrowdsale.minLimit({from: accounts[0]});
  });
});
