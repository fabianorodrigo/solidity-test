const truffleAssert = require('truffle-assertions');
const AtlantisCrowdsale = artifacts.require("AtlantisCrowdsale");
const AtlantisToken = artifacts.require("AtlantisToken");
const MinLimitCrowdsale = artifacts.require("MinLimitCrowdsale");

contract("MinLimitCrowdsale",(accounts)=>{
  let trace = false;
  let contractAtlantisToken = null;
  let contractMinLimitCrowdsale = null;
  let contractAtlantisCrowdsale = null;
  beforeEach(async () => {
    contractAtlantisToken = await AtlantisToken.new({from:accounts[2]});
    if(trace) console.log('SUCESSO: AtlantisToken.new({from:accounts[2]}');
    contractAtlantisCrowdsale = await AtlantisCrowdsale.new(256,accounts[0],contractAtlantisToken.address,500,(await web3.eth.getBlock(await web3.eth.getBlockNumber())).timestamp+685,(await web3.eth.getBlock(await web3.eth.getBlockNumber())).timestamp+685+655,1532892063,{from:accounts[0]});
    if(trace) console.log('SUCESSO: AtlantisCrowdsale.new(256,accounts[0],contractAtlantisToken.address,500,(await web3.eth.getBlock(await web3.eth.getBlockNumber())).timestamp+685,(await web3.eth.getBlock(await web3.eth.getBlockNumber())).timestamp+685+655,1532892063,{from:accounts[0]}');
  });
  
  it('Should execute minLimit()', async () => {
    //transactionParameter: [object Object]
    //_guardCounter: 1
    //_rate: 0
    //_weiRaised: 0
    //_minLimit: 3
    let result = await contractMinLimitCrowdsale.minLimit({from: accounts[0]});
  });
});
