pragma solidity ^0.5.4;

import "openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "openzeppelin-solidity/contracts/crowdsale/validation/TimedCrowdsale.sol";
import "openzeppelin-solidity/contracts/crowdsale/validation/CappedCrowdsale.sol";
import "./MinLimitCrowdsale.sol";


contract AtlantisCrowdsale is
    Ownable,
    CappedCrowdsale,
    TimedCrowdsale,
    MinLimitCrowdsale
{event __CoverageAtlantisCrowdsale(string fileName, uint256 lineNumber);
event __FunctionCoverageAtlantisCrowdsale(string fileName, uint256 fnId);
event __StatementCoverageAtlantisCrowdsale(string fileName, uint256 statementId);
event __BranchCoverageAtlantisCrowdsale(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageAtlantisCrowdsale(string fileName, uint256 branchId);
event __AssertPostCoverageAtlantisCrowdsale(string fileName, uint256 branchId);

    constructor(
        uint256 tokensPerEth,   // 10000
        address payable wallet,
        IERC20 token,
        uint256 hardcap,        // 1000 ETH
        uint256 openingTime,
        uint256 closingTime,
        uint256 minLimit        // 1 ETH
    )
        public
        Crowdsale(tokensPerEth, wallet, token)
        CappedCrowdsale(hardcap)
        TimedCrowdsale(openingTime, closingTime)
        MinLimitCrowdsale(minLimit)
    {emit __FunctionCoverageAtlantisCrowdsale('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/AtlantisCrowdsale.sol',1);

    }

    function offchainSale(address investor, uint256 tokenAmount) public onlyOwner {emit __FunctionCoverageAtlantisCrowdsale('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/AtlantisCrowdsale.sol',2);

emit __CoverageAtlantisCrowdsale('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/AtlantisCrowdsale.sol',33);
        emit __StatementCoverageAtlantisCrowdsale('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/AtlantisCrowdsale.sol',1);
_deliverTokens(investor, tokenAmount);
    }
}
