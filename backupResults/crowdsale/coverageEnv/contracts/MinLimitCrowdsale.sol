pragma solidity ^0.5.4;

import "openzeppelin-solidity/contracts/crowdsale/Crowdsale.sol";


contract MinLimitCrowdsale is Crowdsale {event __CoverageMinLimitCrowdsale(string fileName, uint256 lineNumber);
event __FunctionCoverageMinLimitCrowdsale(string fileName, uint256 fnId);
event __StatementCoverageMinLimitCrowdsale(string fileName, uint256 statementId);
event __BranchCoverageMinLimitCrowdsale(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageMinLimitCrowdsale(string fileName, uint256 branchId);
event __AssertPostCoverageMinLimitCrowdsale(string fileName, uint256 branchId);

    uint256 private _minLimit;

    constructor(uint256 minLimit) public {emit __FunctionCoverageMinLimitCrowdsale('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/MinLimitCrowdsale.sol',1);

emit __CoverageMinLimitCrowdsale('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/MinLimitCrowdsale.sol',10);
        emit __StatementCoverageMinLimitCrowdsale('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/MinLimitCrowdsale.sol',1);
_minLimit = minLimit;
    }

    function minLimit() public      returns (uint256) {emit __FunctionCoverageMinLimitCrowdsale('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/MinLimitCrowdsale.sol',2);

emit __CoverageMinLimitCrowdsale('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/MinLimitCrowdsale.sol',14);
        emit __StatementCoverageMinLimitCrowdsale('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/MinLimitCrowdsale.sol',2);
return _minLimit;
    }

    function _preValidatePurchase(address beneficiary, uint256 weiAmount) internal      {emit __FunctionCoverageMinLimitCrowdsale('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/MinLimitCrowdsale.sol',3);

emit __CoverageMinLimitCrowdsale('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/MinLimitCrowdsale.sol',18);
        emit __StatementCoverageMinLimitCrowdsale('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/MinLimitCrowdsale.sol',3);
super._preValidatePurchase(beneficiary, weiAmount);
emit __CoverageMinLimitCrowdsale('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/MinLimitCrowdsale.sol',19);
        emit __AssertPreCoverageMinLimitCrowdsale('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/MinLimitCrowdsale.sol',1);
emit __StatementCoverageMinLimitCrowdsale('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/MinLimitCrowdsale.sol',4);
require(weiAmount >= _minLimit);emit __AssertPostCoverageMinLimitCrowdsale('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/MinLimitCrowdsale.sol',1);

    }
}
