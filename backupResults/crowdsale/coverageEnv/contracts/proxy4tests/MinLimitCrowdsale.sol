import "../MinLimitCrowdsale.sol";

contract ProxyMinLimitCrowdsale  is Crowdsale,MinLimitCrowdsale  {event __CoverageProxyMinLimitCrowdsale(string fileName, uint256 lineNumber);
event __FunctionCoverageProxyMinLimitCrowdsale(string fileName, uint256 fnId);
event __StatementCoverageProxyMinLimitCrowdsale(string fileName, uint256 statementId);
event __BranchCoverageProxyMinLimitCrowdsale(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageProxyMinLimitCrowdsale(string fileName, uint256 branchId);
event __AssertPostCoverageProxyMinLimitCrowdsale(string fileName, uint256 branchId);


      constructor(uint256   rate,address payable  wallet,IERC20   token,uint256   minLimit) public Crowdsale(rate,wallet,token) MinLimitCrowdsale(minLimit) {emit __FunctionCoverageProxyMinLimitCrowdsale('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/proxy4tests/MinLimitCrowdsale.sol',1);
}
   function test_preValidatePurchase(address  beneficiary, uint256  weiAmount) public      {emit __FunctionCoverageProxyMinLimitCrowdsale('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/proxy4tests/MinLimitCrowdsale.sol',2);

emit __CoverageProxyMinLimitCrowdsale('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/proxy4tests/MinLimitCrowdsale.sol',7);
    emit __StatementCoverageProxyMinLimitCrowdsale('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/proxy4tests/MinLimitCrowdsale.sol',1);
_preValidatePurchase(beneficiary,weiAmount);
   }


}