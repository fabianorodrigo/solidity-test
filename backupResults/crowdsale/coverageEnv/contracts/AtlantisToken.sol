pragma solidity ^0.5.4;

import "openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";
import "openzeppelin-solidity/contracts/token/ERC20/ERC20Detailed.sol";


contract AtlantisToken is ERC20, ERC20Detailed {event __CoverageAtlantisToken(string fileName, uint256 lineNumber);
event __FunctionCoverageAtlantisToken(string fileName, uint256 fnId);
event __StatementCoverageAtlantisToken(string fileName, uint256 statementId);
event __BranchCoverageAtlantisToken(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageAtlantisToken(string fileName, uint256 branchId);
event __AssertPostCoverageAtlantisToken(string fileName, uint256 branchId);

    constructor()
        public
        ERC20Detailed("Atlantis Gold", "ANG", 18)
    {emit __FunctionCoverageAtlantisToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/AtlantisToken.sol',1);

emit __CoverageAtlantisToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/AtlantisToken.sol',12);
        emit __StatementCoverageAtlantisToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/AtlantisToken.sol',1);
uint256 totalAmount = 500e6 * (10**uint(decimals()));
emit __CoverageAtlantisToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/AtlantisToken.sol',13);
        emit __StatementCoverageAtlantisToken('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/crowdsale/contracts/AtlantisToken.sol',2);
_mint(msg.sender, totalAmount);
    }
}
