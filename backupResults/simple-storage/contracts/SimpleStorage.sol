pragma solidity ^0.4.23;

contract SimpleStorage {
    string public _val;

    function get() public view returns (string) {
        return _val;
    }

    function set(string val) public {
        _val = val;
    }
}
