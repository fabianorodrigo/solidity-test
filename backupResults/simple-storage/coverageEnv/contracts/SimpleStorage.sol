pragma solidity ^0.4.23;

contract SimpleStorage {event __CoverageSimpleStorage(string fileName, uint256 lineNumber);
event __FunctionCoverageSimpleStorage(string fileName, uint256 fnId);
event __StatementCoverageSimpleStorage(string fileName, uint256 statementId);
event __BranchCoverageSimpleStorage(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageSimpleStorage(string fileName, uint256 branchId);
event __AssertPostCoverageSimpleStorage(string fileName, uint256 branchId);

    string public _val;

    function get() public      returns (string) {emit __FunctionCoverageSimpleStorage('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/simple-storage/contracts/SimpleStorage.sol',1);

emit __CoverageSimpleStorage('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/simple-storage/contracts/SimpleStorage.sol',7);
        emit __StatementCoverageSimpleStorage('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/simple-storage/contracts/SimpleStorage.sol',1);
return _val;
    }

    function set(string val) public {emit __FunctionCoverageSimpleStorage('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/simple-storage/contracts/SimpleStorage.sol',2);

emit __CoverageSimpleStorage('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/simple-storage/contracts/SimpleStorage.sol',11);
        emit __StatementCoverageSimpleStorage('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/simple-storage/contracts/SimpleStorage.sol',2);
_val = val;
    }
}
