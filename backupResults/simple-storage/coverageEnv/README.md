## Deployment with Truffle HD Wallet & Infura

### Get Started
```shell
npm install
```

### Test
1. Start Local blockchain
```shell
# npm run chain
npx ganache-cli
```

2. In another terminal
```shell
# npm test
npx truffle test
```
