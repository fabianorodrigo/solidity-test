const truffleAssert = require('truffle-assertions');
const SimpleStorage = artifacts.require("SimpleStorage");

contract("SimpleStorage",(accounts)=>{
  let trace = false;
  let contractSimpleStorage = null;
  beforeEach(async () => {
    contractSimpleStorage = await SimpleStorage.new({from: accounts[0]});
    if(trace) console.log('SUCESSO: SimpleStorage.new({from: accounts[0]}');
  });
  
  it('Should execute get()', async () => {
    //transactionParameter: [object Object]
    let result = await contractSimpleStorage.get({from: accounts[0]});
  });
  it('Should execute set(string)', async () => {
    //transactionParameter: [object Object]
    let result = await contractSimpleStorage.set("wpvqkg",{from: accounts[0]});
  });
});
