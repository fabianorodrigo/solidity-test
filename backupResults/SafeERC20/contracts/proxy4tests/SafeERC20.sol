import "../libs/SafeERC20.sol";

contract ProxySafeERC20  {

      using SafeERC20 for IERC20;

   function testsafeTransfer(IERC20  _token, address  _to, uint256  _value) public  returns (bool ){
    return _token.safeTransfer(_to,_value);
   }

   function testsafeTransferFrom(IERC20  _token, address  _from, address  _to, uint256  _value) public  returns (bool ){
    return _token.safeTransferFrom(_from,_to,_value);
   }

   function testsafeApprove(IERC20  _token, address  _spender, uint256  _value) public  returns (bool ){
    return _token.safeApprove(_spender,_value);
   }

   function testclearApprove(IERC20  _token, address  _spender) public  returns (bool ){
    return _token.clearApprove(_spender);
   }


}