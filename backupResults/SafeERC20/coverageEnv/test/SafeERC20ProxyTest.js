const truffleAssert = require('truffle-assertions');
const SafeERC20 = artifacts.require("SafeERC20");
const ProxySafeERC20 = artifacts.require("ProxySafeERC20");

contract("contractProxySafeERC20",(accounts)=>{
    let contractProxySafeERC20 = null;
  let trace = false;
  let contractSafeERC20 = null;    ProxySafeERC20.link('SafeERC20', contractSafeERC20.address);
    contractProxySafeERC20 = await ProxySafeERC20.new({ from: accounts[0] });

  
  it('Should execute testsafeTransfer(IERC20,address,uint256)', async () => {
    let result = await contractProxySafeERC20.testsafeTransfer(contractERC20Burnable.address, accounts[4], 6,{from: accounts[0]});
  });
  it('Should execute testsafeTransferFrom(IERC20,address,address,uint256)', async () => {
    let result = await contractProxySafeERC20.testsafeTransferFrom(contractERC20Snapshot.address, accounts[7], accounts[8], 1337,{from: accounts[0]});
  });
  it('Should execute testsafeApprove(IERC20,address,uint256)', async () => {
    let result = await contractProxySafeERC20.testsafeApprove(contractStandaloneERC20.address, accounts[2], 6,{from: accounts[0]});
  });
  it('Should execute testclearApprove(IERC20,address) WHEN Identifier!=true', async () => {
    let result = await contractProxySafeERC20.testclearApprove(contractERC20Mintable.address, accounts[3],{from: accounts[0]});
  });
  it('Should execute testclearApprove(IERC20,address) WHEN Identifier==true', async () => {
    let result = await contractProxySafeERC20.testclearApprove(contractERC20Mintable.address, accounts[9],{from: accounts[0]});
  });
});
