pragma solidity ^0.5.0;

contract Migrations {event __CoverageMigrations(string fileName, uint256 lineNumber);
event __FunctionCoverageMigrations(string fileName, uint256 fnId);
event __StatementCoverageMigrations(string fileName, uint256 statementId);
event __BranchCoverageMigrations(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageMigrations(string fileName, uint256 branchId);
event __AssertPostCoverageMigrations(string fileName, uint256 branchId);

  address public owner;
  uint public last_completed_migration;

  constructor() public {emit __FunctionCoverageMigrations('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/utils/Migrations.sol',1);

emit __CoverageMigrations('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/utils/Migrations.sol',8);
    emit __StatementCoverageMigrations('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/utils/Migrations.sol',1);
owner = msg.sender;
  }

  modifier restricted() {emit __FunctionCoverageMigrations('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/utils/Migrations.sol',2);

emit __CoverageMigrations('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/utils/Migrations.sol',12);
    emit __StatementCoverageMigrations('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/utils/Migrations.sol',2);
if (msg.sender == owner) {emit __BranchCoverageMigrations('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/utils/Migrations.sol',1,0);_;}else { emit __BranchCoverageMigrations('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/utils/Migrations.sol',1,1);}

  }

  function setCompleted(uint completed) public restricted {emit __FunctionCoverageMigrations('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/utils/Migrations.sol',3);

emit __CoverageMigrations('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/utils/Migrations.sol',16);
    emit __StatementCoverageMigrations('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/utils/Migrations.sol',3);
last_completed_migration = completed;
  }

  function upgrade(address new_address) public restricted {emit __FunctionCoverageMigrations('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/utils/Migrations.sol',4);

emit __CoverageMigrations('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/utils/Migrations.sol',20);
    emit __StatementCoverageMigrations('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/utils/Migrations.sol',4);
Migrations upgraded = Migrations(new_address);
emit __CoverageMigrations('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/utils/Migrations.sol',21);
    emit __StatementCoverageMigrations('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/utils/Migrations.sol',5);
upgraded.setCompleted(last_completed_migration);
  }
}
