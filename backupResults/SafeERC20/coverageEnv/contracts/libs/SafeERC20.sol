pragma solidity ^0.5.0;

import "openzeppelin-eth/contracts/token/ERC20/IERC20.sol";


/**
* @dev Library to perform safe calls to standard method for ERC20 tokens.
*
* Why Transfers: transfer methods could have a return value (bool), throw or revert for insufficient funds or
* unathorized value.
*
* Why Approve: approve method could has a return value (bool) or does not accept 0 as a valid value (BNB token).
* The common strategy used to clean approvals.
*
* We use the Solidity call instead of interface methods because in the case of transfer, it will fail
* for tokens with an implementation without returning a value.
* Since versions of Solidity 0.4.22 the EVM has a new opcode, called RETURNDATASIZE.
* This opcode stores the size of the returned data of an external call. The code checks the size of the return value
* after an external call and reverts the transaction in case the return data is shorter than expected
*/
library SafeERC20 {event __CoverageSafeERC20(string fileName, uint256 lineNumber);
event __FunctionCoverageSafeERC20(string fileName, uint256 fnId);
event __StatementCoverageSafeERC20(string fileName, uint256 statementId);
event __BranchCoverageSafeERC20(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageSafeERC20(string fileName, uint256 branchId);
event __AssertPostCoverageSafeERC20(string fileName, uint256 branchId);

    /**
    * @dev Transfer token for a specified address
    * @param _token erc20 The address of the ERC20 contract
    * @param _to address The address which you want to transfer to
    * @param _value uint256 the _value of tokens to be transferred
    * @return bool whether the transfer was successful or not
    */
    function safeTransfer(IERC20 _token, address _to, uint256 _value) internal returns (bool) {emit __FunctionCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',1);

emit __CoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',30);
        emit __StatementCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',1);
uint256 prevBalance = _token.balanceOf(address(this));

emit __CoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',32);
        emit __StatementCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',2);
if (prevBalance < _value) {emit __BranchCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',1,0);
            // Insufficient funds
emit __CoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',34);
            emit __StatementCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',3);
return false;
        }else { emit __BranchCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',1,1);}


emit __CoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',37);
        emit __StatementCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',4);
address(_token).call(
            abi.encodeWithSignature("transfer(address,uint256)", _to, _value)
        );

emit __CoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',41);
        emit __StatementCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',5);
if (prevBalance - _value != _token.balanceOf(address(this))) {emit __BranchCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',2,0);
            // Transfer failed
emit __CoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',43);
            emit __StatementCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',6);
return false;
        }else { emit __BranchCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',2,1);}


emit __CoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',46);
        emit __StatementCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',7);
return true;
    }

    /**
    * @dev Transfer tokens from one address to another
    * @param _token erc20 The address of the ERC20 contract
    * @param _from address The address which you want to send tokens from
    * @param _to address The address which you want to transfer to
    * @param _value uint256 the _value of tokens to be transferred
    * @return bool whether the transfer was successful or not
    */
    function safeTransferFrom(
        IERC20 _token,
        address _from,
        address _to,
        uint256 _value
    ) internal returns (bool)
    {emit __FunctionCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',2);

emit __CoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',64);
        emit __StatementCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',8);
uint256 prevBalance = _token.balanceOf(_from);

emit __CoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',66);
        emit __StatementCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',9);
if (prevBalance < _value) {emit __BranchCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',3,0);
            // Insufficient funds
emit __CoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',68);
            emit __StatementCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',10);
return false;
        }else { emit __BranchCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',3,1);}


emit __CoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',71);
        emit __StatementCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',11);
if (_token.allowance(_from, address(this)) < _value) {emit __BranchCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',4,0);
            // Insufficient allowance
emit __CoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',73);
            emit __StatementCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',12);
return false;
        }else { emit __BranchCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',4,1);}


emit __CoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',76);
        emit __StatementCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',13);
address(_token).call(
            abi.encodeWithSignature("transferFrom(address,address,uint256)", _from, _to, _value)
        );

emit __CoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',80);
        emit __StatementCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',14);
if (prevBalance - _value != _token.balanceOf(_from)) {emit __BranchCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',5,0);
            // Transfer failed
emit __CoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',82);
            emit __StatementCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',15);
return false;
        }else { emit __BranchCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',5,1);}


emit __CoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',85);
        emit __StatementCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',16);
return true;
    }

   /**
   * @dev Approve the passed address to spend the specified amount of tokens on behalf of msg.sender.
   *
   * Beware that changing an allowance with this method brings the risk that someone may use both the old
   * and the new allowance by unfortunate transaction ordering. One possible solution to mitigate this
   * race condition is to first reduce the spender's allowance to 0 and set the desired value afterwards:
   * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
   *
   * @param _token erc20 The address of the ERC20 contract
   * @param _spender The address which will spend the funds.
   * @param _value The amount of tokens to be spent.
   * @return bool whether the approve was successful or not
   */
    function safeApprove(IERC20 _token, address _spender, uint256 _value) internal returns (bool) {emit __FunctionCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',3);

emit __CoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',102);
        emit __StatementCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',17);
address(_token).call(
            abi.encodeWithSignature("approve(address,uint256)",_spender, _value)
        );

emit __CoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',106);
        emit __StatementCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',18);
if (_token.allowance(address(this), _spender) != _value) {emit __BranchCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',6,0);
            // Approve failed
emit __CoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',108);
            emit __StatementCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',19);
return false;
        }else { emit __BranchCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',6,1);}


emit __CoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',111);
        emit __StatementCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',20);
return true;
    }

   /**
   * @dev Clear approval
   * Note that if 0 is not a valid value it will be set to 1.
   * @param _token erc20 The address of the ERC20 contract
   * @param _spender The address which will spend the funds.
   */
    function clearApprove(IERC20 _token, address _spender) internal returns (bool) {emit __FunctionCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',4);

emit __CoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',121);
        emit __StatementCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',21);
bool success = safeApprove(_token, _spender, 0);

emit __CoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',123);
        emit __StatementCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',22);
if (!success) {emit __BranchCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',7,0);
emit __CoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',124);
            emit __StatementCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',23);
success = safeApprove(_token, _spender, 1);
        }else { emit __BranchCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',7,1);}


emit __CoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',127);
        emit __StatementCoverageSafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/libs/SafeERC20.sol',24);
return success;
    }
}
