import "../libs/SafeERC20.sol";

contract ProxySafeERC20  {event __CoverageProxySafeERC20(string fileName, uint256 lineNumber);
event __FunctionCoverageProxySafeERC20(string fileName, uint256 fnId);
event __StatementCoverageProxySafeERC20(string fileName, uint256 statementId);
event __BranchCoverageProxySafeERC20(string fileName, uint256 branchId, uint256 locationIdx);
event __AssertPreCoverageProxySafeERC20(string fileName, uint256 branchId);
event __AssertPostCoverageProxySafeERC20(string fileName, uint256 branchId);


      using SafeERC20 for IERC20;

   function testsafeTransfer(IERC20  _token, address  _to, uint256  _value) public  returns (bool ){emit __FunctionCoverageProxySafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/proxy4tests/SafeERC20.sol',1);

emit __CoverageProxySafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/proxy4tests/SafeERC20.sol',8);
    emit __StatementCoverageProxySafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/proxy4tests/SafeERC20.sol',1);
return _token.safeTransfer(_to,_value);
   }

   function testsafeTransferFrom(IERC20  _token, address  _from, address  _to, uint256  _value) public  returns (bool ){emit __FunctionCoverageProxySafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/proxy4tests/SafeERC20.sol',2);

emit __CoverageProxySafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/proxy4tests/SafeERC20.sol',12);
    emit __StatementCoverageProxySafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/proxy4tests/SafeERC20.sol',2);
return _token.safeTransferFrom(_from,_to,_value);
   }

   function testsafeApprove(IERC20  _token, address  _spender, uint256  _value) public  returns (bool ){emit __FunctionCoverageProxySafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/proxy4tests/SafeERC20.sol',3);

emit __CoverageProxySafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/proxy4tests/SafeERC20.sol',16);
    emit __StatementCoverageProxySafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/proxy4tests/SafeERC20.sol',3);
return _token.safeApprove(_spender,_value);
   }

   function testclearApprove(IERC20  _token, address  _spender) public  returns (bool ){emit __FunctionCoverageProxySafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/proxy4tests/SafeERC20.sol',4);

emit __CoverageProxySafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/proxy4tests/SafeERC20.sol',20);
    emit __StatementCoverageProxySafeERC20('/home/fabianorodrigo/Projetos/bitbucket/solidity-test/workdir/SafeERC20/contracts/proxy4tests/SafeERC20.sol',4);
return _token.clearApprove(_spender);
   }


}